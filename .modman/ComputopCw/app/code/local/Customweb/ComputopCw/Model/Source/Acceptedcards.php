<?php
/**
 * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2015 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 *
 * @category	Customweb
 * @package		Customweb_ComputopCw
 * @version		2.0.172
 */

class Customweb_ComputopCw_Model_Source_Acceptedcards
{
	public function toOptionArray()
	{
		$options = array(
			array('value' => 'amex', 'label' => Mage::helper('adminhtml')->__("American Express")),
			array('value' => 'diners', 'label' => Mage::helper('adminhtml')->__("Diners Club")),
			array('value' => 'discover', 'label' => Mage::helper('adminhtml')->__("Discover")),
			array('value' => 'master', 'label' => Mage::helper('adminhtml')->__("MasterCard")),
			array('value' => 'maestro', 'label' => Mage::helper('adminhtml')->__("Maestro")),
			array('value' => 'visa', 'label' => Mage::helper('adminhtml')->__("Visa"))
		);
		return $options;
	}
}
