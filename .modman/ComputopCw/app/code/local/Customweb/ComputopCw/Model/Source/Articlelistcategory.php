<?php
/**
 * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2015 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 *
 * @category	Customweb
 * @package		Customweb_ComputopCw
 * @version		2.0.172
 */

class Customweb_ComputopCw_Model_Source_Articlelistcategory
{
	public function toOptionArray()
	{
		$options = array(
			array('value' => 'BEAUTY', 'label' => Mage::helper('adminhtml')->__("Parfümerie, Beauty")),
			array('value' => 'DIY', 'label' => Mage::helper('adminhtml')->__("Heimwerker")),
			array('value' => 'GARDEN', 'label' => Mage::helper('adminhtml')->__("Garten")),
			array('value' => 'TOYS', 'label' => Mage::helper('adminhtml')->__("Spielwaren")),
			array('value' => 'HOBBY', 'label' => Mage::helper('adminhtml')->__("Hobby, Freizeit")),
			array('value' => 'BOOKS', 'label' => Mage::helper('adminhtml')->__("Bücher")),
			array('value' => 'MAGAZINES', 'label' => Mage::helper('adminhtml')->__("Zeitschriften")),
			array('value' => 'MUSIC', 'label' => Mage::helper('adminhtml')->__("Musik, CDs, Platten, etc. (nur physisch)
						")),
			array('value' => 'HEALTH', 'label' => Mage::helper('adminhtml')->__("Gesundheitsartikel")),
			array('value' => 'OTCDRUGS', 'label' => Mage::helper('adminhtml')->__("Arzneimittel (nicht rezeptpflichtig)")),
			array('value' => 'PRESCRIPTIONDRUGS', 'label' => Mage::helper('adminhtml')->__("Arzneimittel (verschreibungspflichtig)
						")),
			array('value' => 'WHITEWARE', 'label' => Mage::helper('adminhtml')->__("Haushaltsgeräte")),
			array('value' => 'CLOTHING', 'label' => Mage::helper('adminhtml')->__("Kleidung, Textilien")),
			array('value' => 'FASHION', 'label' => Mage::helper('adminhtml')->__("Markenmode (Armani etc.)")),
			array('value' => 'SHOES', 'label' => Mage::helper('adminhtml')->__("Schuhe")),
			array('value' => 'ACCESSORIES', 'label' => Mage::helper('adminhtml')->__("Accessoires")),
			array('value' => 'FOOD', 'label' => Mage::helper('adminhtml')->__("Lebensmittel")),
			array('value' => 'BEVERAGES', 'label' => Mage::helper('adminhtml')->__("Getränke (nichtalkoholisch)")),
			array('value' => 'ALCOHOL', 'label' => Mage::helper('adminhtml')->__("Getränke (alkoholisch)")),
			array('value' => 'FURNITURE', 'label' => Mage::helper('adminhtml')->__("Möbel")),
			array('value' => 'DECORATION', 'label' => Mage::helper('adminhtml')->__("Dekorationsartikel")),
			array('value' => 'ART', 'label' => Mage::helper('adminhtml')->__("Kunst")),
			array('value' => 'ANTIQUES', 'label' => Mage::helper('adminhtml')->__("Antiquitäten")),
			array('value' => 'TOOLS', 'label' => Mage::helper('adminhtml')->__("Werkzeuge")),
			array('value' => 'MISC', 'label' => Mage::helper('adminhtml')->__("Sonstiges")),
			array('value' => 'ELECTRONICS', 'label' => Mage::helper('adminhtml')->__("Consumer, Electronic")),
			array('value' => 'LAPTOPS', 'label' => Mage::helper('adminhtml')->__("Notebooks, Laptops, Netbooks, etc.")),
			array('value' => 'COMPUTERS', 'label' => Mage::helper('adminhtml')->__("Computer, Workstation, etc.")),
			array('value' => 'CAMERAS', 'label' => Mage::helper('adminhtml')->__("Kameras")),
			array('value' => 'FLATSCREENS', 'label' => Mage::helper('adminhtml')->__("Flachbildschirme")),
			array('value' => 'SOFTWARE', 'label' => Mage::helper('adminhtml')->__("Computer, Software")),
			array('value' => 'COMPUTERGAMES', 'label' => Mage::helper('adminhtml')->__("Computer, Spiele, Video, Spiele")),
			array('value' => 'STATIONERY', 'label' => Mage::helper('adminhtml')->__("Schreibwaren")),
			array('value' => 'OFFICESUPPLIES', 'label' => Mage::helper('adminhtml')->__("Sonstiger, Bürobedarf")),
			array('value' => 'CARACCESSORIES', 'label' => Mage::helper('adminhtml')->__("Autozubehör")),
			array('value' => 'BIKEACCESSORIES', 'label' => Mage::helper('adminhtml')->__("Motorradzubehör")),
			array('value' => 'WATCHES', 'label' => Mage::helper('adminhtml')->__("Uhren")),
			array('value' => 'JEWELRY', 'label' => Mage::helper('adminhtml')->__("Schmuck")),
			array('value' => 'PETSUPPLIES', 'label' => Mage::helper('adminhtml')->__("Heimtierbedarf")),
			array('value' => 'GIFTS', 'label' => Mage::helper('adminhtml')->__("Geschenkartikel")),
			array('value' => 'RELIGIOUS', 'label' => Mage::helper('adminhtml')->__("Religionsartikel")),
			array('value' => 'FLOWERS', 'label' => Mage::helper('adminhtml')->__("Blumen, Pflanzen")),
			array('value' => 'NICHE', 'label' => Mage::helper('adminhtml')->__("Nischenprodukte")),
			array('value' => 'CRAFT', 'label' => Mage::helper('adminhtml')->__("Handwerksprodukte")),
			array('value' => 'WEDDING', 'label' => Mage::helper('adminhtml')->__("Hochzeitsartikel")),
			array('value' => 'SPORTS', 'label' => Mage::helper('adminhtml')->__("Sportartikel")),
			array('value' => 'ETHNIC', 'label' => Mage::helper('adminhtml')->__("Ethnische, Produkte")),
			array('value' => 'TOBACCO', 'label' => Mage::helper('adminhtml')->__("Tabakprodukte")),
			array('value' => 'PHOTOGRAPHY', 'label' => Mage::helper('adminhtml')->__("Fotografie")),
			array('value' => 'TICKETS', 'label' => Mage::helper('adminhtml')->__("Eintrittskarten, etc.")),
			array('value' => 'MOVIES', 'label' => Mage::helper('adminhtml')->__("Filme (DVD, Videokassetten)"))
		);
		return $options;
	}
}
