<?php
/**
 * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2015 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 *
 * @category	Customweb
 * @package		Customweb_ComputopCw
 * @version		2.0.172
 */

class Customweb_ComputopCw_Model_Source_Businesscustomers
{
	public function toOptionArray()
	{
		$options = array(
			array('value' => 'allow', 'label' => Mage::helper('adminhtml')->__("Allow business customers")),
			array('value' => 'deny', 'label' => Mage::helper('adminhtml')->__("Prevent business customers to use this
							payment method
						")),
			array('value' => 'filter', 'label' => Mage::helper('adminhtml')->__("Treat business customers as private persons
						"))
		);
		return $options;
	}
}
