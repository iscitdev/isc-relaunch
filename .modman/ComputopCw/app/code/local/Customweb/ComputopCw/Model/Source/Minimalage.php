<?php
/**
 * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2015 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 *
 * @category	Customweb
 * @package		Customweb_ComputopCw
 * @version		2.0.172
 */

class Customweb_ComputopCw_Model_Source_Minimalage
{
	public function toOptionArray()
	{
		$options = array(
			array('value' => 'none', 'label' => Mage::helper('adminhtml')->__("No minimal age")),
			array('value' => '6', 'label' => Mage::helper('adminhtml')->__("6")),
			array('value' => '7', 'label' => Mage::helper('adminhtml')->__("7")),
			array('value' => '8', 'label' => Mage::helper('adminhtml')->__("8")),
			array('value' => '9', 'label' => Mage::helper('adminhtml')->__("9")),
			array('value' => '10', 'label' => Mage::helper('adminhtml')->__("10")),
			array('value' => '11', 'label' => Mage::helper('adminhtml')->__("11")),
			array('value' => '12', 'label' => Mage::helper('adminhtml')->__("12")),
			array('value' => '13', 'label' => Mage::helper('adminhtml')->__("13")),
			array('value' => '14', 'label' => Mage::helper('adminhtml')->__("14")),
			array('value' => '15', 'label' => Mage::helper('adminhtml')->__("15")),
			array('value' => '16', 'label' => Mage::helper('adminhtml')->__("16")),
			array('value' => '17', 'label' => Mage::helper('adminhtml')->__("17")),
			array('value' => '18', 'label' => Mage::helper('adminhtml')->__("18")),
			array('value' => '19', 'label' => Mage::helper('adminhtml')->__("19")),
			array('value' => '20', 'label' => Mage::helper('adminhtml')->__("20")),
			array('value' => '21', 'label' => Mage::helper('adminhtml')->__("21"))
		);
		return $options;
	}
}
