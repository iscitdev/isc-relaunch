<?php
/**
 * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2013 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.customweb.ch/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.customweb.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 *
 * @category	Customweb
 * @package		Customweb_ComputopCw
 * @version		2.0.26
 */

class Customweb_ComputopCw_Model_UpdateObserver
{
	private $interval = 0;

	public function executeCron()
	{
		
		$this->interval = (int) (Mage::getStoreConfig('computopcw/general/update_interval'));

		// if the interval is greather than 0, then the orders need to be updated periodically
		if ($this->interval > 0) {
			$this->updatePayments();
		}
	}

	protected function updatePayments()
	{
		$lastSuccessfullUpdate = $this->getLastSuccessfullUpdateTime();
		$now = new DateTime();
		$diff = $now->diff($lastSuccessfullUpdate);
		if ($diff->i >= $this->interval) {
			try {
				$this->processUpdateableTransactions();
				$this->processBatchUpdate();
				Mage::getModel('core/config')->saveConfig('computopcw/general/last_update', $now->format('Y-m-d H:i:s'));
			} catch (Exception $e) {
				return;
			}
		}
	}

	protected function processUpdateableTransactions()
	{
		$paymentsToUpdate = Mage::getResourceModel('sales/order_payment_collection')->addAttributeToSelect('*')
			->addAttributeToFilter('cc_status', 'Y')
			->addAttributeToFilter('method', array(
				'like' => 'computopcw%'
			))
			->load();

		foreach ($paymentsToUpdate as $payment) {
			try {
				$transaction = Mage::helper('ComputopCw')->loadTransactionByPayment($payment->getId());
				if ($transaction !== null && $transaction->getTransactionObject() !== null) {
					$adapter = Mage::helper('ComputopCw')->createContainer()->getBean('Customweb_Payment_Update_IAdapter');
					$adapter->pullTransactionUpdate($transaction->getTransactionObject());
					$transaction->save();

					$adapter->confirmTransactionUpdate($transaction->getTransactionObject());
				}
			} catch (Exception $e) {
				Mage::log('Updating of transaction with id ' . $transaction->getTransactionId() . ' failed. \nReason: ' . $e->getMessage());
				$adapter->confirmFailedTransactionUpdate($transaction->getTransactionObject(), $e->getMessage());
			}
		}
	}

	protected function processBatchUpdate()
	{
		$adapter = Mage::helper('ComputopCw')->createContainer()->getBean('Customweb_Payment_Update_IAdapter');

		try {
			$lastUpdateValue = Mage::getStoreConfig('computopcw/general/last_update');
			if ($lastUpdateValue === null) {
				$lastUpdate = new DateTime();
				$lastUpdate->setTimestamp(0);
			}
			else {
				$lastUpdate = new DateTime($lastUpdateValue);
			}

			$transactionsToUpdate = $adapter->batchUpdate($lastUpdate);
			foreach ($transactionsToUpdate as $paymentId => $parameters) {
				try {
					$transaction = Mage::helper('ComputopCw')->loadTransactionByPayment($paymentId);
					$adapter->processTransactionUpdate($transaction->getTransactionObject(), $parameters);
					$transaction->save();

					$adapter->confirmTransactionUpdate($transaction->getTransactionObject());
				} catch (Exception $e) {
					Mage::log('Updating of transaction with id ' . $transaction->getTransactionId() . ' failed. \nReason: ' . $e->getMessage());
					$updateAdapter->confirmFailedTransactionUpdate($transaction->getTransactionObject(), $e->getMessage());
				}
			}
		} catch (Exception $e) {
			Mage::log('Batch update failed.\nReason: ' . $e->getMessage());
		}
	}

	private function getLastSuccessfullUpdateTime()
	{
		$lastSuccessfullUpdate = new DateTime();
		$lastSuccessfullUpdate->setTimestamp(0);

		try {
			$timeString = Mage::getStoreConfig('expercashtest/general/last_update');
			if ($timeString !== null) {
				$lastSuccessfullUpdate = new DateTime($timeString);
			}
		} catch (Exception $e) {}
		return $lastSuccessfullUpdate;
		
	}
}
