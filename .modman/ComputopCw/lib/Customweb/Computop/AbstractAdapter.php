<?php 
/**
  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2015 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */

//require_once 'Customweb/Computop/Configuration.php';
//require_once 'Customweb/Computop/IConstants.php';


class Customweb_Computop_AbstractAdapter implements Customweb_Computop_IConstants
{
	
	/**
	 * Configuration object.
	 *
	 * @var Customweb_Computop_Configuration
	 */
	private $configuration;
	
	/**
	 * @var Customweb_DependencyInjection_IContainer
	 */
	private $container = null;
	
	/**
	 * @var Customweb_Computop_Risk_Manager
	 */
	private $riskManager = null;

	/**
	 * @var Customweb_Computop_Debitor_Manager
	 */
	private $debitorManager = null;
	
	public function __construct(Customweb_Payment_IConfigurationAdapter $configuration, Customweb_DependencyInjection_IContainer $container, Customweb_Computop_Risk_Manager $manger) {
		$this->configuration = new Customweb_Computop_Configuration($configuration);
		$this->container = $container;
		$this->riskManager = $manger;
	}
	
	/**
	 * Returns the configuration object.
	 *
	 * @return Customweb_Computop_Configuration
	 */
	public function getConfiguration() {
		return $this->configuration;
	}
	
	/**
	 * @return Customweb_DependencyInjection_IContainer
	 */
	public function getContainer() {
		return $this->container;
	}

	/**
	 * @return Customweb_Computop_Debitor_Manager
	 */
	public function getDebitorManager(){
		return $this->getContainer()->getBean('Customweb_Computop_Debitor_Manager');
	}
	
	/**
	 * @return Customweb_Computop_Method_Factory
	 */
	public function getMethodFactory() {
		return $this->getContainer()->getBean('Customweb_Computop_Method_Factory');
	}

	public function getPaymentMethodByTransaction(Customweb_Computop_Authorization_Transaction $transaction){
		return $this->getMethodFactory()->getPaymentMethod($transaction->getTransactionContext()->getOrderContext()->getPaymentMethod(), $transaction->getAuthorizationMethod());
	}

	
	/**
	 * @return Customweb_Computop_Risk_Manager
	 */
	public function getRiskManger() {
		return $this->riskManager;
	}
	
	public function getNotificationUrl(Customweb_Payment_Authorization_ITransactionContext $transactionContext)
	{
		return $this->getContainer()->getBean('Customweb_Payment_Endpoint_IAdapter')->getUrl('process', 'index', $transactionContext->getCustomParameters());
	}
}