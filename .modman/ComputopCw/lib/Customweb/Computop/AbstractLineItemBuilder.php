<?php

/**
 *  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2015 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */

//require_once 'Customweb/Computop/Util.php';
//require_once 'Customweb/Payment/Authorization/DefaultInvoiceItem.php';
//require_once 'Customweb/Util/Currency.php';
//require_once 'Customweb/Payment/Authorization/IInvoiceItem.php';
//require_once 'Customweb/Util/Invoice.php';
//require_once 'Customweb/I18n/Translation.php';

abstract class Customweb_Computop_AbstractLineItemBuilder {
	
	/**
	 *
	 * @var Customweb_Payment_Authorization_IOrderContext
	 */
	private $orderContext = null;
	private $items = null;
	
	private $sum = null;
	
	private $addRoundingAdjustments = false;
	
	/**
	 *
	 * @param Customweb_Payment_Authorization_IOrderContext $orderContext
	 * @param string $items
	 */
	public function __construct(Customweb_Payment_Authorization_IOrderContext $orderContext, $items = null, $addRoundingAdjustments = false) {
		$this->orderContext = $orderContext;
		if ($items === null) {
			$this->items = $this->orderContext->getInvoiceItems();
		}
		else {
			$this->items = $items;
		}
		$this->addRoundingAdjustments = $addRoundingAdjustments;
	}

	public function build(){
		$lines = array();
		$partsClue = $this->getLineItemPartsClue();
		foreach ($this->getItems() as $item) {
			if ($this->isItemAllowedInList($item)) {
				$lines[] = implode($partsClue, $this->getLineItemParts($item));
			}
		}
		$lines = $this->addRoundingAdjustments($lines);
		
		return implode($this->getLineItemClue(), $lines);
	}

	/**
	 * Returns a array with all components of per line item.
	 *
	 * @return array
	 */
	abstract protected function getLineItemParts(Customweb_Payment_Authorization_IInvoiceItem $item);

	/**
	 * Returns an array with the following constants in it:
	 * - Customweb_Payment_Authorization_IInvoiceItem::TYPE_SHIPPING
	 * - Customweb_Payment_Authorization_IInvoiceItem::TYPE_DISCOUNT
	 * - Customweb_Payment_Authorization_IInvoiceItem::TYPE_PRODUCT
	 * - Customweb_Payment_Authorization_IInvoiceItem::TYPE_FEE
	 *
	 * @return array
	 */
	abstract protected function getAllowedProductTypes();


	protected function addRoundingAdjustments(array $lines) {
		if ($this->addRoundingAdjustments) {
			if ($this->sum === null) {
				throw new Exception("Rounding adjustments can only be applied when the sum is not NULL. You may forgot to call addAmountToSum().");
				$total = Customweb_Util_Invoice::getTotalAmountIncludingTax($this->items);
				$comparison = Customweb_Util_Currency::compareAmount($total, $this->sum, $this->getOrderContext()->getCurrencyCode());
				$partsClue = $this->getLineItemPartsClue();
				$item = null;
				if ($comparison > 0) {
					$item = new Customweb_Payment_Authorization_DefaultInvoiceItem("rounding", Customweb_I18n_Translation::__("Rounding Adjustments"), 0, $total - $this->sum, Customweb_Payment_Authorization_IInvoiceItem::TYPE_FEE);
				}
				else if ($comparison < 0) {
					$item = new Customweb_Payment_Authorization_DefaultInvoiceItem("rounding", Customweb_I18n_Translation::__("Rounding Adjustments"), 0, $this->sum - $total, Customweb_Payment_Authorization_IInvoiceItem::TYPE_DISCOUNT);
				}
				if ($item !== null) {
					$lines[] = implode($partsClue, $this->getLineItemParts($item));
				}
			}
		}	
		return $lines;
	}
	
	protected function addAmountToSum($amount) {
		if ($this->sum === null) {
			$this->sum = 0;
		}
		$this->sum += $amount;
		return $this->sum;
	}
	
	protected function isItemAllowedInList(Customweb_Payment_Authorization_IInvoiceItem $item) {
		$types = $this->getAllowedProductTypes();
		if (in_array($item->getType(), $types)) {
			return true;
		}
		else {
			return false;
		}
	}

	protected function getAmountIncludingTax(Customweb_Payment_Authorization_IInvoiceItem $item){
		$amount = $item->getAmountIncludingTax();
		if ($item->getType() == Customweb_Payment_Authorization_IInvoiceItem::TYPE_DISCOUNT) {
			$amount = -1 * $amount;
		}
		return $this->formatAmount($amount);
	}

	protected function getAmountExcludingTax(Customweb_Payment_Authorization_IInvoiceItem $item){
		$amount = $item->getAmountExcludingTax();
		if ($item->getType() == Customweb_Payment_Authorization_IInvoiceItem::TYPE_DISCOUNT) {
			$amount = -1 * $amount;
		}
		return $this->formatAmount($amount);
	}

	protected function getProductPriceIncludingTax(Customweb_Payment_Authorization_IInvoiceItem $item){
		$amount = $item->getAmountIncludingTax();
		if ($item->getType() == Customweb_Payment_Authorization_IInvoiceItem::TYPE_DISCOUNT) {
			$amount = -1 * $amount;
		}
		$amount = $amount / $item->getQuantity();
		return $this->formatAmount($amount);
	}

	protected function getProductPriceExcludingTax(Customweb_Payment_Authorization_IInvoiceItem $item){
		$amount = $item->getAmountExcludingTax();
		if ($item->getType() == Customweb_Payment_Authorization_IInvoiceItem::TYPE_DISCOUNT) {
			$amount = -1 * $amount;
		}
		$amount = $amount / $item->getQuantity();
		return $this->formatAmount($amount);
	}

	protected function formatAmount($amount){
		return Customweb_Computop_Util::formatAmount($amount, $this->getOrderContext()->getCurrencyCode());
	}

	protected function getOrderContext(){
		return $this->orderContext;
	}

	protected function getLineItemPartsClue(){
		return ';';
	}

	protected function getLineItemClue(){
		return '+';
	}

	protected function cleanString($string){
		$string = str_replace($this->getLineItemPartsClue(), ' ', str_replace($this->getLineItemClue(), ' ', $string));
		
		$replacements = $this->getCharReplacements();
		$string = str_replace(array_keys($replacements), array_values($replacements), $string);
		
		return $string;
	}

	protected function getItems(){
		return $this->items;
	}

	protected function getCharReplacements(){
		return array();
	}
	
}