<?php 
/**
  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2015 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */

//require_once 'Customweb/Computop/IConstants.php';
//require_once 'Customweb/Computop/Util.php';
//require_once 'Customweb/Http/Url.php';
//require_once 'Customweb/Util/Invoice.php';
//require_once 'Customweb/Computop/IParameterBuilder.php';


abstract class Customweb_Computop_AbstractParameterBuilder implements Customweb_Computop_IConstants, Customweb_Computop_IParameterBuilder {
	
	private $transactionContext;
	
	private $transaction;
	
	/**
	 * @var Customweb_DependencyInjection_IContainer
	 */
	private $container = null;
	
	/**
	 * @var Customweb_Computop_Configuration
	 */
	private $configuration = null;
	
	/**
	 * @var Customweb_Computop_Method_Factory
	 */
	private $paymentMethodFactory = null;
	
	public function __construct(Customweb_Computop_Authorization_Transaction $transaction, Customweb_DependencyInjection_IContainer $container) {
		$this->transactionContext = $transaction->getTransactionContext();
		$this->transaction = $transaction;
		$this->container = $container;
		$this->configuration = $container->getBean('Customweb_Computop_Configuration');
		$this->paymentMethodFactory = $container->getBean('Customweb_Computop_Method_Factory');
	}
	
	public function getTransID(){
		return Customweb_Computop_Util::getTransactionID(
			$this->getConfiguration(),
			$this->getTransaction()
		);
	}
	
	/**
	 * @return Customweb_DependencyInjection_IContainer
	 */
	protected function getContainer(){
		return $this->container;
	}
	
	/**
	 * 
	 * @return Customweb_Computop_Configuration
	 */
	protected function getConfiguration(){
		return $this->configuration;
	}
	
	/**
	 * 
	 * @return Customweb_Payment_Authorization_PaymentPage_ITransactionContext
	 */
	protected function getTransactionContext(){
		return $this->transactionContext;
	}
	
	/**
	 * 
	 * @return Customweb_Computop_Authorization_Transaction
	 */
	protected function getTransaction(){
		return $this->transaction;
	}
	
	/**
	 * @return Customweb_Payment_Authorization_IOrderContext
	 */
	protected function getOrderContext(){
		return $this->getTransactionContext()->getOrderContext();
	}
	
	/**
	 * @return Customweb_Computop_Method_DefaultMethod
	 */
	protected function getPaymentMethod(){
		return $this->getPaymentMethodFactory()->getPaymentMethod($this->getTransaction()->getTransactionContext()->getOrderContext()->getPaymentMethod(), $this->getTransaction()->getAuthorizationMethod());
	}
	
	/**
	 * @return Customweb_Computop_Method_Factory
	 */
	protected function getPaymentMethodFactory(){
		return $this->paymentMethodFactory;
	}
	
	/**
	 *
	 * @param array $parameters
	 * @return array
	 */
	protected function encryptParameters($parameters){
		return Customweb_Computop_Util::encryptParameters($parameters, $this->getConfiguration());
	}
	
	protected function postProcessParameters($parameters) {
		$parameters = $this->getSecurityParameters($parameters);
		$parameters = $this->encryptParameters($parameters);
		return $parameters;
	}
	
	
	protected function getBasicParameters(){
		$parameters = array();
		$parameters['MerchantID'] = $this->getConfiguration()->getMerchantId();
		$parameters['TransID'] = $this->getTransID();
		return $parameters;
	}
	
	protected function getSecurityParameters($parameters){
		$parameters['MAC'] = $this->getSignature($parameters);
		
		// We don't set the response to be encrypted. The notfication will still be encrypted
		// This enables us to distiguis the notification call(at which time we cannot check wheter the amount was
		// captured) from the sucess url call (at which time we can check for the captured/authorized amount).
		//$parameters['Response'] = 'encrypt';
		return $parameters;
	}
	
	protected function getAmountParameters(){
		$parameters = array();
	
		$parameters['Amount'] =  Customweb_Computop_Util::formatAmount(
				$this->getOrderContext()->getOrderAmountInDecimals(),
				$this->getOrderContext()->getCurrencyCode()
		);
		$parameters['Currency'] = $this->getOrderContext()->getCurrencyCode();
	
		return $parameters;
	}
	
	protected function getPaymentParameters($paymentId){
		$parameters = array();
		$parameters['PayID'] = $paymentId;
		return $parameters;
	}
	
	protected function getSignature(array $parameters){
		$payId = $this->getArrayValue($parameters, 'PayID', '');
		$transId = $this->getArrayValue($parameters, 'TransID', '');
		$merchantId = $this->getArrayValue($parameters, 'MerchantID', '');
		$amount = $this->getArrayValue($parameters, 'Amount', '');
		$currency = $this->getArrayValue($parameters, 'Currency', '');
	
		$stringToHash = $payId . '*' . $transId . '*' . $merchantId . '*' . $amount . '*' . $currency;
		return hash_hmac('sha256', $stringToHash, $this->getConfiguration()->getSignatureKey());
	}
	
	protected function getArrayValue(array $array, $key, $defaultValue){
		return isset($array[$key]) ? $array[$key] : $defaultValue;
	}

	protected function getNotificationUrl()
	{
		return $this->getContainer()->getBean('Customweb_Payment_Endpoint_IAdapter')->getUrl('process', 'index', $this->getTransactionContext()->getCustomParameters());
	}
	
	protected function getNotificaitonUrlCustomParameter($additionalParameters = array()) {
		$url = new Customweb_Http_Url($this->getNotificationUrl());
		$userData = $url->getQueryAsArray();
		$userData = array_merge($userData, $this->getTransactionContext()->getCustomParameters(), $additionalParameters);
		$userData['cwTransIdProcess'] = $this->getTransaction()->getExternalTransactionId();
	
		$parts = array();
		foreach ($userData as $key => $value) {
			$parts[] = $key . '=' . $value;
		}
	
		return implode('|', $parts);
	}
	
	protected function getCorrectedItems($itemsToCorrect){
		$origins = $this->getOrderContext()->getInvoiceItems();
		return Customweb_Util_Invoice::getItemsWithQuantityByAmount($itemsToCorrect, $origins);
	}
}