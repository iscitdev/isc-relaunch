<?php
/**
  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2015 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */

//require_once 'Customweb/Payment/Authorization/Iframe/IAdapter.php';
//require_once 'Customweb/Core/Exception/CastException.php';
//require_once 'Customweb/Core/Http/Response.php';
//require_once 'Customweb/Computop/Authorization/Transaction.php';
//require_once 'Customweb/Payment/Authorization/IAdapter.php';
//require_once 'Customweb/Util/Url.php';
//require_once 'Customweb/Computop/Util.php';
//require_once 'Customweb/Computop/AbstractAdapter.php';


abstract class Customweb_Computop_Authorization_AbstractAdapter extends Customweb_Computop_AbstractAdapter implements Customweb_Payment_Authorization_IAdapter
{

	public function isAuthorizationMethodSupported(Customweb_Payment_Authorization_IOrderContext $orderContext){
		// Check if we can resolve the payment method.
		try {
			$this->getPaymentMethod($orderContext->getPaymentMethod());
			return true;
		}
		catch(Customweb_Payment_Authorization_Method_PaymentMethodResolutionException $e) {
			return false;
		}
	}
	
	protected function getPaymentMethod(Customweb_Payment_Authorization_IPaymentMethod $method) {
		return $this->getMethodFactory()->getPaymentMethod($method, $this->getAuthorizationMethodName());
	}
	
	public function preValidate(Customweb_Payment_Authorization_IOrderContext $orderContext,
		Customweb_Payment_Authorization_IPaymentCustomerContext $paymentContext) {
		
		$paymentMethod = $this->getPaymentMethod($orderContext->getPaymentMethod());
		$paymentMethod->preValidate($orderContext, $paymentContext);
	}
	
	public function validate(Customweb_Payment_Authorization_IOrderContext $orderContext, 
			Customweb_Payment_Authorization_IPaymentCustomerContext $paymentContext, array $formData) {
		
		$paymentMethod = $this->getPaymentMethod($orderContext->getPaymentMethod());
		$paymentMethod->validate($orderContext, $paymentContext, $formData);
		
		//$this->performSolvencyCheck($orderContext, $paymentContext, null, array());
	}

	public function isDeferredCapturingSupported(Customweb_Payment_Authorization_IOrderContext $orderContext, Customweb_Payment_Authorization_IPaymentCustomerContext $paymentContext) {
		return $orderContext->getPaymentMethod()->existsPaymentMethodConfigurationValue('capturing');
	}
	
	public function processAuthorization(Customweb_Payment_Authorization_ITransaction $transaction, array $parameters) {
		$this->authorize($transaction, $parameters);
		return $this->finalizeAuthorizationRequest($transaction);
	}
	
	public function authorize(Customweb_Payment_Authorization_ITransaction $transaction, array $parameters){
		
		if (!($transaction instanceof Customweb_Computop_Authorization_Transaction)) {
			throw new Customweb_Core_Exception_CastException('Customweb_Computop_Authorization_Transaction');
		}
		
// 		$this->cleanupTransaction($transaction);
	
		// If the parameters are encrypted we know that it is a server-to-server response, and
		// we therfore cannot yet send a payment inquiry.
		if(isset($parameters['Len'])){
			$transaction->setCalledByServer(true);
		}
// 		else if(!$transaction->isAuthorizationFailed()){
// 			$this->inquireTransactionStatus($transaction, $parameters);
// 		}
	
		// In case the authorization failed, we stop processing here
		if ($transaction->isAuthorizationFailed()) {
			return;
		}
	
		// In case the transaction is authorized, we do not have to do anything here.            	   		  		 
		if ($transaction->isAuthorized()) {
			return;
		}
		
		$parameters = Customweb_Computop_Util::decryptIfNeeded($this->getConfiguration(), $parameters);
	
		$paymentMethod = $this->getPaymentMethodByTransaction($transaction);
		try {
			$paymentMethod->processAuthorization($transaction, $parameters);
		}
		catch(Customweb_Payment_Exception_PaymentErrorException $e) {
			$transaction->setAuthorizationFailed($e->getErrorMessage());
		}
		catch(Exception $e) {
			$transaction->setAuthorizationFailed($e->getMessage());
		}
	}
	
	public function finalizeAuthorizationRequest(Customweb_Payment_Authorization_ITransaction $transaction){

		if (!($transaction instanceof Customweb_Computop_Authorization_Transaction)) {
			throw new Customweb_Core_Exception_CastException('Customweb_Computop_Authorization_Transaction');
		}
		
		$paymentMethod = $this->getPaymentMethodByTransaction($transaction);
		$content = $paymentMethod->getFinalizeContent($transaction);
	
		if (!empty($content)) {
			return Customweb_Core_Http_Response::_($content);
		}
		else if($transaction->isOutputToShow()){
			return Customweb_Core_Http_Response::_($transaction->getOutputToShow());
		}
		else if($transaction->isCalledByServer()){
			$response = Customweb_Core_Http_Response::_('OK');
			
			
			if($transaction->isRedirectNeeded()){
// 				$response->setLocation($transaction->getRedirectUrl());
				$response = Customweb_Core_Http_Response::htmRedirect($transaction->getRedirectUrl());
			}
			else{
				if ($transaction->getAuthorizationMethod() == Customweb_Payment_Authorization_Iframe_IAdapter::AUTHORIZATION_METHOD_NAME) {
					$url = Customweb_Util_Url::appendParameters($transaction->getTransactionContext()->getIframeBreakOutUrl(), $transaction->getTransactionContext()->getCustomParameters());
					$response = Customweb_Core_Http_Response::htmRedirect($url);
				}
				else {
					if ($transaction->isAuthorizationFailed()) {
						$response = Customweb_Core_Http_Response::htmRedirect($transaction->getFailedUrl());
					}
					else if ($transaction->isAuthorized()) {
						$response = Customweb_Core_Http_Response::htmRedirect($transaction->getSuccessUrl());
					}
				}
			}
			
			return $response;
		}
		else if ($transaction->isAuthorizationFailed()) {
			$response = new Customweb_Core_Http_Response();
			$response->setLocation($transaction->getFailedUrl());
			return $response;
		}
		else if ($transaction->isAuthorized()) {
			$response = new Customweb_Core_Http_Response();
			$response->setLocation($transaction->getSuccessUrl());
			return $response;
		}
		else if($transaction->isRedirectNeeded()){
			$response = new Customweb_Core_Http_Response();
			$response->setLocation($transaction->getRedirectUrl());
			return $response;
		}
		else {
			return Customweb_Core_Http_Response::_("The transaction can be only in authorized or failed state.");
		}
	}
	
}