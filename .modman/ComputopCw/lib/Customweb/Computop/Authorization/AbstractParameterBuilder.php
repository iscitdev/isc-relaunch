<?php
/**
  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2015 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */

//require_once 'Customweb/Computop/AbstractParameterBuilder.php';


abstract class Customweb_Computop_Authorization_AbstractParameterBuilder extends Customweb_Computop_AbstractParameterBuilder {
	
	private $formData = array();
	
	public function __construct(Customweb_Computop_Authorization_Transaction $transaction, Customweb_DependencyInjection_IContainer $container, $formData) {
		parent::__construct($transaction, $container);
		$this->formData = $formData;
	}
	
	public function build() {
		$parameters = $this->buildParametersInner();
		
		if (!is_array($parameters)) {
			throw new Exception("The 'buildParametersInner' method returns not an array.");
		}

		$additionalParams = $this->getPaymentMethod()->getAuthorizationParameters($this->getTransaction(), $this->getFormData());
		$parameters = array_merge($parameters, $additionalParams);
		
		$parameters = $this->postProcessParameters($parameters);
		
		return $parameters;
	}
	
	/**
	 * Subclasses must implement this method to provide parameters according
	 * to the actual authorization method.
	 * 
	 * @return array
	 */
	protected abstract function buildParametersInner();
	
	protected function getFormData() {
		return $this->formData;
	}
	
	protected function getAuthorizationParameters(){
		$parameters = array();
		if($this->getTransaction()->getTransactionContext()->createRecurringAlias()){
			$parameters['RTF'] = 'I';
		}
	
		return $parameters;
	}
	
}