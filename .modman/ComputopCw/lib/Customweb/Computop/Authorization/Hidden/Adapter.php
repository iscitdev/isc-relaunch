<?php
/**
  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2015 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */

//require_once 'Customweb/Payment/Authorization/Hidden/IAdapter.php';
//require_once 'Customweb/Computop/Authorization/AbstractAdapter.php';
//require_once 'Customweb/Computop/Authorization/RedirectParameterBuilder.php';
//require_once 'Customweb/Computop/Authorization/Transaction.php';


/**
 * @Bean
 */
class Customweb_Computop_Authorization_Hidden_Adapter extends Customweb_Computop_Authorization_AbstractAdapter
implements Customweb_Payment_Authorization_Hidden_IAdapter {
	
	private $parameterCache = array();
	
	public function getAuthorizationMethodName() {
		return self::AUTHORIZATION_METHOD_NAME;
	}
	
	public function getAdapterPriority() {
		return 200;
	}
	
	public function createTransaction(Customweb_Payment_Authorization_Hidden_ITransactionContext $transactionContext, $failedTransaction) {
		$transaction = $this->getPaymentMethod($transactionContext->getOrderContext()->getPaymentMethod())->createTransaction($transactionContext, $failedTransaction, self::AUTHORIZATION_METHOD_NAME);
		$transaction->setAdapterClassName(get_class($this));
		$transaction->setLiveTransaction(!$this->getConfiguration()->isTestMode());
		return $transaction;
	}
	
	public function getVisibleFormFields(Customweb_Payment_Authorization_IOrderContext $orderContext, $aliasTransaction, $failedTransaction, $paymentCustomerContext){
		return array_merge(
				$this->getPaymentMethod($orderContext->getPaymentMethod())->getVisibleFormFields($orderContext, $aliasTransaction, $failedTransaction, $paymentCustomerContext, $this->getAuthorizationMethodName()),
				$this->getRiskManger()->getVisibleFormElements($orderContext, $paymentCustomerContext)
		);
	}
	
	public function getHiddenFormFields(Customweb_Payment_Authorization_ITransaction $transaction) {
		if (!($transaction instanceof Customweb_Computop_Authorization_Transaction)) {
			throw new Exception("The given transaction is not of type Customweb_Computop_Authorization_Transaction.");
		}
		if (!isset($this->parameterCache[$transaction->getExternalTransactionId()])) {
			try{
				$builder = new Customweb_Computop_Authorization_RedirectParameterBuilder($transaction, $this->getContainer(), array());
				$this->parameterCache[$transaction->getExternalTransactionId()] = $builder->build();
			}
			catch(Customweb_Payment_Exception_PaymentErrorException $e) {
				$transaction->setAuthorizationFailed($e->getErrorMessage());
				return array();
			}
			catch(Exception $e){
				$transaction->setAuthorizationFailed($e->getMessage());
				return array();
			}
		}
		
		return $this->parameterCache[$transaction->getExternalTransactionId()];
	}
	
	public function getFormActionUrl(Customweb_Payment_Authorization_ITransaction $transaction) {
		if (!($transaction instanceof Customweb_Computop_Authorization_Transaction)) {
			throw new Exception("The given transaction is not of type Customweb_Computop_Authorization_Transaction.");
		}
		$this->getHiddenFormFields($transaction);
		if ($transaction->isAuthorizationFailed()) {
			return $transaction->getFailedUrl();
		}
		else {
			return $this->getConfiguration()->getBaseUrl() . $this->getPaymentMethodByTransaction($transaction)->getAuthorizationUrl();
		}
	}
	

	public function processAuthorization(Customweb_Payment_Authorization_ITransaction $transaction, array $parameters){
		if (!($transaction instanceof Customweb_Computop_Authorization_Transaction)) {
			throw new Exception("The given transaction is not of type Customweb_Computop_Authorization_Transaction.");
		}
		try {
			$this->getRiskManger()->checkTransaction($transaction, $parameters);
		}
		catch(Customweb_Payment_Exception_PaymentErrorException $e) {
			$transaction->setAuthorizationFailed($e->getErrorMessage());
			return $this->finalizeAuthorizationRequest($transaction);
		}
		catch(Exception $e){
			$transaction->setAuthorizationFailed($e->getMessage());
			return $this->finalizeAuthorizationRequest($transaction);
		}
		$this->authorize($transaction, $parameters);
		return $this->finalizeAuthorizationRequest($transaction);
	}
	
}