<?php
/**
  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2015 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */



/**
 * Defines a task which is executed at a given time. The object is serialized. Hence this 
 * class must be serializable. Means the object should never have any references to adatpers, containers etc.
 * 
 * @author Thomas Hunziker
 *
 */
interface Customweb_Computop_Authorization_ITransactionUpdateTask {
	
	/**
	 * The task which should be executed can be triggered inside this method.
	 * 
	 * @param Customweb_Computop_Authorization_Transaction $transaction
	 * @param Customweb_DependencyInjection_IContainer $container
	 * @return void
	 */
	public function execute(Customweb_Computop_Authorization_Transaction $transaction, Customweb_DependencyInjection_IContainer $container);
	
	/**
	 * Returns the DateTime on which the task should be executed. The exact time of execution
	 * can not be guaranteed. The task is rescheduled as long this method returns a value != null.
	 * 
	 * @return DateTime
	 */
	public function getExecutionTime();
	
}