<?php
/**
  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2015 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */

//require_once 'Customweb/Payment/Authorization/Iframe/IAdapter.php';
//require_once 'Customweb/Computop/BackendOperation/Adapter/CaptureAdapter.php';
//require_once 'Customweb/Core/Exception/CastException.php';
//require_once 'Customweb/Core/Http/Response.php';
//require_once 'Customweb/Computop/Authorization/AbstractAdapter.php';
//require_once 'Customweb/Computop/Authorization/RedirectParameterBuilder.php';
//require_once 'Customweb/Computop/Authorization/Transaction.php';
//require_once 'Customweb/Util/Url.php';
//require_once 'Customweb/Util/Html.php';
//require_once 'Customweb/I18n/Translation.php';


/**
 * @Bean
 */
class Customweb_Computop_Authorization_Iframe_Adapter extends Customweb_Computop_Authorization_AbstractAdapter
implements Customweb_Payment_Authorization_Iframe_IAdapter {
	
	private static $iframeRedirect = false;
	
	public function getAuthorizationMethodName(){
		return self::AUTHORIZATION_METHOD_NAME;
	}
	
	public function getAdapterPriority() {
		return 100;
	}

	public function createTransaction(Customweb_Payment_Authorization_Iframe_ITransactionContext $transactionContext, $failedTransaction){
		$transaction = $this->getPaymentMethod($transactionContext->getOrderContext()->getPaymentMethod())->createTransaction($transactionContext, $failedTransaction, self::AUTHORIZATION_METHOD_NAME);
		$transaction->setAdapterClassName(get_class($this));
		$transaction->setLiveTransaction(!$this->getConfiguration()->isTestMode());
		return $transaction;
	}
	
	public function getVisibleFormFields(Customweb_Payment_Authorization_IOrderContext $orderContext,
			$aliasTransaction,
			$failedTransaction,
			$paymentCustomerContext){
		
		return array_merge(
			$this->getPaymentMethod($orderContext->getPaymentMethod())->getVisibleFormFields($orderContext, $aliasTransaction, $failedTransaction, $paymentCustomerContext, $this->getAuthorizationMethodName()),
			$this->getRiskManger()->getVisibleFormElements($orderContext, $paymentCustomerContext)
		);
	}
	
	public function getIframeUrl(Customweb_Payment_Authorization_ITransaction $transaction, array $formData) {
		
		if (!($transaction instanceof Customweb_Computop_Authorization_Transaction)) {
			throw new Customweb_Core_Exception_CastException('Customweb_Computop_Authorization_Transaction');
		}

		if (!isset($this->parameterCache[$transaction->getExternalTransactionId()])) {
			try{
				$this->getRiskManger()->checkTransaction($transaction, $formData);
				$url = $this->getConfiguration()->getBaseUrl() . $this->getPaymentMethodByTransaction($transaction)->getAuthorizationUrl();
				$builder = new Customweb_Computop_Authorization_RedirectParameterBuilder($transaction, $this->getContainer(), $formData);
				$parameters = $builder->build();
				$url = Customweb_Util_Url::appendParameters($url, $parameters);
			}
			catch(Customweb_Payment_Exception_PaymentErrorException $e) {
				$transaction->setAuthorizationFailed($e->getErrorMessage());
				$url = $transaction->getFailedUrl();
			}
			catch(Exception $e){
				$transaction->setAuthorizationFailed($e->getMessage());
				$url = $transaction->getFailedUrl();
			}
		}
		
		if (strlen($url) > 2000) {
			$transaction->setIframeFormData($formData);
			return Customweb_Util_Url::appendParameters($this->getNotificationUrl($transaction->getTransactionContext()), array('computop-iframe-redirect' => 'true'));
		}
		else {
			return $url;
		}
	}
	
	public function processAuthorization(Customweb_Payment_Authorization_ITransaction $transaction, array $parameters){
		if (!($transaction instanceof Customweb_Computop_Authorization_Transaction)) {
			throw new Customweb_Core_Exception_CastException('Customweb_Computop_Authorization_Transaction');
		}
		
		if (isset($parameters['computop-iframe-redirect'])) {
			self::$iframeRedirect = true;
		}
		else {
			$this->authorize($transaction, $parameters);
		}
		return $this->finalizeAuthorizationRequest($transaction);
	}
	
	public function finalizeAuthorizationRequest(Customweb_Payment_Authorization_ITransaction $transaction){
		if (!($transaction instanceof Customweb_Computop_Authorization_Transaction)) {
			throw new Customweb_Core_Exception_CastException('Customweb_Computop_Authorization_Transaction');
		}
		
		if (self::$iframeRedirect) {
			$url = $this->getConfiguration()->getBaseUrl() . $this->getPaymentMethodByTransaction($transaction)->getAuthorizationUrl();
			$builder = new Customweb_Computop_Authorization_RedirectParameterBuilder($transaction, $this->getContainer(), $transaction->getIframeFormData());
			$parameters = $builder->buildParameters();
				
			$output = '<form action="' . $url . '" method="POST" name="iframe_computop_form">';
				$output .= Customweb_Util_Html::buildHiddenInputFields($parameters);
				$output .= Customweb_I18n_Translation::__('<p>If you are not redirect with in 10 seconds, please press the button below:</p>');
				$output .= '<input type="submit" value="' . Customweb_I18n_Translation::__("Continue") . '" />';
				$output .= '<script type="text/javascript"> 
						document.iframe_computop_form.submit(); 
					</script>';
			$output .= '</form>';
			return Customweb_Core_Http_Response::_($output);
		}
		else {
			return parent::finalizeAuthorizationRequest($transaction);
		}
	}
	
	public function getIframeHeight(Customweb_Payment_Authorization_ITransaction $transaction, array $formData) {
		if ($transaction->getPaymentMethod()->existsPaymentMethodConfigurationValue('iframe_height')) {
			return $transaction->getPaymentMethod()->getPaymentMethodConfigurationValue('iframe_height');
		}
		else {
			return 500;
		}
	}
	
	protected function captureTransaction($transaction){
		$capturingAdapter = new Customweb_Computop_BackendOperation_Adapter_CaptureAdapter($this->getConfigurationAdapter());
		$capturingAdapter->capture($transaction);
	}
	
}