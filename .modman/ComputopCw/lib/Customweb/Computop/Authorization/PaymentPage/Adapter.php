<?php
/**
  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2015 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */

//require_once 'Customweb/Computop/BackendOperation/Adapter/CaptureAdapter.php';
//require_once 'Customweb/Computop/Authorization/AbstractAdapter.php';
//require_once 'Customweb/Computop/Authorization/RedirectParameterBuilder.php';
//require_once 'Customweb/Computop/Authorization/Transaction.php';
//require_once 'Customweb/Payment/Authorization/PaymentPage/IAdapter.php';


/**
 * @Bean
 */
class Customweb_Computop_Authorization_PaymentPage_Adapter extends Customweb_Computop_Authorization_AbstractAdapter
implements Customweb_Payment_Authorization_PaymentPage_IAdapter {
	
	private $parameterCache = array();
	
	public function getAuthorizationMethodName(){
		return self::AUTHORIZATION_METHOD_NAME;
	}
	
	public function getAdapterPriority() {
		return 100;
	}

	public function createTransaction(Customweb_Payment_Authorization_PaymentPage_ITransactionContext $transactionContext, $failedTransaction){
		$transaction = $this->getPaymentMethod($transactionContext->getOrderContext()->getPaymentMethod())->createTransaction($transactionContext, $failedTransaction, self::AUTHORIZATION_METHOD_NAME);
		$transaction->setAdapterClassName(get_class($this));
		$transaction->setLiveTransaction(!$this->getConfiguration()->isTestMode());
		return $transaction;
	}
	
	public function getVisibleFormFields(Customweb_Payment_Authorization_IOrderContext $orderContext,
			$aliasTransaction,
			$failedTransaction,
			$paymentCustomerContext){
		
		return array_merge(
			$this->getPaymentMethod($orderContext->getPaymentMethod())->getVisibleFormFields($orderContext, $aliasTransaction, $failedTransaction, $paymentCustomerContext, $this->getAuthorizationMethodName()),
			$this->getRiskManger()->getVisibleFormElements($orderContext, $paymentCustomerContext)
		);
	}
	
	public function getRedirectionUrl(Customweb_Payment_Authorization_ITransaction $transaction, array $formData){
		return null;
	}
	
	public function isHeaderRedirectionSupported(Customweb_Payment_Authorization_ITransaction $transaction, array $formData){
		return false;
	}
	
	public function getParameters(Customweb_Payment_Authorization_ITransaction $transaction, array $formData){
		if (!isset($this->parameterCache[$transaction->getExternalTransactionId()])) {
			try{
				$this->getRiskManger()->checkTransaction($transaction, $formData);
				$builder = new Customweb_Computop_Authorization_RedirectParameterBuilder($transaction, $this->getContainer(), $formData);
				$this->parameterCache[$transaction->getExternalTransactionId()] = $builder->build();
			}
			catch(Customweb_Payment_Exception_PaymentErrorException $e) {
				$transaction->setAuthorizationFailed($e->getErrorMessage());
				return array();
			}
			catch(Exception $e){
				$transaction->setAuthorizationFailed($e->getMessage());
				return array();
			}
		}
		
		return $this->parameterCache[$transaction->getExternalTransactionId()];
	}
	
	public function getFormActionUrl(Customweb_Payment_Authorization_ITransaction $transaction, array $formData){
		if (!($transaction instanceof Customweb_Computop_Authorization_Transaction)) {
			throw new Exception("The given transaction is not of type Customweb_Computop_Authorization_Transaction.");
		}
		$this->getParameters($transaction, $formData);
		if ($transaction->isAuthorizationFailed()) {
			return $transaction->getFailedUrl();
		}
		else {
			return $this->getPaymentPageUrl($transaction);
		}
	}

	protected function captureTransaction($transaction){
		$capturingAdapter = new Customweb_Computop_BackendOperation_Adapter_CaptureAdapter($this->getConfigurationAdapter());
		$capturingAdapter->capture($transaction);
	}
	
	protected function getPaymentPageUrl($transaction){
		return $this->getConfiguration()->getBaseUrl() . $this->getPaymentMethodByTransaction($transaction)->getAuthorizationUrl();
	}
}