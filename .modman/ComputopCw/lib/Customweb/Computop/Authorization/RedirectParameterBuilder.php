<?php 
/**
  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2015 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */

//require_once 'Customweb/Core/Url.php';
//require_once 'Customweb/Computop/Authorization/AbstractParameterBuilder.php';
//require_once 'Customweb/I18n/Translation.php';


class Customweb_Computop_Authorization_RedirectParameterBuilder extends Customweb_Computop_Authorization_AbstractParameterBuilder {

	public function buildParametersInner() {
		return array_merge(
				$this->getBasicParameters(),
				$this->getAuthorizationParameters(),
				$this->getAmountParameters(),
				$this->getUrlParameters()
		);
	}
	
	protected function getUrlParameters(){
		$parameters = array();
		$feedbackUrl = $this->getContainer()->getBean('Customweb_Payment_Endpoint_IAdapter')->getUrl('process', 'index', array('utm_nooverride' => '1'));
		$url = new Customweb_Core_Url($feedbackUrl);
		if (strtolower($url->getScheme()) !== 'https') {
			throw new Exception(Customweb_I18n_Translation::__("The notification URL requires to use the HTTPS protocol. URL: !url", array('!url' => $url->toString())));
		}
		
		// We have to remove the query part of the URL. The query has to be added in the 'Custom' parameter.
		$processableFeedbackUrl = new Customweb_Core_Url($url);
		$processableFeedbackUrl->setQuery(array());
		$feedbackUrl = $processableFeedbackUrl->toString();
		
		$parameters['URLSuccess'] = $feedbackUrl;
		$parameters['URLFailure'] = $feedbackUrl;
		$parameters['URLNotify'] = $feedbackUrl;
		$parameters['Custom'] = $this->getNotificaitonUrlCustomParameter($url->getQueryAsArray());
		
		return $parameters;
	}
	
}