<?php
/**
  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2015 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */

//require_once 'Customweb/Payment/Authorization/Server/IAdapter.php';
//require_once 'Customweb/Computop/Authorization/AbstractAdapter.php';


/**
 * @Bean
 */
class Customweb_Computop_Authorization_Server_Adapter extends Customweb_Computop_Authorization_AbstractAdapter
implements Customweb_Payment_Authorization_Server_IAdapter {
	
	public function getAuthorizationMethodName() {
		return self::AUTHORIZATION_METHOD_NAME;
	}
	
	public function getAdapterPriority() {
		return 300;
	}
	
	public function createTransaction(Customweb_Payment_Authorization_Server_ITransactionContext $transactionContext, $failedTransaction){
		$transaction = $this->getPaymentMethod($transactionContext->getOrderContext()->getPaymentMethod())->createTransaction($transactionContext, $failedTransaction, self::AUTHORIZATION_METHOD_NAME);
		$transaction->setAdapterClassName(get_class($this));
		$transaction->setLiveTransaction(!$this->getConfiguration()->isTestMode());
		return $transaction;
	}
	
	public function getVisibleFormFields(Customweb_Payment_Authorization_IOrderContext $orderContext, $aliasTransaction, $failedTransaction, $paymentCustomerContext){
		return array_merge(
			$this->getPaymentMethod($orderContext->getPaymentMethod())->getVisibleFormFields($orderContext, $aliasTransaction, $failedTransaction, $paymentCustomerContext, $this->getAuthorizationMethodName()),
			$this->getRiskManger()->getVisibleFormElements($orderContext, $paymentCustomerContext)
		);
	}
	
	public function processAuthorization(Customweb_Payment_Authorization_ITransaction $transaction, array $parameters){
		try{
			$this->getRiskManger()->checkTransaction($transaction, $parameters);
		}
		catch(Customweb_Payment_Exception_PaymentErrorException $e) {
			$transaction->setAuthorizationFailed($e->getErrorMessage());
			return $this->finalizeAuthorizationRequest($transaction);
		}
		catch(Exception $e){
			$transaction->setAuthorizationFailed($e->getMessage());
			return $this->finalizeAuthorizationRequest($transaction);
		}
		$this->authorize($transaction, $parameters);
		return $this->finalizeAuthorizationRequest($transaction);
	}
	
	public function getProcessControllerName() {
		return 'process';
	}
	
	public function getProcessActionName() {
		return 'index';
	}
	
	
	
}