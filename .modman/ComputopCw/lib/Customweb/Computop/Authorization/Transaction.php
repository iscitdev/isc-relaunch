<?php
/**
  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2015 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */

//require_once 'Customweb/Payment/Authorization/DefaultTransaction.php';
//require_once 'Customweb/Util/Url.php';
//require_once 'Customweb/Computop/Authorization/TransactionRefund.php';
//require_once 'Customweb/Core/DateTime.php';
//require_once 'Customweb/I18n/Translation.php';
//require_once 'Customweb/Computop/Authorization/TransactionCapture.php';
//require_once 'Customweb/Computop/Authorization/TransactionCancel.php';


class Customweb_Computop_Authorization_Transaction extends Customweb_Payment_Authorization_DefaultTransaction
{
	
	private $pseudoCardNumber = null;
	private $cardExpiry = null;
	private $redirectUrl = null;
	private $authorisationRequestSent = false;
	private $calledByServer = false;
	private $outputToShow = "";
	private $cardBrand = "";
	private $debitorPayId = "";
	private $externalTransId = "";
	private $overridenOrderStatus = null;
	private $iframeFormData = array();
	private $adapterClassName = null;
	private $paymentInformation = null;
	
	/**
	 * @var Customweb_Core_DateTime
	 */
	private $datePaid = null;
	
	/**
	 * @var Customweb_Computop_Authorization_ITransactionUpdateTask[]
	 */
	private $updateTasks = array();
	
	/**
	 * @var boolean
	 */
	private $multiCaptureEnabled = false;
	
	/**
	 * @var Customweb_Computop_Authorization_ILabelProvider[]
	 */
	private $labelProviders = array();
	
	/**
	 * @var string
	 */
	private $invoiceNumber;
	
	public function __construct(Customweb_Payment_Authorization_ITransactionContext $transactionContext) {
		parent::__construct($transactionContext);
		$this->setPaymentId($this->getExternalTransactionId());
	}
	
	/**
	 * @param Customweb_Computop_Authorization_ILabelProvider $provider
	 * @return Customweb_Computop_Authorization_Transaction
	 */
	public function registerLabelProvider(Customweb_Computop_Authorization_ILabelProvider $provider) {
		$this->labelProviders[] = $provider;
		return $this;
	}
	
	/**
	 * @return Customweb_Computop_Authorization_ILabelProvider[]
	 */
	public function getLabelProviders() {
		return $this->labelProviders;
	}

	public function getTransactionSpecificLabels() {
		$parameters = $this->getAuthorizationParameters();
		if ($parameters === null) {
			$parameters = array();
		}
		
		$parameters = array_change_key_case($parameters,CASE_LOWER);
		$labels = array();
		
		if (isset($parameters['amountauth'])) {
			$labels['amountauth'] = array(
					'label' => Customweb_I18n_Translation::__('Authorized amount'),
					'value' => $parameters['amountauth'],
			);
		}
		
		if (isset($parameters['amountcap'])) {
			$labels['amountcap'] = array(
					'label' => Customweb_I18n_Translation::__('Captured amount'),
					'value' => $parameters['amountcap'],
			);
		}
		
		if (isset($parameters['amountcred'])) {
			$labels['amountcred'] = array(
					'label' => Customweb_I18n_Translation::__('Refunded amount'),
					'value' => $parameters['amountcred'],
			);
		}
		
		if (isset($parameters['mid'])) {
			$labels['mid'] = array(
					'label' => Customweb_I18n_Translation::__('Merchant ID'),
					'value' => $parameters['mid'],
			);
		}
		
// 		if (isset($parameters['payid'])) {
// 			$labels['payid'] = array(
// 					'label' => Customweb_I18n_Translation::__('Pay ID'),
// 					'value' => $parameters['payid'],
// 			);
// 		}

		if (isset($parameters['descriptor'])) {
			$labels['descriptor'] = array(
				'label' => Customweb_I18n_Translation::__('Descriptor'),
				'value' => $parameters['descriptor'],
			);
		}

		if (isset($parameters['xid'])) {
			$labels['xid'] = array(
					'label' => Customweb_I18n_Translation::__('XID'),
					'value' => $parameters['xid'],
			);
		}
		if (isset($parameters['status'])) {
			$labels['status'] = array(
					'label' => Customweb_I18n_Translation::__('Status '),
					'value' => $parameters['status'],
			);
		}
		
		if (isset($parameters['code']) && intval($parameters['code']) != 0) {
			$labels['code'] = array(
					'label' => Customweb_I18n_Translation::__('Status code'),
					'value' => $parameters['code'],
			);
		}
		
		if (isset($parameters['type'])) {
			$labels['type'] = array(
					'label' => Customweb_I18n_Translation::__('Payment type'),
					'value' => $parameters['type'],
			);
		}
		
		if (isset($parameters['pcnr'])) {
			$labels['pcnr'] = array(
					'label' => Customweb_I18n_Translation::__('Pseudo card number'),
					'value' => $parameters['pcnr'],
			);
		}
		
		if (isset($parameters['ccexpiry'])) {
			$labels['ccexpiry'] = array(
					'label' => Customweb_I18n_Translation::__('Card number expiry'),
					'value' => $parameters['ccexpiry'],
			);
		}
		
		if (isset($parameters['ccbrand'])) {
			$labels['ccbrand'] = array(
					'label' => Customweb_I18n_Translation::__('Card brand'),
					'value' => $parameters['ccbrand'],
			);
		}
		
		if (isset($parameters['zone'])) {
			$labels['zone'] = array(
					'label' => Customweb_I18n_Translation::__('Country code of card'),
					'value' => $parameters['zone'],
			);
		}
	
		if (isset($parameters['ipzone'])) {
			$labels['ipzone'] = array(
					'label' => Customweb_I18n_Translation::__('IP zone customer'),
					'value' => $parameters['ipzone'],
			);
		}
		
		if (isset($parameters['ipzonea2'])) {
			$labels['ipzonea2'] = array(
					'label' => Customweb_I18n_Translation::__('Country based on customer IP'),
					'value' => $parameters['ipzonea2'],
			);
		}
		
		if (isset($parameters['ipstate'])) {
			$labels['ipstate'] = array(
					'label' => Customweb_I18n_Translation::__('State based on customer IP'),
					'value' => $parameters['ipstate'],
			);
		}
		
		if (isset($parameters['ipcity'])) {
			$labels['ipcity'] = array(
					'label' => Customweb_I18n_Translation::__('City based on customer IP'),
					'value' => $parameters['ipcity'],
			);
		}
		
		if (isset($parameters['fsstatus'])) {
			$labels['fsstatus'] = array(
					'label' => Customweb_I18n_Translation::__('Fraud status'),
					'value' => $parameters['fsstatus'],
			);
		}
		
		if (isset($parameters['fscode'])) {
			$labels['fscode'] = array(
					'label' => Customweb_I18n_Translation::__('Fraud status code'),
					'value' => $parameters['fscode'],
			);
		}
		
		if (isset($parameters['guwid'])) {
			$labels['guwid'] = array(
					'label' => Customweb_I18n_Translation::__('Acquire transaction id'),
					'value' => $parameters['guwid'],
			);
		}
		
		if (isset($parameters['name'])) {
			$labels['name'] = array(
					'label' => Customweb_I18n_Translation::__('Customer name'),
					'value' => $parameters['name'],
			);
		}
		
		if (isset($parameters['e-mail'])) {
			$labels['e-mail'] = array(
					'label' => Customweb_I18n_Translation::__('Account owner email'),
					'value' => $parameters['e-mail'],
			);
		}
		
		if (isset($parameters['billingaddrstreet'])) {
			$labels['billingaddrstreet'] = array(
					'label' => Customweb_I18n_Translation::__('Account owner street'),
					'value' => $parameters['billingaddrstreet'],
			);
		}
		
		if (isset($parameters['billingaddrcity'])) {
			$labels['billingaddrcity'] = array(
					'label' => Customweb_I18n_Translation::__('Account owner city'),
					'value' => $parameters['billingaddrcity'],
			);
		}
		
		if (isset($parameters['billingaddrstate'])) {
			$labels['billingaddrstate'] = array(
					'label' => Customweb_I18n_Translation::__('Account owner state'),
					'value' => $parameters['billingaddrstate'],
			);
		}
		
		if (isset($parameters['billingaddrzip'])) {
			$labels['billingaddrzip'] = array(
					'label' => Customweb_I18n_Translation::__('Account owner state'),
					'value' => $parameters['billingaddrzip'],
			);
		}
		
		if (isset($parameters['billingaddr-countrycode'])) {
			$labels['billingaddr-countrycode'] = array(
					'label' => Customweb_I18n_Translation::__('Account owner country code'),
					'value' => $parameters['billingaddr-countrycode'],
			);
		}
		
		if (isset($parameters['paymentpurpose'])) {
			$labels['paymentpurpose'] = array(
					'label' => Customweb_I18n_Translation::__('Purpose of payment'),
					'value' => $parameters['paymentpurpose'],
			);
		}
		
		if (isset($parameters['paymentguarantee'])) {
			$labels['paymentguarantee'] = array(
					'label' => Customweb_I18n_Translation::__('Payment guarantee'),
					'value' => $parameters['paymentguarantee'],
			);
		}
		
		if (isset($parameters['errortext'])) {
			$labels['errortext'] = array(
					'label' => Customweb_I18n_Translation::__('Error text'),
					'value' => $parameters['errortext'],
			);
		}
		
		if (isset($parameters['codeext'])) {
			$labels['codeext'] = array(
					'label' => Customweb_I18n_Translation::__('External error code'),
					'value' => $parameters['codeext'],
			);
		}
		
		if (isset($parameters['mandatename'])) {
			$labels['mandatename'] = array(
					'label' => Customweb_I18n_Translation::__('Mandate name'),
					'value' => $parameters['mandatename'],
			);
		}
		
		if (isset($parameters['avmatch'])) {
			$labels['avmatch'] = array(
					'label' => Customweb_I18n_Translation::__('AVMatch'),
					'value' => $parameters['avmatch'],
			);
		}
		
		if (isset($parameters['accbank'])) {
			$labels['accbank'] = array(
					'label' => Customweb_I18n_Translation::__('Financial institution account owner'),
					'value' => $parameters['accbank'],
			);
		}
		
		if (isset($parameters['accowner'])) {
			$labels['accowner'] = array(
					'label' => Customweb_I18n_Translation::__('Account owner name'),
					'value' => $parameters['accowner'],
			);
		}
		
		if (isset($parameters['iban'])) {
			$labels['iban'] = array(
					'label' => Customweb_I18n_Translation::__('IBAN'),
					'value' => $parameters['iban'],
			);
		}
		
		if (isset($parameters['bic'])) {
			$labels['bic'] = array(
					'label' => Customweb_I18n_Translation::__('BIC'),
					'value' => $parameters['bic'],
			);
		}
		
		if (isset($parameters['acciban'])) {
			// According to the documentation IBAN contains the financial instituion identifier
			$labels['acciban'] = array(
					'label' => Customweb_I18n_Translation::__('Financial institution'),
					'value' => $parameters['acciban'],
			);
		}
		
		if (isset($parameters['accnr'])) {
			$labels['accnr'] = array(
					'label' => Customweb_I18n_Translation::__('Bank account number'),
					'value' => $parameters['accnr'],
			);
		}
		
		if (isset($parameters['transactionid'])) {
			$labels['transactionid'] = array(
					'label' => Customweb_I18n_Translation::__('TransactionID'),
					'value' => $parameters['transactionid'],
			);
		}
		
		if (isset($parameters['referenzid'])) {
			$labels['referenzid'] = array(
					'label' => Customweb_I18n_Translation::__('ReferenzID'),
					'value' => $parameters['referenzid'],
			);
		}
		
		if ($this->datePaid !== null) {
			$labels['referenzid'] = array(
				'label' => Customweb_I18n_Translation::__('Paid Date'),
				'value' => $this->datePaid->format('c'),
			);
		}
		
		$invoiceNumber = $this->getInvoiceNumber();
		if (!empty($invoiceNumber)) {
			$labels['invoice_number'] = array(
				'label' => Customweb_I18n_Translation::__('Invoice Number'),
				'value' =>	$invoiceNumber,
			);
		}
		
		foreach ($this->getLabelProviders() as $provider) {
			foreach ($provider->getLabels() as $key => $value) {
				$labels[$key] = $value;
			}
		}
		
		return $labels;
	}
	
	public function setPaidDate(DateTime $date) {
		$this->datePaid = new Customweb_Core_DateTime($date);
		return $this;
	}
	
	/**
	 * @return Customweb_Core_DateTime
	 */
	public function getPaidDate() {
		return $this->datePaid;
	}
	
	
	public function setPseudoCardNumber($pcNr){
		$lastThree = substr($pcNr, -3);
		$hiddenPart = str_repeat("x", strlen($pcNr)-3);
		$this->setAliasForDisplay($hiddenPart . $lastThree);
		
		$this->pseudoCardNumber = $pcNr;
	}
	
	public function getPseudoCardNumber(){
		return $this->pseudoCardNumber;
	}
	
	public function setCardExpiry($expiry){
		$this->cardExpiry = $expiry;
	}
	public function getCardExpiry(){
		return $this->cardExpiry;
	}

	public function getCardExpiryMonth() {
		return substr($this->getCardExpiry(), 4);
	}

	public function getCardExpiryYear() {
		return substr($this->getCardExpiry(), 0, 4);
	}
	
	public function setCardBrand($brand){
		$this->cardBrand = $brand;
	}
	
	public function getCardBrand(){
		return $this->cardBrand;
	}
	
	public function setRedirectUrl($redirectUrl){
		$this->redirectUrl = $redirectUrl;
	}
	public function getRedirectUrl(){
		return $this->redirectUrl;
	}
	public function isRedirectNeeded(){
		return strlen($this->getRedirectUrl()) > 3;
	}
	
	public function isCalledByServer(){
		return $this->calledByServer;
	}
	
	public function setCalledByServer($flag = true){
		$this->calledByServer = $flag;
	}
	
	
	public function getFailedUrl() {
		return Customweb_Util_Url::appendParameters(
				$this->getTransactionContext()->getFailedUrl(),
				$this->getTransactionContext()->getCustomParameters()
		);
	}
	
	public function getSuccessUrl() {
		return Customweb_Util_Url::appendParameters(
				$this->getTransactionContext()->getSuccessUrl(),
				$this->getTransactionContext()->getCustomParameters()
		);
	}
	
	public function isAuthorisationRequestSent(){
		return $this->authorisationRequestSent;
	}
	
	public function setAuthorisationRequestSent(){
		return $this->authorisationRequestSent = true;
	}
	
	public function setOutputToShow($content){
		$this->outputToShow = $content;
	}
	
	public function getOutputToShow(){
		return $this->outputToShow;
	}
	public function isOutputToShow(){
		return strlen($this->getOutputToShow()) > 0;
	}
	
	public function getDebitorPaymentId(){
		return $this->debitorPayId;
	}
	public function setDebitorPaymentId($id){
		$this->debitorPayId = $id;
	}
	
	public function setExternalTransId($transId){
		$this->externalTransId = $transId;
	}
	
	public function getExternalTransId(){
		return $this->externalTransId;
	}
	
	protected function getCustomOrderStatusSettingKey($statusKey) {
		if ($this->overridenOrderStatus !== null) {
			return $this->overridenOrderStatus;
		}
		
		return $statusKey;
	}
	
	public function overrideOrderStatusSettingKey($orderStatus) {
		$this->overridenOrderStatus = $orderStatus;
	}

	public function getIframeFormData(){
		return $this->iframeFormData;
	}

	public function setIframeFormData($iframeFormData){
		$this->iframeFormData = $iframeFormData;
		return $this;
	}

	protected function buildNewCaptureObject($captureId, $amount, $status = NULL) {
		return new Customweb_Computop_Authorization_TransactionCapture($captureId, $amount, $status);
	}
	
	/**
	 * @return Customweb_Computop_Authorization_TransactionCapture
	 */
	public function partialCaptureByLineItems($items, $close = false, $additionalMessage = '') {
		return parent::partialCaptureByLineItems($items, $close, $additionalMessage);
	}
	
	/**
	 * @return Customweb_Computop_Authorization_TransactionCapture[]
	 */
	public function getCaptures() {
		return parent::getCaptures();
	}
	
	protected function buildNewCancelObject($cancelId, $status = NULL) {
		return new Customweb_Computop_Authorization_TransactionCancel($cancelId, $status);
	}
		
	/**
	 * @return Customweb_Computop_Authorization_TransactionCancel
	 */
	public function cancel($additionalMessage = '') {
		return parent::cancel($additionalMessage);
	}
	
	protected function buildNewRefundObject($refundId, $amount, $status = NULL) {
		return new Customweb_Computop_Authorization_TransactionRefund($refundId, $amount, $status);
	}
	
	/**
	 * @return Customweb_Computop_Authorization_TransactionRefund
	 */
	public function refundByLineItems($items, $close = false, $additionalMessage = '') {
		return parent::refundByLineItems($items, $close, $additionalMessage);
	}

	public function isMultiCaptureEnabled(){
		return $this->multiCaptureEnabled;
	}

	public function setMultiCaptureEnabled($multiCaptureEnabled){
		$this->multiCaptureEnabled = $multiCaptureEnabled;
		return $this;
	}
	
	public function isCaptureClosable() {
		return $this->isMultiCaptureEnabled();
	}

	public function getAdapterClassName(){
		return $this->adapterClassName;
	}

	public function setAdapterClassName($adapterClassName){
		$this->adapterClassName = $adapterClassName;
		return $this;
	}
	
	public function getPaymentInformation() {
		return $this->paymentInformation;
	}
	
	public function setPaymentInformation($paymentInformation) {
		$this->paymentInformation = $paymentInformation;
		return $this;
	}
	
	public function executeTransactionUpdates(Customweb_DependencyInjection_IContainer $container) {
		$tasks = $this->updateTasks;
		foreach ($tasks as $key => $task) {
			try {
				$task->execute($this, $container);
				if ($task->getExecutionTime() == null) {
					unset($this->updateTasks[$key]);
				}
				else {
					$this->updateNextUpdateExecutionDate($task->getExecutionTime());
				}
			}
			catch(Exception $e) {
				$this->addErrorMessage($e->getMessage());
			}
		}
	}
	
	public function addUpdateTask(Customweb_Computop_Authorization_ITransactionUpdateTask $task) {
		$this->updateTasks[] = $task;
		$this->updateNextUpdateExecutionDate($task->getExecutionTime());
		
	}
	
	private function updateNextUpdateExecutionDate(DateTime $time) {
		$current = $this->getUpdateExecutionDate();
		if ($current !== null) {
			$current = new Customweb_Core_DateTime($current);
			$time = new Customweb_Core_DateTime($time);
			if ($time->getTimestamp() < $current->getTimestamp()) {
				$this->setUpdateExecutionDate($time);
			}
		}
		else {
			$this->setUpdateExecutionDate($time);
		}
	}

	public function getInvoiceNumber(){
		return $this->invoiceNumber;
	}

	public function setInvoiceNumber($invoiceNumber){
		$this->invoiceNumber = $invoiceNumber;
		return $this;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see Customweb_Payment_Authorization_DefaultTransaction::isRefundPossible()
	 */
	public function isPartialRefundPossible(){
		$isPossible = parent::isPartialRefundPossible();
		if($this->isPaymorrowRefund()){
// 			$isPossible = false;
		}
		return $isPossible;
	}
	
	private function isPaymorrowRefund(){
		$pm = $this->getPaymentMethod();
		$pmName = $pm->getPaymentMethodName();
		if(strcasecmp($pmName, 'directdebits') == 0 || strcasecmp($pmName, 'openinvoice') == 0){
			$proc = $pm->getPaymentMethodConfigurationValue('processor');
			if(strcasecmp($proc, 'paymorrow') == 0){
				return true;
			}
		}
		return false;
	}
	
}