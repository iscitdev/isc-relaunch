<?php

/**
 *  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2015 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */

//require_once 'Customweb/Computop/Debitor/Prepayment/FileManager.php';
//require_once 'Customweb/Form/Button.php';
//require_once 'Customweb/Form/ElementGroup.php';
//require_once 'Customweb/Computop/Debitor/Prepayment/Processor.php';
//require_once 'Customweb/Form/Control/Html.php';
//require_once 'Customweb/Form/WideElement.php';
//require_once 'Customweb/I18n/Translation.php';
//require_once 'Customweb/Payment/BackendOperation/Form/Abstract.php';
//require_once 'Customweb/Form/IButton.php';



/**
 * @BackendForm(0)
 */
final class Customweb_Computop_BackendOperation_Form_ImportPrepaymentFile extends Customweb_Payment_BackendOperation_Form_Abstract {
	private $importButton;

	public function getTitle(){
		return Customweb_I18n_Translation::__("Import IDEAL File");
	}

	public function isProcessable(){
		return true;
	}

	public function getElementGroups(){
		return array(
			$this->getElementGroup() 
		);
	}

	private function getElementGroup(){
		$group = new Customweb_Form_ElementGroup();
		$group->addElement($this->getDescriptionElement());
		return $group;
	}

	private function getDescriptionElement(){
		$description = Customweb_I18n_Translation::__(
				"To mark transaction paid with pre-payment a file from IDEAL must be imported. This file is on a SFTP server.
				 By pressing the import button this action is executed manually. By default the import process is triggered over the cron. 
				Please see the manual for more details how to setup a cron.");
		
		$control = new Customweb_Form_Control_Html('description', $description);
		$element = new Customweb_Form_WideElement($control);
		return $element;
	}

	public function getButtons(){
		return array(
			$this->getImportButton() 
		);
	}

	private function getImportButton(){
		if ($this->importButton === null) {
			$this->importButton = new Customweb_Form_Button();
			$this->importButton->setMachineName("import")->setTitle(Customweb_I18n_Translation::__("Import"))->setType(
					Customweb_Form_IButton::TYPE_SUCCESS);
		}
		return $this->importButton;
	}

	public function process(Customweb_Form_IButton $pressedButton, array $formData){
		if ($pressedButton->getMachineName() === 'import') {
			$fileManager = new Customweb_Computop_Debitor_Prepayment_FileManager($this->getContainer());
			$processor = new Customweb_Computop_Debitor_Prepayment_Processor($this->getContainer());
			while (($nextFile = $fileManager->getNextFile()) !== null) {
				$processor->process($nextFile);
				$fileManager->markFileAsProcessed($nextFile);
			}
		}
	}
}