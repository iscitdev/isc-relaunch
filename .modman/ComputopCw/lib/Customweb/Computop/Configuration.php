<?php
/**
  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2015 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */

//require_once 'Customweb/I18n/Translation.php';


/**
 * 
 * @author Thomas Hunziker
 * @Bean
 *
 */
class Customweb_Computop_Configuration {
	
	/**
	 *           	   		  		 
	 * @var Customweb_Payment_IConfigurationAdapter
	 */
	private $configurationAdapter = null;
	
	
	public function __construct(Customweb_Payment_IConfigurationAdapter $configurationAdapter) {
		$this->configurationAdapter = $configurationAdapter;
	}
	
	public function getMerchantId(){
		if ($this->isTestMode()) {
			$merchantId = $this->getConfigurationValue('merchant_id_test');
		}
		else {
			$merchantId = $this->getConfigurationValue('merchant_id_live');
		}
		
		$merchantId = trim($merchantId);
		
		if (empty($merchantId)) {
			throw new Exception(Customweb_I18n_Translation::__("The merchant ID is empty.") . ' ' . Customweb_I18n_Translation::__("Please check your configuration."));
		}
		
		return $merchantId;
	}
	
	public function getEncryptionKey(){
		if ($this->isTestMode()) {
			$encryptionKey = $this->getConfigurationValue('encryption_key_test');
		}
		else {
			$encryptionKey = $this->getConfigurationValue('encryption_key_live');
		}
		$encryptionKey = trim($encryptionKey);
		if (empty($encryptionKey)) {
			throw new Exception(Customweb_I18n_Translation::__("The encryption key is empty.") . ' ' . Customweb_I18n_Translation::__("Please check your configuration."));
		}
		
		return $encryptionKey;
	}
	
	public function getSignatureKey(){
		if ($this->isTestMode()) {
			$signatureKey = $this->getConfigurationValue('signature_key_test');
		}
		else {
			$signatureKey = $this->getConfigurationValue('signature_key_live');
		}
		
		$signatureKey = trim($signatureKey);
		if (empty($signatureKey)) {
			throw new Exception(Customweb_I18n_Translation::__("The signature key is empty.") . ' ' . Customweb_I18n_Translation::__("Please check your configuration."));
		}
		
		return $signatureKey;
	}
	
	public function getBaseUrl() {
		return 'https://www.computop-paygate.com/';
	}
	
	public function getTransactionIdSchema() {
		return $this->getConfigurationValue('transaction_id_schema');
	}
	
	public function getShopDescription($languageCode = null){
		$rs = $this->getConfigurationValue('shop_description', $languageCode);
		if (empty($rs)) {
			throw new Exception(Customweb_I18n_Translation::__("The description field is empty.") . ' ' . Customweb_I18n_Translation::__("Please check your configuration."));
		}
		
		return $rs;
	}

	/**
	 * This method returns the configured income (Erlösecode) code for products with tax code 101 (19%).
	 *
	 * @return string
	 */
	public function getDebitorManagementIncomeCode4TaxCode101() {
		$code = $this->getConfigurationValue('dm_income_code_4_tax_code_101');
		if (empty($code)) {
			throw new Exception("You have activate the debitor management, hence you need to define the income codes. The income code for tax code 101 is empty.");
		}
	
		return $code;
	}

	/**
	 * This method returns the configured income (Erlösecode) code for products with tax code 002 (7%).
	 *
	 * @return string
	 */
	public function getDebitorManagementIncomeCode4TaxCode002() {
		$code = $this->getConfigurationValue('dm_income_code_4_tax_code_002');
		if (empty($code)) {
			throw new Exception("You have activate the debitor management, hence you need to define the income codes. The income code for tax code 002 is empty.");
		}
	
		return $code;
	}

	/**
	 * This method returns the configured income (Erlösecode) code for products with tax code 000 (0%).
	 *
	 * @return string
	 */
	public function getDebitorManagementIncomeCode4TaxCode000() {
		$code = $this->getConfigurationValue('dm_income_code_4_tax_code_000');
		if (empty($code)) {
			throw new Exception("You have activate the debitor management, hence you need to define the income codes. The income code for tax code 000 is empty.");
		}
	
		return $code;
	}
	
	public function getPrepaymentSftpUrl() {
		return $this->getConfigurationValue('prepayment_sftp_url');
	}
	
	public function isPrepaymentFileImportActive() {
		$url = $this->getPrepaymentSftpUrl();
		if (empty($url)) {
			return false;
		}
		else {
			return true;
		}
	}

	public function getPrepaymentSftpUsername() {
		$username = $this->getConfigurationValue('prepayment_sftp_username');
		if (empty($username)) {
			throw new Exception("SFTP username is not set. Please set the SFTP username in the settings.");
		}
	
		return $username;
	}
	
	public function getPrepaymentSftpPassword() {
		$this->validateSftpPrivateKeyPassword();
		return $this->getConfigurationValue('prepayment_sftp_password');
	}
	
	public function getPrepaymentSftpPrivateKey() {
		$this->validateSftpPrivateKeyPassword();
		return $this->getConfigurationValue('prepayment_sftp_private_key');
	}
	
	private function validateSftpPrivateKeyPassword() {
		$privateKey = $this->getConfigurationValue('prepayment_sftp_private_key');
		$password = $this->getConfigurationValue('prepayment_sftp_password');
		if (empty($privateKey) && empty($password)) {
			throw new Exception("Either the password or the private key must be set.");
		}
	}
	
	
	public function getShopId(){
		$shopId = $this->getConfigurationValue('shop_id');
		if (empty($shopId)) {
			throw new Exception("No shop id is set. Please set a shop id.");
		}
		else if (strlen($shopId) > 2) {
			throw new Exception("The given shop id is too long. It has more than 2 chars.");
		}
		return $shopId;
	}
	
	public function isTestMode(){
		return $this->getConfigurationValue('operation_mode') != 'live';
	}
	
	public function getInquiryUrl(){
		return $this->getBaseUrl() . "inquire.aspx";
	}
	
	public function getCaptureUrl(){
		return $this->getBaseUrl() . "capture.aspx";
	}
	
	public function getRefundUrl(){
		return $this->getBaseUrl() . "credit.aspx";
	}
	
	public function getCancellationUrl(){
		return $this->getBaseUrl() . "reverse.aspx";
	}
	
	public function getDebitorManagementUrl(){
		return $this->getBaseUrl() . "debtor.aspx";
	}
	
	public function getDirectUrl(){
		return $this->getBaseUrl() . "direct.aspx";
	}
	
	public function getMasterPassRedirectionUrl() {
		return $this->getBaseUrl() . "MasterPass.aspx";
	}
	
	public function getMasterPassAuthorizationUrl() {
		return $this->getBaseUrl() . 'directmp.aspx';
	}
	
	public function getConfigurationAdapter(){
		return $this->configurationAdapter;
	}
	
	
	
	protected function getConfigurationValue($key, $language = null) {
		return $this->configurationAdapter->getConfigurationValue($key, $language);
	}
}