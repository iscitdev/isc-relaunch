<?php

/**
 *  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2015 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */

//require_once 'Customweb/Computop/Debitor/ParameterBuilder/CreateOpenPosition.php';
//require_once 'Customweb/Computop/Debitor/ParameterBuilder/CreatePrepaymentPosition.php';
//require_once 'Customweb/Computop/Debitor/ParameterBuilder/CancelOpenPosition.php';
//require_once 'Customweb/Computop/Debitor/Request.php';
//require_once 'Customweb/Computop/Debitor/ParameterBuilder/Manage.php';
//require_once 'Customweb/Util/Currency.php';
//require_once 'Customweb/Util/Invoice.php';
//require_once 'Customweb/Computop/AbstractAdapter.php';
//require_once 'Customweb/I18n/Translation.php';
//require_once 'Customweb/Payment/Authorization/DefaultTransactionHistoryItem.php';



/**
 * Adapter implementation for IDEAL debitor management.
 *
 * @Bean
 */
final class Customweb_Computop_Debitor_Adapter extends Customweb_Computop_AbstractAdapter {

	public function createDebitor(Customweb_Computop_Authorization_Transaction $transaction){
		try {
			$builder = new Customweb_Computop_Debitor_ParameterBuilder_Manage($transaction, $this->getContainer());
			$request = new Customweb_Computop_Debitor_Request($this->getConfiguration(), $builder);
			$response = $request->process();
			$this->updateTransaction($transaction, $response);
			$transaction->addHistoryItem(
					new Customweb_Payment_Authorization_DefaultTransactionHistoryItem(Customweb_I18n_Translation::__('Debitor created / updated'),
							'debitor-creation'));
		}
		catch (Exception $e) {
			$transaction->addHistoryItem(
					new Customweb_Payment_Authorization_DefaultTransactionHistoryItem(
							Customweb_I18n_Translation::__('Debitor creation failed with @message',
									array(
										'@message' => $e->getMessage()
									)), 'debitor-creation'));
		}
	}

	public function createPrepaymentPosition(Customweb_Computop_Authorization_Transaction $transaction){
		try {
			$builder = new Customweb_Computop_Debitor_ParameterBuilder_CreatePrepaymentPosition($transaction, $this->getContainer());
			$request = new Customweb_Computop_Debitor_Request($this->getConfiguration(), $builder);
			$response = $request->process();
			$transaction->addHistoryItem(
					new Customweb_Payment_Authorization_DefaultTransactionHistoryItem(Customweb_I18n_Translation::__('Open prepayment position'),
							'debitor-prepayment-position-creation'));
		}
		catch (Exception $e) {
			$transaction->addHistoryItem(
					new Customweb_Payment_Authorization_DefaultTransactionHistoryItem(
							Customweb_I18n_Translation::__('Creation of prepayment position failed with @message',
									array(
										'@message' => $e->getMessage()
									)), 'debitor-prepayment-position-creation'));
		}
	}
	
	public function createOpenPosition(Customweb_Computop_Authorization_Transaction $transaction){
		try {
			$builder = new Customweb_Computop_Debitor_ParameterBuilder_CreateOpenPosition($transaction, $this->getContainer());
			$request = new Customweb_Computop_Debitor_Request($this->getConfiguration(), $builder);
			$response = $request->process();
			$this->updateTransaction($transaction, $response);
			$transaction->addHistoryItem(
					new Customweb_Payment_Authorization_DefaultTransactionHistoryItem(Customweb_I18n_Translation::__('Debitor position opened.'), 
							'debitor-position-creation'));
		}
		catch (Exception $e) {
			$transaction->addHistoryItem(
					new Customweb_Payment_Authorization_DefaultTransactionHistoryItem(
							Customweb_I18n_Translation::__('Debitor position creation failed with @message', 
									array(
										'@message' => $e->getMessage() 
									)), 'debitor-position-creation'));
			$transaction->addErrorMessages($e->getMessage());
		}
	}

	public function cancelOpenPosition(Customweb_Computop_Authorization_Transaction $transaction, $items){
		try {
			$builder = new Customweb_Computop_Debitor_ParameterBuilder_CancelOpenPosition($transaction, $this->getContainer(), $items);
			$request = new Customweb_Computop_Debitor_Request($this->getConfiguration(), $builder);
			$response = $request->process();
			$amount = Customweb_Util_Invoice::getTotalAmountIncludingTax($items);
			$transaction->addHistoryItem(
					new Customweb_Payment_Authorization_DefaultTransactionHistoryItem(
							Customweb_I18n_Translation::__('Debitor position cancelled over !amount.', 
									array(
										'!amount' => Customweb_Util_Currency::formatAmount($amount, $transaction->getCurrencyCode()) 
									)), 'debitor-position-cancellation'));
		}
		catch (Exception $e) {
			$transaction->addHistoryItem(
					new Customweb_Payment_Authorization_DefaultTransactionHistoryItem(
							Customweb_I18n_Translation::__('Debitor position cancellation failed with @message', 
									array(
										'@message' => $e->getMessage() 
									)), 'debitor-position-cancellation'));
			$transaction->addErrorMessages($e->getMessage());
		}
	}

	private function updateTransaction(Customweb_Computop_Authorization_Transaction $transaction, array $response){
		$transaction->setDebitorPaymentId($response['PayID']);
	}
}