<?php

/**
 *  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2015 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */

//require_once 'Customweb/Computop/Debitor/RefundUpdateTask.php';
//require_once 'Customweb/I18n/Translation.php';
//require_once 'Customweb/Payment/Authorization/DefaultTransactionHistoryItem.php';



/**
 *
 * @author Thomas Hunziker
 * @Bean
 *
 */
class Customweb_Computop_Debitor_Manager {
	private $adapter = null;

	public function __construct(Customweb_Computop_Debitor_Adapter $adapter){
		$this->adapter = $adapter;
	}

	/**
	 * This method is invoked, when the transaction is authorized.
	 *
	 * @param Customweb_Computop_Authorization_Transaction $transaction
	 */
	public function processAuthorize(Customweb_Computop_Authorization_Transaction $transaction){
		if ($this->isPaymentMethodRequiresDebitorManagement($transaction->getPaymentMethod(), 'authorization')) {
			$this->getAdapter()->createDebitor($transaction);
			
			if (strtolower($transaction->getPaymentMethod()->getPaymentMethodName()) == 'prepayment') {
				$this->getAdapter()->createPrepaymentPosition($transaction);
				
				// Add payment information (TODO: find a better way to add it)
				$bankAccount = $transaction->getPaymentMethod()->getPaymentMethodConfigurationValue('bank_account');
				$bankAccount = str_replace("\n", "<br />", $bankAccount);
				$rs = array();
				$transactionId = $transaction->getExternalTransactionId();
				preg_match("/^([0-9]+)/", $transactionId, $rs);
				$basketId = $rs[1];
				$text = Customweb_I18n_Translation::__(
						"Please set the transaction number '@basketId' in the subject of the payment. Otherwise could not identify the payment.<br />!bankAccount", 
						array('@basketId' => $basketId, '!bankAccount' => $bankAccount));
				$transaction->setPaymentInformation($text);
			}
		}
	}

	/**
	 * This method is invoked, when the transaction captured.
	 *
	 * @param Customweb_Computop_Authorization_Transaction $transaction
	 */
	public function processCapture(Customweb_Computop_Authorization_Transaction $transaction){
		if ($this->isPaymentMethodRequiresDebitorManagement($transaction->getPaymentMethod(), 'capturing')) {
			// When no more captures are possible we trigger the creation of the open position.
			// prepayment does not require a open position on capture
			if (!$transaction->isCapturePossible() && strtolower($transaction->getPaymentMethod()->getPaymentMethodName()) != 'prepayment') {
				$this->getAdapter()->createOpenPosition($transaction);
			}
		}
	}

	public function processCancel(Customweb_Computop_Authorization_Transaction $transaction){
		if ($this->isPaymentMethodRequiresDebitorManagement($transaction->getPaymentMethod(), 'cancellation')) {
			if (strtolower($transaction->getPaymentMethod()->getPaymentMethodName()) == 'prepayment') {
				$this->getAdapter()->cancelOpenPosition($transaction, $transaction->getTransactionContext()->getOrderContext()->getInvoiceItems());
			}
		}
	}

	public function processRefund(Customweb_Computop_Authorization_Transaction $transaction, $items){
		if ($this->isPaymentMethodRequiresDebitorManagement($transaction->getPaymentMethod(), 'refunding')) {
			$this->getAdapter()->cancelOpenPosition($transaction, $items);
			
// 			$task = new Customweb_Computop_Debitor_RefundUpdateTask($items);
// 			$transaction->addUpdateTask($task);
// 			$transaction->addHistoryItem(
// 					new Customweb_Payment_Authorization_DefaultTransactionHistoryItem(
// 							Customweb_I18n_Translation::__('Debitor position cancellation is scheduled for !time.', 
// 									array(
// 										'!time' => $task->getExecutionTime()->format('c'),
// 									)), 'debitor-position-cancellation'));
		}
	}

	private function isPaymentMethodRequiresDebitorManagement(Customweb_Payment_Authorization_IPaymentMethod $paymentMethod, $operation){
		if ($paymentMethod->existsPaymentMethodConfigurationValue('debitor_management')) {
			$rs = $paymentMethod->getPaymentMethodConfigurationValue("debitor_management");
			if ($rs == 'enabled') {
				return true;
			}
			
			if (is_array($rs) && in_array($operation, $rs)) {
				return true;
			}
		}
		
		return false;
	}

	/**
	 *
	 * @return Customweb_Computop_Debitor_Adapter
	 */
	private function getAdapter(){
		return $this->adapter;
	}
}