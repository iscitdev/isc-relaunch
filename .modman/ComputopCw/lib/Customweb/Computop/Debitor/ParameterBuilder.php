<?php 
/**
  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2013 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.customweb.ch/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.customweb.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */

//require_once 'Customweb/Util/Address.php';
//require_once 'Customweb/Payment/Util.php';
//require_once 'Customweb/Util/Invoice.php';
//require_once 'Customweb/Computop/AbstractParameterBuilder.php';


abstract class Customweb_Computop_Debitor_ParameterBuilder extends Customweb_Computop_AbstractParameterBuilder {
	
	public function build(){
		$parameters =  array_merge(
				$this->getBasicParameters(),
				$this->getCustomerMasterDataParameters()
		);
		$parameters['Currency'] = $this->getOrderContext()->getCurrencyCode();
		$parameters['EventToken'] = 'MD';
		
		$parameters = $this->getSecurityParameters($parameters);
		$parameters = $this->encryptParameters($parameters);
		
		return $parameters;
	}
	
	public function buildCreateOpenPositionParameters(Customweb_Payment_Authorization_IPaymentMethod $paymentMethod){
		$orderContext = $this->getOrderContext();
		$parameters =  $this->getBasicParameters();
		
		$parameters['RefNr'] = Customweb_Payment_Util::applyOrderSchema(
				$this->getConfiguration()->getTransactionIdSchema(),
				$this->getTransactionContext()->getTransactionId(),
				11
		);
		
		$now = new DateTime();
		$parameters['AmountClass'] = "";
		$parameters['EventToken'] = 'OC';
		$parameters['PayType'] = $this->getPayType($paymentMethod);
		$parameters['CustomerID'] = $orderContext->getCustomerId();
		
		if($parameters['PayType'] != 'PRP'){
			$parameters['DebitorID'] = $orderContext->getCustomerId();
			
			$parameters['ShopID'] = $this->getConfiguration()->getShopId();
			$parameters['InvoiceDate'] = $now->format('dmy');
			$parameters['NetAmount'] = Customweb_Util_Invoice::getTotalAmountExcludingTax($this->getOrderContext()->getInvoiceItems());
			$parameters = array_merge($parameters,$this->getTaxAmounts());
		}
		else{
			$parameters['ShoppingBasketID'] = substr($this->getTransaction()->getTransactionId(),-3);
			$parameters['CreationDate'] = $now->format('Ymd');
			$parameters['DueDate'] = $this->getDueDate();
		}
		
		$parameters = $this->getSecurityParameters($parameters);
		$parameters = $this->encryptParameters($parameters);
		
		return $parameters;
	}
	
	public function buildCancelOpenPositionParameters(){
		$parameters =  array_merge(
				$this->getBasicParameters(),
				$this->getAmountParameters()
		);
		$parameters['PayID'] = $this->getTransaction()->getDebitorPaymentId();
		$parameters['EventToken'] = 'OS';
		
		$parameters = $this->getSecurityParameters($parameters);
		$parameters = $this->encryptParameters($parameters);
		
		return $parameters;
	}
	
	private function getCustomerMasterDataParameters(){
		$parameters = array();
		$orderContext = $this->getOrderContext();
		
		$parameters['FirstName'] = $orderContext->getBillingFirstName();
		$parameters['LastName'] = $orderContext->getBillingLastName();
		$address = Customweb_Util_Address::splitStreet($orderContext->getBillingStreet(), $orderContext->getBillingCountryIsoCode(), $orderContext->getBillingPostCode());
		$parameters['AddrStreet'] = $address['street'];
		$parameters['AddrStreetNr'] = $address['street-number'];
		
		$parameters['AddrZip'] = $orderContext->getBillingPostCode();
		$parameters['AddrCity'] = $orderContext->getBillingCity();
		$parameters['AddrCountryCode'] = $orderContext->getBillingCountryIsoCode();
		$parameters['TaxId'] = '1';
		$parameters['DebitorID'] = $orderContext->getCustomerId();
		$parameters['CustomerID'] = $orderContext->getCustomerId();
		
		return $parameters;
	}
	
	private function getTaxAmounts(){
		$parameters = array();
		$taxBins = array('001' => array(), '002' => array(), '000' => array() );
		$netAmountBins = array('001' => array(), '002' => array(), '000' => array() );
		
		$items = $this->getOrderContext()->getInvoiceItems();
		foreach($items as $item){
			$taxCode = $this->taxRateToCode(floor($item->getTaxRate()));
			$taxBins[$taxCode][] = $item->getTaxAmount();
			$netAmountBins[$taxCode][] = $item->getAmountExcludingTax();
		}
		$taxIndex = 1;
		foreach(array_keys($taxBins) as $key){
			$parameters['TaxCode' . $taxIndex] = $key;
			$parameters['TaxAmount' . $taxIndex] = array_sum($taxBins[$key]);
			$parameters['NetAmount' . $taxIndex] = array_sum($netAmountBins[$key]);
				
			$taxIndex++;
		}
		return $parameters;
	}
	
	private function taxRateToCode($rate){
		// From the documentation it is not clear if this mapping might be changed by the merchant. For now its just
		// fixed here but could easily be made configurable.
		$rateToCode = array(19 => '001' , 7 => '002', 0 => '000');
		
		if(isset($rateToCode[$rate])){
			return $rateToCode[$rate];
		}
		return '000';
	}
	
	private function getPayType(Customweb_Payment_Authorization_IPaymentMethod $paymentMethod){
	$paymentMethodName = $paymentMethod->getPaymentMethodName();
		switch($paymentMethodName){	
			case "creditcard" :
				return 'CC';
			case "paypal" :
				return 'PP';
			case "postpay" :
				return 'COD';
			case "directdebits" :
			case "pprodirectdebits" :
			case "debitukdirectdebits" :
			case "intercarddirectdebits" :
			case "instalmentdirectdebits" :
				return 'EDD';
			
			case "giropay" :
			case "ppro-giropay" :
				return 'GP';
			case "directebanking" :
			case "pprodirectebanking" :
				return 'SO';
			
			case "billpayopeninvoice" :
				return 'BP';
				
			case "prepayment" :
				return 'PRP';
			case "billsafeopeninvoice" :
			case 'klarnaopeninvoice':
			case "instalmentinvoice" :
				return 'RG';
			default:
				return 'CC';
		}
	}
	
	private function getDueDate(){
		$now = new DateTime();
		$days = '';
		if($this->getPaymentMethod()->existsPaymentMethodConfigurationValue('payment_period')){
			$days = $this->getPaymentMethod()->getPaymentMethodConfigurationValue('payment_period');
		}
		
		if(!is_int($days)){
			$days = '30';
		}
		$i = new DateInterval('P' . $days . 'D');
		$now->add($i);
		return $now->format('Ymd');
	}
}