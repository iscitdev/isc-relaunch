<?php 
/**
  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2015 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */

//require_once 'Customweb/Util/Address.php';
//require_once 'Customweb/Core/String.php';
//require_once 'Customweb/Util/String.php';
//require_once 'Customweb/Computop/Util.php';
//require_once 'Customweb/Util/Currency.php';
//require_once 'Customweb/Payment/Authorization/IInvoiceItem.php';
//require_once 'Customweb/Computop/AbstractParameterBuilder.php';


abstract class Customweb_Computop_Debitor_ParameterBuilder_Abstract extends Customweb_Computop_AbstractParameterBuilder {
	
	protected final function getCustomerMasterDataParameters(){
		$parameters = $this->getCustomerIdParameters();
		$orderContext = $this->getOrderContext();
		
		$parameters['FirstName'] = $orderContext->getBillingFirstName();
		$parameters['LastName'] = $orderContext->getBillingLastName();
		$address = Customweb_Util_Address::splitStreet($orderContext->getBillingStreet(), $orderContext->getBillingCountryIsoCode(), $orderContext->getBillingPostCode());
		$parameters['AddrStreet'] = Customweb_Util_String::substrUtf8($address['street'], 0, 30);
		if (empty($parameters['AddrStreetNr'])) {
			$parameters['AddrStreetNr'] = '';
		}
		$parameters['AddrStreetNr'] = Customweb_Util_String::substrUtf8($address['street-number'], 0, 8);
		
		$parameters['AddrZip'] = $orderContext->getBillingPostCode();
		$parameters['AddrCity'] = $orderContext->getBillingCity();
		$parameters['AddrCountryCode'] = $orderContext->getBillingCountryIsoCode();
		$parameters['TaxId'] = '1';
		
		return $parameters;
	}
	
	protected final function getCustomerIdParameters() {
		$parameters = array();
		$customerId = substr(preg_replace('/[^0-9]+/', '', $this->getCustomerId()), 0 ,10);
		$parameters['DebitorID'] = $customerId;
		$parameters['CustomerID'] = $customerId;
		return $parameters;
	}
	
	protected final function getCustomerId() {
		$customerId = $this->getOrderContext()->getCustomerId();
		if (empty($customerId)) {
			$customerId = $this->getTransaction()->getExternalTransactionId();
		}
		return $customerId;
	}
	
	protected final function getTaxAmounts($items){
		$parameters = array();
		$taxBins = array('101' => array(), '002' => array(), '000' => array() );
		$netAmountBins = array('101' => array(), '002' => array(), '000' => array() );
		
		foreach($items as $item){
			$taxCode = $this->taxRateToCode($item->getTaxRate());
			if ($item->getType() == Customweb_Payment_Authorization_IInvoiceItem::TYPE_DISCOUNT) {
				$taxBins[$taxCode][] = -1 * $item->getTaxAmount();
				$netAmountBins[$taxCode][] = -1 * $item->getAmountExcludingTax();
			}
			else {
				$taxBins[$taxCode][] = $item->getTaxAmount();
				$netAmountBins[$taxCode][] = $item->getAmountExcludingTax();
			}
		}
		$taxIndex = 1;
		foreach(array_keys($taxBins) as $key){
			$parameters['TaxCode' . $taxIndex] = $key;
			$parameters['TaxAmount' . $taxIndex] = Customweb_Computop_Util::formatAmount(array_sum($taxBins[$key]), $this->getTransaction()->getCurrencyCode());
			$parameters['NetAmount' . $taxIndex] = Customweb_Computop_Util::formatAmount(array_sum($netAmountBins[$key]), $this->getTransaction()->getCurrencyCode());
				
			$taxIndex++;
		}
		return $parameters;
	}
	
	protected final function getAmountClass($items) {
		$incomeCodes = array(
			'000' => $this->getConfiguration()->getDebitorManagementIncomeCode4TaxCode000(),
			'101' => $this->getConfiguration()->getDebitorManagementIncomeCode4TaxCode101(),
			'002' => $this->getConfiguration()->getDebitorManagementIncomeCode4TaxCode002(),
		);
		
		$incomeGroups = array();
		foreach ($items as $item) {
			if ($item instanceof Customweb_Payment_Authorization_IInvoiceItem) {
				$taxCode = $this->taxRateToCode($item->getTaxRate());
				
				if (!isset($incomeGroups[$taxCode])) {
					$incomeGroups[$taxCode] = 0;
				}
				
				if ($item->getType() == Customweb_Payment_Authorization_IInvoiceItem::TYPE_DISCOUNT) {
					$incomeGroups[$taxCode] -= $item->getAmountExcludingTax();
				}
				else {
					$incomeGroups[$taxCode] += $item->getAmountExcludingTax();
				}
			}
		}
		
		$lines = array();
		foreach ($incomeGroups as $taxCode => $groupAmount) {
			if (!isset($incomeCodes[$taxCode])) {
				throw new Exception(Customweb_Core_String::_("No income code found for tax code '!taxCode'.")->format(array('!taxCode' => $taxCode)));
			}
			$compareResult = Customweb_Util_Currency::compareAmount(0, $groupAmount, $this->getOrderContext()->getCurrencyCode());
			if ($compareResult < 0) {
				$lines[] = $incomeCodes[$taxCode] . ';' . Customweb_Util_Currency::formatAmount($groupAmount, $this->getOrderContext()->getCurrencyCode(), '', '') . ';' . $taxCode;
			}
			else if ($compareResult > 0) {
				throw new Exception("You try to send a negative tax amount to IDEAL. This is not possible. This may be happend due to discounts.");
			}
		}
		
		return implode('+', $lines);
	}
	
	protected final function taxRateToCode($rate){
		// From the documentation it is not clear if this mapping might be changed by the merchant. For now its just
		// fixed here but could easily be made configurable.
		$rateToCode = array(19 => '101' , 7 => '002', 0 => '000');
		
		$rate = round($rate);
		
		if(isset($rateToCode[$rate])){
			return $rateToCode[$rate];
		}
		return '000';
	}
	
	protected final function getPayType(){
	$paymentMethodName = strtolower($this->getTransaction()->getPaymentMethod()->getPaymentMethodName());
		// TODO: Move this 
		switch($paymentMethodName){	
			case "creditcard" :
				return 'CC';
			case "paypal" :
				return 'PP';
			case "postpay" :
				return 'COD';
			case "directdebits" :
			case "pprodirectdebits" :
			case "debitukdirectdebits" :
			case "intercarddirectdebits" :
			case "instalmentdirectdebits" :
				return 'EDD';
			
			case "giropay" :
			case "ppro-giropay" :
				return 'GP';
			case "directebanking" :
			case "pprodirectebanking" :
				return 'SO';
			
			case "billpayopeninvoice" :
				return 'BP';
				
			case "prepayment" :
				return 'PRP';
			case "billsafeopeninvoice" :
			case 'klarnaopeninvoice':
			case "instalmentinvoice" :
				return 'RG';
			default:
				return 'CC';
		}
	}
	
	protected final function getDueDate(){
		$now = new DateTime();
		$days = '';
		if($this->getPaymentMethod()->existsPaymentMethodConfigurationValue('payment_period')){
			$days = $this->getPaymentMethod()->getPaymentMethodConfigurationValue('payment_period');
		}
		
		if(!is_int($days)){
			$days = '30';
		}
		$i = new DateInterval('P' . $days . 'D');
		$now->add($i);
		return $now->format('Ymd');
	}
	

	public static function encodeTransactionIdToBasketId($transactionId) {
		if (strlen($transactionId) > 16) {
			throw new Exception("The maximal length of a transaction id for pre-payment is 16 chars.");
		}
	
		if (preg_match('/[^0-9_]/', $transactionId)) {
			throw new Exception("Invalid chars in the transaction id. May be you can have applied an transaction number schema. To use pre-payment you can only use numeric chars.");
		}
	
		$position = strpos($transactionId, '_');
		$postfix = '00';
		if ($position !== false) {
			$postfix = (string)$position;
			if (strlen($postfix) == 1) {
				$postfix = '0' . $postfix;
			}
		}
	
		return str_replace('_', '', $transactionId) . $postfix;
	}
	
	public static function decodeTransactionIdFromBasketId($basketId) {
		$lastDigits = substr($basketId, strlen($basketId) - 2, 2);
		$basketId = substr($basketId, 0, -2);
		$position = (int)$lastDigits;
	
		if ($position > 0) {
			$basketId = substr($basketId, 0, $position) . '_' . substr($basketId, $position);
		}
		return $basketId;
	}
	
}