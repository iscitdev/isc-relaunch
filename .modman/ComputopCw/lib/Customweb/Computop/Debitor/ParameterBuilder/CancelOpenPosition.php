<?php 
/**
  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2015 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */

//require_once 'Customweb/Computop/Debitor/ParameterBuilder/Abstract.php';
//require_once 'Customweb/Computop/Util.php';
//require_once 'Customweb/Util/Currency.php';
//require_once 'Customweb/Util/Invoice.php';


final class Customweb_Computop_Debitor_ParameterBuilder_CancelOpenPosition extends Customweb_Computop_Debitor_ParameterBuilder_Abstract {
	
	private $itemsToCancel;
	
	public function __construct(Customweb_Computop_Authorization_Transaction $transaction, Customweb_DependencyInjection_IContainer $container, $items) {
		parent::__construct($transaction, $container);
		$this->itemsToCancel = $items;
	}
	
	public function build(){
		
		$items = $this->itemsToCancel; 
		
		$parameters =  $this->getBasicParameters();
		$parameters['RefNr'] = $this->getTransactionContext()->getOrderId();
		$parameters['TransID'] = $this->getTransactionContext()->getOrderId();
		
		$now = new DateTime();
		$parameters = array_merge(
				$parameters,
				$this->getCustomerIdParameters(),
				$this->getAmountParameters()
		);
		$parameters['Amount'] = Customweb_Computop_Util::formatAmount(
				Customweb_Util_Invoice::getTotalAmountIncludingTax($items),
				$this->getTransaction()->getCurrencyCode()
		);
		$parameters['NetAmount'] = Customweb_Computop_Util::formatAmount(
				Customweb_Util_Invoice::getTotalAmountExcludingTax($items),
				$this->getTransaction()->getCurrencyCode()
		);
		$parameters = array_merge($parameters, $this->getTaxAmounts($items));
		
		if (strtolower($this->getTransaction()->getPaymentMethod()->getPaymentMethodName()) == 'prepayment') {
			
			$totalImtes = Customweb_Util_Invoice::getTotalAmountIncludingTax($this->itemsToCancel);
			if (Customweb_Util_Currency::compareAmount($totalImtes, $this->getTransaction()->getAuthorizationAmount(), $this->getTransaction()->getCurrencyCode()) >= 0) {
				$parameters['EventToken'] = 'ST';
			}
			else {
				$parameters['EventToken'] = 'UP';
			}
			
			$parameters['PayType'] = $this->getPayType();
			$parameters['ShoppingBasketID'] = $this->getTransactionContext()->getOrderId();
			$parameters['CreationDate'] = $now->format('Ymd');
			$parameters['DueDate'] = $this->getDueDate();
			$parameters['ShopID'] = $this->getConfiguration()->getShopId();
			$parameters['AmountClass'] = $this->getAmountClass($items);
		}
		else {
			$parameters['EventToken'] = 'CR';
			$parameters['PayType'] = $this->getPayType();
			$parameters['ShopID'] = $this->getConfiguration()->getShopId();
			$parameters['AmountClass'] = $this->getAmountClass($items);
			$parameters['InvoiceDate'] = $now->format('dmy');
		}
		
		// As per Miko Hörnlein, we need never to send any PayID for any event token (CR, ST and UP)
		// Phone Call from: 11. March 2015
// 		$parameters['PayID'] = $this->getTransaction()->getDebitorPaymentId();
		
		
		$parameters = $this->getSecurityParameters($parameters);
		$parameters = $this->encryptParameters($parameters);
		
		return $parameters;
	}
		
}