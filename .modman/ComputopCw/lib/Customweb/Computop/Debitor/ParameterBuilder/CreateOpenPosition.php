<?php 
/**
  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2015 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */

//require_once 'Customweb/Computop/Debitor/ParameterBuilder/Abstract.php';
//require_once 'Customweb/Computop/Util.php';
//require_once 'Customweb/Util/Invoice.php';


final class Customweb_Computop_Debitor_ParameterBuilder_CreateOpenPosition extends Customweb_Computop_Debitor_ParameterBuilder_Abstract {

	public function build(){
		$items = $this->getTransaction()->getCapturedLineItems();
		
		$orderContext = $this->getOrderContext();
		$parameters =  $this->getBasicParameters();
		$parameters['RefNr'] = $this->getTransactionContext()->getOrderId();
		
		$parameters['TransID'] = $this->getTransactionContext()->getOrderId();
		
		$now = new DateTime();
		$parameters['AmountClass'] = $this->getAmountClass($items);
		$parameters['PayType'] = $this->getPayType();
		$parameters = array_merge(
			$parameters, 
			$this->getCustomerIdParameters(),
			$this->getAmountParameters()
		);
		
		$parameters['ShopID'] = $this->getConfiguration()->getShopId();
		$parameters['InvoiceDate'] = $now->format('dmy');
		$parameters['EventToken'] = 'OC';
		
		
		$parameters['NetAmount'] = Customweb_Computop_Util::formatAmount(
			Customweb_Util_Invoice::getTotalAmountExcludingTax($items),
			$this->getTransaction()->getCurrencyCode()
		);
		$parameters['Amount'] = Customweb_Computop_Util::formatAmount(
				Customweb_Util_Invoice::getTotalAmountIncludingTax($items),
				$this->getTransaction()->getCurrencyCode()
		);
		$parameters = array_merge($parameters, $this->getTaxAmounts($items));
		
		$parameters = $this->getSecurityParameters($parameters);
		$parameters = $this->encryptParameters($parameters);
		
		return $parameters;
	}
		
}