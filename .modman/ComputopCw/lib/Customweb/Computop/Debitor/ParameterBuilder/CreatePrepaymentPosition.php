<?php 
/**
  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2015 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */

//require_once 'Customweb/Computop/Debitor/ParameterBuilder/Abstract.php';


final class Customweb_Computop_Debitor_ParameterBuilder_CreatePrepaymentPosition extends Customweb_Computop_Debitor_ParameterBuilder_Abstract {

	public function build(){
		$items = $this->getTransaction()->getTransactionContext()->getOrderContext()->getInvoiceItems();
		
		$parameters =  $this->getBasicParameters();
		$parameters['RefNr'] = $this->getTransactionContext()->getOrderId();
		
		$now = new DateTime();
		$parameters['AmountClass'] = $this->getAmountClass($items);
		$parameters['PayType'] = $this->getPayType();
		$parameters = array_merge(
			$parameters, 
			$this->getCustomerIdParameters(),
			$this->getAmountParameters()
		);
		unset($parameters['DebitorID']);
		
		$parameters['ShopID'] = $this->getConfiguration()->getShopId();
		
		$parameters['EventToken'] = 'AN';
		
		$parameters['ShoppingBasketID'] = $this->getTransactionContext()->getOrderId();
		$parameters['TransID'] = $this->getTransactionContext()->getOrderId();
		$parameters['CreationDate'] = $now->format('Ymd');
		$parameters['DueDate'] = $this->getDueDate();
		
		$parameters = $this->getSecurityParameters($parameters);
		$parameters = $this->encryptParameters($parameters);
				
		return $parameters;
	}
		
}