<?php

/**
 *  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2015 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */

//require_once 'Customweb/Core/String.php';
//require_once 'Customweb/Core/Util/System.php';
//require_once 'Customweb/Core/Url.php';

//require_once 'Net/SFTP.php';

final class Customweb_Computop_Debitor_Prepayment_FileManager {
	const STORAGE_KEY = "prepayment-file-import";
	
	/**
	 *
	 * @var Customweb_DependencyInjection_IContainer
	 */
	private $container;
	private $sftpFolderPath;
	private $sftpHost;
	private $sftpPathPrefix = '';
	private $sftpPort = 22;
	private static $createdFiles = array();
	
	/**
	 *
	 * @var Net_SFTP
	 */
	private $sftp;

	public function __construct(Customweb_DependencyInjection_IContainer $container){
		$this->container = $container;
		$url = new Customweb_Core_Url($this->getConfiguration()->getPrepaymentSftpUrl());
		$this->sftpHost = $url->getHost();
		$this->sftpPort = $url->getPort();
		
		$path = $url->getFullPath();
		if (empty($path)) {
			$this->sftpFolderPath = '/';
		}
		else if (strlen($path) > 1 && substr($path, -1) == '/') {
			$this->sftpFolderPath = $path;
		}
		else {
			$this->sftpFolderPath = dirname($path);
			$this->sftpPathPrefix = basename($path);
		}
	}

	/**
	 *
	 * @return Customweb_Computop_Configuration
	 */
	public function getConfiguration(){
		return $this->container->getBean('Customweb_Computop_Configuration');
	}

	/**
	 * Downloads the next file from the remote server and returns the local path to the file.
	 *
	 * @return string Local path of the file
	 */
	public function getNextFile(){
		$nextRemoteFile = $this->findNextRemoteFolder();
		if ($nextRemoteFile !== null) {
			$tempLocalFile = tempnam(Customweb_Core_Util_System::getTemporaryDirPath(), 'prepayment_ideal_');
			$rs = $this->getSftpClient()->get($nextRemoteFile, $tempLocalFile);
			if ($rs === false) {
				unlink($tempLocalFile);
				throw new Exception(Customweb_Core_String::_("Unable to download the file '@file'.")->format(array(
					'@file' => $nextRemoteFile 
				)));
			}
			self::$createdFiles[$tempLocalFile] = $nextRemoteFile;
			
			return $tempLocalFile;
		}
		
		return null;
	}

	/**
	 * Marks the given local file as processed.
	 * The method will also mark the file remotely as processed.
	 *
	 * @param string $path
	 * @return void
	 */
	public function markFileAsProcessed($path){
		if (isset(self::$createdFiles[$path])) {
			$remoteFile = self::$createdFiles[$path];
			$this->getSftpClient()->delete($remoteFile);
			unlink($path);
			
			$this->getStorage()->write(self::STORAGE_KEY, $this->getKeyByFileName($remoteFile), array(
				'isProcessed' => true,
				'fileName' => $remoteFile,
				'processedOn' => time(),
			));
		}
	}

	private function findNextRemoteFolder(){
		$files = $this->getSftpClient()->nlist($this->sftpFolderPath);
		foreach ($files as $file) {
			if (strpos($file, $this->sftpPathPrefix) === 0) {
				$candiatedFile = rtrim($this->sftpFolderPath, '/') . '/' . $file;
				if (!$this->checkIfFileIsProcessed($candiatedFile)) {
					return $candiatedFile;
				}
			}
		}
		
		return null;
	}

	private function checkIfFileIsProcessed($remoteFileName){
		$storage = $this->getStorage();
		$hash = $this->getKeyByFileName($remoteFileName);
		
		// We don't work here with locks, because we can not guarantee the isolation. We guarantee 
		// the correct processing by processing the transaction inside database transaction.

		$value = $storage->read(self::STORAGE_KEY, $hash);
		if ($value === null) {
			$value = $storage->write(self::STORAGE_KEY, $hash, array('fileName' => $remoteFileName));
			return false;
		}
		
		if (!isset($value['isProcessed']) || $value['isProcessed'] === false) {
			return false;
		}
		else {
			return true;
		}
	}
	
	private function getKeyByFileName($remoteFilePath) {
		return sha1(basename($remoteFilePath));
	}

	/**
	 *
	 * @return Net_SFTP
	 */
	private function getSftpClient(){
		if ($this->sftp === null) {
			$this->sftp = new Net_SFTP($this->sftpHost, $this->sftpPort);
			
			$username = $this->getConfiguration()->getPrepaymentSftpUsername();
			$privateKey = $this->getConfiguration()->getPrepaymentSftpPrivateKey();
			
			if (!empty($privateKey)) {
				$key = new Crypt_RSA();
				$key->loadKey($privateKey);
				$this->sftp->login($username, $key);
			}
			else {
				$this->sftp->login($username, $this->getConfiguration()->getPrepaymentSftpPassword());
			}
		}
		
		return $this->sftp;
	}

	/**
	 *
	 * @return Customweb_Storage_IBackend
	 */
	private function getStorage(){
		return $this->container->getBean('Customweb_Storage_IBackend');
	}
}