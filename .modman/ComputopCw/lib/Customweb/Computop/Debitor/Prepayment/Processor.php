<?php

/**
 *  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2015 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */
//require_once 'Customweb/Computop/Authorization/Transaction.php';
//require_once 'Customweb/Core/DateTime.php';
//require_once 'Customweb/I18n/Translation.php';
//require_once 'Customweb/Payment/Authorization/DefaultTransactionHistoryItem.php';

class Customweb_Computop_Debitor_Prepayment_Processor {
	
	/**
	 *
	 * @var Customweb_DependencyInjection_IContainer
	 */
	private $container;

	public function __construct(Customweb_DependencyInjection_IContainer $container){
		$this->container = $container;
	}

	public function process($file){
		if (($handle = fopen($file, "r")) !== FALSE) {
			while (($data = fgetcsv($handle, 1000)) !== FALSE) {
				if (count($data) >= 6) {
					$basketId = $data[1];
					$amount = trim($data[4]);
					$eventTokent = $data[5];
					if (strtoupper($eventTokent) == 'ZE') {
						// The amount can be either '0', '-' or '+'. 
						// '-' means the customer has paid not enough. 
						// '+' means the customer has paid to much.
						// '0' means the customer has paid the exact amount.
						if ($amount == '-') {
							$this->markTransactionAsFailedPaymentAmount($basketId);
						}
						else {
							$this->captureTransaction($basketId);
						}
						
					}
				}
			}
			fclose($handle);
		}
		else {
			throw new Exception("Unable to open temporary file.");
		}
	}

	/**
	 * This method marks the given transaction as failed payment. Means the customer has paid tool less. Hence 
	 * we can not deliver the goods.
	 * 
	 * @param int $basketId
	 * @throws Exception
	 */
	private function markTransactionAsFailedPaymentAmount($basketId){
		$this->getTransactionHandler()->beginTransaction();
		try {
			$transaction = $this->loadTransactionByBasketId($basketId);
			if ($transaction instanceof Customweb_Computop_Authorization_Transaction) {
				$transaction->addHistoryItem(new Customweb_Payment_Authorization_DefaultTransactionHistoryItem(Customweb_I18n_Translation::__("Payment received. However it was lower than the authorization amount."), 'prepayment-import'));
				$this->getCancelAdpater()->cancel($transaction);
				$this->getTransactionHandler()->persistTransactionObject($transaction);
			}
			$this->getTransactionHandler()->commitTransaction();
		}
		catch (Exception $e) {
			$this->getTransactionHandler()->rollbackTransaction();
			throw $e;
		}
	}
	
	private function captureTransaction($basketId){
		$this->getTransactionHandler()->beginTransaction();
		try {
			$transaction = $this->loadTransactionByBasketId($basketId);
			if ($transaction instanceof Customweb_Computop_Authorization_Transaction) {
				$transaction->setPaidDate(Customweb_Core_DateTime::_());
				if ($transaction->isCapturePossible()) {
					$this->getCaptureAdpater()->capture($transaction);
					$transaction->addHistoryItem(new Customweb_Payment_Authorization_DefaultTransactionHistoryItem(Customweb_I18n_Translation::__("Payment received. Capture triggered."), 'prepayment-import'));
					$this->getTransactionHandler()->persistTransactionObject($transaction);
					$this->getShopCaptureAdpater()->capture($transaction);
				}
			}
			$this->getTransactionHandler()->commitTransaction();
		}
		catch (Exception $e) {
			$this->getTransactionHandler()->rollbackTransaction();
			throw $e;
		}
	}
	
	private function loadTransactionByBasketId($basketId) {
		try {
			$transactions = $this->getTransactionHandler()->findTransactionsByOrderId($basketId);
			foreach ($transactions as $t) {
				if ($t->isAuthorized()) {
					return $t;
				}
			}
		}
		catch(Exception $e) {
			return null;
		}
	}

	/**
	 *
	 * @return Customweb_Payment_ITransactionHandler
	 */
	private function getTransactionHandler(){
		return $this->container->getBean('Customweb_Payment_ITransactionHandler');
	}

	/**
	 * @return Customweb_Computop_BackendOperation_Adapter_CaptureAdapter
	 */
	private function getCaptureAdpater(){
		return $this->container->getBean('Customweb_Computop_BackendOperation_Adapter_CaptureAdapter');
	}
	
	/**
	 * @return Customweb_Payment_BackendOperation_Adapter_Shop_ICapture
	 */
	private function getShopCaptureAdpater(){
		return $this->container->getBean('Customweb_Payment_BackendOperation_Adapter_Shop_ICapture');
	}

	/**
	 * @return Customweb_Computop_BackendOperation_Adapter_CancellationAdapter
	 */
	private function getCancelAdpater(){
		return $this->container->getBean('Customweb_Computop_BackendOperation_Adapter_CancellationAdapter');
	}
}