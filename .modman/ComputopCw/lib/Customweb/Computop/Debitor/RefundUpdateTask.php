<?php

/**
 *  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2015 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */

//require_once 'Customweb/Computop/Debitor/Adapter.php';
//require_once 'Customweb/Core/Exception/CastException.php';
//require_once 'Customweb/Computop/Authorization/ITransactionUpdateTask.php';
//require_once 'Customweb/Core/DateTime.php';



/**
 *
 * @author Thomas Hunziker
 * @Bean
 *
 */
class Customweb_Computop_Debitor_RefundUpdateTask implements Customweb_Computop_Authorization_ITransactionUpdateTask {

	private $items;
	
	private $processed = false;
	
	public function __construct(array $items) {
		$this->items = $items;
	}
	
	public function execute(Customweb_Computop_Authorization_Transaction $transaction, Customweb_DependencyInjection_IContainer $container) {
		$adapter = $container->getBean('Customweb_Computop_Debitor_Adapter');
		if (!($adapter instanceof Customweb_Computop_Debitor_Adapter)) {
			throw new Customweb_Core_Exception_CastException('Customweb_Computop_Debitor_Adapter');
		}
		
		$adapter->cancelOpenPosition($transaction, $this->items);
		$this->processed = true;

	}

	public function getExecutionTime() {
		if ($this->processed === false) {
			$date = new Customweb_Core_DateTime();
			
			// We wait until 8 in the morning to process the cancel operation.
			$numberOfHoursToWait = 8 - (int)$date->format('H') + 24;
			
			return $date->addHours($numberOfHoursToWait);
		}
		else {
			return null;
		}
	}

}