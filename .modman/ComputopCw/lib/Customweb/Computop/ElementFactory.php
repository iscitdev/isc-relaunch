<?php 
/**
  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2015 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */

//require_once 'Customweb/Computop/IntentionFactory.php';
//require_once 'Customweb/Form/Validator/Checked.php';
//require_once 'Customweb/Form/Validator/NotEmpty.php';
//require_once 'Customweb/Form/Control/Select.php';
//require_once 'Customweb/Form/Control/SingleCheckbox.php';
//require_once 'Customweb/Form/Validator/MinimalLength.php';
//require_once 'Customweb/Util/Country.php';
//require_once 'Customweb/Form/Element.php';
//require_once 'Customweb/I18n/Translation.php';
//require_once 'Customweb/Form/Control/TextInput.php';


/**
 * This class provides static method for creating default elements often 
 * used. The element
 * 
 * @author hunziker
 *
 */
final class Customweb_Computop_ElementFactory {
	
	private static function getMonthArray() {
		return array(
				'none' => Customweb_I18n_Translation::__('Month'),
				'01' => '01', '02' => '02', '03' => '03', '04' => '04',
				'05' => '05', '06' => '06', '07' => '07', '08' => '08',
				'09' => '09', '10' => '10', '11' => '11', '12' => '12',
		);
	}
	
	private function __construct() { }
	

	/**
	 * This method creates a bank name element.
	 *
	 * @param string $fieldName The field name of the bank code element
	 * @return Customweb_Form_IElement
	 */
	public static function getBankNameElement($fieldName, $errorMessage = null) {
		$control = new Customweb_Form_Control_TextInput($fieldName);
		$control->addValidator(new Customweb_Form_Validator_NotEmpty($control, Customweb_I18n_Translation::__("You have to enter name of your bank.")));
		$control->setAutocomplete(false);
		$element = new Customweb_Form_Element(
				Customweb_I18n_Translation::__('Bank Name'),
				$control,
				Customweb_I18n_Translation::__('Please enter here the name of your bank.')
		);
	
		$element->setElementIntention(Customweb_Computop_IntentionFactory::getBankBicIntention())
		->setErrorMessage($errorMessage);
	
		return $element;
	}
	

	public static function getCompanyHolderName($fieldName, $defaultName = '', $errorMessage = null, $required = false) {
		$control = new Customweb_Form_Control_TextInput($fieldName, $defaultName);
		
		$description = Customweb_I18n_Translation::__('Please provide here the company holder name.');
		if (!$required) {
			$description .= ' ' . Customweb_I18n_Translation::__('If it is not applicable leave the field empty.');
		}
		
		$control->setAutocomplete(false);
		$element = new Customweb_Form_Element(
				Customweb_I18n_Translation::__('Company Holder Name'),
				$control,
				$description
		);
		$element->setElementIntention(Customweb_Computop_IntentionFactory::getClick2PayUsernameIntention())
		->setErrorMessage($errorMessage);
		
		if ($required) {
			$control->addValidator(new Customweb_Form_Validator_NotEmpty($control, Customweb_I18n_Translation::__("You have to enter a company holder name.")));
			$element->setRequired(true);
		}
	
		return $element;
	}
	
	public static function getSocialSecurityNumberElement($fieldName, $defaultvalue = '') {
		$control = new Customweb_Form_Control_TextInput($fieldName, $defaultvalue);
		$control->addValidator(new Customweb_Form_Validator_NotEmpty($control, Customweb_I18n_Translation::__("You have to enter your social security number.")));
		$control->setAutocomplete(false);
		$element = new Customweb_Form_Element(
				Customweb_I18n_Translation::__('SSN'),
				$control,
				Customweb_I18n_Translation::__('Please enter here your social security number')
		);
	
		return $element;
	}
	
	public static function getPlaceOfBirthElement($fieldName) {
		$control = new Customweb_Form_Control_TextInput($fieldName);
		$control->addValidator(new Customweb_Form_Validator_NotEmpty($control, Customweb_I18n_Translation::__("You have to enter your place of birth.")));
		$control->setAutocomplete(false);
		$element = new Customweb_Form_Element(
				Customweb_I18n_Translation::__('Place of birth'),
				$control,
				Customweb_I18n_Translation::__('Please enter here your place of birth.')
		);
	
		return $element;
	}
	
	/**
	 * This method creates a account number element.
	 *
	 * @param string $fieldName The field name of the account number element
	 * @return Customweb_Form_IElement
	 */
	public static function getPlaceOfResidenceElement($fieldName, $defaultCountry = null ,$errorMessage = null) {
		$options = array();
		foreach (Customweb_Util_Country::getCountries() as $countryCode => $country) {
			$options[$country['code']] = $country['name'];
		}
		$control = new Customweb_Form_Control_Select($fieldName, $options, $defaultCountry);
		
		$control->addValidator(new Customweb_Form_Validator_NotEmpty($control, Customweb_I18n_Translation::__("You have to enter the place of residence of the account owner.")));
		$element = new Customweb_Form_Element(
				Customweb_I18n_Translation::__('Account owner place of residence'),
				$control,
				Customweb_I18n_Translation::__('Please enter here the country of the place of residence of the account owner.')
		);
	
		$element->setElementIntention(Customweb_Computop_IntentionFactory::getPlaceOfResidenceIntention())
		->setErrorMessage($errorMessage);
	
		return $element;
	}
	
	public static function getGenderElement($fieldName, $errorMessage = null, $label = null, $description = null, $default = null) {
		$gender = array(
				'female' => Customweb_I18n_Translation::__('Female'),
				'male' => Customweb_I18n_Translation::__('Male')
		);
		
		if ($label === null) {
			$lable = Customweb_I18n_Translation::__('Your gender');
		}
		if ($description === null) {
			$description = Customweb_I18n_Translation::__('Please select your gender for the billing address.');
		}
	
		$control = new Customweb_Form_Control_Select($fieldName, $gender, $default);
	
		$control->addValidator(new Customweb_Form_Validator_NotEmpty($control, Customweb_I18n_Translation::__("You have to select your gender.")));
		$element = new Customweb_Form_Element(
				$label,
				$control,
				$description
		);
	
		return $element;
	}
	
	public static function getLegalFormElement($fieldName ,$errorMessage = null, $default = null) {
		$legalForm = array(
				'ag' => Customweb_I18n_Translation::__('AG'),
				'eg' => Customweb_I18n_Translation::__('EG'),
				'einzel' => Customweb_I18n_Translation::__('Eizel'),
				'ek' => Customweb_I18n_Translation::__('EK'),
				'e_ges' => Customweb_I18n_Translation::__('E GES'),
				'ev' => Customweb_I18n_Translation::__('EV'),
				'foundation' => Customweb_I18n_Translation::__('Foundation'),
				'freelancer' => Customweb_I18n_Translation::__('Freelance'),
				'gbr' => Customweb_I18n_Translation::__('GBR'),
				'gmbh' => Customweb_I18n_Translation::__('GmbH'),
				'gmbh_ig' => Customweb_I18n_Translation::__('GmbH IG'),
				'gmbh_co_kg' => Customweb_I18n_Translation::__('GmbH CO KG'),
				'inv_kk' => Customweb_I18n_Translation::__('Inv KK'),
				'kg' => Customweb_I18n_Translation::__('KG'),
				'kgaa' => Customweb_I18n_Translation::__('KGAA'),
				'k_ges' => Customweb_I18n_Translation::__('K GES'),
				'ltd' => Customweb_I18n_Translation::__('LTD'),
				'ltd_co_kg' => Customweb_I18n_Translation::__('LTD CO KG'),
				'ohg' => Customweb_I18n_Translation::__('OHG'),
				'public_inst' => Customweb_I18n_Translation::__('Public institution'),
				'misc_capital' => Customweb_I18n_Translation::__('Misc capital'),
				'misc' => Customweb_I18n_Translation::__('Misc'),
				'ug' => Customweb_I18n_Translation::__('UG')
		);
	
		$control = new Customweb_Form_Control_Select($fieldName, $legalForm, $default);
	
		$control->addValidator(new Customweb_Form_Validator_NotEmpty($control, Customweb_I18n_Translation::__("You have to select your company's legal form.")));
		$element = new Customweb_Form_Element(
				Customweb_I18n_Translation::__('Company legal form'),
				$control,
				Customweb_I18n_Translation::__('Please select the legal form of your company.')
		);
	
		return $element;
	}
	
	public static function getEmailAddressElement($fieldName, $defaultAddress, $errorMessage = null) {
		$control = new Customweb_Form_Control_TextInput($fieldName, $defaultAddress);
		$control->addValidator(new Customweb_Form_Validator_NotEmpty($control, Customweb_I18n_Translation::__("You have to enter the E-Mail address of the account owner.")));
		$control->setAutocomplete(false);
		$element = new Customweb_Form_Element(
				Customweb_I18n_Translation::__('Account owner email address'),
				$control,
				Customweb_I18n_Translation::__('Please enter the email address of the account owner.')
		);
	
		$element->setElementIntention(Customweb_Computop_IntentionFactory::getEmailAddressIntention())
		->setErrorMessage($errorMessage);
	
		return $element;
	}
	
	public static function getAddressStreetElement($fieldName, $defaultAddress, $errorMessage = null) {
		$control = new Customweb_Form_Control_TextInput($fieldName, $defaultAddress);
		$control->addValidator(new Customweb_Form_Validator_NotEmpty($control, Customweb_I18n_Translation::__("You have to enter the street of the account owner.")));
		$control->setAutocomplete(false);
		$element = new Customweb_Form_Element(
				Customweb_I18n_Translation::__('Street'),
				$control,
				Customweb_I18n_Translation::__('Please enter the street of the account owner.')
		);
	
		$element->setElementIntention(Customweb_Computop_IntentionFactory::getAddressStreetIntention())
		->setErrorMessage($errorMessage);
	
		return $element;
	}
	
	public static function getSalutationElement($fieldName) {
		$control = new Customweb_Form_Control_TextInput($fieldName);
		$control->addValidator(new Customweb_Form_Validator_NotEmpty($control, Customweb_I18n_Translation::__("You have to enter your salutation.")));
		$control->setAutocomplete(false);
		$element = new Customweb_Form_Element(
				Customweb_I18n_Translation::__('Salutation'),
				$control,
				Customweb_I18n_Translation::__('Please enter your salutation here.')
		);
	
		return $element;
	}
	
	public static function getAddressZipElement($fieldName, $defaultAddress, $errorMessage = null) {
		$control = new Customweb_Form_Control_TextInput($fieldName, $defaultAddress);
		$control->addValidator(new Customweb_Form_Validator_NotEmpty($control, Customweb_I18n_Translation::__("You have to enter the postal code of the account owner.")));
		$control->setAutocomplete(false);
		$element = new Customweb_Form_Element(
				Customweb_I18n_Translation::__('Account owner postal code'),
				$control,
				Customweb_I18n_Translation::__('Please enter the postal code of the account owner.')
		);
	
		$element->setElementIntention(Customweb_Computop_IntentionFactory::getAddressZipIntention())
		->setErrorMessage($errorMessage);
	
		return $element;
	}
	
	public static function getAddressCityElement($fieldName, $defaultAddress, $errorMessage = null) {
		$control = new Customweb_Form_Control_TextInput($fieldName, $defaultAddress);
		$control->addValidator(new Customweb_Form_Validator_NotEmpty($control, Customweb_I18n_Translation::__("You have to enter the city of the account owner.")));
		$control->setAutocomplete(false);
		$element = new Customweb_Form_Element(
				Customweb_I18n_Translation::__('Account owner city'),
				$control,
				Customweb_I18n_Translation::__('Please enter the city of the account owner.')
		);
	
		$element->setElementIntention(Customweb_Computop_IntentionFactory::getAddressCityIntention())
		->setErrorMessage($errorMessage);
	
		return $element;
	}
	
	public static function getMobilePhoneNumberElement($fieldName, $defaultAddress, $errorMessage = null) {
		$control = new Customweb_Form_Control_TextInput($fieldName, $defaultAddress);
		$control->addValidator(new Customweb_Form_Validator_NotEmpty($control, Customweb_I18n_Translation::__("You have to enter your mobile phone number.")));
		$control->setAutocomplete(false);
		$element = new Customweb_Form_Element(
				Customweb_I18n_Translation::__('Mobile phone number'),
				$control,
				Customweb_I18n_Translation::__('Please enter your mobile phone number.')
		);
	
		$element->setElementIntention(Customweb_Computop_IntentionFactory::getMobilePhoneIntention())
		->setErrorMessage($errorMessage);
	
		return $element;
	}
	
	public static function getBillingPhoneNumberElement($fieldName, $errorMessage = null, $default = null, $minimalLength = 0) {
		$control = new Customweb_Form_Control_TextInput($fieldName, $default);
		$control->addValidator(new Customweb_Form_Validator_NotEmpty($control, Customweb_I18n_Translation::__("You have to enter your phone number.")));
		$control->setAutocomplete(false);
		$element = new Customweb_Form_Element(
				Customweb_I18n_Translation::__('Phone number'),
				$control,
				Customweb_I18n_Translation::__('Please enter your phone number.')
		);
	
		$element->setElementIntention(Customweb_Computop_IntentionFactory::getMobilePhoneIntention())
		->setErrorMessage($errorMessage);
		
		if ($minimalLength > 0) {
			$control->addValidator(new Customweb_Form_Validator_MinimalLength($control, 
					Customweb_I18n_Translation::__("You phone number must be at least @min chars long.", array('@min' => $minimalLength)), $minimalLength));
		}
	
		return $element;
	}
	
	public static function getGtcCheckboxElement($fieldName) {
		$control = new Customweb_Form_Control_SingleCheckbox($fieldName, 'YES' ,'I accept the general terms and conditions');
		$control->addValidator(new Customweb_Form_Validator_Checked($control, Customweb_I18n_Translation::__("You have to accept the general terms and conditions to continue.")));
		$element = new Customweb_Form_Element(
				Customweb_I18n_Translation::__('General Terms and Conditions'),
				$control,
				Customweb_I18n_Translation::__('To continue the order you have to accept the general terms and conditions.')
		);
	
		return $element;
	}
}