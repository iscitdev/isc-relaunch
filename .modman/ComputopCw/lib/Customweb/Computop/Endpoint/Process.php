<?php 
/**
  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2015 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */

//require_once 'Customweb/Computop/Configuration.php';
//require_once 'Customweb/Payment/Authorization/Iframe/IAdapter.php';
//require_once 'Customweb/Payment/Endpoint/Annotation/ExtractionMethod.php';
//require_once 'Customweb/Core/Exception/CastException.php';
//require_once 'Customweb/Core/Http/Response.php';
//require_once 'Customweb/Payment/Endpoint/Controller/Process.php';
//require_once 'Customweb/Computop/Authorization/Transaction.php';
//require_once 'Customweb/Util/Url.php';
//require_once 'Customweb/Computop/Util.php';


/**
 * @Controller("process")
 */
class Customweb_Computop_Endpoint_Process extends Customweb_Payment_Endpoint_Controller_Process
{
	/**
	 * @Action("index")
	 */
	public function process(Customweb_Payment_Authorization_ITransaction $transaction, Customweb_Core_Http_IRequest $request)
	{
		if($this->isUpdate($request)) {
			return $this->updateTransaction($transaction, $request);
		} else {
			$adapter = $this->getAdapterFactory()->getAuthorizationAdapterByName($transaction->getAuthorizationMethod());
			return $adapter->processAuthorization($transaction, $request->getParameters());
		}
	}
	
	/**
	 * @param Customweb_Core_Http_IRequest $request
	 * @ExtractionMethod
	 */
	public function getTransactionId(Customweb_Core_Http_IRequest $request)
	{
		$parameters = $request->getParameters();
		if (isset($parameters['cwTransIdProcess'])) {
			return array(
				'id' => $parameters['cwTransIdProcess'],
				'key' => Customweb_Payment_Endpoint_Annotation_ExtractionMethod::EXTERNAL_TRANSACTION_ID_KEY,
			);
		}
	
		$parameters = Customweb_Computop_Util::decryptIfNeeded($this->getConfiguration(), $parameters);
		if (isset($parameters['PayID']) && !empty($parameters['PayID'])) {
			return array(
				'id' => $parameters['PayID'],
				'key' => Customweb_Payment_Endpoint_Annotation_ExtractionMethod::PAYMENT_ID_KEY,
			);
		}
		
		return parent::getTransactionId($request);
	}
	
	/**
	 * @return Customweb_Payment_IConfigurationAdapter
	 */
	private function getConfiguration()
	{
		return new Customweb_Computop_Configuration($this->getContainer()->getBean('Customweb_Payment_IConfigurationAdapter'));
	}
	
	/**
	 * @param Customweb_Core_Http_IRequest $request
	 * @return boolean
	 */
	private function isUpdate(Customweb_Core_Http_IRequest $request)
	{
		$parameters = $request->getParameters();
		if(isset($parameters['is_update']) || (isset($parameters['cwTransIdProcess']) && isset($parameters['Status']))) {
			return true;
		}
		return false;
	}
	
	/**
	 * @param Customweb_Payment_Authorization_ITransaction $transaction
	 * @param Customweb_Core_Http_IRequest $request
	 * @throws Customweb_Core_Exception_CastException
	 * @return mixed
	 */
	private function updateTransaction(Customweb_Payment_Authorization_ITransaction $transaction, Customweb_Core_Http_IRequest $request)
	{
		$parameters = $request->getParameters();
		
		if (!($transaction instanceof Customweb_Computop_Authorization_Transaction)) {
			throw new Customweb_Core_Exception_CastException('Customweb_Computop_Authorization_Transaction');
		}
		
		// Since we are only able to send one set of dynamic parameters, we can't distinguish between an notification or
		// customer request based on the base URL. Hence we need to do it based on the parameters sent.
		if (isset($parameters['cwTransIdProcess'])) {
			if ($transaction->getAuthorizationMethod() == Customweb_Payment_Authorization_Iframe_IAdapter::AUTHORIZATION_METHOD_NAME) {
				$url = Customweb_Util_Url::appendParameters($transaction->getTransactionContext()->getIframeBreakOutUrl(), $transaction->getTransactionContext()->getCustomParameters());
				return 'redirect:' . $url;
			}
			elseif (Customweb_Computop_Util::isStatusSuccessful(strtolower($parameters['Status']))) {
				return 'redirect:' . $transaction->getSuccessUrl();
			}
			else {
				$transaction->setAuthorizationFailed(Customweb_Computop_Util::generateSpecificErrorMessages($parameters));
				return 'redirect:' . $transaction->getFailedUrl();
			}
		}
		
		$parameters = Customweb_Computop_Util::decryptIfNeeded($this->getConfiguration(), $parameters);
		$paymentMethodWrapper = $this->getContainer()
			->getBean('Customweb_Computop_Method_Factory')
			->getMethodFactory()
			->getPaymentMethod($transaction->getTransactionContext()->getOrderContext()->getPaymentMethod(), $transaction->getAuthorizationMethod());
		$paymentMethodWrapper->processTransactionUpdate($transaction, $parameters);
		return Customweb_Core_Http_Response::_("ok");
	}
}