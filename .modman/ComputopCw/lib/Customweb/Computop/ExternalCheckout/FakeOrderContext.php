<?php

/**
 *  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2015 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */

//require_once 'Customweb/Payment/Authorization/IOrderContext.php';



/**
 * Order context which is created based on the checkout context. 
 *
 * @author Thomas Hunziker
 *
 */
class Customweb_Computop_ExternalCheckout_FakeOrderContext implements Customweb_Payment_Authorization_IOrderContext{

	/**
	 * @var Customweb_Payment_ExternalCheckout_IContext
	 */
	private $context;
	
	public function __construct(Customweb_Payment_ExternalCheckout_IContext $context) {
		$this->context = $context;	
	}
	
	public function getCheckoutId() {
		return $this->context->getContextId();
	}

	public function getShippingAddress() {
		return $this->context->getShippingAddress();
	}

	public function getBillingAddress() {
		return $this->context->getBillingAddress();
	}

	public function getCustomerId() {
		return $this->context->getCustomerId();
	}

	public function isNewCustomer() {
		throw new Exception("Not supported exception.");
	}

	public function getCustomerRegistrationDate() {
		throw new Exception("Not supported exception.");
	}

	public function getCustomerEMailAddress() {
		return $this->context->getCustomerEmailAddress();
	}

	public function getOrderAmountInDecimals() {
		return $this->context->getOrderAmountInDecimals();
	}

	public function getCurrencyCode() {
		return $this->context->getCurrencyCode();
	}

	public function getInvoiceItems() {
		return $this->context->getInvoiceItems();
	}

	public function getShippingMethod() {
		return $this->context->getShippingMethodName();
	}

	public function getPaymentMethod() {
		return $this->context->getPaymentMethod();
	}

	public function getLanguage() {
		return $this->context->getLanguage();
	}

	public function getOrderParameters() {
		throw new Exception("Not supported exception.");
	}

	public function getBillingEMailAddress() {
		throw new Exception("Not supported exception.");
	}

	public function getBillingGender() {
		throw new Exception("Not supported exception.");
	}

	public function getBillingSalutation() {
		throw new Exception("Not supported exception.");
	}

	public function getBillingFirstName() {
		throw new Exception("Not supported exception.");
	}

	public function getBillingLastName() {
		throw new Exception("Not supported exception.");
	}

	public function getBillingStreet() {
		throw new Exception("Not supported exception.");
	}

	public function getBillingCity() {
		throw new Exception("Not supported exception.");
	}

	public function getBillingPostCode() {
		throw new Exception("Not supported exception.");
	}

	public function getBillingState() {
		throw new Exception("Not supported exception.");
	}

	public function getBillingCountryIsoCode() {
		throw new Exception("Not supported exception.");
	}

	public function getBillingPhoneNumber() {
		throw new Exception("Not supported exception.");
	}

	public function getBillingMobilePhoneNumber() {
		throw new Exception("Not supported exception.");
	}

	public function getBillingDateOfBirth() {
		throw new Exception("Not supported exception.");
	}

	public function getBillingCommercialRegisterNumber() {
		throw new Exception("Not supported exception.");
	}

	public function getBillingCompanyName() {
		throw new Exception("Not supported exception.");
	}

	public function getBillingSalesTaxNumber() {
		throw new Exception("Not supported exception.");
	}

	public function getBillingSocialSecurityNumber() {
		throw new Exception("Not supported exception.");
	}

	public function getShippingEMailAddress() {
		throw new Exception("Not supported exception.");
	}

	public function getShippingGender() {
		throw new Exception("Not supported exception.");
	}

	public function getShippingSalutation() {
		throw new Exception("Not supported exception.");
	}

	public function getShippingFirstName() {
		throw new Exception("Not supported exception.");
	}

	public function getShippingLastName() {
		throw new Exception("Not supported exception.");
	}

	public function getShippingStreet() {
		throw new Exception("Not supported exception.");	}

	public function getShippingCity() {
		throw new Exception("Not supported exception.");
	}

	public function getShippingPostCode() {
		throw new Exception("Not supported exception.");
	}

	public function getShippingState() {
		throw new Exception("Not supported exception.");
	}

	public function getShippingCountryIsoCode() {
		throw new Exception("Not supported exception.");
	}

	public function getShippingPhoneNumber() {
		throw new Exception("Not supported exception.");
	}

	public function getShippingMobilePhoneNumber() {
		throw new Exception("Not supported exception.");
	}

	public function getShippingDateOfBirth() {
		throw new Exception("Not supported exception.");
	}

	public function getShippingCompanyName() {
		throw new Exception("Not supported exception.");
	}

	public function getShippingCommercialRegisterNumber() {
		throw new Exception("Not supported exception.");
	}

	public function getShippingSalesTaxNumber() {
		throw new Exception("Not supported exception.");
	}

	public function getShippingSocialSecurityNumber() {
		throw new Exception("Not supported exception.");
	}

}