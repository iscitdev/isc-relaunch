<?php

/**
 *  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2015 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */

//require_once 'Customweb/Computop/ExternalCheckout/MasterPass/RedirectionParameterBuilder.php';
//require_once 'Customweb/Mvc/Template/RenderContext.php';
//require_once 'Customweb/Payment/ExternalCheckout/AbstractCheckoutEndpoint.php';
//require_once 'Customweb/Core/Http/Response.php';
//require_once 'Customweb/Computop/Method/MasterPass/AuthorizationParameterBuilder.php';
//require_once 'Customweb/Payment/Authorization/ITransactionHistoryItem.php';
//require_once 'Customweb/Computop/Container.php';
//require_once 'Customweb/Payment/Authorization/OrderContext/Address/Default.php';
//require_once 'Customweb/Util/Html.php';
//require_once 'Customweb/I18n/Translation.php';
//require_once 'Customweb/Computop/RemoteRequest/Default.php';
//require_once 'Customweb/Computop/Util.php';
//require_once 'Customweb/Mvc/Layout/RenderContext.php';
//require_once 'Customweb/Mvc/Template/SecurityPolicy.php';
//require_once 'Customweb/Payment/Authorization/DefaultTransactionHistoryItem.php';



/**
 * @Controller("masterpass")
 */
class Customweb_Computop_ExternalCheckout_MasterPass_Endpoint extends Customweb_Payment_ExternalCheckout_AbstractCheckoutEndpoint {
	
	/**
	 *
	 * @var Customweb_Computop_Container
	 */
	private $container;

	public function __construct(Customweb_DependencyInjection_IContainer $container){
		parent::__construct($container);
		$this->container = new Customweb_Computop_Container($container);
	}

	/**
	 * @Action("redirect")
	 */
	public function redirectAction(Customweb_Core_Http_IRequest $request){
		$context = $this->loadContextFromRequest($request);
		try {
			$this->checkContextTokenInRequest($request, $context);

			// We set already here the payment method to be able to access the
			// setting data in the redirection parameter builder.
			$checkoutService = $this->container->getCheckoutService();
			foreach ($checkoutService->getPossiblePaymentMethods($context) as $method) {
				if (strtolower($method->getPaymentMethodName()) == 'masterpass') {
					$checkoutService->updatePaymentMethod($context, $method);
					break;
				}
			}
			
			$templateContext = new Customweb_Mvc_Template_RenderContext();
			$templateContext->setSecurityPolicy(new Customweb_Mvc_Template_SecurityPolicy());
			$templateContext->setTemplate('checkout/masterpass/redirect');
			$templateContext->addVariable('url', $this->getRedirectionUrl($context));
			$token = $this->getSecurityTokenFromRequest($request);
			$templateContext->addVariable('formData', Customweb_Util_Html::buildHiddenInputFields($this->getRedirectionParameters($context, $token)));
			$templateContext->addVariable('redirectionText', Customweb_I18n_Translation::__("You will be redirected in a few seconds to MasterPass."));
			
			$content = $this->getTemplateRenderer()->render($templateContext);
			
			$layoutContext = new Customweb_Mvc_Layout_RenderContext();
			$layoutContext->setTitle('MasterPass: Redirection');
			$layoutContext->setMainContent($content);
			return $this->getLayoutRenderer()->render($layoutContext);
		}
		catch(Exception $e) {
			$this->getCheckoutService()->markContextAsFailed($context, $e->getMessage());
			return Customweb_Core_Http_Response::redirect($context->getCartUrl());
		}
	}

	/**
	 * @Action("update-context")
	 */
	public function updateContextAction(Customweb_Core_Http_IRequest $request){
		$parameters = $request->getParameters();
		$checkoutService = $this->container->getCheckoutService();
		
		$this->getTransactionHandler()->beginTransaction();
		$context = $this->loadContextFromRequest($request);
		try {
			$this->checkContextTokenInRequest($request, $context);
			$parameters = Customweb_Computop_Util::decryptIfNeeded($this->container->getConfiguration(), $parameters);
			
			if (!isset($parameters['Status'])) {
				return 'no status parameter provided.';
			}
			
			if (strtolower($parameters['Status']) != 'ok') {
				$checkoutService->markContextAsFailed($context, $parameters['Description'] . ' CODE: ' . $parameters['Code']);
				$this->getTransactionHandler()->commitTransaction();
				return Customweb_Core_Http_Response::redirect($context->getCartUrl());
			}
			
			if (!isset($parameters['masterpassid'])) {
				return "no parameter 'masterpassid' provided.";
			}
				
			if (!isset($parameters['transactionid'])) {
				return "no parameter 'transactionid' provided.";
			}
			
			$checkoutService->updateProviderData($context,
					array(
						'MasterPassID' => $parameters['masterpassid'],
						'TransactionID' => $parameters['transactionid']
					));
			
			// If the AddrStreet is set we are able to set the addresses.
			if (isset($parameters['AddrStreet'])) {
					
				$shippingAddress = $this->getShippingAddressFromParameters($parameters);
				$checkoutService->updateShippingAddress($context, $shippingAddress);
					
				if (!isset($parameters['conemail'])) {
					throw new Exception("The conemail parameter is missing. Without this parameter no user account can be created.");
				}
					
				// We have got also a billing address
				if (isset($parameters['billingname'])) {
					$billingAddress = $this->getBillingAddressFromParameters($parameters);
					$checkoutService->updateBillingAddress($context, $billingAddress);
				}
				else {
					// In case we do not get a billing address, we use the shipping address.
					$checkoutService->updateBillingAddress($context, $shippingAddress);
				}
			}
			
			$this->getTransactionHandler()->commitTransaction();
			
			return $checkoutService->authenticate($context, $parameters['conemail'], $this->getConfirmationPageUrl($context, $this->getSecurityTokenFromRequest($request)));
		}
		catch(Exception $e) {
			$this->getCheckoutService()->markContextAsFailed($context, $e->getMessage());
			$this->getTransactionHandler()->commitTransaction();
			return Customweb_Core_Http_Response::redirect($context->getCartUrl());
		}
	}

	/**
	 * @Action("confirmation")
	 */
	public function confirmationAction(Customweb_Core_Http_IRequest $request){
		$context = $this->loadContextFromRequest($request);
		try {
			$this->checkContextTokenInRequest($request, $context);
			$checkoutService = $this->container->getCheckoutService();
			$parameters = $request->getParameters();
			
			$templateContext = new Customweb_Mvc_Template_RenderContext();
			$confirmationErrorMessage = null;
			$shippingMethodErrorMessage = null;
			$additionalFormErrorMessage = null;
			if (isset($parameters['masterpass_update_shipping'])) {
				try {
					$checkoutService->updateShippingMethod($context, $request);
				}
				catch (Exception $e) {
					$shippingMethodErrorMessage = $e->getMessage();
				}
			}
			else if (isset($parameters['masterpass_confirmation'])) {
				try {
					$checkoutService->processAdditionalFormElements($context, $request);
				} catch (Exception $e) {
					$additionalFormErrorMessage = $e->getMessage();
				}
				if ($additionalFormErrorMessage === null) {
					try {
						$checkoutService->validateReviewForm($context, $request);
				
						$transaction = $checkoutService->createOrder($context);
						if (!$transaction->isAuthorized() && !$transaction->isAuthorizationFailed()) {
							$this->authorizeTransaction($context, $transaction);
						}
						if ($transaction->isAuthorizationFailed()) {
							$confirmationErrorMessage = current($transaction->getErrorMessages());
						}
						else {
							return Customweb_Core_Http_Response::redirect($transaction->getSuccessUrl());
						}
					}
					catch (Exception $e) {
						$confirmationErrorMessage = $e->getMessage();
					}
				}
			}
			
			$templateContext->setSecurityPolicy(new Customweb_Mvc_Template_SecurityPolicy());
			$templateContext->setTemplate('checkout/masterpass/confirmation');
			
			$templateContext->addVariable('additionalFormElements', $checkoutService->renderAdditionalFormElements($context, $additionalFormErrorMessage));
			$templateContext->addVariable('shippingPane', $checkoutService->renderShippingMethodSelectionPane($context, $shippingMethodErrorMessage));
			$templateContext->addVariable('reviewPane', $checkoutService->renderReviewPane($context, true, $confirmationErrorMessage));
			$templateContext->addVariable('confirmationPageUrl', $this->getConfirmationPageUrl($context, $this->getSecurityTokenFromRequest($request)));
			$templateContext->addVariable('javascript', $this->getAjaxJavascript('.computop-masterpass-shipping-pane', '.computop-masterpass-confirmation-pane'));
			
			$content = $this->getTemplateRenderer()->render($templateContext);
			
			$layoutContext = new Customweb_Mvc_Layout_RenderContext();
			$layoutContext->setTitle(Customweb_I18n_Translation::__('MasterPass: Order Confirmation'));
			$layoutContext->setMainContent($content);
			return $this->getLayoutRenderer()->render($layoutContext);
			
		}
		catch(Exception $e) {
			$this->getCheckoutService()->markContextAsFailed($context, $e->getMessage());
			return Customweb_Core_Http_Response::redirect($context->getCartUrl());
		}
	}

	private function authorizeTransaction(Customweb_Payment_ExternalCheckout_IContext $context, Customweb_Computop_Authorization_Transaction $transaction){
		$this->getTransactionHandler()->beginTransaction();
		try {
			$providerData = $context->getProviderData();
			if (!isset($providerData['MasterPassID'])) {
				throw new Exception("No MasterPassID set in the provider data.");
			}
			if (!isset($providerData['TransactionID'])) {
				throw new Exception("No TransactionID set in the provider data.");
			}
			
			// @formatter:off
			$request = new Customweb_Computop_RemoteRequest_Default(
				$this->container->getConfiguration(), 
				new Customweb_Computop_Method_MasterPass_AuthorizationParameterBuilder($transaction, $this->container, $providerData['MasterPassID'], $providerData['TransactionID']), 
				$this->container->getConfiguration()->getMasterPassAuthorizationUrl()
			);
			// @formatter:on
			$request->process();
			
			$transaction->authorize();
			$this->getDebitorManager()->processAuthorize($transaction);
			$transaction->setAuthorizationParameters($request->getResponseParameters());
		}
		catch (Exception $e) {
			$transaction->setAuthorizationFailed($e->getMessage());
		}
		try {
			$captureSetting = $transaction->getPaymentMethod()->getPaymentMethodConfigurationValue('capturing');
			$parameters = $request->getResponseParameters();
			
			$transaction->setPaymentId($parameters['PayID']);
			if(isset($parameters['TransID'])){
				$transaction->setExternalTransId($parameters['TransID']);
			}
				
			if($captureSetting == 'direct' && $parameters['Status'] != 'pending'){
				$transaction->capture();
				$this->getDebitorManager()->processCapture($transaction);
			}
		}
		catch(Exception $e) {
			$transaction->addHistoryItem(new Customweb_Payment_Authorization_DefaultTransactionHistoryItem($e->getMessage(), Customweb_Payment_Authorization_ITransactionHistoryItem::ACTION_CAPTURING));
		}
		
		$this->getTransactionHandler()->persistTransactionObject($transaction);
		$this->getTransactionHandler()->commitTransaction();
	}

	private function getConfirmationPageUrl(Customweb_Payment_ExternalCheckout_IContext $context, $token){
		return $this->getUrl('masterpass', 'confirmation', 
				array(
					'context-id' => $context->getContextId(),
					'token' => $token,
				));
	}

	private function getRedirectionParameters(Customweb_Payment_ExternalCheckout_IContext $context, $token){
		$builder = new Customweb_Computop_ExternalCheckout_MasterPass_RedirectionParameterBuilder($context, $this->container, $token);
		return $builder->build();
	}

	private function getRedirectionUrl(Customweb_Payment_ExternalCheckout_IContext $context){
		return $this->container->getConfiguration()->getMasterPassRedirectionUrl();
	}

	private function getShippingAddressFromParameters(array $parameters){
		$requiredParamters = array(
			'AddrStreet',
			'name',
			'AddrCity',
			'AddrZip',
			'AddrCountryCode',
			'conemail' 
		);
		foreach ($requiredParamters as $parameterName) {
			if (!isset($parameters[$parameterName])) {
				throw new Exception("Parameter $parameterName is missing.");
			}
		}
		
		list($firstname, $lastname) = explode(' ', $parameters['name'], 2);
		$shippingAddress = new Customweb_Payment_Authorization_OrderContext_Address_Default();
		// @formatter:off
		$shippingAddress
			->setFirstName($firstname)
			->setLastName($lastname)
			->setStreet($parameters['AddrStreet'])
			->setCity($parameters['AddrCity'])
			->setCountryIsoCode($parameters['AddrCountryCode'])
			->setPostCode($parameters['AddrZip'])
			->setEMailAddress($parameters['conemail']);
		// @formatter:on
		

		if (isset($parameters['addrstate'])) {
			$shippingAddress->setState($parameters['addrstate']);
		}
		if (isset($parameters['Phone'])) {
			$shippingAddress->setPhoneNumber($parameters['Phone']);
		}
		if (isset($parameters['addrstreet2'])) {
			$shippingAddress->setStreet($shippingAddress->getStreet() . " " . $parameters['addrstreet2']);
		}
		if (isset($parameters['addrstreet3'])) {
			$shippingAddress->setStreet($shippingAddress->getStreet() . " " . $parameters['addrstreet3']);
		}
		return $shippingAddress;
	}

	private function getBillingAddressFromParameters(array $parameters){
		$requiredParamters = array(
			'billingaddrstreet',
			'billingname',
			'billingaddrcity',
			'billingaddrzip',
			'billingaddrcountrycode',
			'conemail'
		);
		foreach ($requiredParamters as $parameterName) {
			if (!isset($parameters[$parameterName])) {
				throw new Exception("Parameter $parameterName is missing.");
			}
		}
		
		list($firstname, $lastname) = explode(' ', $parameters['billingname'], 2);
		$billingAddress = new Customweb_Payment_Authorization_OrderContext_Address_Default();
		// @formatter:off
		$billingAddress
			->setFirstName($firstname)
			->setLastName($lastname)
			->setStreet($parameters['billingaddrstreet'])
			->setCity($parameters['billingaddrcity'])
			->setCountryIsoCode($parameters['billingaddrcountrycode'])
			->setPostCode($parameters['billingaddrzip'])
			->setEMailAddress($parameters['conemail']);
		// @formatter:on

		if (isset($parameters['billingaddrstreet2'])) {
			$billingAddress->setStreet($billingAddress->getStreet() . " " . $parameters['billingaddrstreet2']);
		}
		if (isset($parameters['billingaddrstreet3'])) {
			$billingAddress->setStreet($billingAddress->getStreet() . " " . $parameters['billingaddrstreet3']);
		}
		return $billingAddress;
	}
	
	/**
	 * @return Customweb_Computop_Debitor_Manager
	 */
	protected function getDebitorManager(){
		return $this->getContainer()->getBean('Customweb_Computop_Debitor_Manager');
	}
	

	/**
	 * @return Customweb_Computop_Method_Factory
	 */
	protected function getMethodFactory() {
		return $this->getContainer()->getBean('Customweb_Computop_Method_Factory');
	}
	
	protected function getPaymentMethodByTransaction(Customweb_Computop_Authorization_Transaction $transaction){
		return $this->getMethodFactory()->getPaymentMethod($transaction->getTransactionContext()->getOrderContext()->getPaymentMethod(), $transaction->getAuthorizationMethod());
	}
	
	
}