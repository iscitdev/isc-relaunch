<?php

/**
 *  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2015 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */

//require_once 'Customweb/Computop/Method/MasterPass/LineItemBuilder.php';
//require_once 'Customweb/Util/String.php';
//require_once 'Customweb/Computop/Util.php';
//require_once 'Customweb/Util/Currency.php';
//require_once 'Customweb/Core/Assert.php';
//require_once 'Customweb/Core/Url.php';
//require_once 'Customweb/Computop/ExternalCheckout/FakeOrderContext.php';



class Customweb_Computop_ExternalCheckout_MasterPass_RedirectionParameterBuilder {
	
	/**
	 * @var Customweb_Computop_Container
	 */
	private $container;
	
	/**
	 * @var Customweb_Payment_ExternalCheckout_IContext
	 */
	private $context;
	
	private $token;
	
	public function __construct(Customweb_Payment_ExternalCheckout_IContext $context, Customweb_Computop_Container $container, $token) {
		$this->container = $container;
		$this->context = $context;	
		$this->token = $token;
	}
	
	/**
	 * @return array
	 */
	public function build() {
		
		$paymentMethod = $this->context->getPaymentMethod();
		Customweb_Core_Assert::notNull($paymentMethod);
		
		$acceptedCards = 'master,amex,diners,discover,maestro,visa';
		if ($paymentMethod->existsPaymentMethodConfigurationValue('accepted_cards')) {
			$acceptedCards = implode(',', $paymentMethod->getPaymentMethodConfigurationValue('accepted_cards'));
		}
		
		$authLevelBasic = 'false';
		if ($paymentMethod->existsPaymentMethodConfigurationValue('threed_check')) {
			if (strtolower($paymentMethod->getPaymentMethodConfigurationValue('threed_check')) == 'basic') {
				$authLevelBasic = 'true';
			}
		}
		
		$parameters = array(
			'MerchantID' => $this->getConfiguration()->getMerchantId(),
			'TransID' => 'mp_c_' . time() . '__' . $this->context->getContextId(),
			'RefNr' => $this->context->getContextId(),
			'ReqID' => 'mp_c_' . time() . '__' . $this->context->getContextId(),
			'Amount' => Customweb_Util_Currency::formatAmount($this->context->getOrderAmountInDecimals(), $this->context->getCurrencyCode(), '', ''),
			'Currency' => $this->context->getCurrencyCode(),
			'OrderDesc' => Customweb_Util_String::substrUtf8($this->getConfiguration()->getShopDescription($this->context->getLanguage()), 0, 200),
			'ArticleList' => $this->buildArticleList(),
			'AcceptableCards' => $acceptedCards,
			'SuppressShippingAddress' => $this->isContextContainingOnlyVirtualProducts() ? 'true' : 'false',
			'RewardProgram' => 'false',
			'AuthLevelBasic' => $authLevelBasic,
		);
		
		if ($paymentMethod->existsPaymentMethodConfigurationValue('shipping_profile_id')) {
			$profileId = $paymentMethod->getPaymentMethodConfigurationValue('shipping_profile_id');
			if (!empty($profileId)) {
				$parameters['ShippingProfile'] = $profileId;
			}
		}
		
		$feedbackUrl = new Customweb_Core_Url($this->container->getEndpointAdapter()->getUrl('masterpass', 'update-context', array('context-id' => $this->context->getContextId(), 'token' => $this->token, 'utm_nooverride' => '1')));
		$feedbackUrlParameters = $feedbackUrl->getQueryAsArray();
		$feedbackUrl->setQuery(array());
		$plainFeedbackUrl = $feedbackUrl->toString();
		
		if (strpos($plainFeedbackUrl, 'https') !== 0) {
			throw new Exception("Feedback URL does not use HTTPS. Please check the configuration.");
		}
		
		$plainFeedbackUrl = str_replace(':443', '', $plainFeedbackUrl);
		
		$parameters['URLSuccess'] = $plainFeedbackUrl;
		$parameters['URLFailure'] = $plainFeedbackUrl;
		$parameters['URLNotify'] = $plainFeedbackUrl;
		$parameters['Custom'] = $this->buildCustomParameters($feedbackUrlParameters);
		
		return Customweb_Computop_Util::encryptParameters($parameters, $this->getConfiguration());
	}
	
	private function getConfiguration() {
		return $this->container->getConfiguration();
	}
	
	private function buildArticleList() {
		$orderContext = new Customweb_Computop_ExternalCheckout_FakeOrderContext($this->context);
		$builder = new Customweb_Computop_Method_MasterPass_LineItemBuilder($orderContext);
		return $builder->build();
	}
	
	private function isContextContainingOnlyVirtualProducts() {
		foreach ($this->context->getInvoiceItems() as $item) {
			if ($item->isShippingRequired()) {
				return false;
			}
		}
		return true;
	}
	
	private function buildCustomParameters(array $userData) {
		$parts = array();
		foreach ($userData as $key => $value) {
			$parts[] = $key . '=' . $value;
		}
		
		return implode('|', $parts);
	}
	
}
	