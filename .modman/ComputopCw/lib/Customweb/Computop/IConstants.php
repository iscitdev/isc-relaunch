<?php 
/**
  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2015 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */



/**
 * This interface provides some constants for the  Computop service.
 * 
 * @author Severin Klingler
 *           	   		  		 
 */
interface Customweb_Computop_IConstants {

	const URL_QPAY_INIT = '';
	
	/* #### Paymorrow #### */
	/**
	 * Paymorrow needs an unique reference number for every
	 * operation(Capture, Refund) on a transaction. We achieve that by appending
	 * the count of former operations, leaded by zeros up to this amount.
	 * E.g., with MAX_OPERATION_DIGITS = 3, 999 Capture PLUS 999 Refunds are possible etc.
	 *
	 * @var int
	 */
	const MAX_OPERATION_DIGITS = 3;
}