<?php 
/**
  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2015 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */

//require_once 'Customweb/Form/Intention/Intention.php';


final class Customweb_Computop_IntentionFactory {
	private static $bankBic = null;
	private static $accountIban = null;
	private static $payboxNumber = null;
	private static $click2payUsername = null;
	private static $click2payPan = null;
	private static $placeOrResidence = null;
	private static $emailAddress = null;
	private static $addressStreet = null;
	private static $addressZip = null;
	private static $addressCity = null;
	private static $mobilePhoneNumber = null;
	private static $accountNumber  = null;

	private function __construct() {}

	/**
	 * This method returns the bank code element intention.
	 * 
	 * @return Customweb_Form_Intention_Intention
	 */
	public static function getBankBicIntention(){
		if(self::$bankBic === null){
			self::$bankBic = new Customweb_Form_Intention_Intention('bank-bic');
		}
		return self::$bankBic;
	}
	
	/**
	 * This method returns the paybox number element intention.
	 *
	 * @return Customweb_Form_Intention_Intention
	 */
	public static function getPayboxNumberIntention(){
		if(self::$payboxNumber === null){
			self::$payboxNumber = new Customweb_Form_Intention_Intention('paybox-number');
		}
		return self::$payboxNumber;
	}
	
	/**
	 * This method returns the account number element intention.
	 * 
	 * @return Customweb_Form_Intention_Intention
	 */
	public static function getAccountIbanIntention(){
		if(self::$accountIban === null){
			self::$accountIban = new Customweb_Form_Intention_Intention('bank-account-iban');
		}
		return self::$accountIban;
	}
	
	/**
	 *
	 * @return Customweb_Form_Intention_Intention
	 */
	public static function getClick2PayUsernameIntention(){
		if(self::$click2payUsername === null){
			self::$click2payUsername = new Customweb_Form_Intention_Intention('click2pay-username');
		}
		return self::$click2payUsername;
	}
	
	public static function getClick2PayPanIntention(){
		if(self::$click2payPan === null){
			self::$click2payPan = new Customweb_Form_Intention_Intention('click2pay-pan');
		}
		return self::$click2payPan;
	}
	
	public static function getPlaceOfResidenceIntention(){
		if(self::$placeOrResidence === null){
			self::$placeOrResidence = new Customweb_Form_Intention_Intention('place-of-residence');
		}
		return self::$placeOrResidence;
	}
	
	public static function getEmailAddressIntention(){
		if(self::$emailAddress === null){
			self::$emailAddress = new Customweb_Form_Intention_Intention('email-address');
		}
		return self::$emailAddress;
	}
	
	public static function getAddressStreetIntention(){
		if(self::$emailAddress === null){
			self::$emailAddress = new Customweb_Form_Intention_Intention('address-street');
		}
		return self::$emailAddress;
	}
	
	public static function getAddressZipIntention(){
		if(self::$emailAddress === null){
			self::$emailAddress = new Customweb_Form_Intention_Intention('address-zip');
		}
		return self::$emailAddress;
	}
	
	public static function getAddressCityIntention(){
		if(self::$emailAddress === null){
			self::$emailAddress = new Customweb_Form_Intention_Intention('address-city');
		}
		return self::$emailAddress;
	}
	
	public static function getMobilePhoneIntention(){
		if(self::$mobilePhoneNumber === null){
			self::$mobilePhoneNumber = new Customweb_Form_Intention_Intention('mobile-phone-number');
		}
		return self::$mobilePhoneNumber;
	}
}