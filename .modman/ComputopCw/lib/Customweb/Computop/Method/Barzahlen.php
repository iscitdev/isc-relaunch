<?php
/**
  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2015 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */

//require_once 'Customweb/Mvc/Template/RenderContext.php';
//require_once 'Customweb/Util/String.php';
//require_once 'Customweb/Computop/Method/DefaultMethod.php';
//require_once 'Customweb/Computop/Util.php';
//require_once 'Customweb/Http/Url.php';
//require_once 'Customweb/Mvc/Template/SecurityPolicy.php';
//require_once 'Customweb/I18n/Translation.php';


/** 
 * 
 * @author Thomas Hunziker
 * @Method(paymentMethods={'Barzahlen'}, authorizationMethods={'ServerAuthorization'})
 */
class Customweb_Computop_Method_Barzahlen extends Customweb_Computop_Method_DefaultMethod{

	
	public function getAuthorizationUrl() {
		return $this->getServerUrl();
	}
	
	public function getAuthorizationParameters(Customweb_Computop_Authorization_Transaction $transaction, array $httpRequestParameters = array()) {
	
		$parameters = parent::getAuthorizationParameters($transaction, $httpRequestParameters);
		$orderContext = $transaction->getTransactionContext()->getOrderContext();
		
		if (strtolower($orderContext->getBillingCountryIsoCode()) != 'de') {
			throw new Exception(Customweb_I18n_Translation::__("Only customers from Germany are allowed."));
		}
		
		// The RefNr is limited to 20 chars
		$parameters['RefNr'] = Customweb_Util_String::cutStartOff($parameters['RefNr'], 20);
		
		
		$parameters['Email'] = Customweb_Util_String::substrUtf8($orderContext->getBillingEMailAddress(), 0, 80);
		$parameters['Street'] = Customweb_Util_String::substrUtf8($orderContext->getBillingStreet(), 0, 60);
		$parameters['Zip'] = Customweb_Util_String::substrUtf8($orderContext->getBillingPostCode(), 0, 5);
		$parameters['City'] = Customweb_Util_String::substrUtf8($orderContext->getBillingCity(), 0, 50);
		$parameters['CountryCode'] = $orderContext->getBillingCountryIsoCode();
		$parameters['Language'] = Customweb_Computop_Util::getCleanLanguageCode($orderContext->getLanguage());

		// Workaround for handling notify URL
		$parameters['URLNotify'] = $this->getContainer()->getBean('Customweb_Payment_Endpoint_IAdapter')->getUrl('process', 'index');
		$parameters['Custom'] = $this->getUpdateUrlCustomParameter($transaction);

		return $parameters;
	}

	public function processTransactionUpdate(Customweb_Computop_Authorization_Transaction $transaction, array $parameters) {
		if (strtolower($parameters['Status']) == 'ok') {
			$transaction->capture("Payment received.");
			$adapterFactory->getCaptureAdapter()->capture($transaction);
			$transaction->setPaid();
			$transaction->overrideOrderStatusSettingKey('status_paid');
		}
		else {
			if (strtolower($this->getPaymentMethodConfigurationValue('cancel_on_expire')) == 'cancel') {
				$transaction->cancel(strip_tags($parameters['description']));
				$adapterFactory->getCancellationAdapter()->cancel($transaction);
			}
		}
	}

	public function getFinalizeContent(Customweb_Computop_Authorization_Transaction $transaction) {
		if (!$transaction->isAuthorized()) {
			return null;
		}

		$showCompletePage = strtolower($this->getPaymentMethodConfigurationValue('show_complete_page'));

		if ($showCompletePage == 'yes') {
			$parameters = $transaction->getAuthorizationParameters();
			
			$showConfig = $this->getPaymentMethodConfigurationValue('payment_types');
			$slipUrl = $parameters['paymentsliplink'];
			$mobileDeviceText = $parameters['infotext1'];
			
			$additionalText = '';
			if (isset($parameters['infotext2'])) {
				$additionalText = $parameters['infotext2'];
			}
			
			$showPdfSlip = in_array('pdf', $showConfig);
			$showMoblieDeviceText = in_array('mobile', $showConfig);
			$showAdditionalText = in_array('additionalText', $showConfig);

			$templateContext = new Customweb_Mvc_Template_RenderContext();
			$templateContext->setSecurityPolicy(new Customweb_Mvc_Template_SecurityPolicy());
			$templateContext->setTemplate('barzahlen');
			
			$templateContext->addVariable('pdfSlipUrl', $slipUrl);
			$templateContext->addVariable('pdfSlipBaseUrl', basename($slipUrl));
			$templateContext->addVariable('mobileDeviceText', $mobileDeviceText);
			$templateContext->addVariable('additionalText', $additionalText);
			$templateContext->addVariable('isShowPdfSlip', $showPdfSlip);
			$templateContext->addVariable('isShowMobileDeviceText', $showMoblieDeviceText);
			$templateContext->addVariable('isShowAdditionalText', $showAdditionalText);
			$templateContext->addVariable('finishUrl', $transaction->getSuccessUrl());
			
			$engine = $this->getContainer()->getBean('Customweb_Mvc_Template_IRenderer');
			return $engine->render($templateContext);
		}
		else {
			return null;
		}
	}
	
	protected function getUpdateUrlCustomParameter(Customweb_Computop_Authorization_Transaction $transaction) {
		$notificationUrl = $this->getContainer()->getBean('Customweb_Payment_Endpoint_IAdapter')->getUrl('process', 'index', $transaction->getTransactionContext()->getCustomParameters());
		$url = new Customweb_Http_Url($notificationUrl);
		$userData = $url->getQueryAsArray();
		$userData['is_update'] = 'true';
		$userData = array_merge($userData, $transaction->getTransactionContext()->getCustomParameters());
	
		$parts = array();
		foreach ($userData as $key => $value) {
			$parts[] = $key . '=' . $value;
		}
	
		return implode('|', $parts);
	}
}

