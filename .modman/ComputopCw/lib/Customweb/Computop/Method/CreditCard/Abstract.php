<?php

/**
 *  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2015 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */

//require_once 'Customweb/Computop/Method/DefaultMethod.php';
//require_once 'Customweb/Payment/Util.php';
//require_once 'Customweb/Computop/Method/Util.php';



/**
 *
 * @author Thomas Hunziker
 */
class Customweb_Computop_Method_CreditCard_Abstract extends Customweb_Computop_Method_DefaultMethod {

	public function getAuthorizationParameters(Customweb_Computop_Authorization_Transaction $transaction, array $httpRequestParameters = array()){
		$parameters = parent::getAuthorizationParameters($transaction);
		
		// Since AMEX allows only a certain range of RefNr, we have to limit for all credit cards.
		// For offline payments the limitation may be different. Max length 20 chars, only numbers and chars from A-Z. First char not '0'.
		$parameters['RefNr'] = strtoupper(
				Customweb_Payment_Util::applyOrderSchema($this->getGlobalConfiguration()->getTransactionIdSchema(), 
						$transaction->getExternalTransactionId(), 20));
		$parameters['RefNr'] = preg_replace("/[^A-Z0-9]+/", '', $parameters['RefNr']);
		if (substr($parameters['RefNr'], 0, 1) === '0') {
			$parameters['RefNr'] = substr($parameters['RefNr'], 1);
		}
		
		unset($parameters['CCExpiryMonth']);
		unset($parameters['CCExpiryYear']);
		
		$parameters = Customweb_Computop_Method_Util::addTestingParameters($parameters, $transaction, $this->getGlobalConfiguration());
		
		return array_merge(Customweb_Computop_Method_Util::getAddressParameters($transaction->getTransactionContext()->getOrderContext()), 
				$parameters);
	}

	public function processAuthorization(Customweb_Computop_Authorization_Transaction $transaction, array $httpRequestParameters = array()){
		parent::processAuthorization($transaction, $httpRequestParameters);
		if ($transaction->getTransactionContext()->createRecurringAlias()) {
			$this->updateAliasInformation($transaction, $httpRequestParameters);
		}
	}

	protected function updateAliasInformation(Customweb_Computop_Authorization_Transaction $transaction, array $httpRequestParameters){
		if ($transaction->isAuthorized() && isset($httpRequestParameters['PCNr'])) {
			$transaction->setPseudoCardNumber($httpRequestParameters['PCNr']);
			$transaction->setCardExpiry($httpRequestParameters['CCExpiry']);
			$transaction->setCardBrand($httpRequestParameters['CCBrand']);
		}
	}
}