<?php
/**
  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2015 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */

//require_once 'Customweb/Computop/Method/CreditCard/CardHandler.php';
//require_once 'Customweb/Payment/Authorization/Method/CreditCard/CardInformation.php';
//require_once 'Customweb/Payment/Authorization/Method/CreditCard/ElementBuilder.php';
//require_once 'Customweb/Computop/Authorization/Transaction.php';
//require_once 'Customweb/Computop/Method/CreditCard/Abstract.php';
//require_once 'Customweb/I18n/Translation.php';


/** 
 * 
 * @author Thomas Hunziker
 * @Method(paymentMethods={'CreditCard'}, authorizationMethods={'HiddenAuthorization'})
 */
class Customweb_Computop_Method_CreditCard_HiddenAuthorization extends Customweb_Computop_Method_CreditCard_Abstract {
		
	public function getAuthorizationParameters(Customweb_Computop_Authorization_Transaction $transaction, array $httpRequestParameters = array()) {
		$parameters = parent::getAuthorizationParameters($transaction);
		
		$aliasTransaction = $transaction->getTransactionContext()->getAlias();
		if($aliasTransaction !== null && $aliasTransaction !== 'new' && $aliasTransaction instanceof Customweb_Computop_Authorization_Transaction){
			$parameters['CCNr'] = $aliasTransaction->getPseudoCardNumber();
		}
		
		return $parameters;
	}
	
	public function getVisibleFormFields(Customweb_Payment_Authorization_IOrderContext $orderContext, $aliasTransaction, $failedTransaction, $paymentCustomerContext, $authorizationMethod) {
		return $this->getFormBuilder($orderContext, $aliasTransaction, $failedTransaction, $paymentCustomerContext, $authorizationMethod)->build();
	}

	public function getAuthorizationUrl(){
		return $this->getServerUrl();
	}

	public function processAuthorization(Customweb_Computop_Authorization_Transaction $transaction, array $httpRequestParameters = array()) {
		parent::processAuthorization($transaction, $httpRequestParameters);
		if($transaction->getTransactionContext()->getAlias() !== null){
			$this->updateAliasInformation($transaction, $httpRequestParameters);
		}
	}
	
	public function getFormBuilder(Customweb_Payment_Authorization_IOrderContext $orderContext, $aliasTransaction, $failedTransaction, $paymentCustomerContext, $authorizationMethod) {

		$formBuilder = new Customweb_Payment_Authorization_Method_CreditCard_ElementBuilder();

		$informationMap = Customweb_Payment_Authorization_Method_CreditCard_CardInformation::getCardInformationObjects($this->getPaymentInformationMap(), $this->getPaymentMethodConfigurationValue('credit_card_brands'));
		$cardHandler = new Customweb_Computop_Method_CreditCard_CardHandler($informationMap);
		
		
		
		$formBuilder
			->setCardHolderFieldName(null)
			->setCardNumberFieldName('CCNr')
			->setCvcFieldName('CCCVC')
			->setExpiryField('CCExpiry', 'YYMM')
			->setExpiryYearNumberOfDigits(4)
			->setBrandFieldName('CCBrand')
			->setCardHandler($cardHandler)
			->setAutoBrandSelectionActive(true);
		
		// Apply alias transaction
		if($aliasTransaction !== null && $aliasTransaction !== 'new' && $aliasTransaction instanceof Customweb_Computop_Authorization_Transaction){
			
			$aliasBrand = strtolower($aliasTransaction->getCardBrand());
			$brands = $cardHandler->getInteralBrandMap();
			if (isset($brands[$aliasBrand])) {
				$aliasBrand = $brands[$aliasBrand];
			}
			
			$formBuilder
				->setMaskedCreditCardNumber($aliasTransaction->getAliasForDisplay())
				->setSelectedBrand($aliasBrand)
				->setCardNumberFieldName('AliasCCNr')
				->setSelectedExpiryMonth($aliasTransaction->getCardExpiryMonth())
				->setSelectedExpiryYear($aliasTransaction->getCardExpiryYear());
		}
		
		// Apply failed transaction
		if ($failedTransaction !== null && $failedTransaction instanceof Customweb_Computop_Authorization_Transaction) {
			$this->handleFailedTransaction($failedTransaction, $formBuilder);
		}
			
		return $formBuilder;
	}
	
	private function handleFailedTransaction(Customweb_Computop_Authorization_Transaction $failedTransaction, Customweb_Payment_Authorization_Method_CreditCard_ElementBuilder $formBuilder) {
		$failedParameters = $failedTransaction->getAuthorizationParameters();
		$errorCode = null;
		if (isset($failedParameters['Code']) && strlen($failedParameters['Code']) == 8) {
			$errorCode = substr($failedParameters['Code'], 4);
		}
		switch($errorCode) {
			case '0015':
			case '0016':
				$formBuilder->setCardNumberElementErrorMessage(Customweb_I18n_Translation::__('Invalid credit card number entered.'));
				break;
			case '0094':
			case '1350':
			case '0017':
				$formBuilder->setExpiryElementErrorMessage(Customweb_I18n_Translation::__('Invalid expiry date selected.'));
				break;
			case '0018':
				$formBuilder->setCardNumberElementErrorMessage(Customweb_I18n_Translation::__('Invalid card brand selected.'));
				break;
			case '0019':
				$formBuilder->setCvcElementErrorMessage(Customweb_I18n_Translation::__('Invalid CVC Code entered.'));
				break;
		}
	}
	
}