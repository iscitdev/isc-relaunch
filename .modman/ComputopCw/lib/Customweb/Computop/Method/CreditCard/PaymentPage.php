<?php
/**
  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2015 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */

//require_once 'Customweb/Computop/Util.php';
//require_once 'Customweb/Computop/Method/CreditCard/Abstract.php';


/** 
 * 
 * @author Thomas Hunziker
 * @Method(paymentMethods={'CreditCard'}, authorizationMethods={'PaymentPage', 'IFrameAuthorization'})
 */
class Customweb_Computop_Method_CreditCard_PaymentPage extends Customweb_Computop_Method_CreditCard_Abstract {	
	public function getAuthorizationParameters(Customweb_Computop_Authorization_Transaction $transaction, array $httpRequestParameters = array()) {
		$parameters = parent::getAuthorizationParameters($transaction);
		$orderContext = $transaction->getTransactionContext()->getOrderContext();
		$parameters['Language'] = strtolower(Customweb_Computop_Util::getCleanLanguageCode($orderContext->getLanguage()));
		
		return array_merge(
			$this->getAllowedCardZonesParameters(),
			$this->getAllowedCardZonesParameters(),
			$this->getTemplateParameter(),
			$parameters
		);
	}
	
	private function getAllowedIpZonesParameters(){
		$parameters = array();
		$ipZone = $this->getPaymentMethodConfigurationValue('ip_zone');
		if(strlen($this->getAllowedIpZones()) > 0){
			$parameters['IPZone'] = $ipZone;
		}
		return $parameters;
	}
	
	private function getAllowedCardZonesParameters(){
		$parameters = array();
		$zone = $this->getPaymentMethodConfigurationValue('zone');
		if(strlen($zone) > 0){
			$parameters['Zone'] = $zone;
		}
		return $parameters;
	}
	

	private function getTemplateParameter(){
		$parameters = array();
		if ($this->existsPaymentMethodConfigurationValue('template_file')) {
			$templatePath = $this->getPaymentMethodConfigurationValue('template_file');
			if(strlen($templatePath) > 0 ){
				$parameters['Template'] = $templatePath;
			}
		}
		return $parameters;
	}
	
	
}