<?php
/**
  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2015 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */

//require_once 'Customweb/Core/Exception/CastException.php';
//require_once 'Customweb/Payment/Authorization/Recurring/ITransactionContext.php';
//require_once 'Customweb/Computop/Authorization/Transaction.php';
//require_once 'Customweb/Computop/Method/CreditCard/Abstract.php';


/** 
 * 
 * @author Thomas Hunziker
 * @Method(paymentMethods={'CreditCard'}, authorizationMethods={'Recurring'})
 */
class Customweb_Computop_Method_CreditCard_Recurring extends Customweb_Computop_Method_CreditCard_Abstract {

	public function getAuthorizationParameters(Customweb_Computop_Authorization_Transaction $transaction, array $httpRequestParameters = array()) {
		$parameters = parent::getAuthorizationParameters($transaction);
		
		$transactionContext = $transaction->getTransactionContext();
		if (!($transactionContext instanceof Customweb_Payment_Authorization_Recurring_ITransactionContext)) {
			throw new Customweb_Core_Exception_CastException('Customweb_Payment_Authorization_Recurring_ITransactionContext');
		}
		
		$initialTransaction = $transactionContext->getInitialTransaction();
		if (!($initialTransaction instanceof Customweb_Computop_Authorization_Transaction)) {
			throw new Customweb_Core_Exception_CastException('Customweb_Computop_Authorization_Transaction');
		}
		
		$parameters['CCNr'] = $initialTransaction->getPseudoCardNumber();
		$parameters['CCExpiry'] = $initialTransaction->getCardExpiry();
		$parameters['CCBrand'] = $initialTransaction->getCardBrand();
		
		return $parameters;
	}
	
}