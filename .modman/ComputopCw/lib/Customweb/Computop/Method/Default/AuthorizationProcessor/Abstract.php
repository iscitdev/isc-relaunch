<?php
/**
  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2015 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */

//require_once 'Customweb/Computop/Method/Default/AuthorizationProcessor/IProcessor.php';
//require_once 'Customweb/Payment/Exception/PaymentErrorException.php';
//require_once 'Customweb/Computop/Util.php';
//require_once 'Customweb/Payment/Authorization/ErrorMessage.php';
//require_once 'Customweb/I18n/Translation.php';


abstract class Customweb_Computop_Method_Default_AuthorizationProcessor_Abstract implements Customweb_Computop_Method_Default_AuthorizationProcessor_IProcessor{
	
	/**
	 * @var Customweb_DependencyInjection_IContainer
	 */
	private $container = null;
	
	/**
	 * @var Customweb_Computop_Authorization_Transaction
	 */
	private $transaction = null;
	
	/**
	 * @var Customweb_Computop_Configuration
	 */
	private $configuration = null;
	
	private $httpRequestParameters = array();
	
	public function __construct(Customweb_Computop_Authorization_Transaction $transaction, Customweb_DependencyInjection_IContainer $container, array $httpRequestParameters = array()) {
		$this->container = $container;
		$this->transaction = $transaction;
		$this->configuration = $this->getContainer()->getBean('Customweb_Computop_Configuration');
		$this->httpRequestParameters = $httpRequestParameters;
	}

	/**
	 * @return Customweb_Computop_Method_Factory
	 */
	protected function getMethodFactory() {
		return $this->getContainer()->getBean('Customweb_Computop_Method_Factory');
	}
	
	protected function getPaymentMethodByTransaction(Customweb_Computop_Authorization_Transaction $transaction){
		return $this->getMethodFactory()->getPaymentMethod($transaction->getTransactionContext()->getOrderContext()->getPaymentMethod(), $transaction->getAuthorizationMethod());
	}

	/**
	 * @return Customweb_Computop_Debitor_Manager
	 */
	protected function getDebitorManager(){
		return $this->getContainer()->getBean('Customweb_Computop_Debitor_Manager');
	}
	
	/**
	 * This method checks if the set authorization parameters are valid regarding merchant id and transaction id.
	 *
	 * @param Customweb_Computop_Authorization_Transaction $transaction
	 * @throws Customweb_Payment_Exception_PaymentErrorException
	 */
	protected function checkAuthorizationParameterIntegrity() {
		$parameters = $this->getTransaction()->getAuthorizationParameters();
		$transactionId = Customweb_Computop_Util::getTransactionID($this->getConfiguration(), $this->getTransaction());
		if($parameters['TransID'] != $transactionId){
			throw new Customweb_Payment_Exception_PaymentErrorException(new Customweb_Payment_Authorization_ErrorMessage(Customweb_I18n_Translation::__(
					"The transaction id (!transactionId) and the transaction id of in the callback (!callbackId) do not correspond.",
					array(
						'!transactionId' => $transactionId,
						'!callbackId' => $parameters['TransID'],
					)
			)));
		}
		if($parameters['mid'] != $this->getConfiguration()->getMerchantId()){
			throw new Customweb_Payment_Exception_PaymentErrorException(new Customweb_Payment_Authorization_ErrorMessage(Customweb_I18n_Translation::__(
					"The configured merchant id (!transactionId) and the merchant id of in the callback (!callbackId) do not correspond.",
					array(
						'!transactionId' => $this->getConfiguration()->getMerchantId(),
						'!callbackId' => $parameters['mid'],
					)
			)));
		}
	}
	
	protected function processAuthorizationParameters() {
		$parameters = $this->getTransaction()->getAuthorizationParameters();
		$status = null;
		if (isset($parameters['Status'])) {
			$status = strtolower($parameters['Status']);
		}
		
		if (Customweb_Computop_Util::isStatusSuccessful($status)) {
			$this->getTransaction()->authorize();
			$this->getDebitorManager()->processAuthorize($this->getTransaction());
			
			$paymentMethod = $this->getPaymentMethodByTransaction($this->getTransaction());
		
			$this->getTransaction()->setPaymentId($parameters['PayID']);
			if(isset($parameters['TransID'])){
				$this->getTransaction()->setExternalTransId($parameters['TransID']);
			}
		
			if($paymentMethod->getCaptureMode($this->getTransaction()) == 'AUTO' && $status != 'pending'){
				$this->handleDirectCapture();
			}
		
			if($status == 'pending'){
				$this->getTransaction()->setAuthorizationUncertain();
			}
			if(isset($parameters['PaymentGuarantee']) && $parameters['PaymentGuarantee'] != 'FULL'){
				$this->getTransaction()->setAuthorizationUncertain();
			}
			if(isset($parameters['SecCriteria']) && $parameters['SecCriteria'] != '1'){
				$this->getTransaction()->setAuthorizationUncertain();
			}
		
			if($paymentMethod->isTransactionUpdateable()){
				$this->getTransaction()->setUpdateExecutionDate(null);
			}
		}
		else{
			$failedMessage = Customweb_Computop_Util::generateSpecificErrorMessages($parameters);
			$this->getTransaction()->setAuthorizationFailed($failedMessage);
		}
	}
	
	protected function handleDirectCapture(){
		$this->getTransaction()->capture();
		$this->getDebitorManager()->processCapture($this->getTransaction());
	}
	
	/**
	 * @return Customweb_DependencyInjection_IContainer
	 */
	protected function getContainer(){
		return $this->container;
	}

	/**
	 * @param Customweb_DependencyInjection_IContainer $container
	 * @return Customweb_Computop_Method_Authorization_AbstractProcessor
	 */
	protected function setContainer(Customweb_DependencyInjection_IContainer $container){
		$this->container = $container;
		return $this;
	}

	/**
	 * @return Customweb_Computop_Authorization_Transaction
	 */
	protected function getTransaction(){
		return $this->transaction;
	}

	/**
	 * @param Customweb_Computop_Authorization_Transaction $transaction
	 * @return Customweb_Computop_Method_Authorization_AbstractProcessor
	 */
	protected function setTransaction(Customweb_Computop_Authorization_Transaction $transaction){
		$this->transaction = $transaction;
		return $this;
	}

	/**
	 * @return Customweb_Computop_Configuration
	 */
	protected function getConfiguration(){
		return $this->configuration;
	}

	/**
	 * @param Customweb_Computop_Configuration $configuration
	 * @return Customweb_Computop_Method_Authorization_AbstractProcessor
	 */
	protected function setConfiguration(Customweb_Computop_Configuration $configuration){
		$this->configuration = $configuration;
		return $this;
	}

	/**
	 * @return array
	 */
	protected function getHttpRequestParameters(){
		return $this->httpRequestParameters;
	}
	
	
	
	
}