<?php

/**
 *  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2015 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */

//require_once 'Customweb/Computop/Util.php';
//require_once 'Customweb/Payment/Util.php';
//require_once 'Customweb/Util/Invoice.php';
//require_once 'Customweb/Computop/AbstractParameterBuilder.php';

class Customweb_Computop_Method_Default_CaptureParameterBuilder extends Customweb_Computop_AbstractParameterBuilder {
	private $close;
	
	/**
	 *
	 * @var Customweb_Payment_Authorization_IInvoiceItem[]
	 */
	private $captureItems;

	public function __construct(Customweb_Computop_Authorization_Transaction $transaction, Customweb_DependencyInjection_IContainer $container, array $items, $close){
		parent::__construct($transaction, $container);
		$this->close = $close;
		$this->captureItems = $items;
	}

	public function build(){
		$parameters = $this->buildInner();
		
		$parameters = $this->getSecurityParameters($parameters);
		$parameters = $this->encryptParameters($parameters);
		
		return $parameters;
	}

	protected function buildInner(){
		$parameters = array_merge($this->getBasicParameters(), $this->getAmountParameters(), $this->getCaptureParameters());
		$parameters['Amount'] = Customweb_Computop_Util::formatAmount(
				Customweb_Util_Invoice::getTotalAmountIncludingTax($this->getCaptureItems()), $this->getOrderContext()->getCurrencyCode());
		
		if ($this->getPaymentMethod()->existsPaymentMethodConfigurationValue('multiple_captures') &&
				 $this->getPaymentMethod()->getPaymentMethodConfigurationValue('multiple_captures') == 'enabled' && $this->isClose()) {
			$parameters['FinishAuth'] = 'Y';
		}
		
		return $parameters;
	}

	protected function getCaptureParameters(){
		$parameters = array();
		$parameters['PayID'] = $this->getTransaction()->getPaymentId();
		$parameters['RefNr'] = Customweb_Payment_Util::applyOrderSchema($this->getConfiguration()->getTransactionIdSchema(), 
				$this->getTransaction()->getExternalTransactionId(), 29) . 'C';
		return $parameters;
	}

	/**
	 *
	 * @return boolean
	 */
	protected function isClose(){
		return $this->close;
	}

	/**
	 *
	 * @return Customweb_Payment_Authorization_IInvoiceItem[]
	 */
	protected function getCaptureItems(){
		return $this->captureItems;
	}

	/**
	 * Returns an invoice number based on the transaction id.
	 * However it may be
	 * not the actual invoice number of the shopping cart.
	 *
	 * @return string
	 */
	protected function getNextInvoiceNumer(){
		$number = count($this->getTransaction()->getCaptures());
		return $this->getTransaction()->getExternalTransactionId() . '_c_' . ($number + 1);
	}
}