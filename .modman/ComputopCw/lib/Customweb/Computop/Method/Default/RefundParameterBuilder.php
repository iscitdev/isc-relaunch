<?php
/**
  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2015 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */

//require_once 'Customweb/Computop/Util.php';
//require_once 'Customweb/Util/Invoice.php';
//require_once 'Customweb/Computop/AbstractParameterBuilder.php';


class Customweb_Computop_Method_Default_RefundParameterBuilder extends Customweb_Computop_AbstractParameterBuilder {
	
	private $close;
	
	/**
	 * @var Customweb_Payment_Authorization_IInvoiceItem[]
	 */
	private $refundItems;
	
	public function __construct(Customweb_Computop_Authorization_Transaction $transaction, Customweb_DependencyInjection_IContainer $container, array $items, $close) {
		parent::__construct($transaction, $container);
		$this->close = $close;
		$this->refundItems = $items;
	}
	
	/**
	 *
	 */
	public function build(){
		$parameters = $this->buildInner();
		$parameters = $this->getSecurityParameters($parameters);
		$parameters = $this->encryptParameters($parameters);
		return $parameters;
	}
	
	protected function buildInner() {
		$parameters = array_merge(
				$this->getBasicParameters(),
				$this->getAmountParameters(),
				$this->getRefundParameters()
		);
		$parameters['Amount'] = Customweb_Computop_Util::formatAmount(
				Customweb_Util_Invoice::getTotalAmountIncludingTax($this->getRefundItems()),
				$this->getOrderContext()->getCurrencyCode()
		);
		
		return $parameters;
	}

	protected function getRefundParameters(){
		$parameters = array();
		$parameters['PayID'] = $this->getTransaction()->getPaymentId();
		return $parameters;
	}
	

	/**
	 * @return boolean
	 */
	protected function isClose(){
		return $this->close;
	}
	
	/**
	 * @return Customweb_Payment_Authorization_IInvoiceItem[]
	 */
	protected function getRefundItems(){
		return $this->refundItems;
	}
	
	
	/**
	 * Returns an refund number based on the transaction id. However it may be
	 * not the actual refund number of the shopping cart.
	 *
	 * @return string
	 */
	protected function getNextRefundNumer() {
		$number = count($this->getTransaction()->getRefunds());
		return $this->getTransaction()->getExternalTransactionId() . '_r_' . ($number+1);
	}
	
	
}