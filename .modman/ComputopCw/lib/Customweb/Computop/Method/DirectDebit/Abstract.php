<?php
/**
  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2015 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */

//require_once 'Customweb/Computop/Method/DefaultMethod.php';
//require_once 'Customweb/Computop/Util.php';
//require_once 'Customweb/Payment/Authorization/Method/Sepa/Mandate.php';


/**
 * Abstract implementation of different direct debit processors.
 * 
 * @author Thomas Hunziker
 *
 */
class Customweb_Computop_Method_DirectDebit_Abstract extends Customweb_Computop_Method_DefaultMethod {

	public function getAuthorizationParameters(Customweb_Computop_Authorization_Transaction $transaction, array $httpRequestParameters = array()) {
		$parameters = parent::getAuthorizationParameters($transaction, $httpRequestParameters);
		
		$orderContext = $transaction->getTransactionContext()->getOrderContext();
		$parameters['MandateID'] = $this->cleanMandateId($transaction->getExternalTransactionId());
		$parameters['DtOfSgntr'] = date('d.m.Y');
		
		if ($transaction->getTransactionContext()->createRecurringAlias()) {
			$parameters['MdtSeqType'] = 'FRST';
		}
		else {
			$parameters['MdtSeqType'] = 'OOFF';
		}
		
		$parameters['Language'] = strtolower(Customweb_Computop_Util::getCleanLanguageCode($orderContext->getLanguage()));
		

		$email = $orderContext->getCustomerEMailAddress();
		if (!empty($email)) {
			$parameters['E-Mail'] = $email;
		}
		
		
		return $parameters;
	}
	
	protected function cleanMandateId($id) {
		return Customweb_Payment_Authorization_Method_Sepa_Mandate::sanitizeMandateId($id);
	}
	
}