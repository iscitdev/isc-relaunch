<?php

/**
 *  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2015 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */

//require_once 'Customweb/Computop/Processor/BillPay/AbstractMethod.php';
//require_once 'Customweb/Computop/Method/Util.php';
//require_once 'Customweb/Payment/Authorization/Method/Sepa/ElementBuilder.php';



/**
 * This open invoice implementation does not use any processor.
 * The transaction is threaded as a
 * regular order. No data is sent.
 *
 * @author Thomas Hunziker
 * @Method(paymentMethods={'DirectDebits'}, authorizationMethods={'ServerAuthorization'}, processors={'BillPay'})
 *
 */
class Customweb_Computop_Method_DirectDebit_BillPay extends Customweb_Computop_Processor_BillPay_AbstractMethod {
	
	public function getVisibleFormFields(Customweb_Payment_Authorization_IOrderContext $orderContext, $aliasTransaction, $failedTransaction, $paymentCustomerContext, $authorizationMethod){
		$builder = new Customweb_Payment_Authorization_Method_Sepa_ElementBuilder();
		$builder->setBicFieldName('BIC')->setIbanFieldName('IBAN')->setAccountHolderFieldName('AccountOwner')->setAccountHolderName(
				$orderContext->getBillingFirstName() . ' ' . $orderContext->getBillingLastName());
		
		return array_merge($builder->build(), 
				parent::getVisibleFormFields($orderContext, $aliasTransaction, $failedTransaction, $paymentCustomerContext, $authorizationMethod));
	}

	public function getAuthorizationParameters(Customweb_Computop_Authorization_Transaction $transaction, array $httpRequestParameters = array()){
		$parameters = parent::getAuthorizationParameters($transaction, $httpRequestParameters);
		
		$parameters['BillPayAction'] = '2';
		
		$orderContext = $transaction->getTransactionContext()->getOrderContext();
		return array_merge(
				$parameters,
				Customweb_Computop_Method_Util::getBicParameters($orderContext, $httpRequestParameters),
				Customweb_Computop_Method_Util::getIbanParameters($orderContext, $httpRequestParameters),
				Customweb_Computop_Method_Util::getAccountOwnerParameters($orderContext, $httpRequestParameters)
		);
	}
}