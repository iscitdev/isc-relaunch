<?php
/**
  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2015 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */

//require_once 'Customweb/Computop/Method/DirectDebit/Abstract.php';
//require_once 'Customweb/Util/String.php';
//require_once 'Customweb/Computop/ElementFactory.php';
//require_once 'Customweb/I18n/Translation.php';
//require_once 'Customweb/Computop/Method/Util.php';


/**
 * Abstract implementation to use with the direct processing of
 * Computop.
 * 
 * @author Thomas Hunziker
 *
 */
class Customweb_Computop_Method_DirectDebit_Evo_Abstract extends Customweb_Computop_Method_DirectDebit_Abstract {


	public function getVisibleFormFields(Customweb_Payment_Authorization_IOrderContext $orderContext, $aliasTransaction, $failedTransaction, $paymentCustomerContext, $authorizationMethod) {
		$elements = parent::getVisibleFormFields($orderContext, $aliasTransaction, $failedTransaction, $paymentCustomerContext, $authorizationMethod);
		
		$billingGender = $orderContext->getBillingGender();
		if(empty($billingGender)){
			$elements[] = Customweb_Computop_ElementFactory::getGenderElement(
					'billingGender', null,
					Customweb_I18n_Translation::__('Billing Address Gender'),
					Customweb_I18n_Translation::__('Please select your gender for the billing address.')
			);
		}
		
		return $elements;
	}
	
	
	
	public function getAuthorizationParameters(Customweb_Computop_Authorization_Transaction $transaction, array $httpRequestParameters = array()) {
		$parameters = parent::getAuthorizationParameters($transaction, $httpRequestParameters);
		$orderContext = $transaction->getTransactionContext()->getOrderContext();
		
		$parameters['FirstName'] = Customweb_Util_String::substrUtf8($orderContext->getBillingFirstName(), 0, 30);
		$parameters['LastName'] = Customweb_Util_String::substrUtf8($orderContext->getBillingLastName(), 0, 30);
		$parameters['Salutation'] = Customweb_Util_String::substrUtf8(
				Customweb_Computop_Method_Util::translateGender(
						Customweb_Computop_Method_Util::getBillingGender($orderContext, $httpRequestParameters), $orderContext), 0, 10);
		
		$companyName = $orderContext->getBillingCompanyName();
		if (!empty($companyName)) {
			$parameters['CompanyOrPerson'] = 'F';
		}
		else {
			$parameters['CompanyOrPerson'] = 'P';
		}

		if ($orderContext->getBillingDateOfBirth() !== null) {
			$parameters['DateOfBirth'] = $orderContext->getBillingDateOfBirth()->format('Ymd');
		}
		
		$parameters['AddrCountryCode'] = $orderContext->getBillingCountryIsoCode();

		$email = $orderContext->getCustomerEMailAddress();
		if (!empty($email)) {
			$parameters['E-Mail'] = $email;
		}
		
		return $parameters;
	}
}

