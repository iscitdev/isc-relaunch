<?php
/**
  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2015 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */

//require_once 'Customweb/Core/Util/System.php';
//require_once 'Customweb/Computop/Method/DirectDebit/Evo/Abstract.php';
//require_once 'Customweb/Computop/Method/Util.php';
//require_once 'Customweb/Payment/Authorization/Method/Sepa/ElementBuilder.php';


/**
 * Abstract implementation to use with the direct processing of
 * Computop.
 * 
 * @author Thomas Hunziker
 * @Method(paymentMethods={'DirectDebits'}, authorizationMethods={'ServerAuthorization'}, processors={'Evo'})
 *
 */
class Customweb_Computop_Method_DirectDebit_Evo_Server extends Customweb_Computop_Method_DirectDebit_Evo_Abstract {
	
	public function getVisibleFormFields(Customweb_Payment_Authorization_IOrderContext $orderContext, $aliasTransaction, $failedTransaction, $paymentCustomerContext, $authorizationMethod) {
		$builder = new Customweb_Payment_Authorization_Method_Sepa_ElementBuilder();
		$builder->setBicFieldName('BIC')->setIbanFieldName('IBAN')
		->setAccountHolderFieldName('AccOwner')->setAccountHolderName($orderContext->getBillingFirstName() . ' ' . $orderContext->getBillingLastName());
		
		return array_merge(
				parent::getVisibleFormFields($orderContext, $aliasTransaction, $failedTransaction, $paymentCustomerContext, $authorizationMethod),
				$builder->build()
		);
		
	}
	
	public function getAuthorizationParameters(Customweb_Computop_Authorization_Transaction $transaction, array $httpRequestParameters = array()) {
		$parameters = parent::getAuthorizationParameters($transaction, $httpRequestParameters);
		$parameters['IPAddr'] = Customweb_Core_Util_System::getClientIPAddress();
		
		$orderContext = $transaction->getTransactionContext()->getOrderContext();
		return array_merge(
			$parameters,
			Customweb_Computop_Method_Util::getBicParameters($orderContext, $httpRequestParameters),
			Customweb_Computop_Method_Util::getIbanParameters($orderContext, $httpRequestParameters),
			Customweb_Computop_Method_Util::getAccountOwnerParameters($orderContext, $httpRequestParameters)
		);
	}

	public function getAuthorizationUrl(){
		return 'edddirect.aspx';
	}
	
}




