<?php
/**
  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2015 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */

//require_once 'Customweb/Computop/Method/DirectDebit/Abstract.php';
//require_once 'Customweb/Util/Address.php';
//require_once 'Customweb/Util/String.php';
//require_once 'Customweb/Computop/ElementFactory.php';
//require_once 'Customweb/I18n/Translation.php';
//require_once 'Customweb/Computop/Method/Util.php';


/**
 * Abstract implementation to use with the direct processing of
 * Computop.
 * 
 * @author Thomas Hunziker
 *
 */
class Customweb_Computop_Method_DirectDebit_InterCard_Abstract extends Customweb_Computop_Method_DirectDebit_Abstract {


	public function getVisibleFormFields(Customweb_Payment_Authorization_IOrderContext $orderContext, $aliasTransaction, $failedTransaction, $paymentCustomerContext, $authorizationMethod) {
		$elements = parent::getVisibleFormFields($orderContext, $aliasTransaction, $failedTransaction, $paymentCustomerContext, $authorizationMethod);
		
		$billingGender = $orderContext->getBillingGender();
		if(empty($billingGender)){
			$elements[] = Customweb_Computop_ElementFactory::getGenderElement(
					'billingGender', null,
					Customweb_I18n_Translation::__('Billing Address Gender'),
					Customweb_I18n_Translation::__('Please select your gender for the billing address.')
			);
		}

		$shippingGender = $orderContext->getShippingGender();
		if (empty($shippingGender) && !Customweb_Computop_Method_Util::equalsBillingAndShippingName($orderContext)) {
			$elements[] = Customweb_Computop_ElementFactory::getGenderElement(
					'shippingGender',
					null,
					Customweb_I18n_Translation::__('Shipping Address Gender'),
					Customweb_I18n_Translation::__('Please select your gender for the shipping address.')
			);
		}
		
		return $elements;
	}
	
	
	
	public function getAuthorizationParameters(Customweb_Computop_Authorization_Transaction $transaction, array $httpRequestParameters = array()) {
		$parameters = parent::getAuthorizationParameters($transaction, $httpRequestParameters);
	
		$orderContext = $transaction->getTransactionContext()->getOrderContext();
		$customerId = $orderContext->getCustomerId();
		if (empty($customerId)) {
			$customerId = 'guest_' . date('Y-m-d');
		}
		$parameters['CustomerID'] = Customweb_Util_String::substrUtf8($customerId, 0, 30);
		
		$companyName = $orderContext->getBillingCompanyName();
		if (empty($companyName)) {
			$parameters['CustomerClassification'] = 'private';
		}
		else {
			$parameters['CustomerClassification'] = 'business';
		}
		
		if ($orderContext->getBillingDateOfBirth() !== null) {
			$parameters['DateOfBirth'] = $orderContext->getBillingDateOfBirth()->format('Ymd');
		}
		
		$email = $orderContext->getCustomerEMailAddress();
		if (!empty($email)) {
			$parameters['eMail'] = $email;
		}
		$parameters['bdSalutation'] = Customweb_Util_String::substrUtf8(Customweb_Computop_Method_Util::translateGender(Customweb_Computop_Method_Util::getBillingGender($orderContext, $httpRequestParameters), $orderContext), 0, 4);
		
		$parameters['bdFirstName'] = Customweb_Util_String::substrUtf8($orderContext->getBillingFirstName(), 0, 30);
		$parameters['bdLastName'] = Customweb_Util_String::substrUtf8($orderContext->getBillingLastName(), 0, 30);
		
		$address = Customweb_Util_Address::splitStreet($orderContext->getBillingStreet(), $orderContext->getBillingCountryIsoCode(), $orderContext->getBillingPostCode());
		
		$parameters['bdStreet'] = Customweb_Util_String::substrUtf8($address['street'], 0, 50);
		$parameters['bdStreetNr'] = Customweb_Util_String::substrUtf8($address['street-number'], 0, 5);
		$parameters['bdZip'] = Customweb_Util_String::substrUtf8($orderContext->getBillingPostCode(), 0, 5);
		$parameters['bdCity'] = Customweb_Util_String::substrUtf8($orderContext->getBillingCity(), 0, 50);
		$parameters['bdCountryCode'] = $orderContext->getBillingCountryIsoCode();
		$phone = $orderContext->getBillingPhoneNumber();
		if (!empty($phone)) {
			$parameters['bdPhone'] = Customweb_Util_String::substrUtf8($phone, 0, 15);
		}
		$parameters['UseBillingData'] = 'No';
		
		$parameters['sdSalutation'] = Customweb_Util_String::substrUtf8(Customweb_Computop_Method_Util::translateGender(Customweb_Computop_Method_Util::getShippingGender($orderContext, $httpRequestParameters), $orderContext), 0, 4);
		$parameters['sdFirstName'] = Customweb_Util_String::substrUtf8($orderContext->getShippingFirstName(), 0, 30);
		$parameters['sdLastName'] = Customweb_Util_String::substrUtf8($orderContext->getShippingLastName(), 0, 30);
		
		$address = Customweb_Util_Address::splitStreet($orderContext->getShippingStreet(), $orderContext->getShippingCountryIsoCode(), $orderContext->getShippingPostCode());
		$parameters['sdStreet'] = Customweb_Util_String::substrUtf8($address['street'], 0, 50);
		$parameters['sdStreetNr'] = Customweb_Util_String::substrUtf8($address['street-number'], 0, 5);
		
		$parameters['sdCountryCode'] = $orderContext->getShippingCountryIsoCode();
		$parameters['sdZip'] = Customweb_Util_String::substrUtf8($orderContext->getShippingPostCode(), 0, 5);
		$parameters['sdCity'] = Customweb_Util_String::substrUtf8($orderContext->getBillingCity(), 0, 50);
		
		return $parameters;
	}
}

