<?php
/**
  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2015 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */

//require_once 'Customweb/Computop/Processor/RatePay/AbstractMethod.php';
//require_once 'Customweb/Form/Validator/Checked.php';
//require_once 'Customweb/Computop/ElementFactory.php';
//require_once 'Customweb/Computop/Processor/RatePay/DirectDebit/InitializeParameterBuilder.php';
//require_once 'Customweb/Form/Control/SingleCheckbox.php';
//require_once 'Customweb/Computop/Processor/RatePay/DirectDebit/AuthorizeParameterBuilder.php';
//require_once 'Customweb/Form/Element.php';
//require_once 'Customweb/I18n/Translation.php';
//require_once 'Customweb/Payment/Authorization/Method/Sepa/ElementBuilder.php';


/**
 * This open invoice implementation does not use any processor. The transaction is threaded as a
 * regular order. No data is sent.
 * 
 * @author Thomas Hunziker
 * @Method(paymentMethods={'DirectDebits'}, authorizationMethods={'ServerAuthorization'}, processors={'RatePay'})
 *
 */
class Customweb_Computop_Method_DirectDebit_RatePay extends Customweb_Computop_Processor_RatePay_AbstractMethod {

	public function getVisibleFormFields(Customweb_Payment_Authorization_IOrderContext $orderContext, $aliasTransaction, $failedTransaction, $paymentCustomerContext, $authorizationMethod) {
	
		$builder = new Customweb_Payment_Authorization_Method_Sepa_ElementBuilder();
		$builder->setBicFieldName('BIC')->setIbanFieldName('IBAN')->setAccountHolderFieldName('AccOwner')
			->setAccountHolderName($orderContext->getBillingFirstName() . ' ' . $orderContext->getBillingLastName());
		
		$elements = $builder->build();
		
		$elements[] = Customweb_Computop_ElementFactory::getBankNameElement('AccBank');
		
		if ($orderContext->getPaymentMethod()->existsPaymentMethodConfigurationValue('sepa_compliance_text', $orderContext->getLanguage())) {
			$text = $orderContext->getPaymentMethod()->getPaymentMethodConfigurationValue('sepa_compliance_text', $orderContext->getLanguage());
			if (!empty($text)) {
				$checkbox = new Customweb_Form_Control_SingleCheckbox("consent_text", "accepted", Customweb_I18n_Translation::__("I have read the agreement below and I agree with it."));
				$checkbox->addValidator(new Customweb_Form_Validator_Checked($checkbox, Customweb_I18n_Translation::__("You have to agree to the direct debit agreement.")));
				$element = new Customweb_Form_Element(Customweb_I18n_Translation::__("Direct Debit Agreement"), $checkbox);
				$element->setDescription($text);
				$elements[] = $element;
			}
		}
		
		return array_merge(
				parent::getVisibleFormFields($orderContext, $aliasTransaction, $failedTransaction, $paymentCustomerContext, $authorizationMethod),
				$elements
		);
	
	}
	
	public function validate(Customweb_Payment_Authorization_IOrderContext $orderContext, Customweb_Payment_Authorization_IPaymentCustomerContext $paymentContext, array $formData){
		if (!isset($formData['consent_text']) || $formData['consent_text'] != 'accepted') {
			throw new Exception(Customweb_I18n_Translation::__("You have to agree to the direct debit agreement."));
		}
		
	}
	
	public function processAuthorization(Customweb_Computop_Authorization_Transaction $transaction, array $httpRequestParameters = array()) {
		parent::processAuthorization($transaction, $httpRequestParameters);
		$transaction->setAuthorizationParameters(array_merge($transaction->getAuthorizationParameters(), array(
			'RPMethod' => 'ELV',
		)));
		
	}
	
	protected function getAuthorizeParameterBuilder(Customweb_Computop_Authorization_Transaction $transaction, array $httpRequestParameters) {
		return new Customweb_Computop_Processor_RatePay_DirectDebit_AuthorizeParameterBuilder($transaction, $this->getContainer(), $httpRequestParameters);
	}
	
	protected function getInitializeParameterBuilder(Customweb_Computop_Authorization_Transaction $transaction, array $httpRequestParameters) {
		return new Customweb_Computop_Processor_RatePay_DirectDebit_InitializeParameterBuilder($transaction, $this->getContainer());
	}
	
}