<?php
/**
  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2015 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */

//require_once 'Customweb/Computop/Method/DirectEbanking/LabelProvider.php';
//require_once 'Customweb/Payment/Exception/PaymentErrorException.php';
//require_once 'Customweb/Computop/Method/DefaultMethod.php';
//require_once 'Customweb/Payment/Authorization/ErrorMessage.php';
//require_once 'Customweb/Form/ElementFactory.php';
//require_once 'Customweb/I18n/Translation.php';
//require_once 'Customweb/Computop/Method/Util.php';


abstract class Customweb_Computop_Method_DirectEbanking_Abstract extends Customweb_Computop_Method_DefaultMethod {


	public function createTransaction(Customweb_Payment_Authorization_PaymentPage_ITransactionContext $transactionContext, $failedTransaction, $authorizationMethod) {
		$transaction = parent::createTransaction($transactionContext, $failedTransaction, $authorizationMethod);
		$transaction->registerLabelProvider(new Customweb_Computop_Method_DirectEbanking_LabelProvider($transaction));
		return $transaction;
	}
	
	
	public function processAuthorization(Customweb_Computop_Authorization_Transaction $transaction, array $httpRequestParameters = array()) {
		parent::processAuthorization($transaction, $httpRequestParameters);
		if ($transaction->isAuthorized() && isset($httpRequestParameters['SecCriteria']) && $httpRequestParameters['SecCriteria'] === '0') {
			$transaction->setAuthorizationUncertain(Customweb_I18n_Translation::__("Sofort does not guarantee the payment."));
		}
		if ($transaction->isAuthorized()) {
			$transaction->capture();
			$this->getDebitorManager()->processCapture($transaction);
		}
	}
	
	public function getVisibleFormFields(Customweb_Payment_Authorization_IOrderContext $orderContext, $aliasTransaction, $failedTransaction, $paymentCustomerContext, $authorizationMethod) {
		$elements = parent::getVisibleFormFields($orderContext, $aliasTransaction, $failedTransaction, $paymentCustomerContext, $authorizationMethod);
		if ($this->isSofortIdentActive($orderContext) && $orderContext->getBillingDateOfBirth() === null) {
			$elements[] = Customweb_Form_ElementFactory::getDateOfBirthElement('dobYear', 'dobMonth', 'dobDay');
		}
		
		return $elements;
	}
	
	public function getAuthorizationParameters(Customweb_Computop_Authorization_Transaction $transaction, array $httpRequestParameters = array()) {
		$parameters = parent::getAuthorizationParameters($transaction, $httpRequestParameters);
		
		$orderContext = $transaction->getTransactionContext()->getOrderContext();
		if (!in_array(strtoupper($orderContext->getBillingCountryIsoCode()), $this->getAllowedCountryCodes())) {
			throw new Customweb_Payment_Exception_PaymentErrorException(new Customweb_Payment_Authorization_ErrorMessage(Customweb_I18n_Translation::__("The billing country is not supported.")));	
		}
		$parameters['AddrCountryCode'] = $orderContext->getBillingCountryIsoCode();
		
		if ($this->isSofortIdentActive($orderContext)) {
			$parameters['Sofortaction'] = 'Ident';
			$parameters['FirstName'] = $orderContext->getBillingFirstName();
			$parameters['LastName'] = $orderContext->getBillingLastName();
			$parameters['AddrStreet'] = $orderContext->getBillingStreet();
			$parameters['AddrCity'] = $orderContext->getBillingCity();
			$parameters['AddrZip'] = $orderContext->getBillingPostCode();
			$parameters['Birthday'] = Customweb_Computop_Method_Util::getDateOfBirth($orderContext, $httpRequestParameters)->format('Ymd');
		}
		
		return $parameters;
	}
	
	protected function isSofortIdentActive(Customweb_Payment_Authorization_IOrderContext $orderContext) {
		
		if (strtoupper($orderContext->getBillingCountryIsoCode()) != 'DE') {
			return false;
		}
		
		if ($this->existsPaymentMethodConfigurationValue('sofort_ident') && $this->getPaymentMethodConfigurationValue('sofort_ident') == 'active') {
			return true;
		}
		else {
			return false;
		}
	}
	
	abstract protected function getAllowedCountryCodes();

	
	
}