<?php
/**
  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2015 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */

//require_once 'Customweb/Computop/Method/DirectEbanking/Abstract.php';


/**
 * This open invoice implementation does not use any processor. The transaction is threaded as a
 * regular order. No data is sent.
 * 
 * @author Thomas Hunziker
 * @Method(paymentMethods={'DirectEBanking'}, authorizationMethods={'PaymentPage'}, processors={'Direct'})
 *
 */
class Customweb_Computop_Method_DirectEbanking_Direct extends Customweb_Computop_Method_DirectEbanking_Abstract {

	protected function getAllowedCountryCodes() {
		return array(
			'DE', 'BE', 'AT', 'NL', 'FR', 'GB', 'IT', 'PL', 'ES', 'CH',
		);
	}
	
	public function getAuthorizationParameters(Customweb_Computop_Authorization_Transaction $transaction, array $httpRequestParameters = array()) {
		$parameters = parent::getAuthorizationParameters($transaction, $httpRequestParameters);
		$parameters['Language'] = $transaction->getTransactionContext()->getOrderContext()->getLanguage()->getIso2LetterCode();
		return $parameters;
	}
	
}