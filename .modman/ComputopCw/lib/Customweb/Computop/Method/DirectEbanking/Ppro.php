<?php
/**
  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2015 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */

//require_once 'Customweb/Computop/Method/DirectEbanking/Abstract.php';
//require_once 'Customweb/Payment/Exception/PaymentErrorException.php';
//require_once 'Customweb/Util/String.php';
//require_once 'Customweb/Payment/Authorization/ErrorMessage.php';
//require_once 'Customweb/I18n/Translation.php';
//require_once 'Customweb/Form/ElementFactory.php';


/**
 * This open invoice implementation does not use any processor. The transaction is threaded as a
 * regular order. No data is sent.
 * 
 * @author Thomas Hunziker
 * @Method(paymentMethods={'DirectEBanking'}, authorizationMethods={'PaymentPage'}, processors={'PPRO'})
 *
 */
class Customweb_Computop_Method_DirectEbanking_Ppro extends Customweb_Computop_Method_DirectEbanking_Abstract {

	public function getVisibleFormFields(Customweb_Payment_Authorization_IOrderContext $orderContext, $aliasTransaction, $failedTransaction, $paymentCustomerContext, $authorizationMethod) {
		$accountOwner = $orderContext->getBillingFirstName() . ' ' . $orderContext->getBillingLastName();
		$elements[] = Customweb_Form_ElementFactory::getAccountOwnerNameElement('AccOwner', $accountOwner);
		$elements[] = Customweb_Form_ElementFactory::getAccountNumberElement('AccNr');
		
		return $elements;
	}
	
	public function getAuthorizationParameters(Customweb_Computop_Authorization_Transaction $transaction, array $httpRequestParameters = array()) {
		$parameters = parent::getAuthorizationParameters($transaction, $httpRequestParameters);
		$orderContext = $transaction->getTransactionContext()->getOrderContext();
		
		if (empty($httpRequestParameters['AccNr'])) {
			throw new Customweb_Payment_Exception_PaymentErrorException(new Customweb_Payment_Authorization_ErrorMessage(Customweb_I18n_Translation::__("No bank account number provided.")));	
		}
		$parameters['AccNr'] = Customweb_Util_String::substrUtf8($httpRequestParameters['AccNr'], 0, 12);
		
		if (isset($httpRequestParameters['AccOwner'])) {
			$accountOwner = $httpRequestParameters['AccOwner'];
		}
		else {
			$accountOwner = $orderContext->getBillingFirstName() . ' ' . $orderContext->getBillingLastName();
		}
		$parameters['AccOwner'] = Customweb_Util_String::substrUtf8($httpRequestParameters['AccOwner'], 0, 50);
		
		return $parameters;
	}
	
	protected function getAllowedCountryCodes() {
		return array(
			'DE', 'BE', 'AT', 'NL', 'FR', 'GB', 'IT', 'PL', 'ES', 'CH',
		);
	}
	
}