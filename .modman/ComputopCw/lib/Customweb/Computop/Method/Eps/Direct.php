<?php
/**
  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2015 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */

//require_once 'Customweb/Core/String.php';
//require_once 'Customweb/Computop/Method/Eps/Abstract.php';
//require_once 'Customweb/Util/String.php';
//require_once 'Customweb/Core/Charset.php';


/** 
 * 
 * @author Thomas Hunziker
 * @Method(paymentMethods={'EPS'}, authorizationMethods={'PaymentPage', 'IFrameAuthorization'}, processors={'Direct'})
 * 
 */
class Customweb_Computop_Method_Eps_Direct extends Customweb_Computop_Method_Eps_Abstract {	
	
	public function getAuthorizationParameters(Customweb_Computop_Authorization_Transaction $transaction, array $httpRequestParameters = array()) {
		$parameters = parent::getAuthorizationParameters($transaction, $httpRequestParameters);
		$parameters['OrderDesc'] = Customweb_Util_String::substrUtf8($parameters['OrderDesc'], 0, 35);
		$parameters['OrderDesc2'] = substr($this->getDetailedOrderDescription($transaction->getTransactionContext()->getOrderContext()), 0, 384);
		
		return $parameters;
	}
	
	private function getDetailedOrderDescription(Customweb_Payment_Authorization_IOrderContext $orderContext){
		$lines = array();
		$items = $orderContext->getInvoiceItems();
		foreach($items as $item){
			$name = Customweb_Core_String::_($item->getName());
			$name->convertTo(Customweb_Core_Charset::forName('ASCII'));
			
			$lines[] = $item->getQuantity() . 'x ' . $name->__toString() . ' ' . $item->getAmountIncludingTax();
		}
	
		return implode(',', $lines);
	}
	
}