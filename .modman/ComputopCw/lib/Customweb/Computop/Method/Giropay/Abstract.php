<?php
/**
  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2015 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */

//require_once 'Customweb/Storage/IBackend.php';
//require_once 'Customweb/Computop/Method/Giropay/IssuerListRequest.php';
//require_once 'Customweb/Core/Exception/CastException.php';
//require_once 'Customweb/Computop/Method/DefaultMethod.php';
//require_once 'Customweb/Computop/Method/Util.php';
//require_once 'Customweb/Payment/Authorization/Method/Sepa/ElementBuilder.php';


abstract class Customweb_Computop_Method_Giropay_Abstract extends Customweb_Computop_Method_DefaultMethod {
	
	private static $issuerList = null;
	
	public function getVisibleFormFields(Customweb_Payment_Authorization_IOrderContext $orderContext, $aliasTransaction, $failedTransaction, $paymentCustomerContext, $authorizationMethod) {
		$builder = new Customweb_Payment_Authorization_Method_Sepa_ElementBuilder();
		$builder->setBicFieldName('BIC')->setIbanFieldName('IBAN')->setBicOptionList($this->getGiropayIssuerOptionList());
		
		return array_merge(
				parent::getVisibleFormFields($orderContext, $aliasTransaction, $failedTransaction, $paymentCustomerContext, $authorizationMethod),
				$builder->build()
		);
	}
	
	public function getAuthorizationParameters(Customweb_Computop_Authorization_Transaction $transaction, array $httpRequestParameters = array()) {
		$parameters = parent::getAuthorizationParameters($transaction, $httpRequestParameters);
		$orderContext = $transaction->getTransactionContext()->getOrderContext();
	
		return array_merge(
			$parameters,
			Customweb_Computop_Method_Util::getBicParameters($orderContext, $httpRequestParameters),
			Customweb_Computop_Method_Util::getIbanParameters($orderContext, $httpRequestParameters)
		);
	}
	
	protected function getGiropayIssuerOptionList() {
		if (self::$issuerList === null) {
			$storageBackend = $this->getContainer()->getBean('Customweb_Storage_IBackend');
			if (!($storageBackend instanceof Customweb_Storage_IBackend)) {
				throw new Customweb_Core_Exception_CastException('Customweb_Storage_IBackend');
			}
			
			$data = $storageBackend->read('giropay', 'issuer_list');
			if ($data === null || ($data['last_update'] + 3600) < time()) {
				$data['last_update'] = time();
				$request = new Customweb_Computop_Method_Giropay_IssuerListRequest($this->getGlobalConfiguration());
				$request->process();
				$data['list'] = $request->getIssuerList();
				$storageBackend->write('giropay', 'issuer_list', $data);
			}
			self::$issuerList = array();
			foreach ($data['list'] as $bic) {
				self::$issuerList[$bic] = $bic;
			}
		}
		return self::$issuerList;
	}
	

	public function processAuthorization(Customweb_Computop_Authorization_Transaction $transaction, array $httpRequestParameters = array()) {
		parent::processAuthorization($transaction, $httpRequestParameters);
		if ($transaction->isAuthorized()) {
			$transaction->capture();
			$this->getDebitorManager()->processCapture($transaction);
		}
	}

	public function capture(Customweb_Computop_Authorization_Transaction $transaction, array $items, $close) {
		throw new Exception("Capture is not supported by this payment method.");
	}
	
}
