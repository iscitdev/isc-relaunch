<?php
/**
  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2015 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */

//require_once 'Customweb/Computop/Method/Giropay/Abstract.php';
//require_once 'Customweb/I18n/Translation.php';


/**
 *
 * @author Thomas Hunziker
 * @Method(paymentMethods={'Giropay'}, authorizationMethods={'PaymentPage'}, processors={'Direct'})
 */
class Customweb_Computop_Method_Giropay_Direct extends Customweb_Computop_Method_Giropay_Abstract {

	public function getAuthorizationParameters(Customweb_Computop_Authorization_Transaction $transaction, array $httpRequestParameters = array()) {
		$parameters = parent::getAuthorizationParameters($transaction, $httpRequestParameters);
		$parameters['AutoStart'] = '1';
		return $parameters;
	}
	
	public function refund(Customweb_Computop_Authorization_Transaction $transaction, array $items, $close) {
		throw new Exception(Customweb_I18n_Translation::__("Refunds are not supported by the payment method."));
	}
	
}

