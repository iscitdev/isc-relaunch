<?php
/**
  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2015 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */

//require_once 'Customweb/Computop/Util.php';
//require_once 'Customweb/Computop/RemoteRequest/Abstract.php';


class Customweb_Computop_Method_Giropay_IssuerListRequest extends Customweb_Computop_RemoteRequest_Abstract {
	
	private $list = array();
	
	public function getIssuerList() {
		return $this->list;
	}
	
	protected function getRequestUrl() { 
		return $this->getConfiguration()->getBaseUrl() . 'giropayList.aspx';
	}
	
	protected function getRequestParameters() {
		$parameters = array(
			'MerchantID' => $this->getConfiguration()->getMerchantId(),
		);
		return Customweb_Computop_Util::encryptParameters($parameters, $this->getConfiguration());
	}
	
	protected function processResponse() {
		$responseParameters = $this->getResponseParameters();
		
		if (!isset($responseParameters['BICList'])) {
			throw new Exception("No BICList parameter returned by the giropayList service.");
		}		
		
		$this->list = explode(',', $responseParameters['BICList']);
	}
	
	
}