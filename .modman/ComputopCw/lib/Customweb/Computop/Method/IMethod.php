<?php 
/**
  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2015 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */

//require_once 'Customweb/Payment/Authorization/IPaymentMethodWrapper.php';


interface Customweb_Computop_Method_IMethod extends Customweb_Payment_Authorization_IPaymentMethodWrapper {
	
	/**
	 * 
	 * @param Customweb_Payment_Authorization_IOrderContext $orderContext
	 * @param Customweb_Payment_Authorization_IPaymentCustomerContext $paymentContext
	 * @throws Exception
	 * @return void
	 */
	public function validate(Customweb_Payment_Authorization_IOrderContext $orderContext, 
			Customweb_Payment_Authorization_IPaymentCustomerContext $paymentContext, array $formData);
	
	/**
	 * Returns the visible form fields that are needed for hidden and
	 * server authorization.
	 *
	 * @return Customweb_Form_IElement[] List of visible form fields for this payment method.
	 */
	public function getVisibleFormFields(Customweb_Payment_Authorization_IOrderContext $orderContext, $aliasTransaction, $failedTransaction, $paymentCustomerContext, $authorizationMethod);
	
	/**
	 * Creates a transaction object depending on the given payment method.
	 * 
	 * @param Customweb_Payment_Authorization_PaymentPage_ITransactionContext $transactionContext
	 * @param Customweb_Computop_Authorization_Transaction $failedTransaction
	 * @param string $authorizationMethod
	 * @return Customweb_Computop_Authorization_Transaction
	 */
	public function createTransaction(Customweb_Payment_Authorization_PaymentPage_ITransactionContext $transactionContext, $failedTransaction, $authorizationMethod);

	/**
	 * The capture mode of the payment method.
	 *
	 * @return string
	 */
	public function getCaptureMode(Customweb_Computop_Authorization_Transaction $transaction);
	
	/**
	 * This method returns the primary authorization URL.
	 * 
	 * @return string URL for authorization
	 */
	public function getAuthorizationUrl();
	
	/**
	 * Returns the payment method specific parameters to authorize the 
	 * transaction. Depending on the authorization method this method is used
	 * to generate the redirection or they are sent to the remote server.
	 * 
	 * @param Customweb_Payment_Authorization_ITransaction $transaction
	 * @param array $httpRequestParameters Parameters send in the HTTP request.
	 * @return array
	 */
	public function getAuthorizationParameters(Customweb_Computop_Authorization_Transaction $transaction, array $httpRequestParameters = array());
	
	/**
	 * This method process a given authorization. The method is called when the authorization is successful.
	 * 
	 * @param Customweb_Computop_Authorization_Transaction $transaction
	 * @param array $httpRequestParameters
	 * @return void
	 */
	public function processAuthorization(Customweb_Computop_Authorization_Transaction $transaction, array $httpRequestParameters = array());

	/**
	 * This content is show on the finalize page. If an empty string is returned, no content is shown.
	 *
	 * @param Customweb_Computop_Authorization_Transaction $transaction
	 * @return The content to show on finalize page.
	 */
	public function getFinalizeContent(Customweb_Computop_Authorization_Transaction $transaction);
	
	/**
	 * Returns true if the transaction is updateable. The transaction is marked
	 * to be checked on a regular base for updates.
	 *
	 * @return boolean
	 */
	public function isTransactionUpdateable();
	
	/**
	 * This method process a pull update of the given transaction. In case the payment method
	 * indicates it can handle updates, this method should be implemented.
	 *
	 * @return void
	 */
	public function processTransactionPullUpdate(Customweb_Computop_Authorization_Transaction $transaction);
	
	/**
	 * This method is called, when a transaction update is send over the notify URL by the
	 * Computop.
	 *
	 * @return void
	 */
	public function processTransactionUpdate(Customweb_Computop_Authorization_Transaction $transaction,  array $parameters);
	
	/**
	 * Refund the given line items on the given transaction.
	 * 
	 * @param Customweb_Computop_Authorization_Transaction $transaction
	 * @param array $items
	 */
	public function refund(Customweb_Computop_Authorization_Transaction $transaction, array $items, $close);
	

	/**
	 * Capture the given line items on the given transaction.
	 *
	 * @param Customweb_Computop_Authorization_Transaction $transaction
	 * @param array $items
	 */
	public function capture(Customweb_Computop_Authorization_Transaction $transaction, array $items, $close);
	
	/**
	 * Cancel the given transaction.
	 * 
	 * @param Customweb_Computop_Authorization_Transaction $transaction
	 */
	public function cancel(Customweb_Computop_Authorization_Transaction $transaction);
	
}