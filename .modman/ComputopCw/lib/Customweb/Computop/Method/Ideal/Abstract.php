<?php
/**
  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2015 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */

//require_once 'Customweb/Storage/IBackend.php';
//require_once 'Customweb/Core/Exception/CastException.php';
//require_once 'Customweb/Payment/Exception/PaymentErrorException.php';
//require_once 'Customweb/Computop/Method/DefaultMethod.php';
//require_once 'Customweb/Payment/Authorization/ErrorMessage.php';
//require_once 'Customweb/I18n/Translation.php';
//require_once 'Customweb/Computop/Method/Ideal/IssuerListRequest.php';
//require_once 'Customweb/Payment/Authorization/Method/Sepa/ElementBuilder.php';


abstract class Customweb_Computop_Method_Ideal_Abstract extends Customweb_Computop_Method_DefaultMethod {
	
	private static $issuerList = null;
	
	public function getVisibleFormFields(Customweb_Payment_Authorization_IOrderContext $orderContext, $aliasTransaction, $failedTransaction, $paymentCustomerContext, $authorizationMethod) {
		$builder = new Customweb_Payment_Authorization_Method_Sepa_ElementBuilder();
		$builder->setBicFieldName('IssuerID')->setBicOptionList($this->getIdealIssuerOptionList());
		
		return array_merge(
				parent::getVisibleFormFields($orderContext, $aliasTransaction, $failedTransaction, $paymentCustomerContext, $authorizationMethod),
				$builder->build()
		);
	}
	
	public function getAuthorizationParameters(Customweb_Computop_Authorization_Transaction $transaction, array $httpRequestParameters = array()) {
		$parameters = parent::getAuthorizationParameters($transaction, $httpRequestParameters);
	
		if (!isset($httpRequestParameters['IssuerID']) && empty($httpRequestParameters['IssuerID'])) {
			throw new Customweb_Payment_Exception_PaymentErrorException(new Customweb_Payment_Authorization_ErrorMessage(Customweb_I18n_Translation::__("No Issuer ID provided.")));
		}
		
		$options = $this->getIdealIssuerOptionList();
		if (!isset($options[$httpRequestParameters['IssuerID']])) {
			throw new Customweb_Payment_Exception_PaymentErrorException(new Customweb_Payment_Authorization_ErrorMessage(Customweb_I18n_Translation::__("Invalid Issuer ID provided.")));
		}
		$parameters['IssuerID'] = $httpRequestParameters['IssuerID'];
		
		return $parameters;
	}
	
	public function processAuthorization(Customweb_Computop_Authorization_Transaction $transaction, array $httpRequestParameters = array()) {
		parent::processAuthorization($transaction, $httpRequestParameters);
		if ($transaction->isAuthorized()) {
			$transaction->capture();
			$this->getDebitorManager()->processCapture($transaction);
		}
	}
	
	protected function getIdealIssuerOptionList() {
		if (self::$issuerList === null) {
			$storageBackend = $this->getContainer()->getBean('Customweb_Storage_IBackend');
			if (!($storageBackend instanceof Customweb_Storage_IBackend)) {
				throw new Customweb_Core_Exception_CastException('Customweb_Storage_IBackend');
			}
			
			$data = $storageBackend->read('ideal', 'issuer_list');
			if ($data === null || ($data['last_update'] + 3600) < time()) {
				$data['last_update'] = time();
				$request = new Customweb_Computop_Method_Ideal_IssuerListRequest($this->getGlobalConfiguration());
				$request->process();
				$data['list'] = $request->getIssuerList();
				$storageBackend->write('ideal', 'issuer_list', $data);
			}
			self::$issuerList = $data['list'];
		}
		return self::$issuerList;
	}

	public function capture(Customweb_Computop_Authorization_Transaction $transaction, array $items, $close) {
		throw new Exception("Capture is not supported by this payment method.");
	}
	
}
