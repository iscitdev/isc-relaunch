<?php
/**
  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2015 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */

//require_once 'Customweb/Util/String.php';
//require_once 'Customweb/Computop/Method/Ideal/Abstract.php';
//require_once 'Customweb/Form/ElementFactory.php';


/** 
 * 
 * @author Thomas Hunziker
 * @Method(paymentMethods={'iDEAL'}, authorizationMethods={'PaymentPage', 'IFrameAuthorization'}, processors={'PPRO'})
 * 
 */
class Customweb_Computop_Method_Ideal_Ppro extends Customweb_Computop_Method_Ideal_Abstract {
	
	public function getVisibleFormFields(Customweb_Payment_Authorization_IOrderContext $orderContext, $aliasTransaction, $failedTransaction, $paymentCustomerContext, $authorizationMethod) {
		$elements = parent::getVisibleFormFields($orderContext, $aliasTransaction, $failedTransaction, $paymentCustomerContext, $authorizationMethod);
	
		$accountOwner = $orderContext->getBillingFirstName() . ' ' . $orderContext->getBillingLastName();
		$elements[] = Customweb_Form_ElementFactory::getAccountOwnerNameElement('AccOwner', $accountOwner);
	
		return $elements;
	}
	
	public function getAuthorizationParameters(Customweb_Computop_Authorization_Transaction $transaction, array $httpRequestParameters = array()) {
		$parameters = parent::getAuthorizationParameters($transaction, $httpRequestParameters);
		$orderContext = $transaction->getTransactionContext()->getOrderContext();
	
		if (isset($httpRequestParameters['AccOwner'])) {
			$accountOwner = $httpRequestParameters['AccOwner'];
		}
		else {
			$accountOwner = $orderContext->getBillingFirstName() . ' ' . $orderContext->getBillingLastName();
		}
		$parameters['AccOwner'] = Customweb_Util_String::substrUtf8($httpRequestParameters['AccOwner'], 0, 50);
	
		return $parameters;
	}
	
}
