<?php

/**
 *  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2015 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */

//require_once 'Customweb/Computop/Util.php';
//require_once 'Customweb/Core/Assert.php';
//require_once 'Customweb/Computop/AbstractParameterBuilder.php';
//require_once 'Customweb/Computop/Method/Util.php';



class Customweb_Computop_Method_MasterPass_AuthorizationParameterBuilder extends Customweb_Computop_AbstractParameterBuilder{
	
	private $masterPassId;
	private $transactionId;
	
	public function __construct(Customweb_Computop_Authorization_Transaction $transaction, Customweb_DependencyInjection_IContainer $container, $masterPassId, $transactionId) {
		parent::__construct($transaction, $container);
		Customweb_Core_Assert::hasLength($masterPassId);
		Customweb_Core_Assert::hasLength($transactionId);
		$this->masterPassId = $masterPassId;
		$this->transactionId = $transactionId;
	}
	
	public function build() {
		$parameters = array_merge(
			$this->getBasicParameters(),
			$this->getAmountParameters(),
			$this->getRefNrParameters(),
			$this->getCaptureParameters(),
			$this->getDescriptionParameters(),
			$this->getMasterPassParameters()
		);
		
		$parameters = Customweb_Computop_Method_Util::addTestingParameters($parameters, $this->getTransaction(), $this->getConfiguration());
		
		return Customweb_Computop_Util::encryptParameters($parameters, $this->getConfiguration());
	}
	
	private function getRefNrParameters() {
		$refNr = '';
		if ($this->getTransaction()->getTransactionContext()->isOrderIdUnique()) {
			$refNr = $this->getTransaction()->getTransactionContext()->getOrderId();
		}
		if (empty($refNr)) {
			$refNr = $this->getTransaction()->getTransactionContext()->getTransactionId();
		}
		return array(
			'RefNr' => $refNr,
		);
	}
	
	private function getDescriptionParameters() {
		return array(
			'OrderDesc' => $this->getConfiguration()->getShopDescription($this->getTransaction()->getTransactionContext()->getOrderContext()->getLanguage()),
		);
	}
	
	private function getCaptureParameters() {
		$captureSetting = $this->getTransaction()->getPaymentMethod()->getPaymentMethodConfigurationValue('capturing');
		if ($captureSetting == 'direct') {
			$value = 'AUTO';
		}
		else {
			$value = 'MANUAL';
		}
		return array(
			'Capture' => $value,
		);
	}
	
	private function getMasterPassParameters() {
		return array(
			'TransactionID' => $this->transactionId,
			'MasterPassID' => $this->masterPassId,
		);
	}
	
}
	