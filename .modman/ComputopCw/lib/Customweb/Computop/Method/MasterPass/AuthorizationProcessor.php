<?php
/**
  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2015 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */

//require_once 'Customweb/Computop/Method/Default/AuthorizationProcessor/Abstract.php';
//require_once 'Customweb/Computop/RemoteRequest/Default.php';
//require_once 'Customweb/Computop/Method/MasterPass/AuthorizationParameterBuilder.php';


class Customweb_Computop_Method_MasterPass_AuthorizationProcessor extends Customweb_Computop_Method_Default_AuthorizationProcessor_Abstract {

	
	public function process() {
		$parameters = $this->getHttpRequestParameters();
		if (!isset($parameters['masterpassid'])) {
			throw new Exception("no parameter 'masterpassid' provided.");
		}
		if (!isset($parameters['transactionid'])) {
			throw new Exception("no parameter 'transactionid' provided.");
		}
		
		// Send Authorization Create Request
		$request = new Customweb_Computop_RemoteRequest_Default(
				$this->getConfiguration(),
				new Customweb_Computop_Method_MasterPass_AuthorizationParameterBuilder($this->getTransaction(), $this->getContainer(), $parameters['masterpassid'], $parameters['transactionid']),
				$this->getConfiguration()->getMasterPassAuthorizationUrl()
		);
		
		// Since there is a bug in the Computop we need to wait 
		// 32 seconds otherwise the transaction will be rejected.
		sleep(32);
		
		$request->process();
		$this->getTransaction()->setAuthorizationParameters(array_merge($this->getTransaction()->getAuthorizationParameters(), $request->getResponseParameters()));
		
		$this->processAuthorizationParameters();
		
	}
	
	
	
}