<?php
/**
  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2015 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */

//require_once 'Customweb/Computop/AbstractLineItemBuilder.php';
//require_once 'Customweb/Core/String.php';
//require_once 'Customweb/Util/String.php';
//require_once 'Customweb/Payment/Authorization/IInvoiceItem.php';
//require_once 'Customweb/Core/Charset.php';


class Customweb_Computop_Method_MasterPass_LineItemBuilder extends Customweb_Computop_AbstractLineItemBuilder {
	
	protected function getLineItemParts(Customweb_Payment_Authorization_IInvoiceItem $item) {
		$parts = array();
		
		$quantity = round($item->getQuantity());
		$parts[] = Customweb_Util_String::substrUtf8($this->cleanString($item->getName()), 0, 50);
		$parts[] = Customweb_Util_String::substrUtf8($quantity, 0, 7);
		$parts[] = $this->getProductPriceIncludingTax($item);
		return $parts;
	}
	
	protected function getAllowedProductTypes() {
		return array (
			Customweb_Payment_Authorization_IInvoiceItem::TYPE_PRODUCT,
			Customweb_Payment_Authorization_IInvoiceItem::TYPE_FEE,
			Customweb_Payment_Authorization_IInvoiceItem::TYPE_SHIPPING,
			Customweb_Payment_Authorization_IInvoiceItem::TYPE_DISCOUNT
		);
	}
	
	protected function cleanString($string) {
		$string = parent::cleanString($string);
		$output = new Customweb_Core_String($string);
		return $output->convertTo(Customweb_Core_Charset::forName('ASCII'))->toString();
	}
	
}