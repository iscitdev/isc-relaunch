<?php
/**
  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2015 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */

//require_once 'Customweb/Computop/Method/MasterPass/LineItemBuilder.php';
//require_once 'Customweb/Util/String.php';
//require_once 'Customweb/Computop/Method/DefaultMethod.php';
//require_once 'Customweb/Computop/Method/MasterPass/AuthorizationProcessor.php';


/** 
 * 
 * @author Thomas Hunziker
 * @Method(paymentMethods={'MasterPass'}, authorizationMethods={'PaymentPage'})
 */
class Customweb_Computop_Method_MasterPass_Method extends Customweb_Computop_Method_DefaultMethod{
	
	public function getAuthorizationParameters(Customweb_Computop_Authorization_Transaction $transaction, array $httpRequestParameters = array()) {
		$parameters = parent::getAuthorizationParameters($transaction, $httpRequestParameters);
			
		$orderContext = $transaction->getTransactionContext()->getOrderContext();
		
		$builder = new Customweb_Computop_Method_MasterPass_LineItemBuilder($orderContext);
		$parameters['ArticleList'] = $builder->build();
		

		$parameters['AcceptableCards'] = 'master,amex,diners,discover,maestro,visa';
		if ($this->existsPaymentMethodConfigurationValue('accepted_cards')) {
			$parameters['AcceptableCards'] = implode(',', $this->getPaymentMethodConfigurationValue('accepted_cards'));
		}
		
		$parameters['AuthLevelBasic'] = 'false';
		if ($this->existsPaymentMethodConfigurationValue('threed_check')) {
			if (strtolower($this->getPaymentMethodConfigurationValue('threed_check')) == 'basic') {
				$parameters['AuthLevelBasic'] = 'true';
			}
		}
		
		if ($this->existsPaymentMethodConfigurationValue('shipping_profile_id')) {
			$profileId = $this->getPaymentMethodConfigurationValue('shipping_profile_id');
			if (!empty($profileId)) {
				$parameters['ShippingProfile'] = $profileId;
			}
		}
		
		// Since we already collect the shipping address, we do not want to display it.
		$parameters['SuppressShippingAddress'] = 'true';
		
		$shippingAddress = $orderContext->getShippingAddress();
		
		$parameters['Name'] = Customweb_Util_String::substrUtf8($shippingAddress->getFirstName() . ' ' . $shippingAddress->getLastName(), 0, 100);
		$parameters['AddrStreet'] = Customweb_Util_String::substrUtf8($shippingAddress->getStreet(), 0, 40);
		$parameters['AddrCity'] = Customweb_Util_String::substrUtf8($shippingAddress->getCity(), 0, 25);
		$parameters['AddrZip'] = Customweb_Util_String::substrUtf8($shippingAddress->getPostCode(), 0, 20);
		$parameters['AddrCountryCode'] = $shippingAddress->getCountryIsoCode();
		if ($shippingAddress->getState() != null) {
			$parameters['AddrState'] = $shippingAddress->getState();
		}
		
		$parameters['ReqID'] = 'mp_c_' . time() . '__' . $transaction->getTransactionId();
		
		return $parameters;
	}
	
	protected function getAuthorizationProcessor(Customweb_Computop_Authorization_Transaction $transaction, array $httpRequestParameters = array()) {
		return new Customweb_Computop_Method_MasterPass_AuthorizationProcessor(
				$transaction,
				$this->getContainer(),
				$httpRequestParameters
		);
	}
	
}