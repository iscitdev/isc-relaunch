<?php
/**
  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2015 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */

//require_once 'Customweb/Util/Address.php';
//require_once 'Customweb/Computop/Processor/BillSafe/LineItemBuilder.php';
//require_once 'Customweb/Computop/Processor/BillSafe/ParameterBuilder/Capture.php';
//require_once 'Customweb/Util/String.php';
//require_once 'Customweb/Computop/Method/DefaultMethod.php';
//require_once 'Customweb/Util/Currency.php';
//require_once 'Customweb/Util/Invoice.php';
//require_once 'Customweb/Util/Country.php';
//require_once 'Customweb/Computop/Processor/BillSafe/ParameterBuilder/Cancel.php';
//require_once 'Customweb/I18n/Translation.php';
//require_once 'Customweb/Computop/Processor/BillSafe/ParameterBuilder/Refund.php';


/**
 * This open invoice implementation does not use any processor. The transaction is threaded as a
 * regular order. No data is sent.
 * 
 * @author Thomas Hunziker
 * @Method(paymentMethods={'OpenInvoice'}, authorizationMethods={'PaymentPage'}, processors={'BillSAFE'})
 *
 */
class Customweb_Computop_Method_OpenInvoice_BillSafe extends Customweb_Computop_Method_DefaultMethod{

	public function createTransaction(Customweb_Payment_Authorization_PaymentPage_ITransactionContext $transactionContext, $failedTransaction, $authorizationMethod) {
		$transaction = parent::createTransaction($transactionContext, $failedTransaction, $authorizationMethod);
		$transaction->setMultiCaptureEnabled(true);
		return $transaction;
	}
	
	public function preValidate(Customweb_Payment_Authorization_IOrderContext $orderContext,
			Customweb_Payment_Authorization_IPaymentCustomerContext $paymentContext) {
		parent::preValidate($orderContext, $paymentContext);
		$this->validateMinimalAge($orderContext->getBillingAddress()->getDateOfBirth());
	}
	
	public function getAuthorizationParameters(Customweb_Computop_Authorization_Transaction $transaction, array $httpRequestParameters = array()) {
		$parameters = parent::getAuthorizationParameters($transaction, $httpRequestParameters);
		
		$orderContext = $transaction->getTransactionContext()->getOrderContext();
	
		$parameters['EventToken'] = 'PO';
		$parameters['FirstName'] = Customweb_Util_String::formatString($orderContext->getBillingFirstName(), 0, 30);
		$parameters['LastName'] = Customweb_Util_String::formatString($orderContext->getBillingLastName(), 0, 30);

		if (strlen($orderContext->getBillingCompanyName()) > 0) {
			if ($this->isBusinessCustomerAllowed()) {
				$parameters['CompanyName'] = Customweb_Util_String::substrUtf8($orderContext->getBillingCompanyName(), 0, 50);
			}
			else if ($this->isBusinessCustomerDenied()) {
				throw new Exception(Customweb_I18n_Translation::__("Business customers are not allowed to order with this payment method."));
			}
		}
		$address = Customweb_Util_Address::splitStreet($orderContext->getBillingStreet(), $orderContext->getBillingCountryIsoCode(), $orderContext->getBillingPostCode());
		$parameters['AddrStreet'] = Customweb_Util_String::substrUtf8($address['street'], 0, 50);
		$parameters['AddrStreetNr'] = Customweb_Util_String::substrUtf8($address['street-number'], 0, 15);
	
		$parameters['AddrZip'] = Customweb_Util_String::substrUtf8($orderContext->getBillingPostCode(), 0, 5);
		$parameters['AddrCity'] = Customweb_Util_String::substrUtf8($orderContext->getBillingCity(), 0, 50);
		$parameters['AddrCountryCode'] = Customweb_Util_Country::getCountry3LetterCode($orderContext->getBillingCountryIsoCode());
		$parameters['Email'] = Customweb_Util_String::substrUtf8($orderContext->getCustomerEMailAddress(), 0, 100);
		
		if ($orderContext->getBillingGender() == 'female') {
			$parameters['Gender'] = 'w';
		}
		else if ($orderContext->getBillingGender() == 'male') {
			$parameters['Gender'] = 'm';
		}
		
		if ($orderContext->getBillingDateOfBirth() !== null) {
			$parameters['DateOfBirth'] = $orderContext->getBillingDateOfBirth()->format('Y-m-d');
		}
		
		$parameters['TaxAmount'] = Customweb_Util_Currency::formatAmount($this->getTotalTaxAmount($orderContext), $orderContext->getCurrencyCode(), '', '');
		
		$builder = new Customweb_Computop_Processor_BillSafe_LineItemBuilder($orderContext);
		$parameters['ArticleList'] = $builder->build();
		
		return $parameters;
	}
	
	protected function getTotalTaxAmount(Customweb_Payment_Authorization_IOrderContext $orderContext){
		return Customweb_Util_Invoice::getTotalTaxAmount($orderContext->getInvoiceItems());
	}
	
	
	/* Backend Operations */
	protected function getCancelParameterBuilder(Customweb_Computop_Authorization_Transaction $transaction) {
		return new Customweb_Computop_Processor_BillSafe_ParameterBuilder_Cancel($transaction, $this->getContainer());
	}
	
	protected function getRefundParameterBuilder(Customweb_Computop_Authorization_Transaction $transaction, array $items, $close) {
		return new Customweb_Computop_Processor_BillSafe_ParameterBuilder_Refund($transaction, $this->getContainer(), $items, $close);
	}
	
	protected function getCaptureParameterBuilder(Customweb_Computop_Authorization_Transaction $transaction, array $items, $close) {
		return new Customweb_Computop_Processor_BillSafe_ParameterBuilder_Capture($transaction, $this->getContainer(), $items, $close);
	}
	
	public function getAuthorizationUrl(){
		return 'billsafe.aspx';
	}
	
	
}