<?php
/**
  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2015 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */

//require_once 'Customweb/Computop/Processor/Klarna/ParameterBuilder/Capture.php';
//require_once 'Customweb/Util/String.php';
//require_once 'Customweb/Computop/ElementFactory.php';
//require_once 'Customweb/Computop/Method/DefaultMethod.php';
//require_once 'Customweb/Computop/Processor/Klarna/ParameterBuilder/Cancel.php';
//require_once 'Customweb/Util/Country.php';
//require_once 'Customweb/Form/ElementFactory.php';
//require_once 'Customweb/I18n/Translation.php';
//require_once 'Customweb/Computop/Method/Util.php';
//require_once 'Customweb/Computop/Processor/Klarna/ParameterBuilder/Refund.php';
//require_once 'Customweb/Util/Address.php';
//require_once 'Customweb/Computop/Processor/Klarna/LineItemBuilder.php';
//require_once 'Customweb/Core/Util/System.php';
//require_once 'Customweb/Computop/RemoteRequest/Default.php';
//require_once 'Customweb/Computop/Processor/Klarna/ParameterBuilder/Update.php';
//require_once 'Customweb/Util/Invoice.php';


/**
 * This open invoice implementation does not use any processor. The transaction is threaded as a
 * regular order. No data is sent.
 * 
 * @author Thomas Hunziker
 * @Method(paymentMethods={'OpenInvoice'}, authorizationMethods={'ServerAuthorization'}, processors={'Klarna'})
 *
 */
class Customweb_Computop_Method_OpenInvoice_Klarna extends Customweb_Computop_Method_DefaultMethod{

	public function createTransaction(Customweb_Payment_Authorization_PaymentPage_ITransactionContext $transactionContext, $failedTransaction, $authorizationMethod) {
		$transaction = parent::createTransaction($transactionContext, $failedTransaction, $authorizationMethod);
		$transaction->setMultiCaptureEnabled(true);
		return $transaction;
	}
	
	public function preValidate(Customweb_Payment_Authorization_IOrderContext $orderContext,
			Customweb_Payment_Authorization_IPaymentCustomerContext $paymentContext) {
		parent::preValidate($orderContext, $paymentContext);
		$this->validateMinimalAge($orderContext->getBillingAddress()->getDateOfBirth());
	}
	
	public function capture(Customweb_Computop_Authorization_Transaction $transaction, array $items, $close) {
		// Klarna requires first to update the line items to be updated, afterwards they 
		// can be captured. However the capture of the items can only be done once. Hence we 
		// capture them only when the transaction is closed. In addition we can only execute once
		// a reverse. Hence we do only do something when $close == true.
		if ($close) {
			
			// The final items are those, which should be charged finally.
			$finalItems = Customweb_Util_Invoice::addLineItems($transaction->getCapturedLineItems(), $items);
			
			// Update the line items.
			$request = new Customweb_Computop_RemoteRequest_Default(
				$this->getGlobalConfiguration(),
				new Customweb_Computop_Processor_Klarna_ParameterBuilder_Update($transaction, $this->getContainer(), $finalItems, true),
				$this->getGlobalConfiguration()->getCancellationUrl()
			);
			$request->process();
			
			// Execute the capture.
			$request = new Customweb_Computop_RemoteRequest_Default(
					$this->getGlobalConfiguration(),
					new Customweb_Computop_Processor_Klarna_ParameterBuilder_Capture($transaction, $this->getContainer(), $finalItems, true),
					$this->getGlobalConfiguration()->getCaptureUrl()
			);
			$request->process();
			$captureItem = $transaction->partialCaptureByLineItems($items, $close);
			$this->processCapture($captureItem, $request);
			
			$params = $request->getResponseParameters();
			if (isset($params['invno'])) {
				$transaction->setInvoiceNumber($params['invno']);
			}
		}
		else {
			$transaction->partialCaptureByLineItems($items, $close);
		}
	}
	
	
	
	public function getVisibleFormFields(Customweb_Payment_Authorization_IOrderContext $orderContext, $aliasTransaction, $failedTransaction, $paymentCustomerContext, $authorizationMethod) {
		$elements = parent::getVisibleFormFields($orderContext, $aliasTransaction, $failedTransaction, $paymentCustomerContext, $authorizationMethod);
		
		/* @var $paymentCustomerContext Customweb_Payment_Authorization_IPaymentCustomerContext */
		$contextMap = $paymentCustomerContext->getMap();
		
		if($orderContext->getBillingMobilePhoneNumber() == null){
			$default = $orderContext->getBillingPhoneNumber();
			if (isset($contextMap['MobileNr'])) {
				$default = $contextMap['MobileNr'];
			}
			$elements[] = Customweb_Computop_ElementFactory::getMobilePhoneNumberElement('MobileNr', $default);
		}
	
		if(strlen($orderContext->getBillingCompanyName()) > 0 && $this->isBusinessCustomerAllowed()){
			if($orderContext->getBillingCommercialRegisterNumber() == null){
				$default = null;
				if (isset($contextMap['CommercialNumber'])) {
					$default = $contextMap['CommercialNumber'];
				}
				
				// The social security number field is reussed for the commercial register number
				$elements[] = Customweb_Form_ElementFactory::getCommercialNumberElement('CommercialNumber', $default);
			}
		}
		else{
			if($this->isSsnNeeded($orderContext) && strlen($orderContext->getBillingSocialSecurityNumber()) <= 0){
				$default = null;
				if (isset($contextMap['SocialSecurityNumber'])) {
					$default = $contextMap['SocialSecurityNumber'];
				}
				$elements[] = Customweb_Computop_ElementFactory::getSocialSecurityNumberElement('SocialSecurityNumber', $default);
			}
			
			if($orderContext->getBillingDateOfBirth() === null){
				$defaultDobYear = null;
				if (isset($contextMap['dobYear'])) {
					$defaultDobYear = $contextMap['dobYear'];
				}
				$defaultDobMonth = null;
				if (isset($contextMap['dobMonth'])) {
					$defaultDobMonth = $contextMap['dobMonth'];
				}
				$defaultDobDay = null;
				if (isset($contextMap['dobDay'])) {
					$defaultDobDay = $contextMap['dobDay'];
				}
				$startAge = 0;
				if ($this->existsPaymentMethodConfigurationValue('minimal_age') && $this->getPaymentMethodConfigurationValue('minimal_age') != 'none') {
					$startAge = (int)$this->getPaymentMethodConfigurationValue('minimal_age');
				}
				$elements[] = Customweb_Form_ElementFactory::getDateOfBirthElement('dobYear', 'dobMonth', 'dobDay', $defaultDobYear, $defaultDobMonth, $defaultDobDay, null, $startAge);
			}
			
			$billingGender = $orderContext->getBillingGender();
			if(empty($billingGender)){
				$default = null;
				if (isset($contextMap['billingGender'])) {
					$default = $contextMap['billingGender'];
				}
				$elements[] = Customweb_Computop_ElementFactory::getGenderElement(
					'billingGender', null,
					Customweb_I18n_Translation::__('Billing Address Gender'),
					Customweb_I18n_Translation::__('Please select your gender for the billing address.'),
					$default
				);
			}
		}
	
		return $elements;
	}
	
	
	public function getAuthorizationParameters(Customweb_Computop_Authorization_Transaction $transaction, array $httpRequestParameters = array()) {
		$parameters = parent::getAuthorizationParameters($transaction, $httpRequestParameters);
		
		$orderContext = $transaction->getTransactionContext()->getOrderContext();
		
		
		$parameters['IPAddr'] = Customweb_Core_Util_System::getClientIPAddress();
		
		if (strlen($orderContext->getBillingCompanyName()) > 0 && $this->isBusinessCustomerDenied()) {
			throw new Exception("Company addresses are not allowed.");
		}
		
		if (strlen($orderContext->getBillingCompanyName()) > 0 && $this->isBusinessCustomerAllowed()) {
			$parameters['CompanyOrPerson'] = 'F';
			
			$commercialNumber = $orderContext->getBillingCommercialRegisterNumber();
			if(empty($commercialNumber) && isset($httpRequestParameters['CommercialNumber'])){
				$commercialNumber = $httpRequestParameters['CommercialNumber'];
				$transaction->getTransactionContext()->getPaymentCustomerContext()->updateMap(array(
					'commercialNumber' => $httpRequestParameters['commercialNumber'],
				));
				
			}
			
			if (empty($commercialNumber)) {
				throw new Exception("The commercial register number is required.");
			}
			
			// The specification requires that 'SocialSecurityNumber' contains the commercial number,
			// however the given length is wrong. 5 chars is too short. Since this we assume 100 chars (billPay).
			$parameters['SocialSecurityNumber'] = Customweb_Util_String::substrUtf8($commercialNumber, 0, 100);
				
		}
		else {
			$parameters['CompanyOrPerson'] = 'P';
			$parameters['DateOfBirth'] = Customweb_Computop_Method_Util::getDateOfBirth($orderContext, $httpRequestParameters)->format('Y-m-d');
			
			if(isset($httpRequestParameters['dobYear'])) {
				$transaction->getTransactionContext()->getPaymentCustomerContext()->updateMap(array(
					'dobYear' => $httpRequestParameters['dobYear'],
				));
			}
			
			if(isset($httpRequestParameters['dobMonth'])) {
				$transaction->getTransactionContext()->getPaymentCustomerContext()->updateMap(array(
					'dobMonth' => $httpRequestParameters['dobMonth'],
				));
			}
			
			if(isset($httpRequestParameters['dobDay'])) {
				$transaction->getTransactionContext()->getPaymentCustomerContext()->updateMap(array(
					'dobDay' => $httpRequestParameters['dobDay'],
				));
			}
			
			$gender = Customweb_Computop_Method_Util::getBillingGender($orderContext, $httpRequestParameters);
			if ($gender == 'female') {
				$parameters['Gender'] = 'f';
			}
			else {
				$parameters['Gender'] = 'm';
			}

			if(isset($httpRequestParameters['billingGender'])) {
				$transaction->getTransactionContext()->getPaymentCustomerContext()->updateMap(array(
					'billingGender' => $httpRequestParameters['billingGender'],
				));
			}
			
			if($this->isSsnNeeded($orderContext)) {
				$number = $orderContext->getBillingSocialSecurityNumber();
				if (empty($number) && isset($httpRequestParameters['SocialSecurityNumber'])) {
					$number = $httpRequestParameters['SocialSecurityNumber'];
					$transaction->getTransactionContext()->getPaymentCustomerContext()->updateMap(array(
						'SocialSecurityNumber' => $httpRequestParameters['SocialSecurityNumber'],
					));
				}
				
				if (empty($number)) {
					throw new Exception("Social security number is required.");
				}
				
				$parameters['SocialSecurityNumber'] = Customweb_Util_String::substrUtf8($number, 0, 5);
			}
			
		}
		
		$parameters['KlarnaAction'] = '-1';
		
		$invoiceFlag = '0';
		if ($this->existsPaymentMethodConfigurationValue('invoice_delivery')) {
			$option = $this->getPaymentMethodConfigurationValue('invoice_delivery');
			if ($option == 'email') {
				$invoiceFlag = '8';
			}
			else if ($option == 'post') {
				$invoiceFlag = '4';
			}
		}
		
		$parameters['InvoiceFlag'] = $invoiceFlag;
		$parameters['MobileNr'] = $this->getMobilePhoneNumber($orderContext, $httpRequestParameters);

		if(isset($httpRequestParameters['MobileNr'])) {
			$transaction->getTransactionContext()->getPaymentCustomerContext()->updateMap(array(
				'MobileNr' => $httpRequestParameters['MobileNr'],
			));
		}
		
		$builder = new Customweb_Computop_Processor_Klarna_LineItemBuilder($orderContext);
		$parameters['OrderDesc'] = $builder->build();
		
		return array_merge($parameters, $this->getAdressesParameters($orderContext));
	}
	
	
	private function isSsnNeeded($orderContext){
		$countriesThatNeedSsn = array('SE', 'FI', 'DK', 'NO');
		return in_array($orderContext->getBillingCountryIsoCode(),$countriesThatNeedSsn);
	}
	
	private function getMobilePhoneNumber(Customweb_Payment_Authorization_IOrderContext $orderContext, array $parameters){
		$number = $orderContext->getBillingMobilePhoneNumber();
		if(empty($number) && isset($parameters['MobileNr'])){
			$number = $parameters['MobileNr'];
		}
		
		if (empty($number)){
			throw new Exception("The mobile phone number is required.");
		}
		return Customweb_Util_String::substrUtf8($number, 0, 20);
	}
	
	private function getAdressesParameters(Customweb_Payment_Authorization_IOrderContext $orderContext){
		$parameters = array();
		
		// Billing address parameters
		$parameters['bdFirstName'] = Customweb_Util_String::substrUtf8($orderContext->getBillingFirstName(), 0, 30);
		$parameters['bdLastName'] = Customweb_Util_String::substrUtf8($orderContext->getBillingLastName(), 0, 30);
		
		$address = Customweb_Util_Address::splitStreet($orderContext->getBillingStreet(), $orderContext->getBillingCountryIsoCode(), $orderContext->getBillingPostCode());
		$parameters['bdStreet'] = Customweb_Util_String::substrUtf8($address['street'], 0, 32);
		$parameters['bdStreetNr'] = Customweb_Util_String::substrUtf8($address['street-number'], 0, 5);
		$parameters['bdZip'] = Customweb_Util_String::substrUtf8($orderContext->getBillingPostCode(), 0, 5);
		$parameters['bdCity'] = Customweb_Util_String::substrUtf8($orderContext->getBillingCity(), 0, 32);
		$parameters['bdCountryCode'] = Customweb_Util_Country::getCountry3LetterCode($orderContext->getBillingCountryIsoCode());
		$parameters['Email'] = Customweb_Util_String::substrUtf8($orderContext->getBillingEMailAddress(), 0, 80);
	
	
		// Shipping address parameters
		$parameters['sdFirstName'] = Customweb_Util_String::substrUtf8($orderContext->getShippingFirstName(), 0, 30);
		$parameters['sdLastName'] = Customweb_Util_String::substrUtf8($orderContext->getShippingLastName(), 0, 30);
			
		$address = Customweb_Util_Address::splitStreet($orderContext->getShippingStreet(), $orderContext->getShippingCountryIsoCode(), $orderContext->getShippingPostCode());
		$parameters['sdStreet'] =  Customweb_Util_String::substrUtf8($address['street'], 0, 32);
		$parameters['sdStreetNr'] = Customweb_Util_String::substrUtf8($address['street-number'], 0, 5);
		$parameters['sdZip'] = Customweb_Util_String::substrUtf8($orderContext->getShippingPostCode(), 0, 5);
		$parameters['sdCity'] = Customweb_Util_String::substrUtf8($orderContext->getShippingCity(), 0, 32);
		$parameters['sdCountryCode'] = Customweb_Util_Country::getCountry3LetterCode($orderContext->getShippingCountryIsoCode());
	
		return $parameters;
	}

	public function getAuthorizationUrl(){
		return 'klarna.aspx';
	}
	
	
	/* Backend Operations */

	protected function getCancelParameterBuilder(Customweb_Computop_Authorization_Transaction $transaction) {
		return new Customweb_Computop_Processor_Klarna_ParameterBuilder_Cancel($transaction, $this->getContainer());
	}
	
	protected function getRefundParameterBuilder(Customweb_Computop_Authorization_Transaction $transaction, array $items, $close) {
		return new Customweb_Computop_Processor_Klarna_ParameterBuilder_Refund($transaction, $this->getContainer(), $items, $close);
	}
	
	
}