<?php
/**
  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2015 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */

//require_once 'Customweb/Computop/Method/DefaultMethod.php';


/**
 * This open invoice implementation does not use any processor. The transaction is threaded as a
 * regular order. No data is sent.
 * 
 * @author Thomas Hunziker
 * @Method(paymentMethods={'OpenInvoice'}, authorizationMethods={'ServerAuthorization'}, processors={'None'})
 *
 */
class Customweb_Computop_Method_OpenInvoice_None extends Customweb_Computop_Method_DefaultMethod {
	
	public function processAuthorization(Customweb_Computop_Authorization_Transaction $transaction, array $httpRequestParameters = array()) {
		$transaction->authorize();
		$this->getDebitorManager()->processAuthorize($transaction);
	}

	public function capture(Customweb_Computop_Authorization_Transaction $transaction, array $items, $close) {
		$transaction->partialCaptureByLineItems($items, $close);
	}
	
	public function refund(Customweb_Computop_Authorization_Transaction $transaction, array $items, $close) {
		$transaction->refundByLineItems($items, $close);
	}
	
	public function cancel(Customweb_Computop_Authorization_Transaction $transaction) {
		$transaction->cancel();
	}
		
	public function preValidate(Customweb_Payment_Authorization_IOrderContext $orderContext,
			Customweb_Payment_Authorization_IPaymentCustomerContext $paymentContext) {
		parent::preValidate($orderContext, $paymentContext);
		$this->validateMinimalAge($orderContext->getBillingAddress()->getDateOfBirth());
	}
	
	
}