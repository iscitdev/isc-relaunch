<?php
/**
  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2015 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */

//require_once 'Customweb/Computop/Processor/Paymorrow/OpenInvoice/AuthorizeParameterBuilder.php';
//require_once 'Customweb/Computop/Processor/Paymorrow/AbstractMethod.php';


/**
 * This open invoice implementation does not use any processor. The transaction is threaded as a
 * regular order. No data is sent.
 * 
 * @author Thomas Hunziker
 * @Method(paymentMethods={'OpenInvoice'}, authorizationMethods={'ServerAuthorization'}, processors={'Paymorrow'})
 *
 */
class Customweb_Computop_Method_OpenInvoice_Paymorrow extends Customweb_Computop_Processor_Paymorrow_AbstractMethod {

	protected function getAuthorizeParameterBuilder(Customweb_Computop_Authorization_Transaction $transaction, array $httpRequestParameters) {
		return new Customweb_Computop_Processor_Paymorrow_OpenInvoice_AuthorizeParameterBuilder($transaction, $this->getContainer(), $httpRequestParameters);
	}

	public function preValidate(Customweb_Payment_Authorization_IOrderContext $orderContext,
			Customweb_Payment_Authorization_IPaymentCustomerContext $paymentContext) {
		parent::preValidate($orderContext, $paymentContext);
		$this->validateMinimalAge($orderContext->getBillingAddress()->getDateOfBirth());
	}
	
}