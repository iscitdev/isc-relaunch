<?php
/**
  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2015 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */

//require_once 'Customweb/Computop/Method/DefaultMethod.php';
//require_once 'Customweb/Computop/Method/PayPal/ParameterBuilder/Refund.php';
//require_once 'Customweb/Computop/Method/PayPal/ParameterBuilder/Capture.php';


/** 
 * 
 * @author Thomas Hunziker
 * @Method(paymentMethods={'PayPal'}, authorizationMethods={'PaymentPage'})
 */
class Customweb_Computop_Method_PayPal_Method extends Customweb_Computop_Method_DefaultMethod{
	
	public function getAuthorizationParameters(Customweb_Computop_Authorization_Transaction $transaction, array $httpRequestParameters = array()) {
		$parameters = parent::getAuthorizationParameters($transaction, $httpRequestParameters);
			
		$orderContext = $transaction->getTransactionContext()->getOrderContext();
		if($parameters['Capture'] == 'MANUAL' && $this->existsPaymentMethodConfigurationValue('paypal_transaction_type')) {
			$parameters['TxType'] = $this->getPaymentMethodConfigurationValue('paypal_transaction_type');
		}
		
		$companyName = $orderContext->getShippingCompanyName();
		if (!empty($companyName)) {
			$parameters['FirstName'] = $orderContext->getShippingFirstName() . ' ' . $orderContext->getShippingLastName();
			$parameters['LastName'] = $companyName;
		}
		else {
			$parameters['FirstName'] = $orderContext->getShippingFirstName();
			$parameters['LastName'] = $orderContext->getShippingLastName();
		}
		$parameters['AddrStreet'] = $orderContext->getShippingStreet();
		$parameters['AddrCity'] = $orderContext->getShippingCity();
		$state = $orderContext->getShippingState();
		if (!empty($state)) {
			$parameters['AddrState'] = $orderContext->getShippingState();
		}
		$parameters['AddrZIP'] = $orderContext->getShippingPostCode();
		$parameters['AddrCountryCode'] = $orderContext->getShippingCountryIsoCode();
		
		$phone = $orderContext->getShippingPhoneNumber();
		if (!empty($phone)) {
			$parameters['Phone'] = $phone;
		}
		
		$parameters['Language'] = $orderContext->getLanguage()->getIetfCode();
		
		$parameters['NoShipping'] = '1';
		$parameters['AllowNote'] = 'no';
		
		return $parameters;
	}
	
	protected function processCapture(Customweb_Computop_Authorization_TransactionCapture $captureItem, Customweb_Computop_RemoteRequest_Default $request) {
		parent::processCapture($captureItem, $request);
		
		$params = $request->getResponseParameters();
		if (isset($params['TID'])) {
			$captureItem->setPayPalTransactionId($params['TID']);
		}
	}
	
	public function refund(Customweb_Computop_Authorization_Transaction $transaction, array $items, $close) {
		return $this->refundByCapture($transaction, $items, $close);
	}
	
	protected function getCaptureReferenceRefundParameterBuilder(Customweb_Computop_Authorization_Transaction $transaction, $items, $close, Customweb_Computop_Authorization_TransactionCapture $referenceCapture) {
		return new Customweb_Computop_Method_PayPal_ParameterBuilder_Refund($transaction, $this->getContainer(), $items, $close, $referenceCapture);
	}
	
	protected function getCaptureParameterBuilder(Customweb_Computop_Authorization_Transaction $transaction, array $items, $close) {
		return new Customweb_Computop_Method_PayPal_ParameterBuilder_Capture($transaction, $this->getContainer(), $items, $close);
	}
	
}