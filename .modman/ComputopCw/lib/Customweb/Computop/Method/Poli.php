<?php
/**
  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2015 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */

//require_once 'Customweb/Util/String.php';
//require_once 'Customweb/Computop/Method/DefaultMethod.php';
//require_once 'Customweb/Form/ElementFactory.php';
//require_once 'Customweb/I18n/Translation.php';


/** 
 * 
 * @author Thomas Hunziker
 * @Method(paymentMethods={'Poli'}, authorizationMethods={'PaymentPage'})
 */
class Customweb_Computop_Method_Poli extends Customweb_Computop_Method_DefaultMethod{

	public function preValidate(Customweb_Payment_Authorization_IOrderContext $orderContext, 
			Customweb_Payment_Authorization_IPaymentCustomerContext $paymentContext) {
		$allowedCountries = array(
			'AU', 'NZ',
		);
	
		if (!in_array($orderContext->getBillingCountryIsoCode(), $allowedCountries)) {
			throw new Exception(Customweb_I18n_Translation::__("Country is not allowed for this payment method."));
		}
	
		return parent::preValidate($orderContext, $paymentContext);
	}
	
	public function getVisibleFormFields(Customweb_Payment_Authorization_IOrderContext $orderContext, $aliasTransaction, $failedTransaction, $paymentCustomerContext, $authorizationMethod) {
		$this->validate($orderContext, $paymentCustomerContext);
		$elements = parent::getVisibleFormFields($orderContext, $aliasTransaction, $failedTransaction, $paymentCustomerContext, $authorizationMethod);
		$accountOwner = $orderContext->getBillingFirstName() . ' ' . $orderContext->getBillingLastName();
		$elements[] = Customweb_Form_ElementFactory::getAccountOwnerNameElement('AccOwner', $accountOwner);
		
		return $elements;
	}
	
	public function getAuthorizationParameters(Customweb_Computop_Authorization_Transaction $transaction, array $httpRequestParameters = array()) {
		$parameters = parent::getAuthorizationParameters($transaction, $httpRequestParameters);
		$orderContext = $transaction->getTransactionContext()->getOrderContext();
		
		$this->preValidate($orderContext, $transaction->getPaymentCustomerContext());

		if (isset($httpRequestParameters['AccOwner'])) {
			$parameters['AccOwner'] = $httpRequestParameters['AccOwner'];
		}
		else {
			$parameters['AccOwner'] = $orderContext->getBillingFirstName() . ' ' . $orderContext->getBillingLastName();
		}
		$parameters['AccOwner'] = Customweb_Util_String::substrUtf8($parameters['AccOwner'], 0, 50);

		$parameters['AddrCountryCode'] = $orderContext->getBillingCountryIsoCode();
		
		return $parameters;
	}
}