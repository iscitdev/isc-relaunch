<?php
/**
  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2015 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */

//require_once 'Customweb/Computop/Method/PostPay/ParameterBuilder/Capture.php';
//require_once 'Customweb/Computop/Method/DefaultMethod.php';
//require_once 'Customweb/Computop/Util.php';
//require_once 'Customweb/Computop/Method/PostPay/ParameterBuilder/Refund.php';
//require_once 'Customweb/Payment/Authorization/IInvoiceItem.php';
//require_once 'Customweb/Computop/Method/PostPay/LineItemBuilder.php';
//require_once 'Customweb/Computop/Method/PostPay/ParameterBuilder/Cancel.php';


/** 
 * 
 * @author Thomas Hunziker
 * @ Method(paymentMethods={'PostPay'}, authorizationMethods={'PaymentPage'})
 */
class Customweb_Computop_Method_PostPay_Method extends Customweb_Computop_Method_DefaultMethod{
	
	public function getAuthorizationParameters(Customweb_Computop_Authorization_Transaction $transaction, array $httpRequestParameters = array()) {
		$parameters = parent::getAuthorizationParameters($transaction, $httpRequestParameters);
		
		$orderContext = $transaction->getTransactionContext()->getOrderContext();
		
		$shippingAmount = 0;
		$articelList = array();
		
		foreach($orderContext->getInvoiceItems() as $item){
			if($item->getType() == Customweb_Payment_Authorization_IInvoiceItem::TYPE_SHIPPING){
				$shippingAmount += $item->getAmountIncludingTax();
			}
		}
		
		$builder = new Customweb_Computop_Method_PostPay_LineItemBuilder($orderContext);
		$parameters['ArticleList'] = $builder->build();
		$parameters['shAmount'] =  Customweb_Computop_Util::formatAmount(
				$shippingAmount,
				$orderContext->getCurrencyCode()
		);
	
		return $parameters;
	}
	
	protected function getCancelParameterBuilder(Customweb_Computop_Authorization_Transaction $transaction) {
		return new Customweb_Computop_Method_PostPay_ParameterBuilder_Cancel($transaction, $this->getContainer());
	}
	
	protected function getRefundParameterBuilder(Customweb_Computop_Authorization_Transaction $transaction, array $items, $close) {
		return new Customweb_Computop_Method_PostPay_ParameterBuilder_Refund($transaction, $this->getContainer(), $items, $close);
	}
	
	protected function getCaptureParameterBuilder(Customweb_Computop_Authorization_Transaction $transaction, array $items, $close) {
		return new Customweb_Computop_Method_PostPay_ParameterBuilder_Capture($transaction, $this->getContainer(), $items, $close);
	}
	
}