<?php 
/**
  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2015 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */

//require_once 'Customweb/I18n/Translation.php';
//require_once 'Customweb/Computop/Method/Default/CancelParameterBuilder.php';


class Customweb_Computop_Method_PostPay_ParameterBuilder_Cancel extends Customweb_Computop_Method_Default_CancelParameterBuilder {
	
	protected function buildInner() {
		$parameters = parent::buildInner();
		
		$parameters['Reason'] = 'DealerRequest';
		$parameters['Note'] = Customweb_I18n_Translation::__("Cancel of the order.");
		
		return $parameters;
	}
	
}