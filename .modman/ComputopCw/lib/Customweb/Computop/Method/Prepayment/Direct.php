<?php
/**
  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2015 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */

//require_once 'Customweb/Computop/ElementFactory.php';
//require_once 'Customweb/Util/String.php';
//require_once 'Customweb/Computop/Method/DefaultMethod.php';
//require_once 'Customweb/Util/Country.php';
//require_once 'Customweb/Form/Element.php';
//require_once 'Customweb/I18n/Translation.php';
//require_once 'Customweb/Form/ElementFactory.php';
//require_once 'Customweb/Computop/Method/Util.php';
//require_once 'Customweb/Util/Address.php';
//require_once 'Customweb/Form/Validator/NotEmpty.php';
//require_once 'Customweb/Form/Control/TextInput.php';
//require_once 'Customweb/Form/Intention/Factory.php';


/**
 * @author Thomas Hunziker
 * @Method(paymentMethods={'Prepayment'}, authorizationMethods={'ServerAuthorization'}, processors={'Direct'})
 *
 */
class Customweb_Computop_Method_Prepayment_Direct extends Customweb_Computop_Method_DefaultMethod {
	
	public function getVisibleFormFields(Customweb_Payment_Authorization_IOrderContext $orderContext, $aliasTransaction, $failedTransaction, $paymentCustomerContext, $authorizationMethod) {
		$elements = parent::getVisibleFormFields($orderContext, $aliasTransaction, $failedTransaction, $paymentCustomerContext, $authorizationMethod);
		$accountGender = $orderContext->getBillingGender();
		$elements[] = Customweb_Computop_ElementFactory::getGenderElement(
				'AccountOwnerGender', null,
				Customweb_I18n_Translation::__('Gender of Account Owner'),
				Customweb_I18n_Translation::__('Please select the gender of the account holder.')
		);
		
		$elements[] = $this->getAccountOwnerFirstNameElement($orderContext);
		$elements[] = $this->getAccountOwnerLastNameElement($orderContext);
		$elements[] = Customweb_Form_ElementFactory::getAccountNumberElement('AccNr');
		$elements[] = Customweb_Form_ElementFactory::getBankCodeElement('AccIBAN');
		$elements[] = Customweb_Computop_ElementFactory::getPlaceOfBirthElement('PlaceOfBirth');
		$elements[] = $this->getTranferMethodElement();
		
		return $elements;
	}
	
	public function getAuthorizationParameters(Customweb_Computop_Authorization_Transaction $transaction, array $httpRequestParameters = array()) {
		$parameters = parent::getAuthorizationParameters($transaction, $httpRequestParameters);
		$orderContext = $transaction->getTransactionContext()->getOrderContext();

		if (!isset($httpRequestParameters['AccountOwnerFirstName']) || empty($httpRequestParameters['AccountOwnerFirstName'])) {
			throw new Exception(Customweb_I18n_Translation::__("No account owner first name provided."));
		}

		if (!isset($httpRequestParameters['AccountOwnerLastName']) || empty($httpRequestParameters['AccountOwnerLastName'])) {
			throw new Exception(Customweb_I18n_Translation::__("No account owner last name provided."));
		}
		
		if (!isset($httpRequestParameters['AccNr']) || empty($httpRequestParameters['AccNr'])) {
			throw new Exception(Customweb_I18n_Translation::__("No bank account number provided."));
		}

		if (!isset($httpRequestParameters['AccIBAN']) || empty($httpRequestParameters['AccIBAN'])) {
			throw new Exception(Customweb_I18n_Translation::__("No bank code provided."));
		}

		if (!isset($httpRequestParameters['AccountOwnerGender']) || empty($httpRequestParameters['AccountOwnerGender'])) {
			throw new Exception(Customweb_I18n_Translation::__("No account owner gender provided."));
		}

		if (!isset($httpRequestParameters['PlaceOfBirth']) || empty($httpRequestParameters['PlaceOfBirth'])) {
			throw new Exception(Customweb_I18n_Translation::__("No place of birth provided."));
		}

		if (isset($httpRequestParameters['TransactionMethod']) && $httpRequestParameters['TransactionMethod'] == 'directdebit') {
			$parameters['RequestReason'] = 'VL';
		}
		else {
			$parameters['RequestReason'] = 'VK';
		}
		$parameters['CustomerID'] = Customweb_Util_String::substrUtf8($orderContext->getCustomerId(), 0, 20);
		
		$parameters['AccIBAN'] = Customweb_Util_String::substrUtf8($httpRequestParameters['AccIBAN'], 0, 34);
		$parameters['AccNr'] = Customweb_Util_String::substrUtf8($httpRequestParameters['AccNr'], 0, 10);
		
		$gender = Customweb_Computop_Method_Util::translateGender($httpRequestParameters['AccountOwnerGender'], $orderContext);
		
		$accountOwnerField = $gender . ';' . $httpRequestParameters['AccountOwnerFirstName'] . ';' . $httpRequestParameters['AccountOwnerLastName'];
		$parameters['AccOwner'] = Customweb_Util_String::substrUtf8($accountOwnerField, 0, 55);
		
		$parameters['FirstName'] = Customweb_Util_String::substrUtf8($orderContext->getBillingFirstName(), 0, 24);
		$parameters['LastName'] = Customweb_Util_String::substrUtf8($orderContext->getBillingLastName(), 0, 30);
		$parameters['PlaceOfBirth'] = Customweb_Util_String::substrUtf8($httpRequestParameters['PlaceOfBirth'], 0, 30);
		
		$address = Customweb_Util_Address::splitStreet($orderContext->getBillingStreet(), $orderContext->getBillingCountryIsoCode(), $orderContext->getBillingPostCode());
		
		$parameters['AddrStreet'] = Customweb_Util_String::substrUtf8($address['street'], 0, 30);
		$parameters['AddrStreetNr'] = Customweb_Util_String::substrUtf8($address['street-number'], 0, 8);
		$parameters['AddrZip'] = Customweb_Util_String::substrUtf8($orderContext->getBillingPostCode(), 0, 5);
		$parameters['AddrCity'] = Customweb_Util_String::substrUtf8($orderContext->getBillingCity(), 0, 30);
		$parameters['AddrCountryCode'] = Customweb_Util_Country::getCountry3LetterCode($orderContext->getBillingCountryIsoCode());
		
		return $parameters;
	}
	
	public function isTransactionUpdateable(){
		return true;
	}
	
	public function processTransactionPullUpdate(Customweb_Computop_Authorization_Transaction $transaction) {
		$this->pullCapturedAmounts($transaction);
	}
	
	private function getAccountOwnerFirstNameElement(Customweb_Payment_Authorization_IOrderContext $orderContext) {
		$control = new Customweb_Form_Control_TextInput('AccountOwnerFirstName', $orderContext->getBillingFirstName());
		$control->addValidator(new Customweb_Form_Validator_NotEmpty($control, Customweb_I18n_Translation::__("You have to enter the first name of the account owner.")));
		$control->setAutocomplete(false);
		$element = new Customweb_Form_Element(
				Customweb_I18n_Translation::__('Account owner first name.'),
				$control,
				Customweb_I18n_Translation::__('Please enter here the first name of the account owner.')
		);
		$element->setElementIntention(Customweb_Form_Intention_Factory::getAccountOwnerNameIntention())
		->setErrorMessage($errorMessage);
	
		return $element;
	}

	private function getAccountOwnerLastNameElement(Customweb_Payment_Authorization_IOrderContext $orderContext) {
		$control = new Customweb_Form_Control_TextInput('AccountOwnerLastName', $orderContext->getBillingLastName());
		$control->addValidator(new Customweb_Form_Validator_NotEmpty($control, Customweb_I18n_Translation::__("You have to enter the last name of the account owner.")));
		$control->setAutocomplete(false);
		$element = new Customweb_Form_Element(
				Customweb_I18n_Translation::__('Account owner last name.'),
				$control,
				Customweb_I18n_Translation::__('Please enter here the first name of the account owner.')
		);
		$element->setElementIntention(Customweb_Form_Intention_Factory::getAccountOwnerNameIntention())
		->setErrorMessage($errorMessage);
	
		return $element;
	}
	
	private function getTranferMethodElement() {
		$options = array(
			'directdebit' => Customweb_I18n_Translation::__("Direct Debit"),
			'banktransfer' => Customweb_I18n_Translation::__("Manual Bank Transfer"),
		);
		
		$control = new Customweb_Form_Control_TextInput('TransactionMethod', $orderContext->getBillingLastName());
		$control->setAutocomplete(false);
		$element = new Customweb_Form_Element(
				Customweb_I18n_Translation::__('Transaction Method'),
				$control,
				Customweb_I18n_Translation::__('To tranfer the money we can debit your bank account by direct debit or you can tranfer the money manually to our bank account.')
		);
		
		return $element;
	}
	
	public function refund(Customweb_Computop_Authorization_Transaction $transaction, array $items, $close) {
		throw new Exception(Customweb_I18n_Translation::__("Refunds are not supported by the payment method."));
	}
	
}