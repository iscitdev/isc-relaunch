<?php
/**
  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2015 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */

//require_once 'Customweb/Util/String.php';
//require_once 'Customweb/Computop/ElementFactory.php';
//require_once 'Customweb/Computop/Method/DefaultMethod.php';
//require_once 'Customweb/Form/ElementFactory.php';
//require_once 'Customweb/I18n/Translation.php';


/** 
 * 
 * @author Thomas Hunziker
 * @Method(paymentMethods={'Qiwi'}, authorizationMethods={'PaymentPage'})
 */
class Customweb_Computop_Method_Qiwi extends Customweb_Computop_Method_DefaultMethod{
	
	public function preValidate(Customweb_Payment_Authorization_IOrderContext $orderContext,
			Customweb_Payment_Authorization_IPaymentCustomerContext $paymentContext) {
		$allowedCountries = array(
			'RU', 'KZ', 'UA',
		);
		
		if (!in_array($orderContext->getBillingCountryIsoCode(), $allowedCountries)) {
			throw new Exception(Customweb_I18n_Translation::__("Country is not allowed for this payment method."));
		}
		
		return parent::preValidate($orderContext, $paymentContext);
	}
	
	
	public function getVisibleFormFields(Customweb_Payment_Authorization_IOrderContext $orderContext, $aliasTransaction, $failedTransaction, $paymentCustomerContext, $authorizationMethod) {
		$elements = parent::getVisibleFormFields($orderContext, $aliasTransaction, $failedTransaction, $paymentCustomerContext, $authorizationMethod);
		
		$accountOwner = $orderContext->getBillingFirstName() . ' ' . $orderContext->getBillingLastName();
		$mobilePhone = $orderContext->getBillingMobilePhoneNumber();
		
		$elements[] = Customweb_Form_ElementFactory::getAccountOwnerNameElement('AccOwner', $accountOwner);
		$elements[] = Customweb_Computop_ElementFactory::getMobilePhoneNumberElement('MobileNo', $mobilePhone);
		
		return $elements;
	}
	
	public function getAuthorizationParameters(Customweb_Computop_Authorization_Transaction $transaction, array $httpRequestParameters = array()) {
		$this->preValidate($transaction->getTransactionContext()->getOrderContext(), $transaction->getTransactionContext()->getPaymentCustomerContext());
		
		$parameters = parent::getAuthorizationParameters($transaction, $httpRequestParameters);
		$orderContext = $transaction->getTransactionContext()->getOrderContext();
		if (isset($httpRequestParameters['AccOwner']) && !empty($httpRequestParameters['AccOwner'])) {
			$parameters['AccOwner'] = $httpRequestParameters['AccOwner'];
		}
		else {
			$parameters['AccOwner'] = $orderContext->getBillingFirstName() . ' ' . $orderContext->getBillingLastName();
		}
		if (isset($httpRequestParameters['MobileNo']) && !empty($httpRequestParameters['MobileNo'])) {
			$parameters['MobileNo'] = $httpRequestParameters['MobileNo'];
		}
		else {
			throw new Exception(Customweb_I18n_Translation::__("No mobile phone number provided."));
		}
		
		$parameters['AccOwner'] = Customweb_Util_String::substrUtf8($parameters['AccOwner'], 0, 50);
		$parameters['MobileNo'] = Customweb_Util_String::substrUtf8($parameters['MobileNo'], 0, 20);
		$parameters['AddrCountryCode'] = $transaction->getTransactionContext()->getOrderContext()->getBillingCountryIsoCode();
		
		return $parameters;
	}
	
}