<?php 
/**
  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2015 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */

//require_once 'Customweb/Util/Address.php';
//require_once 'Customweb/Payment/Authorization/Method/Sepa/Iban.php';
//require_once 'Customweb/Payment/Exception/PaymentErrorException.php';
//require_once 'Customweb/Util/String.php';
//require_once 'Customweb/Computop/Util.php';
//require_once 'Customweb/Util/Country.php';
//require_once 'Customweb/Payment/Authorization/ErrorMessage.php';
//require_once 'Customweb/Date/DateTime.php';
//require_once 'Customweb/Payment/Authorization/Method/Sepa/Bic.php';
//require_once 'Customweb/I18n/Translation.php';



class Customweb_Computop_Method_Util {
	
	private function __construct() {
		
	}
	
	/**
	 * Returns the address parameters for the customer. We use the billing address.
	 * 
	 * @param Customweb_Payment_Authorization_IOrderContext $orderContext
	 * @return multitype:string
	 */
	public static function getAddressParameters(Customweb_Payment_Authorization_IOrderContext $orderContext) {
		$parameters = array();
		$address = Customweb_Util_Address::splitStreet($orderContext->getBillingStreet(), $orderContext->getBillingCountryIsoCode(), $orderContext->getBillingPostCode());
		$parameters['AddrStreet'] = Customweb_Util_String::substrUtf8($address['street'], 0, 32);
		$parameters['AddrStreetNr'] = Customweb_Util_String::substrUtf8($address['street-number'], 0, 5);
		$parameters['AddrZip'] = Customweb_Util_String::substrUtf8($orderContext->getBillingPostCode(), 0, 5);
		$parameters['AddrCity'] = Customweb_Util_String::substrUtf8($orderContext->getBillingCity(), 0, 32);
		return $parameters;
	}
	
	/**
	 * Returns the address parameters of the customer including the country code.
	 * 
	 * @param Customweb_Payment_Authorization_IOrderContext $orderContext
	 * @return string
	 */
	public static function getAddressParametersIncludingCountry(Customweb_Payment_Authorization_IOrderContext $orderContext) {
		$parameters = $this->getBillingAddressParameters($orderContext);
		$parameters['AddrCountryCode'] = Customweb_Util_Country::getCountry3LetterCode($orderContext->getBillingCountryIsoCode());
		return $parameters;
	}

	/**
	 * Returns 'NO' in case the customer is not a new one. If it is a new the method returns 'YES'.
	 * 
	 * @param Customweb_Payment_Authorization_IOrderContext $orderContext
	 * @return string
	 */
	public static function isNewCustomer(Customweb_Payment_Authorization_IOrderContext $orderContext){
		if($orderContext->isNewCustomer() == 'existing'){
			return 'NO';
		}
		else{
			return 'YES';
		}
	}
	
	/**
	 * This methdo extracts the date of birth from either the given request parameters or 
	 * from the order context. The method throws an exception in case no date could
	 * be extracted.
	 * 
	 * @param Customweb_Payment_Authorization_IOrderContext $orderContext
	 * @param array $httpRequestParameters
	 * @throws Exception
	 * @return Customweb_Date_DateTime
	 */
	public static function getDateOfBirth(Customweb_Payment_Authorization_IOrderContext $orderContext, array $httpRequestParameters) {
		if(isset($httpRequestParameters['dobYear']) && isset($httpRequestParameters['dobMonth']) && isset($httpRequestParameters['dobDay'])){
			return new Customweb_Date_DateTime($httpRequestParameters['dobYear'] . '-' . $httpRequestParameters['dobMonth'] . '-' . $httpRequestParameters['dobDay']);
		}
		else if ($orderContext->getBillingDateOfBirth() !== null){
			return $orderContext->getBillingDateOfBirth();
		}
		else {
			throw new Exception("Could not read date of birth.");
		}
	}

	public static function getBicParameters(Customweb_Payment_Authorization_IOrderContext $orderContext, array $httpRequestParameters) {
		if (!isset($httpRequestParameters['BIC']) && empty($httpRequestParameters['BIC'])) {
			throw new Customweb_Payment_Exception_PaymentErrorException(new Customweb_Payment_Authorization_ErrorMessage(Customweb_I18n_Translation::__("No BIC provided.")));
		}
		$bicHandler = new Customweb_Payment_Authorization_Method_Sepa_Bic();
		$httpRequestParameters['BIC'] = $bicHandler->sanitize($httpRequestParameters['BIC']);
		$bicHandler->validate($httpRequestParameters['BIC']);
	
		return array(
			'BIC' => $httpRequestParameters['BIC'],
		);
	}
	
	public static function getAccountOwnerParameters(Customweb_Payment_Authorization_IOrderContext $orderContext, array $httpRequestParameters) {
		if (isset($httpRequestParameters['AccOwner'])) {
			$accountOwner = $httpRequestParameters['AccOwner'];
		}
		else {
			$accountOwner = $orderContext->getBillingFirstName() . ' ' . $orderContext->getBillingLastName();
		}
		return array(
			'AccOwner' => Customweb_Util_String::substrUtf8(strip_tags($accountOwner), 0, 50),
		);
	}
	
	public static function getIbanParameters(Customweb_Payment_Authorization_IOrderContext $orderContext, array $httpRequestParameters) {
		if (!isset($httpRequestParameters['IBAN']) && empty($httpRequestParameters['IBAN'])) {
			throw new Customweb_Payment_Exception_PaymentErrorException(new Customweb_Payment_Authorization_ErrorMessage(Customweb_I18n_Translation::__("No IBAN provided.")));
		}
	
		$ibanHandler = new Customweb_Payment_Authorization_Method_Sepa_Iban();
		$httpRequestParameters['IBAN'] = $ibanHandler->sanitize($httpRequestParameters['IBAN']);
		$ibanHandler->validate($httpRequestParameters['IBAN']);
	
		return array(
			'IBAN' => $httpRequestParameters['IBAN'],
		);
	}
	
	public static function getBillingGender(Customweb_Payment_Authorization_IOrderContext $orderContext, array $parameters){
		if($orderContext->getBillingGender() !== null) {
			$gender = $orderContext->getBillingGender();
		}
		else if(isset($parameters['billingGender'])){
			$gender = $parameters['billingGender'];
		}
		else {
			throw new Exception("No billing gender could be read.");
		}
	
		return $gender;
	}

	public static function getShippingGender(Customweb_Payment_Authorization_IOrderContext $orderContext, array $parameters){
		if($orderContext->getShippingGender() !== null) {
			$gender = $orderContext->getShippingGender();
		}
		else if(isset($parameters['shippingGender'])){
			$gender = $parameters['shippingGender'];
		}
		else {
			// We assume the same gender for shipping and billing, when the names are equal.
			if (self::equalsBillingAndShippingName($orderContext)) {
				try {
					$gender = self::getBillingGender($orderContext, $parameters);
				}
				catch(Exception $e) {
					throw new Exception("No shipping gender could be read.");
				}
			}
			else {
				throw new Exception("No shipping gender could be read.");
			}
		}
	
		return $gender;
	}
	
	public static function addTestingParameters(array $parameters, Customweb_Computop_Authorization_Transaction $transaction, Customweb_Computop_Configuration $configuration) {
		if($configuration->isTestMode()){
			if($transaction->getAuthorizationAmount() > 10000){
				$parameters['OrderDesc'] = 'Test:0305';
			}
			else{
				$parameters['OrderDesc'] = 'Test:0000';
			}
		}
		
		return $parameters;
	}
	
	/**
	 * Returns true, when the shipping and billing names are equal.
	 * 
	 * @param Customweb_Payment_Authorization_IOrderContext $orderContext
	 * @return boolean
	 */
	public static function equalsBillingAndShippingName(Customweb_Payment_Authorization_IOrderContext $orderContext) {
		return $orderContext->getBillingFirstName() == $orderContext->getShippingFirstName() && $orderContext->getBillingLastName() == $orderContext->getShippingLastName();
	}
	
	public static function translateGender($gender, Customweb_Payment_Authorization_IOrderContext $orderContext) {
		$lang = Customweb_Computop_Util::getCleanLanguageCode($orderContext->getLanguage());
		
		$map = array(
			'male' => array('de' => 'Herr' , 'en' => 'Mr', 'fr' => 'Monsieur' ),
			'female' => array('de' => 'Frau', 'en' => 'Mrs', 'fr' => 'Madame')
		);
		
		if (isset($map[$gender])) {
			if (isset($map[$gender][$lang])) {
				return $map[$gender][$lang];
			}
			else {
				return $map[$gender]['en'];
			}
		}
		else {
			throw new Exception("Invalid gender format.");
		}
	}
	
	
}