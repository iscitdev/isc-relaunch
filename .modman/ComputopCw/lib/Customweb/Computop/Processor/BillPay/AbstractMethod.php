<?php
/**
  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2015 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */

//require_once 'Customweb/Computop/Processor/BillPay/ParameterBuilder/Cancel.php';
//require_once 'Customweb/Util/String.php';
//require_once 'Customweb/Computop/ElementFactory.php';
//require_once 'Customweb/Computop/Method/DefaultMethod.php';
//require_once 'Customweb/Computop/Processor/BillPay/ParameterBuilder/Refund.php';
//require_once 'Customweb/Util/Country.php';
//require_once 'Customweb/Computop/Processor/BillPay/LineItemBuilder.php';
//require_once 'Customweb/Form/ElementFactory.php';
//require_once 'Customweb/I18n/Translation.php';
//require_once 'Customweb/Computop/Method/Util.php';
//require_once 'Customweb/Util/Address.php';
//require_once 'Customweb/Core/Util/System.php';
//require_once 'Customweb/Computop/Util.php';
//require_once 'Customweb/Computop/Processor/BillPay/OrderDescriptionBuilder.php';
//require_once 'Customweb/Computop/Processor/BillPay/ParameterBuilder/Capture.php';


/**
 * This method provides an abstract implementation of the different BillPay payment methods.
 * 
 * @author Thomas Hunziker
 *
 */
abstract class Customweb_Computop_Processor_BillPay_AbstractMethod extends Customweb_Computop_Method_DefaultMethod {

	public function createTransaction(Customweb_Payment_Authorization_PaymentPage_ITransactionContext $transactionContext, $failedTransaction, $authorizationMethod) {
		$transaction = parent::createTransaction($transactionContext, $failedTransaction, $authorizationMethod);
		$transaction->setMultiCaptureEnabled(true);
		return $transaction;
	}
	
	public function getVisibleFormFields(Customweb_Payment_Authorization_IOrderContext $orderContext, $aliasTransaction, $failedTransaction, $paymentCustomerContext, $authorizationMethod) {
		$elements = parent::getVisibleFormFields($orderContext, $aliasTransaction, $failedTransaction, $paymentCustomerContext, $authorizationMethod);
		$elements[] = Customweb_Computop_ElementFactory::getGtcCheckboxElement('GtcValue');

		/* @var $paymentCustomerContext Customweb_Payment_Authorization_IPaymentCustomerContext */
		$contextMap = $paymentCustomerContext->getMap();
		
		if($orderContext->getBillingDateOfBirth() == null){
			$defaultDobYear = null;
			if (isset($contextMap['dobYear'])) {
				$defaultDobYear = $contextMap['dobYear'];
			}
			$defaultDobMonth = null;
			if (isset($contextMap['dobMonth'])) {
				$defaultDobMonth = $contextMap['dobMonth'];
			}
			$defaultDobDay = null;
			if (isset($contextMap['dobDay'])) {
				$defaultDobDay = $contextMap['dobDay'];
			}
			$startAge = 0;
			if ($this->existsPaymentMethodConfigurationValue('minimal_age') && $this->getPaymentMethodConfigurationValue('minimal_age') != 'none') {
				$startAge = (int)$this->getPaymentMethodConfigurationValue('minimal_age');
			}
			$elements[] = Customweb_Form_ElementFactory::getDateOfBirthElement('dobYear', 'dobMonth', 'dobDay', $defaultDobYear, $defaultDobMonth, $defaultDobDay, null, $startAge);
		}
		
		$billingGender = $orderContext->getBillingGender();
		if(empty($billingGender)){
			$default = null;
			if (isset($contextMap['billingGender'])) {
				$default = $contextMap['billingGender'];
			}
			$elements[] = Customweb_Computop_ElementFactory::getGenderElement(
					'billingGender', null, 
					Customweb_I18n_Translation::__('Billing Address Gender'), 
					Customweb_I18n_Translation::__('Please select your gender for the billing address.'),
					$default
			);
			
		}
		
		$shippingGender = $orderContext->getShippingGender();
		if (empty($shippingGender) && !Customweb_Computop_Method_Util::equalsBillingAndShippingName($orderContext)) {
			$default = null;
			if (isset($contextMap['shippingGender'])) {
				$default = $contextMap['shippingGender'];
			}
			$elements[] = Customweb_Computop_ElementFactory::getGenderElement(
				'shippingGender', 
				null,
				Customweb_I18n_Translation::__('Shipping Address Gender'),
				Customweb_I18n_Translation::__('Please select your gender for the shipping address.'),
					$default
			);
		}
		
		if(strlen($orderContext->getBillingCompanyName()) > 0 && !$this->isBusinessCustomerFiltered()){
			if (strlen($orderContext->getBillingCommercialRegisterNumber()) <= 0) {
				$default = null;
				if (isset($contextMap['commercialNumber'])) {
					$default = $contextMap['commercialNumber'];
				}
				$elements[] = Customweb_Form_ElementFactory::getCommercialNumberElement('commercialNumber', $default);
			}
			
			$default = null;
			if (isset($contextMap['LegalForm'])) {
				$default = $contextMap['LegalForm'];
			}
			$elements[] = Customweb_Computop_ElementFactory::getLegalFormElement('LegalForm', null, $default);
			
			$default = null;
			if (isset($contextMap['RegisterNumber'])) {
				$default = $contextMap['RegisterNumber'];
			}
			$elements[] = Customweb_Form_ElementFactory::getCommercialNumberElement('RegisterNumber', $default, false);
			
			$default = null;
			if (isset($contextMap['HolderName'])) {
				$default = $contextMap['HolderName'];
			}
			$elements[] = Customweb_Computop_ElementFactory::getCompanyHolderName('HolderName', $default);
		}
		
		return $elements;
	}
	
	public function getAuthorizationParameters(Customweb_Computop_Authorization_Transaction $transaction, array $httpRequestParameters = array()) {
		$parameters = parent::getAuthorizationParameters($transaction, $httpRequestParameters);
		
		$orderContext = $transaction->getTransactionContext()->getOrderContext();
		$parameters['NewCustomer'] = Customweb_Computop_Method_Util::isNewCustomer($orderContext);
		if (isset($httpRequestParameters['GtcValue']) && $httpRequestParameters['GtcValue'] == 'YES') {
			$parameters['GtcValue'] = 'YES';
		}
		else {
			$parameters['GtcValue'] = 'NO';
		}
		
		$parameters['DateOfBirth'] = Customweb_Computop_Method_Util::getDateOfBirth($orderContext, $httpRequestParameters)->format('Y-m-d');
		
		$parameters['IPAddr'] = Customweb_Core_Util_System::getClientIPAddress();
		
		if (strlen($orderContext->getBillingCompanyName()) > 0) {
			if ($this->isBusinessCustomerAllowed()) {
				$parameters['CompanyOrPerson'] = 'F';
				$parameters['CompanyName'] = Customweb_Util_String::substrUtf8($orderContext->getBillingCompanyName(), 0, 200);
				
				if (!isset($httpRequestParameters['LegalForm'])) {
					throw new Exception(Customweb_I18n_Translation::__("Company legal form is required."));
				}
				
				if (strlen($orderContext->getBillingCommercialRegisterNumber()) > 0) {
					$parameters['RegisterNumber'] = Customweb_Util_String::substrUtf8($orderContext->getBillingCommercialRegisterNumber(), 0, 20);
				}
				else if (isset($httpRequestParameters['RegisterNumber']) && strlen($httpRequestParameters['RegisterNumber']) > 0) {
					$parameters['RegisterNumber'] = Customweb_Util_String::substrUtf8($httpRequestParameters['RegisterNumber'], 0, 100);
				}
				
				if (strlen($orderContext->getBillingSalesTaxNumber()) > 0) {
					$parameters['TaxNumber'] = Customweb_Util_String::substrUtf8($orderContext->getBillingCommercialRegisterNumber(), 0, 20);
				}
				
				if (isset($httpRequestParameters['HolderName']) && strlen($httpRequestParameters['HolderName']) > 0) {
					$parameters['HolderName'] = Customweb_Util_String::substrUtf8($httpRequestParameters['HolderName'], 0, 100);
				}
			}
			else if ($this->isBusinessCustomerFiltered()) {
				$parameters['CompanyOrPerson'] = 'P';
			}
			else {
				throw new Exception(Customweb_I18n_Translation::__("Business customers are not allowed to order with this payment method."));
			}
		}
		else {
			$parameters['CompanyOrPerson'] = 'P';
		}
		
		$parameters['EMail'] = Customweb_Util_String::substrUtf8($orderContext->getBillingEMailAddress(), 0, 50);

		$builder = new Customweb_Computop_Processor_BillPay_LineItemBuilder($orderContext);
		$parameters['ArticleList'] = $builder->build();
		
		$descriptionBuilder = new Customweb_Computop_Processor_BillPay_OrderDescriptionBuilder($orderContext);
		$parameters['OrderDesc'] = $descriptionBuilder->build();
		
		$parameters['Language'] = Customweb_Computop_Util::getCleanLanguageCode($orderContext->getLanguage());
		
		$parameters['UseBillingData'] = 'No';
		$parameters = array_merge(
			$parameters, 
			$this->getBillingAddressParameters($orderContext, $httpRequestParameters),
			$this->getShippingAddressParameters($orderContext, $httpRequestParameters)
		);
		
		$this->storeFormData($transaction, $httpRequestParameters);
		
		return $parameters;
	}
	
	protected function storeFormData(Customweb_Computop_Authorization_Transaction $transaction, array $httpRequestParameters = array()) {
		if(isset($httpRequestParameters['dobYear'])) {
			$transaction->getTransactionContext()->getPaymentCustomerContext()->updateMap(array(
				'dobYear' => $httpRequestParameters['dobYear'],
			));
		}
		
		if(isset($httpRequestParameters['dobMonth'])) {
			$transaction->getTransactionContext()->getPaymentCustomerContext()->updateMap(array(
				'dobMonth' => $httpRequestParameters['dobMonth'],
			));
		}
		
		if(isset($httpRequestParameters['dobDay'])) {
			$transaction->getTransactionContext()->getPaymentCustomerContext()->updateMap(array(
				'dobDay' => $httpRequestParameters['dobDay'],
			));
		}
		
		if(isset($httpRequestParameters['shippingGender'])) {
			$transaction->getTransactionContext()->getPaymentCustomerContext()->updateMap(array(
				'shippingGender' => $httpRequestParameters['shippingGender'],
			));
		}

		if(isset($httpRequestParameters['billingGender'])) {
			$transaction->getTransactionContext()->getPaymentCustomerContext()->updateMap(array(
				'billingGender' => $httpRequestParameters['billingGender'],
			));
		}

		if(isset($httpRequestParameters['LegalForm'])) {
			$transaction->getTransactionContext()->getPaymentCustomerContext()->updateMap(array(
				'LegalForm' => $httpRequestParameters['LegalForm'],
			));
		}

		if(isset($httpRequestParameters['commercialNumber'])) {
			$transaction->getTransactionContext()->getPaymentCustomerContext()->updateMap(array(
				'commercialNumber' => $httpRequestParameters['commercialNumber'],
			));
		}

		if(isset($httpRequestParameters['RegisterNumber'])) {
			$transaction->getTransactionContext()->getPaymentCustomerContext()->updateMap(array(
				'RegisterNumber' => $httpRequestParameters['RegisterNumber'],
			));
		}
		
		if(isset($httpRequestParameters['TaxNumber'])) {
			$transaction->getTransactionContext()->getPaymentCustomerContext()->updateMap(array(
				'TaxNumber' => $httpRequestParameters['TaxNumber'],
			));
		}

		if(isset($httpRequestParameters['HolderName'])) {
			$transaction->getTransactionContext()->getPaymentCustomerContext()->updateMap(array(
				'HolderName' => $httpRequestParameters['HolderName'],
			));
		}
		
	}

	public function getAuthorizationUrl(){
		return 'billpay.aspx';
	}
	
	/**
	 * Returns the billing address of the customer as parameters (prefix 'bd').
	 *
	 * @param Customweb_Payment_Authorization_IOrderContext $orderContext
	 * @return string
	 */
	protected function getBillingAddressParameters(Customweb_Payment_Authorization_IOrderContext $orderContext, array $httpRequestParameters){
		$parameters = array();
		$parameters['bdSalutation'] = Customweb_Computop_Method_Util::translateGender(Customweb_Computop_Method_Util::getBillingGender($orderContext, $httpRequestParameters), $orderContext);
		$parameters['bdFirstName'] = Customweb_Util_String::substrUtf8($orderContext->getBillingFirstName(), 0, 50);
		$parameters['bdLastName'] = Customweb_Util_String::substrUtf8($orderContext->getBillingLastName(), 0, 50);
	
		$address = Customweb_Util_Address::splitStreet($orderContext->getBillingStreet(), $orderContext->getBillingCountryIsoCode(), $orderContext->getBillingPostCode());
		$parameters['bdStreet'] = Customweb_Util_String::substrUtf8($address['street'], 0, 50);
		$parameters['bdStreetNr'] = Customweb_Util_String::substrUtf8($address['street-number'], 0, 5);
		$parameters['bdZip'] = Customweb_Util_String::substrUtf8(preg_replace('/[^0-9]+/', '', $orderContext->getBillingPostCode()), 0, 5);
		$parameters['bdCity'] = Customweb_Util_String::substrUtf8($orderContext->getBillingCity(), 0, 50);
		$parameters['bdCountryCode'] = Customweb_Util_Country::getCountry3LetterCode($orderContext->getBillingCountryIsoCode());
	
		return $parameters;
	}
	
	/**
	 * Returns the shipping parameters
	 *
	 * @param Customweb_Payment_Authorization_IOrderContext $orderContext
	 * @return string
	 */
	protected function getShippingAddressParameters(Customweb_Payment_Authorization_IOrderContext $orderContext, array $httpRequestParameters){
		$parameters = array();
		$parameters['sdSalutation'] = Customweb_Computop_Method_Util::translateGender(Customweb_Computop_Method_Util::getShippingGender($orderContext, $httpRequestParameters), $orderContext);
		$parameters['sdFirstName'] = Customweb_Util_String::substrUtf8($orderContext->getShippingFirstName(), 0, 50);
		$parameters['sdLastName'] = Customweb_Util_String::substrUtf8($orderContext->getShippingLastName(), 0, 50);
			
		$address = Customweb_Util_Address::splitStreet($orderContext->getShippingStreet(), $orderContext->getShippingCountryIsoCode(), $orderContext->getShippingPostCode());
		$parameters['sdStreet'] =  Customweb_Util_String::substrUtf8($address['street'], 0, 50);
		$parameters['sdStreetNr'] = Customweb_Util_String::substrUtf8($address['street-number'], 0, 5);
		$parameters['sdZip'] = Customweb_Util_String::substrUtf8(preg_replace('/[^0-9]+/', '', $orderContext->getShippingPostCode()), 0, 5);
		$parameters['sdCity'] = Customweb_Util_String::substrUtf8($orderContext->getShippingCity(), 0, 50);
		$parameters['sdCountryCode'] = Customweb_Util_Country::getCountry3LetterCode($orderContext->getShippingCountryIsoCode());
	
		return $parameters;
	}
	
	
	/* Backend Opertions */
	
	protected function getCancelParameterBuilder(Customweb_Computop_Authorization_Transaction $transaction) {
		return new Customweb_Computop_Processor_BillPay_ParameterBuilder_Cancel($transaction, $this->getContainer());
	}
	
	protected function getRefundParameterBuilder(Customweb_Computop_Authorization_Transaction $transaction, array $items, $close) {
		return new Customweb_Computop_Processor_BillPay_ParameterBuilder_Refund($transaction, $this->getContainer(), $items, $close);
	}
	
	protected function getCaptureParameterBuilder(Customweb_Computop_Authorization_Transaction $transaction, array $items, $close) {
		return new Customweb_Computop_Processor_BillPay_ParameterBuilder_Capture($transaction, $this->getContainer(), $items, $close);
	}
	
	
	
	
}