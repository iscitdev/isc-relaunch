<?php
/**
  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2015 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */

//require_once 'Customweb/Computop/AbstractLineItemBuilder.php';
//require_once 'Customweb/Util/String.php';
//require_once 'Customweb/Payment/Authorization/IInvoiceItem.php';
//require_once 'Customweb/Util/Invoice.php';


class Customweb_Computop_Processor_BillPay_OrderDescriptionBuilder extends Customweb_Computop_AbstractLineItemBuilder {
	
	public function build() {
		
		$totalShippingCostExcludingTax = 0.0;
		$totalShippingCostIncludingTax = 0.0;
		$totalDiscountIncludingTax = 0.0;
		$totalDiscountExcludingTax = 0.0;
		
		
		foreach($this->getItems() as $item){
			if($item->getType() == Customweb_Payment_Authorization_IInvoiceItem::TYPE_SHIPPING){
				$totalShippingCostExcludingTax += $item->getAmountExcludingTax();
				$totalShippingCostIncludingTax += $item->getAmountIncludingTax();
			}
			else if($item->getType() == Customweb_Payment_Authorization_IInvoiceItem::TYPE_DISCOUNT){
				$totalDiscountExcludingTax += $item->getAmountExcludingTax();
				$totalDiscountIncludingTax += $item->getAmountIncludingTax();
			}
		}
		
		$items = array();
		$items[] = Customweb_Util_String::substrUtf8($this->getOrderContext()->getShippingMethod(), 0, 50);
		$items[] = $this->formatAmount($totalShippingCostExcludingTax);
		$items[] = $this->formatAmount($totalShippingCostIncludingTax);
		$items[] = $this->formatAmount($totalDiscountExcludingTax);
		$items[] = $this->formatAmount($totalDiscountIncludingTax);
		$items[] = $this->formatAmount(Customweb_Util_Invoice::getTotalAmountExcludingTax($this->getOrderContext()->getInvoiceItems()));
		$items[] = $this->formatAmount(Customweb_Util_Invoice::getTotalAmountIncludingTax($this->getOrderContext()->getInvoiceItems()));
		
		return implode($this->getLineItemPartsClue(), $items);
	}
	
	protected function getLineItemParts(Customweb_Payment_Authorization_IInvoiceItem $item) {
		
	}
	
	protected function getAllowedProductTypes() {
		
	}
	
}