<?php 
/**
  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2015 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */

//require_once 'Customweb/Computop/Processor/BillSafe/LineItemBuilder.php';
//require_once 'Customweb/Computop/Method/Default/RefundParameterBuilder.php';
//require_once 'Customweb/Computop/Util.php';
//require_once 'Customweb/Util/Invoice.php';


class Customweb_Computop_Processor_BillSafe_ParameterBuilder_Refund extends Customweb_Computop_Method_Default_RefundParameterBuilder {
	
	protected function buildInner() {
		$parameters = parent::buildInner();

		$parameters['TaxAmount'] = Customweb_Computop_Util::formatAmount(
				Customweb_Util_Invoice::getTotalTaxAmount($this->getRefundItems()),
				$this->getOrderContext()->getCurrencyCode()
		);
		
		$builder = new Customweb_Computop_Processor_BillSafe_LineItemBuilder($this->getOrderContext(), $this->getRefundItems());
		$parameters['ArticleList'] = $builder->build();
		
		return $parameters;
	}
	
}