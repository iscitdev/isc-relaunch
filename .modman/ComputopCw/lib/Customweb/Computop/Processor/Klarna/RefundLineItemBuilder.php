<?php
/**
  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2015 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */

//require_once 'Customweb/Computop/AbstractLineItemBuilder.php';
//require_once 'Customweb/Util/String.php';
//require_once 'Customweb/Payment/Authorization/IInvoiceItem.php';



/**
 * Klarna does not allow to refund an item partially. Means Klarna allow only the refund of the full line
 * item. However it is allowed to add new line items with the refund. 
 * 
 * To simulate the partial refund, we refund the whole line item in question
 * 
 * @author Thomas Hunziker
 *
 */
class Customweb_Computop_Processor_Klarna_RefundLineItemBuilder extends Customweb_Computop_AbstractLineItemBuilder {
	
	/**
	 * @var Customweb_Payment_Authorization_IInvoiceItem[]
	 */
	private $captureItems;
	
	public function __construct(Customweb_Payment_Authorization_IOrderContext $orderContext, $items = null, array $captureItems){
		parent::__construct($orderContext, $items);
		$this->captureItems = $captureItems;
	}
	

	public function build(){
		$lines = array();
		$partsClue = $this->getLineItemPartsClue();
		foreach ($this->getItems() as $item) {
			if ($this->isItemAllowedInList($item)) {
				$lines[] = implode($partsClue, $this->getCompleteRefundLine($item));
				$lines[] = implode($partsClue, $this->getAddNewLine($item));
			}
		}
	
		return implode($this->getLineItemClue(), $lines);
	}
	
	protected function getLineItemParts(Customweb_Payment_Authorization_IInvoiceItem $item) {
		throw new Exception("Not supported exception.");
	}

	private function getCompleteRefundLine(Customweb_Payment_Authorization_IInvoiceItem $item) {
		$originalLineItem = $this->getOriginalLineItemBySku($item->getSku());
		if ($originalLineItem === null) {
			throw new Exception("Unable to find the SKU of the original line item.");
		}
	
		$parts = array();
		$parts[] = Customweb_Util_String::substrUtf8(round($item->getQuantity()), 0, 5);
		$parts[] = Customweb_Util_String::substrUtf8($this->cleanString($item->getSku()), 0, 50);
		$parts[] = Customweb_Util_String::substrUtf8($this->cleanString($item->getName()), 0, 100);
		$parts[] = $this->getProductPriceExcludingTax($originalLineItem);
		$parts[] = round($item->getTaxRate());
		// Discount
		$parts[] = 0;
	
		// ArticleFlag
		if ($item->getType() == Customweb_Payment_Authorization_IInvoiceItem::TYPE_SHIPPING) {
			$parts[] = 8;
		}
		else {
			$parts[] = 0;
		}
		return $parts;
	}

	private function getAddNewLine(Customweb_Payment_Authorization_IInvoiceItem $item) {
		$originalLineItem = $this->getOriginalLineItemBySku($item->getSku());
		if ($originalLineItem === null) {
			throw new Exception("Unable to find the SKU of the original line item.");
		}
	
		$parts = array();
		$parts[] = "add:" . Customweb_Util_String::substrUtf8(round($item->getQuantity()), 0, 5);
		$parts[] = Customweb_Util_String::substrUtf8($this->cleanString($item->getSku()), 0, 50);
		$parts[] = Customweb_Util_String::substrUtf8($this->cleanString($item->getName()), 0, 100);
		$parts[] = $this->getProductPriceExcludingTax($originalLineItem) - $this->getProductPriceExcludingTax($item);
		$parts[] = round($item->getTaxRate());
		// Discount
		$parts[] = 0;
	
		// ArticleFlag
		if ($item->getType() == Customweb_Payment_Authorization_IInvoiceItem::TYPE_SHIPPING) {
			$parts[] = 8;
		}
		else {
			$parts[] = 0;
		}
		return $parts;
	}
	
	protected function getAllowedProductTypes() {
		return array (
			Customweb_Payment_Authorization_IInvoiceItem::TYPE_PRODUCT,
			Customweb_Payment_Authorization_IInvoiceItem::TYPE_FEE,
			Customweb_Payment_Authorization_IInvoiceItem::TYPE_SHIPPING,
			Customweb_Payment_Authorization_IInvoiceItem::TYPE_DISCOUNT
		);
	}
	
	private function getOriginalLineItemBySku($sku) {
		$lineItems = $this->captureItems;
		foreach ($lineItems as $item) {
			if ($item->getSku() == $sku) {
				return $item;
			}
		}
		return null;
	}
	
}
