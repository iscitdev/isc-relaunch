<?php

/**
 *  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2015 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */

//require_once 'Customweb/Computop/Processor/Paymorrow/ParameterBuilder/Capture.php';
//require_once 'Customweb/Computop/ElementFactory.php';
//require_once 'Customweb/Computop/Method/DefaultMethod.php';
//require_once 'Customweb/Computop/Processor/Paymorrow/Util.php';
//require_once 'Customweb/Computop/Processor/Paymorrow/ParameterBuilder/Cancel.php';
//require_once 'Customweb/Form/ElementFactory.php';
//require_once 'Customweb/I18n/Translation.php';
//require_once 'Customweb/Computop/Processor/Paymorrow/AuthorizationProcessor.php';
//require_once 'Customweb/Computop/Processor/Paymorrow/ParameterBuilder/Authorize.php';
//require_once 'Customweb/Form/Control/HiddenHtml.php';
//require_once 'Customweb/Computop/Processor/Paymorrow/ParameterBuilder/Refund.php';
//require_once 'Customweb/Form/HiddenElement.php';



/**
 * This method provides an abstract implementation of the different Paymorrow payment methods.
 *
 * @author Thomas Hunziker
 *
 */
abstract class Customweb_Computop_Processor_Paymorrow_AbstractMethod extends Customweb_Computop_Method_DefaultMethod {

	public function createTransaction(Customweb_Payment_Authorization_PaymentPage_ITransactionContext $transactionContext, $failedTransaction, $authorizationMethod){
		$transaction = parent::createTransaction($transactionContext, $failedTransaction, $authorizationMethod);
		$transaction->setMultiCaptureEnabled(true);
		Customweb_Computop_Processor_Paymorrow_Util::generateDeviceId($transaction->getPaymentCustomerContext());
		return $transaction;
	}

	public function getVisibleFormFields(Customweb_Payment_Authorization_IOrderContext $orderContext, $aliasTransaction, $failedTransaction, $paymentCustomerContext, $authorizationMethod){
		$elements = parent::getVisibleFormFields($orderContext, $aliasTransaction, $failedTransaction, $paymentCustomerContext, $authorizationMethod);
		$elements[] = Customweb_Computop_ElementFactory::getGtcCheckboxElement('TermsAndConditions');
		
		/* @var $paymentCustomerContext Customweb_Payment_Authorization_IPaymentCustomerContext */
		$contextMap = $paymentCustomerContext->getMap();
		
		if ($orderContext->getBillingDateOfBirth() == null) {
			$defaultDobYear = null;
			if (isset($contextMap['dobYear'])) {
				$defaultDobYear = $contextMap['dobYear'];
			}
			$defaultDobMonth = null;
			if (isset($contextMap['dobMonth'])) {
				$defaultDobMonth = $contextMap['dobMonth'];
			}
			$defaultDobDay = null;
			if (isset($contextMap['dobDay'])) {
				$defaultDobDay = $contextMap['dobDay'];
			}
			$startAge = 0;
			if ($this->existsPaymentMethodConfigurationValue('minimal_age') && $this->getPaymentMethodConfigurationValue('minimal_age') != 'none') {
				$startAge = (int) $this->getPaymentMethodConfigurationValue('minimal_age');
			}
			$elements[] = Customweb_Form_ElementFactory::getDateOfBirthElement('dobYear', 'dobMonth', 'dobDay', $defaultDobYear, $defaultDobMonth, 
					$defaultDobDay, null, $startAge);
		}
		
		$deviceId = Customweb_Computop_Processor_Paymorrow_Util::getDeviceId($paymentCustomerContext);
		$fraudHtml = '<iframe src="https://paymorrow.net/perthPmFrame/client.action?session_id=' . $deviceId .
				 '" style="width:1px; height: 1px; position: absolute; top:0; left:0px; border: none;"></iframe>';
		$elements[] = new Customweb_Form_HiddenElement(new Customweb_Form_Control_HiddenHtml('fraud_iframe', $fraudHtml));
		
		$billingGender = $orderContext->getBillingGender();
		if (empty($billingGender)) {
			$default = null;
			if (isset($contextMap['billingGender'])) {
				$default = $contextMap['billingGender'];
			}
			$elements[] = Customweb_Computop_ElementFactory::getGenderElement('billingGender', null, 
					Customweb_I18n_Translation::__('Billing Address Gender'), 
					Customweb_I18n_Translation::__('Please select your gender for the billing address.'), $default);
		}
		
		$phone = $orderContext->getBillingPhoneNumber();
		if (empty($phone)) {
			$elements[] = Customweb_Computop_ElementFactory::getBillingPhoneNumberElement('Phone');
		}
		
		return $elements;
	}

	protected function getAuthorizationProcessor(Customweb_Computop_Authorization_Transaction $transaction, array $httpRequestParameters = array()){
		return new Customweb_Computop_Processor_Paymorrow_AuthorizationProcessor($transaction, $this->getContainer(), 
				$this->getAuthorizeParameterBuilder($transaction, $httpRequestParameters));
	}

	protected function getAuthorizeParameterBuilder(Customweb_Computop_Authorization_Transaction $transaction, array $httpRequestParameters){
		return new Customweb_Computop_Processor_Paymorrow_ParameterBuilder_Authorize($transaction, $this->getContainer(), 
				$httpRequestParameters);
	}

	/* Backend Operations */
	
	/**
	 * With Paymorrow, only one capture is possible.
	 * 
	 * (non-PHPdoc)
	 * @see Customweb_Computop_Method_DefaultMethod::capture()
	 */
	public function capture(Customweb_Computop_Authorization_Transaction $transaction, array $items, $close){
		parent::capture($transaction, $items, true);
	}
	
	/**
	 * With Paymorrow, only one refund is possible.
	 * 
	 * (non-PHPdoc)
	 * @see Customweb_Computop_Method_DefaultMethod::refund()
	 */
	public function refund(Customweb_Computop_Authorization_Transaction $transaction, array $items, $close){
		$this->refundByCapture($transaction, $items, true);
	}
	
	protected function getCancelParameterBuilder(Customweb_Computop_Authorization_Transaction $transaction){
		return new Customweb_Computop_Processor_Paymorrow_ParameterBuilder_Cancel($transaction, $this->getContainer());
	}

	protected function getCaptureParameterBuilder(Customweb_Computop_Authorization_Transaction $transaction, array $items, $close){
		return new Customweb_Computop_Processor_Paymorrow_ParameterBuilder_Capture($transaction, $this->getContainer(), $items, $close);
	}

	protected function getCaptureReferenceRefundParameterBuilder(Customweb_Computop_Authorization_Transaction $transaction, $items, $close, Customweb_Computop_Authorization_TransactionCapture $referenceCapture){
		return new Customweb_Computop_Processor_Paymorrow_ParameterBuilder_Refund($transaction, $this->getContainer(), $items, $close, $referenceCapture);
	}
}