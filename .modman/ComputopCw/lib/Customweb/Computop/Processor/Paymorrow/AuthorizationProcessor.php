<?php

/**
 *  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2015 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */

//require_once 'Customweb/Computop/Method/Default/AuthorizationProcessor/Abstract.php';
//require_once 'Customweb/Computop/RemoteRequest/Default.php';
//require_once 'Customweb/Computop/Processor/Paymorrow/ParameterBuilder/Confirm.php';

class Customweb_Computop_Processor_Paymorrow_AuthorizationProcessor extends Customweb_Computop_Method_Default_AuthorizationProcessor_Abstract {
	private $authorizeParameterBuilder;

	public function __construct(Customweb_Computop_Authorization_Transaction $transaction, Customweb_DependencyInjection_IContainer $container, Customweb_Computop_IParameterBuilder $authorizeParameterBuilder){
		parent::__construct($transaction, $container);
		$this->authorizeParameterBuilder = $authorizeParameterBuilder;
	}

	public function process(){
		
		// Send Authorization Create Reqeust
		$request = new Customweb_Computop_RemoteRequest_Default($this->getConfiguration(), $this->authorizeParameterBuilder, 
				'paymorrow.aspx');
		
		$request->process();
		$this->getTransaction()->setAuthorizationParameters(
				array_merge($this->getTransaction()->getAuthorizationParameters(), $request->getResponseParameters()));
		
		// Send Confirmation Request
		$request = new Customweb_Computop_RemoteRequest_Default($this->getConfiguration(), 
				new Customweb_Computop_Processor_Paymorrow_ParameterBuilder_Confirm($this->getTransaction(), $this->getContainer()), 
				'paymorrow.aspx');
		
		$request->process();
		$this->getTransaction()->setAuthorizationParameters(
				array_merge($this->getTransaction()->getAuthorizationParameters(), $request->getResponseParameters()));
		
		$this->processAuthorizationParameters();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see Customweb_Computop_Method_Default_AuthorizationProcessor_Abstract::handleDirectCapture()
	 */
	protected function handleDirectCapture(){
		$pm = $this->getPaymentMethodByTransaction($this->getTransaction());
		$pm->capture($this->getTransaction(), $this->getTransaction()->getUncapturedLineItems(), true);
		$this->getDebitorManager()->processCapture($this->getTransaction());
	}
}