<?php 
/**
  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2015 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */

//require_once 'Customweb/Payment/Authorization/Method/Sepa/Iban.php';
//require_once 'Customweb/Payment/Exception/PaymentErrorException.php';
//require_once 'Customweb/Payment/Authorization/ErrorMessage.php';
//require_once 'Customweb/Payment/Authorization/Method/Sepa/Bic.php';
//require_once 'Customweb/I18n/Translation.php';
//require_once 'Customweb/Computop/Processor/Paymorrow/ParameterBuilder/Authorize.php';


class Customweb_Computop_Processor_Paymorrow_DirectDebit_AuthorizeParameterBuilder extends Customweb_Computop_Processor_Paymorrow_ParameterBuilder_Authorize
{
	/**
	 * (non-PHPdoc)
	 * @see Customweb_Computop_Processor_Paymorrow_ParameterBuilder_Authorize::buildInner()
	 */
	protected function buildInner() {
		$httpRequestParameters = $this->getFormData();
		
		if (!isset($httpRequestParameters['IBAN']) && empty($httpRequestParameters['IBAN'])) {
			throw new Customweb_Payment_Exception_PaymentErrorException(new Customweb_Payment_Authorization_ErrorMessage(Customweb_I18n_Translation::__("No IBAN provided.")));
		}
		
		$ibanHandler = new Customweb_Payment_Authorization_Method_Sepa_Iban();
		$ibanHandler->validate($httpRequestParameters['IBAN']);
		
			
		if (!isset($httpRequestParameters['BIC']) && empty($httpRequestParameters['BIC'])) {
			throw new Customweb_Payment_Exception_PaymentErrorException(new Customweb_Payment_Authorization_ErrorMessage(Customweb_I18n_Translation::__("No BIC provided.")));
		}
		$bicHandler = new Customweb_Payment_Authorization_Method_Sepa_Bic();
		$bicHandler->validate($httpRequestParameters['BIC']);
		
		
		$parameters = parent::buildInner();
		$parameters['RPMethod'] = 'SDD';
		$parameters['BIC'] = $httpRequestParameters['BIC'];
		$parameters['IBAN'] = $httpRequestParameters['IBAN'];
		$parameters['MandateProvided'] = 'NO';
		return $parameters;
	}
}