<?php

/**
 *  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2015 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */

//require_once 'Customweb/Computop/AbstractLineItemBuilder.php';
//require_once 'Customweb/Util/String.php';
//require_once 'Customweb/Payment/Authorization/IInvoiceItem.php';

class Customweb_Computop_Processor_Paymorrow_LineItemBuilder extends Customweb_Computop_AbstractLineItemBuilder {
	private $itemType = array(
		Customweb_Payment_Authorization_IInvoiceItem::TYPE_PRODUCT => "GOODS",
		Customweb_Payment_Authorization_IInvoiceItem::TYPE_FEE => "PAYMENT_FEE",
		Customweb_Payment_Authorization_IInvoiceItem::TYPE_SHIPPING => "SHIPPING",
		Customweb_Payment_Authorization_IInvoiceItem::TYPE_DISCOUNT => "VOUCHER" 
	);

	protected function getLineItemParts(Customweb_Payment_Authorization_IInvoiceItem $item){
		$parts = array();
		
		/*
		 * There are several rounding issues with Paymorrow.
		 * They check the total amount via the unit price. Now,
		 * if the unit price or the quantity are too small,
		 * i.e. more than two decimal places, it will lead to rounding errors.
		 *
		 * Therefore we check first, if the rounded values differ
		 * more than 1 (Cent) and if so, set the quantity to 1
		 * and the unit price to the total amount.
		 * Also, to avoid confusion, we add an explanation to
		 * the item name, which shows the actual unit price and
		 * quantity.
		 */
		
		$totalAmountInCents = $this->getAmountIncludingTax($item);
		$unitPriceInCents = $this->getProductPriceIncludingTax($item);
		$quantity = round($item->getQuantity(), 2);
		$name = $this->cleanString($item->getName());
		
		if (abs($quantity * $unitPriceInCents - $totalAmountInCents) > 1) {
			$name .= ' (' . round($item->getAmountIncludingTax() / $item->getQuantity(), 4) . ' ' . $this->getOrderContext()->getCurrencyCode() . ' x ' .
					 round($item->getQuantity(), 4) . ' Stk.)';
			$quantity = 1;
			$unitPriceInCents = $totalAmountInCents;
		}
		
		//ArticleList: Quantity;ArticleID;Name;Type;Category;UnitPriceGross;GrossAmount;VatRate;VatAmount
		$parts[] = Customweb_Util_String::substrUtf8($quantity, 0, 7);
		$parts[] = Customweb_Util_String::substrUtf8($this->cleanString($item->getSku()), 0, 20);
		$parts[] = Customweb_Util_String::substrUtf8($this->cleanString($name), 0, 50);
		$parts[] = $this->itemType[$item->getType()];
		
		$parts[] = $this->getOrderContext()->getPaymentMethod()->getPaymentMethodConfigurationValue("articlelist_category");
		$parts[] = $unitPriceInCents;
		$parts[] = $totalAmountInCents;
		$parts[] = number_format(round($item->getTaxRate()), 0) * 100;
		$parts[] = $this->formatAmount($item->getTaxAmount());
		
		return $parts;
	}

	protected function getAllowedProductTypes(){
		return array_keys($this->itemType);
	}
}