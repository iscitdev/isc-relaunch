<?php

/**
 *  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2015 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */

//require_once 'Customweb/Payment/Exception/PaymentErrorException.php';
//require_once 'Customweb/Util/String.php';
//require_once 'Customweb/Computop/Processor/Paymorrow/Util.php';
//require_once 'Customweb/Payment/Util.php';
//require_once 'Customweb/Util/Country.php';
//require_once 'Customweb/I18n/Translation.php';
//require_once 'Customweb/Computop/Method/Util.php';
//require_once 'Customweb/Util/Address.php';
//require_once 'Customweb/Computop/Processor/Paymorrow/ParameterBuilder/Abstract.php';
//require_once 'Customweb/Core/Util/System.php';
//require_once 'Customweb/Computop/Util.php';
//require_once 'Customweb/Util/Currency.php';
//require_once 'Customweb/Payment/Authorization/ErrorMessage.php';
//require_once 'Customweb/Util/Invoice.php';
//require_once 'Customweb/Computop/Processor/Paymorrow/LineItemBuilder.php';

class Customweb_Computop_Processor_Paymorrow_ParameterBuilder_Authorize extends Customweb_Computop_Processor_Paymorrow_ParameterBuilder_Abstract {
	
	/*
	 * TODO: Document external factors
	 *  - Paymorrow only accepts 7% or 19% as VAT rate (germany)
	 *  - Only one capture/refund possible
	 */
	private $formData = array();

	public function __construct(Customweb_Computop_Authorization_Transaction $transaction, Customweb_DependencyInjection_IContainer $container, $formData){
		parent::__construct($transaction, $container);
		$this->formData = $formData;
	}

	public function build(){
		$parameters = $this->buildInner();
		return $this->postProcessParameters($parameters);
	}

	protected function buildInner(){
		return array_merge($this->getBasicParameters(), $this->getPaymorrowBaseParameters(), $this->getAddressParameters(), 
				$this->getAmountParameters());
	}

	protected function getFormData(){
		return $this->formData;
	}

	protected function getPaymorrowBaseParameters(){
		$parameters = array();
		
		$httpRequestParameters = $this->getFormData();
		$orderContext = $this->getTransaction()->getTransactionContext()->getOrderContext();
		
		$companyName = $orderContext->getBillingCompanyName();
		if (!empty($companyName) && $this->getPaymentMethod()->isBusinessCustomerDenied()) {
			throw new Customweb_Payment_Exception_PaymentErrorException(
					new Customweb_Payment_Authorization_ErrorMessage(Customweb_I18n_Translation::__("Business customers are not allowed to order.")));
		}
		
		$parameters['TaxAmount'] = Customweb_Util_Currency::formatAmount(Customweb_Util_Invoice::getTotalTaxAmount($orderContext->getInvoiceItems()), 
				$orderContext->getCurrencyCode(), '', '');
		
		$parameters['RefNr'] = Customweb_Payment_Util::applyOrderSchema($this->getConfiguration()->getTransactionIdSchema(), 
				$this->getTransaction()->getExternalTransactionId(), 30);
		$parameters['OrderDesc'] = Customweb_Util_String::substrUtf8(
				Customweb_Computop_Util::getShortDescription($this->getConfiguration(), $this->getTransaction()), 0, 384);
		
		$parameters['EventToken'] = '10';
		if (isset($httpRequestParameters['TermsAndConditions']) && $httpRequestParameters['TermsAndConditions'] == 'YES') {
			$parameters['TermsAndConditions'] = 'YES';
		}
		else {
			$parameters['TermsAndConditions'] = 'NO';
		}
		
		$builder = new Customweb_Computop_Processor_Paymorrow_LineItemBuilder($orderContext, null);
		$parameters['ArticleList'] = $builder->build();
		
		$gender = Customweb_Computop_Method_Util::getBillingGender($orderContext, $httpRequestParameters);
		if (isset($httpRequestParameters['billingGender'])) {
			$this->getTransaction()->getTransactionContext()->getPaymentCustomerContext()->updateMap(
					array(
						'billingGender' => $httpRequestParameters['billingGender'] 
					));
		}
		
		$parameters['Gender'] = strtoupper($gender);

		$parameters['Language'] = 'DE';
		
		$parameters['e-mail'] = Customweb_Util_String::substrUtf8($orderContext->getBillingEMailAddress(), 0, 80);
		
		$parameters['NewCustomer'] = Customweb_Computop_Method_Util::isNewCustomer($orderContext);
		
		$parameters['DateOfBirth'] = Customweb_Computop_Method_Util::getDateOfBirth($orderContext, $httpRequestParameters)->format('Ymd');
		if (isset($httpRequestParameters['dobYear'])) {
			$this->getTransaction()->getTransactionContext()->getPaymentCustomerContext()->updateMap(
					array(
						'dobYear' => $httpRequestParameters['dobYear'] 
					));
		}
		
		if (isset($httpRequestParameters['dobMonth'])) {
			$this->getTransaction()->getTransactionContext()->getPaymentCustomerContext()->updateMap(
					array(
						'dobMonth' => $httpRequestParameters['dobMonth'] 
					));
		}
		
		if (isset($httpRequestParameters['dobDay'])) {
			$this->getTransaction()->getTransactionContext()->getPaymentCustomerContext()->updateMap(
					array(
						'dobDay' => $httpRequestParameters['dobDay'] 
					));
		}
		
		$parameters['IPAddr'] = Customweb_Core_Util_System::getClientIPAddress();
		
		/* @var $requestContext Customweb_Core_Http_IRequest */
		$requestContext = $this->getContainer()->getBean('Customweb_Core_Http_IRequest');
		$headers = array_change_key_case($requestContext->getParsedHeaders());
		$parameters['BrowserHeader'] = (isset($headers['user-agent']) ? $headers['user-agent'] : '');
		
		$phone = $orderContext->getBillingPhoneNumber();
		if (empty($phone)) {
			if (!empty($httpRequestParameters['Phone'])) {
				$phone = $httpRequestParameters['Phone'];
			}
			else {
				throw new Customweb_Payment_Exception_PaymentErrorException(
						new Customweb_Payment_Authorization_ErrorMessage(Customweb_I18n_Translation::__("No phone number provided.")));
			}
		}
		$parameters['Phone'] = Customweb_Util_String::substrUtf8($phone, 0, 30);
		
		$deviceId = Customweb_Computop_Processor_Paymorrow_Util::getDeviceId($this->getTransaction()->getPaymentCustomerContext());
		
		if ($deviceId === null) {
			throw new Customweb_Payment_Exception_PaymentErrorException(
					new Customweb_Payment_Authorization_ErrorMessage(Customweb_I18n_Translation::__("Unable to load device id from customer context.")));
		}
		
		$parameters['CookieID'] = $deviceId;
		$parameters['BrowserSessionID'] = $deviceId;
		$parameters['DeviceID'] = $deviceId;
		
		return $parameters;
	}

	/**
	 * Returns the billing address of the customer as parameters (prefix 'bd').
	 *
	 * @param Customweb_Payment_Authorization_IOrderContext $orderContext
	 * @return string
	 */
	private function getAddressParameters(){
		$orderContext = $this->getTransaction()->getTransactionContext()->getOrderContext();
		
		$parameters = array();
		
		$parameters['FirstName'] = Customweb_Util_String::substrUtf8($orderContext->getBillingFirstName(), 0, 50);
		$parameters['LastName'] = Customweb_Util_String::substrUtf8($orderContext->getBillingLastName(), 0, 100);
		
		$address = Customweb_Util_Address::splitStreet($orderContext->getBillingStreet(), $orderContext->getBillingCountryIsoCode(), 
				$orderContext->getBillingPostCode());
		$parameters['bdStreet'] = Customweb_Util_String::substrUtf8($address['street'], 0, 50);
		$parameters['bdStreetNr'] = Customweb_Util_String::substrUtf8($address['street-number'], 0, 5);
		$parameters['bdZip'] = Customweb_Util_String::substrUtf8(preg_replace('/[^0-9]+/', '', $orderContext->getBillingPostCode()), 0, 5);
		$parameters['bdCity'] = Customweb_Util_String::substrUtf8($orderContext->getBillingCity(), 0, 50);
		$parameters['bdCountryCode'] = Customweb_Util_Country::getCountryISONumericCode($orderContext->getBillingCountryIsoCode());
		
		$address = Customweb_Util_Address::splitStreet($orderContext->getShippingStreet(), $orderContext->getShippingCountryIsoCode(), 
				$orderContext->getShippingPostCode());
		$parameters['sdStreet'] = Customweb_Util_String::substrUtf8($address['street'], 0, 50);
		$parameters['sdStreetNr'] = Customweb_Util_String::substrUtf8($address['street-number'], 0, 5);
		$parameters['sdZip'] = Customweb_Util_String::substrUtf8(preg_replace('/[^0-9]+/', '', $orderContext->getShippingPostCode()), 0, 5);
		$parameters['sdCity'] = Customweb_Util_String::substrUtf8($orderContext->getShippingCity(), 0, 50);
		$parameters['sdCountryCode'] = Customweb_Util_Country::getCountryISONumericCode($orderContext->getShippingCountryIsoCode());
		
		return $parameters;
	}
}