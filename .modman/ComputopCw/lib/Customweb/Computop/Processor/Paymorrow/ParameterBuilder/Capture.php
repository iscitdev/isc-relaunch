<?php

/**
 *  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2015 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */

//require_once 'Customweb/Computop/IConstants.php';
//require_once 'Customweb/Computop/Util.php';
//require_once 'Customweb/Payment/Util.php';
//require_once 'Customweb/Computop/Method/Default/CaptureParameterBuilder.php';
//require_once 'Customweb/Util/Invoice.php';
//require_once 'Customweb/Computop/Processor/Paymorrow/LineItemBuilder.php';

class Customweb_Computop_Processor_Paymorrow_ParameterBuilder_Capture extends Customweb_Computop_Method_Default_CaptureParameterBuilder {

	protected function buildInner(){
		$parameters = parent::buildInner();
		
		if ($this->isClose()) {
			$parameters['EventToken'] = '22';
		}
		else {
			$parameters['EventToken'] = '21';
		}
		
		$builder = new Customweb_Computop_Processor_Paymorrow_LineItemBuilder($this->getTransactionContext()->getOrderContext(), 
				$this->getCorrectedItems($this->getCaptureItems()));
		$parameters['ArticleList'] = $builder->build();
		
		$parameters['TaxAmount'] = Customweb_Computop_Util::formatAmount(
				Customweb_Util_Invoice::getTotalTaxAmount($this->getCaptureItems()), $this->getTransaction()->getCurrencyCode());
		
		return $parameters;
	}

	protected function getCaptureParameters(){
		$parameters = array();
		$parameters['PayID'] = $this->getTransaction()->getPaymentId();
		$parameters['RefNr'] = Customweb_Payment_Util::applyOrderSchema($this->getConfiguration()->getTransactionIdSchema(), 
				$this->getTransaction()->getExternalTransactionId(), (29 - Customweb_Computop_IConstants::MAX_OPERATION_DIGITS)) .
				 $this->getCaptureCountAsString() . 'C';
		return $parameters;
	}

	protected function getCaptureCountAsString(){
		$ct = $this->getCaptureCount();
		return str_pad(strval($ct), Customweb_Computop_IConstants::MAX_OPERATION_DIGITS, '0', STR_PAD_LEFT);
	}

	protected function getCaptureCount(){
		return count($this->getTransaction()->getCaptures());
	}
	
	protected function encryptParameters($parameters){
		return Customweb_Computop_Util::encryptParametersPaymorrow($parameters, $this->getConfiguration());
	}
}