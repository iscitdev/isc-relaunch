<?php 
/**
  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2015 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */

//require_once 'Customweb/Computop/Processor/Paymorrow/ParameterBuilder/Abstract.php';


class Customweb_Computop_Processor_Paymorrow_ParameterBuilder_Confirm extends Customweb_Computop_Processor_Paymorrow_ParameterBuilder_Abstract {

	
	public function build() {
		$parameters = array();
		$parameters['MerchantID'] = $this->getConfiguration()->getMerchantId();
		$authParams = $this->getTransaction()->getAuthorizationParameters();
		$parameters['PayID'] = $authParams['PayID'];
		$parameters['EventToken'] = '13';
		
		if (strtolower($this->getTransaction()->getPaymentMethod()->getPaymentMethodName()) == 'openinvoice') {
			$parameters['RPMethod'] = 'INVOICE';
		}
		else {
			$parameters['RPMethod'] = 'SDD';
		}
		
		$parameters = $this->getSecurityParameters($parameters);
		return $this->encryptParameters($parameters);
	}
	
}