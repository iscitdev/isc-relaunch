<?php

/**
 *  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2015 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */

//require_once 'Customweb/Computop/Method/Default/RefundReferenceCaptureParameterBuilder.php';
//require_once 'Customweb/Computop/IConstants.php';
//require_once 'Customweb/Computop/Util.php';
//require_once 'Customweb/Payment/Util.php';
//require_once 'Customweb/Util/Invoice.php';
//require_once 'Customweb/Computop/Processor/Paymorrow/LineItemBuilder.php';

class Customweb_Computop_Processor_Paymorrow_ParameterBuilder_Refund extends Customweb_Computop_Method_Default_RefundReferenceCaptureParameterBuilder {

	protected function buildInner(){
		$parameters = parent::buildInner();
		
		/*
		 * we never close a refund at Paymorrow, otherwise
		 * you'd always have to refund everything that was
		 * captured before. 
		 */
		$parameters['EventToken'] = '31';
		
		$builder = new Customweb_Computop_Processor_Paymorrow_LineItemBuilder($this->getTransactionContext()->getOrderContext(), 
				$this->getCorrectedItems($this->getRefundItems()));
		$parameters['ArticleList'] = $builder->build();
		$parameters['TaxAmount'] = Customweb_Computop_Util::formatAmount(
				Customweb_Util_Invoice::getTotalTaxAmount($this->getRefundItems()), $this->getTransaction()->getCurrencyCode());
		$transactionCaptures = $this->getTransaction()->getCaptures();
		$parameters['XID'] = $this->getReferencedCaptureItem()->getExternalCaptureId();
		
		return $parameters;
	}

	protected function getRefundParameters(){
		$parameters = parent::getRefundParameters();
		$parameters['RefNr'] = Customweb_Payment_Util::applyOrderSchema($this->getConfiguration()->getTransactionIdSchema(), 
				$this->getTransaction()->getExternalTransactionId(), (29 - Customweb_Computop_IConstants::MAX_OPERATION_DIGITS)) .
				 $this->getRefundCountAsString() . 'R';
		return $parameters;
	}

	protected function getRefundCountAsString(){
		$ct = $this->getRefundCount();
		return str_pad(strval($ct), Customweb_Computop_IConstants::MAX_OPERATION_DIGITS, '0', STR_PAD_LEFT);
	}

	protected function getRefundCount(){
		return count($this->getTransaction()->getRefunds());
	}

	protected function encryptParameters($parameters){
		return Customweb_Computop_Util::encryptParametersPaymorrow($parameters, $this->getConfiguration());
	}
}