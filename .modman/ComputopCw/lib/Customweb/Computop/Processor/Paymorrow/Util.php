<?php 
/**
  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2015 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */

//require_once 'Customweb/Util/Rand.php';


class Customweb_Computop_Processor_Paymorrow_Util {
	
	const DEVICE_CONTEXT_KEY = 'paymorrow_device_id';
	
	private function __construct() {
		
	}
	
	public static function generateDeviceId(Customweb_Payment_Authorization_IPaymentCustomerContext $paymentCustomerContext) {
		$deviceId = Customweb_Util_Rand::getUuid();
		$paymentCustomerContext->updateMap(array(
			self::DEVICE_CONTEXT_KEY => $deviceId,
		));
		return $deviceId;
	}
	
	public static function getDeviceId(Customweb_Payment_Authorization_IPaymentCustomerContext $paymentCustomerContext) {
		$map = $paymentCustomerContext->getMap();
		
		if (isset($map[self::DEVICE_CONTEXT_KEY]) && !empty($map[self::DEVICE_CONTEXT_KEY])) {
			return $map[self::DEVICE_CONTEXT_KEY];
		}
		else {
			return null;
		}
	}
	
	public static function resetDeviceId(Customweb_Payment_Authorization_IPaymentCustomerContext $paymentCustomerContext) {
		$paymentCustomerContext->updateMap(array(
			self::DEVICE_CONTEXT_KEY => NULL,
		));
	}
	
	
}