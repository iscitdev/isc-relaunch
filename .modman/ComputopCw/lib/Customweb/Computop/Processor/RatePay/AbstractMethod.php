<?php
/**
  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2015 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */

//require_once 'Customweb/Computop/Processor/RatePay/ParameterBuilder/Capture.php';
//require_once 'Customweb/Computop/Processor/RatePay/ParameterBuilder/Initialize.php';
//require_once 'Customweb/Computop/ElementFactory.php';
//require_once 'Customweb/Computop/Processor/RatePay/ParameterBuilder/Cancel.php';
//require_once 'Customweb/Computop/RemoteRequest/Default.php';
//require_once 'Customweb/Computop/Method/DefaultMethod.php';
//require_once 'Customweb/Computop/Processor/RatePay/ParameterBuilder/Authorize.php';
//require_once 'Customweb/Computop/Processor/RatePay/AuthorizationProcessor.php';
//require_once 'Customweb/I18n/Translation.php';
//require_once 'Customweb/Form/ElementFactory.php';
//require_once 'Customweb/Computop/Processor/RatePay/ParameterBuilder/Refund.php';



/**
 * This method provides an abstract implementation of the different BillPay payment methods.
 * 
 * @author Thomas Hunziker
 *
 */
abstract class Customweb_Computop_Processor_RatePay_AbstractMethod extends Customweb_Computop_Method_DefaultMethod {

	public function createTransaction(Customweb_Payment_Authorization_PaymentPage_ITransactionContext $transactionContext, $failedTransaction, $authorizationMethod) {
		$transaction = parent::createTransaction($transactionContext, $failedTransaction, $authorizationMethod);
		$transaction->setMultiCaptureEnabled(true);
		return $transaction;
	}
	
	public function getVisibleFormFields(Customweb_Payment_Authorization_IOrderContext $orderContext, $aliasTransaction, $failedTransaction, $paymentCustomerContext, $authorizationMethod) {
		$elements = parent::getVisibleFormFields($orderContext, $aliasTransaction, $failedTransaction, $paymentCustomerContext, $authorizationMethod);

		/* @var $paymentCustomerContext Customweb_Payment_Authorization_IPaymentCustomerContext */
		$contextMap = $paymentCustomerContext->getMap();
		
		if(strlen($orderContext->getBillingCompanyName()) > 0 && $this->isBusinessCustomerAllowed()){
			$commercialNumber = $orderContext->getBillingCommercialRegisterNumber();
			if(empty($commercialNumber)){
				$default = null;
				if (isset($contextMap['CompanyId'])) {
					$default = $contextMap['CompanyId'];
				}
				$elements[] = Customweb_Form_ElementFactory::getCommercialNumberElement('CompanyID', $default);
			}
			$vatId = $orderContext->getBillingSalesTaxNumber();
			if (empty($vatId)) {
				$default = null;
				if (isset($contextMap['VatID'])) {
					$default = $contextMap['VatID'];
				}
				$elements[] = Customweb_Form_ElementFactory::getSalesTaxNumberElement('VatID', $default, false);
			}
		}
		else{
				
			if($orderContext->getBillingDateOfBirth() === null){
				$defaultDobYear = null;
				if (isset($contextMap['dobYear'])) {
					$defaultDobYear = $contextMap['dobYear'];
				}
				$defaultDobMonth = null;
				if (isset($contextMap['dobMonth'])) {
					$defaultDobMonth = $contextMap['dobMonth'];
				}
				$defaultDobDay = null;
				if (isset($contextMap['dobDay'])) {
					$defaultDobDay = $contextMap['dobDay'];
				}
				$startAge = 0;
				if ($this->existsPaymentMethodConfigurationValue('minimal_age') && $this->getPaymentMethodConfigurationValue('minimal_age') != 'none') {
					$startAge = (int)$this->getPaymentMethodConfigurationValue('minimal_age');
				}
				$elements[] = Customweb_Form_ElementFactory::getDateOfBirthElement('dobYear', 'dobMonth', 'dobDay', $defaultDobYear, $defaultDobMonth, $defaultDobDay, null, $startAge);
			}
				
			$billingGender = $orderContext->getBillingGender();
			if(empty($billingGender)){
				$default = null;
				if (isset($contextMap['billingGender'])) {
					$default = $contextMap['billingGender'];
				}
				$elements[] = Customweb_Computop_ElementFactory::getGenderElement(
						'billingGender', null,
						Customweb_I18n_Translation::__('Billing Address Gender'),
						Customweb_I18n_Translation::__('Please select your gender for the billing address.'),
						$default
				);
			}
		}
		
		$phone = $orderContext->getBillingPhoneNumber();
		if(empty($phone) || strlen($phone) < 6){
			$default = null;
			if (isset($contextMap['Phone'])) {
				$default = $contextMap['Phone'];
			}
			$elements [] = Customweb_Computop_ElementFactory::getBillingPhoneNumberElement('Phone', null, $default, 6);
		}
		
		return $elements;
	}
	
	
	protected function getAuthorizationProcessor(Customweb_Computop_Authorization_Transaction $transaction, array $httpRequestParameters = array()) {
		return new Customweb_Computop_Processor_RatePay_AuthorizationProcessor(
				$transaction, 
				$this->getContainer(), 
				$this->getAuthorizeParameterBuilder($transaction, $httpRequestParameters),
				$this->getInitializeParameterBuilder($transaction, $httpRequestParameters)
		);
	}

	protected function getAuthorizeParameterBuilder(Customweb_Computop_Authorization_Transaction $transaction, array $httpRequestParameters) {
		return new Customweb_Computop_Processor_RatePay_ParameterBuilder_Authorize($transaction, $this->getContainer(), $httpRequestParameters);
	}
	
	protected function getInitializeParameterBuilder(Customweb_Computop_Authorization_Transaction $transaction, array $httpRequestParameters) {
		return new Customweb_Computop_Processor_RatePay_ParameterBuilder_Initialize($transaction, $this->getContainer());
	}
	
	
	
	/* Backend Operations */
	
	public function cancel(Customweb_Computop_Authorization_Transaction $transaction) {
		$request = new Customweb_Computop_RemoteRequest_Default(
				$this->getGlobalConfiguration(),
				$this->getCancelParameterBuilder($transaction),
				'ratepay.aspx'
		);
		$request->process();
		$cancelItem = $transaction->cancel();
		$this->processCancel($cancelItem, $request);
	}
	
	
	public function refund(Customweb_Computop_Authorization_Transaction $transaction, array $items, $close) {
		// We need a different URL to which, we send the refund.
		$request = new Customweb_Computop_RemoteRequest_Default(
				$this->getGlobalConfiguration(),
				$this->getRefundParameterBuilder($transaction, $items, $close),
				'ratepay.aspx'
		);
		$request->process();
		$refundItem = $transaction->refundByLineItems($items, $close);
		$this->processRefund($refundItem, $request);
	}
	
	protected function getCancelParameterBuilder(Customweb_Computop_Authorization_Transaction $transaction) {
		return new Customweb_Computop_Processor_RatePay_ParameterBuilder_Cancel($transaction, $this->getContainer());
	}
	
	protected function getRefundParameterBuilder(Customweb_Computop_Authorization_Transaction $transaction, array $items, $close) {
		return new Customweb_Computop_Processor_RatePay_ParameterBuilder_Refund($transaction, $this->getContainer(), $items, $close);
	}
	
	protected function getCaptureParameterBuilder(Customweb_Computop_Authorization_Transaction $transaction, array $items, $close) {
		return new Customweb_Computop_Processor_RatePay_ParameterBuilder_Capture($transaction, $this->getContainer(), $items, $close);
	}
	
	
}

