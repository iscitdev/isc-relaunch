<?php
/**
  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2015 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
*/

//require_once 'Customweb/Payment/Authorization/Method/Sepa/Iban.php';
//require_once 'Customweb/Payment/Exception/PaymentErrorException.php';
//require_once 'Customweb/Util/String.php';
//require_once 'Customweb/Computop/Processor/RatePay/ParameterBuilder/Authorize.php';
//require_once 'Customweb/Payment/Authorization/ErrorMessage.php';
//require_once 'Customweb/Payment/Authorization/Method/Sepa/Bic.php';
//require_once 'Customweb/I18n/Translation.php';


class Customweb_Computop_Processor_RatePay_DirectDebit_AuthorizeParameterBuilder extends Customweb_Computop_Processor_RatePay_ParameterBuilder_Authorize
{
	protected function buildInner() {
	
		$httpRequestParameters = $this->getFormData();
		if (!isset($httpRequestParameters['IBAN']) && empty($httpRequestParameters['IBAN'])) {
			throw new Customweb_Payment_Exception_PaymentErrorException(new Customweb_Payment_Authorization_ErrorMessage(Customweb_I18n_Translation::__("No IBAN provided.")));
		}
	
		$ibanHandler = new Customweb_Payment_Authorization_Method_Sepa_Iban();
		$ibanHandler->validate($httpRequestParameters['IBAN']);
	
	
		if (!isset($httpRequestParameters['BIC']) && empty($httpRequestParameters['BIC'])) {
			throw new Customweb_Payment_Exception_PaymentErrorException(new Customweb_Payment_Authorization_ErrorMessage(Customweb_I18n_Translation::__("No BIC provided.")));
		}
		$bicHandler = new Customweb_Payment_Authorization_Method_Sepa_Bic();
		$bicHandler->validate($httpRequestParameters['BIC']);
	
		if (!isset($httpRequestParameters['AccBank']) && empty($httpRequestParameters['AccBank'])) {
			throw new Customweb_Payment_Exception_PaymentErrorException(new Customweb_Payment_Authorization_ErrorMessage(Customweb_I18n_Translation::__("No bank name provided.")));
		}
	
		if (!isset($httpRequestParameters['AccOwner']) && empty($httpRequestParameters['AccOwner'])) {
			throw new Customweb_Payment_Exception_PaymentErrorException(new Customweb_Payment_Authorization_ErrorMessage(Customweb_I18n_Translation::__("No account owner provided.")));
		}
	
		$parameters = parent::buildInner();
		$parameters['BIC'] = $httpRequestParameters['BIC'];
		$parameters['AccOwner'] = Customweb_Util_String::substrUtf8($httpRequestParameters['AccOwner'], 0, 55);
	
		$parameters['IBAN'] = $httpRequestParameters['IBAN'];
		$parameters['AccBank'] = Customweb_Util_String::substrUtf8($httpRequestParameters['AccBank'], 0, 27);
		return $parameters;
	}
	
	
}