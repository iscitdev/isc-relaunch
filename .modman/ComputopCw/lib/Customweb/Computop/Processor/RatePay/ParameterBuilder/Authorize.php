<?php 
/**
  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2015 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */

//require_once 'Customweb/Util/Address.php';
//require_once 'Customweb/Computop/Processor/RatePay/LineItemBuilder.php';
//require_once 'Customweb/Payment/Exception/PaymentErrorException.php';
//require_once 'Customweb/Core/Util/System.php';
//require_once 'Customweb/Util/String.php';
//require_once 'Customweb/Computop/Util.php';
//require_once 'Customweb/Util/Country.php';
//require_once 'Customweb/Payment/Authorization/ErrorMessage.php';
//require_once 'Customweb/Computop/AbstractParameterBuilder.php';
//require_once 'Customweb/I18n/Translation.php';
//require_once 'Customweb/Computop/Method/Util.php';


class Customweb_Computop_Processor_RatePay_ParameterBuilder_Authorize extends Customweb_Computop_AbstractParameterBuilder
{
	private $formData = array();
	
	public function __construct(Customweb_Computop_Authorization_Transaction $transaction, Customweb_DependencyInjection_IContainer $container, $formData) {
		parent::__construct($transaction, $container);
		$this->formData = $formData;
	}
	
	public function build() {
		$parameters = $this->buildInner();
		return $this->postProcessParameters($parameters);
	}
	
	protected function buildInner() {
		return array_merge(
			$this->getBasicParameters(),
			$this->getRatePayBaseParameters(),
			$this->getAddressParameters(),
			$this->getAmountParameters()
		);
	}
	
	protected function getFormData() {
		return $this->formData;
	}
	
	protected function getRatePayBaseParameters() {
		$parameters = array();
		$httpRequestParameters = $this->getFormData();
		$orderContext = $this->getTransaction()->getTransactionContext()->getOrderContext();

		
		$companyName = $orderContext->getBillingCompanyName();
		if (!empty($companyName) && $this->getPaymentMethod()->isBusinessCustomerDenied()) {
			throw new Customweb_Payment_Exception_PaymentErrorException(new Customweb_Payment_Authorization_ErrorMessage(Customweb_I18n_Translation::__("Business customers are not allowed to order.")));
		}
		
		if (!empty($companyName) && $this->getPaymentMethod()->isBusinessCustomerAllowed()) {
			$parameters['CompanyName'] = Customweb_Util_String::formatString($companyName, 0, 100);

			$companyId = $orderContext->getBillingCommercialRegisterNumber();
			if (empty($companyId)) {
				if (!empty($httpRequestParameters['CompanyID'])) {
					$companyId = $httpRequestParameters['CompanyID'];
					$this->getTransaction()->getTransactionContext()->getPaymentCustomerContext()->updateMap(array(
						'CompanyID' => $companyId
					));
				}
				else {
					throw new Customweb_Payment_Exception_PaymentErrorException(new Customweb_Payment_Authorization_ErrorMessage(Customweb_I18n_Translation::__("No commercial company number provided.")));
				}
			}
			$parameters['CompanyID'] = $companyId;

			$vatId = $orderContext->getBillingSalesTaxNumber();
			if (empty($vatId)) {
				if (!empty($httpRequestParameters['VatID'])) {
					$vatId = $httpRequestParameters['VatID'];
					$this->getTransaction()->getTransactionContext()->getPaymentCustomerContext()->updateMap(array(
						'VatID' => $vatId
					));
				}
			}
			if (!empty($vatId)) {
				$parameters['VatID'] = $vatId;
			}
		}
		else {
			$parameters['DateOfBirth'] = Customweb_Computop_Method_Util::getDateOfBirth($orderContext, $httpRequestParameters)->format('Ymd');

			if(isset($httpRequestParameters['dobYear'])) {
				$this->getTransaction()->getTransactionContext()->getPaymentCustomerContext()->updateMap(array(
					'dobYear' => $httpRequestParameters['dobYear'],
				));
			}

			if(isset($httpRequestParameters['dobMonth'])) {
				$this->getTransaction()->getTransactionContext()->getPaymentCustomerContext()->updateMap(array(
					'dobMonth' => $httpRequestParameters['dobMonth'],
				));
			}

			if(isset($httpRequestParameters['dobDay'])) {
				$this->getTransaction()->getTransactionContext()->getPaymentCustomerContext()->updateMap(array(
					'dobDay' => $httpRequestParameters['dobDay'],
				));
			}
		}
		
		$customerId = $orderContext->getCustomerId();
		if (empty($customerId)) {
			$customerId = 'guest';
		}
		$parameters['CustomerID'] = Customweb_Util_String::substrUtf8($customerId, 0, 30);
		$parameters['CustomerClassification'] = 'neutral';
		
		$gender = Customweb_Computop_Method_Util::getBillingGender($orderContext, $httpRequestParameters);
		if(isset($httpRequestParameters['billingGender'])) {
			$this->getTransaction()->getTransactionContext()->getPaymentCustomerContext()->updateMap(array(
				'billingGender' => $httpRequestParameters['billingGender'],
			));
		}
		
		
		if ($gender == 'female') {
			$parameters['Gender'] = 'F';
		}
		else if ($gender == 'male') {
			$parameters['Gender'] = 'M';
		} 
		else {
			$parameters['Gender'] = 'U';
		}
		
		$parameters['Nationality'] = Customweb_Util_Country::getCountryISONumericCode($orderContext->getBillingCountryIsoCode());
		$parameters['AllowMarketing'] = 'no';
		
		$parameters['E-Mail'] = Customweb_Util_String::substrUtf8($orderContext->getBillingEMailAddress(), 0, 80);
		$parameters['IPAddr'] = Customweb_Core_Util_System::getClientIPAddress();
		
		$phone = $orderContext->getBillingPhoneNumber();
		if (empty($phone) || strlen($phone) < 6) {
			if (!empty($httpRequestParameters['Phone'])) {
				$phone = $httpRequestParameters['Phone'];
				$this->getTransaction()->getTransactionContext()->getPaymentCustomerContext()->updateMap(array(
					'Phone' => $httpRequestParameters['Phone'],
				));
			}
			else {
				throw new Customweb_Payment_Exception_PaymentErrorException(new Customweb_Payment_Authorization_ErrorMessage(Customweb_I18n_Translation::__("No phone number provided.")));
			}
		}
		if (strlen($phone) < 6) {
			throw new Customweb_Payment_Exception_PaymentErrorException(new Customweb_Payment_Authorization_ErrorMessage(Customweb_I18n_Translation::__("The provided phone number is too short. It must be at least 6 chars long.")));
		}
		$parameters['Phone'] = Customweb_Util_String::substrUtf8($phone, 0, 30);
		
		$parameters['AllowCredInq'] = 'yes';
		$parameters['EventToken'] = 'A';
		
		$parameters['RefNr'] = $this->getTransactionContext()->getOrderId();
		if (empty($parameters['RefNr'])) {
			$parameters['RefNr'] = $this->getTransaction()->getTransactionId();
		}
		
		$builder = new Customweb_Computop_Processor_RatePay_LineItemBuilder($orderContext);
		$parameters['ShoppingBasket'] = $builder->build();
		
		$parameters['ShoppingBasketAmount'] = Customweb_Computop_Util::formatAmount(
			$orderContext->getOrderAmountInDecimals(),
			$orderContext->getCurrencyCode()
		);
		
		$parameters['OrderDesc'] = Customweb_Util_String::substrUtf8(Customweb_Computop_Util::getShortDescription($this->getConfiguration(), $this->getTransaction()), 0, 768);
		
		return $parameters;
	}
	

	/**
	 * Returns the billing address of the customer as parameters (prefix 'bd').
	 *
	 * @param Customweb_Payment_Authorization_IOrderContext $orderContext
	 * @return string
	 */
	private function getAddressParameters(){
		
		$orderContext = $this->getTransaction()->getTransactionContext()->getOrderContext();
		
		$parameters = array();
		$shippingCompanyName = $orderContext->getShippingCompanyName();
		if (!empty($shippingCompanyName)) {
			$parameters['FirstName'] = $orderContext->getShippingFirstName() . ' ' . $orderContext->getShippingLastName();
			$parameters['LastName'] = $shippingCompanyName;
		}
		else {
			$parameters['FirstName'] = $orderContext->getShippingFirstName();
			$parameters['LastName'] = $orderContext->getShippingLastName();
		}
		
		$parameters['FirstName'] = Customweb_Util_String::substrUtf8($parameters['FirstName'], 0, 100);
		$parameters['LastName'] = Customweb_Util_String::substrUtf8($parameters['LastName'], 0, 100);
		
		
		$address = Customweb_Util_Address::splitStreet($orderContext->getBillingStreet(), $orderContext->getBillingCountryIsoCode(), $orderContext->getBillingPostCode());
		$parameters['AddrStreet'] = Customweb_Util_String::substrUtf8($address['street'], 0, 50);
		$parameters['AddrStreetNr'] = Customweb_Util_String::substrUtf8($address['street-number'], 0, 5);
		$parameters['AddrZIP'] = Customweb_Util_String::substrUtf8(preg_replace('/[^0-9]+/', '', $orderContext->getBillingPostCode()), 0, 5);
		$parameters['AddrCity'] = Customweb_Util_String::substrUtf8($orderContext->getBillingCity(), 0, 50);
		$parameters['AddrCountryCode'] = $orderContext->getBillingCountryIsoCode();
		
		$address = Customweb_Util_Address::splitStreet($orderContext->getShippingStreet(), $orderContext->getShippingCountryIsoCode(), $orderContext->getShippingPostCode());
		$parameters['sdStreet'] =  Customweb_Util_String::substrUtf8($address['street'], 0, 50);
		$parameters['sdStreetHouseNumber'] = Customweb_Util_String::substrUtf8($address['street-number'], 0, 5);
		$parameters['sdZIPCode'] = Customweb_Util_String::substrUtf8(preg_replace('/[^0-9]+/', '', $orderContext->getShippingPostCode()), 0, 5);
		$parameters['sdCity'] = Customweb_Util_String::substrUtf8($orderContext->getShippingCity(), 0, 50);
		$parameters['sdCountryCode'] = $orderContext->getShippingCountryIsoCode();
	
		return $parameters;
	}
	
}