<?php 
/**
  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2015 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */

//require_once 'Customweb/Computop/Processor/RatePay/LineItemBuilder.php';
//require_once 'Customweb/Computop/Util.php';
//require_once 'Customweb/Computop/Method/Default/CaptureParameterBuilder.php';
//require_once 'Customweb/Util/Invoice.php';


class Customweb_Computop_Processor_RatePay_ParameterBuilder_Capture extends Customweb_Computop_Method_Default_CaptureParameterBuilder {
	
	protected function buildInner() {
		$parameters = parent::buildInner();
		
		$parameters['EventToken'] = 'C';
		$builder = new Customweb_Computop_Processor_RatePay_LineItemBuilder($this->getTransactionContext()->getOrderContext(), $this->getCaptureItems());
		$parameters['ShoppingBasket'] = $builder->build();
		$parameters['ShoppingBasketAmount'] = Customweb_Computop_Util::formatAmount(
			Customweb_Util_Invoice::getTotalAmountIncludingTax($this->getCaptureItems()),
			$this->getTransaction()->getCurrencyCode()
		);
		
		return $parameters;
	}
	
}