<?php 
/**
  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2015 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */

//require_once 'Customweb/Computop/AbstractParameterBuilder.php';


class Customweb_Computop_Processor_RatePay_ParameterBuilder_Verify extends Customweb_Computop_AbstractParameterBuilder
{
	
	public function build() {
		$parameters = $this->buildInner();
		return $this->postProcessParameters($parameters);
	}
		
	protected function buildInner() {
		$authParams = $this->getTransaction()->getAuthorizationParameters();
		$parameters = array();
		$parameters['MerchantID'] = $this->getConfiguration()->getMerchantId();
		$parameters['PayID'] = $authParams['PayID'];
		$parameters['EventToken'] = 'V';
		return $parameters;
	}
	
}
