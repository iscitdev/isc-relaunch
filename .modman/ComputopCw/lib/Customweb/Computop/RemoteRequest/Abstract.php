<?php 
/**
  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2015 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */

//require_once 'Customweb/Computop/Util.php';


abstract class Customweb_Computop_RemoteRequest_Abstract {
	
	/**
	 * @var Customweb_Computop_Configuration
	 */
	private $configruation;
	
	private $responseParameters = array();
	
	public function __construct(Customweb_Computop_Configuration $configuration) {
		$this->configruation = $configuration;
	}
	
	public function process() {
		$response = Customweb_Computop_Util::sendPostRequest($this->getRequestUrl(), $this->getRequestParameters());
		// Sometimes we get a html page after the post parameters in the response body
		// we don't want the html so we just take the parameters and discard the html
		$responseParts = explode("\n",$response);
		$response = $responseParts[0];
		
		parse_str($response, $responseParameters);
		$this->responseParameters = Customweb_Computop_Util::decryptIfNeeded($this->getConfiguration(), $responseParameters);
		$this->processResponse();
		return $this->responseParameters;
	}
	
	/**
	 * @return Customweb_Computop_Configuration
	 */
	protected function getConfiguration() {
		return $this->configruation;
	}
	
	public function getResponseParameters() {
		return $this->responseParameters;
	}
	
	/**
	 * Returns the URL to which the HTTP request is sent to.
	 * 
	 * @return String Target URL
	 */
	abstract protected function getRequestUrl();
	
	/**
	 * Returns a list of parametes which should be sent to the target URL.
	 *
	 * @return array
	 */
	abstract protected function getRequestParameters();
	
	/**
	 * This method checks the response and may throw an exception in case
	 * something is not valid.
	 * 
	 * @return void
	 * @throws Exception
	 */
	abstract protected function processResponse();
}