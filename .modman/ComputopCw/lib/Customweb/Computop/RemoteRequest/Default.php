<?php 
/**
  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2015 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */

//require_once 'Customweb/Payment/Exception/PaymentErrorException.php';
//require_once 'Customweb/Computop/Util.php';
//require_once 'Customweb/Computop/RemoteRequest/Abstract.php';
//require_once 'Customweb/Payment/Authorization/ErrorMessage.php';
//require_once 'Customweb/I18n/Translation.php';


class Customweb_Computop_RemoteRequest_Default extends Customweb_Computop_RemoteRequest_Abstract{
	
	/**
	 * @var Customweb_Computop_IParameterBuilder
	 */
	private $builder = null;
	
	/**
	 * @var string
	 */
	private $url = null;
	
	public function __construct(Customweb_Computop_Configuration $configuration, Customweb_Computop_IParameterBuilder $builder, $remoteFile) {
		parent::__construct($configuration);
		$this->builder = $builder;
		
		if (substr($remoteFile, 0, 4) != 'http') {
			$this->url = $this->getConfiguration()->getBaseUrl() . $remoteFile;
		}
		else {
			$this->url = $remoteFile;
		}
	}
	
	protected function getParameterBuilder() {
		return $this->builder;
	}
	
	protected function getRequestUrl() {
		return $this->url;
	}
	
	protected function getRequestParameters() {
		return $this->getParameterBuilder()->build();
	}
	
	protected function processResponse() {
		$parameters = array_change_key_case($this->getResponseParameters(), CASE_LOWER);
		if (!isset($parameters['status'])) {
			$parts = array();
			foreach ($parameters as $key => $value) {
				$parts[] = $key . ' = ' . strip_tags($value);
			}
			throw new Exception(Customweb_I18n_Translation::__(
					"Remote server returns no status. Parameters: @parameters",
					array(
						'@parameters' => implode(', ', $parts),
					)
			));
		}
		if (!Customweb_Computop_Util::isStatusSuccessful($parameters['status'])) {
			
			$code = 'No Code returned';
			if (isset($parameters['code'])) {
				$code = $parameters['code'];
			}
			
			$description = "no reason provided for failure";
			if (isset($parameters['description'])) {
				$description = $parameters['description'];
			}
			if (strtoupper($description) == 'EXTERNAL SYSTEM ERROR') {
				$userMessage = Customweb_I18n_Translation::__(
						"The payment method is not available."
				);
			}
			else {
				$userMessage = Customweb_I18n_Translation::__(
						"Transaction fail due to !description.",
						array(
							'!description' => $description,
						)
				);
			}
			
			$backendMessage = Customweb_I18n_Translation::__(
					"Request to '!url' failed because of !description (Code: !code).",
					array(
						'!code' => $code,
						'!description' => $description,
						'!url' => $this->getRequestUrl(),
					)
			);
			
			throw new Customweb_Payment_Exception_PaymentErrorException(new Customweb_Payment_Authorization_ErrorMessage($userMessage, $backendMessage));
		}
	}
	
}