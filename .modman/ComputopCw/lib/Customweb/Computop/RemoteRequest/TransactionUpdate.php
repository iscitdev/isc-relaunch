<?php 
/**
  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2015 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */

//require_once 'Customweb/Computop/Util.php';
//require_once 'Customweb/Computop/RemoteRequest/Abstract.php';
//require_once 'Customweb/I18n/Translation.php';


class Customweb_Computop_RemoteRequest_TransactionUpdate extends Customweb_Computop_RemoteRequest_Abstract{
	
	/**
	 * @var Customweb_Computop_Authorization_Transaction
	 */
	private $transaction = null;
	
	private $authorizedAmount = null;
	
	private $capturedAmount = null;
	
	private $refundedAmount = null;
	
	public function __construct(Customweb_Computop_Configuration $configuration, Customweb_Computop_Authorization_Transaction $transcation) {
		parent::__construct($configuration);
		$this->transaction = $transcation;
	}
	
	protected function getRequestUrl() {
		return $this->getConfiguration()->getInquiryUrl();
	}
	
	protected function getRequestParameters() {
		$parameters['MerchantID'] = $this->getConfiguration()->getMerchantId();
		$parameters['TransID'] = Customweb_Computop_Util::getTransactionID(
			$this->getConfiguration(),
			$this->getTransaction()
		);
		$parameters['PayID'] = $this->getTransaction()->getPaymentId();
		
		return Customweb_Computop_Util::encryptParameters($parameters, $this->getConfiguration());
	}
	
	protected function processResponse() {
		$parameters = $this->getResponseParameters();
		if (!Customweb_Computop_Util::isStatusSuccessful($parameters['Status'])) {
			throw new Exception(Customweb_I18n_Translation::__(
					"Request failed because of !description (Code: !code)",
					array(
						'!code' => $parameters['Code'],
						'!description' => $parameters['Description'],
					)
			));
		}
		$this->authorizedAmount = $parameters['AmountAuth'];
		$this->capturedAmount = $parameters['AmountCap'];
		$this->refundedAmount = $parameters['AmountCred'];
		
	}

	protected function getTransaction(){
		return $this->transaction;
	}

	public function getAuthorizedAmount(){
		return $this->authorizedAmount;
	}

	public function getCapturedAmount(){
		return $this->capturedAmount;
	}

	public function getRefundedAmount(){
		return $this->refundedAmount;
	}
	
	
	
}