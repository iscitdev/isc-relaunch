<?php 
/**
  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2015 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */



/**
 * This interface defines the integration with a risk service. A risk
 * service provides a indication if a order should be accepted or not.
 * 
 * @author Thomas Hunziker
 *
 */
interface Customweb_Computop_Risk_IService {
	
	/**
	 * Checks a given transaction if it should be accepted or not. In case not exception
	 * is thrown, the order can be accepted. In case a Customweb_Computop_Risk_Exception_OrderRejectException
	 * is thrown the order is not acceptable by the service.
	 * 
	 * @param Customweb_Computop_Authorization_Transaction $transaction $transaction
	 * @param array $httpRequestParameters
	 * @return void
	 * @throws Customweb_Computop_Risk_Exception_OrderRejectException
	 * @throws Customweb_Computop_Risk_Exception_ConfigurationException
	 * @throws Customweb_Computop_Risk_Exception_ServiceOutageException
	 */
	public function check(Customweb_Computop_Authorization_Transaction $transaction, array $httpRequestParameters);
	
	/**
	 * Returns a list of form elements, which are required to execute the check. 
	 * 
	 * @param Customweb_Payment_Authorization_IOrderContext $orderContext
	 * @param Customweb_Payment_Authorization_IPaymentCustomerContext $paymentCustomerContext
	 * @return Customweb_Form_IElement[] List of visible form fields for this verification office.
	 */
	public function getFormElements(Customweb_Payment_Authorization_IOrderContext $orderContext, Customweb_Payment_Authorization_IPaymentCustomerContext $paymentCustomerContext);
	
}
