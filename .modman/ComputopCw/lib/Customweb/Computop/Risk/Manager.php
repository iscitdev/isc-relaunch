<?php 
/**
  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2015 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */



/**
 * 
 * @author Thomas Hunziker
 * @Bean
 *
 */
class Customweb_Computop_Risk_Manager {
	
	/**
	 * @var Customweb_Computop_Risk_Service_Factory
	 */
	private $serviceFactory = null;
	
	public function __construct(Customweb_Computop_Risk_Service_Factory $factory) {
		$this->serviceFactory = $factory;
	}

	/**
	 * Returns a list of elements, which may be filled in by the customer.
	 * 
	 * @param Customweb_Payment_Authorization_IOrderContext $orderContext
	 * @param Customweb_Payment_Authorization_IPaymentCustomerContext $paymentCustomerContext
	 * @return Customweb_Form_IElement[]
	 */
	public function getVisibleFormElements(Customweb_Payment_Authorization_IOrderContext $orderContext, Customweb_Payment_Authorization_IPaymentCustomerContext $paymentCustomerContext) {
		if (!$this->isRiskManagementActive($orderContext)) {
			return array();
		}
		
		return $this->getServiceFactory()->getService($orderContext)->getFormElements($orderContext, $paymentCustomerContext);
	}
	
	/**
	 * Validates the given transaction.
	 * 
	 * @param Customweb_Computop_Authorization_Transaction $transaction
	 * @param array $httpRequestParameters
	 * @throws Exception In case something is not valid.
	 * @return boolean Constant true
	 */
	public function checkTransaction(Customweb_Computop_Authorization_Transaction $transaction, array $httpRequestParameters) {
		if (!$this->isRiskManagementActive($transaction->getTransactionContext()->getOrderContext())) {
			return true;
		}
		
		try {
			$this->getServiceFactory()->getService($transaction->getTransactionContext()->getOrderContext())->check($transaction, $httpRequestParameters);
		} catch(Customweb_Computop_Risk_Exception_ServiceOutageException $e) {
			$paymentMethod = $transaction->getPaymentMethod();
			if ($paymentMethod->existsPaymentMethodConfigurationValue('risk_service_outage') && $paymentMethod->getPaymentMethodConfigurationValue('risk_service_outage') == 'allow') {
				$transaction->addErrorMessage($e->getErrorMessage());
				return true;
			}
			else {
				throw $e;
			}
		}
		
		return true;
	}
	
	
	/**
	 * Checks whether the risk mananagement is active for a given order context.
	 * 
	 * @param Customweb_Payment_Authorization_IOrderContext $orderContext
	 * @return boolean
	 */
	protected function isRiskManagementActive(Customweb_Payment_Authorization_IOrderContext $orderContext){
		$paymentMethod = $orderContext->getPaymentMethod();
		
		if ($paymentMethod->existsPaymentMethodConfigurationValue('risk_service') && $paymentMethod->getPaymentMethodConfigurationValue("risk_service") != 'disabled') {
			if ($paymentMethod->existsPaymentMethodConfigurationValue("min_amount_risk_management")) {
				if ($paymentMethod->getPaymentMethodConfigurationValue("min_amount_risk_management") <= $orderContext->getOrderAmountInDecimals()) {
					return true;
				}
				else {
					return false;
				}
			}
			else {
				return true;
			}
		}
		
		return false;
	}

	/**
	 * @return Customweb_Computop_Risk_Service_Factory
	 */
	protected function getServiceFactory(){
		return $this->serviceFactory;
	}
	
}