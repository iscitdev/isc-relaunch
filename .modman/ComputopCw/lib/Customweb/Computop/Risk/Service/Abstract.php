<?php 
/**
  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2015 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */

//require_once 'Customweb/Computop/Risk/Service/Request.php';
//require_once 'Customweb/Payment/Util.php';
//require_once 'Customweb/Payment/Authorization/ErrorMessage.php';
//require_once 'Customweb/Computop/Risk/IService.php';
//require_once 'Customweb/I18n/Translation.php';
//require_once 'Customweb/Computop/Risk/Exception/OrderRejectException.php';


/**
 * Abstract implementation of a service endpoint for checking a transaction 
 * regarding the risk. The class is responsible for caching the response result and 
 * for the process flow including the execution of HTTP request to the remote service.
 * 
 * @author Thomas Hunziker
 *
 */
abstract class Customweb_Computop_Risk_Service_Abstract implements Customweb_Computop_Risk_IService {
	
	const RESPONSE_RESULT_LEVEL_RED = 'red';
	const RESPONSE_RESULT_LEVEL_YELLOW = 'yellow';
	const RESPONSE_RESULT_LEVEL_GREEN = 'green';
	const RESPONSE_RESULT_LEVEL_NO_RESULT = 'no_result';
	
	
	/**
	 * @var Customweb_Storage_IBackend
	 */
	private $storage = null;
	
	/**
	 * @var Customweb_Computop_Configuration
	 */
	private $configuration = null;
	
	/**
	 * @var Customweb_Computop_Method_Factory
	 */
	private $paymentMethodFactory = null;
	
	public function __construct(Customweb_Storage_IBackend $storage, Customweb_Computop_Configuration $configuration, Customweb_Computop_Method_Factory $factory) {
		$this->storage = $storage;
		$this->configuration = $configuration;
		$this->paymentMethodFactory = $factory;
	}
	
	/**
	 * The URL on which the service is accessible. 
	 * 
	 * @return string Service Endpoint URL
	 */
	abstract protected function getServiceUrl();
	
	/**
	 * Evaluates the processed response of the remote service. This method
	 * may throw a Customweb_Computop_Risk_Exception_OrderRejectException
	 * exception in case the transaction should be rejected.
	 * 
	 * @param mixed $response The response from the remote service.
	 * @throws Customweb_Computop_Risk_Exception_OrderRejectException Throws exception in case the transaction should be rejected.
	 * @return boolean Static true
	 */
	abstract protected function evaluateResponse(Customweb_Computop_Authorization_Transaction $transaction, $response);
	
	public function check(Customweb_Computop_Authorization_Transaction $transaction, array $httpRequestParameters) {
		
		$response = $this->loadServiceResponseFromCache($transaction, $httpRequestParameters);
		if ($response === null) {
			$parameters = array_merge($this->getDynamicParameters($transaction, $httpRequestParameters), $this->getStaticParameters($transaction, $httpRequestParameters));
			$request = new Customweb_Computop_Risk_Service_Request($this->getConfiguration(), $parameters, $this->getServiceUrl());
			$request->process();
			$response = $this->processRequest($request);
			$this->writeServiceResponseToCache($transaction, $httpRequestParameters, $response);
		}
		
		return $this->evaluateResponse($transaction, $response);
	}

	public function getFormElements(Customweb_Payment_Authorization_IOrderContext $orderContext, Customweb_Payment_Authorization_IPaymentCustomerContext $paymentCustomerContext) {
		return array();
	}
		
	/**
	 * This method returns a list of parameters which are always the same from
	 * request to request for the same customer. This parameters are used as 
	 * the primary key to cache the response locally.
	 * 
	 * @param Customweb_Computop_Authorization_Transaction $transaction
	 * @param array $httpRequestParameters
	 * @return multitype:
	 */
	protected function getStaticParameters(Customweb_Computop_Authorization_Transaction $transaction, array $httpRequestParameters) {
		return array();
	}
	
	/**
	 * This method returns the parameters which differs from request to request even it is the
	 * same customer. 
	 * 
	 * @param Customweb_Computop_Authorization_Transaction $transaction
	 * @param array $httpRequestParameters
	 * @return multitype:string
	 */
	protected function getDynamicParameters(Customweb_Computop_Authorization_Transaction $transaction, array $httpRequestParameters) {
		$parameters = array();
		$parameters['MerchantID'] = $this->getConfiguration()->getMerchantId();
		$parameters['TransID'] = Customweb_Payment_Util::applyOrderSchema(
				$this->getConfiguration()->getTransactionIdSchema(),
				$transaction->getExternalTransactionId(),
				63
		) . 'V';
		$parameters['RefNr'] = $parameters['RefNr'] = Customweb_Payment_Util::applyOrderSchema(
				$this->getConfiguration()->getTransactionIdSchema(),
				$transaction->getExternalTransactionId(),
				29
		) . 'V';
		$paymentMethod = $this->getPaymentMethodByTransaction($transaction);
		
		if (method_exists($paymentMethod, 'getShortDescription')) {
			$parameters['OrderDesc'] = $paymentMethod->getShortDescription($transaction);
		}
		else {
			$parameters['OrderDesc'] = $this->getConfiguration()->getShopDescription();
		}
	
		return $parameters;
	}
	
	/**
	 * This method extracts the parameters, which should be stored in the cache. They 
	 * are passed to the self::evaluateResponse() method. Subclasses may override this.
	 * 
	 * @param Customweb_Computop_Risk_Service_Request $request
	 * @return mixed
	 */
	protected function processRequest(Customweb_Computop_Risk_Service_Request $request) {
		return $request->getResponseParameters();
	}
	
	/**
	 * Loads the response from the cache backend.
	 * 
	 * @param Customweb_Computop_Authorization_Transaction $transaction
	 * @param array $httpRequestParameters
	 * @return object|NULL
	 */
	protected function loadServiceResponseFromCache(Customweb_Computop_Authorization_Transaction $transaction, array $httpRequestParameters) {
		$staticParameters = $this->getStaticParameters($transaction, $httpRequestParameters);
		$cacheKey = $this->getCacheKey($staticParameters);
		$spaceKey = $this->getCacheSpaceKey();
		$result = $this->getStorage()->read($spaceKey, $cacheKey);
		
		if ($result !== null && isset($result['last_update']) && isset($result['response'])) {
			$lastUpdate = $result['last_update'];
			$cacheDays = $transaction->getPaymentMethod()->getPaymentMethodConfigurationValue('risk_management_cache');
			if (($lastUpdate + $cacheDays * 86400) > time()) {
				return $result['response'];
			}
		}
		return null;
	}
	
	/**
	 * Stores the response into the storage backend.
	 * 
	 * @param Customweb_Computop_Authorization_Transaction $transaction
	 * @param array $httpRequestParameters
	 * @param unknown $response
	 */
	protected function writeServiceResponseToCache(Customweb_Computop_Authorization_Transaction $transaction, array $httpRequestParameters, $response) {
		$staticParameters = $this->getStaticParameters($transaction, $httpRequestParameters);
		$cacheKey = $this->getCacheKey($staticParameters);
		$spaceKey = $this->getCacheSpaceKey();
		
		$result = array(
			'last_update' => time(),
			'response' => $response,
		);
		
		$this->getStorage()->write($spaceKey, $cacheKey, $result);
	}
	
	protected final function getCacheSpaceKey() {
		return get_class($this);
	}
	
	protected final function getCacheKey(array $staticParameters) {
		return hash('sha512', implode('_', $staticParameters));
	}
	
	
	/**
	 * @return Customweb_Storage_IBackend
	 */
	protected function getStorage() {
		return $this->storage;
	}

	protected function getConfiguration(){
		return $this->configuration;
	}

	protected function getPaymentMethodFactory(){
		return $this->paymentMethodFactory;
	}
	
	/**
	 * @param Customweb_Computop_Authorization_Transaction $transaction
	 * @return Customweb_Computop_Method_IMethod
	 */
	protected function getPaymentMethodByTransaction(Customweb_Computop_Authorization_Transaction $transaction){
		return $this->getPaymentMethodFactory()->getPaymentMethod($transaction->getTransactionContext()->getOrderContext()->getPaymentMethod(), $transaction->getAuthorizationMethod());
	}
	
	/**
	 * This method throws depending on the given category a exception or not. A category
	 * is one of:
	 * - RED
	 * - YELLOW
	 * - GREEN
	 * - NO_RESULT
	 * @param unknown $category
	 */
	protected function evaluateResponseLevel(Customweb_Computop_Authorization_Transaction $transaction, $level, $description) {
		$paymentMethod = $transaction->getPaymentMethod();
		$allowedLevels = $paymentMethod->getPaymentMethodConfigurationValue("allowed_risk_levels");
		
		if(!in_array(strtolower($level), $allowedLevels)){
			$userMessage = Customweb_I18n_Translation::__(
				"The payment method '!paymentMethodName' is not available to you. Please select another payment method.",
				array(
					'!paymentMethodName' => $paymentMethod->getPaymentMethodDisplayName(),
				)
			);
			$backendMessage = Customweb_I18n_Translation::__(
					"The risk management has not been passed because of a not allowed level. The level is '!level' and the reason is '!description'.",
					array(
						'!level' => $level,
						'!description' => $description,
					)
			);
			throw new Customweb_Computop_Risk_Exception_OrderRejectException(new Customweb_Payment_Authorization_ErrorMessage(
					$userMessage, $backendMessage
			));
		}
		
		return true;
	}
	
}