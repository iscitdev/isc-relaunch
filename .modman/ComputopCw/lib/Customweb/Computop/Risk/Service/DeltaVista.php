<?php 
/**
  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2015 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */

//require_once 'Customweb/Util/Address.php';
//require_once 'Customweb/Core/Util/System.php';
//require_once 'Customweb/Util/String.php';
//require_once 'Customweb/Computop/Util.php';
//require_once 'Customweb/Computop/Risk/Service/Abstract.php';


/**
 * @Service(name='DeltaVista')
 */
class Customweb_Computop_Risk_Service_DeltaVista extends Customweb_Computop_Risk_Service_Abstract{
	
	protected function getStaticParameters(Customweb_Computop_Authorization_Transaction $transaction, array $httpRequestParameters) {
		$parameters = parent::getStaticParameters($transaction, $httpRequestParameters);
		
		$orderContext = $transaction->getTransactionContext()->getOrderContext();
		$parameters['Amount'] =  Customweb_Computop_Util::formatAmount(
				$orderContext->getOrderAmountInDecimals(),
				$orderContext->getCurrencyCode()
		);
		$parameters['Currency'] = $orderContext->getCurrencyCode();
		
		$parameters['ProductName'] = $this->getProductName($transaction, $httpRequestParameters);
		$parameters['CustomerID'] = Customweb_Util_String::substrUtf8($orderContext->getCustomerId(), 0, 30);
		$parameters['LegalForm'] = $this->getLegalForm($transaction, $httpRequestParameters);
		$parameters['LastName'] = Customweb_Util_String::substrUtf8($orderContext->getBillingLastName(), 0, 30);
		$parameters['FirstName'] = Customweb_Util_String::substrUtf8($orderContext->getBillingFirstName(), 0, 30);
		$address = Customweb_Util_Address::splitStreet($orderContext->getBillingStreet(), $orderContext->getBillingCountryIsoCode(), $orderContext->getBillingPostCode());
		$parameters['AddrStreet'] = Customweb_Util_String::substrUtf8($address['street'], 0, 50);
		$parameters['AddrStreetNr'] = Customweb_Util_String::substrUtf8($address['street-number'], 0, 15);
		$parameters['AddrCity'] = Customweb_Util_String::substrUtf8($orderContext->getBillingCity(), 0, 40);
		$parameters['AddrZip'] = Customweb_Util_String::substrUtf8($orderContext->getBillingPostCode(), 0, 10);
		$parameters['IPAddr'] = Customweb_Core_Util_System::getClientIPAddress();
		
		return $parameters;
	}
	
	protected function getServiceUrl() {
		return 'deltavista.aspx';
	}
	
	protected function evaluateResponse(Customweb_Computop_Authorization_Transaction $transaction, $response) {
		$level = self::RESPONSE_RESULT_LEVEL_NO_RESULT;
		if(isset($response['Result'])){
			$rs = strtolower($response['Result']);
			$map = array(
				'no result' => self::RESPONSE_RESULT_LEVEL_NO_RESULT, 
				'green' => self::RESPONSE_RESULT_LEVEL_GREEN,
				'red' => self::RESPONSE_RESULT_LEVEL_RED,
				'yellow' => self::RESPONSE_RESULT_LEVEL_YELLOW,
			);
			if (isset($map[$rs])) {
				$level = $map[$rs];
			}
		}
		
		return $this->evaluateResponseLevel($transaction, $level, $response['Description']);		
	}
	
	private function getProductName(Customweb_Computop_Authorization_Transaction $transaction, array $httpRequestParameters) {
		$paymentMethod = $transaction->getPaymentMethod();
		$legalMap = array('UNKNOWN' => 'Consumer', 'PERSON' => 'Consumer', 'COMPANY' => 'Business');
		$checkMethod =	$paymentMethod->getPaymentMethodConfigurationValue('deltavista_inquiry_method');
		$legalForm = $legalMap[$this->getLegalForm($transaction, $httpRequestParameters)];
		return $checkMethod . $legalForm;
	}

	private function getLegalForm(Customweb_Computop_Authorization_Transaction $transaction, array $httpRequestParameters){
		$company = $transaction->getTransactionContext()->getOrderContext()->getBillingCompanyName();
		if(strlen($company) > 0){
			return 'COMPANY';
		}
		else{
			return 'PERSON';
		}
	}
	
}