<?php 
/**
  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2015 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */

//require_once 'Customweb/DependencyInjection/Bean/Provider/Annotation/Util.php';
//require_once 'Customweb/Core/String.php';
//require_once 'Customweb/Computop/Risk/Annotation/Service.php';
//require_once 'Customweb/Annotation/Scanner.php';
//require_once 'Customweb/DependencyInjection/Container/Extendable.php';


/**
 * 
 * @author Thomas Hunziker
 * @Bean
 *
 */
class Customweb_Computop_Risk_Service_Factory {
	
	/**
	 * @var Customweb_Computop_Risk_Annotation_Service[]
	 */
	private $serviceAnnotations = null;
	
	/**
	 * @var Customweb_Computop_Risk_IService[]
	 */
	private $instances = array();
	
	/**
	 * @var Customweb_DependencyInjection_IContainer
	 */
	private $container = null;
	
	public function __construct(Customweb_DependencyInjection_IContainer $container) {
		$this->container = $container;
	}
	
	/**
	 * 
	 * @param Customweb_Payment_Authorization_IOrderContext $orderContext
	 * @return Customweb_Computop_Risk_IService
	 */
	public function getService(Customweb_Payment_Authorization_IOrderContext $orderContext) {
		$className = $this->getServiceClassName($orderContext);
		
		if (!isset($this->instances[$className])) {
			$bean = Customweb_DependencyInjection_Bean_Provider_Annotation_Util::createBeanInstance($className, $className);
			$container = new Customweb_DependencyInjection_Container_Extendable($this->getContainer());
			$this->instances[$className] = $bean->getInstance($container);
		}
		
		return $this->instances[$className];
	}
	
	/**
	 * @param Customweb_Payment_Authorization_IOrderContext $orderContext
	 * @throws Exception
	 * @return string
	 */
	protected function getServiceClassName(Customweb_Payment_Authorization_IOrderContext $orderContext) {
		$serviceName = $orderContext->getPaymentMethod()->getPaymentMethodConfigurationValue('risk_service');
		$serviceNameKey = strtolower($serviceName);
		
		$map = $this->getAnnotationMap();
		foreach ($map as $className => $annotation) {
			if (strtolower($annotation->getName()) == $serviceNameKey) {
				return $className;		
			}
		}
		
		throw new Exception(Customweb_Core_String::_("Unable to find a service implementation for '@serviceName'.")->format(array('@serviceName' => $serviceName)));
	}
	
	/**
	 * Returns a list of payment methods found in the packages.
	 *
	 * @return Customweb_Computop_Risk_Annotation_Service[]
	 */
	final protected function getAnnotationMap() {
		if ($this->serviceAnnotations === null) {
			$this->serviceAnnotations = array();
			$scanner = new Customweb_Annotation_Scanner();
				
			$annotations = $scanner->find(
				'Customweb_Computop_Risk_Annotation_Service',
				array(
					'Customweb_Computop_Risk_Service',
				)
			);
	
			foreach ($annotations as $className => $annotation) {
				if ($annotation instanceof Customweb_Computop_Risk_Annotation_Service) {
					$this->serviceAnnotations[$className] = $annotation;
				}
			}
		}
		return $this->serviceAnnotations;
	}

	/**
	 * @return Customweb_DependencyInjection_IContainer
	 */
	protected function getContainer(){
		return $this->container;
	}
	
	
	
	
}