<?php 
/**
  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2015 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */

//require_once 'Customweb/Util/Address.php';
//require_once 'Customweb/Util/String.php';
//require_once 'Customweb/Computop/ElementFactory.php';
//require_once 'Customweb/Computop/Risk/Service/Abstract.php';


/**
 * @Service(name='PayProtect')
 */
class Customweb_Computop_Risk_Service_PayProtect  extends Customweb_Computop_Risk_Service_Abstract{
	
	
	public function getFormElements(Customweb_Payment_Authorization_IOrderContext $orderContext, Customweb_Payment_Authorization_IPaymentCustomerContext $paymentCustomerContext) {
		$elements = parent::getFormElements($orderContext, $paymentCustomerContext);
		$customerMap = $paymentCustomerContext->getMap();
		
		if($orderContext->getBillingSalutation() == null && !isset($customerMap['salutation'])){
			$elements[] = Customweb_Computop_ElementFactory::getSalutationElement('Salutation');
		}
		
		return $elements;
	}
	
	protected function getStaticParameters(Customweb_Computop_Authorization_Transaction $transaction, array $httpRequestParameters) {
		$parameters = parent::getStaticParameters($transaction, $httpRequestParameters);
		
		$orderContext = $this->getOrderContext();
		$parameters['Salutation'] = Customweb_Util_String::substrUtf8($this->getSalutation($transaction, $httpRequestParameters), 0, 50);
		$parameters['FirstName'] = Customweb_Util_String::substrUtf8($orderContext->getBillingFirstName(), 0, 50);
		$parameters['LastName'] = Customweb_Util_String::substrUtf8($orderContext->getBillingLastName(), 0, 50);
		$address = Customweb_Util_Address::splitStreet($orderContext->getBillingStreet(), $orderContext->getBillingCountryIsoCode(), $orderContext->getBillingPostCode());
		$parameters['AddrStreet'] = Customweb_Util_String::substrUtf8($address['street'], 0, 50);
		$parameters['AddrStreetNr'] = Customweb_Util_String::substrUtf8($address['street-number'], 0, 15);
		
		$parameters['AddrZip'] = Customweb_Util_String::substrUtf8($orderContext->getBillingPostCode(), 0, 10);
		$parameters['AddrCity'] = Customweb_Util_String::substrUtf8($orderContext->getBillingCity(), 0, 40);
		$parameters['AddrCountryCode'] = $orderContext->getBillingCountryIsoCode();
		
		$parameters['CustomerID'] = Customweb_Util_String::substrUtf8($orderContext->getCustomerId(), 0, 30);
		$parameters['EventToken'] = 'B';
		$parameters['e-mail'] = Customweb_Util_String::substrUtf8($orderContext->getCustomerEMailAddress(), 0, 80);
		
		return $parameters;
	}
	
	protected function getServiceUrl() {
		return 'payprotect.aspx';
	}
	
	protected function evaluateResponse(Customweb_Computop_Authorization_Transaction $transaction, $response) {
		$resultMapping = array('rot' => self::RESPONSE_RESULT_LEVEL_RED, 'gruen' => self::RESPONSE_RESULT_LEVEL_GREEN);
		$level = self::RESPONSE_RESULT_LEVEL_NO_RESULT;
		if(isset($response['Result'])){
			$level = $resultMapping[strtolower($response['Result'])];
		}
		
		return $this->evaluateResponseLevel($transaction, $level, $response['Description']);
	}
	
	private function getSalutation(Customweb_Computop_Authorization_Transaction $transaction, array $httpRequestParameters){
		$orderContext = $transaction->getTransactionContext()->getOrderContext();
		if($orderContext->getBillingSalutation()){
			return $orderContext->getBillingSalutation();
		}
		
		/* @var $paymentCustomerContext Customweb_Payment_Authorization_IPaymentCustomerContext */
		$paymentCustomerContext = $transaction->getPaymentCustomerContext();
		if (isset($httpRequestParameters['Salutation'])) {
			$paymentCustomerContext->updateMap(array(
				'salutation' => $httpRequestParameters['Salutation'],
			));
		}
		$map = $paymentCustomerContext->getMap();
		
		return $map['salutation'];
	}
}


