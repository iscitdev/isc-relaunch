<?php 
/**
  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2015 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */

//require_once 'Customweb/Computop/Risk/Exception/ServiceOutageException.php';
//require_once 'Customweb/Computop/Util.php';
//require_once 'Customweb/Computop/RemoteRequest/Abstract.php';
//require_once 'Customweb/Payment/Authorization/ErrorMessage.php';
//require_once 'Customweb/I18n/Translation.php';


class Customweb_Computop_Risk_Service_Request extends Customweb_Computop_RemoteRequest_Abstract{
	
	/**
	 * @var array
	 */
	private $parameters = null;
	
	/**
	 * @var string
	 */
	private $url = null;
	
	public function __construct(Customweb_Computop_Configuration $configuration, array $parameters, $remoteFile) {
		parent::__construct($configuration);
		$this->parameters = $parameters;
		
		if (substr($remoteFile, 0, 4) != 'http') {
			$this->url = $this->getConfiguration()->getBaseUrl() . $remoteFile;
		}
		else {
			$this->url = $remoteFile;
		}
	}
	
	protected function getRequestUrl() {
		return $this->url;
	}
	
	protected function getRequestParameters() {
		return $this->parameters;
	}
	
	protected function processResponse() {
		$parameters = array_change_key_case($this->getResponseParameters(), CASE_LOWER);
		if (!Customweb_Computop_Util::isStatusSuccessful($parameters['status'])) {
			
			if (!isset($parameters['code'])) {
				$parameters['code'] = 'unkown code';
			}
			
			throw new Customweb_Computop_Risk_Exception_ServiceOutageException(new Customweb_Payment_Authorization_ErrorMessage(Customweb_I18n_Translation::__(
					"Request failed because of !description (Code: !code)",
					array(
						'!code' => $parameters['code'],
						'!description' => $parameters['description'],
					)
			)));
		}
	}
	
}