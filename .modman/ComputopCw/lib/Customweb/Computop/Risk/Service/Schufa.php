<?php 
/**
  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2015 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */

//require_once 'Customweb/Util/String.php';
//require_once 'Customweb/Computop/Risk/Service/Abstract.php';
//require_once 'Customweb/I18n/Translation.php';
//require_once 'Customweb/Form/ElementFactory.php';
//require_once 'Customweb/Computop/Method/Util.php';


/**
 * @Service(name='Schufa')
 */
class Customweb_Computop_Risk_Service_Schufa  extends Customweb_Computop_Risk_Service_Abstract{
	
	
	public function getFormElements(Customweb_Payment_Authorization_IOrderContext $orderContext, Customweb_Payment_Authorization_IPaymentCustomerContext $paymentCustomerContext) {
		$elements = parent::getFormElements($orderContext, $paymentCustomerContext);
		$customerMap = $paymentCustomerContext->getMap();
		if($orderContext->getBillingDateOfBirth() == null){
			$elements[] = Customweb_Form_ElementFactory::getDateOfBirthElement("dobYear", "dobMonth", "dobDay");
		}
		return $elements;
	}

	protected function getStaticParameters(Customweb_Computop_Authorization_Transaction $transaction, array $httpRequestParameters) {
		$parameters = parent::getStaticParameters($transaction, $httpRequestParameters);
		$orderContext = $transaction->getTransactionContext()->getOrderContext();
		$parameters['Firstname'] = Customweb_Util_String::substrUtf8($orderContext->getBillingFirstName(), 0, 32);
		$parameters['Surname'] = Customweb_Util_String::substrUtf8($orderContext->getBillingLastName(), 0, 32);
		$parameters['Birthdate'] = $this->getDateOfBirth($transaction, $httpRequestParameters);
		$parameters['AddrStreet'] = Customweb_Util_String::substrUtf8($orderContext->getBillingStreet(), 0, 32);
		$parameters['AddrZip'] = Customweb_Util_String::substrUtf8($orderContext->getBillingPostCode(), 0, 5);
		$parameters['AddrCity'] = Customweb_Util_String::substrUtf8($orderContext->getBillingCity(), 0, 32);
		
		return $parameters;
	}
	
	protected function getDynamicParameters(Customweb_Computop_Authorization_Transaction $transaction, array $httpRequestParameters) {
		$parameters = parent::getDynamicParameters($transaction, $httpRequestParameters);
		unset($parameters['RefNr']);
		return $parameters;
	}
	
	protected function getServiceUrl() {
		return 'SchufaIdentDirect.aspx';
	}
	
	protected function processRequest(Customweb_Computop_Risk_Service_Request $request) {
		$parameters = $request->getResponseParameters();
	
		if (isset($parameters['Match'])) {
			return array(
				'Match' => $parameters['Match'],
				'Description' => $parameters['Description'],
			);
		}
		else {
			return self::RESPONSE_RESULT_LEVEL_NO_RESULT;
		}
	}
	
	protected function evaluateResponse(Customweb_Computop_Authorization_Transaction $transaction, $response) {
		$level = $response;
		$description = Customweb_I18n_Translation::__('No result found');
		if ($level !== self::RESPONSE_RESULT_LEVEL_NO_RESULT) {
			$matches = explode(',', $response['Match']);
			$overallMatch = $matches[0];
			$paymentMethod = $transaction->getPaymentMethod();
			$greenPercentage = $paymentMethod->getPaymentMethodConfigurationValue('shufa_green_percentage');
			$yellowPercentage = $paymentMethod->getPaymentMethodConfigurationValue('schufa_yellow_percentage');
				
			if($overallMatch >= $greenPercentage){
				$levle = self::RESPONSE_RESULT_LEVEL_GREEN;
			}
			elseif($overallMatch >= $yellowPercentage){
				$levle = self::RESPONSE_RESULT_LEVEL_YELLOW;
			}
			else{
				$levle = self::RESPONSE_RESULT_LEVEL_RED;
			}
			$description = $response['Description'];
		}
		$this->evaluateResponseLevel($transaction, $level, $description);
	}
	
	private function getDateOfBirth(Customweb_Computop_Authorization_Transaction $transaction, array $httpRequestParameters){
		$orderContext = $transaction->getTransactionContext()->getOrderContext();
		return Customweb_Computop_Method_Util::getDateOfBirth($orderContext, $httpRequestParameters)->format('d.m.Y');
	}
	
}