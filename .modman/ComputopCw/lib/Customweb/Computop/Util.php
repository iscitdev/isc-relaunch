<?php

/**
 *  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2015 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */

//require_once 'Customweb/Computop/Blowfish.php';
//require_once 'Customweb/Util/Currency.php';
//require_once 'Customweb/Payment/Util.php';
//require_once 'Customweb/Http/Request.php';
//require_once 'Customweb/Payment/Authorization/ErrorMessage.php';
//require_once 'Customweb/I18n/Translation.php';

final class Customweb_Computop_Util {
	private static $notEncryptedParameters = array(
		'Template' => 'Template',
		'CCCVC' => 'CCCVC',
		'CCExpiry' => 'CCExpiry',
		'CCNr' => 'CCNr',
		'Language' => 'Language',
		'language' => 'language' 
	);
	private static $userMessages = null;

	private function __construct(){
		// prevent any instantiation of this class
	}

	public static function getTransactionID(Customweb_Computop_Configuration $configuration, Customweb_Computop_Authorization_Transaction $transaction){
		return Customweb_Payment_Util::applyOrderSchema($configuration->getTransactionIdSchema(), $transaction->getExternalTransactionId(), 64);
	}

	public static function getShortDescription(Customweb_Computop_Configuration $configuration, Customweb_Computop_Authorization_Transaction $transaction){
		$description = $configuration->getShopDescription($transaction->getTransactionContext()->getOrderContext()->getLanguage());
		$description = str_replace('{id}', $transaction->getExternalTransactionId(), $description);
		$description = str_replace('{ID}', $transaction->getExternalTransactionId(), $description);
		
		return $description;
	}

	public static function isStatusSuccessful($status){
		$status = strtolower($status);
		if ($status == 'authorized' || $status == 'ok' || $status == 'pending' || $status == 'authorize_request' || $status == 'init') {
			return true;
		}
		else {
			return false;
		}
	}

	public static function encryptParameters($parameters, Customweb_Computop_Configuration $configuration){
		$parametersToEncrypt = array();
		$unsecuredParameters = array();

		foreach($parameters as $key => $value){
			if (is_array($value)) {
				throw new Exception("There is a parameter which is an array, but only strings can be used. Key Name: " . $key);
			}
			if(self::isEncryptionNeeded($key)){
				$parametersToEncrypt[$key] = utf8_decode($value);
			}
			else {
				$unsecuredParameters[$key] = utf8_decode($value);
			}
		}
		return self::encryptParametersInner($configuration, $unsecuredParameters, $parametersToEncrypt);
	}
	
	public static function encryptParametersPaymorrow($parameters, Customweb_Computop_Configuration $configuration){
		$notToEncrypt = array(
			'Template' => 'Template',
			'CCCVC' => 'CCCVC',
			'CCExpiry' => 'CCExpiry',
			'CCNr' => 'CCNr'
		);
		$parametersToEncrypt = array();
		$unsecuredParameters = array();
	
		foreach ($parameters as $key => $value) {
			if (!isset($notToEncrypt[$key])) {
				$parametersToEncrypt[$key] = utf8_decode($value);
			}
			else {
				$unsecuredParameters[$key] = utf8_decode($value);
			}
		}
		return self::encryptParametersInner($configuration, $unsecuredParameters, $parametersToEncrypt);
	}

	private static function encryptParametersInner(Customweb_Computop_Configuration $configuration, array $unsecuredParameters, array $parametersToEncrypt){
		$finalParameters = array();
		$encryptor = new Customweb_Computop_Blowfish();
		$stringToEncrypt = self::parametersToString($parametersToEncrypt);
		$finalParameters['MerchantID'] = $parametersToEncrypt['MerchantID'];
		$finalParameters['Len'] = strlen($stringToEncrypt);
		$finalParameters['Data'] = $encryptor->ctEncrypt($stringToEncrypt, $finalParameters['Len'], $configuration->getEncryptionKey());
		return array_merge($unsecuredParameters, $finalParameters);
	}

	protected static function parametersToString($parameters){
		$items = array();
		foreach ($parameters as $key => $value) {
			$items[] = $key . '=' . $value;
		}
		return implode('&', $items);
	}

	private static function isEncryptionNeeded($key){
		if (isset(self::$notEncryptedParameters[$key])) {
			return false;
		}
		else {
			return true;
		}
	}

	public static function getCleanLanguageCode($lang){
		$supportedLanguages = array(
			'de_DE',
			'en_US',
			'fr_FR',
			'da_DK',
			'cs_CZ',
			'es_ES',
			'hr_HR',
			'it_IT',
			'hu_HU',
			'nl_NL',
			'no_NO',
			'pl_PL',
			'pt_PT',
			'ru_RU',
			'ro_RO',
			'sk_SK',
			'sl_SI',
			'fi_FI',
			'sv_SE',
			'tr_TR',
			'el_GR',
			'ja_JP',
			'bs_BA',
			'bg_BG',
			'zh_CN',
			'sr_RS',
			'uk_UA' 
		);
		
		return substr(Customweb_Payment_Util::getCleanLanguageCode($lang, $supportedLanguages), 0, 2);
	}

	public static function getRequestFingerprint(Customweb_Computop_Configuration $configuration, array $parameters, $algorithm = 'md5'){
		$stringToHash = "";
		foreach ($parameters as $key => $value) {
			$stringToHash .= $value;
		}
		return hash($algorithm, $stringToHash);
	}

	public static function sortParameters($order, $parameters){
		$keys = explode(',', $order);
		$sortedParameters = array();
		foreach ($keys as $key) {
			$sortedParameters[] = $parameters[$key];
		}
		return $sortedParameters;
	}

	public static function formatAmount($amount, $currency){
		return (int) Customweb_Util_Currency::formatAmount($amount, $currency, '', '');
	}

	public static function readAmount($amount, $currency){
		$digits = Customweb_Util_Currency::getDecimalPlaces($currency);
		return $amount / $digits;
	}

	public static function sendPostRequest($url, array $parameters){
		$request = new Customweb_Http_Request($url);
		$request->setBody($parameters);
		$request->setMethod('POST');
		$request->appendCustomHeaders(array(
			'Content-Type' => 'application/x-www-form-urlencoded' 
		));
		$request->send();
		
		$handler = $request->getResponseHandler();
		if ($handler->getStatusCode() != '200') {
			throw new Exception("The server response with a invalid HTTP status code (status code != 200).");
		}
		
		return $handler->getBody();
	}

	public static function parseResponse($response){
		$array = array();
		parse_str($response, $array);
		return $array;
	}

	public static function generateHtmlForm($actionUrl, $parameters){
		$html = '
			<form action="' . $actionUrl . '" method="POST" name="process_form">';
		
		foreach ($parameters as $key => $value) {
			$html .= '<input type="hidden" name="' . $key . '" value="' . $value . '" />';
		}
		
		$html .= '<button title="' . Customweb_I18n_Translation::__("Continue") . '" type="submit" class="scalable go" style="">
				<span>
					<span>
						<span>' . Customweb_I18n_Translation::__("Continue") . '</span>
					</span>
				</span>
			</button>
		</form>';
		
		$html .= '<script type="text/javascript">
			window.onload = function(){
				document.process_form.submit();
			};
			</script>';
		
		return $html;
	}

	public static function generateSpecificErrorMessages(array $parameters){
		$adminMessage = Customweb_I18n_Translation::__("The transaction failed with an unkown error.");
		if (isset($parameters['Description']) || isset($parameters['Code'])) {
			$description = isset($parameters['Description']) ? $parameters['Description'] : '';
			$errorCode = isset($parameters['Code']) ? $parameters['Code'] : '';
			$adminMessage = Customweb_I18n_Translation::__("Payment processing failure !code with message: !error", 
					array(
						'!code' => $errorCode,
						'!error' => $description 
					));
		}
		
		$userMessage = Customweb_I18n_Translation::__("The payment failed. Please contact us to find out what is going on with your order.");
		
		if (isset($parameters['Code']) && strlen($parameters['Code']) == 8) {
			$subfix = substr($parameters['Code'], 4);
			$resolvedMessage = self::resolveUserMessage($subfix);
			if ($resolvedMessage !== null) {
				$userMessage = $resolvedMessage;
			}
		}
		
		return new Customweb_Payment_Authorization_ErrorMessage($userMessage, $adminMessage);
	}

	private static function resolveUserMessage($errorCode){
		if (self::$userMessages === null) {
			self::$userMessages = array(
				'1106' => Customweb_I18n_Translation::__('The provided card data seems to be wrong.'),
				'1107' => Customweb_I18n_Translation::__('The provided card is expired.'),
				'1462' => Customweb_I18n_Translation::__('The payment was not accepted. Please check your address data.'),
				'1112' => Customweb_I18n_Translation::__('The order with this payment method and with this amount seems not to be possible.'),
				'1112' => Customweb_I18n_Translation::__('The order with this payment method and with this amount seems not to be possible.'),
				'0045' => Customweb_I18n_Translation::__('The payment system seems to be overloaded. Please try it later.'),
				'0053' => Customweb_I18n_Translation::__('You have successfully cancelled the payment.'),
				'0305' => Customweb_I18n_Translation::__('Your payment was refused. Please try a different payment method.'),
				'0314' => Customweb_I18n_Translation::__('Your card is invalid.') 
			);
		}
		
		if (isset(self::$userMessages[$errorCode])) {
			return self::$userMessages[$errorCode];
		}
		else {
			return null;
		}
	}

	public static function decryptIfNeeded(Customweb_Computop_Configuration $configuration, array $parameters){
		if (isset($parameters['data'])) {
			$parameters['Data'] = $parameters['data'];
			unset($parameters['data']);
		}
		
		if (isset($parameters['len'])) {
			$parameters['Len'] = $parameters['len'];
			unset($parameters['len']);
		}
		
		if (isset($parameters['Data'])) {
			$decryptor = new Customweb_Computop_Blowfish();
			$parameterString = $decryptor->ctDecrypt($parameters['Data'], $parameters['Len'], $configuration->getEncryptionKey());
			parse_str($parameterString, $dataParameters);
			$parameters = array_merge($parameters, $dataParameters);
		}
		return $parameters;
	}
}