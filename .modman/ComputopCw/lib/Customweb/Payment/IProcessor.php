<?php
/**
  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2013 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.customweb.ch/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.customweb.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */



/**
 * This interface specifies the interface of a processor. The processor is the effective 
 * entity which process the transaction.
 * 
 * In certain cases it's required to collect additional data or send different data depending
 * on the processor.
 * 
 * @author Thomas Hunziker
 *
 */
interface Customweb_Payment_Method_IProcessor {
	
	/**
	 * This method filters the given set of form elements accoriding to the processor.
	 * 
	 * @param Customweb_Payment_Authorization_IOrderContext $orderContext The context of the order
	 * @param Customweb_Payment_Authorization_ITransaction $aliasTransaction The alias to use with this form field. If NULL no alias is used.
	 * @param Customweb_Payment_Authorization_ITransaction $failedTransaction A previous transaction which may provide additional information.
	 * @param Customweb_payment_authorization_IPaymentCustomerContext $paymentCustomerContext The payment customer context.
	 * @param Customweb_Form_IElement[] $elements List of visible form fields for this payment method.
	 * @return void
	 * @throws Exception
	 */
	public function filterVisibleFormElements(Customweb_Payment_Authorization_IOrderContext $orderContext, 
			$aliasTransaction, 
			$failedTransaction,
			$paymentCustomerContext, 
			array &$elements);
	
	/**
	 * This method filters the given authorization parameters accoring to the processor.
	 * 
	 * @param Customweb_Computop_Authorization_Transaction $transaction
	 * @param array $httpRequestParameters
	 * @param array $authorizationParameters
	 * @return void
	 * @throws Exception
	 */
	public function filterAuthorizationParameters(Customweb_Payment_Authorization_ITransaction $transaction, array $httpRequestParameters, array &$authorizationParameters);
	
}

