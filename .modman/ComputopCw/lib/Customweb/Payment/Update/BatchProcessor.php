<?php 
/**
  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2013 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.customweb.ch/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.customweb.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */

//require_once 'Customweb/Util/System.php';
//require_once 'Customweb/Payment/Update/AbstractLockProcessor.php';
//require_once 'Customweb/Util/String.php';
//require_once 'Customweb/Payment/Update/IHandler.php';


/**
 * This processor executes batches of transaction updates. The API checks which
 * transactions may be updated. Then on all this transactions a update is executed.
 * The determination if a transaction should be updated or not is done by the API.
 * The API may call a webservice which returns a list of transactions to update.
 * 
 * 
 * @author Thomas Hunziker / Simon Schurter
 *
 */
class Customweb_Payment_Update_BatchProcessor extends Customweb_Payment_Update_AbstractLockProcessor {
	
	private $transactionObject = null;
	private $transactionId = null;
	
	const STORAGE_BACKEND_SPACE = "batch_update_processor";
	const STORAGE_BACKEND_LAST_UPDATE_KEY = 'last_successful_update';
	
	public function process() {
		if ($this->getUpdateAdapter() === null) {
			return;
		}
		
		if (!$this->tryLockUpdate()){
			return;
		}
		
		$approximatelyExecutedTime = 4;
		$maxExecutionTime = Customweb_Util_System::getMaxExecutionTime() - $approximatelyExecutedTime;
		$start = $this->getStartTime();
		$maxEndtime = $maxExecutionTime + $start;
		
		try {
			$candidates = $this->getUpdateAdapter()->batchUpdate($this->getLastSuccessfulBatchUpdate());
			foreach ($candidates as $paymentId => $parameters) {
				if ($maxEndtime > time()) {
					$this->executeUpdate($paymentId, $parameters);
				}
				else {
					break;
				}
			}
			$this->setLastSuccessfulBatchUpdate(new DateTime('@'.$start));
		}
		catch(Exception $e) {
			$this->getHandler()->log("Failed to load scheduled transactions: " . $e->getMessage(), Customweb_Payment_Update_IHandler::LOG_TYPE_ERROR);
		}
		
		$this->unlockUpdate();
	}
	
	
	private function executeUpdate($paymentId, $parameters) {
		$this->getHandler()->beginTransaction();
		
		$transactionObject = null;
		try {
			$transactionId = $this->getHandler()->findTransactionIdByPaymentId($paymentId);
			$transactionObject = $this->getHandler()->loadTransactionObject($transactionId);
			if ($transactionObject === null) {
				throw new Exception(Customweb_Util_String::formatString("Unable to load transaction by payment id '!id'.", array('!id' => $paymentId)));
			}
			$this->getUpdateAdapter()->processTransactionUpdate($transactionObject, $parameters);
			$this->getHandler()->persistTransactionObject($transactionId, $transactionObject);
			$this->getHandler()->commitTransaction();
			$this->getUpdateAdapter()->confirmTransactionUpdate($transactionObject);
			
		} catch(Exception $e) {
			$this->getHandler()->commitTransaction();
			$this->getHandler()->log($e->getMessage(), Customweb_Payment_Update_IHandler::LOG_TYPE_ERROR);
			$this->getUpdateAdapter()->confirmFailedTransactionUpdate($transactionObject, $e->getMessage());
		}
	}
	
	protected function getBackendStorageSpace() {
		return self::STORAGE_BACKEND_SPACE;
	}
	
	private function getLastSuccessfulBatchUpdate() {
		$time = $this->getStorageBackend()->read(self::STORAGE_BACKEND_SPACE, self::STORAGE_BACKEND_LAST_UPDATE_KEY);
		if (!empty($time)) {
			$time = '@' . $time;
		}
		return new DateTime($time);
	}
	
	private function setLastSuccessfulBatchUpdate(DateTime $updated) {
		$time = $updated->getTimestamp();
		$this->getStorageBackend()->write(self::STORAGE_BACKEND_SPACE, self::STORAGE_BACKEND_LAST_UPDATE_KEY, $time);
	}
	
}