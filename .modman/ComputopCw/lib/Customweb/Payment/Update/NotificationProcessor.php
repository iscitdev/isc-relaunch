<?php 
/**
  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2013 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.customweb.ch/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.customweb.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */

//require_once 'Customweb/Payment/Update/AbstractProcessor.php';
//require_once 'Customweb/Util/String.php';
//require_once 'Customweb/Payment/Update/IHandler.php';


/**
 * This processor updates transactions based on a given HTTP request. 
 * 
 * @author Thomas Hunziker / Simon Schurter
 *
 */
class Customweb_Payment_Update_NotificationProcessor extends Customweb_Payment_Update_AbstractProcessor {
	
	private $transactionObject = null;
	private $transactionId = null;
	
	
	public function process() {
		if ($this->getUpdateAdapter() === null) {
			return;
		}
				
		$this->getHandler()->beginTransaction();
		try {
			$this->loadTransactionId();
			$this->loadTransaction();
			$this->executeUpdate();
		}
		catch(Exception $e) {
			$this->getHandler()->commitTransaction();
			$this->failUpdate($e->getMessage());
		}
		
	}
	
	private function loadTransactionId() {
		$map = $this->getUpdateAdapter()->preprocessTransactionUpdate($this->getHandler()->getRequestParameters());
		
		if (isset($map['transactionId'])) {
			$this->transactionId = $map['transactionId'];
		}
		elseif (isset($map['paymentId'])) {
			$this->transactionId = $this->getHandler()->findTransactionIdByPaymentId($map['paymentId']);
		}
		
		if ($this->transactionId == null) {
			throw new Exception("No transaction found for the given update request.");
		}
	}
	
	private function loadTransaction() {
		$this->transactionObject = $this->getHandler()->loadTransactionObject($this->transactionId);
		if ($this->transactionObject === null) {
			throw new Exception(Customweb_Util_String::formatString("No transaction found with given id '!id'.", array(
				'!id' => $this->transactionId,
			)));
		}
	}
	
	private function executeUpdate() {
		$this->getUpdateAdapter()->processTransactionUpdate($this->transactionObject, $this->getHandler()->getRequestParameters());
		$this->getHandler()->persistTransactionObject($this->transactionId, $this->transactionObject);
		$this->getHandler()->commitTransaction();
		$this->getUpdateAdapter()->confirmTransactionUpdate($this->transactionObject);
		$this->getHandler()->log(
				Customweb_Util_String::formatString(
						"Transaction with id '!id' successful updated.",
						array('!id' => $this->transactionId)
				),
				Customweb_Payment_Update_IHandler::LOG_TYPE_INFO
		);
	}
	
	private function failUpdate($message) {
		$this->getHandler()->log($message, Customweb_Payment_Update_IHandler::LOG_TYPE_ERROR);
		$this->getUpdateAdapter()->confirmFailedTransactionUpdate($this->transactionObject, $message);
	}

	
	
}