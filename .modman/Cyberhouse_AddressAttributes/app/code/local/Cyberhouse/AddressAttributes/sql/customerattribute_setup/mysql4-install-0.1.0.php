<?php
$installer = $this;
$installer->startSetup();


$installer->addAttribute("customer_address", "housenumber",  array(
    "type"     => "varchar",
    "backend"  => "",
    "label"    => "Haus Nummer",
    "input"    => "text",
    "source"   => "",
    "visible"  => true,
    "required" => true,
    "default" => "",
    "frontend" => "",
    "unique"     => false,
    "note"       => ""
));

$attribute   = Mage::getSingleton("eav/config")->getAttribute("customer_address", "housenumber");
        
$used_in_forms=array();
$used_in_forms[]="adminhtml_customer_address";
$used_in_forms[]="customer_register_address";
$used_in_forms[]="customer_address_edit";
        $attribute->setData("used_in_forms", $used_in_forms)
		->setData("is_used_for_customer_segment", true)
		->setData("is_system", 0)
		->setData("is_user_defined", 1)
		->setData("is_visible", 1)
		->setData("sort_order", 75)
		;
        $attribute->save();

$installer->endSetup();
	 