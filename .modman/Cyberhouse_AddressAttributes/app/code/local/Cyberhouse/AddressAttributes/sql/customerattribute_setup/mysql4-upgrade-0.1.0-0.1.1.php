<?php

$installer = $this;
$installer->startSetup();

$configModel = Mage::getConfig();

// text
$configModel->saveConfig(
	"customer/address_templates/text",
	"{{depend prefix}}{{var prefix}} {{/depend}}{{var firstname}} {{depend middlename}}{{var middlename}} {{/depend}}{{var lastname}}{{depend suffix}} {{var suffix}}{{/depend}}
	{{depend company}}{{var company}}{{/depend}}
	{{if street1}}{{var street1}} {{var housenumber}}{{/if}}
	{{depend street2}}{{var street2}}{{/depend}}
	{{depend street3}}{{var street3}}{{/depend}}
	{{depend street4}}{{var street4}}{{/depend}}
	{{if postcode}}{{var postcode}}{{/if}} {{if city}}{{var city}}{{/if}}
	{{var country}}
	T: {{var telephone}}
	{{depend fax}}F: {{var fax}}{{/depend}}
	{{depend vat_id}}VAT: {{var vat_id}}{{/depend}}");

// text one line
$configModel->saveConfig(
	"customer/address_templates/oneline",
	"{{depend prefix}}{{var prefix}} {{/depend}}{{var firstname}} {{depend middlename}}{{var middlename}} {{/depend}}{{var lastname}}{{depend suffix}} {{var suffix}}{{/depend}}, {{var street}} {{var housenumber}}, {{var postcode}} {{var city}}, {{var country}}");

// html
$configModel->saveConfig(
	"customer/address_templates/html",
	"{{depend prefix}}{{var prefix}} {{/depend}}{{var firstname}} {{depend middlename}}{{var middlename}} {{/depend}}{{var lastname}}{{depend suffix}} {{var suffix}}{{/depend}}<br/>
	{{depend company}}{{var company}}<br />{{/depend}}
	{{if street1}}{{var street1}} {{var housenumber}}<br />{{/if}}
	{{depend street2}}{{var street2}}<br />{{/depend}}
	{{depend street3}}{{var street3}}<br />{{/depend}}
	{{depend street4}}{{var street4}}<br />{{/depend}}
	{{if postcode}}{{var postcode}}{{/if}} {{if city}}{{var city}}{{/if}}<br/>
	{{var country}}<br/>
	{{depend telephone}}T: {{var telephone}}{{/depend}}
	{{depend fax}}<br/>F: {{var fax}}{{/depend}}
	{{depend vat_id}}<br/>VAT: {{var vat_id}}{{/depend}}");

// pdf
$configModel->saveConfig(
	"customer/address_templates/pdf",
	"{{depend prefix}}{{var prefix}} {{/depend}}{{var firstname}} {{depend middlename}}{{var middlename}} {{/depend}}{{var lastname}}{{depend suffix}} {{var suffix}}{{/depend}}|
	{{depend company}}{{var company}}|{{/depend}}
	{{if street1}}{{var street1}} {{var housenumber}}{{/if}}
	{{depend street2}}{{var street2}}|{{/depend}}
	{{depend street3}}{{var street3}}|{{/depend}}
	{{depend street4}}{{var street4}}|{{/depend}}
	{{if postcode}}{{var postcode}}{{/if}}|
	{{if city}}{{var city}},|{{/if}}
	{{var country}}|
	{{depend telephone}}T: {{var telephone}}{{/depend}}|
	{{depend fax}}<br/>F: {{var fax}}{{/depend}}|
	{{depend vat_id}}<br/>VAT: {{var vat_id}}{{/depend}}|");

// javascript
$configModel->saveConfig(
	"customer/address_templates/js_template",
	'#{prefix} #{firstname} #{middlename} #{lastname} #{suffix}<br/>#{company}<br/>#{street0} #{var housenumber}<br/>#{street1}<br/>#{street2}<br/>#{street3}<br/>#{postcode} #{city}<br/>#{country_id}<br/>T: #{telephone}<br/>F: #{fax}<br/>VAT: #{vat_id}');

$configModel->cleanCache();
$installer->endSetup();