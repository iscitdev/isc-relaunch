<?php

class Cyberhouse_ApiImport_Helper_Test extends Danslo_ApiImport_Helper_Test
{

    protected $_linkedProducts = null;

    protected $_defaultProductAttributes = array(
        'description'       => 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores',
        '_attribute_set'    => 'Default',
        'short_description' => 'Some short description',
        '_product_websites' => 'website_1',
        'status'            => Mage_Catalog_Model_Product_Status::STATUS_ENABLED,
        'visibility'        => Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH,
        'tax_class_id'      => 0,
        'is_in_stock'       => 1,
		'_store'			=> 'default_4',
    );

    protected $_defaultCustomerAttributes = array(
        '_website'          => 'base',
        '_store'            => 'default',
        'group_id'          => 1
    );

    public function removeAllProducts()
    {
        Mage::getSingleton('core/resource')->getConnection('core_write')->query('TRUNCATE TABLE catalog_product_entity');
    }

    protected function _getLinkedProducts()
    {
        /*
         * We create 3 simple products so we can test configurable/bundle links.
         */
        if ($this->_linkedProducts === null) {
            $this->_linkedProducts = $this->generateRandomSimpleProducts(3);

            /*
             * Use the color option for configurables. Note that this attribute
             * must be added to the specified attribute set!
             */
            foreach (array('red', 'yellow', 'green') as $key => $color) {
                $this->_linkedProducts[$key + 1]['color'] = $color;
            }
            Mage::getModel('api_import/import_api')->importEntities($this->_linkedProducts);
        }
        return $this->_linkedProducts;
    }

    public function generateRandomSimpleProducts($numProducts, $iterator = 0)
    {
        $products = array();

        for ($i = 1; $i <= $numProducts; $i++) {
		
			$product_id = ($i + ($iterator*$numProducts) );
            $products[$i] = array_merge(
                $this->_defaultProductAttributes,
                array(
                    'sku'    => 'some_sku_' . $product_id,
                    '_type'  => Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
                    'name'   => 'Some product ( ' . ($product_id) . ' ) imported',
                    'price'  => rand(1, 1000),
                    'weight' => rand(1, 1000),
                    'qty'    => rand(1, 30),
					'_category' => rand(3,402),
					'_store' => 'default_'.rand(1,5),
                )
            );
			
			/**
			*	Custom Attributes
			*/
			for($k = 0; $k < rand(1,10);$k++){
				$products[$i]["test_attribute_".rand(1,400)] = rand(0,1000);
			}
        }

        return $products;
    }

}
