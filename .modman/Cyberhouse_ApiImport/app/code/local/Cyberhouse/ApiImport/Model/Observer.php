<?php

class Cyberhouse_ApiImport_Model_Observer
{

    protected function _indexFlatProducts(&$event)
    {
        $data =  $event->getNewData();
        foreach (Mage::app()->getStores() as $storeId => $store) {
            Mage::getResourceSingleton('catalog/product_flat_indexer')->saveProduct($data['product_ids'],$storeId);
        }
        return true;
    }

    public function indexFlatProducts($observer)
    { 
        /*
         * Obtain all imported entity IDs.
         */
        $entityIds = array();
        foreach ($observer->getEntities() as $entity) {
            $entityIds[] = $entity['entity_id'];
        }

        /*
         * Generate a fake mass update event that we pass to our indexers.
         */
        $event = Mage::getModel('index/event');
        $event->setNewData(array(
            'reindex_price_product_ids' => &$entityIds, // for product_indexer_price
            'reindex_stock_product_ids' => &$entityIds, // for indexer_stock
            'product_ids'               => &$entityIds, // for category_indexer_product
            'reindex_eav_product_ids'   => &$entityIds,  // for product_indexer_eav
        ));

        /*
         * Index our product entities.
         */
        try {
            
            if (Mage::getStoreConfig('api_import/import_settings/enable_flat_product_index')) {
                $this->_indexFlatProducts($event);
            }
        } catch (Exception $e) {
            return false;
        }

        return true;
    }

}
