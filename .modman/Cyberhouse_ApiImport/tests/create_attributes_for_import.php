<?php

require_once 'app/Mage.php';

Mage::init();

define('NUM_ENTITIES', 400);
define('API_USER', 'api');
define('API_KEY', '123456');
$url = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB) . 'index.php/api/soap/?wsdl';

$client = new SoapClient($url);
try{
	$session = $client->login(API_USER, API_KEY);
}
catch (Exception $e){
	print_r($e);
}	

echo "Starting Import".PHP_EOL;
$totalTime = microtime(true);
for ($i = 201; $i <= NUM_ENTITIES; $i++){
	unset($data);
	$data = array(
	   "attribute_code" => "test_attribute_".$i,
	   "frontend_input" => "text",
	   "scope" => "store",
	   // "default_value" => "1",
	   "is_unique" => 0,
	   "is_required" => 0,
	   "apply_to" => array(),
	   "is_configurable" => 0,
	   "is_searchable" => 0,
	   "is_visible_in_advanced_search" => 0,
	   "is_comparable" => 0,
	   "is_used_for_promo_rules" => 0,
	   "is_visible_on_front" => 0,
	   "used_in_product_listing" => 0,
	   "additional_fields" => array(),
	   "frontend_label" => array(array("store_id" => "0", "label" => "Test Attribute ".$i))
	);
	try {
		$result = $client->call($session, 'product_attribute.create', array($data));
		if($result && $result > 0){
			$result2 = $client->call($session, 'product_attribute_set.attributeAdd', 
			array(
					$result,
					4
				)
			);
		}
	}
	catch(Exception $e) {
		printf('Import failed: %s' . PHP_EOL, $e->getMessage());
		printf('Server returned: %s' . PHP_EOL, $result);
		exit;
	}

}
$totalTime = microtime(true) - $totalTime;
printf('========== Import statistics ==========' . PHP_EOL);
printf("Total duration:\t\t%fs"    . PHP_EOL, $totalTime);

$client->endSession($session);