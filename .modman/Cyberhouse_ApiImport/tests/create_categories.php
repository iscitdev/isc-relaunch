<?php
   
require_once 'app/Mage.php';

Mage::init();
   
define('API_USER', 'api');
define('API_KEY', '123456');
$url = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB) . 'index.php/api/soap/?wsdl';

$client = new SoapClient($url);
try{
	$session = $client->login(API_USER, API_KEY);
}
catch (Exception $e){
	print_r($e);
}	

echo "Starting Import".PHP_EOL;
$totalTime = microtime(true);

for( $i = 2; $i<=402; $i++ ){
$category_data = array(
	rand(2,$i),
	 array(
			'name'=>('Category_'.($i-1)),
			'is_active'=>1,
			'include_in_menu'=>2,
			'available_sort_by'=>'position',
			'default_sort_by'=>'position'
		   )
   );
   


	try{
		$result = $client->call(
		$session,
		'category.create',$category_data);
	}
	catch (Exception $e){
		print_r($e);
		exit;
	}	

}

$totalTime = microtime(true) - $totalTime;
printf('========== Import statistics ==========' . PHP_EOL);
printf("Total duration:\t\t%fs"    . PHP_EOL, $totalTime);

$client->endSession($session);


