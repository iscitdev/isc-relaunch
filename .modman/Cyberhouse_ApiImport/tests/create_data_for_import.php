<?php
require_once 'app/Mage.php';

Mage::init();

define('TOTAL_ENTITIES', 1000000);

$helper = Mage::helper('api_import/test');


$entityTypes = array(
    'product' => array(
        'entity' => Mage_ImportExport_Model_Export_Entity_Product::getEntityTypeCode(),
        'model'  => 'catalog/product',
        'types'  => array(
            'simple',
        )
    ),
);

$iterator = 0;
$chunk_size = 5000;

foreach ($entityTypes as $typeName => $entityType) {
    foreach ($entityType['types'] as $subType) {
        /*
        * Generation method depends on product type.
        */
        printf('Generating %d %s %ss...' . PHP_EOL, TOTAL_ENTITIES, $subType, $typeName);
		while($iterator * $chunk_size < TOTAL_ENTITIES){
			$entities = $helper->{sprintf('generateRandom%s%ss', ucfirst($subType), ucfirst($typeName))}($chunk_size,$iterator++);
			file_put_contents("var/import/chunk_".$iterator.".json",json_encode($entities));
		}
		
		/*$entities = Array ( 
			Array (
				"description" =>"Some description 1", 
				"_attribute_set" =>"Default", 
				"short_description" =>"Some short description 1",
				"_product_websites" => "base",			
				"status" =>1, 
				"visibility" =>4,
				"tax_class_id" =>0,
				"is_in_stock" =>1,
				"_store" =>"default_4", 
				"sku" =>"some_sku_1", 
				"_type" =>"simple",
				"name" =>"Some product ( 1 )", 
				"price" =>57, 
				"weight" =>755, 
				"qty" =>13, 
				"_category" =>28, 
				"test_attribute_156" =>"116", 
				"test_attribute_105" =>"827", 
				"test_attribute_73" =>"560",
				"test_attribute_143" =>"440",
				),
			array(
				//"_product_websites" => "base_2",
				"description" => "Beschreibung",
				"short_description" => "kurze Beschreibung",
				"_store" => "default_2",
				"test_attribute_156" =>100, 
				"test_attribute_105" =>100, 
				"test_attribute_73" =>100,
				"test_attribute_143" =>100,
			)
		); */
		
	}
}