<?php
/**
 * Copyright 2011 Daniel Sloof
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
require_once 'app/Mage.php';

Mage::init();

define('API_USER', 'api');
define('API_KEY', '123456');
define('USE_API', false);

$argv = $_SERVER['argv'];
if(count($argv)>1)
    $chunk = $argv[1];
else
    $chunk = $_GET['chunk'];
$filename = __DIR__."/var/import/chunk_".$chunk.".json";

if(!$chunk || !file_exists($filename))
	die("File not found");


if (USE_API) {
    /*
     * Create an API connection.
     * Standard timeout for Zend_Http_Client is 10 seconds, so we must lengthen it.
     */
    $client = new SoapClient(Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB) . 'index.php/api/soap/?wsdl');
    $session = $client->login(API_USER, API_KEY);
}

/*
 * Attempt to import generated products.
*/
printf('Starting import...' . PHP_EOL);
$totalTime = microtime(true);

$entities = file_get_contents($filename);
if( $entities != false){

	$entities = json_decode($entities,true);

	$typeName = Mage_ImportExport_Model_Export_Entity_Product::getEntityTypeCode();
	
	if (USE_API) {
		try {
			$result = $client->call($session, 'import.importEntities', array($entities, $typeName));
			printf("Result: %s". PHP_EOL,print_r($result,true));
		}
		catch(Exception $e) {
			printf('Import failed: %s' . PHP_EOL, print_r($e,true));
			printf('Server returned: %s' . PHP_EOL, print_r($result,true));
			exit;
		}
	} else {
		/*
		* For debugging purposes only.
		*/
		$result = Mage::getModel('api_import/import_api')->importEntities($entities, $typeName );
		printf("Result: %s". PHP_EOL,print_r($result,true));
	}
	$totalTime = microtime(true) - $totalTime;

	/*
	* Generate some rough statistics.
	*/
	
	$imported_number = count($entities);
	
	$typeName = "product";
	
	printf('========== Import statistics ==========' . PHP_EOL);
	printf("Total duration:\t\t%fs"    . PHP_EOL, $totalTime);
	printf("Average per %s:\t%fs" . PHP_EOL, $typeName, $totalTime / $imported_number);
	printf("%ss per second:\t%fs" . PHP_EOL, ucfirst($typeName), 1 / ($totalTime / $imported_number));
	printf("%ss per hour:\t%fs"   . PHP_EOL, ucfirst($typeName), (60 * 60) / ($totalTime / $imported_number));
	printf('=======================================' . PHP_EOL . PHP_EOL);
	//printf('Done! Magento reports %d %ss.' . PHP_EOL, Mage::getModel("catalog/product")->getCollection()->count(), $typeName);

}
/*
 * Cleanup.
 */
if (USE_API) {
    $client->endSession($session);
}
