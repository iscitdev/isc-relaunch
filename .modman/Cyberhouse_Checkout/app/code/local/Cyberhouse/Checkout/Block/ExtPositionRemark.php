<?php

class Cyberhouse_Checkout_Block_ExtPositionRemark extends Mage_Core_Block_Template {

    protected $inputType = "cart";

    public function getItem(){
        return $this->getParentBlock()->getItem();
    }

    public function getExtPositionRemark(){
        return $this->getItem()->getExtPositionRemark();
    }


    public function getInputType ()
    {
        if ($this->getParentBlock()->getInputType()) {
            return $this->getParentBlock()->getInputType();
        }
        return $this->inputType;
    }

}