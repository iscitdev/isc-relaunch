<?php

class Cyberhouse_Checkout_Helper_Data extends Mage_Core_Helper_Abstract
{

    private $maxStreetLength = 30;

    public function generateOrderXml(Mage_Sales_Model_Order $order)
    {

        if(!$order->getId()) {
            return;
        }

        $doc = new DOMDocument('1.0', 'utf-8');

        $root = $doc->createElement("place_order_request");
        $root->setAttribute('xmlns', 'http://isc-gmbh.info/schemata/erp');

        // collect simple, first-level attributes + values
        $orderData = array(
            'target_domain' => $order->getStore()->getWebsite()->getCode(),
            'magento_order_id' => $order->getIncrementId(),
            'customer_email' => $order->getCustomerEmail(),
            'order_date' => date("c", strtotime($order->getCreatedAt())),
            'ext_reference_no' => $order->getExtReferenceNo(),
            'currency' => $order->getOrderCurrencyCode(),

            'payment_type' => $this->mapPaymentType($order->getPayment()->getMethod()),
            //			'payment_ref_id'		=> (int)$order->getPayment()->getPaymentoperatorTransactionId(),
            'payment_id' => '',
            'cash_on_delivery_costs' => $order->getCodFee(),

            'total_net' => sprintf("%01.2f", ($order->getSubtotal() + $order->getCodFee() + $order->getShippingAmount())),
            'total_tax' => sprintf("%01.2f", $order->getTaxAmount()),
            'total_gross' => sprintf("%01.2f", $order->getGrandTotal()),
            'transport_costs' => sprintf("%01.2f", $order->getShippingAmount())
        );

        if(strpos($order->getPayment()->getMethod(), "computopcw") === 0) {
            $transaction = Mage::helper('ComputopCw')->loadTransactionByOrder($order->getId());
            if($transaction && $transaction->getPaymentId() && $transaction->getPaymentId() != $order->getIncrementId()) {
                $orderData['payment_id'] = $transaction->getPaymentId();
            }
            else {
                if ( ( (Mage::app()->getLocale()->date()->toValue() - $order->getCreatedAtDate()->toValue()) / 3600) < 24 ) {
                    throw new Exception( "Order with ID: " . $order->getId() . " not processed by Computop now!" );
                }
            }
        }


        // add shipping address
        $doc = $this->generateAddressXml($doc, $root, $order->getShippingAddress(), $order);

        // add billing address
        $doc = $this->generateAddressXml($doc, $root, $order->getBillingAddress(), $order);

        // add fields to doc
        foreach($orderData as $attribute => $value) {
            $root->appendChild($doc->createElement($attribute, $value));
        }

        // add ordered items
        $orderItems = $doc->createElement('order_items');
        foreach($order->getAllItems() as $item) {

            $orderItem = $doc->createElement('item');
            $product = Mage::getModel('catalog/product')->load($item->getProductId());

            // prepare item attributes
            $itemAttributes = array(
                'unit_sales_price' => round($item->getPrice(), 2),
                'quantity' => round($item->getQtyOrdered(), 0),
                'product_id' => $item->getSku(),
                'item_no' => $product->getProductNo(),
                'item_version' => $product->getProductVersion(),
                'ext_position_remark' => $item->getExtPositionRemark(),
                'parent_product_id' => $item->getParentProductSku(),
            );

            // set attributes
            foreach($itemAttributes as $attribute => $value) {
                $orderItem->setAttribute($attribute, $value);
            }

            // append to orderitems-wrap
            $orderItems->appendChild($orderItem);
        }
        $root->appendChild($orderItems);

        // append root to document
        $doc->appendChild($root);

        // format output xml for better reading
        $doc->formatOutput = true;

        $output = $doc->saveXML();

        Mage::log($output, null, 'order_messages.xml');

        return $output;
    }

    /**
     * add address xml structure to document
     * @param $doc
     * @param $root
     * @param Mage_Sales_Model_Order_Address $address
     * @param Mage_Sales_Model_Order $order
     * @return mixed
     */
    public function generateAddressXml($doc, $root, Mage_Sales_Model_Order_Address $address, $order = null)
    {

        $customer = Mage::getModel('customer/customer')->load($order->getCustomerId());

        $addressNode = $doc->createElement($address->getAddressType() . '_address');

        $addressStreet = $address->getStreet();

        $addressData = array(
            'street1' => mb_substr($addressStreet[0], 0, $this->maxStreetLength, 'UTF-8'),
            'street2' => $address->getHousenumber(),
            'postal_code' => $address->getPostcode(),
            'city' => mb_substr($address->getCity(), 0, 30, 'UTF-8'),
            'country' => $address->getCountryId(),
            'firstname' => $address->getFirstname(),
            'lastname' => $address->getLastname(),
            'telephone' => mb_substr($address->getTelephone(), 0, 24, 'UTF-8'),
            'company_name' => mb_substr($address->getCompany(), 0, 24, 'UTF-8'),
            'debitor_no' =>  $customer ? $customer->getDebitorNo() : '',
            'email' => $address->getEmail(),
            'ust_id' => $address->getVatId() ? $address->getVatId() : $order->getCustomerTaxvat()
        );


        foreach($addressData as $attribute => $value) {
            $addressNode->appendChild($doc->createElement($attribute, htmlspecialchars($value)));
        }

        $root->appendChild($addressNode);

        return $doc;
    }

    public function mapPaymentType($paymentType)
    {
        switch($paymentType) {
            case 'phoenix_cashondelivery':
                return 'cashondelivery';
                break;
            case 'paymentoperator_purchase_on_account':
                return 'checkmo';
                break;
            case 'computopcw_directdebits':
                return 'paymentoperator_eft';
                break;
            case 'computopcw_paypal':
                return 'paymentoperator_paypal_standard';
                break;
            case 'computopcw_creditcard':
                return 'paymentoperator_cc';
                break;
            case 'cashondelivery':
                return 'barzahlung_wvk';
                break;
        }

        return $paymentType;
    }
}