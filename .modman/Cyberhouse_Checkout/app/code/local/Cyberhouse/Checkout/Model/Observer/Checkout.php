<?php

class Cyberhouse_Checkout_Model_Observer_Checkout extends Mage_Core_Model_Observer
{

    /**
     * Append custom quote item data to order item
     *
     * @event sales_convert_quote_item_to_order_item
     * @param Varien_Object $observer
     * @return Mage_Bundle_Model_Observer
     */
    public function setCustomDataToOrderItem ( $observer )
    {
        $orderItem = $observer->getEvent()->getOrderItem();
        $quoteItem = $observer->getEvent()->getItem();

        foreach (array( 'parent_product_sku', 'ext_position_remark' ) as $field_to_copy) {
            $orderItem->setData( $field_to_copy, $quoteItem->getData( $field_to_copy ) );
        }

        return $this;
    }

    /**
     * @event checkout_cart_save_before
     * @event r4q_checkout_cart_save_before
     * @param Varien_Event_Observer $observer
     * @return $this
     */
    public function saveCustomDataToQuoteItem ( Varien_Event_Observer $observer )
    {
        $request = Mage::app()->getRequest();
        $quote = $observer->getCart()->getQuote();
        $cartData = $request->getParam( 'cart' );
        /** Prevent dispatching of logic if it the event is caused by the wrong controller */
        if ($this->isNotRequest($quote, $request)) {
            return $this;
        }
        elseif ($cartData && array_key_exists( "item", $cartData )) {
                $cartData = $cartData['item'];
        }
        if (is_array( $cartData )) {
            foreach ($cartData as $itemId => $itemInfo) {
                $quoteItem = $quote->getItemById( $itemId );
                if (isset( $itemInfo['ext_position_remark'] )) {
                    $this->addDataToQuoteItem($quoteItem,'ext_position_remark', $itemInfo['ext_position_remark'] );
                }
                if (isset( $itemInfo['remark'] ) ) {
                    $this->addDataToQuoteItem($quoteItem,'r4q_note', $itemInfo['remark'],false );
                }
            }
        }
    }

    protected function isNotRequest($quote, $request){
        if ($quote instanceof ITwebexperts_Request4quote_Model_Quote && $request->getRouteName() != "requestquote") {
            return true;
        }
        return false;
    }

    protected function addDataToQuoteItem ( $quoteItem, $key, $value, $validate = true )
    {
        $value = trim( $value );
        if (!$validate ||  $this->validateCustomQuoteItemData( $value ) ) {
            $quoteItem->setData( $key, $value );
            Mage::getSingleton( 'checkout/session' )->setCartWasUpdated( true );
        } elseif($validate) {
            $message = Mage::helper( "cyberhouse_checkout" )->__( 'The reference number is limited to 35 characters.' );
            Mage::getSingleton( 'checkout/session' )->addError( $message );
            Mage::app()->getResponse()->setRedirect( Mage::getUrl('checkout/cart') )->sendResponse();
            exit;
        }
    }

    protected function validateCustomQuoteItemData ( $string )
    {
        if (strlen( $string ) == 0) {
            return true;
        }
        return ( strlen( $string ) <= 35 && preg_match( '/^[a-zA-Z 0-9öäüÜÄÖß]+$/', $string ) );
    }

    /**
     * @event checkout_type_onepage_save_order
     * @param $observer
     */
    public function saveCustomData ( $observer )
    {
        $event = $observer->getEvent();
        $order = $event->getOrder();
        $fieldVal = Mage::app()->getRequest()->getParams();
        if (isset( $fieldVal['ext_reference_no'] )) {
            $order->setExtReferenceNo( $fieldVal['ext_reference_no'] );
        }
    }

    /**
     * @event controller_action_predispatch_checkout_onepage_saveOrder
     * @param $observer
     */
    public function validateCustomData ( $observer )
    {
        //custom validation for ext_reference_no
        if ($customValue = $observer->getControllerAction()->getRequest()->getParam( 'ext_reference_no', false )) {
            if (!preg_match( '/^[a-zA-Z0-9öäüÜÄÖß]+$/', $customValue ) || strlen( $customValue ) > 20) {
                $result['success'] = false;
                $result['error'] = true;
                $result['error_messages'] = Mage::helper( 'core' )->__( 'Please input only letters numbers and "_".' );
                Mage::app()->getFrontController()->getResponse()->setBody( Mage::helper( 'core' )->jsonEncode( $result ) )->sendResponse();
                exit;
            }
        }
    }
}