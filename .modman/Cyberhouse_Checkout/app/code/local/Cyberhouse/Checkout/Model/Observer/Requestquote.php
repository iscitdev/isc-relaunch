<?php

class Cyberhouse_Checkout_Model_Observer_Requestquote
{

    public function __construct ()
    {
        $this->configuredMaxQty = Mage::getStoreConfig( "cataloginventory/item_options/max_sale_qty" );
    }

    protected $configuredMaxQty;
    protected $helper;
    protected $currentCustomerVisibility;
    protected $requestCart;


    /**
     * Handles a Add To Cart Action before it takes place
     *
     * @event controller_action_predispatch_checkout_cart_ajaxadd
     * @event controller_action_predispatch_checkout_cart_add
     * @param Varien_Event_Observer $observer
     * @return $this
     */
    public function addToCartAction ( Varien_Event_Observer $observer )
    {
        Varien_Profiler::start(__CLASS__.'::'.__FUNCTION__);
        if (!$this->isQuoteRequestPossible()) {
            return $this;
        }
        $this->setRequestQuoteConfig();
        /** @var Mage_Core_Controller_Front_Action $controller */
        $controller = $observer->getControllerAction();
        /** @var Mage_Core_Controller_Request_Http $request */
        $request = $controller->getRequest();
        $productId = (int)$request->getParam( 'product' );
        if ($productId) {
            $product = Mage::getModel( 'catalog/product' )
                ->setStoreId( Mage::app()->getStore()->getId() )
                ->load( $productId );
            if ($product->getId()) {
                $qtyData = $this->checkAddProductConditions( $product, $request->getParam( 'qty',1 ) );
                if (isset( $qtyData['requestquote_qty'] ) && $qtyData['requestquote_qty'] > 0) {
                    $parentProductSku = $request->getParam( 'parent_product_sku' );
                    $this->addToRequestQuote( $product, $qtyData['requestquote_qty'], array( "parent_product_sku" => $parentProductSku ) );
                    Varien_Profiler::start(__CLASS__.'::'.__FUNCTION__.'::save_request_cart');
                    $this->getRequestCart()->save();
                    Varien_Profiler::stop(__CLASS__.'::'.__FUNCTION__.'::save_request_cart');
                    $request->setParam( 'qty', $qtyData['qty'] );
                    if ($qtyData['qty'] <= 0) {
                        $this->setResponseJson( $product, $controller );
                        $request->setDispatched( true );
                        $controller->setFlag( '',
                            Mage_Core_Controller_Front_Action::FLAG_NO_DISPATCH,
                            true );
                    }
                }
            }
        }
        Varien_Profiler::stop(__CLASS__.'::'.__FUNCTION__);
        return $this;
    }


    /**
     * Split Quote to Quote and Quote Request
     * @event controller_action_predispatch_checkout_cart_updatePost
     * @param Varien_Event_Observer $observer
     * @return $this
     */
    public function splitQuoteOnUpdate ( Varien_Event_Observer $observer )
    {
        if (!$this->isQuoteRequestPossible()) {
            return $this;
        }

        $this->setRequestQuoteConfig();
        /** @var Mage_Core_Controller_Request_Http $request */
        $request = $observer->getControllerAction()->getRequest();
        $updateAction = (string)$this->getUpdateAction();
        if ($updateAction != "empty_cart") {
            $cartData = $this->getCartData( $request );
            if ($cartData) {
                $splittedCartData = $this->checkUpdateCartData( $cartData, $updateAction );
                $this->setUpdateRequest( $request, $splittedCartData );
            }
        }
    }

    protected function setResponseJson ( Mage_Catalog_Model_Product $product, $controller )
    {
        $helper = Mage::helper( "checkout" );
        $output = array(
            'success' => true,
            'header' => $helper->__( 'Successfully added!' ),
            'message' => $helper->__( '%s was added to your quote request.', Mage::helper( 'core' )->escapeHtml( $product->getName() ) ),
            'sku' => $product->getSku(),
            'cart_url' => Mage::getUrl( 'checkout/cart' ),
            'cart_label' => $helper->__( 'My Cart' ),
            'close_label' => $helper->__( 'Close' ),
        );
        $controller->getResponse()->setBody(Mage::helper("core")->jsonEncode($output));
    }


    /**
     * Disable sales qty check for quote requests
     */
    public function setRequestQuoteConfig ()
    {
        if ($this->isQuoteRequestPossible()) {
            $store = Mage::app()->getStore();
            $store->setConfig( "cataloginventory/item_options/backorders", 1 );
            $store->setConfig( "cataloginventory/item_options/max_sale_qty", 99999 );
        }
    }

    /**
     * Get Filtered cart update data
     * @param $request
     * @return mixed
     */
    protected function getCartData ( $request )
    {
        $cartData = $request->getParam( 'cart' );
        if (is_array( $cartData )) {
            $cartData = $this->getFilteredQtyData( $cartData );
        }
        return $cartData;
    }

    protected function getFilteredQtyData ( $cartData )
    {
        $filter = new Zend_Filter_LocalizedToNormalized(
            array( 'locale' => Mage::app()->getLocale()->getLocaleCode() )
        );
        foreach ($cartData as $index => $data) {
            if (isset( $data['qty'] )) {
                $cartData[$index]['qty'] = $filter->filter( trim( $data['qty'] ) );
            }
        }
        return $cartData;
    }

    /**
     * Splits the update data request into 2 parts
     * @param $data
     * @param $updateAction
     * @return mixed
     */
    protected function checkUpdateCartData ( $data, $updateAction )
    {
        $quote = Mage::getSingleton( "checkout/session" )->getQuote();
        foreach ($data as $itemId => $itemInfo) {
            $quoteItem = $quote->getItemById( $itemId );
            if($this->isAddAllToQuote()){
                $data[$itemId]["requestquote_qty"] = $itemInfo['qty'];
                $data[$itemId]['qty'] = 0;
            }
            else {
                $data[$itemId] = $this->checkProductConditions( $quoteItem->getProduct(), $itemInfo['qty'] ) + $data[$itemId];
            }
        }
        return $data;
    }

    protected function checkProductConditions ( Mage_Catalog_Model_Product $product, $qty )
    {
        $itemInfo = array( "qty" => $qty );
        $stockQty = $product->getStockItem()->getQty();
        // Check Stock Data
        if ($stockQty < $itemInfo['qty']) {
            $itemInfo['requestquote_qty'] = $itemInfo['qty'] - $stockQty;
            $itemInfo['qty'] = $stockQty;
        }
        //Checkt Max Qty
        if ($itemInfo['qty'] > $this->getMaxQuoteQty()) {
            $itemInfo['requestquote_qty'] = $itemInfo['qty'] - $this->getMaxQuoteQty();
            $itemInfo['qty'] = $this->getMaxQuoteQty();
        }
        return $itemInfo;
    }

    protected function checkAddProductConditions($product, $qty = 1){
        $cart = Mage::helper( 'checkout/cart' )->getCart();
        $quoteItem = $cart->getQuote()->getItemByProduct($product);
        if ($quoteItem ){
            $itemInfo = array( "qty" => $qty );
            $currentCartQty = $quoteItem->getQty();
            $stockQty = $product->getStockItem()->getQty();
            // Check Stock Data
            if ($stockQty < $currentCartQty + $itemInfo['qty']) {
                $itemInfo['requestquote_qty'] = $currentCartQty + $itemInfo['qty'] - $stockQty;
                $itemInfo['qty'] -= $itemInfo['requestquote_qty'];
            }
            //Checkt Max Qty
            if ($currentCartQty + $itemInfo['qty'] > $this->getMaxQuoteQty()) {
                $itemInfo['requestquote_qty'] = $currentCartQty + $itemInfo['qty'] - $this->getMaxQuoteQty();
                $itemInfo['qty'] = $this->getMaxQuoteQty() - $currentCartQty;
            }
            return $itemInfo;
        }
        else{
            return $this->checkProductConditions($product, $qty);
        }
    }

    protected function setUpdateRequest ( Mage_Core_Controller_Request_Http $request, $splittedCartData )
    {
        $request->setParam( "cart", $splittedCartData );
    }

    /**
     * Adds items to request object if necessary
     * @event checkout_cart_save_after
     * @param $observer Varien_Event_Observer
     */
    public function addItemsToRequestQuote ( Varien_Event_Observer $observer )
    {
        $splittedCartData = $this->getCartData( Mage::app()->getRequest() );
        if ($splittedCartData) {
            $quote = $observer->getCart()->getQuote();
            foreach ($splittedCartData as $itemId => $cartData) {
                if (isset( $cartData['requestquote_qty'] ) && $cartData['requestquote_qty'] > 0) {
                    $quoteItem = $quote->getItemById( $itemId );
                    $product = $quoteItem->getProduct();
                    $this->addToRequestQuote( $product, $cartData['requestquote_qty'], $quoteItem->getData() );
                    if ($cartData['qty'] == $this->getMaxQuoteQty()) {
                        Mage::getSingleton( 'core/session' )->addNotice( Mage::helper( 'cataloginventory' )->__( 'The maximum quantity allowed for purchase is %s.', $this->getMaxQuoteQty() * 1 ) . " (" . $product->getName() . ")" );
                    } elseif (isset( $cartData['requestquote_qty'] ) &&  !$this->isAddAllToQuote() ) {
                        Mage::getSingleton( 'core/session' )->addNotice( Mage::helper( 'cataloginventory' )->__( '%s is not available with the selected quantity. %s items added to quote request.', $product->getName(), $cartData['requestquote_qty'] ) );
                    }
                    Mage::getSingleton( 'core/session' )->addSuccess( $this->getHelper()->__( "%s was added to your quote request.", $product->getName() ) );
                }
            }
            $this->getRequestCart()->save();
        }
    }

    protected function addToRequestQuote ( Mage_Catalog_Model_Product $product, $qty, $data = null )
    {
        Varien_Profiler::start(__CLASS__.'::'.__FUNCTION__);
        /** @var Mage_Sales_Model_Quote $requestCart */
        $requestCart = $this->getRequestCart();
        $product->getStockItem()->setIsInStock(1); //Fake stock to put it on request
        $requestCart->addProduct( $product, array( 'qty' => $qty ) );
        if ($data) {
            $quoteItem = $this->getRequestQuote()->getItemByProduct( $product );
            foreach (array( 'parent_product_sku', 'ext_position_remark' ) as $key) {
                if (isset( $data[$key] ) && ( !$quoteItem->hasData( $key ) || !$quoteItem->getData( $key ) )) {
                    $quoteItem->setData( $key, $data[$key] );
                }
            }
        }
        Varien_Profiler::stop(__CLASS__.'::'.__FUNCTION__);
    }


    private function getMaxQuoteQty ()
    {
        return $this->configuredMaxQty;
    }

    protected function isQuoteRequestPossible ()
    {
        if (!$this->currentCustomerVisibility) {
            $this->currentCustomerVisibility = Mage::helper( 'cyberhouse_customergroups' )->getCurrentCustomerVisibilityLevel();
        }
        if (in_array( "30", $this->currentCustomerVisibility )) {
            return true;
        }
        return false;
    }

    /**
     * @return Cyberhouse_Checkout_Helper_Data
     */
    protected function getHelper ()
    {
        if (!$this->helper) {
            $this->helper = Mage::helper( "cyberhouse_checkout" );
        }
        return $this->helper;
    }

    /**
     * Retrieve r4q shopping cart model object
     *
     * @return ITwebexperts_Request4quote_Model_Cart
     */
    protected function getRequestCart ()
    {
        if (!$this->requestCart) {
            $this->requestCart = Mage::getSingleton( 'request4quote/cart' );
        }
        return $this->requestCart;
    }

    /**
     * Get current active quote instance
     *
     * @return ITwebexperts_Request4quote_Model_Quote
     */
    protected function getRequestQuote ()
    {
       return $this->getRequestCart()->getQuote();
    }

    protected function getUpdateAction(){
        return Mage::app()->getRequest()->getParam( 'update_cart_action' );
    }

    protected function isAddAllToQuote(){
        return $this->getUpdateAction() == "add_all_to_quote";
    }
}