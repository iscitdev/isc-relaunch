<?php

/**
 * Created by PhpStorm.
 * User: rhutterer
 * Date: 07.07.14
 * Time: 08:55
 */
class Cyberhouse_Checkout_Model_Sync
{

    const STATE_PENDING = 'pending';
    const QUEUE_SETTING_NAME = 'outgoing';
    const QUEUE_TYPE_SETTING = 'order_outgoing';

    /**
     * entry point for cronjob
     */
    public function run ()
    {
        $orders = $this->getOrders();
        foreach ($orders as $order) {
            //sync it
            $this->syncOrder( $order );
        }
    }

    /**
     * send the order to NAV-Connector
     * @param Mage_Sales_Model_Order $order
     * @return bool
     */
    protected function syncOrder ( Mage_Sales_Model_Order $order )
    {
        // queue helper
        try {
            $orderXml = Mage::helper( 'cyberhouse_checkout/data' )->generateOrderXml( $order );
            $additional_headers = array( 'correlation-id' => $order->getIncrementId() );
            $sent = Mage::helper( 'cyberhouse_queue/data' )->sendToQueue( $orderXml, self::QUEUE_SETTING_NAME, self::QUEUE_TYPE_SETTING, false, false, $additional_headers );
            if ($sent) {
                $order->setState( 'synced', 'synced', 'Synced successfully' )->save();
            }
        } catch (Exception $e) {
            Mage::log( $e->getMessage(), Zend_log::NOTICE, 'order_messages.log' );
        }
    }


    /**
     * collects the orders which need to be synced
     * states are pending_computop|complete
     */
    protected function getOrders ()
    {
        $orders = Mage::getModel( 'sales/order' )->getCollection()
            ->addAttributeToFilter( 'status', array( 'eq' => self::STATE_PENDING ) );
        foreach ($orders as $order) {
            $order->setState( 'pending_sync', 'pending_sync', 'Sync in progress' )->save();
        }
        return $orders;
    }

}