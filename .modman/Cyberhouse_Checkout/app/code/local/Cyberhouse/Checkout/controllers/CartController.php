<?php
/**
 * User: ssc
 * Date: 10/17/13
 * Time: 3:32 PM
 */
require_once 'Mage/Checkout/controllers/CartController.php';


class Cyberhouse_Checkout_CartController extends Mage_Checkout_CartController {

	/**
	 * Ajax add product to shopping cart action
	 */
	public function ajaxAddAction()
	{
		$cart   = $this->_getCart();
		$params = $this->getRequest()->getParams();
		try {
			if (isset($params['qty'])) {
				$filter = new Zend_Filter_LocalizedToNormalized(
					array('locale' => Mage::app()->getLocale()->getLocaleCode())
				);
				$params['qty'] = $filter->filter($params['qty']);
			}

			$product = $this->_initProduct();
			$related = $this->getRequest()->getParam('related_product');

			/**
			 * Check product availability
			 */
			if (!$product) {
                $output = array(
                    'success' 	=> false,
                    'header' 	=> $this->__('The following error(s) occured:'),
                    'message' 	=> $this->__('Cannot add the item to shopping cart.'),
                    'debug'		=> "Product not found",
                    'close_label' => $this->__('Close')
                );
                echo json_encode($output);
				return;
			}

			$cart->addProduct($product, $params);
			if (!empty($related)) {
				$cart->addProductsByIds(explode(',', $related));
			}

			$parentProductSku = $this->getRequest()->getParam('parent_product_sku');
			$quoteItem = $cart->getQuote()->getItemByProduct($product);
			$quoteItem->setData('parent_product_sku', $parentProductSku);

			/*
			$parentProduct = Mage::getModel('catalog/product')->loadByAttribute('sku', $parentProductSku);
			$quoteItemName = $quoteItem->getName();
			$quoteItem->setData('name', $quoteItemName.' ('.$this->__('suitable for').' '.$parentProduct->getName().' )');
			*/

			$cart->save();

			$this->_getSession()->setCartWasUpdated(true);

			/**
			 * @todo remove wishlist observer processAddToCart
			 */
			Mage::dispatchEvent('checkout_cart_add_product_complete',
				array('product' => $product, 'request' => $this->getRequest(), 'response' => $this->getResponse())
			);

			$itemCount = $cart->getItemsCount();
			$itemMessage = ($itemCount > 1) ? $this->__('There are %s items in your cart', $itemCount) : $this->__('There is 1 item in your cart');

			$output = array(
				'success' => true,
				'header' => $this->__('Successfully added!'),
				'message' => $this->__('%s was added to your shopping cart.', Mage::helper('core')->escapeHtml($product->getName())),
				'item_message' => $itemMessage,
				'sku' => $product->getSku()
			);
		} catch (Mage_Core_Exception $e) {

			if ($this->_getSession()->getUseNotice(true)) {
				$message = Mage::helper('core')->escapeHtml($e->getMessage());
			} else {
				$message = $e->getMessage();
			}

			$output = array(
				'success' 	=> false,
				'header' 	=> $this->__('The following error(s) occured:'),
				'message' 	=> $message,

			);
		} catch (Exception $e) {
			$output = array(
				'success' 	=> false,
				'header' 	=> $this->__('The following error(s) occured:'),
				'message' 	=> $this->__('Cannot add the item to shopping cart.'),
				'debug'		=> $e->getMessage()
			);
		}

		$output['cart_url'] = Mage::getUrl('checkout/cart');
		$output['cart_label'] = $this->__('My Cart');
		$output['close_label'] = $this->__('Close');
		$output['cart_count'] = Mage::helper('checkout/cart')->getItemsQty();

		echo json_encode($output);
	}


}