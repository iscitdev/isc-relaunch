<?php
$installer = $this;

// Required tables
$statusTable = $installer->getTable('sales/order_status');
$statusStateTable = $installer->getTable('sales/order_status_state');

// Insert statuses
$installer->getConnection()->insertArray(
    $statusTable,
    array(
        'status',
        'label'
    ),
    array(
        array('status' => 'pending_sync', 'label' => 'Sync in progress'),
        array('status' => 'synced', 'label' => 'Synced successfully'),
    )
);

$installer->endSetup();