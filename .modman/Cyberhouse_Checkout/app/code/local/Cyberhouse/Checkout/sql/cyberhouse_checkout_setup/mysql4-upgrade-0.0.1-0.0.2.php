<?php

$installer = new Mage_Sales_Model_Resource_Setup('core_setup');

$installer->startSetup();
$installer->addAttribute('order', 'ext_reference_no', array('type' => 'varchar'));
$installer->endSetup();