<?php

$installer = new Mage_Sales_Model_Resource_Setup('core_setup');

$installer->startSetup();
$installer->addAttribute('quote_item', 'ext_position_remark', array('type' => 'varchar'));
$installer->addAttribute('quote_item', 'parent_product_sku', array('type' => 'varchar'));

$installer->addAttribute('order_item', 'ext_position_remark', array('type' => 'varchar'));
$installer->addAttribute('order_item', 'parent_product_sku', array('type' => 'varchar'));
$installer->endSetup();