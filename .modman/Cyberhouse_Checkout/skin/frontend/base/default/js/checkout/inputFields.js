jQuery(document).ready(function () {
    bindChangeEventOnCart();
    jQuery(document).on('cartreload', function(){
        bindChangeEventOnCart();
    });
});

function bindChangeEventOnCart(){
    jQuery('#shopping-cart-table input').on('change', function () {
        var button = jQuery('#cart-form').find('button.btn-proceed-checkout').last()[0];
        var form = jQuery('#shopping-cart-table').parents('form')[0];
        saveBeforeNextStep(button,form);
    });
    jQuery('#shopping-quote-table input').on('change', function () {
        saveBeforeNextStep('#shopping-quote-table button.btn-checkout', '#requestQuoteForm');
    });
}

var sendRequest = [];

function saveBeforeNextStep(button, form){
    button = jQuery(button);
    if(button.attr('onclick') != undefined && button.attr('onclick') != ""){
        var href = button.attr('onclick').match(/^window.location(?:.href)?='(.*)'(?:.*)$/)[1];
        button.data('href',href);
        var requestQuoteForm = jQuery(form);
        sendRequest.push({
            "requestForm": requestQuoteForm
        });
        button.removeAttr('onclick').off().on('click', function (event) {
            event.stopPropagation();
            jQuery('#update_cart_action_container').attr('name','update_cart_action').attr('value','update_qty');
            var ajaxCalls = 0;
            var currentButton = jQuery(this);
            for(var i=0;i< sendRequest.length; i++){
                var request = sendRequest[i];
                currentButton.addClass("loading");
                jQuery.ajax({
                    type: 'POST',
                    url:  request.requestForm.attr('action'),
                    data: request.requestForm.serialize(),
                    complete: function(){
                       ajaxCalls++;
                        if(ajaxCalls >= sendRequest.length){
                            window.location.href =  currentButton.data('href');
                        }
                    }
                });
            }

        });
    }
}