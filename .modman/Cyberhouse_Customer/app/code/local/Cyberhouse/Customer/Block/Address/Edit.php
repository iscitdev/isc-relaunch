<?php

class Cyberhouse_Customer_Block_Address_Edit extends Mage_Customer_Block_Address_Edit
{

    public function getCountryHtmlSelect( $defValue = null, $name = 'country_id', $id = 'country', $title = 'Country' ) {
        if (is_null( $defValue )) {
            $defValue = $this->getCountryId();
        }
        $options = Mage::helper( "cyberhouse_customer" )->getCountryOptions();
        $html = $this->getLayout()->createBlock( 'core/html_select' )
            ->setName( $name )
            ->setId( $id )
            ->setTitle( Mage::helper( 'directory' )->__( $title ) )
            ->setClass( 'validate-select' )
            ->setValue( $defValue )
            ->setOptions( $options )
            ->getHtml();

        return $html;
    }

    public function getCountryCollection() {
        $collection = $this->getData( 'country_collection' );
        if (is_null( $collection )) {
            $collection = Mage::helper( "directory" )->getCountryCollection();
            $this->setData( 'country_collection', $collection );
        }

        return $collection;
    }
}