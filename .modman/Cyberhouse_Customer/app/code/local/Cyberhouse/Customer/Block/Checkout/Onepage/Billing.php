<?php


class Cyberhouse_Customer_Block_Checkout_Onepage_Billing extends Mage_Checkout_Block_Onepage_Billing {

    public function getCountryOptions()
    {
        return Mage::helper("cyberhouse_customer")->getCountryOptions();
    }
}