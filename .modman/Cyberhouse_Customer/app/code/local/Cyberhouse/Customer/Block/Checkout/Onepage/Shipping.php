<?php

class Cyberhouse_Customer_Block_Checkout_Onepage_Shipping extends Mage_Checkout_Block_Onepage_Shipping {

    public function getCountryOptions()
    {
        return Mage::helper("cyberhouse_customer")->getCountryOptions();
    }
}