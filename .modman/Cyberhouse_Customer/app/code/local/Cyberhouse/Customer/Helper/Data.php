<?php

class Cyberhouse_Customer_Helper_Data extends Mage_Core_Helper_Abstract
{

    protected $country_collection;

    public function getCountryOptions(){
        /** @var Mage_Customer_Model_Customer $customer */
        $customer = Mage::helper("customer")->getCustomer();
        $customer_key = '_DEBITOR_'.($customer && $customer->getDebitorNo())?"YES":"NO";
        $customerGroup = '_CUSTOMER_GROUP_'.($customer?$customer->getGroupId():0);
        $cacheKey = 'DIRECTORY_COUNTRY_SELECT_STORE_'.Mage::app()->getStore()->getCode().$customer_key.$customerGroup;
        if (Mage::app()->useCache('config') && $cache = Mage::app()->loadCache($cacheKey)) {
            $options = unserialize($cache);
        } else {
            $options = $this->getCountryCollection()->toOptionArray();
            if (Mage::app()->useCache('config')) {
                Mage::app()->saveCache(serialize($options), $cacheKey, array('config'));
            }
        }
        return $options;
    }

    public function getCountryCollection()
    {
        $collection = $this->country_collection;
        if (is_null($collection)) {
            $collection = Mage::helper("directory")->getCountryCollection();
            $this->country_collection = $collection;
        }

        return $collection;
    }

}
	 