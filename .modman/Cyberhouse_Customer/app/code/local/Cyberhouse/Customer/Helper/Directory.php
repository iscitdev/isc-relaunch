<?php

class Cyberhouse_Customer_Helper_Directory extends Mage_Directory_Helper_Data {

    public function getCountryCollection() {
        /** @var Mage_Customer_Model_Customer $customer */
        $customer = Mage::helper('customer')->getCustomer();
        if (!$this->_countryCollection) {
            if($customer) {
                $group = Mage::getModel('customer/group')->load($customer->getGroupId());
                if ($group && $group->getId() && $group->getAvailableCountries()) {
                    $this->_countryCollection = Mage::getModel('directory/country')->getResourceCollection()
                    ->addCountryIdFilter( explode(",", $group->getAvailableCountries()) );
                }
                elseif( $customer->getDebitorNo() ) {
                    $this->_countryCollection = Mage::getModel('directory/country')->getResourceCollection()
                        ->loadByStore("admin");
                }
            }
            if (!$this->_countryCollection) {
                $this->_countryCollection = Mage::getModel('directory/country')->getResourceCollection()
                    ->loadByStore();
            }
        }
        return $this->_countryCollection;
    }
}