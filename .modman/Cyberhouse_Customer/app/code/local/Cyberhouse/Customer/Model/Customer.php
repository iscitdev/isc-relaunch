<?php
/**
 * Created by PhpStorm.
 * User: apo
 * Date: 16.06.14
 * Time: 06:50
 */

class Cyberhouse_Customer_Model_Customer extends Mage_Customer_Model_Customer {

    public function getPrefix(){
        $prefix = $this->getData('prefix');
        if ($prefix) {
            return Mage::helper("customer")->__($prefix);
        }
        return "";
    }

    public function getSuffix(){
        $suffix = $this->getData('suffix');
        if ($suffix) {
            return Mage::helper("customer")->__($suffix);
        }
        return "";
    }
} 