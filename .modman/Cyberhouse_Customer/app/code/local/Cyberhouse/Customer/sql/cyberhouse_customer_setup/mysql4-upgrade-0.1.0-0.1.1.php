<?php
/**
 * Created by PhpStorm.
 * User: rhutterer
 * Date: 14.05.14
 * Time: 10:52
 */
/* @var $installer Mage_Customer_Model_Entity_Setup */
$installer = $this;
$installer->startSetup();


$config = new Mage_Core_Model_Config();
$config ->saveConfig('customer/address/prefix_show', "req", 'default', 0);
$config ->saveConfig('customer/address/prefix_options', "Herr;Frau", 'default', 0);


Mage::app()->cleanCache();

$installer->endSetup();