<?php

class Cyberhouse_CustomerGroupAgreements_Model_Observer{


    /**
     * Add additional Field to agreements
     * @param Varien_Event_Observer $observer
     * @event adminhtml_block_html_before
     * @return $this
     */
    public function addCustomerGroup( Varien_Event_Observer $observer ){
        $block = $observer->getBlock();
        if (!isset($block)) {
            return $this;
        }

        if ($block->getType() == 'adminhtml/checkout_agreement_edit_form') {
            /* @var $block Mage_Adminhtml_Block_Checkout_Agreement_Edit_Form */
            $form = $block->getForm();
            /** @var Varien_Data_Form $fieldset */
            $fieldset   = $form->getElement('base_fieldset');
            $model  = Mage::registry('checkout_agreement');
            $fieldset->addField('customer_group_ids', 'multiselect', array(
                'name'  => 'customer_group_ids[]',
                'label'    => Mage::helper("customer")->__('Customer Group'),
                'title'    => Mage::helper("customer")->__('Customer Group'),
                'disable'      => false,
                'values' => Mage::getModel('adminhtml/system_config_source_customer_group_multiselect')->toOptionArray(),
                'value' => explode(",",$model->getCustomerGroupIds()),
            ));
        }
    }

    /**
     * Convert multiselect to string
     * @event model_save_before
     * @param Varien_Event_Observer $observer
     */
    public function checkoutAggreementSaveBefore ( Varien_Event_Observer $observer ){
        $model = $observer->getObject();
        if ($model instanceof Mage_Checkout_Model_Agreement) {
            $model->setData('customer_group_ids',implode(",",$model->getData('customer_group_ids')) );
        }
    }

    public function filterAgreements( Varien_Event_Observer $observer ) {
        $collection = $observer->getCollection();
        if ($collection instanceof Mage_Checkout_Model_Resource_Agreement_Collection) {
            $groupId = false;
            //Get group from quote
            $quote = Mage::getSingleton("checkout/cart")->getQuote();
            if ($quote) {
                $configAddressType = Mage::helper('customer/address')->getTaxCalculationAddressType();
                if ($configAddressType == "shipping") {
                    $address = $quote->getShippingAddress();
                }
                else {
                    $address = $quote->getBillingAddress();
                }
                //Dispatch collect Totals Events to get the right groupId
                Mage::dispatchEvent('sales_quote_address_collect_totals_before', array("quote_address" => $address));
                $groupId = $quote->getCustomerGroupId();
                Mage::dispatchEvent('sales_quote_address_collect_totals_after', array("quote_address" => $address));
            }
            //Get group from customers
            if(!$groupId){
                $groupId = Mage::getSingleton('customer/session')->getCustomerGroupId();
            }
            //Get default customer group
            if (!$groupId) {
                $groupId = Mage::getStoreConfig( 'customer/create_account/default_group' );
            }
            //Filter collection
            if ($groupId) {
                $collection->addFieldToFilter('customer_group_ids',array("finset" => $groupId) );
            }
        }
    }
}