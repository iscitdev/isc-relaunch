<?php

class Cyberhouse_CustomerGroupAgreements_Model_Resource_Agreement_Collection extends Mage_Checkout_Model_Resource_Agreement_Collection {

    /**
     * Retrieve all ids for collection
     *
     * @return array
     */
    public function getAllIds()
    {
        Mage::dispatchEvent('core_collection_abstract_load_before', array('collection' => $this));
        return parent::getAllIds();
    }

}