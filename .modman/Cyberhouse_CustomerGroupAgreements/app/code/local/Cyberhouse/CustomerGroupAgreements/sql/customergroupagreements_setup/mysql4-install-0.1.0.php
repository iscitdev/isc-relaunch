<?php
$installer = $this;
$installer->startSetup();
$installer->getConnection()->addColumn($installer->getTable('checkout_agreement'), 'customer_group_ids',
    'VARCHAR(255) NOT NULL DEFAULT "" AFTER `is_html`'
);
$installer->endSetup();
	 