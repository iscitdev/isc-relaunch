<?php

/**
 * User: ssc
 * Date: 1/27/14
 * Time: 2:22 PM
 */
class Cyberhouse_CustomerGroups_Block_Adminhtml_Customer_Group_Edit_Form extends Mage_Adminhtml_Block_Customer_Group_Edit_Form
{

    /**
     * Prepare form for render
     */
    protected function _prepareLayout() {

        if (Mage::getSingleton( 'adminhtml/session' )->getCustomerGroupData()) {
            $values = Mage::getSingleton( 'adminhtml/session' )->getCustomerGroupData();
        } else {
            $values = Mage::registry( 'current_group' )->getData();
        }

        parent::_prepareLayout();

        $form = $this->getForm();
        $form->setMethod('post');
        $fieldset = $form->getElement( 'base_fieldset' );

        $fieldset->addField( 'visibility_level', 'text',
            array(
                'name' => 'visibility_level',
                'label' => Mage::helper( 'cyberhouse_customergroups' )->__( 'Visibility Level' ),
                'title' => Mage::helper( 'cyberhouse_customergroups' )->__( 'Visibility Level' ),
                'note' => Mage::helper( 'cyberhouse_customergroups' )->__( 'Mehrere Werten mit Komma trennen, Bsp: "10,20"' ),
                'class' => 'required-entry',
                'required' => true,
                'value' => isset( $values['visibility_level'] ) ? $values['visibility_level'] : "",
            )
        );

        $fieldset->addField( 'available_countries', 'multiselect',
            array(
                'name' => 'available_countries',
                'label' => Mage::helper( 'cyberhouse_customergroups' )->__( 'Available Countries' ),
                'title' => Mage::helper( 'cyberhouse_customergroups' )->__( 'Available Countries' ),
                'values' => Mage::helper( 'cyberhouse_customergroups' )->getAvailableCountries(),
                'value' => isset($values['available_countries'])?$values['available_countries'] : ""
            )
        );


        return $this;
    }

}