<?php
/**
 * User: ssc
 * Date: 1/27/14
 * Time: 3:05 PM
 */ 
class Cyberhouse_CustomerGroups_Block_Adminhtml_Customer_Group_Grid extends Mage_Adminhtml_Block_Customer_Group_Grid {

	/**
	 * Configuration of grid
	 */
	protected function _prepareColumns()
	{

		$parent = parent::_prepareColumns();

		$this->addColumn('visiblity_level', array(
			'header' => Mage::helper('cyberhouse_customergroups')->__('Visibility Level'),
			'index' => 'visibility_level',
			'width' => '200px'
		));

		return $parent;
	}

}