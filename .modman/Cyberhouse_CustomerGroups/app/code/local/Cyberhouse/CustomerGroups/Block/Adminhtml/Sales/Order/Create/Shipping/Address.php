<?php

class Cyberhouse_CustomerGroups_Block_Adminhtml_Sales_Order_Create_Shipping_Address extends Mage_Adminhtml_Block_Sales_Order_Create_Shipping_Address {

    protected function _addAttributesToForm($attributes, Varien_Data_Form_Abstract $form)
    {
        parent::_addAttributesToForm( $attributes, $form );
        Mage::helper( 'cyberhouse_customergroups/adminhtml' )->bindCountriesToForm( $this->_form, Mage::getSingleton( 'adminhtml/session_quote' )->getCustomer() );

        return $this;
    }
}