<?php

class Cyberhouse_CustomerGroups_Helper_Adminhtml extends Mage_Core_Helper_Abstract {

    public function bindCountriesToForm ( $form, $customer )
    {
        if($form->getElement('country_id')){
            $countries = Mage::helper( 'cyberhouse_customergroups' )->getAvailableCountries( $customer );
            $form->getElement( 'country_id' )->setValues( $countries );
        }
    }
}