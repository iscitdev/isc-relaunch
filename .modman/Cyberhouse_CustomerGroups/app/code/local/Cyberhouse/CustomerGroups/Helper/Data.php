<?php

class Cyberhouse_CustomerGroups_Helper_Data extends Mage_Core_Helper_Abstract {

    protected $customerVisibilityLevel = array();

    public function getAvailableCountries(){
        $countryList = Mage::getModel('directory/country')->getResourceCollection()->loadByStore()->toOptionArray();
        $countryList = $this->sortCountries($countryList);
        return $countryList;
    }

    public function sortCountries(array &$sort)
    {
        if (empty($sort)) {
            return false;
        }
        $oldLocale = setlocale(LC_COLLATE, "0");
        $localeCode = Mage::app()->getLocale()->getLocaleCode();
        // use fallback locale if $localeCode is not available
        setlocale(LC_COLLATE,  $localeCode . '.utf8', 'C.utf-8', 'en_US.utf8');
        uasort($sort, array($this, "valueCompare"));
        setlocale(LC_COLLATE, $oldLocale);
        return $sort;
    }

    protected function valueCompare( $val1, $val2 ) {
        return strcoll($val1['label'],$val2['label']);
    }

    /**
     * Returns the visibility levels of the given customer
     * @param Mage_Customer_Model_Customer $customer
     * @return array
     */
    public function getCustomerVisibilityLevel( Mage_Customer_Model_Customer $customer)
    {
        if(!$this->customerVisibilityLevel && ! isset($this->customerVisibilityLevel[$customer->getId()]) ){
            $visibilityLevel = array();
            if($customer){
                // filtering for customer group
                $customerGroup = Mage::getSingleton('customer/group')->load(
                    $customer->getGroupId()
                );
                $visibilityLevel = explode(',', $customerGroup->getVisibilityLevel());
            }
            $this->customerVisibilityLevel[$customer->getId()] = $visibilityLevel;
        }
       return $this->customerVisibilityLevel[$customer->getId()];
    }

    /**
     * Returns the visibility levels of the current customer
     * @return array
     */
    public function getCurrentCustomerVisibilityLevel()
    {
        return $this->getCustomerVisibilityLevel(Mage::helper('customer')->getCurrentCustomer());
    }

    /**
     * Method to check if the Customer has Visibility Level 30
     * @return bool
     */
    public function hasCurrentCustomerLevel30(){
        if (in_array("30", $this->getCurrentCustomerVisibilityLevel())){
            return true;
        }
        else{
            return false;
        }

    }
}