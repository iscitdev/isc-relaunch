<?php

class Cyberhouse_CustomerGroups_Model_Observer extends Mage_Core_Model_Abstract{

    /**
     * Save the visibility values and available countries set in the adminhtml interface.
     * Called in adminhtml
     *
     * @param Varien_Event_Observer $observer
     * @return null
     */
    public function customerGroupSaveBefore(Varien_Event_Observer $observer)
    {
        /*
         * Update values
         */
        if (Mage::app()->getStore()->isAdmin()) {
            $customerGroup = $observer->getEvent()->getObject();
            $visibilityLevel = Mage::app()->getRequest()->getParam('visibility_level', '');
            $customerGroup->setVisibilityLevel($visibilityLevel);
            $availableCountries = Mage::app()->getRequest()->getParam('available_countries', '');
            if ($availableCountries) {
                $availableCountries = implode(",",$availableCountries);
                $customerGroup->setAvailableCountries($availableCountries);
            }
        }
    }

    /**
     * Adds an extra layout handle for each visibility level of a customer
     * Called in frontend area
     *
     * @event controller_action_layout_load_before
     * @param Varien_Event_Observer $observer
     */
    public function addLayoutHandle ( Varien_Event_Observer $observer )
    {
        $customer = Mage::helper("customer")->getCustomer();
        if ($customer) {
            $visibilityLevels = array_filter(Mage::helper("cyberhouse_customergroups")->getCustomerVisibilityLevel($customer));
            if ($visibilityLevels) {
                sort($visibilityLevels);
                $observer->getEvent()->getLayout()->getUpdate()->addHandle("visibility_".implode("_",$visibilityLevels));
            }
        }
    }
}