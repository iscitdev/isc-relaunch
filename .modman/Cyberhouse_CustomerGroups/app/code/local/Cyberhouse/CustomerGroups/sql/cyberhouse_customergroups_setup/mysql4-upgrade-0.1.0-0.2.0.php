<?php
$installer = $this;
$installer->getConnection()->addColumn(
    $installer->getTable('customer/customer_group'),
    'available_countries',
    array (
        'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
        'length'    => 255,
        'comment'   => 'Available Countries'
    )
);
