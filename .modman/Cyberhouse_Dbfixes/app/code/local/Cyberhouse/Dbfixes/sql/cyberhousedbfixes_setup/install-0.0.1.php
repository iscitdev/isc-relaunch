<?php

/* @var $installer Mage_Catalog_Model_Resource_Eav_Mysql4_Setup */
$installer = $this;

$installer->startSetup();

$installer->getConnection()->insert( $installer->getTable( "directory/country" ),
    array(
        "country_id" => "XK",
        "iso2_code" => "XK",
        "iso3_code" => "RKS"
    )
);


$installer->endSetup();