<?php

/* @var $installer Mage_Catalog_Model_Resource_Eav_Mysql4_Setup */
$installer = $this;

$installer->startSetup();

$installer->getConnection()->modifyColumn("catalog_product_entity_text","value","MEDIUMTEXT");

$installer->endSetup();