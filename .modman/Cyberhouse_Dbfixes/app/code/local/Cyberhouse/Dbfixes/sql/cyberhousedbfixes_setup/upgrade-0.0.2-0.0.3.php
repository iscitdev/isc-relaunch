<?php

/* @var $installer Mage_Catalog_Model_Resource_Eav_Mysql4_Setup */
$installer = $this;

$installer->startSetup();
//Widget UTF-8 Serialization fix - may be caused by bad import
$widgets = Mage::getResourceModel("widget/widget_instance_collection" );
foreach ($widgets as $widget) {
    $params = $widget->getData( 'widget_parameters');
    $params = @unserialize( $params );
    if (!$params) {
        $params = utf8_decode( $widget->getData( 'widget_parameters') );
        $params = @unserialize( $params );
        if ($params) {
            $widget->setData( 'widget_parameters', $params );
            $widget->save();
        }
    }
}

$installer->endSetup();