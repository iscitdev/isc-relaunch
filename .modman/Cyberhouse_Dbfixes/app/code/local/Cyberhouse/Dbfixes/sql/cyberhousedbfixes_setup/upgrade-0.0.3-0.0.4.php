<?php

/* @var $installer Mage_Catalog_Model_Resource_Eav_Mysql4_Setup */
$installer = $this;
$installer->startSetup();
if ($installer->tableExists($installer->getTable('computopcw_transaction'))) {
    $installer->getConnection()
        ->addIndex(
            $installer->getTable('computopcw_transaction'),
            $installer->getIdxName('computopcw_transaction', array('order_id'), Varien_Db_Adapter_Interface::INDEX_TYPE_INDEX),
            array('order_id'),
            Varien_Db_Adapter_Interface::INDEX_TYPE_INDEX
        );
}

$installer->endSetup();