<?php
class Cyberhouse_Fastorder_Helper_Data extends Mage_Core_Helper_Abstract {


    /**
     * Method to get the VersionCollection for a product
     */
    public function getVersionCollectionByProduct($productSku){

        if($productSku){
            return $this->getVersionCollectionByProductNumber($productSku);
        }
        return false;

    }

    /**
     * Method to get the VersionCollection By productNo
     */
    protected function getVersionCollectionByProductNumber($number){

        if($number){

            $collection = Mage::getModel( 'catalog/product' )->getCollection()
                ->addAttributeToSelect( '*' )
                ->addFieldToFilter( 'sku', array( 'like' => $number . '\_0%' ) );

            if ($collection->count() > 0) {
                return $collection;
            }
        }
        return false;
    }


}