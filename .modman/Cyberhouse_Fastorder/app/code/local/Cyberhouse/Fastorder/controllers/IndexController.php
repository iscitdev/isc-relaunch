<?php

require_once 'Mage/Checkout/controllers/CartController.php';
class Cyberhouse_Fastorder_IndexController extends Mage_Checkout_CartController
{

    public function indexAction(){
        $this->getResponse()->setRedirect(Mage::getUrl("checkout/cart"))->sendResponse();
        exit;
    }

    public function updateAction()
    {
		if ($this->getRequest()->isXmlHttpRequest()) {
            $productSku = $this->getRequest()->getPost( 'productSku' );
			$cart = Mage::helper( 'checkout/cart' )->getCart();
			$product = Mage::getModel( 'catalog/product' )->loadByAttribute("sku",$productSku);
            if (!$product || !$product->getId()) {
                $collection = Mage::helper("fastorder/data")->getVersionCollectionByProduct($productSku);
                if ($collection) {
                    $product = $collection->getFirstItem();
                }
            }
            if ($product && $product->getId()) {
                try {
                    $this->getRequest()->setParam("product",$product->getId());
                    $this->getRequest()->setDispatched( false );
                    Mage::dispatchEvent("controller_action_predispatch_checkout_cart_add",array("controller_action" => $this) );
                    if(!$this->getRequest()->isDispatched()){
                        $cart->addProduct( $product->getId() ,array('qty' => $this->getRequest()->getParam('qty',1) ) );
                        $cart->save();
                    }
                    $this->getRequest()->setDispatched( true );
                    $this->_getSession()->setCartWasUpdated( true );
                    //$checkout = Mage::getSingleton( 'checkout/type_onepage' );
                    //$checkout->initCheckout();
                    $this->loadLayout();
                    $output = "<div>".$this->getLayout()->getBlock( 'header' )->toHtml() . $this->getLayout()->getBlock( 'content' )->toHtml()."</div>";
                } catch (Exception $e){
                    $output = Mage::helper("core")->jsonEncode(
                        array(
                            "message" => $e->getMessage(),
                        )
                    );
                }
            }
            else {
                $output = Mage::helper("core")->jsonEncode(
                    array(
                        "message" => Mage::helper('checkout')->__('The product does not exist.'),
                    )
                );
            }
            $this->getResponse()->setBody($output);
		}
		else {
			$this->indexAction();
		}
    }

    public function updateTopCartAction() {
        $this->loadLayout();
        $this->renderLayout();
    }

}