function insertProduct(){
    var inputField = jQuery('#fastorder-input');
    var qtyField = jQuery('#fastorder-qty');
    var inputValue = inputField.val();
    if(inputValue != "" && inputValue.length >= 6){
        var qty = qtyField.val();
        if(qty == ""){
            qty = 1;
        }
        insertProductBySku( inputValue ,qty);
        inputField.val("");
        qtyField.val("");
    }
}

function insertProductBySku( productSku, qty ) {
    jQuery.fancybox.showLoading();
    jQuery.post(
        fastorderUpdateUrl,
        {productSku: productSku, qty: qty},
        function(data) {
            try{
                var dataObject = jQuery.parseJSON(data);
                alert(dataObject.message);
            }
            catch (e){
                var response = jQuery(data);
                if( response.find("#cart-form").length > 0) {
                    jQuery('#cart-form').replaceWith(response.find('#cart-form'));
                    jQuery('#quote-form').replaceWith(response.find('#quote-form'));
                    jQuery('#cart-count').replaceWith(response.find('#cart-count'));
                    dataForm = new VarienForm('cart-form', false);
                    jQuery('button').addClass('btn');
                    jQuery(document).trigger('cartreload');
                }
            }
        }
    ).always(function() {
            jQuery.fancybox.hideLoading();
        });
}