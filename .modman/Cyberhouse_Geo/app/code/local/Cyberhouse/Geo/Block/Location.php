<?php

/**
 * Created by PhpStorm.
 * User: wfreund
 * Date: 12.02.14
 * Time: 15:37
 */
class Cyberhouse_Geo_Block_Location extends Mage_Core_Block_Template {

    public function __construct () {
        parent::__construct();
        $this->setTemplate('geo/location.phtml');
    }

    public function getUserCountry () {
        return $this->helper("geo")->getUserCountry();
    }

    public function getAvailableCountries () {
        return $this->helper("geo")->getAvailableCountries();
    }

    public function getStoreByCountry ($country = null) {
        return $this->helper("geo")->getStoreByCountryAndLanguage($country);
    }

    public function getUserStore () {
        return $this->getStoreByCountry($this->getUserCountry()->getCountryId());
    }

    public function getStoreCurrentUrl ($store) {
        return $this->helper("geo")->getStoreCurrentUrl($store);
    }

    protected function _toHtml () {
        if ( Mage::helper("geo")->isRedirect() ) {
            return parent::_toHtml();
        }
        return "";
    }

    public function getTax () {
        $taxClassId = 1;
        /** @var Mage_Customer_Model_Address $address */
        $address = Mage::getModel("customer/address");
        $address->setCountryId(Mage::getStoreConfig('general/country/default'));
        $request = Mage::getSingleton('tax/calculation')->getRateRequest($address, null, null, null);
        $taxPercent = Mage::getSingleton('tax/calculation')->getRate($request->setProductClassId($taxClassId));
        return floatval($taxPercent);
    }
}