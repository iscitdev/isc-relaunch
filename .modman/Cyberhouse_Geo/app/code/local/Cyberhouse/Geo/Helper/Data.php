<?php

class Cyberhouse_Geo_Helper_Data extends Mage_Core_Helper_Abstract
{

    protected $country_fallback;

    const COUNTRY_FALLBACK = 'general/country/default';
    const EXCLUDE_URLS = "cyberhouse_geo/general/exclude";

    protected $_countries = array();
    protected $user_country;
    protected $stores;
    protected $exclude_urls = array();
    protected $translation_list;

    const GEO_REDIRECT = "geo_redirect";

    public function __construct()
    {
        $this->setCountryFallback(Mage::getStoreConfig(self::COUNTRY_FALLBACK));
    }

    /**
     * Get the country of the current ip
     * @return Mage_Directory_Model_Country
     */
    public function getUserCountry()
    {
        if(!$this->user_country) {
            $geoIP = Mage::getSingleton('geoip/country');
            $geoIPCountry = $geoIP->getCountry();
            $countries = $this->getCountries($geoIPCountry ? $geoIPCountry : $this->country_fallback);
            $this->user_country = reset($countries);
        }
        return $this->user_country;
    }

    /**
     * Get allowed countries by store
     * @param string $store_id
     * @return array of country objects
     */
    protected function getAllowedCountries($store_id = "")
    {
        if(!$store_id) {
            $store_id = Mage::app()->getStore()->getId();
        }
        return $this->getCountries(explode(',', (string)Mage::getStoreConfig('general/country/allow', $store_id)));
    }

    /**
     * Get either all countries or a specific set of countries
     * @param null $country_ids
     *
     * @return array
     */
    public function getCountries($country_ids = null)
    {
        $countries = array();
        if(!$this->_countries) {
            $this->_initCountries();
        }
        if($country_ids && !empty($country_ids)) {
            if(!is_array($country_ids)) {
                $country_ids = array($country_ids);
            }
            foreach($country_ids as $country_id) {
                if(array_key_exists($country_id, $this->_countries)) {
                    $countries [$country_id] = $this->_countries[$country_id];
                }
            }
        }
        if($country_ids === null) {
            return $this->_countries;
        }
        return $countries;
    }

    /**
     * Initialize Countries and sort them by locale name
     */
    private function _initCountries()
    {
        $collection = Mage::getModel("directory/country")->getCollection();
        $sort_collection = array();
        $country_collection = array();
        foreach($collection as $country) {
            $sort_collection[$country->getCountryId()] = $country->getName();
            $country_collection[$country->getCountryId()] = $country;
        }
        //Sort countries by locale names
        $sort_collection = $this->asortMultibyte($sort_collection);
        foreach($sort_collection as $country_id => $country_name) {
            $this->_countries[$country_id] = $country_collection[$country_id];
        }
    }

    /**
     * Get allowed countries by store
     *
     * @param string $store
     *
     * @return array|bool
     */
    public function getAvailableCountries($store = null)
    {
        return $this->getAllowedCountries($store);
    }

    /**
     * Sort an array with multibyte ( A,Ä,B,...)
     * @param array $sort
     * @return array|bool
     */
    protected function asortMultibyte(array $sort)
    {
        if(empty($sort)) {
            return false;
        }
        $oldLocale = setlocale(LC_COLLATE, "0");
        $localeCode = Mage::app()->getLocale()->getLocaleCode();
        // use fallback locale if $localeCode is not available
        setlocale(LC_COLLATE, $localeCode . '.utf8', 'C.utf-8', 'en_US.utf8');
        asort($sort, SORT_LOCALE_STRING);
        setlocale(LC_COLLATE, $oldLocale);
        return $sort;
    }

    /**
     * Get the browser language
     * @return string
     */
    public function getBrowserLanguage()
    {
        $zendLocale = new Zend_Locale();
        return $zendLocale->getLanguage();
    }

    /**
     * Function so set the country fallback (necessary if ip can't be detected)
     * @param $country
     */
    public function setCountryFallback($country)
    {
        if($country) {
            $this->country_fallback = $country;
        }
    }

    /**
     * Locale of the current browser (en-US)
     * @return null|string
     */
    public function getBrowserLocale()
    {
        return Zend_Locale::getLocaleToTerritory($this->getBrowserLanguage());
    }

    /**
     * default Language by store
     *
     * @param null $store
     *
     * @return mixed
     */
    public function getLanguageByStore($store = null)
    {
        $locale = $this->getLocaleByStore($store);
        return $this->getLanguageCodeByLocale($locale);
    }

    /**
     * Returns the country code of a given store
     * @param null $store
     * @return string
     */
    public function getCountryByStoreLocale($store = null)
    {
        $locale = $this->getLocaleByStore($store);
        return $this->getCountryCodeByLocale($locale);
    }

    /**
     * Magento locale setting for current store
     * @param null $store
     * @return string
     */
    public function getLocaleByStore($store = null)
    {
        return Mage::getStoreConfig("general/locale/code", $store);
    }

    /**
     * Retrieve the localized name of the language (eg. German, Deutsch, ...)
     * @param null $store
     * @return mixed
     */
    public function getStoreTranslatedLanguage($store = null)
    {
        $store = Mage::app()->getStore($store);
        if(!$this->translation_list) {
            $store_locale = Mage::app()->getLocale()->getLocaleCode();
            $this->translation_list = Zend_Locale::getTranslationList('language', $store_locale);
        }
        return $this->translation_list[$this->getLanguageByStore($store)];
    }

    /**
     * Splits the locale to get the language code
     * @param $store_locale
     * @return string
     */
    public function getLanguageCodeByLocale($store_locale)
    {
        return substr($store_locale, 0, strpos($store_locale, "_"));
    }

    /**
     * Splits the locale to get the country code
     * @param $store_locale
     * @return string
     */
    public function getCountryCodeByLocale($store_locale)
    {
        return substr($store_locale, strpos($store_locale, "_") + 1);
    }

    /**
     * Default country by store
     *
     * @param null $store
     *
     * @return mixed
     */
    public function getCountryCodeByStore($store = null)
    {
        return Mage::getStoreConfig("general/country/default", $store);
    }

    /**
     *
     * @param null $country if null country will be detected by ip
     *
     * @return array
     */
    public function getStoresByCountry($country = null)
    {
        $stores = array();
        if(!$country) {
            $country = $this->getUserCountry()->getCountryId();
        }
        foreach($this->getStores() as $store) {
            if($country == $this->getCountryCodeByStore($store)) {
                $stores [$store->getId()] = $store;
            }
        }
        uasort($stores, array($this, 'sortStores'));
        return $stores;
    }

    /**
     * Sort Stores according to the default settings in Backend
     * @param Mage_Core_Model_Store $store1
     * @param Mage_Core_Model_Store $store2
     * @return int
     */
    protected function sortStores($store1, $store2){
        if (Mage::app()->getDefaultStoreView()->getId() == $store1->getId()) {
            return -1;
        }
        if (Mage::app()->getDefaultStoreView()->getId() == $store2->getId()) {
            return 1;
        }
        return 0;
    }

    /**
     * Returns the default store view of a given country
     * @param null $country
     * @return mixed
     */
    public function getDefaultStoreByCountry($country = null)
    {
        return reset($this->getStoresByCountry($country));
    }

    /**
     *  Get all Stores
     * @param bool $asJson
     * @return string | array
     */
    public function getStores($asJson = false)
    {
        if(!$this->stores) {
            $stores = Mage::getModel('core/store')->getCollection()->addFieldToFilter('is_active', 1);
            foreach($stores as $store) {
                $this->stores[$store->getId()] = $store;
            }
        }
        if($asJson) {
            $return = array();
            foreach($this->stores as $key => $store) {
                $return[$key] = $store->getName();
            }
            return Mage::helper('core')->jsonEncode($return);
        }
        return $this->stores;
    }

    /**
     * Get current nice url from store
     *
     * @param null $store
     *
     * @return string
     */
    public function getStoreCurrentUrl($store = null)
    {
        $store = Mage::app()->getStore($store);
        $currentStore = Mage::app()->getStore();
        //let the user change the language in the store
        $rewrite_collection = Mage::getSingleton("core/url_rewrite")->getCollection();
        $idPath = Mage::app()->getRequest()->getPathInfo();
        $rewrite_collection->addStoreFilter($store->getId());
        $rewrite_collection->addFieldToFilter("target_path", $idPath);
        $rewrite = $rewrite_collection->getFirstItem();
        if($rewrite && $rewrite->getId()) {
            $url = $rewrite->getRequestPath();
            if (strpos( $url, "http" ) === false) {
                $url = $store->getBaseUrl() . $url;
            }
            return str_replace( $currentStore->getCode(), $store->getCode(), $url );
        } else {
            return $store->getCurrentUrl();
        }
    }

    /**
     * Get Store by Country and Language
     *
     * @param null $country if null country will be detected by ip
     * @param null $language if null default browser language will be used
     *
     * @return mixed
     */
    public function getStoreByCountryAndLanguage($country = null, $language = null)
    {
        $stores = $this->getStoresByCountry($country);
        if($stores) {
            if(!$language) {
                $language = $this->getBrowserLanguage();
            }
            foreach($stores as $store) {
                if($this->getLanguageByStore($store) == $language) {
                    return $store;
                }
            }
            return reset($stores);
        }
        return Mage::app()->getStore();
    }

    /**
     * @param $website Mage_Core_Model_Website
     * @param null $language
     * @return Mage_Core_Model_Store|mixed
     */
    public function getStoreByWebsiteAndLanguage($website, $language = null)
    {
        $stores = $website->getStores();
        if($stores) {
            if(!$language) {
                $language = $this->getBrowserLanguage();
            }
            foreach($stores as $store) {
                if($this->getLanguageByStore($store) == $language) {
                    return $store;
                }
            }
            return reset($stores);
        }
        return Mage::app()->getStore();
    }

    /**
     * Check whether last call was a redirect
     * @return mixed
     */
    public function isRedirect()
    {
        return Mage::getSingleton("core/session")->getData(self::GEO_REDIRECT, true);
    }

    /**
     * Set that this is a redirect from the geo module
     * @param null $store
     */
    public function setRedirect($store = null)
    {
        Mage::getSingleton("core/session")->setData(self::GEO_REDIRECT, true);
        if($store) {
            Mage::getSingleton("core/session")->setData(self::GEO_REDIRECT . '_store', $store);
        }
    }

    /** @return Mage_Core_Model_Store */
    public function getRedirectStore()
    {
        return Mage::getSingleton("core/session")->getData(self::GEO_REDIRECT . '_store');
    }

    /**
     * true if the current url has a store code in it
     * @return bool
     */
    protected function hasStoreCode()
    {
        if($this->_canBeStoreCodeInUrl()) {
            $storeCode = "";
            $store_code = Mage::app()->getRequest()->getStoreCodeFromPath();
            if(strpos(Mage::app()->getRequest()->getRequestUri(), $store_code) !== false) {
                $storeCode = $store_code;
            }
            $stores = Mage::app()->getStores(true, true);
            if($storeCode !== '' && isset($stores[$storeCode])) {
                return true;
            }
        }
        return false;
    }

    /**
     * true if a store code is allowed in url by config
     * @return bool
     */
    protected function _canBeStoreCodeInUrl()
    {
        return Mage::getStoreConfigFlag(Mage_Core_Model_Store::XML_PATH_STORE_IN_URL);
    }

    /**
     * returns the config value of the excluded urls
     * @return mixed
     */
    protected function getExcludeUrls()
    {
        return Mage::getStoreConfig(self::EXCLUDE_URLS);
    }

    /**
     * returns the excluded urls array
     * transforms the config value into the array if necessary
     * @return array
     */
    protected function getExcludedUrlsArray()
    {
        if(!$this->exclude_urls) {
            $exclude_urls_string = $this->getExcludeUrls();
            if($exclude_urls_string) {
                $exclude_urls_string = str_replace("/", "\\/", $exclude_urls_string);
                $this->exclude_urls = explode("\n", $exclude_urls_string);
                foreach($this->exclude_urls as $key => $url){
                    if(!$url){
                        unset($this->exclude_urls[$key]);
                    }
                }
            }
        }
        return $this->exclude_urls;
    }

    /**
     * function to determine if a given url is excluded from redirect - e.g. payment urls
     * @param $url
     * @return bool
     */
    public function isUrlExcluded($url)
    {
        $exclude_urls = $this->getExcludedUrlsArray();
        if($exclude_urls && count($exclude_urls)) {
            foreach($exclude_urls as $exclude_url) {
                if(preg_match("/" . $exclude_url . "/", $url)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * function to determine if the current ulr is excluded
     * @return bool
     */
    public function isCurrentUrlExcluded()
    {
        return $this->isUrlExcluded(Mage::app()->getRequest()->getRequestUri());
    }

    /**
     * returns a cookie value
     * @param null $name
     * @return mixed
     */
    public function getCookie($name = null)
    {
        return Mage::getSingleton('core/cookie')->get($name);
    }

    /**
     * Sets a cookie value
     * @param $name
     * @param $data
     * @return $this
     */
    public function setCookie($name, $data)
    {
        if(!is_string($data))
            $data = serialize($data);
        Mage::getSingleton('core/cookie')->set($name, $data, true, null, null, null, false);
        $_COOKIE[$name] = $data;
        return $this;
    }
}