<?php

class Cyberhouse_Geo_Helper_Http extends Mage_Core_Helper_Http {
    /**
     * Retrieve Client Remote Address
     *
     * @param bool $ipToLong converting IP to long format
     * @return string IPv4|long
     */
    public function getRemoteAddr($ipToLong = false)
    {
        $ips = parent::getRemoteAddr($ipToLong);
        $ips = explode(",",$ips);
        return reset($ips);
    }
}