<?php

class Cyberhouse_Geo_Model_Observer {


    public function redirectLocale ($observer) {
        /**
         * @var $helper Cyberhouse_Geo_Helper_Data
         */
        if ( !Mage::app()->getStore()->isAdmin() && $this->isFirstVisit() && !Mage::helper("geo")->isCurrentUrlExcluded()) {
            $redirect_store = Mage::helper("geo")->getStoreByCountryAndLanguage();
            if ( $redirect_store->getId() != Mage::app()->getStore()->getId() ) { //Redirect
                if ( !Mage::app()->getStore()->getIsActive() && $redirect_store === null ) {
                    $redirect_store = Mage::app()->getWebsite()->getDefaultStore();
                }
                if ( $redirect_store && !Mage::helper("geo")->isUrlExcluded($redirect_store->getCurrentUrl(false)) ) {
                    Mage::helper("geo")->setRedirect(Mage::app()->getStore());
                    $this->redirectToUrl($redirect_store->getCurrentUrl(false));
                }
            }
        }
    }

    /**
     * Redirect a 404 page to the home page if it is a broken link
     *
     * @param $observer
     */
    public function redirect404 ($observer) {
        $lang = Mage::app()->getRequest()->getParam('lang', true);
        $from_store = Mage::app()->getRequest()->getParam('___from_store', false);
        if ( $lang === "false" || $from_store == true ) { //Redirect
            $this->redirectToHome();
        }
    }

    /**
     * Redirect to the home page if the current category is not visible in the new store_view
     *
     * @param $observer
     */
    public function redirectInvisibleCategory ($observer) {
        if ( $observer->getEvent()->getCategory() ) {
            $category = $observer->getEvent()->getCategory();
            /** @var $category Mage_Catalog_Model_Category */
            $category_collection = $category->getParentCategories();
            /** @var $par_category Mage_Catalog_Model_Category */
            foreach ( $category_collection as $par_category ) {
                if ( $par_category->hasIsActive() && !$par_category->getIsActive() ) {
                    $this->redirectToHome();
                }
            }
        }
    }

    private function redirectToHome () {
      $this->redirectToUrl(Mage::getUrl(""));
    }

    private function redirectToUrl( $url ) {
        Mage::app()->getResponse()->setRedirect($url);
        Mage::app()->getResponse()->sendResponse();
        exit();
    }

    /**
     * check whether this is the first visit
     * @return boolean
     */
    protected function isFirstVisit () {
        if ( Mage::helper("geo")->getCookie("first_visit") ) {
            return false;
        }
        else {
            Mage::helper("geo")->setCookie("first_visit",true);
            return true;
        }
    }
}