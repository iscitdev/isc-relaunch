jQuery(document).ready(function(){

    /**  IE does not support Fade*/
    var isIE = window.ActiveXObject || "ActiveXObject" in window;
    if (isIE) {
        jQuery('.modal').removeClass('fade');
    };

    if(jQuery('#modalGeo').length > 0){
        jQuery('#modalGeo').modal({
            backdrop: "static",
            keyboard: false
        });
    }
});
