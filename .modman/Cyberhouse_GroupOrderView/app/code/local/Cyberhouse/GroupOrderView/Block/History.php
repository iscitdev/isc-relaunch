<?php   
class Cyberhouse_GroupOrderView_Block_History extends Mage_Sales_Block_Order_History{


    public function __construct()
    {
        parent::__construct();
        $this->setTemplate('grouporderview/history.phtml');

        $customer =  Mage::getSingleton('customer/session')->getCustomer();
        if($customer->hasDebitorNo()){

            $debitorNo = Mage::getModel('eav/entity_attribute')->loadByCode('1', 'debitor_no');

            $orders = Mage::getResourceModel('sales/order_collection')
                ->addFieldToSelect('*')
                ->addFieldToFilter("debitor_table.value", $customer->getDebitorNo())
                ->addFieldToFilter('state', array('in' => Mage::getSingleton('sales/order_config')->getVisibleOnFrontStates()))
                ->setOrder('created_at', 'desc')
            ;

            $orders->getSelect()
                ->joinLeft(array('debitor_table' => 'customer_entity_varchar'), 'debitor_table.entity_id=main_table.customer_id', array('debitor_no' => 'value'))
                ->where('debitor_table.attribute_id='.$debitorNo->getAttributeId());

        $this->setOrders($orders);
        }

        Mage::app()->getFrontController()->getAction()->getLayout()->getBlock('root')->setHeaderTitle(Mage::helper('grouporderview')->__('Our Orders'));
    }


}