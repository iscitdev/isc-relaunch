<?php

require_once(Mage::getModuleDir('controllers','Mage_Sales').DS.'OrderController.php');

class Cyberhouse_GroupOrderView_OrderController extends Mage_Sales_OrderController {


    /**
     * Check order view availability
     *
     * @param   Mage_Sales_Model_Order $order
     * @return  bool
     */
    protected function _canViewOrder($order)
    {
        $customer = Mage::getSingleton('customer/session')->getCustomer();
        $customerId = $customer->getId();
        $availableStates = Mage::getSingleton('sales/order_config')->getVisibleOnFrontStates();
        if ($order->getId() && $order->getCustomerId()){
            $orderCustomer = Mage::getModel("customer/customer")->load($order->getCustomerId());
            $ownOrder = $order->getCustomerId() == $customerId;
            $debitorOrder = $orderCustomer->getDebitorNo() == $customer->getDebitorNo();
            if( $ownOrder || $debitorOrder ) {
                if (in_array( $order->getState(), $availableStates, $strict = true )) {
                    return true;
                }
            }
        }
        return false;
    }


    /**
     * Customer order history
     */
    public function ourhistoryAction()
    {
        $this->loadLayout();
        $this->_initLayoutMessages('catalog/session');

        $this->getLayout()->getBlock('head')->setTitle($this->__('Our Orders'));

        if ($block = $this->getLayout()->getBlock('customer.account.link.back')) {
            $block->setRefererUrl($this->_getRefererUrl());
        }
        $this->renderLayout();
    }

    /**
     * Init layout, messages and set active block for customer
     *
     * @return null
     */
    protected function _viewAction()
    {
        if (!$this->_loadValidOrder()) {
            return;
        }

        $this->loadLayout();
        $this->_initLayoutMessages('catalog/session');

        $navigationBlock = $this->getLayout()->getBlock('customer_account_navigation');
        if ($navigationBlock) {

            if (strpos($this->_getRefererUrl(),"ourhistory") !== false) {
                $navigationBlock->setActive('sales/order/ourhistory');
            }
            else {
                $navigationBlock->setActive('sales/order/history');
            }
        }
        $this->renderLayout();
    }

}