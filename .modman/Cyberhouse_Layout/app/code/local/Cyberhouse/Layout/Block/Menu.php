<?php

class Cyberhouse_Layout_Block_Menu extends Mage_Core_Block_Template {

	protected function _getItems() {
		$items = array();

		$rootCategoryId = Mage::app()->getStore()->getRootCategoryId();
        /** @var Mage_Catalog_Model_Category $rootCategory */
		$rootCategory = Mage::getModel('catalog/category')->load($rootCategoryId);

		$rootChildren = $rootCategory->getChildrenCategories();


		foreach($rootChildren as $child){
			$category = Mage::getModel('catalog/category')->load($child->getId());
			$currentCategory = Mage::registry('current_category');
			$currentCategoryId = ($currentCategory) ? $currentCategory->getId() : 0;

			$items[] = array(
				'name' => $category->getName(),
				'link' => $category->getUrl(),
				'active' => $category->getId() == $currentCategoryId,
				'class' => 'category-'.$category->getId() ." _". $category->getCategoryAssetId(),
				'id' => $category->getId(),
				'hasChildren' => $category->getChildren()>0,
				'assetId' => $category->getAssetId(),
				'category' => $category
			);


		}


		//add service center module
		$items[] = array(
			'name' => $this->__('Service-Center'),
			'link' => Mage::getUrl('cms/service-center'),
			'active' => Mage::app()->getFrontController()->getRequest()->getRouteName() == 'servicecenter',
			'class' => 'page-service-center'
		);

		// add cms pages by identifier for localized links and urls
		$cmsPages = array('isc-contact');

		foreach ($cmsPages as $cmsPage) {
			$page = Mage::getModel('cms/page')->setStoreId(Mage::app()->getStore()->getId())->load($cmsPage, 'identifier');
            if ($page->getIdentifier()) {
				$items[] = array(
					'name' => $page->getTitle(),
					'link' => Mage::getBaseUrl().$page->getIdentifier(),
					'active' => $page->getIdentifier() == Mage::getSingleton('cms/page')->getIdentifier(),
					'class' => 'page-'.$page->getId().' page-'.$page->getIdentifier()
				);
			}
		}

		// add cart to main menu
		$count = Mage::helper('checkout/cart')->getItemsQty();

		$countString = ' <span id="cart-count">';
		$countString .= ($count > 0) ? ' ('.$count.')': '';
		$countString .= '</span>';

		$items[] = array(
			'name' => $this->__('Cart').$countString,
			'link' => Mage::getUrl('checkout/cart'),
			'active' => Mage::app()->getFrontController()->getRequest()->getRouteName() == 'checkout',
			'class' => 'cart',
		);

		return $items;
	}


}