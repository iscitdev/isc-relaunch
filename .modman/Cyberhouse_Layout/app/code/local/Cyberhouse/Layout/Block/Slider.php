<?php

class Cyberhouse_Layout_Block_Slider extends Mage_Core_Block_Template {

    const CACHE_TAG = "block_slider";

    /**
     * override the constructor to set template
     */
    public function _construct()
    {
        $this->addData(array(
            'cache_lifetime' => 3600,
            'cache_tags' => array(self::CACHE_TAG)
        ));
        parent::_construct();
    }

    public function getSliderElements(){
        $elements = "";
        $collection = Mage::getModel("widget/widget_instance")->getCollection()
            ->addFieldToFilter("instance_type","cyberhouse_layout/slider_element")
            ->addFieldToFilter('store_ids', array('in' => Mage::app()->getStore()->getId()))
            ->addOrder("sort_order",Varien_Data_Collection::SORT_ORDER_ASC);

        /** @var $element Mage_Widget_Model_Widget_Instance */
        foreach ($collection as $element) {
            $elements .= $this->getLayout()->createBlock($element->getType(),$element->getTitle(),$element->getWidgetParameters()?$element->getWidgetParameters():array())->toHtml();
        }

        return $elements;
    }
}