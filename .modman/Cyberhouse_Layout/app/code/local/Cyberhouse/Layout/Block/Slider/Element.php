<?php

class Cyberhouse_Layout_Block_Slider_Element
    extends Mage_Core_Block_Template
    implements Mage_Widget_Block_Interface {

    protected $_template = "cyberhouse/slider_element.phtml";

    public function getCategory() {
        if ($this->getData( 'category_id' )) {
            $category_id = explode( "/", $this->getData( 'category_id' ) );
            $category_id = $category_id[1];
            if ($category_id) {
                $category = Mage::getModel( 'catalog/category' )->load( $category_id );
                return $category;
            }
        }
        return null;
    }

    public function getProduct() {
        if ($this->getData( 'product_id' )) {
            $product_id = explode( "/", $this->getData( 'product_id' ) );
            $product_id = $product_id[1];
            if ($product_id) {
                $product = Mage::getModel( 'catalog/product' )->load( $product_id );
                return $product;
            }
        }
        return null;
    }

    public function getLink() {
        $link = $this->getData( 'link' );
        if (!$link) {
            if ($this->getData( 'category_id' )) {
                $category = $this->getCategory();
                if ($category) {
                    $link = $this->getCategory()->getUrl();
                }
            }
            if ($this->getData( 'product_id' )) {
                $product = $this->getProduct();
                if ($product) {
                    $link = $this->getProduct()->getUrl();
                }
            }
        }
        return $link;
    }
    public function getTarget() {
        $target = $this->getData( 'link_target' );
        return $target;
    }

    public function getImageUrl() {
        if (file_exists( Mage::getDesign()->getSkinBaseDir() . DS . $this->getImage() )) {
            return $this->getSkinUrl( $this->getImage() );
        } else {
            return Mage::getBaseUrl( "media" ) . DS . $this->getImage();
        }
    }

    public function getSpan(){
        return "span".( $this->getData('span')?$this->getData('span'):"4");
    }

}