<?php

class Cyberhouse_Layout_Block_Slider_Widget_Chooser_Category extends Mage_Adminhtml_Block_Catalog_Category_Widget_Chooser {
    /**
     * Prepare chooser element HTML
     *
     * @param Varien_Data_Form_Element_Abstract $element Form Element
     * @return Varien_Data_Form_Element_Abstract
     */
    public function prepareElementHtml(Varien_Data_Form_Element_Abstract $element)
    {
        $element = parent::prepareElementHtml($element);

        $chooseButton = $this->getLayout()->createBlock('adminhtml/widget_button')
            ->setType('button')
            ->setId('control_delete'.$element->getId())
            ->setClass('btn-chooser')
            ->setLabel('Delete')
            ->setOnclick("unset_id(this,'category_id');")
            ->setDisabled($element->getReadonly());


        $element->setData('after_element_html', $element->getData('after_element_html').$chooseButton->toHtml());
        return $element;
    }
}