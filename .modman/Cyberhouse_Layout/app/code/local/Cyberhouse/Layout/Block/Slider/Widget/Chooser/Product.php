<?php

class Cyberhouse_Layout_Block_Slider_Widget_Chooser_Product extends Mage_Adminhtml_Block_Catalog_Product_Widget_Chooser {
    /**
     * Prepare chooser element HTML
     *
     * @param Varien_Data_Form_Element_Abstract $element Form Element
     * @return Varien_Data_Form_Element_Abstract
     */
    public function prepareElementHtml(Varien_Data_Form_Element_Abstract $element)
    {
        $element = parent::prepareElementHtml($element);

        $javascript = '
                <script type="text/javascript">//<![CDATA[
                function unset_id(item, name){
                    $$(\'input[type="hidden"][name="parameters[\'+name+\']"]\')[0].value = "";
                    $(item).siblings(\'label\')[0].update("");
                }
            //]]></script>
            ';

        $chooseButton = $this->getLayout()->createBlock('adminhtml/widget_button')
            ->setType('button')
            ->setId('control_delete'.$element->getId())
            ->setClass('btn-chooser')
            ->setLabel('Delete')
            ->setOnclick("unset_id(this,'product_id');")
            ->setDisabled($element->getReadonly());


        $element->setData('after_element_html', $element->getData('after_element_html').$chooseButton->toHtml().$javascript);
        return $element;
    }
}