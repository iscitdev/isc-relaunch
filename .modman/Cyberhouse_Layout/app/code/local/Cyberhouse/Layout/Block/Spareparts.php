<?php

class Cyberhouse_Layout_Block_Spareparts extends Mage_Core_Block_Template
{

    private $sparepartsHelper;

    private $_explosionImage = null;
    private $_parts = null;

    public function  _construct()
    {
        parent::_construct();
        $this->getCurrentProduct()->setData('url', null);
        $this->getCurrentProduct()->setData('request_path', null);

        $this->sparepartsHelper = Mage::helper('cyberhouse_layout/spareparts');
    }


    public function getSpareparts($_product = null)
    {
        if(!$_product) {
            $_product = $this->getCurrentProduct();
        }

        if(!$_product) {
            return false;
        }

        $sparepartSkus = $this->sparepartsHelper->getSpareparts($_product);

        $data = array();
        $altParts = array();

        foreach($sparepartSkus as $sku) {
            $prod = $this->sparepartsHelper->getProductBySku($sku, true);

            $original = $this->convertProductToRow($prod);
            if(!$this->sparepartsHelper->isInStock($prod)) {
                $alternative = $this->getAlternative($prod);
                if($alternative && $alternative->getId()){
                    $original['has_alternative'] = true;
                    $original['sku_alternative'] = $alternative->getSku();
                    $altParts[$alternative->getSku()] = $this->convertProductToRow($alternative);
                    $altParts[$alternative->getSku()]['pos'] = $original['pos'];
                    $altParts[$alternative->getSku()]['is_alternative'] = true;
                }
            }
            if($original) {
                $data[$prod->getSku()] = $original;
            }
        }
        $data = array_merge($data, $altParts);

        uasort($data, array($this, "_sortSpareparts"));
        return $data;
    }

    /**
     * Return the on first in stock alternative part of a product
     * @param $prod
     * @return Mage_Catalog_Model_Product
     */
    protected function getAlternative($prod){
        $alt_parts = $this->getAlternativePartArray($prod);
        if($alt_parts){
            $alt_parts_skus = array();
            foreach($alt_parts as $alternative){
                $alt_parts_skus [] = $alternative['pl'][0]['sku'];
            }
            $alt_parts_skus = array_unique($alt_parts_skus);
            $this->getPartsHelper()->loadSkus($alt_parts_skus);
            foreach($alt_parts_skus as $sku){
                $alternative = $this->getPartsHelper()->getProductBySku($sku);
                if($this->getPartsHelper()->isInStock($alternative)) {
                    return $alternative;
                }
            }
        }
        return Mage::getModel("catalog/product");
    }

    /**
     * returns alternative parts array from product if possible
     * @param $_prod
     * @return mixed
     */
    private function getAlternativePartArray($_prod){

        $data = Mage::helper('cyberhouse_layout/data')->jsonToArray($_prod->getParts());
        if (isset( $data['altParts'] )) {
            return  Mage::helper('cyberhouse_layout/data')->jsonToArray($data['altParts']);
        }
        return "";
    }

    private $partsHelper;

    /**
     * @return Cyberhouse_Catalog_Helper_Data
     */
    protected function getPartsHelper()
    {
        if(!$this->partsHelper) {
            $this->partsHelper = Mage::helper("cyberhouse_layout/spareparts");
        }
        return '';
    }

    //First sort by position then by alternative and then by stock
    public function _sortSpareparts($a, $b)
    {
        if(!$a['pos'])
            return 1;
        if(!$b['pos'])
            return -1;
        if($a['pos'] < $b['pos']) {
            return -1;
        }
        if($a['pos'] > $b['pos']) {
            return 1;
        }

        if(!isset($a['has_alternative']) && !isset($a['is_alternative'])) {
            return 1;
        }
        if(!isset($b['has_alternative']) && !isset($b['is_alternative'])) {
            return 1;
        }

        return ($a['qty_in_stock'] < $b['qty_in_stock']) ? -1 : 1;
    }

    protected $cartItems;

    protected function getCartItems()
    {
        if(!$this->cartItems) {
            $cartItemsCollection = Mage::getSingleton('checkout/session')->getQuote()->getItemsCollection();
            foreach($cartItemsCollection as $cartItem) {
                $this->cartItems[$cartItem->getSku()] = true;
            }
        }
        return $this->cartItems;
    }

    protected function convertProductToRow($prod)
    {
        if(!$prod || !$prod->getId()) {
            return false;
        }

        $cartItems = $this->getCartItems();
        $cartHelper = $this->helper('checkout/cart');

        /* @var $prod Mage_Catalog_Model_Product */
        if($prod->getStockItem() && $prod->getStockItem()->getQty() !== null) {
            $qtyInStock = $prod->getStockItem()->getQty();
        } else {

            $qtyInStock = Mage::getModel('cataloginventory/stock_item')->loadByProduct($prod)->getQty();
        }
        $formattedPrice = $this->sparepartsHelper->getFormattedPrice($prod);
        $component_url = "";
        /*if($this->_hasUrl($prod)) {
            $component_url = $this->getCurrentProduct(true)->getProductUrl() . "component/" . $prod->getSku();
        }*/
        $row = array(
            'product' => $prod,
            'name' => $prod->getName(),
            'sku' => $prod->getSku(),
            'product_id' => $prod->getId(),
            'pos' => $prod->getPartsListOrder(),
            'price' => $this->sparepartsHelper->getPrice($prod),
            'formatted_price' => $formattedPrice,
            'img' => $prod->getProductImage(500),
            'asset_id' => $prod->getAssetId(),
            'add_url' => $cartHelper->getAddUrl($prod),
            'ajax_url' => Mage::getUrl('checkout/cart/ajaxadd'),
            'salable' => $prod->isSalable(),
            'qty_in_stock' => $qtyInStock,
            'in_cart' => isset($cartItems[$prod->getSku()]) ? 1 : 0,
            'component_url' => $component_url,
            'sku_alternative' => "",
            "has_alternative" => false,
            "customer_group" => '',
            "ident_no" => $prod->getProductIdent()
        );
        return $row;
    }

    protected function _hasUrl($product)
    {
        return $this->sparepartsHelper->hasSpareparts($product);
    }

    private function _getExplosionData()
    {
        if($this->_explosionImage == null) {
            $_product = $this->getCurrentProduct();
            if(!$_product->getExplosionImage()) {
                $this->_explosionImage = false;
            }
            else {
                $this->_explosionImage = $_product->getExplosionImage();
            }
        }

        return $this->_explosionImage;
    }

    private function _getPartsData()
    {
        if($this->_parts == null) {
            $_product = $this->getCurrentProduct();
            if(!$_product->getParts()) {
                $this->_parts = false;
            }
            else {
                $this->_parts = $_product->getParts();
            }
        }

        return Mage::helper('cyberhouse_layout/data')->jsonToArray($this->_parts);
    }

    public function getExplosionDrawingUrl()
    {
        $data = $this->_getExplosionData();
        if(!($data)) {
            return false;
        }
        return Mage::helper('cyberhouse_url/data')->getExplosionDrawingUrl($data);
    }

    public function getExplosionDrawingAssetId()
    {
        $data = $this->_getExplosionData();
        if(!isset($data)) {
            return false;
        }
        return $data;
    }

    public function getExplosionDrawingParts()
    {
        $data = $this->_getPartsData();

        if(!isset($data['parts']) || empty($data['parts'])) {
            return array();
        }
        $data = Mage::helper('cyberhouse_layout/data')->jsonToArray($data['parts']);

        $dataFinal = array();
        foreach($data as $item) {
            $partsList = array();
            if(isset($item['drawing']['explosionMarkers'])){
                $explosionitem = $item['drawing']['explosionMarkers'];
                    foreach($explosionitem as $myitem){
                        $myitem['sku'] = $item['sku'];
                        $myitem['pos'] = $item['pos'];
                        $dataFinal[$item['pos']] = $myitem;

                    }
            }
        }
        uasort($dataFinal, array($this, "_sortSpareparts"));
        return $dataFinal;

    }

    public function getPriceInfoHtml()
    {
        $price_block = $this->getLayout()->createBlock("magesetup/catalog_product_price");
        if($price_block) {
            $htmlTemplate = $this->getLayout()->createBlock('core/template')
                ->setTemplate('magesetup/price_info.phtml')
                ->setFormattedTaxRate($price_block->getFormattedTaxRate())
                ->setIsIncludingTax($price_block->isIncludingTax())
                ->setIsIncludingShippingCosts($price_block->isIncludingShippingCosts())
                ->setIsShowShippingLink($price_block->isShowShippingLink())
                ->setIsShowWeightInfo($price_block->getIsShowWeightInfo())
                ->setFormattedWeight($price_block->getFormattedWeight())
                ->toHtml();
            return $htmlTemplate;
        }

    }

    /**
     * @return Mage_Catalog_Model_Product
     */
    public function getCurrentProduct($from_registry = false)
    {
        if($from_registry) {
            return Mage::registry('current_product');
        }
        return Mage::getBlockSingleton("catalog/product_view")->getProduct();
    }

    /** @return Mage_Catalog_Model_Category $category */
    protected function getCurrentCategory()
    {
        return Mage::registry('current_category');
    }

    public function initPriceAlertBlock($product){
        $block = $this->getPriceAlertBlock()->setProduct($product);
        $block->preparePriceAlertData();
        return $block;
    }

    public function initStockAlertBlock($product){
        $block = $this->getStockAlertBlock()->setProduct($product);
        $block->prepareStockAlertData();
        return $block;
    }

    /**
     * @return Cyberhouse_ProductAlert_Block_Product_View
     */
    public function getPriceAlertBlock(){
        return $this->getAlertUrlsBlock()->getChild('productalert_price');
    }

    /**
     * @return Cyberhouse_ProductAlert_Block_Product_View
     */
    public function getStockAlertBlock(){
        return $this->getAlertUrlsBlock()->getChild('productalert_stock');
    }

    public function getExtraAlert ( $product )
    {
        $block = $this->getAlertUrlsBlock()->getChild('backtostock_notice');
        if($block){
            return $block->setProduct($product);
        }
        return $this->getLayout()->createBlock("core/text_list");
    }

    /** @return Mage_Core_Block_Text_List */
    protected function getAlertUrlsBlock ()
    {
        return $this->getParentBlock()->getChild('alert_urls');
    }

}