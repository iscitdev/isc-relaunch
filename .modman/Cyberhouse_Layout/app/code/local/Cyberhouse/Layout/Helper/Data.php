<?php

class Cyberhouse_Layout_Helper_Data extends Mage_Core_Helper_Abstract {

	public function jsonToArray($json) {
		$decoded = json_decode($json, true);
		if ($decoded === null && json_last_error() !== JSON_ERROR_NONE) {
			return false;
		}
		return $decoded;
	}

	public function getProductCategoryName($product) {
		$categoryIds = $product->getCategoryIds();
		if (empty($categoryIds)) {
			return false;
		}

		// TODO: handle multiple categories?
		$categoryId = reset($categoryIds); // use first category

		$category = Mage::getModel('catalog/category')->load($categoryId);

		return ($category->getName()) ? $category->getName() : false;
	}

    public function getNoImageAssetId(){
        return Mage::getStoreConfig("iscconfig_options/asset_settings/asset_no_image_id");
    }
}