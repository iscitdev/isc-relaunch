<?php


/**
 * Class Cyberhouse_Layout_Helper_Spareparts
 */
class Cyberhouse_Layout_Helper_Spareparts extends Mage_Core_Helper_Abstract
{
    private $productCache = array();

    /**
     * @param $product Mage_Catalog_Model_Product
     */
    public function getSpareparts($product) {
        $skus = array();
        $sparepartsXml = $product->getData('sparepartmap');

        if(!empty($sparepartsXml)) {
            $xml = new SimpleXMLElement($sparepartsXml);

            $count = $xml->sparepart->count();

            for ($i = 0; $i < $count; $i++) {
                $productId = $xml->sparepart[$i]["productId"]->__toString();
                $skus[] = $productId;
            }
        }

        return $skus;
    }

    /**
     * If the product is already loaded it returns the product from cache otherwise it will be loaded
     * @param $sku
     * @param bool $autoload
     * @return Mage_Catalog_Model_Product
     */
    public function getProductBySku( $sku, $autoload = false ) {
        if (!$this->productCache || !array_key_exists( $sku, $this->productCache )) {
            if ($autoload) {
                $product = Mage::getModel( "catalog/product" )->loadByAttribute( "sku", $sku );
                if ($product && $product->getId()) {
                    $this->productCache[$sku] = $product;
                }
            }
            else {
                return null;
            }
        }
        return $this->productCache[$sku];
    }

    /**
     * @param Mage_Catalog_Model_Product $product
     * @return string
     */
    public function getFormattedPrice(Mage_Catalog_Model_Product $product){
        $price = $this->getPrice($product);
        $formattedPrice = ( ( $price > 0 ) ? Mage::helper( 'core' )->currency( $price, true, false ) . '*' : '----' );
        return $formattedPrice;
    }

    /**
     * @param Mage_Catalog_Model_Product $product
     * @return float
     */
    public function getPrice(Mage_Catalog_Model_Product $product){
        return Mage::helper( 'tax' )->getPrice( $product, $product->getPrice() );
    }

    public function hasSpareparts($product) {

    }

    /**
     * @param Mage_Catalog_Model_Product $product
     * @return boolean
     */
    public function isInStock($product){
        return $product && $product->getStockItem() && $product->getStockItem()->getQty() !== null && $product->getStockItem()->getQty() > 0;
    }
}