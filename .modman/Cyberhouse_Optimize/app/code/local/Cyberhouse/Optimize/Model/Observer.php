<?php

class Cyberhouse_Optimize_Model_Observer extends Mage_CatalogRule_Model_Observer
{
    protected $_preloadedPrices = array();

    public function beforeCollectTotals(Varien_Event_Observer $observer)
    {
        $quote = $observer->getQuote();
        $date = Mage::app()->getLocale()->storeTimeStamp($quote->getStoreId());
        $websiteId = $quote->getStore()->getWebsiteId();
        $groupId = $quote->getCustomerGroupId();

        $productIds = array();
        foreach ($quote->getAllItems() as $item) {
            $productIds[] = $item->getProductId();
        }

        $cacheKey = spl_object_hash($quote);

        if (!isset($this->_preloadedPrices[$cacheKey])) {
            $this->_preloadedPrices[$cacheKey] = Mage::getResourceSingleton('catalogrule/rule')
                ->getRulePrices($date, $websiteId, $groupId, $productIds);
        }

        foreach ($this->_preloadedPrices[$cacheKey] as $productId => $price) {
            $key = implode('|', array($date, $websiteId, $groupId, $productId));
            $this->_rulePrices[$key] = $price;
        }
    }


    public function addStockItemData($observer){
        $collection = $observer->getCollection();
        Mage::getSingleton("cataloginventory/stock")->addItemsToProducts($collection);
    }


    /**
     * Remove unnecessary handles
     * @param $observer
     */
    public function controllerActionLayoutLoadBefore($observer){
        $update = $observer->getLayout()->getUpdate();
        foreach ($update->getHandles() as $handle) {
            if( strpos($handle,'CATEGORY_') === 0
                || (strpos($handle,'PRODUCT_') === 0
                && strpos($handle,'PRODUCT_TYPE_') === false)) {
                $update->removeHandle($handle);
            }
        }
    }

    public function coreBlockAbstractToHtmlBefore( $observer ) {
        $block = $observer->getBlock();
        if ($block instanceof Mage_Cms_Block_Widget_Block || $block instanceof Mage_Cms_Block_Block) {
            $cacheKeyData = array(
                Mage_Cms_Model_Block::CACHE_TAG,
                $block->getBlockId(),
                Mage::app()->getStore()->getId()
            );
            $block->setCacheKey(implode(",",$cacheKeyData));
            $block->setCacheTags(array(Mage_Cms_Model_Block::CACHE_TAG));
            $block->setCacheLifetime(false);
        }
    }
}