<?php


class Cyberhouse_Optimize_Model_Resource_Product_Type_Configurable_Product_Collection extends Mage_Catalog_Model_Resource_Product_Type_Configurable_Product_Collection {
    public function isEnabledFlat(){
        return Mage_Catalog_Model_Resource_Product_Collection::isEnabledFlat();
    }
}