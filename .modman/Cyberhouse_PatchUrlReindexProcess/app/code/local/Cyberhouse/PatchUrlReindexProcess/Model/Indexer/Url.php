<?php

class Cyberhouse_PatchUrlReindexProcess_Model_Indexer_Url extends Mage_Catalog_Model_Indexer_Url
{

    protected function _registerProductEvent ( Mage_Index_Model_Event $event )
    {
        $product = $event->getDataObject();
        $visibilityOrStatusChange = false;

        if (( $product->dataHasChangedFor( 'status' ) && $product->getData( 'status' ) != Mage_Catalog_Model_Product_Status::STATUS_DISABLED ) ||
            ( $product->dataHasChangedFor( 'visibility' ) && $product->getData( 'visibility' ) != Mage_Catalog_Model_Product_Visibility::VISIBILITY_NOT_VISIBLE )
        ) {
            $visibilityOrStatusChange = true;
        }

        $dataChange = $product->dataHasChangedFor( 'url_key' )
            || $product->getIsChangedCategories()
            || $product->getIsChangedWebsites()
            || $visibilityOrStatusChange;

        if (!$product->getExcludeUrlRewrite() && $dataChange) {
            $event->addNewData( 'rewrite_product_ids', array( $product->getId() ) );
        }
    }
}