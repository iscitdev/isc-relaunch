<?php

class Cyberhouse_PatchUrlReindexProcess_Model_Url extends Mage_Catalog_Model_Url
{

    /**
     * Speed up url reindex by ignoring disabled and invisible products
     *
     * @param int $storeId
     *
     * @return $this
     */
    public function refreshProductRewrites ( $storeId )
    {
        $this->_categories = array();
        $storeRootCategoryId = $this->getStores( $storeId )->getRootCategoryId();
        $storeRootCategoryPath = $this->getStores( $storeId )->getRootCategoryPath();
        $this->_categories[$storeRootCategoryId] = $this->getResource()->getCategory( $storeRootCategoryId, $storeId );

        $lastEntityId = 0;
        $process = true;

        $enableOptimisation = Mage::getStoreConfigFlag( 'cyberhouse_patchurlreindexprocess/index/enable' );
        $excludeProductsDisabled = Mage::getStoreConfigFlag( 'cyberhouse_patchurlreindexprocess/index/exclude_disabled_products' );
        $excludeProductsNotVisible = Mage::getStoreConfigFlag( 'cyberhouse_patchurlreindexprocess/index/exclude_invisible_products' );
        $useCategoriesInUrl = Mage::getStoreConfig( 'catalog/seo/product_use_categories' );

        while ($process == true) {
            $products = $this->getResource()->getProductsByStore( $storeId, $lastEntityId );
            if (!$products) {
                $process = false;
                break;
            }

            $this->_rewrites = array();
            $this->_rewrites = $this->getResource()->prepareRewrites( $storeId, false, array_keys( $products ) );

            $loadCategories = array();
            foreach ($products as $product) {
                foreach ($product->getCategoryIds() as $categoryId) {
                    if (!isset( $this->_categories[$categoryId] )) {
                        $loadCategories[$categoryId] = $categoryId;
                    }
                }
            }

            if ($loadCategories) {
                foreach ($this->getResource()->getCategories( $loadCategories, $storeId ) as $category) {
                    $this->_categories[$category->getId()] = $category;
                }
            }


            foreach ($products as $product) {

                // skip disabled products
                if ($enableOptimisation && $excludeProductsDisabled && $product->getData( "status" ) == Mage_Catalog_Model_Product_Status::STATUS_DISABLED) {
                    continue;
                }

                // skip invisible products
                if ($enableOptimisation && $excludeProductsNotVisible && $product->getData( "visibility" ) == Mage_Catalog_Model_Product_Visibility::VISIBILITY_NOT_VISIBLE) {
                    continue;
                }

                // Always Reindex short url
                $this->_refreshProductRewrite( $product, $this->_categories[$storeRootCategoryId] );


                if ($useCategoriesInUrl != "0" || !$enableOptimisation) {
                    foreach ($product->getCategoryIds() as $categoryId) {
                        if ($categoryId != $storeRootCategoryId && isset( $this->_categories[$categoryId] )) {
                            if (strpos( $this->_categories[$categoryId]['path'], $storeRootCategoryPath . '/' ) !== 0) {
                                continue;
                            }
                            $this->_refreshProductRewrite( $product, $this->_categories[$categoryId] );
                        }
                    }
                }

            }

            unset( $products );
            $this->_rewrites = array();
        }

        $this->_categories = array();
        return $this;
    }
}