<?php

class Cyberhouse_PrintProduct_Block_Catalog_Product_View_Link extends Mage_Core_Block_Template {

    protected function getProductId(){
        if ($this->getData('product_id') && is_int($this->getData('product_id'))) {
            return $this->getData('product_id');
        }
        $blockProduct = $this->getLayout()->getBlockSingleton("catalog/product_view")->getProduct();
        if ($blockProduct && $blockProduct->getId()) {
            return $blockProduct->getId();
        }
        return Mage::registry( "current_product" )->getId();
    }

    public function getPrintUrl ()
    {
        return Mage::getUrl( "printproduct/index/print", array( "product" => $this->getProductId() ) );
    }

}