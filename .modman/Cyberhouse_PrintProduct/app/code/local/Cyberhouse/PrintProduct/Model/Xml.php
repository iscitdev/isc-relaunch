<?php

class Cyberhouse_PrintProduct_Model_Xml extends SimpleXMLElement
{

    /**
     * Adds a child with $value inside CDATA
     * @param  $name
     * @param  $value
     * @return SimpleXMLElement
     */
    public function addChildWithCDATA ( $name, $value = NULL )
    {
        $new_child = $this->addChild( $name );

        if ($new_child !== NULL) {
            $node = dom_import_simplexml( $new_child );
            $no = $node->ownerDocument;
            $node->appendChild( $no->createCDATASection( $value ) );
        }

        return $new_child;
    }
}