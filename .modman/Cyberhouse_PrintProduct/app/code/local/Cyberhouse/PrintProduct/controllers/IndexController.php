<?php

class Cyberhouse_PrintProduct_IndexController extends Mage_Core_Controller_Front_Action
{

    /** @var $coreHelper Mage_Core_Helper_Data */
    protected $coreHelper;


    public function _construct(){
        parent::_construct();
        $this->coreHelper = Mage::helper( "core" );
    }

    public function indexAction(){
        $url = Mage::helper( "core/http" )->getHttpReferer();
        $this->_redirectUrl($url);
    }

    public function printAction()
    {
        $productId = $this->getRequest()->getParam( "product" );
        $product = $this->getProduct( $productId );
        if ($product && $product->getId()) {
            try{
                $productXml = $this->getProductXml( $product );
                $pdf = $this->getPdf( $productXml->asXML() );
                $filename = $product->getName()."_".$product->getProductNo()."_".$product->getProductVersion().".pdf";
                $this->_prepareDownloadResponse($filename, $pdf);
            }
            catch(Exception $e){
                Mage::getSingleton("core/session")->addError($this->__($e->getMessage()));
                $this->_forward( "index" );
            }
        }
        else {
            Mage::getSingleton("core/session")->addError($this->__("Coudn't find product with ID: $productId"));
            $this->_forward( "index" );
        }
    }

    protected function getProductXml(Mage_Catalog_Model_Product $product){
        $xml = new Cyberhouse_PrintProduct_Model_Xml('<?xml version="1.0" encoding="UTF-8" standalone="yes" ?><productsheet  xmlns="http://isc-gmbh.info/productsheet"></productsheet>');
        $this->appendHeadlines($xml->addChild('headlines'));
        $this->appendCompanyInfo($xml->addChild("header"));
        $this->appendProductDetails($xml->addChild("article"),$product);
        $this->appendSpareparts($xml,$product);
        $this->appendRelatedProducts($xml, $product );
        return $xml;
    }

    protected function getPdf ( $xmlString )
    {
        $client = new Zend_Http_Client();
        $client->setUri(Mage::getStoreConfig("print_product/general/uri"));
        $client->setConfig(array(
            'maxredirects' => 0,
            'timeout'      => 60));
        /** @var Zend_Http_Response $pdfStream */
        $pdfStream = $client->setRawData($xmlString,"text/xml")->request('POST');
        if($pdfStream->isError()){
            throw new Exception( $pdfStream->getBody() );
        }
        return $pdfStream->getBody();
    }

    protected function appendHeadlines(SimpleXMLElement $xml){
        $helper = Mage::helper("cyberhouse_printproduct");
        $translationHelper = Mage::helper('catalog');
        $xml->addChildWithCDATA( "companyPhone", $translationHelper->__('Telephone')) ;
        $xml->addChildWithCDATA( "companyMail", $helper->__("Customer Service")) ;
        $xml->addChildWithCDATA( "articleNr", $helper->__("Article number") );
        $xml->addChildWithCDATA( "identNr", $helper->__("Identify number") );
        $xml->addChildWithCDATA( "accessory", $helper->__("Accessories") );
        $xml->addChildWithCDATA( "accessoryArticleNr", $helper->__("Article number") );
        $xml->addChildWithCDATA( "accessoryIdentNr", $helper->__("Identify number")  );
        $xml->addChildWithCDATA( "accessoryName", $helper->__("Product Name") );
        $xml->addChildWithCDATA('sparepart',$helper->__("Spare parts"));
        $xml->addChildWithCDATA( "sparepartPos", $translationHelper->__('Pos'));
        $xml->addChildWithCDATA( "sparepartArticleNr", $translationHelper->__('SKU') );
        $xml->addChildWithCDATA( "sparepartIdentNr", $helper->__('Identify number') );
        $xml->addChildWithCDATA( "sparepartName", $helper->__("Product Name") );
    }

    protected function appendCompanyInfo(SimpleXMLElement $xml){
        $info = Mage::getStoreConfig("general/imprint");
        $xml->addChildWithCDATA( "company", $info['company_first'] );
        $xml->addChildWithCDATA( "department", $this->coreHelper->__("Customer Service")) ;
        $xml->addChildWithCDATA( "address", $info['street'] );
        $xml->addChildWithCDATA( "zip", $info['zip'] );
        $xml->addChildWithCDATA( "phone", $info['telephone']) ;
        $xml->addChildWithCDATA( "email", $info['email'] );
        $xml->addChildWithCDATA( "website", $info['web'] );
        $xml->addChildWithCDATA( "footerText", Mage::getStoreConfig("print_product/content/footer")) ;
    }

    protected function appendProductDetails(SimpleXMLElement $xml, Mage_Catalog_Model_Product $product){
        $xml->addChildWithCDATA( "articleNr", $product->getProductNo()) ;
        $xml->addChildWithCDATA( "identNr", $product->getProductIdent() );
        $xml->addChildWithCDATA( "articleShortName", $product->getName() );
        $xml->addChildWithCDATA( "articleLongName", $product->getItemType());
        $xml->addChildWithCDATA( "assetId", $product->getExplosionImage() );
        /* $xml->addChildWithCDATA( "sku", $product->getSku() );
        $xml->addChildWithCDATA( "version", $product->getProductVersion() );
        $xml->addChildWithCDATA( "image", $product->getAssetId() ); */
        return $xml;
    }

    protected function appendSpareparts (SimpleXMLElement $xml, Mage_Catalog_Model_Product $product )
    {
        /** @var Cyberhouse_Layout_Block_Spareparts $sparePartsBlock */
        Mage::register( 'product', $product );
        Mage::register( 'current_product', $product );
        $sparePartsBlock = Mage::getBlockSingleton( "cyberhouse_layout/spareparts" );
        $spareParts = $sparePartsBlock->getSpareparts( $product );
        if(count($spareParts)){
            $xml = $xml->addChild("spareparts");
            foreach ($spareParts as $sparePart) {
                $sparePartXml = $xml->addChild("sparepart" );
                if($sparePart['pos']) {
                    $sparePartXml->addChildWithCDATA( 'pos', $sparePart['pos']) ;
                }
                $number = explode( "_", $sparePart['sku'] );
                $sparePartXml->addChildWithCDATA( 'articleNr', reset($number) );
                $sparePartXml->addChildWithCDATA( 'identNr', $sparePart['ident_no']) ;
                $sparePartXml->addChildWithCDATA( 'name', $sparePart['name']) ;
                //$sparePartXml->addChildWithCDATA( 'price', $sparePart['price'] );
            }
        }
    }

    protected function appendRelatedProducts (SimpleXMLElement $xml, Mage_Catalog_Model_Product $product )
    {
        $relatedProducts = $product->getRelatedProductCollection()->addAttributeToSelect("*");
        if (count( $relatedProducts )) {
            $xml = $xml->addChild("accessories");
            foreach ($relatedProducts as $relatedProduct) {
                $relatedXml = $xml->addChild("accessory" );
                $number = explode( "_", $relatedProduct['sku'] );
                $relatedXml->addChildWithCDATA( "articleNr", reset($number) );
                $relatedXml->addChildWithCDATA( "identNr", $relatedProduct->getProductIdent()) ;
                $relatedXml->addChildWithCDATA( "name", $relatedProduct->getName()) ;
                /*$relatedXml->addChildWithCDATA( "description", $relatedProduct->getDescription() );
                $relatedXml->addChildWithCDATA( "price", Mage::helper("cyberhouse_catalog")->getPrice($relatedProduct) );
                $relatedXml->addChildWithCDATA( "version", $relatedProduct->getProductVersion() );
                $relatedXml->addChildWithCDATA( "name", $relatedProduct->getName() );
                $formattedPrice = Mage::helper("cyberhouse_catalog")->getFormattedPrice($relatedProduct);
                $relatedXml->addChildWithCDATA( "formatted_price", $formattedPrice ); */
            }
        }
    }

    protected function getProduct ( $productid )
    {
        $product = Mage::getModel( "catalog/product" )->load( $productid );
        return $product;
    }

}
