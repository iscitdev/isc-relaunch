<?php

class Cyberhouse_ProductAlert_Block_Ebizmarts_Autoresponder_Backtostock_Notice extends Ebizmarts_Autoresponder_Block_Backtostock_Notice {

    protected $product;

    public function setProduct ( $product )
    {
        if ($product instanceof Mage_Catalog_Model_Product) {
            $this->product = $product;
        }
        return $this;
    }

    public function getProduct(){
        if (!$this->product) {
            return parent::getProduct();
        }
        return $this->product;
    }

}