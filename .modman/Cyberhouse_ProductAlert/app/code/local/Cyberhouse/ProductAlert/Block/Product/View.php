<?php

class Cyberhouse_ProductAlert_Block_Product_View extends Mage_ProductAlert_Block_Product_View
{

    protected $_origTemplate;
    protected $productsAlreadyRegistered;

    /**
     * @param $product
     * @return $this
     */
    public function setProduct ( $product )
    {
        if ($product instanceof Mage_Catalog_Model_Product) {
            $this->_product = $product;
            $this->_getHelper()->setProduct( $this->_product );
            if ($this->_origTemplate) {
                $this->_template = $this->_origTemplate;
            }
        }
        return $this;
    }

    /**
     * save origin template to be able to reuse this block
     * + issalable logic is used different -> removed the condition
     */

    public function prepareStockAlertData ()
    {
        if (!$this->_origTemplate) {
            $this->_origTemplate = $this->_template;
        }
        if (!$this->_getHelper()->isStockAlertAllowed() || !$this->_product) {
            $this->setTemplate( '' );
            return;
        }
        $this->setSignupUrl( $this->_getHelper()->getSaveUrl( 'stock' ) );
    }

    /**
     * save origin template to be able to reuse this block
     */
    public function preparePriceAlertData ()
    {
        if (!$this->_origTemplate) {
            $this->_origTemplate = $this->_template;
        }
        parent::preparePriceAlertData();
    }

    /**
     * Don't display anything if the visibility level does not match
     * @return Mage_Core_Block_Abstract
     */
    public function _beforeToHtml ()
    {
        $customer = Mage::helper( 'customer' )->getCurrentCustomer();
        if (!$customer || !in_array( 10, Mage::helper( "cyberhouse_customergroups" )->getCustomerVisibilityLevel( $customer ) )) {
            $this->setTemplate( "" );
        }
        if(!$customer->getId()){
            $this->setSignupUrl( "#out-of-stock-message" );
        }
        return parent::_beforeToHtml();
    }

    public function getSignupLabel(){
        return Mage::helper( 'cyberhouse_productalert' )->__( 'Out of Stock. Get a notice?' );
    }

    public function getSignupTitle(){
        return Mage::helper( 'cyberhouse_productalert' )->__( 'You will be notified by email when the product is available again.' );
    }

    public function cachePriceProducts($products){
        $this->cacheProducts( $products, "productalert/price_collection" );
    }

    public function cacheStockProducts($products){
        $this->cacheProducts( $products, "productalert/stock_collection" );
    }

    protected function cacheProducts ( $products, $resourceModel )
    {
        $ids = array();
        foreach($products as $product){
            $ids[$product['product_id']] = $product['product_id'];
        }
        $customer = Mage::helper( 'customer' )->getCurrentCustomer();
        $collection = Mage::getResourceModel( $resourceModel )
            ->addFieldToFilter( 'customer_id', $customer->getId() )
            ->addFieldToFilter( 'website_id', Mage::app()->getStore()->getWebsiteId() )
            ->addFieldToFilter( 'send_count', "0" )
            ->addFieldToFilter( 'product_id', array( "in" => $ids ) )
            ->addFieldToSelect( 'product_id' )->getData();
        foreach ($collection as $inCollection) {
            $this->productsAlreadyRegistered[$inCollection['product_id']] = $inCollection['product_id'];
        }
    }

    public function isProductRegistered ( )
    {

        if (!$this->productsAlreadyRegistered || !is_array( $this->productsAlreadyRegistered )) {
            return false;
        }
        return array_key_exists($this->_product->getId(), $this->productsAlreadyRegistered);
    }

}