<?php

class Cyberhouse_Queue_Block_Sales_Order_Grid extends Mage_Adminhtml_Block_Sales_Order_Grid
{
    protected function _prepareMassaction()
    {
        parent::_prepareMassaction();

        // Append new mass action option
        $this->getMassactionBlock()->addItem(
            'cyberhouse_queue',
            array(
                'label' => $this->__('Sync manually to Navision'),
                'url' => $this->getUrl('queue/adminhtml_manual/sync') //this should be the url where there will be mass operation
            )
        );
    }
}