<?php

class Cyberhouse_Queue_Helper_Data extends Mage_Core_Helper_Abstract {

	public function __construct() {
		// include stomp library
	}

	protected function getMessagingSetting($settingName) {
		return Mage::getStoreConfig('isc_messaging_settings/' . $settingName);
	}

	protected function getBasicConfig($queueNameSetting, $queueTypeSetting) {
		$config = array(
			'scheme'		=> 'tcp',
			'host' 			=> $this->getMessagingSetting('queue_host/queue_host'),
			'port' 			=> $this->getMessagingSetting('queue_host/queue_port'),
			'queue' 		=> $this->getMessagingSetting('queue_name/'.$queueNameSetting),
			'type'			=> $this->getMessagingSetting('queue_message_type/'.$queueTypeSetting),
			'readtimeout'	=> 10
		);

		if (array_search("", $config) !== false) {
			Mage::log('Error in messaging config! Please configure all fields!', null, 'messaging.log');
			return false;
		}

		return $config;
	}

	public function sendToQueueAndGetResponse($messageBody, $queueNameSetting, $queueTypeSetting) {

		// create temporary queue name
		$replyToQueue = '/temp-queue/queue_'.rand(10000000, 20000000).'/';

		if (!$config = $this->getBasicConfig($queueNameSetting, $queueTypeSetting)) {
			return false;
		}

		$connection = $this->sendToQueue($messageBody, $queueNameSetting, $queueTypeSetting, $replyToQueue, true);

		$connection->setReadTimeout($config['readtimeout']);
		$connection->subscribe($replyToQueue);

		if ($message = $connection->readFrame()) {
			return $message;
		} else {
			// no response
			return false;
		}
	}

    /**
     * @param $messageBody            string        complete message body (xml)
     * @param $queueNameSetting        string        setting name for queue name to fetch
     * @param $queueTypeSetting        string        setting name for queue type to fetch
     * @param $replyToQueue            bool|string    "reply to" queue name
     * @param bool $returnConnection   bool         whether this function should return the message or the connection
     * @param array $additional_headers array       additional header data that is sent to the queue
     * @return bool|Stomp|string (message)
     */
	public function sendToQueue($messageBody, $queueNameSetting, $queueTypeSetting, $replyToQueue = false, $returnConnection = false, $additional_headers = array()) {

		if (!$config = $this->getBasicConfig($queueNameSetting, $queueTypeSetting)) {
			return false;
		}

		$con = $this->connectToQueue($config['host'], $config['port'], $config['readtimeout']);

		$headers = array(
			'type' 			=> $config['type'],
			'persistent' 	=> 'true',
		);
		if ($replyToQueue) {
			$headers['reply-to'] = $replyToQueue;
		}
        $headers += $additional_headers;
		$messageSent = $con->send($config['queue'], $messageBody, $headers);

		if ($returnConnection) {
			return $con;
		} else {
			$con->disconnect();
			return $messageSent;
		}
	}

	private function connectToQueue($host, $port, $timeout) {
		$con = new Stomp('tcp://'.$host.':'.$port);
		$con->setReadTimeout($timeout);
		$con->connect();

		return $con;
	}

	public function getDataFromQueue($queueNameSetting = 'incoming') {

		// load config
		if (!$config = $this->getBasicConfig($queueNameSetting, 'default')) {
			return false;
		}

		// connect to queue
		$connection = $this->connectToQueue($config['host'], $config['port'], $config['readtimeout']);
		$connection->setReadTimeout($config['readtimeout']);
		$connection->subscribe($config['queue']);
		// read message from queue
        $message = $connection->readFrame();
		if ($message !== false) {
            if( $message->body) {
                $connection->ack($message);
                return $message;
            }
            else {
                Mage::log('Stomp returns that there are still messages in the queue but returns an empty message!!! ',Zend_Log::CRIT);
                return false;
            }
		} else{
			return false;
		}
	}
}