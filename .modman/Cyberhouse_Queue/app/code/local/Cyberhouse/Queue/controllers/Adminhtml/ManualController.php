<?php


class Cyberhouse_Queue_Adminhtml_ManualController extends Mage_Adminhtml_Controller_Action
{
    public function syncAction()
    {
        $orderIds = $this->getRequest()->getParam('order_ids');

        $orders = Mage::getModel('sales/order')->getCollection()
            ->addAttributeToFilter('entity_id', array('in' => $orderIds));
        foreach($orders as $order) {
            $order->setState('pending_sync', 'Sync in progress', 'Sync in progress')->save();

            $orderXml = Mage::helper('cyberhouse_checkout/data')->generateOrderXml($order);
            $sent = Mage::helper('cyberhouse_queue/data')->sendToQueue($orderXml, Cyberhouse_Checkout_Model_Sync::QUEUE_SETTING_NAME, Cyberhouse_Checkout_Model_Sync::QUEUE_TYPE_SETTING);
            if($sent) {
                $order->setState('synced', 'Synced successfully', 'Synced successfully')->save();
            }

        }
        $this->_redirect('adminhtml/sales_order/index');
        return $this;

    }
}