<?php

class Cyberhouse_ScannerInput_Block_Checkout_Cart_Head extends Mage_Core_Block_Template
{

    public function getScannerInputFormPostUrl()
    {
        $isSecure = Mage::app()->getStore()->isCurrentlySecure();
        if ($isSecure) {
            return Mage::getUrl( 'scannerinput/upload', array( '_secure' => true ) );
        } else {
            return Mage::getUrl( 'scannerinput/upload' );
        }
    }
}