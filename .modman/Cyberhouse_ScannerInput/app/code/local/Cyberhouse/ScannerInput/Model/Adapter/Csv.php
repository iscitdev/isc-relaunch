<?php

class Cyberhouse_ScannerInput_Model_Adapter_Csv {
    private $csv_reader = null;
    private $filename = null;

    /**
     * @return Mage_ImportExport_Model_Import_Adapter_Csv
     * @throws Exception
     */
    protected function getCsvReader(){
        if(!$this->csv_reader){
           throw new Exception("No file specified");
        }
        return $this->csv_reader;
    }

    public function getFilename(){
        return $this->filename;
    }

    public function init($filename){
        if(!$filename){
            throw new Exception("No filename specified");
        }
        $this->filename = $filename;
        $this->csv_reader = $this->getReaderInstance($this->filename);
        return $this;
    }

    protected function getReaderInstance($filename){
        return new Mage_ImportExport_Model_Import_Adapter_Csv ($filename);
    }

    public function getData(){
        $data = array();
        while($this->getCsvReader()->current()){
            $data [] = $this->getCsvReader()->current();
            $this->getCsvReader()->next();
        }
        return $data;
    }
}