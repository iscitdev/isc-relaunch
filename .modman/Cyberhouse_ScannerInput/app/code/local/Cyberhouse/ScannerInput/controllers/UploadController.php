<?php

class Cyberhouse_ScannerInput_UploadController extends Mage_Core_Controller_Front_Action
{

    const BASE_OFFSET = 21;
    const SKU_LENGTH = 18;

    private function translate( $input )
    {
        return Mage::helper( 'cyberhouse_scannerinput' )->__( $input );
    }

    public function indexAction()
    {
        if (isset( $_FILES['file']['name'] )) {
            if (isset( $_FILES['file']['type'] ) && in_array($_FILES['file']['type'],array("text/plain","text/csv"))) {

                if ($_FILES['file']['size'] > 0) {
                    $csvAdapter = Mage::getModel("cyberhouse_scannerinput/adapter_csv")->init($_FILES['file']['tmp_name']);

                    $csvData = $csvAdapter->getData();
                    $imporData = array();
                    foreach ($csvData as $key => $entry) {
                        $product = $this->getProduct($entry['sku']);
                        if(!$product){
                            Mage::log( "IMPORT: couldn't find product with sku: ". $entry['sku']);
                            Mage::getSingleton( 'checkout/session' )->addError( Mage::helper( 'cyberhouse_scannerinput' )->__( "No article found with SKU %s", $entry['sku'] ) );
                        }
                        else {
                            $imporData[$key] = array("product" => $product->getId(),"qty"=>$entry['qty']);
                        }
                    }

                    if (count( $imporData ) > 0) {
                        $cart = Mage::helper( 'checkout/cart' )->getCart();
                        foreach ($imporData as $importProductData) {
                            $this->getRequest()->setParams($importProductData);
                            $this->getRequest()->setDispatched( false );
                            Mage::dispatchEvent("controller_action_predispatch_checkout_cart_add",array("controller_action" => $this) );
                            if(!$this->getRequest()->isDispatched()){
                                $cart->addProduct( $importProductData['product'] ,array('qty' => $this->getRequest()->getParam('qty',1) ) );
                            }
                        }

                        $cart->save();
                        Mage::getSingleton( 'checkout/session' )->setCartWasUpdated( true );
                    }
                    $this->getRequest()->setDispatched( true );

                } else {
                    Mage::getSingleton( 'checkout/session' )->addError( $this->translate( 'Can not read an empty file' ) );
                }
            } else {
                Mage::getSingleton( 'checkout/session' )->addError( $this->translate( 'Only a textfile is accepted' ) );
            }

        } else {
            Mage::getSingleton( 'checkout/session' )->addError( $this->translate( 'Please select a file to upload' ) );
        }

        $isSecure = Mage::app()->getStore()->isCurrentlySecure();
        if ($isSecure) {
            $this->getResponse()->setRedirect( Mage::getUrl( 'checkout/cart', array( '_secure' => true ) ) );
        } else {
            $this->getResponse()->setRedirect( Mage::getUrl( 'checkout/cart' ) );
        }

    }

    protected function getProduct ( $productSku )
    {
        $collection = Mage::helper("cyberhouse_catalog")->getVersionCollectionByProductNumber($productSku, "");
        if ($collection) {
            return $collection->getFirstItem();
        }
        return $collection;
    }
}
