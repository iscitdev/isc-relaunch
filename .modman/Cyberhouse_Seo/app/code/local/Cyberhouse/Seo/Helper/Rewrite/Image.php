<?php

class Cyberhouse_Seo_Helper_Rewrite_Image extends Mirasvit_Seo_Helper_Rewrite_Image
{
	public function getConfig()
	{
		return Mage::getSingleton('seo/config');
	}

    public function init(Mage_Catalog_Model_Product $product, $attributeName, $imageFile=null, $useImageFriendlyUrls=true)
    {
        $config = $this->getConfig();

        $urlKey = '';
        $this->_reset();
        $this->_setModel(Mage::getModel('seo/rewrite_product_image'));
        $this->_getModel()->setDestinationSubdir($attributeName);
//        $this->_getModel()->setUrlKey($urlKey);
        $this->setProduct($product);

        $this->setWatermark(Mage::getStoreConfig("design/watermark/{$this->_getModel()->getDestinationSubdir()}_image"));
        $this->setWatermarkImageOpacity(Mage::getStoreConfig("design/watermark/{$this->_getModel()->getDestinationSubdir()}_imageOpacity"));
        $this->setWatermarkPosition(Mage::getStoreConfig("design/watermark/{$this->_getModel()->getDestinationSubdir()}_position"));
        $this->setWatermarkSize(Mage::getStoreConfig("design/watermark/{$this->_getModel()->getDestinationSubdir()}_size"));


        $this->setImageAlt($attributeName);
        return $this;
    }


    public function generateAlt()
    {
        if ($template = $this->getConfig()->getImageAltTemplate()) {
            $alt = Mage::helper('mstcore/parsevariables')->parse(
                $template,
                array('product' => $this->getProduct())
            );
        } else {
            $product = $this->getProduct();
            $alt = $product->getName();
        }
        $alt = trim($alt);
        return $alt;
    }

    protected function setImageAlt($attributeName) {
        if (!$this->getConfig()->getIsEnableImageAlt()) {
            return;
        }
        $alt = $this->generateAlt();
        $product = $this->getProduct();
        $key = $attributeName . '_label';
        if (!$product->getData($key)) {
            $product->setData($attributeName . '_label', $alt);
            if ($gallery = $product->getMediaGalleryImages()) {
                $alt = $this->generateAlt();
                foreach ($gallery as $image) {
                    if (! $image->getLabel()) {
                        $image->setLabel($alt);
                    }
                }
            }
        }
    }

}