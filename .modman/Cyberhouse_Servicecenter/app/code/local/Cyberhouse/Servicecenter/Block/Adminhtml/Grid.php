<?php

/**
 * Created by PhpStorm.
 * User: rhutterer
 * Date: 25.07.14
 * Time: 11:25
 */
class Cyberhouse_Servicecenter_Block_Adminhtml_Grid extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_blockGroup = 'cyberhouse_servicecenter';
        $this->_controller = 'adminhtml_servicecenter';
        $this->_headerText = Mage::helper('core')->__('Servicecenter');
        parent::__construct();
        $this->_removeButton('add');
    }
}