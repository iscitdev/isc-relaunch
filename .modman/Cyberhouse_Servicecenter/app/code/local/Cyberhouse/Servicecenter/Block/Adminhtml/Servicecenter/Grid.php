<?php

/**
 * Created by PhpStorm.
 * User: rhutterer
 * Date: 25.07.14
 * Time: 11:33
 */
class Cyberhouse_Servicecenter_Block_Adminhtml_Servicecenter_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('id');
        $this->setDefaultSort('id');
        $this->setDefaultDir('ASC');
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('servicecenter/retoure')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    public function _afterLoadCollection()
    {
        foreach($this->getCollection() as $item) {
            $item->setCustomerId(Mage::getModel('customer/customer')->load($item->getCustomerId())->getName());
        }
        return $this;
    }


    protected function _prepareColumns()
    {
        $this->addColumn('id', array(
            'header' => Mage::helper('core')->__('ID'),
            'align' => 'right',
            'width' => '50px',
            'index' => 'id',
        ));

        $this->addColumn('created', array(
            'header' => Mage::helper('core')->__('created at'),
            'align' => 'left',
            'index' => 'created'
        ));

        $this->addColumn('title', array(
            'header' => Mage::helper('core')->__('Title'),
            'align' => 'left',
            'index' => 'customer_id',
            'filter' => false,
            'sortable' => false
        ));


        $this->addColumn('return_number', array(
            'header' => Mage::helper('core')->__('Retouren Nummer'),
            'align' => 'left',
            'index' => 'return_number'
        ));

        $this->addColumn('action', array(
            'header' => Mage::helper('core')->__('Action'),
            'width' => '50px',
            'type' => 'action',
            'getter' => 'getId',
            'actions' => array(
                array(
                    'caption' => Mage::helper('core')->__('save PDF'),
                    'url' => array(
                        'base' => 'servicecenter_backend/adminhtml_index/pdf'
                    ),
                    'field' => 'id'
                )
            ),
            'filter' => false,
            'sortable' => false,
            'index' => 'stores',
            'is_system' => true,
        ));


        return parent::_prepareColumns();
    }


    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/download', array('id' => $row->getId()));
    }

}