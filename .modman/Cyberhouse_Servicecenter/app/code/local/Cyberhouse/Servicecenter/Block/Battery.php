<?php

class Cyberhouse_Servicecenter_Block_Battery extends Cyberhouse_Servicecenter_Block_Success {

    protected $product;
    public function getProduct(){
        if (!$this->product) {
            $this->product = Mage::helper("core")->jsonDecode(Mage::helper("servicecenter")->getRetoure()->getArticle());
        }
        return $this->product;
    }

    public function getActionURL() {
        return Mage::getStoreConfig( Cyberhouse_ServiceCenter_Model_Constants::PDF );
    }

}