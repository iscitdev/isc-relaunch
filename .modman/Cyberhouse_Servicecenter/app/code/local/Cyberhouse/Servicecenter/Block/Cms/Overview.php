<?php

class Cyberhouse_Servicecenter_Block_Cms_Overview extends Mage_Core_Block_Template
{

    protected function getMenu ()
    {
        return Mage::app()->getLayout()->getBlock( "czcmsplus.menutop" )->getMenu();
    }

    public function getMenuItems ()
    {
        $pagesArray = array();
        $pages = $this->getMenu()->getPages();
        foreach ($pages as $page) {
            $pagesArray [] =
                array(
                    "description" => $page->getDescription(),
                    "name" => $page->getName(),
                    "url" => $page->getUrl(),
                    "icon" => $page->getPageCssClass(),
                    "cssId" => $page->getPageCssId()
                );
        }
        return $pagesArray;
    }

}