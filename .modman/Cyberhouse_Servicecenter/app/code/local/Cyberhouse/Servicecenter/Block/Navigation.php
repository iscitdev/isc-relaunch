<?php

class Cyberhouse_Servicecenter_Block_Navigation extends Mage_Core_Block_Template
{

    protected $steps;

    const BEHAVIOUR_REPLACE = "replace";
    const BEHAVIOUR_INSERT = "insert";

    public function __construct ()
    {
        $this->addStep( 1, "Identify article" );
        $this->addStep( 2, "Failure / damage type" );
        $this->addStep( 3, "Address" );
        $this->addStep( 4, "Done" );
    }

    public function addStep ( $key, $value, $behaviour = self::BEHAVIOUR_REPLACE )
    {
        if ($behaviour == self::BEHAVIOUR_REPLACE) {
            $this->steps[$key] = $value;
        }
        if ($behaviour == self::BEHAVIOUR_INSERT) {
            array_splice( $this->steps, $key, 0, array( $value ) );
        }
    }

    public function removeStep ( $key )
    {
        unset( $this->steps[$key] );
    }

    public function getSteps(){
        return $this->steps;
    }
}