<?php

class Cyberhouse_Servicecenter_Block_Reason extends Mage_Core_Block_Template {

    protected $product;
    public function getProduct(){
        if (!$this->product) {
            $this->product = Mage::helper("core")->jsonDecode(Mage::helper("servicecenter")->getRetoure()->getArticle());
        }
        return $this->product;
    }

}