<?php

/**
 * Created by JetBrains PhpStorm.
 * User: avolk
 * Date: 23.10.13
 * Time: 16:31
 * To change this template use File | Settings | File Templates.
 */
class Cyberhouse_Servicecenter_Block_Servicecenter extends Mage_Core_Block_Template
{
    protected $pickupCollection;
    protected $pickupCodes;

    public function __construct ()
    {
        parent::__construct();
        $this->prepareCollections();
    }

    protected function _prepareLayout ()
    {
        parent::_prepareLayout();
        $pageSizes = array( 10 => 10, 20 => 20, 'all' => 'all' );

        $pager = $this->getLayout()->createBlock( 'page/html_pager', 'servicecenter.account.pager_online' );
        $pager->setAvailableLimit( $pageSizes );
        $pager->setCollection( $this->getCollection() );
        $this->setChild( 'pager_online', $pager );

        $pager = $this->getLayout()->createBlock( 'page/html_pager', 'servicecenter.account.pager_pickup' );
        $pager->setAvailableLimit($pageSizes);
        $pager->setPageVarName( "pickup_page" );
        $pager->setLimitVarName( "pickup_limit" );
        $pager->setCollection( $this->pickupCollection );
        $this->setChild( 'pager_pickup', $pager );

        return $this;
    }

    /**
     * Get Customer Model
     *
     * @return Mage_Customer_Model_Session
     */
    public function getCustomer ()
    {
        return Mage::getSingleton( 'customer/session' )->getCustomer();
    }

    protected function prepareCollections(){
        $collection = Mage::getResourceModel( 'servicecenter/retoure_collection' );
        $collection->addFieldToFilter( 'customer_id', $this->getCustomer()->getId() );
        $collection->addFieldToFilter( 'status_code', array( "nin" => $this->getPickupCodes() ) );
        $collection->setOrder( 'created', 'DESC' );
        $pickupCollection = Mage::getResourceModel( 'servicecenter/retoure_collection' )
            ->addFieldToFilter( 'customer_id', $this->getCustomer()->getId() )
            ->addFieldToFilter( 'status_code', array( "in" => $this->getPickupCodes() ) )
            ->setOrder( 'created', 'DESC' );
        $this->setCollection( $collection );
        $this->pickupCollection = $pickupCollection;
    }

    protected function getPickupCodes(){
        if (!$this->pickupCodes) {
            $config = Mage::getStoreConfig("cyberhouse_servicecenter/pickup_codes");
            $codes = explode(",",$config);
            $this->pickupCodes = $codes;
        }
        return $this->pickupCodes;
    }

    public function getPagerHtml ($type = "online")
    {
        return $this->getChildHtml( 'pager_'.$type );
    }

    public function getHeaderBlock ()
    {
        if (Mage::getStoreConfig( "cyberhouse_servicecenter/general/header_identifier" )) {
            return $this->getLayout()->createBlock( "cms/block" )->setBlockId( Mage::getStoreConfig( "cyberhouse_servicecenter/general/header_identifier" ) )->toHtml();
        }
        return "";
    }

    public function getStatusCode(){
        return Mage::helper( 'servicecenter' )->getStatusCode();
    }


    public function setLimit ( $limit )
    {
        $this->getCollection()->setPageSize( $limit );
        $this->pickupCollection->setPageSize( $limit );
    }

    public function getPickupCollection(){
        return $this->pickupCollection;
    }

}