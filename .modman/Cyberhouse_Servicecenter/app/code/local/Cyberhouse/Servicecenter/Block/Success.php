<?php

class Cyberhouse_Servicecenter_Block_Success extends Mage_Core_Block_Template {

    protected $currentReturn;

    public function getCurrentRetoure(){
        if (!$this->currentReturn) {
            $params = Mage::registry( "current_step_params" );
            $id = $params['id'];
            $this->currentReturn = Mage::getModel( "servicecenter/retoure" )->setCustomerId($this->getCustomer()->getId())->load( $id );
        }
        return $this->currentReturn;
    }

    public function getAccountRetoure(Cyberhouse_Servicecenter_Model_Retoure $retoure = null){
        if (!$retoure) {
            $retoure = $this->getCurrentRetoure();
        }
        $helper = Mage::helper('servicecenter/AccountPage');
        return $helper->getAccountRetoure($retoure);
    }

    public function getRetoureUrl(Cyberhouse_Servicecenter_Model_Retoure $retoure = null){
        if (!$retoure) {
            $retoure = $this->getCurrentRetoure();
        }
        $downloadUrl = "";
        if ($retoure) {
            $downloadUrl = Mage::getUrl( 'servicecenter/accountpage/download/', array( 'id' => $retoure->getId() ) );
        }
        return $downloadUrl;
    }

    /**
     * Get Customer Model
     *
     * @return Mage_Customer_Model_Session
     */
    public function getCustomer ()
    {
        return Mage::getSingleton( 'customer/session' )->getCustomer();
    }

}