<?php
/**
 * Created by JetBrains PhpStorm.
 * User: avolk
 * Date: 10.12.13
 * Time: 18:31
 * To change this template use File | Settings | File Templates.
 */

class Cyberhouse_Servicecenter_Helper_AccountPage extends Mage_Core_Helper_Abstract {

	/**
	 * @param $retoure Cyberhouse_Servicecenter_Model_Retoure
	 * @return Cyberhouse_Servicecenter_Model_AccountRetoure
	 */
	public function getAccountRetoure($retoure) {

		$accountReturn = new Cyberhouse_Servicecenter_Model_AccountRetoure();
        if ($retoure) {
            $address = Mage::helper('core')->jsonDecode($retoure->getData('address'));
            $accountReturn->setAddress($address);
            $dhlData = Mage::helper('core')->jsonDecode($retoure->getData('dhl_data'));
            $productData = Mage::helper('core')->jsonDecode($retoure->getData('article'));
            $accountReturn->setProductInfo($this->getProductInfo($productData));
            if($dhlData && array_key_exists('issueDate', $dhlData)) {
                $accountReturn->setDate($this->getDate($dhlData['issueDate']));
            }
			else {
				$accountReturn->setDate($this->getDate($retoure->getCreated()));
			}
            $accountReturn->setPrintLabelId($retoure->getData('id'));
        }
		return $accountReturn;

	}

	private function getProductInfo($productData) {
		if($productData) {
			return array(
				"product_no" => $productData['item_no'],
				"serial_no" => $productData['serial_no'],
				"name" => $productData['description'],
				"description" => $productData['description2']
			);
		} else {
			return array();
		}

	}

	private function getDate($date) {
		$dateTime = new DateTime($date);
		return $dateTime->format('d.m.Y');
	}
}