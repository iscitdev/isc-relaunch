<?php
/**
 * Created by JetBrains PhpStorm.
 * User: avolk
 * Date: 05.12.13
 * Time: 17:43
 * To change this template use File | Settings | File Templates.
 */

class Cyberhouse_Servicecenter_Helper_DHL extends Mage_Core_Helper_Abstract {

	/**
	 * Model for DHLRetour which holds information about the soap call
	 *
	 * @var $dhlRetoure Cyberhouse_Servicecenter_Model_DHLRetoure
	 */
	private $dhlRetoure;

	function __construct() {
		$this->dhlRetoure = Mage::getModel('servicecenter/DHLRetoure');
	}

	/**
	 * Creates Model for DHLRetour, which holds all information needed for
	 * calling the soap interface and creating the label
	 *
	 * @param $retoure Cyberhouse_Servicecenter_Model_Retoure
	 * @return Cyberhouse_Servicecenter_Model_DHLRetoure
	 */
	public function createDHLRetoure($retoure) {

		$address = Mage::helper('core')->jsonDecode($retoure->getData('address'));
		$this->dhlRetoure->setSenderAddress($this->mapAddress($address));
		$this->dhlRetoure->setDeliveryName($this->getDeliveryName($address['country_id']));
		$this->dhlRetoure->setCustomerReference($retoure->getData('return_number'));
		return $this->dhlRetoure;
	}

	/**
	 * Maps the data array from an Mage_Customer_Model_Address to an associative array
	 * where the keys are used for the attributes of the xml
	 *
	 * @param $address array
	 * @return array
	 */
	private function mapAddress($address) {
		$dhlAddress = array(

			"senderName1" => $address['firstname'] . " " . $address['lastname'],
			"senderCareOfName" => "",
			"senderContactPhone"=> $address['telephone'],
			"senderStreet" => $address['street'],
			"senderStreetNumber" => $address['housenumber'],
			"senderBoxNumber" => "",
			"senderPostalCode" => $address['postcode'],
			"senderCity" => $address['city']

		);

		return $dhlAddress;
	}

	/**
	 * Returns the delivery name for the given country code
	 *
	 * @param $countryCode
	 * @return string the delivery name for the country
	 */
	private function getDeliveryName($countryCode) {

		switch($countryCode) {
			case "DE":
				return Cyberhouse_Servicecenter_Model_DHLRetoure::DELIVERY_NAME_DE;
			case "AT":
				return Cyberhouse_Servicecenter_Model_DHLRetoure::DELIVERY_NAME_AT;
			default:
				return Cyberhouse_Servicecenter_Model_DHLRetoure::DELIVERY_NAME_DE;
		}

	}

}