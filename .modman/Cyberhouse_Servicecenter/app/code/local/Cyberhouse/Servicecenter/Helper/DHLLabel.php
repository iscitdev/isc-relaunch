<?php

/**
 * Created by JetBrains PhpStorm.
 * User: avolk
 * Date: 09.12.13
 * Time: 17:12
 * To change this template use File | Settings | File Templates.
 */
class Cyberhouse_Servicecenter_Helper_DHLLabel extends Mage_Core_Helper_Abstract
{

    /**
     * @var Cyberhouse_Servicecenter_Model_DHLRetoure
     */
    private $dhlRetoure;


    /**
     * Calls the dhl interface and returns the response as xml string
     *
     * @param $retoure Cyberhouse_Servicecenter_Model_Retoure
     * @return string
     */
    public function createLabel ( $retoure )
    {
        /** @var $helper Cyberhouse_Servicecenter_Helper_DHL */
        $helper = Mage::helper( 'servicecenter/DHL' );
        $this->dhlRetoure = $helper->createDHLRetoure( $retoure );
        $client = new SoapClient( Mage::getStoreConfig( Cyberhouse_ServiceCenter_Model_Constants::WSDL ), array( "trace" => true ) );
        $client->__setSoapHeaders( new SoapHeader( Mage::getStoreConfig( Cyberhouse_ServiceCenter_Model_Constants::SECURITY_NAMESPACE ), 'Security', $this->getSoapHeader(), true ) );
        $client->__setLocation( Mage::getStoreConfig( Cyberhouse_ServiceCenter_Model_Constants::SOAP_CONNECTOR ) );
        try {
            $client->__soapCall( 'BookLabel', array( $this->getSoapBody() ) );
        } catch (Exception $e) {
            Mage::getSingleton( "core/session" )->addError( $e->getMessage() );
            Mage::log( 'Error calling dhl interface ' . $e->getMessage(), null, 'soap_dhl_label.log' );
            return false;
        }

        return $client->__getLastResponse();
    }

    /**
     * Creates a wsse soap header
     *
     * @return SoapVar the header
     */
    private function getSoapHeader ()
    {

        //Information about creating a wsse header for soap:
        //http://www.php.net/manual/en/soapclient.soapclient.php#97273

        $authInformation = $this->dhlRetoure->getAuthInformation();
        $securityNamespace = Mage::getStoreConfig( Cyberhouse_ServiceCenter_Model_Constants::SECURITY_NAMESPACE );
        //Create soap variables for username and password
        $objSoapVarUser = new SoapVar( $authInformation['username'], XSD_STRING,
            NULL, $securityNamespace, NULL,
            $securityNamespace );
        $objSoapVarPass = new SoapVar( $authInformation['password'], XSD_STRING,
            NULL, $securityNamespace, NULL,
            $securityNamespace );

        //Create auth node with soap variables
        $objWSSEAuth = new stdClass();
        $objWSSEAuth->Username = $objSoapVarUser;
        $objWSSEAuth->Password = $objSoapVarPass;

        //Create SoapVar with auth information nodes
        $objSoapVarWSSEAuth = new SoapVar( $objWSSEAuth, SOAP_ENC_OBJECT, NULL,
            $securityNamespace, 'UsernameToken',
            $securityNamespace );

        //Create token node and add auth nodes
        $objWSSEToken = new stdClass();
        $objWSSEToken->UsernameToken = $objSoapVarWSSEAuth;
        $objSoapVarWSSEToken = new SoapVar( $objWSSEToken, SOAP_ENC_OBJECT, NULL,
            $securityNamespace, 'UsernameToken',
            $securityNamespace );

        //Create security node
        $objSoapVarHeaderVal = new SoapVar( $objSoapVarWSSEToken, SOAP_ENC_OBJECT, NULL,
            $securityNamespace, 'Security',
            $securityNamespace );

        return $objSoapVarHeaderVal;
    }

    /**
     * Creates an array with all parameters for the soap call to the dhl label creator
     *
     * @return array
     */
    private function getSoapBody ()
    {
        $soapBody = array(

            "portalId" => $this->dhlRetoure->getPortalId(),
            "deliveryName" => $this->dhlRetoure->getDeliveryName(),
            "shipmentReference" => $this->dhlRetoure->getShipmentReference(),
            "customerReference" => $this->dhlRetoure->getCustomerReference(),
            "labelFormat" => $this->dhlRetoure->getLabelFormat()

        );

        $soapBody = array_merge( $soapBody, $this->dhlRetoure->getSenderAddress() );

        return $soapBody;
    }

}