<?php

class Cyberhouse_Servicecenter_Helper_Data extends Mage_Core_Helper_Abstract
{

    /**
     * @var $retoure Cyberhouse_Servicecenter_Model_Retoure
     */
    private $retoure;

    const CURRENT_STEP_IDENTIFIER = "current_step";
    const STATUS_OK = 1;
    const STATUS_TO_HEAVY = 2;
    const STATUS_FORBIDDEN = 3;
    const STATUS_ERROR = 4;
    const SKIP_CONFIG = "cyberhouse_servicecenter/skip_dhl_label/status";

    protected $skipConfig;

    function __construct ()
    {
        $this->loadRetoureFromSession();
    }

    /**
     * Returns an array which associates the right urls for the respond codes
     * from the eData api
     *
     * @param bool|\determines $asJson determines if array is needed as json
     *          default == false
     * @return array
     */
    public function getRedirectUrls ( $asJson = false )
    {

        $urls = array(
            2 => Mage::getUrl( 'servicecenter/index/collect' ),
            3 => Mage::getUrl( 'servicecenter/index/fuel' ),
            4 => Mage::getUrl( 'servicecenter/index/error' )
        );

        return ( $asJson ) ? Mage::helper( 'core' )->jsonEncode( $urls ) : $urls;
    }

    private function loadRetoureFromSession ()
    {
        $this->retoure = Mage::getSingleton( 'customer/session' )->getData( Cyberhouse_Servicecenter_Model_Constants::RETURN_CACHE_IDENTIFIER );
    }

    //Methods for interacting with retoure model

    /**
     * Returns the retoure model with all information to persist
     *
     * @return Cyberhouse_Servicecenter_Model_Retoure
     */
    public function getRetoure ()
    {
        return $this->retoure;
    }

    /**
     * Adds all information given from the edata interface to the database model retoure
     * Param $article as associative array
     *
     * @param $article array
     */
    public function addArticleData ( $article )
    {
        //Create new retoure object
        $retoure = Mage::getModel( 'servicecenter/retoure' );
        $retoure->setCustomerId( Mage::getSingleton( 'customer/session' )->getCustomerId() );
        //Serialize array to json for saving in database
        if (is_array( $article )) {
            $json = Mage::helper( 'core' )->jsonEncode( $article );
        }
        //Set article data
        $retoure->setArticle( $json );
        $retoure->setStatusCode( $article['status_no'] );
        //Add id to session for getting the right object
        Mage::getSingleton( 'customer/session' )->setData( Cyberhouse_ServiceCenter_Model_Constants::RETURN_CACHE_IDENTIFIER, $retoure );
    }

    /**
     * Adds the chosen address to the retoure model
     *
     * @param $address Mage_Customer_Model_Address
     */
    public function addAddress ( $address )
    {
        $this->retoure->setData( "address_id", $address->getId() );
        $this->retoure->setData( "address", Mage::helper( 'core' )->jsonEncode( $address ) );
        $this->mapCodeToCountry( $address );
        Mage::getSingleton( 'customer/session' )->setData( Cyberhouse_Servicecenter_Model_Constants::RETURN_CACHE_IDENTIFIER, $this->retoure );
    }

    public function addFailureDescription ( $failureDescription )
    {
        $this->retoure->setData( "failure_description", Mage::helper( 'core' )->jsonEncode( $failureDescription ) );
        Mage::getSingleton( 'customer/session' )->setData( Cyberhouse_Servicecenter_Model_Constants::RETURN_CACHE_IDENTIFIER, $this->retoure );
    }

    protected function mapCodeToCountry ( $address )
    {
        $currentStatus = $this->retoure->getStatusCode();
        $countryCode = $address['country_id'];
        $mapConfig = Mage::getStoreConfig( "cyberhouse_servicecenter/status_country_convert/status_" . $currentStatus );
        if ($mapConfig && array_key_exists( $countryCode, $mapConfig )) {
            $newStatus = $mapConfig[$countryCode];
            $articleData = Mage::helper( "core" )->jsonDecode( $this->retoure->getArticle() );
            if (is_array( $articleData )) {
                $articleData['status_no'] = $newStatus;
                $json = Mage::helper( 'core' )->jsonEncode( $articleData );
                //Set article data
                $this->retoure->setArticle( $json );
            }
            $this->retoure->setStatusCode( $newStatus );
        }
    }

    /**
     * Adds the response from the dhl interface to the retoure model for persisting it
     *
     * @param $data
     */
    public function addDHLData ( $data )
    {
        $doc = new DOMDocument();
        $doc->loadXML( $data );
        $this->retoure->setData( "dhl_label", $doc->getElementsByTagName( "label" )->item( 0 )->nodeValue );
        $response = $doc->getElementsByTagName( 'BookLabelResponse' )->item( 0 );
        $responseData = array();
        if ($response->hasAttributes()) {
            foreach ($response->attributes as $attribute) {
                $responseData[$attribute->nodeName] = $attribute->nodeValue;
            }
        }
        $this->retoure->setData( 'dhl_data', Mage::helper( 'core' )->jsonEncode( $responseData ) );
    }

    public function addReturnNumber ()
    {
        $this->persistRetoure();
        $returnNumber = "RW" . str_pad( $this->retoure->getId(), 8, "0", STR_PAD_LEFT );
        $this->retoure->setData( 'return_number', $returnNumber );
        Mage::getSingleton( 'customer/session' )->setData( Cyberhouse_Servicecenter_Model_Constants::RETURN_CACHE_IDENTIFIER, $this->retoure );
    }

    /**
     * Gets the product id from the retoure model saved in the session
     *
     * @return bool
     */
    public function getItemId ()
    {
        $article = Mage::helper( 'core' )->jsonDecode( $this->retoure->getData( 'article' ) );
        return ( array_key_exists( 'edata_id', $article ) ) ? $article['edata_id'] : false;
    }

    /**
     * Returns the current locale of the shop
     * Fallback en
     *
     * @return string
     */
    public function getLocale ()
    {
        //Get current store locale for getting the symptoms in the right language
        if (Mage::app()->getLocale()) {
            $locale = substr( Mage::app()->getLocale()->getLocaleCode(), 0, 2 );
        }
        //Set fallback if locale could not be parsed
        if (!isset( $locale )) {
            $locale = "en";
        }

        return strtoupper( $locale );
    }

    /**
     * Saves the field to database
     */
    public function persistRetoure ()
    {
        $this->retoure->save();
    }

    public function statusMapper ( $status )
    {
        if ((int)( $status / 100 ) == 2) { // return code 2xx
            return self::STATUS_OK;
        }
        switch ($status) {
            case 200:
                //Product can be returned online
                return self::STATUS_OK;
            case 406:
                //Product is to heavy
                return self::STATUS_TO_HEAVY;
            case 403:
                //Product is forbidden -> go to fuel
                return self::STATUS_FORBIDDEN;
            case 400:
                return self::STATUS_ERROR;
            case 404:
                //An error has occurred
                return self::STATUS_ERROR;
        }
    }

    public function addStatusCode ( $status )
    {
        $this->loadRetoureFromSession();
        $this->retoure->setData( "status_code", $status );
        Mage::getSingleton( 'customer/session' )->setData( Cyberhouse_Servicecenter_Model_Constants::RETURN_CACHE_IDENTIFIER, $this->retoure );
    }

    public function setCurrentStep ( $step )
    {
        Mage::getSingleton( 'customer/session' )->setData( self::CURRENT_STEP_IDENTIFIER, $step );
    }

    public function nextStep ()
    {
        Mage::getSingleton( 'customer/session' )->setData( self::CURRENT_STEP_IDENTIFIER, $this->getCurrentStep() + 1 );
    }

    public function getStatusCode ()
    {
        return Mage::getSingleton( 'customer/session' )->getData( Cyberhouse_Servicecenter_Model_Constants::RETURN_CACHE_IDENTIFIER )->getData( "status_code" );
    }

    public function getCurrentStep ()
    {
        return Mage::getSingleton( 'customer/session' )->getData( self::CURRENT_STEP_IDENTIFIER );
    }

    public function getErrorResponse ( $messages )
    {
        $url = $this->getRedirectUrls( false );
        return array(
            "status" => 4,
            "messages" => $messages,
            "url" => $url[4]
        );
    }

    public function skipReturnLabel( $statusCode = "" ){
        if (!$this->skipConfig) {
            $skipConfig = Mage::getStoreConfig( self::SKIP_CONFIG );
            if($skipConfig){
                $skipConfig = explode( ",", $skipConfig );
            }
            $this->skipConfig = $skipConfig;
        }

        if (!$statusCode) {
            $statusCode = $this->retoure->getStatusCode();
        }
        return in_array($statusCode, $this->skipConfig);
    }
}
