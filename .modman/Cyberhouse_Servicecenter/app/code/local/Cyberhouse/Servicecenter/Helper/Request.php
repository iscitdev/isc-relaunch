<?php
/**
 * Created by JetBrains PhpStorm.
 * User: avolk
 * Date: 12.12.13
 * Time: 11:00
 * To change this template use File | Settings | File Templates.
 */

class Cyberhouse_Servicecenter_Helper_Request extends Mage_Core_Helper_Abstract {

	/**
	 * @param $url string
	 * @param $parameter array
	 * @return bool|Zend_Http_Response
	 */
	public function sendGetRequest($url, $parameter) {
		try {
			Mage::log('Sending get request to ' . $url, null, 'servicecenter.log');
			$config = array(
				'timeout' => 30
			);
			$client = new Zend_Http_Client($url, $config);
			$client->setParameterGet($parameter);
			$response = $client->request(Zend_Http_Client::GET);
		} catch(Exception $e) {
			Mage::log('Sending get request failed with exception: ' . $e->getMessage(), null, 'servicecenter.log');
			$response = false;
		}
		Mage::log($response->asString(), null, 'servicecenter.log');
		return $response;
	}

	public function sendPostRequest($url, $data, $isRaw) {
		try {
			$client = new Zend_Http_Client($url, array('timeout' => 30));
			if($isRaw) {
				$client->setHeaders(array(
					'Accept' => 'application/xml',
					'Content-Type' => 'application/xml'
				));
				$client->setRawData($data, 'text/xml');
			} else {
				$client->setParameterPost($data);
			}
			$response = $client->request(Zend_Http_Client::POST);
		} catch(Exception $e) {
			Mage::log('Sending post Request failed', null, 'servicecenter.log');
			$response = false;
		}

		return $response;
	}

}