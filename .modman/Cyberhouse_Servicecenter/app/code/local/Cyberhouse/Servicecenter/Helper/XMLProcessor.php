<?php

/**
 * Created by JetBrains PhpStorm.
 * User: avolk
 * Date: 14.11.13
 * Time: 16:09
 * To change this template use File | Settings | File Templates.
 */
class Cyberhouse_Servicecenter_Helper_XMLProcessor extends Mage_Core_Helper_Abstract
{

    const XML_FILE_DIR = 'retoure';


    //Identification of an article

    const SERVICECENTER_STATUS_TEXT = "cyberhouse_servicecenter/status_text/status_";

    const SERVICECENTER_CARRIER_CODE = "cyberhouse_servicecenter/carrier_code/status_";

    public function createIdentify($params)
    {

        $document = new DOMDocument('1.0', 'utf-8');
        $document->xmlStandalone = true;
        $root = $document->createElementNS('http://isc-gmbh.info/schemata/returnorder', 'ItemIdentificationRequest');
        $item = $document->createElement('itemInfo');
        $item = $this->setAttributes($params, $item);
        $locale = (stristr(Mage::app()->getStore()->getCode(), 'de')) ? 'de' : 'en';
        $locale = $document->createElement('locale', $locale);
        if ($item !== false) {
            $root->appendChild($item);
        } else {
            return false;
        }
        $document->appendChild($root);
        return $document->saveXML();
    }

    /**
     * @param $params array
     * @param $item DOMElement
     * @return mixed
     */
    private function setAttributes($params, $item)
    {
        if (array_key_exists('serial', $params)) {
            $item->setAttribute('serial_no', $params['serial']);
        } else if (array_key_exists('article', $params) && array_key_exists('identify', $params)) {
            $item->setAttribute('item_no', $params['article']);
            $item->setAttribute('ident_no', $params['identify']);
        } else {
            return false;
        }

        return $item;
    }

    public function parseIdentify($xml)
    {
        //Load xml response from eData
        $document = new DOMDocument();
        $document->loadXML($xml);

        /**
         * @var $element DOMNodeList
         */
        $list = $document->getElementsByTagName('ItemIdentificationResponse');
        $response = array();
        if ($list->length > 0) {
            /** @var $element DOMElement */
            $element = $list->item(0);
            if ($element->hasChildNodes()) {
                /** @var $children DOMElement */
                foreach ($element->childNodes as $children) {
                    if ($children->hasAttributes()) {
                        /** @var $attribute DOMAttr */
                        foreach ($children->attributes as $attribute) {
                            $response[$attribute->nodeName] = $attribute->nodeValue;
                        }
                    }
                }
            }
        }

        return $response;
    }

    //Symptoms

    /**
     * Parses the response for the symptoms call for translations, which
     * should be shown in frontend
     *
     * @param $symptoms string the response from eData as xml string
     * @return array with options as string
     */
    public function parseSymptoms($symptoms)
    {
        //Load xml response from eData
        $document = new DOMDocument();
        $document->loadXML($symptoms);

        //Query to select translation nodes via xpath
        $query = "//collection/symptom";
        $xpath = new DOMXPath($document);
        $symptoms = $xpath->query($query);

        //Loop all nodes and wrap them in options for frontend
        $options = array(($this->__("Choose symptom")));
        if ($symptoms->length > 0) {
            /** @var $symptom DOMNode */
            foreach ($symptoms as $symptom) {
                $navId = '';
                $value = '';
                /** @var $childNode DOMNode */
                foreach ($symptom->childNodes as $childNode) {
                    if ($childNode->nodeName == 'navId') {
                        $navId = $childNode->nodeValue;
                    }
                    if ($childNode->nodeName == 'description') {
                        /** @var $translation DOMNode */
                        foreach ($childNode->childNodes as $translation) {
                            if ($translation->nodeName == 'translation') {
                                $value = $translation->nodeValue;
                            }
                        }
                    }
                }
                if (strlen($value) > 0) {
                    $options[$navId] = $value;
                }
            }
        }

        return $options;
    }

    //ERP Request after order

    /**
     * @param $retoure Cyberhouse_Servicecenter_Model_Retoure
     * @return string
     */
    public function createReturnOrderERP($retoure)
    {
        $coreHelper = Mage::helper('core');
        $dhlData = $coreHelper->jsonDecode($retoure->getData('dhl_data'));
        $article = $coreHelper->jsonDecode($retoure->getData('article'));
        $customer = Mage::getModel('customer/customer')->load($retoure->getData('customer_id'));
        $address = $coreHelper->jsonDecode($retoure->getData('address'));
        $failure = $coreHelper->jsonDecode($retoure->getData('failure_description'));

        $document = new DOMDocument('1.0', 'utf-8');
        $document->xmlStandalone = true;
        $root = $document->createElementNS('http://isc-gmbh.info/schemata/erp', 'return_order_request');
        $root->appendChild($document->createElement('return_order_id', $retoure->getData('return_number')));
        $root->appendChild($document->createElement('return_order_date', $dhlData['issueDate']));
        $item = $document->createElement('return_order_status');
        $item->appendChild($document->createElement('status_no', $article['status_no']));
        $item->appendChild($document->createElement('status_text', $this->getStatusText($article['status_no'])));
        $root->appendChild($item);
        $root->appendChild($document->createElement('username', $customer->getEmail()));
        $item = $document->createElement('article_details');
        $serialNumber = array_key_exists('serial_no', $article) ? $article['serial_no'] : '';
        $item->setAttribute('serial_no', $serialNumber);
        $item->setAttribute('year_of_manufacture', '');
        $item->setAttribute('date_of_sale', '');
        $item->setAttribute('cash_voucher_no', '');
        $item->setAttribute('product_id', $article['sku']);
        $item->setAttribute('item_no', $article['item_no']);
        $item->setAttribute('item_version', array_key_exists('item_version', $article) ? $article['item_version'] : "");
        $root->appendChild($item);
        $item = $document->createElement('user_details');
        $item->appendChild($document->createElement('anrede', $address['prefix']));
        $item->appendChild($document->createElement('firstname', $address['firstname']));
        $item->appendChild($document->createElement('lastname', $address['lastname']));
        $item->appendChild($document->createElement('company_name', $address['company']));
        $item->appendChild($document->createElement('telephone', $address['telephone']));
        $item->appendChild($document->createElement('street', $address['street']));
        $item->appendChild($document->createElement('street2', $address['housenumber']));
        $item->appendChild($document->createElement('postal_code', $address['postcode']));
        $item->appendChild($document->createElement('city', $address['city']));
        $item->appendChild($document->createElement('country', $address['country_id']));
        $item->appendChild($document->createElement('debitor_no', ''));
        $item->appendChild($document->createElement('email', $customer->getEmail()));
        $root->appendChild($item);
        $item = $document->createElement('sca_details');
        $item->appendChild($document->createElement('main_symptom_code', $failure['symptom']));
        $item->appendChild($document->createElement('failure_remark', $failure['description']));
        $root->appendChild($item);
        $item = $document->createElement('carrier_information');

        $date = $retoure->getDate();
        $phone = $address['telephone'];
        $packageSize = $retoure->getPackageSize();

        if ($article['status_no'] == 200 || $article['status_no'] == 201 || $article['status_no'] == 202 || $article['status_no'] == 203) {
            $item->appendChild($document->createElement('package_type', "paket"));
        } else if ($article['status_no'] == 204) {
            $item->appendChild($document->createElement('package_type', "palette"));
        }

        if ($date) {
            $item->appendChild($document->createElement('preferred_day', $date));
        }
        if ($phone) {
            $item->appendChild($document->createElement('contact_telephone', $phone));
        }
        if ($packageSize) {
            $item->appendChild($document->createElement('anzahl_packstuecke', $packageSize));
        }

        $item->appendChild($document->createElement('carrier_code', $this->getCarrierCode($article['status_no'])));
        $item->appendChild($document->createElement('package_no', $dhlData['idc']));
        $item->appendChild($document->createElement('carrier_order_no', ""));
        $root->appendChild($item);
        $document->appendChild($root);
        //@RHU save also in filesystem
        $fileName = $retoure->getData('return_number') . '.xml';
        $document->save(Mage::getBaseDir('var') . '/' . self::XML_FILE_DIR . '/' . $fileName);
        return $document->saveXML();
    }

    private function getStatusText($id)
    {
        $value = Mage::getStoreConfig(self::SERVICECENTER_STATUS_TEXT .$id);
        return $value ? $value : "";
    }

    private function getCarrierCode($id)
    {
        $value = Mage::getStoreConfig(self::SERVICECENTER_CARRIER_CODE .$id);
        return $value ? $value : "";
    }
}