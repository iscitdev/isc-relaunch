<?php
/**
 * Created by JetBrains PhpStorm.
 * User: avolk
 * Date: 10.12.13
 * Time: 18:25
 * To change this template use File | Settings | File Templates.
 *
 * This model provides all information which should be displayed to the user in the account
 *
 */

class Cyberhouse_Servicecenter_Model_AccountRetoure extends Mage_Core_Model_Abstract {

	/**
	 * @var $date string
	 */
	private $date;

	/**
	 * @var $productInfo array
	 */
	private $productInfo;

	/**
	 * @var $address array
	 */
	private $address;

	/**
	 * @var $printLabelId int
	 */
	private $printLabelId;

	/**
	 * @param array $address
	 */
	public function setAddress($address) {
		$this->address = $address;
	}

	/**
	 * @return array
	 */
	public function getAddress() {
		return $this->address;
	}

	/**
	 * @param string $date
	 */
	public function setDate($date) {
		$this->date = $date;
	}

	/**
	 * @return string
	 */
	public function getDate() {
		return $this->date;
	}

	/**
	 * @param int $printLabelId
	 */
	public function setPrintLabelId($printLabelId) {
		$this->printLabelId = $printLabelId;
	}

	/**
	 * @return int
	 */
	public function getPrintLabelId() {
		return $this->printLabelId;
	}

	/**
	 * @param array $productInfo
	 */
	public function setProductInfo($productInfo) {
		$this->productInfo = $productInfo;
	}

	/**
	 * @return array
	 */
	public function getProductInfo() {
		return $this->productInfo;
	}
}