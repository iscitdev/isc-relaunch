<?php
/**
 * Created by JetBrains PhpStorm.
 * User: avolk
 * Date: 12.12.13
 * Time: 10:22
 * To change this template use File | Settings | File Templates.
 */

class Cyberhouse_Servicecenter_Model_Constants {

	/**
	 * 	CONSTANTS FOR COMMUNICATION WITH APIs
	 */

	//Constants for EData
	const URL_EDATA_PATH_SYMPTOMS = "cyberhouse_servicecenter/url/symptoms",
		  URL_EDATA_PATH_IDENTIFY = "cyberhouse_servicecenter/url/identify";

	//Constants for DHL
	const SOAP_CONNECTOR = "cyberhouse_servicecenter/url/soap_connect",
		  WSDL = "cyberhouse_servicecenter/url/wsdl",
		  SECURITY_NAMESPACE = "cyberhouse_servicecenter/url/security_namespace",
		  PDF = "cyberhouse_servicecenter/url/pdf";

	/**
	 * CONSTANTS FOR MODULE
	 */

	const RETURN_CACHE_IDENTIFIER = "online_retoure";
}