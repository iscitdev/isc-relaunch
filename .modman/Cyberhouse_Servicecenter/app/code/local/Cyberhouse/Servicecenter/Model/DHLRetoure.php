<?php
/**
 * Created by JetBrains PhpStorm.
 * User: avolk
 * Date: 15.11.13
 * Time: 09:52
 * To change this template use File | Settings | File Templates.
 */

class Cyberhouse_Servicecenter_Model_DHLRetoure extends Mage_Core_Model_Abstract {

	const DELIVERY_NAME_DE = "RetourenWeb03",
		  DELIVERY_NAME_AT = "Web_AT_CB03";

	/**
	 * The auth information to access the dhl interface
	 * Username
	 * Password
	 *
	 * @var $authInformation array
	 */
	private $authInformation;

	/**
	 * An associative array with all information of the sender address
	 * Keys:
	 *
	 * senderName1: Firstname,
	 * senderName2: Lastname,
	 * senderStreet: Street
	 * senderStreetNumber: Street number
	 * senderBoxNumber: Box number optional
	 * senderPostalCode: postal code
	 * senderCity: City
	 *
	 * @var $senderAddress array
	 */
	private $senderAddress;

	/**
	 * Format of the label (file type)
	 * default: pdf
	 *
	 * @var $labelFormat string
	 */
	private $labelFormat;

	/**
	 * Optional
	 *
	 * @var $customerReference string
	 */
	private $customerReference;

	/**
	 * Optional shipment reference
	 *
	 * @var $shipmentReference string
	 */
	private $shipmentReference;

	/**
	 * Name of the retour portal which should receive the delivery
	 *
	 * @var $deliverName string
	 */
	private $deliveryName;

	/**
	 * URL-Postfix of the portal
	 *
	 * @var $portalId string
	 */
	private $portalId;

	function __construct() {
		$this->labelFormat = "pdf";
		$this->authInformation = array("username" => "webisc", "password" => "isc1995+-AR");
		$this->portalId = "isc";
	}

	/**
	 * @param array $authInformation
	 */
	public function setAuthInformation($authInformation) {
		$this->authInformation = $authInformation;
	}

	/**
	 * @return array
	 */
	public function getAuthInformation() {
		return $this->authInformation;
	}

	/**
	 * @param array $senderAddress
	 */
	public function setSenderAddress($senderAddress) {
		$this->senderAddress = $senderAddress;
	}

	/**
	 * @return array
	 */
	public function getSenderAddress() {
		return $this->senderAddress;
	}

	/**
	 * @param string $customerReference
	 */
	public function setCustomerReference($customerReference) {
		$this->customerReference = $customerReference;
	}

	/**
	 * @return string
	 */
	public function getCustomerReference() {
		return $this->customerReference;
	}

	/**
	 * @param string $deliveryName
	 */
	public function setDeliveryName($deliveryName) {
		$this->deliveryName = $deliveryName;
	}

	/**
	 * @return string
	 */
	public function getDeliveryName() {
		return $this->deliveryName;
	}

	/**
	 * @param string $labelFormat
	 */
	public function setLabelFormat($labelFormat) {
		$this->labelFormat = $labelFormat;
	}

	/**
	 * @return string
	 */
	public function getLabelFormat() {
		return $this->labelFormat;
	}

	/**
	 * @param string $portalId
	 */
	public function setPortalId($portalId) {
		$this->portalId = $portalId;
	}

	/**
	 * @return string
	 */
	public function getPortalId() {
		return $this->portalId;
	}

	/**
	 * @param string $shipmentReference
	 */
	public function setShipmentReference($shipmentReference) {
		$this->shipmentReference = $shipmentReference;
	}

	/**
	 * @return string
	 */
	public function getShipmentReference() {
		return $this->shipmentReference;
	}
}