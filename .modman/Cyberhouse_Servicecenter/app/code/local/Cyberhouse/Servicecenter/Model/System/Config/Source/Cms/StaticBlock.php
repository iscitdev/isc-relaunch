<?php

class Cyberhouse_Servicecenter_Model_System_Config_Source_Cms_StaticBlock
{
    protected $_options;

    public function toOptionArray()
    {
        if (!$this->_options) {
            $blockCollection = Mage::getResourceModel('cms/block_collection')->addFieldToSelect(array('identifier'))
                ->load();
            foreach ($blockCollection as $block) {
                $this->_options[$block->getIdentifier()] = $block->getIdentifier();
            }
        }
        return $this->_options;
    }
}