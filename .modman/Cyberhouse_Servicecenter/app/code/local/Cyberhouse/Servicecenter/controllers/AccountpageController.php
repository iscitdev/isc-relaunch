<?php
/**
 * Created by JetBrains PhpStorm.
 * User: avolk
 * Date: 10.12.13
 * Time: 17:52
 * To change this template use File | Settings | File Templates.
 */

class Cyberhouse_Servicecenter_AccountpageController extends Mage_Core_Controller_Front_Action {

	protected function _getSession() {
		return Mage::getSingleton('customer/session');
	}

	public function preDispatch() {
		parent::preDispatch();
		if (!Mage::getSingleton('customer/session')->authenticate($this)) {
			$this->setFlag('', 'no-dispatch', true);
		}
	}

	/**
	 * Shows index site with all available return orders
	 */
	public function indexAction() {
		$this->loadLayout();
		$this->renderLayout();
	}

	/**
	 * Gets the base64 code of the pdf decodes it and
	 * returns the pdf to the browser for downloading it
	 *
	 */
	public function downloadAction() {
		$id = $this->getRequest()->getParam('id');
		/** @var $return Cyberhouse_Servicecenter_Model_Retoure */
		$return = Mage::getModel('servicecenter/Retoure')->load($id);
		if($return->getCustomerId() == $this->_getSession()->getCustomer()->getId()){
			$productNumber = Mage::helper('core')->jsonDecode($return->getData('article'));
			$productNumber = $productNumber['sku'];
			$data = $return->getData('dhl_label');
			$pdf = base64_decode($data);
			header('Content-type: application/pdf');
			header('Content-Disposition: attachment; filename="label_'. $return->getData('return_number') .'.pdf"');
			echo $pdf;
		}
		else {
			Mage::getSingleton("core/session")->addError($this->__("Error"));
			$this->_redirectError(Mage::getUrl("servicecenter/accountpage"));
		}
	}
}