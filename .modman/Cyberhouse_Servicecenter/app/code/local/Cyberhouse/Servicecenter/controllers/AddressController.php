<?php
/**
 * Created by JetBrains PhpStorm.
 * User: avolk
 * Date: 06.11.13
 * Time: 10:43
 * To change this template use File | Settings | File Templates.
 */

class Cyberhouse_Servicecenter_AddressController extends Mage_Core_Controller_Front_Action {

	/**
	 * Constants for json structure to identify the status of the save action
	 * STATUS_SUCCESS -> No error occurred, address is not saved in address book
	 * STATUS_SUCCESS_SAVE -> No error occurred, address is gonna be saved in address book
	 * STATUS_ERROR -> An error occurred
	 */
	const STATUS_SUCCESS = 1,
		  STATUS_SUCCESS_SAVE = 2,
		  STATUS_ERROR = 3;

	public function saveAction() {
		if (!$this->_validateFormKey()) {
			return $this->_redirect('*/*/');
		}
		// Save data
		if ($this->getRequest()->isPost()) {

			//Get customer from session and address from customer
			$customer = Mage::getSingleton('customer/session')->getCustomer();
			/* @var $address Mage_Customer_Model_Address */
			$address = Mage::getModel('customer/address');

			//Get next step id
			$nextStepId = $this->getRequest()->getParam("next-step-id");
			$errors = array();

			//Bind addres to address form
			/* @var $addressForm Mage_Customer_Model_Form */
			$addressForm = Mage::getModel('customer/form');
			$addressForm->setFormCode('customer_address_edit')
				->setEntity($address);
			//Check if addressId exists
			$addressId = $this->getRequest()->getPost('address_address_id', false);
			$saveAddress = false;
			if ($addressId) {
				$existsAddress = $customer->getAddressById($addressId);
				if ($existsAddress->getId() && $existsAddress->getCustomerId() == $customer->getId()) {
					$address->setId($existsAddress->getId());
				}
				$address->setData($existsAddress->getData());
				$addressForm->setEntity($address);
				$addressErrors = $addressForm->validateData($address->getData());
			} else {
				//Get user address data from request
				$data = $this->getRequest()->getPost('service-center', array());
				//Extract fields from array
				$addressData = $addressForm->extractData($addressForm->prepareRequest($data));
				//Validate address data (fields not empty)
				$addressErrors = $addressForm->validateData($addressData);
				$saveAddress = array_key_exists('save_in_address_book', $data) && $data['save_in_address_book'] == 1;
				//Assign customer id to address and define to not use this address as default address
				$addressForm->compactData($addressData);
				$address->setCustomerId($customer->getId())
					->setIsDefaultBilling($this->getRequest()->getParam('default_billing', false))
					->setIsDefaultShipping($this->getRequest()->getParam('default_shipping', false));
			}

			if ($addressErrors !== true) {
				$errors = $addressErrors;
			}

			try {

				//Check address
				$addressErrors = $address->validate();
				if ($addressErrors !== true) {
					$errors = array_merge($errors, $addressErrors);
				}

				//Define response array with status for save address
				$response = array(
					'status' => 0,
					'successmessage' => "",
					'errormessage' => array(),
					'nextStepId' => $nextStepId
				);

				if(strcmp($address->getData('country_id'), "AT") != 0 && strcmp($address->getData('country_id'), "DE") != 0) {
					$errors = array($this->__("Online return is only possible to Germany or Austria"));
				}

				/** @var $helper Cyberhouse_Servicecenter_Helper_Data */
				$helper = Mage::helper('servicecenter');

				if (count($errors) === 0 && $saveAddress) {
					//Save address and set status to success with save
					$address->save();
					//Add data to retoure model in session
					$helper->addAddress($address);
					$response['successmessage'] = $this->__('The address has been saved.');
					$response['status'] = self::STATUS_SUCCESS_SAVE;
				} else if (count($errors) === 0 && !$saveAddress) {
					//Save address and set status to success without save
					$response['status'] = self::STATUS_SUCCESS;
					//Add data to retoure model in session
					$helper->addAddress($address);
				} else {
					//An error occurred set response to status error
					Mage::getSingleton('customer/session')->setAddressFormData($this->getRequest()->getPost());
					$response['status'] = self::STATUS_ERROR;
					foreach ($errors as $errorMessage) {
						$response['errormessage'][] = $errorMessage;
					}
				}
				echo Mage::helper('core')->jsonEncode($response);
				return;
			} catch (Mage_Core_Exception $e) {
				$response['errormessage'][] = $this->__('Cannot save address.');
				$response['status'] = self::STATUS_ERROR;
				echo Mage::helper('core')->jsonEncode($response);
				Mage::getSingleton('customer/session')->setAddressFormData($this->getRequest()->getPost())
					->addException($e, $e->getMessage());
			} catch (Exception $e) {
				$response['errormessage'][] = $this->__('Cannot save address.');
				$response['status'] = self::STATUS_ERROR;
				echo Mage::helper('core')->jsonEncode($response);
				Mage::getSingleton('customer/session')->setAddressFormData($this->getRequest()->getPost())
					->addException($e, $this->__('Cannot save address.'));
			}
		}
	}
}