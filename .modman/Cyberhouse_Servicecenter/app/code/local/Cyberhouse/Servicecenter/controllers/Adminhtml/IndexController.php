<?php

/**
 * Created by PhpStorm.
 * User: rhutterer
 * Date: 25.07.14
 * Time: 09:05
 */
class Cyberhouse_Servicecenter_Adminhtml_IndexController extends Mage_Adminhtml_Controller_Action
{

    protected function _initAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('customer/servicecenter')
            ->_addBreadcrumb(Mage::helper('adminhtml')->__('Customer'), Mage::helper('adminhtml')->__('Entities Manager'));

        return $this;
    }


    public function indexAction()
    {
        $this->_initAction()
            ->renderLayout();
    }


    public function downloadAction()
    {
        $retoure = Mage::getModel('servicecenter/retoure')->load($this->getRequest()->getParam('id'));
        Mage::helper('servicecenter/XMLProcessor')->createReturnOrderERP($retoure);
        $fileName = $retoure->getData('return_number') . '.xml';
        $filePath = Mage::getBaseDir('var') . '/' . Cyberhouse_Servicecenter_Helper_XMLProcessor::XML_FILE_DIR . '/' . $fileName;
        $this->_prepareDownloadResponse($fileName, file_get_contents($filePath));
    }

    /**
     * Gets the base64 code of the pdf decodes it and
     * returns the pdf to the browser for downloading it
     *
     */
    public function pdfAction() {
        $id = $this->getRequest()->getParam('id');
        /** @var $return Cyberhouse_Servicecenter_Model_Retoure */
        $return = Mage::getModel('servicecenter/Retoure')->load($id);
        $productNumber = Mage::helper('core')->jsonDecode($return->getData('article'));
        $productNumber = $productNumber['sku'];
        $data = $return->getData('dhl_label');
        $pdf = base64_decode($data);
        header('Content-type: application/pdf');
        header('Content-Disposition: attachment; filename="label_'. $return->getData('return_number') .'.pdf"');
        echo $pdf;
    }

}