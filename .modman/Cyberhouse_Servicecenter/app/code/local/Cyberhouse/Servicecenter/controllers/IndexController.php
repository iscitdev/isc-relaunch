<?php

/**
 * Created by JetBrains PhpStorm.
 * User: avolk
 * Date: 23.10.13
 * Time: 16:25
 * To change this template use File | Settings | File Templates.
 */
class Cyberhouse_Servicecenter_IndexController extends Mage_Core_Controller_Front_Action
{

    const SERVICECENTER_INDEX_ACTION = "SERVICE_STEP1";

    /** @var $helper Cyberhouse_Servicecenter_Helper_Data */
    protected $helper;

    public function _construct ()
    {
        parent::_construct();
        $this->helper = Mage::helper( 'servicecenter' );
        return $this;
    }

    public function preDispatch ()
    {
        $session = Mage::getSingleton( 'customer/session' );
        if (!$session->getCustomerId()) {
            $session->setAfterAuthUrl( Mage::helper( 'core/url' )->getCurrentUrl() );
            $session->addNotice( $this->getLayout()->createBlock( 'cms/block' )->setBlockId( 'service_center_no_logon' )->toHtml() );
            $this->getResponse()->setRedirect(
                Mage::getUrl( 'customer/account/login' )
            );

            $this->setFlag( '', self::FLAG_NO_DISPATCH, true );
            return $this;
        }
        return parent::preDispatch();
    }

    public function indexAction ()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

    /**
     * @deprecated
     */
    public function collectAction ()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

    /**
     * @deprecated
     */
    public function fuelAction ()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

    public function errorAction ()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

    public function getSymptomAction ()
    {
        //Get option value for hasOperated get parameter
        $operated = $this->getRequest()->getParam( 'operated' );
        /** @var $coreHelper Mage_Core_Helper_Data */
        $coreHelper = Mage::helper( 'core' );
        //Get item id of the article
        $itemId = $this->helper->getItemId();
        if ($itemId === false) {
            $this->getResponse()->setBody( $coreHelper->jsonEncode( $this->helper->getErrorResponse( array( "The service is currently not available. Please contact the owner of the shop." ) ) ) );
        } else {
            $locale = $this->helper->getLocale();

            //Send Request to edata
            $url = Mage::getStoreConfig( Cyberhouse_ServiceCenter_Model_Constants::URL_EDATA_PATH_SYMPTOMS );
            $parameter = array(
                "itemId" => $itemId,
                "type" => 'MAIN',
                'locale' => $locale,
                'hasOperated' => $operated
            );
            /** @var $requestHelper Cyberhouse_Servicecenter_Helper_Request */
            $requestHelper = Mage::helper( 'servicecenter/request' );
            /** @var $response Zend_Http_Response */
            $response = $requestHelper->sendGetRequest( $url, $parameter );
            //Parse Response
            if ($response !== false) {
                $options = Mage::helper( 'servicecenter/XMLProcessor' )->parseSymptoms( $response->getBody() );
                $json = array(
                    "status" => 1,
                    "messages" => array(),
                    "data" => $options
                );
                $this->getResponse()->setBody( $coreHelper->jsonEncode( $json ) );
            } else {
                $this->getResponse()->setBody( $coreHelper->jsonEncode( $this->helper->getErrorResponse( array( "The service is currently not available. Please contact the owner of the shop." ) ) ) );
            }
        }
    }

    public function callDHLAction ()
    {
        $errors = false;
        /** @var $helper Cyberhouse_Servicecenter_Helper_DHLLabel */
        $helper = Mage::helper( 'servicecenter/DHLLabel' );
        if ($this->getRequest()->getParam( "address_address_id" ) != "") {
            $addressId = $this->getRequest()->getParam( "address_address_id" );
            $address = Mage::helper( "customer" )->getCustomer()->getAddressById( $addressId );
        } else {
            $address = Mage::getModel( 'customer/address' );
            $address->addData( $this->getRequest()->getParam( "service-center" ) );

            $street = $address->getStreet();
            if (is_array( $street )) {
                $address->setData( "street", implode( "\n", $street ) );
            }
        }

        $retoure = $this->helper->getRetoure();
        $statuscode = $retoure->getStatusCode();

        $date = $this->getRequest()->getParam( "date" );
        $phone = $this->getRequest()->getParam( "telephone" );

        if ($statuscode == 203 || $statuscode == 204) {
            if (empty( $phone )) {
                Mage::getSingleton( "core/session" )->addError( $this->__( "Phone number is required" ) );
                $errors = true;
            }
            $address->setTelephone( $phone );
            if ($this->validateDate( $date )) {
                $dateArray = Zend_Locale_Format::getDate( $date, array( 'date_format' => 'yyyy.MM.dd' ) );
                $retoure->setDate( "{$dateArray['day']}.{$dateArray['month']}.{$dateArray['year']}" );
            }

            Mage::register( 'servicecenter_current_date', $date );
            Mage::register( 'servicecenter_current_phone', $phone );
        }

        $addressForm = Mage::getModel( 'customer/form' );
        $addressForm->setFormCode( 'customer_address_edit' )->setEntity( $address );

        $addressData = $addressForm->extractData( $this->getRequest(), 'address', false );
        $validationResult = $addressForm->validateData( $addressData );

        Mage::register( 'servicecenter_current_address', $address );

        $content = "";
        if (is_array( $validationResult ) || $errors) {
            foreach ($validationResult as $error) {
                Mage::getSingleton( "core/session" )->addError( $error );
            }
            $content = $this->getCurrentStepHtml();
        } else if (!$this->validateDate( $date ) && !empty( $date )) {
            Mage::getSingleton( "core/session" )->addError( $this->__( "Invalid Date" ) );
            $content = $this->getCurrentStepHtml();
        } else {
            $retoure->setPackageSize( $this->getRequest()->getParam( "package-size" ) );

            $this->helper->addAddress( $address );
            if (!$this->helper->skipReturnLabel()) {
                $label = $helper->createLabel( $retoure );
                if ($label !== false) {
                    $this->helper->addDHLData( $label );
                }
                else  {
                    $content = $this->loadHTMLFromHandle( self::SERVICECENTER_INDEX_ACTION );
                }
            }
            $xml = Mage::helper( 'servicecenter/XMLProcessor' )->createReturnOrderERP( $retoure );
            $response = Mage::helper( 'cyberhouse_queue/data' )->sendToQueue( $xml, 'outgoing', 'return_order_outgoing' );
            Mage::log( $response, Zend_Log::INFO, 'servicecenter.log' );
            $parameter = array( "id" => $retoure->getId() );
            $this->helper->persistRetoure();
            if(!$content){
                $content = $this->getNextStepHtml( $parameter );
            }
        }

        $status = $this->helper->statusMapper( $this->helper->getStatusCode() );
        $this->getResponse()->setBody( $this->getResponseJson( $status, null, null, $content ) );
    }

    public function saveFailureDescriptionAction ()
    {
        $productWorked = $this->getRequest()->getParam( "product-worked" );

        if ($productWorked == null) {
            Mage::getSingleton( "core/session" )->addError( $this->__( "No viability selected" ) );
            $content = $this->getCurrentStepHtml();
        } else {
            $failureDescription = array(
                "description" => $this->getRequest()->getParam( "error-description" ),
                "product-worked" => $productWorked,
                "symptom" => $this->getRequest()->getParam( "symptom" )
            );
            $this->helper->addFailureDescription( $failureDescription );
            $this->helper->addReturnNumber();

            // load handle
            $content = $this->getNextStepHtml();
            $this->helper->nextStep();
        }
        $this->getResponse()->setBody( Mage::helper( 'core' )->jsonEncode( array( "content" => $content ) ) );
    }

    /**
     * @deprecated
     */
    public function successAction ()
    {
        $this->loadLayout();
        $block = $this->getLayout()->getBlock( 'service.center.success' );
        $block->setData( 'id', $this->getRequest()->getParam( 'id' ) );
        $this->renderLayout();
    }

    public function checkAction ()
    {
        $params = $this->getRequest()->getParams();
        /** @var $xmlHelper Cyberhouse_Servicecenter_Helper_XMLProcessor */
        $xmlHelper = Mage::helper( 'servicecenter/XMLProcessor' );
        $xml = $xmlHelper->createIdentify( $params );
        //Creation of the xml didn't work
        if ($xml === false) {
            $this->getResponse()->setBody( Mage::helper( 'core' )->jsonEncode( $this->helper->getErrorResponse( array( "The service is currently not available. Please contact the owner of the shop." ) ) ) );
        } else {
            $url = Mage::getStoreConfig( Cyberhouse_ServiceCenter_Model_Constants::URL_EDATA_PATH_IDENTIFY );

            /** @var $requestHelper Cyberhouse_Servicecenter_Helper_Request */
            $requestHelper = Mage::helper( 'servicecenter/Request' );
            $response = $requestHelper->sendPostRequest( $url, $xml, true );
            if ($response === false) {
                $this->getResponse()->setBody( Mage::helper( 'core' )->jsonEncode( $this->helper->getErrorResponse( array( "The service is currently not available. Please contact the owner of the shop." ) ) ) );
            } else {
                $articleData = $xmlHelper->parseIdentify( $response->getBody() );

                $status = $this->helper->statusMapper( $articleData["status_no"] );

                //If identification of the article was successful
                //add data to session
                $statusNr = $articleData["status_no"];
                $this->helper->addArticleData( $articleData );
                $this->helper->addStatusCode( $statusNr );
                $this->helper->setCurrentStep( 1 );

                //Get array of possible redirect urls for a specific status
                $urls = $this->helper->getRedirectUrls( false );
                //Get url for status if available
                $url = ( array_key_exists( $status, $urls ) ) ? $urls[$status] : "";

                $content = $this->getNextStepHtml();
                $this->helper->nextStep();
                //Generate array for response
                $response = $this->getResponseJson( $status, $url, $articleData, $content );
                //Render response which is processed in script.js -> checkProductIdentity
                $this->getResponse()->setBody( $response );
            }
        }
    }

    protected function getNextStepHtml ( $param = null )
    {
        return $this->loadHTMLFromHandle( $this->getNextStepHandle(), $param );
    }

    protected function getCurrentStepHtml ( $param = null )
    {
        return $this->loadHTMLFromHandle( $this->getCurrentStepHandle(), $param );
    }

    private function getCurrentStepHandle ()
    {
        return "SERVICE_" . $this->helper->getStatusCode() . "_STEP" . (string)( $this->helper->getCurrentStep() );
    }

    private function getNextStepHandle ()
    {
        return "SERVICE_" . $this->helper->getStatusCode() . "_STEP" . (string)( $this->helper->getCurrentStep() + 1 );
    }

    private function loadHTMLFromHandle ( $handle, $params = null )
    {
        if ($params !== null) {
            Mage::register( "current_step_params", $params );
        }
        $this->loadLayout( array( 'default', $handle ) );
        return $this->getLayout()->getOutput();
    }

    /**
     * @param $status
     * @param $url
     * @param $articleData
     * @param $content
     * @return array
     */
    private function getResponseJson ( $status, $url, $articleData, $content )
    {
        $response = array(
            "status" => $status,
            "messages" => ( $status == 4 ) ? array( $this->helper->__( "The product could not be found" ) ) : array(),
            "url" => $url,
            "productData" => $articleData
        );
        $response = array_merge( $response, array( "content" => $content ) );
        return Mage::helper( 'core' )->jsonEncode( $response );
    }

    private function validateDate ( $date )
    {
        $validator = new Zend_Validate_Date();
        return $validator->isValid( $date );
    }

}