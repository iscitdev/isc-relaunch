<?php

$allRetours = Mage::getResourceModel('servicecenter/retoure_collection');
$helper = Mage::helper( "core" );
foreach( $allRetours as $retoure){
    $json = $helper->jsonDecode( $retoure->getArticle() );
    $retoure->setStatusCode($json['status_no']);
}
$allRetours->save();