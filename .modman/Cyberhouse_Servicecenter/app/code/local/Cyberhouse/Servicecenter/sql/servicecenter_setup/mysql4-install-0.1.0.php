<?php
$installer = $this;
$installer->startSetup();
$sql=<<<SQLTEXT
create table service_center_retoure(id int not null auto_increment, article text, address_id int, address text, failure_description text, dhl_label text, dhl_data text, primary key(id));
SQLTEXT;

$installer->run($sql);

$installer->endSetup();
