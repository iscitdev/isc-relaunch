<?php
$installer = $this;
$installer->startSetup();
$sql=<<<SQLTEXT
alter table service_center_retoure add customer_id int unsigned default null;
alter table service_center_retoure add index customer_id (customer_id);
alter table service_center_retoure add constraint customer_id foreign key(customer_id) references customer_entity(entity_id) on delete cascade on update cascade;
SQLTEXT;

$installer->run($sql);

$installer->endSetup();
