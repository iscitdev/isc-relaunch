<?php
$installer = $this;
$installer->startSetup();
$sql=<<<SQLTEXT
alter table service_center_retoure add created TIMESTAMP DEFAULT CURRENT_TIMESTAMP;
SQLTEXT;

$installer->run($sql);

$installer->endSetup();
