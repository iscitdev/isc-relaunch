<?php
$installer = $this;
$installer->startSetup();
$sql=<<<SQLTEXT
alter table `service_center_retoure` CHANGE COLUMN `dhl_label` `dhl_label` MEDIUMTEXT NULL DEFAULT NULL ;
SQLTEXT;

$installer->run($sql);

$installer->endSetup();
