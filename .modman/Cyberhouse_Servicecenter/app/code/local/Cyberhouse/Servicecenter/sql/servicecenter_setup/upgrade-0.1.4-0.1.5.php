<?php
$installer = $this;
/** @installer Mage_Core_Model_Resource_Setup */
$installer->startSetup();
$installer->getConnection()
    ->addColumn($installer->getTable("servicecenter/retoure"),
        'status_code',
    array(
        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
        'nullable' => false,
        'length' => 5,
        'default' => '200',
        'comment' => "Status Code"
    )
);

$installer->endSetup();
