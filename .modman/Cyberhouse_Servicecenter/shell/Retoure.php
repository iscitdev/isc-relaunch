<?php
/**
 * Created by PhpStorm.
 * User: rhutterer
 * Date: 28.07.14
 * Time: 12:41
 */
include_once 'abstract.php';

class Cyberhouse_Shell_Retoure extends Mage_Shell_Abstract
{
    public function run()
    {
        $retoure = Mage::getModel('servicecenter/retoure')->load(1);
        $helper = Mage::helper('servicecenter/XMLProcessor')->createReturnOrderERP($retoure);
    }
}
$test = new Cyberhouse_Shell_Retoure();
$test->run();