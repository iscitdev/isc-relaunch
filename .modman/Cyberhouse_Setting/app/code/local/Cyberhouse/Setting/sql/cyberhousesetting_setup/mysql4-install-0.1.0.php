<?php

$this->startSetup();

Mage::getModel('eav/entity_attribute')
	->loadByCode('customer_address', 'telephone')
	->setIsRequired(false)
	->save();

$this->endSetup();
