<?php
$installer = new Mage_Catalog_Model_Resource_Eav_Mysql4_Setup('core_setup');
$installer->startSetup();

$attributeName = "explosion_image";

$existingAttribute = $installer->getAttribute(Mage_Catalog_Model_Product::ENTITY, $attributeName);
if($existingAttribute){
    $installer->updateAttribute(Mage_Catalog_Model_Product::ENTITY, $attributeName, 'used_in_product_listing', 1);
}else {

    $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, $attributeName, array(
        'group' => 'ISC',
        'type' => 'text',
        'label' => 'Explosion Image',
        'input' => 'text',
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'sort_order' => 0,
        'visible' => true,
        'required' => false,
        'user_defined' => true,
        'searchable' => false,
        'filterable' => false,
        'comparable' => false,
        'visible_on_front' => false,
        'unique' => false,
        'is_configurable' => false,
        'is_html_allowed_on_front' => true,
        'used_in_product_listing' => 1
    ));

}
$installer->endSetup();