<?php
$installer = $this;
$installer->startSetup();

$configModel = Mage::getConfig();

$configModel->saveConfig("cataloginventory/item_options/max_sale_qty","25","default",0);

$configModel->cleanCache();

$installer->endSetup();