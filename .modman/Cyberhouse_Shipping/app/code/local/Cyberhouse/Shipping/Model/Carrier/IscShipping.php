<?php

class Cyberhouse_Shipping_Model_Carrier_IscShipping extends Mage_Shipping_Model_Carrier_Abstract
{
	/* Use group alias */
	protected $_code = 'cyberhouse_shipping';
	protected $ERROR_OK = 200;


	public function collectRates(Mage_Shipping_Model_Rate_Request $request)
	{
		// skip if not enabled
		if (!Mage::getStoreConfig('carriers/' . $this->_code . '/active')) {
			return false;
		}

		$result           = Mage::getModel('shipping/rate_result');
		$handlingFallback = Mage::getStoreConfig('carriers/' . $this->_code . '/handling_fallback');
		$handling         = $handlingFallback;

		$method = Mage::getModel('shipping/rate_result_method');
		$method->setCarrier($this->_code);
		$method->setCarrierTitle(Mage::getStoreConfig('carriers/' . $this->_code . '/title'));
		/* Use method name */
		$method->setMethod('delivery');
		$method->setMethodTitle(Mage::getStoreConfig('carriers/' . $this->_code . '/methodtitle'));

        if ($this->inCart()) {
            $quote = Mage::getSingleton("checkout/session")->getQuote();
            $shippingAddress = $quote->getShippingAddress();
            $loadedShippingAddress = Mage::getModel("sales/quote_address")->load($shippingAddress->getId());
            $error = false;
            $handling = $loadedShippingAddress->getShippingAmount();
        }
        else {

            // get delivery cost from isc internal systems

            // 1. get quote items
            // 2. build transport cost request xml
            // 3. send to queue
            $quote      = Mage::getModel('checkout/cart')->getQuote();
            $xmlMessage = $this->generateTransportCostXml($quote);
            Mage::log($xmlMessage, null, 'transport_cost.log');
            $response = Mage::helper('cyberhouse_queue/data')->sendToQueueAndGetResponse($xmlMessage, 'outgoing', 'transport_cost_outgoing');

            $error               = false;
            $cashOnDeliveryCosts = false;

            if ($response !== false) {
                $message = $response->body;
                Mage::log($message, null, 'transport_cost.log');
                $data = simplexml_load_string($message);
                if (isset($data->shoppingcart_info)) {
                    $info  = $data->shoppingcart_info;
                    $costs = $info->costs;
                    if (isset($info->cash_on_delivery_costs)) {
                        $cashOnDeliveryCosts = (int)$info->cash_on_delivery_costs;
                    }
                    if ((int)$info->error == 200) {
                        $handling = floatval($costs);
                        // add spediteur to title
                        if ($info->spediteur) {
                            $method->setMethodTitle($method->getMethodTitle() . " (" . $info->spediteur . ")");
                        }
                        if ($cashOnDeliveryCosts !== false) {
                            Mage::getSingleton('core/session')->setCashOnDeliveryCosts($cashOnDeliveryCosts);
                        }
                    } else {
                        // set fallback price if calculation fails
                        $handling = $handlingFallback;

                        // $error = Mage::getModel('shipping/rate_result_error');
                        // $error->setData('error_message', "#".$data->shoppingcart_info->error.": ".$data->shoppingcart_info->error_txt);
                    }
                }
            }
        }

		$method->setCost($handling);
		$method->setPrice($handling);

		if ($error !== false) {
			$result->append($error);
		} else {
			$result->append($method);
		}

		return $result;
	}

    protected function inCart(){
        $request = Mage::app()->getRequest();
        $module = $request->getModuleName();
        $controller = $request->getControllerName();
        $action = $request->getActionName();
        if($module == 'checkout' && $controller == 'cart' && $action == 'index')
        {
            return true;
        }
        return false;
    }

	/**
	 * Generate xml for transport cost request from quote
	 * @param Mage_Sales_Model_Quote $quote
	 */
	protected function generateTransportCostXml(Mage_Sales_Model_Quote $quote)
	{

		$doc = new DOMDocument('1.0', 'utf-8');

		$root = $doc->createElement("transport_costs_request");
		$root->setAttribute('xmlns', 'http://isc-gmbh.info/schemata/erp');

		$paymentType = $quote->getPayment()->getMethod();

		// collect simple, first-level attributes + values
		$orderData = array(
			'target_domain' => $quote->getStore()->getWebsite()->getCode(),
			'currency'      => $quote->getQuoteCurrencyCode(),
			'payment_type'  => Mage::helper("cyberhouse_checkout")->mapPaymentType($paymentType)
		);

		// add billing address
		$doc = $this->generateAddressXml($doc, $root, $quote->getShippingAddress());

		// add fields to doc
		foreach ($orderData as $attribute => $value) {
			$root->appendChild($doc->createElement($attribute, $value));
		}

		// add ordered items
		$orderItems = $doc->createElement('order_items');
		foreach ($quote->getItemsCollection() as $item) {

			$orderItem = $doc->createElement('item');
			$product   = Mage::getModel('catalog/product')->load($item->getProductId());

			// prepare item attributes
			$itemAttributes = array(
				'unit_sales_price' => round($item->getPrice(), 2),
				'quantity'         => round($item->getQty(), 0),
				'product_id'       => $item->getSku(),
				'item_no'          => $product->getProductNo(),
				'item_version'     => $product->getProductVersion()
			);

			// set attributes
			foreach ($itemAttributes as $attribute => $value) {
				$orderItem->setAttribute($attribute, $value);
			}

			// append to orderitems-wrap
			$orderItems->appendChild($orderItem);
		}
		$root->appendChild($orderItems);

		// append root to document
		$doc->appendChild($root);

		// format output xml for better reading
		$doc->formatOutput = true;

		$output = $doc->saveXML();

		return $output;
	}

	public function generateAddressXml($doc, $root, $address)
	{
		$addressNode = $doc->createElement('address');

		$addressStreet = $address->getStreet();

		$addressData = array(
			'street'      => $addressStreet[0],
			'postal_code' => $address->getPostcode(),
			'city'        => $address->getCity(),
			'country'     => $address->getCountryId()
		);

		foreach ($addressData as $attribute => $value) {
			$addressNode->appendChild($doc->createElement($attribute, $value));
		}

		$root->appendChild($addressNode);

		return $doc;
	}
}

