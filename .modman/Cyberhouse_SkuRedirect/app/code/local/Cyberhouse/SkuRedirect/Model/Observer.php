<?php

class Cyberhouse_SkuRedirect_Model_Observer {

    public function redirect($observer) {

        $storeId = Mage::app()->getStore()->getId();
        $sku = $observer->getFront()->getRequest()->getParam('sku', null);
        if ($sku == null) {
			return;
		}
        /* @var $_product Mage_Catalog_Model_Product */
        $_product = Mage::helper('catalog/product')->getProduct($sku, $storeId, 'sku');

        if (!$_product->getId()) {
			Mage::log('Redirect for invalid product. SKU was: '.$sku);
			return;
		}
        if ($_product->isVisibleInSiteVisibility()) {
			if (in_array(Mage::app()->getStore()->getId(), $_product->getStoreIds())) {
                $category_ids = $_product->getCategoryIds();
                $_product->setCategoryId(reset($category_ids));
				$url = $_product->getProductUrl();

				header ('HTTP/1.1 302 Moved Permanently');
				header ('Location: '.$url);
				die();

			} else {
				Mage::log('Product not assigned for current store for invisible product. SKU was: '.$sku);
			}
        } else {
			Mage::log('Redirect for invisible product. SKU was: '.$sku);
		}
    }
}
