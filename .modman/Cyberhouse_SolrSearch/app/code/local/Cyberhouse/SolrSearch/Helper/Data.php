<?php

class Cyberhouse_SolrSearch_Helper_Data extends Mage_Core_Helper_Abstract {

    const QUERY_VAR_NAME = 'q';
    const MAX_QUERY_LEN  = 200;
    const MIN_QUERY_LEN = 4;


    /**
     * Query object
     *
     * @var Mage_CatalogSearch_Model_Query
     */
    protected $_query;

    /**
     * Query string
     *
     * @var string
     */
    protected $_queryText;

    /**
     * Is a maximum length cut
     *
     * @var bool
     */
    protected $_isMaxLength = false;

	public function doRequest($url) {

		$c = curl_init($url);
		curl_setopt($c, CURLOPT_HEADER, 0);
		curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($c, CURLOPT_CONNECTTIMEOUT ,2);
        curl_setopt($c, CURLOPT_TIMEOUT, 10); //timeout in seconds

		curl_setopt($c, CURLOPT_FOLLOWLOCATION, true );
		curl_setopt($c, CURLOPT_USERAGENT, isset($_GET['user_agent']) ? $_GET['user_agent'] : $_SERVER['HTTP_USER_AGENT'] );

		$output = curl_exec($c);
		$output = json_decode($output,true);

		curl_close($c);

		return $output;
	}

	public function getSolrUrl() {
		$solrHost = $this->getSolrSetting('solr_host');
		$solrPort = $this->getSolrSetting('solr_port');
		$solrCore = $this->getSolrSetting('solr_core');

		if (empty($solrHost) || empty($solrPort) || empty($solrCore)) {
			throw new Exception('Incomplete solr settings');
		}

		return $solrHost.':'.$solrPort.'/solr/'.$solrCore.'/';
	}

	public function getSolrSetting($settingName) {
		return Mage::getStoreConfig('isc_solr_settings/solr_setting/' . $settingName);
	}

    /**
     * Retrieve HTML escaped search query
     *
     * @return string
     */
    public function getEscapedQueryText()
    {
        return $this->escapeHtml($this->getQueryText());
    }

    /**
     * Retrieve search query text
     *
     * @return string
     */
    public function getQueryText()
    {
        if (!isset($this->_queryText)) {
            $this->_queryText = $this->_getRequest()->getParam($this->getQueryParamName());
            if ($this->_queryText === null) {
                $this->_queryText = '';
            } else {
                /* @var $stringHelper Mage_Core_Helper_String */
                $stringHelper = Mage::helper('core/string');
                $this->_queryText = is_array($this->_queryText) ? ''
                    : $stringHelper->cleanString(trim($this->_queryText));

                $maxQueryLength = $this->getMaxQueryLength();
                if ($maxQueryLength !== '' && $stringHelper->strlen($this->_queryText) > $maxQueryLength) {
                    $this->_queryText = $stringHelper->substr($this->_queryText, 0, $maxQueryLength);
                    $this->_isMaxLength = true;
                }
            }
        }
        return $this->_queryText;
    }

    /**
     * Retrieve search query parameter name
     *
     * @return string
     */
    public function getQueryParamName()
    {
        return self::QUERY_VAR_NAME;
    }

    public function getMaxQueryLength(){
        return self::MAX_QUERY_LEN;
    }

    public function getMinQueryLenght(){
        return self::MIN_QUERY_LEN;
    }

    public function prepareQueryParam( $query ) {
        $no_space_query = trim(str_replace(" ","",str_replace("."," ",$query)));
        if (is_numeric( $query ) || is_numeric($no_space_query) ) {
            $query = "einhell_article:".$no_space_query;
        }
        return $query;
    }

}