<?php

class Cyberhouse_SolrSearch_Model_Observer {


    public function disableCatalogSearchIndex(){
        $values = Mage::getConfig()->getNode('global/index/indexer');
        unset($values['catalogsearch_fulltext']);
        unset($values['tag_summary]']);
        @Mage::getConfig()->setNode('global/index/indexer',$values, true);
    }

}