<?php

class Cyberhouse_SolrSearch_Model_Search extends Mage_Core_Model_Abstract
{

    private $_queryAction = 'select';
    private $_page = 1;
    const MAX_RESPONSE_TIME = 10; //Maximum allowed response for query processing in Seconds
    private $responseStart;

    private $_searchFields = array();
    private $_returnFields = array();

    public function __construct() {
        $this->setData( 'rows', Mage::helper( 'cyberhouse_solrsearch' )->getSolrSetting( 'solr_result_rows' ) );
    }

    private function _getConfigParams() {
        return array(
            'wt' => 'json',
            'defType' => 'edismax',
            'qf' => urlencode( 'content title keywords einhell_article textSearch' ),
            'fl' => empty( $this->_returnFields ) ? '*' : join( ',', $this->_returnFields ),
            'rows' => $this->getData( 'rows' ),
            'start' => ( ( $this->_page - 1 ) * $this->getData( 'rows' ) )
        );
    }

    private function _getLocaleString() {
        return '&fq=site:' . substr( Mage::app()->getStore()->getCode(), 0, 6 ) .
        '&fq=language:' . substr( Mage::app()->getLocale()->getLocaleCode(), 0, 2 );
    }

    public function setPage( $page ) {
        $this->_page = $page;
    }

    private function buildQueryParams( $queryParams = array() ) {
        $string = '?';
        $divider = '';

        foreach ($queryParams as $paramName => $paramValue) {
            $string .= $divider . $paramName . '=' . $paramValue;
            $divider = '&';
        }

        // include config params
        foreach ($this->_getConfigParams() as $paramName => $paramValue) {
            $string .= $divider . $paramName . '=' . $paramValue;
            $divider = '&';
        }

        $string .= $this->_getLocaleString();

        return $string;
    }

    public function setSearchFields( $searchFields ) {
        $this->_searchFields = $searchFields;
        return $this;
    }

    public function setReturnFields( $returnFields ) {
        $this->_returnFields = $returnFields;
        return $this;
    }

    public function query( $query, $extra = array() ) {

        $solrHelper = Mage::helper( 'cyberhouse_solrsearch' );

        // build query url
        $solrUrl = $solrHelper->getSolrUrl();

        if(!is_array($query)){
            $queryParams = array(
                'q' => $query
            );
        }
        else {
            $queryParams = $query;
        }

        $queryParams += $extra;

        $queryParamsString = $this->buildQueryParams( $queryParams );

        // assemble url
        $solrQueryUrl = $solrUrl . $this->_queryAction;
        $solrQueryUrl .= $queryParamsString;
        $queryResult = $solrHelper->doRequest( $solrQueryUrl );

        return $this->parseQueryResult( $queryResult, $solrQueryUrl );
    }

    private function parseQueryResult( $queryResult, $requestUrl = null ) {

        $this->responseStart = time();
        $r = $queryResult['response'];

        // do same parsing and preparing here
        $meta = array(
            'numFound' => 0,
            'solrFound' => $r['numFound'],
            'request_url' => $requestUrl,
            'docsCount' => count( $r['docs'] )
        );

        if (!$r['numFound']) {
            return array( 'meta' => $meta );
        }

        if ($requestUrl) {
            $meta['request_url'] = $requestUrl;
        }

        $resultSkus = array();

        foreach ($r['docs'] as $item) {
            $resultSkus[] = $item['sku'];
        }

        $results = $this->getProductsResultRecursive( $resultSkus );

        $meta['numFound'] = count( $results );

        return array(
            'meta' => $meta,
            'items' => $results
        );
    }

    protected $loadedProducts = array();

    protected function getProductsResultRecursive( $skus ) {
        if (!is_array( $skus )) {
            $skus = array( $skus );
        }
        $skus = array_unique( $skus );

        $collection = Mage::getModel( 'catalog/product' )->getCollection()
            ->addAttributeToSelect( "*" )
            ->addAttributeToFilter( 'sku', array( 'in' => $skus ) );

        $productResults = $this->getProductResults( $collection );

        $result = $productResults->getResults();
        $spareParts = $productResults->getSpareparts();
        foreach ($spareParts as $sparePart) {
            $result = array_merge( $result, $this->getSparePartProducts( $sparePart, 1 ) );
        }

        return $result;
    }

    protected function getSparePartProducts( $sparepart, $level, $max_level = 5 ) {
        $result = array();
        if ($level < $max_level) {
            if (!$this->isMaxTimeReached()) {
                //Collect possible parents
                if ($sparepart && $sparepart->getId()) {
                    //get parent products of current sparepart
                    $potentialParents = Mage::getModel( 'catalog/product' )->getCollection()->addAttributeToSelect( '*' );
                    if (!empty( $this->loadedProducts )) {
                        $potentialParents->addIdFilter( array_keys( $this->loadedProducts ), true );
                    }
                    $potentialParents->addAttributeToFilter( 'parts', array( 'like' => "%" . $sparepart->getSku() . "%" ) );
                    $sparePartsResult = $this->getProductResults( $potentialParents );
                    $productResults = $sparePartsResult->getResults();
                    //Add Child Sparepart to parent
                    foreach (array_keys( $productResults ) as $key) {
                        $productResults[$key]['sparepart'] = $sparepart->getSku();
                    }
                    $result += $productResults;
                    $parentSpareParts = $sparePartsResult->getSpareparts();
                    //Work you way up until there are either no spareparts left or max_level is reached
                    if ($parentSpareParts && !empty( $parentSpareParts )) {
                        foreach ($parentSpareParts as $spart) {
                            $result = array_merge( $result, $this->getSparePartProducts( $spart, $level + 1 ) );
                        }
                    }
                }
            }
            else {
               // Mage::log(__METHOD__ . " max response time reached!");
            }
        }

        return $result;
    }

    protected function getProductResults( $collection ) {
        $results = array();
        $spareParts = array();
        foreach ($collection as $product) {
if (!array_key_exists( $product->getId(), $this->loadedProducts )) {

                    $identNo = $product->getIdentNo();
                    if($identNo == null){
                        $identNo = 0;
                    }
                    $results[] = array(
                        'name' => $product->getName(),
                        'sku' => $product->getSku(),
                        'url' => $this->getProductUrl( $product ),
                        'manufacturer' => $product->getManufacturerName(),
                        'edataid' => $product->getEdataId(),
                        'image_url' => Mage::helper('isc_parser/data')->getAssetUrl($product->getProductPicture(), 75),
                        'tiny_image_url' => Mage::helper('isc_parser/data')->getAssetUrl($product->getProductPicture(), 30),
                        'item_type' => $product->getTpgValue(),
                        'item_name' => $product->getItemName(),
                        'product_no' => $product->getItemNo(),
                        'product_ident' => $identNo
                    );
                    $this->loadedProducts[$product->getId()] = $product;
            }
        }
        return new Varien_Object( array( "results" => $results, "spareparts" => $spareParts ) );
    }

    private $currentCategoryIds = null;

    /**
     * @return array
     */
    private function getCurrentCategoryIds(){
        if (!$this->currentCategoryIds) {
            $this->currentCategoryIds = Mage::getModel("catalog/category")->load(Mage::app()->getStore()->getRootCategoryId())->getAllChildren(true);
        }
        return $this->currentCategoryIds;
    }

    private function getProductUrl( Mage_Catalog_Model_Product $product ) {
        if (!$product->getCategoryId()) {
            $categoryIds = $product->getCategoryIds();
            $currentCategories = $this->getCurrentCategoryIds();
            if (count( $categoryIds )) {
                foreach ($categoryIds as $categoryId) {
                    if (in_array( $categoryId, $currentCategories )) {
                        $product->setData( "category_id", $categoryId );
                        break;
                    }
                }
            }
        }
        $product->setData( 'url', null );
        $product->setData( 'request_path', null );
        $url = $product->getProductUrl();

        return $url;
    }

    private function isMaxTimeReached() {
        if(!$this->responseStart){
            $this->responseStart = time();
        }
        return time() >= ($this->responseStart + self::MAX_RESPONSE_TIME);
    }


}