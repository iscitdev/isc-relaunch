<?php

class Cyberhouse_SolrSearch_IndexController extends Mage_Core_Controller_Front_Action {

	public function indexAction() {

		$this->loadLayout();

		$crumbs = $this->getLayout()->getBlock('breadcrumbs');

		$crumbs->addCrumb('search', array(
			'label' => $this->__('Search'),
			'title' => 'Search',
			'link' => Mage::getUrl('search')
		));

		$this->renderLayout();
	}

	public function ajaxAction() {
		$query = $this->getRequest()->getParam('q', false);
		$results = array('numFound' => 0);

		if ($query !== false) {
            if( strlen($query) > Mage::helper("cyberhouse_solrsearch")->getMinQueryLenght()){
                $results['meta']['info'] = $this->__('Please enter at least %s characters.',Mage::helper("cyberhouse_solrsearch")->getMinQueryLenght());
            }
            $solr = Mage::getModel('cyberhouse_solrsearch/search');
            $query = Mage::helper("cyberhouse_solrsearch")->prepareQueryParam($query);

            $extra = array();
			$rows = $this->getRequest()->getParam('rows', false);
			if (is_numeric($rows) && $rows > 0 && $rows < 100) {
				$solr->setRows($rows);
			}

			$page = $this->getRequest()->getParam('page', 1);
			if (is_numeric($page) && $page > 0) {
				$solr->setPage($page);
			}

			$results = $solr->query(urlencode($query),$extra);
			$this->getResponse()->setHeader('Content-type', 'application/json');
		}

		$this->getResponse()->setBody(json_encode($results));
	}
}