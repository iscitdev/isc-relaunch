<?php

class Cyberhouse_Updater_Helper_Data extends Mage_Core_Helper_Abstract
{

    protected  $shipping_helper, $sales_helper;

    public function __construct(){
        $this->shipping_helper = Mage::helper( "shipping" );
        $this->sales_helper = Mage::helper("sales");
    }

    public function getTrackingLink ( $order )
    {
        if ($order->hasTrackingUrl()) {
            $title = $this->sales_helper->__( 'Track this shipment' );
            $output = '<a target="_blank" href="' . $order->getTrackingUrl() . '" title="' . $title . '">' . $title . '</a>';
        } else {
            $output = $this->shipping_helper->__( 'Tracking information is currently unavailable.' );
        }
        return '<div class="tracking_url">' . $output . '</div>';
    }

}
	 