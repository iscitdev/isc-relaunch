<?php

class Cyberhouse_Updater_Model_Cron {

	protected $_logfile;

	public function __construct() {
		// get logfile name from model
		$this->_logfile = Cyberhouse_Updater_Model_Updater::$logfile;
	}

	public function update() {

		// get message from queue
		$response = Mage::helper('cyberhouse_queue/data')->getDataFromQueue();

		if ($response !== false) {
			// process message
			$message = $response->body;
			$xml = simplexml_load_string($message);

			$updater = Mage::getModel('cyberhouse_updater/updater');

			// detect message type
			$type = $xml->getName(); // retrieve root node name

			switch ($type) {
				case 'place_order_response':
					$updater->handlePlaceOrderResponse($xml);
					break;
				case 'return_order_response':
					$updater->handleReturnOrderResponse($xml);
					break;
				default:
			}

			// re-run to get next message
			$this->update();
		} else {
		}
	}
}