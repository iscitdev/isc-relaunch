<?php

class Cyberhouse_Updater_Model_Updater extends Mage_Core_Model_Abstract {

	public static $logfile = 'statusupdater.log';
    protected $notify_user = true;

	public function handlePlaceOrderResponse($message) {

		$orderId = $message->ERP_ORDER_ID;
        /** @var Mage_Sales_Model_Order $order */
		$order = Mage::getModel('sales/order')->loadByIncrementId($orderId);
		if (!$order->getId()) {
			Mage::log('Status updater: order '.$orderId. ' not found', null, self::$logfile);
            return;
		}

        $order->setData('last_order_response', $message);

        if (isset( $message->TRACKING_URL )) {
            $order->setData('tracking_url', $message->TRACKING_URL);
        }

		$orderStatusId = $message->ORDER_STATUS_ID;

		Mage::log('Set status for order '.$orderId.' to '.$orderStatusId, null, self::$logfile);

		switch ($orderStatusId) {
			case 10:
			case 20:
			case 30:
				$order->setState(Mage_Sales_Model_Order::STATE_NEW, true)->save();
				break;
			case 40:
				$order->setState(Mage_Sales_Model_Order::STATE_PROCESSING, true)->save();
				break;
			case 50:
                $order->setData('state', Mage_Sales_Model_Order::STATE_COMPLETE);
                $order->setStatus(Mage_Sales_Model_Order::STATE_COMPLETE);
                $comment = 'Order was set to Complete by our automation tool.';
                $history = $order->addStatusHistoryComment($comment, false);
                $history->setIsCustomerNotified($this->notify_user);
                $order->save();
                $order->sendOrderUpdateEmail($this->notify_user, $comment);
				break;
			case 60:
				$order->setState(Mage_Sales_Model_Order::STATE_CANCELED, true)->save();
				break;
            case 80:
                $order->setState(Mage_Sales_Model_Order::STATE_NEW, "on_hold")->save();
                break;
			default:
				Mage::log('Unknown status code found. Order: '.$orderId.' Status number: '.$orderStatusId, null, self::$logfile);
		}

		if (isset($message->SHIPMENT_NO)) {
			// TODO: do stuff with shipment no
		}

		if (isset($message->TRACKING_ID)) {
			// TODO: do stuff with tracking id
		}

		if (isset($message->TRANSPORT_COMPANY_ID)) {
			// TODO: do stuff with transport company id
		}
	}

	public function handleReturnOrderResponse($message) {
		// not needed
	}
}