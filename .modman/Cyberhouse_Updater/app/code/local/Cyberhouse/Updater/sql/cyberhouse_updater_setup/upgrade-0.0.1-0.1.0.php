<?php

$installer = new Mage_Sales_Model_Resource_Setup('core_setup');

$installer->startSetup();

$installer->addAttribute('order', 'tracking_url', array('type'=>'varchar'));
$installer->addAttribute('order', 'last_order_response', array('type'=>'text', 'visible'=>false));

$installer->endSetup();