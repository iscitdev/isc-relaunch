<?php

/**
 * Created by PhpStorm.
 * User: rhutterer
 * Date: 14.05.14
 * Time: 09:33
 */
require 'abstract.php';
class Cyberhouse_Updater_Shell_Updater extends Mage_Shell_Abstract
{

    public function run()
    {
        $message = '<place_order_response>
<ERP_ORDER_ID>EW00012409</ERP_ORDER_ID>
<ORDER_STATUS_ID>50</ORDER_STATUS_ID>
<SHIPMENT_NO>LG2062238</SHIPMENT_NO>
<TRACKING_ID/>
<TRANSPORT_COMPANY_ID/>
</place_order_response>';


        // process message
        $xml = simplexml_load_string($message);


        $updater = Mage::getModel('cyberhouse_updater/updater');

        // detect message type
        $type = $xml->getName(); // retrieve root node name

        switch ($type) {
            case 'place_order_response':
                $updater->handlePlaceOrderResponse($xml);
                break;
            case 'return_order_response':
                $updater->handleReturnOrderResponse($xml);
                break;
            default:
        }


    }
}

$shell = new Cyberhouse_Updater_Shell_Updater();
$shell->run();