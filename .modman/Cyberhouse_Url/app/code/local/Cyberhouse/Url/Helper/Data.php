<?php
/**
 * Created by JetBrains PhpStorm.
 * User: ssc
 * Date: 8/23/13
 * Time: 12:05 PM
 * To change this template use File | Settings | File Templates.
 */
class Cyberhouse_Url_Helper_Data extends Mage_Core_Helper_Abstract {

    const XML_PATH_ASSET_BASE_URL = 'iscconfig_options/asset_settings/asset_base_url';
    const XML_PATH_IMAGE_FACTORY_PATH = 'iscconfig_options/asset_settings/asset_if_path';

	// example structure: https://assets.einhell.com/im/imf/300/282_887/
	protected $assetUrlBase;
	protected $assetUrlImageFactoryPath;
	protected $fileExtension = '.jpg';

	/**
	 * Get current manufacturer from url
	 * @return Cyberhouse_Manufacturer_Model_Manufacturer
	 */
	public function getManufacturer() {
//		return Mage::helper("manufacturer")->getCurrentManufacturer();
	}

	/**
	 * Generate complete asset url from asset id
	 * @param string $assetId
	 * @param int $size
	 * @param bool $square
	 * @return string
	 */
	public function getAssetUrl($assetId, $size = 300, $square = false) {
        if (!$assetId) {
			return "";
		}

		$url = $this->getImageFactoryAbsolutePath();

		if ($square) {
			$size = 'x' . $size . ',y' .$size;
		}

		$url .= $size.'/'.$assetId.'/';
		return $url;
	}

	public function getDownloadAssetUrl($assetId) {
		return $this->getAssetUrl($assetId, 'none');
	}


	public function getExplosionDrawingUrl($assetId, $width = 800) {
		return $this->getAssetUrl($assetId,$width).$assetId.$this->fileExtension;
	}


    public function getImageFactoryAbsolutePath(){
        return $this->getAssetBaseUrl().$this->getImageFactoryPath();
    }

    public function getAssetBaseUrl() {
        if (!$this->assetUrlBase) {
            $this->assetUrlBase =  Mage::getStoreConfig(self::XML_PATH_ASSET_BASE_URL);
        }
        return $this->assetUrlBase;
    }

    public function getImageFactoryPath(){
        if (!$this->assetUrlImageFactoryPath) {
            $this->assetUrlImageFactoryPath = Mage::getStoreConfig(self::XML_PATH_IMAGE_FACTORY_PATH);
        }
        return $this->assetUrlImageFactoryPath;
    }

}