<?php

/**
 * Created by JetBrains PhpStorm.
 * User: ssc
 * Date: 8/23/13
 * Time: 12:05 PM
 * To change this template use File | Settings | File Templates.
 */
class Cyberhouse_Url_Model_Catalog_Category extends Mage_Catalog_Model_Category
{

    /**
     * Get category url
     *
     * @return string
     */
    public function getUrl() {
        $url = parent::getUrl();
        return $url;
    }


}