<?php
class Cyberhouse_ValidateVatOnRegister_Helper_Data extends Mage_Core_Helper_Abstract
{

    const VAT_PROCESSED_FLAG = "vat_processed_flat";

    public function registerFlag($value = true, $graceful = true){
        Mage::register(self::VAT_PROCESSED_FLAG, $value, $graceful);
    }

    public function isVatSuccess($clear = false){
        $result = Mage::registry(self::VAT_PROCESSED_FLAG);
        if ( $clear ) {
            $this->registerFlag(false,true);
        }
        return $result;
    }
}
	 