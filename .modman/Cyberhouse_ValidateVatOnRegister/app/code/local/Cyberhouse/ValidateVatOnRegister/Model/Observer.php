<?php

class Cyberhouse_ValidateVatOnRegister_Model_Observer{
    /**
     * Adopts the specifications of an attribute after load
     * @param $observer
     * @Event customer_entity_attribute_load_after
     */
    public function applyVat($observer)
    {
        $attribute = $observer->getEvent()->getAttribute();
        $this->setVatRequired($attribute);
    }

    /**
     * Adopts the specifications of each attribute within a attribute_collection (e.g. form)
     * @param $observer
     * @return $this
     * @Event core_collection_abstract_load_after
     */
    public function applyToCollection($observer)
    {
        $collection = $observer->getEvent()->getCollection();
        if ($collection instanceof Mage_Customer_Model_Resource_Form_Attribute_Collection) {
            foreach($collection as $attribute){
                $this->setVatRequired($attribute);
            }
        }
        return $this;
    }

    protected function setVatRequired($attribute){
        if($attribute->getAttributeCode() == "taxvat"){
            if(Mage::getStoreConfig("customer/address/taxvat_show") == "req"){
                $attribute
                    ->setIsVisible(true)
                    ->setIsRequired(true)
                    ->setScopeIsVisible(true)
                    ->setScopeIsRequired(true);
            }
            if(Mage::getStoreConfig("customer/address/taxvat_show") != ""){
                $attribute->setDataModel("register_vat/vat");
            }
        }
    }

    public function setCustomerGroup($observer){
        try {
            $customer = $observer->getEvent()->getCustomer();
            /** @var $customerHelper Mage_Customer_Helper_Data */
            $customerHelper = Mage::helper('customer');
            $customerAddress = Mage::getModel("customer/address")->setCountry(Mage::getStoreConfig("general/country/default",$customer->getStore()));
            if ($customer->getTaxvat() == ''
                || !Mage::helper('core')->isCountryInEU($customerAddress->getCountry()))
            {
                $defaultGroupId = $customerHelper->getDefaultCustomerGroupId($customer->getStore());

                if (!$customer->getDisableAutoGroupChange() && $customer->getGroupId() != $defaultGroupId) {
                    $customer->setGroupId($defaultGroupId);
                    $customer->save();
                }
            } else {
                $result = Mage::getSingleton("customer/session")->getData("vat_validation_result");
                if($result && $result->getRequestSuccess()){
                    $newGroupId = $customerHelper->getCustomerGroupIdBasedOnVatNumber(
                        $customerAddress->getCountry(), $result, $customer->getStore()
                    );

                    if (!$customer->getDisableAutoGroupChange() && $customer->getGroupId() != $newGroupId) {
                        $customer->setGroupId($newGroupId);
                        $customer->save();
                    }
                }

                return true;
            }
        } catch (Exception $e) {
            return false;
        }
    }

}
