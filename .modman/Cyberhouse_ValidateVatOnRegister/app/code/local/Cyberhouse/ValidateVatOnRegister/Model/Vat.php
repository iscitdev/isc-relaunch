<?php

class Cyberhouse_ValidateVatOnRegister_Model_Vat extends Mage_Eav_Model_Attribute_Data_Text
{

    public function validateValue( $value ) {
        $errors = parent::validateValue( $value );
        if(Mage::app()->getFrontController()->getAction()->getFullActionName() == "customer_account_createpost"){
            if ($errors === true && $value && !Mage::app()->getStore()->isAdmin()) {
                $country = Mage::getStoreConfig( "general/country/default" );
                $possible_country_code = substr( $value, 0, 2 );
                if (Mage::helper( 'core' )->isCountryInEU( $possible_country_code )) {
                    $possible_country = Mage::getModel("directory/country")->loadByCode($possible_country_code);
                    if ($possible_country && $possible_country->getId() ) {
                        $country = $possible_country_code;
                    }
                }
                $result = Mage::helper( "customer" )->checkVatNumber( $country, $value );
                Mage::getSingleton( "customer/session" )->setData( "vat_validation_result", $result );
                if (!$result->getIsValid()) {
                    $errors = array( Mage::helper( "customer" )->__( 'The VAT ID entered (%s) is not a valid VAT ID.', $value ) );
                } else {
                    Mage::helper( "validatevatonregister" )->registerFlag( true );
                }
            }
        }
        return $errors;
    }


}