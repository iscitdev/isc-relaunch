<?php

class Cyberhouse_Vat_Helper_Data extends Mage_Customer_Helper_Data
{

    const VAT_VALIDATION_WSDL_URL = 'http://ec.europa.eu/taxation_customs/vies/checkVatService.wsdl';


    public function checkVatNumber ( $countryCode, $vatNumber, $requesterCountryCode = '', $requesterVatNumber = '' )
    {
        if ($this->includesCountryCode( $vatNumber )) {
            $vatNumber = substr( str_replace( ' ', '', trim( $vatNumber ) ), 2 );
        }
        if ($this->includesCountryCode( $requesterVatNumber )) {
            $requesterVatNumber = substr( str_replace( ' ', '', trim( $requesterVatNumber ) ), 2 );
        }
        $cacheKey = "VAT_" . $countryCode . "_" . $vatNumber;
        $cachedValidation = Mage::app()->getCache()->load( $cacheKey );
        if (!$cachedValidation) {
            $validationResult = $this->doValidation( $countryCode, $vatNumber, $requesterCountryCode, $requesterVatNumber );
            if($validationResult->getRequestDate() !== ''){
                Mage::app()->getCache()->save( serialize( $validationResult ), $cacheKey, array( "CONFIG" ), 60 * 60 );
            }
            else {
                //Notify that the tax service reported an error
            }
            return $validationResult;
        }
        return unserialize( $cachedValidation );
    }

    protected function doValidation ( $countryCode, $vatNumber, $requesterCountryCode, $requesterVatNumber )
    {
        $oldSocketTimeout = ini_get( 'default_socket_timeout' );
        ini_set( 'default_socket_timeout', $this->_getSoapTimeout() );
        $validationResult = parent::checkVatNumber( $countryCode, $vatNumber, $requesterCountryCode, $requesterVatNumber );
        ini_set( 'default_socket_timeout', $oldSocketTimeout );
        return $validationResult;
    }

    public function getCustomer ()
    {
        if (Mage::registry( 'current_customer' )) //Adminhtml or Register
            return Mage::registry( 'current_customer' );
        else //Frontend
            return parent::getCustomer();
    }

    protected function _getSoapTimeout ()
    {
        return 15;
    }

    /**
     * Create SOAP client based on VAT validation service WSDL
     *
     * @param boolean $trace
     * @return SoapClient
     */
    protected function _createVatNumberValidationSoapClient ( $trace = false )
    {
        return new SoapClient( self::VAT_VALIDATION_WSDL_URL, array( 'trace' => $trace, "connection_timeout" => $this->_getSoapTimeout() ) );
    }

    /**
     * parse the VAT if it includes the countrycode
     * @param string $vatNumber
     * @return boolean
     */
    public function includesCountryCode ( $vatNumber )
    {
        $countryCode = substr( str_replace( ' ', '', trim( $vatNumber ) ), 0, 2 );
        if (array_key_exists( strtoupper( $countryCode ), Mage::helper( 'cyberhouse_vat/countries' )->getCountries() )) {
            return true;
        }
        return false;
    }
}