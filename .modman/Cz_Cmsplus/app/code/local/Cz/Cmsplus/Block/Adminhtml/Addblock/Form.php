<?php
/**
 * clever + zöger gmbh
 * http://www.clever-zoeger.de
 * @author Ronny Hempel <rhempel@clever-zoeger.de>
 * @version $Id: Form.php 266 2011-11-21 14:14:51Z rhempel $
 * $HeadURL: https://svn.clever-zoeger.de/svn/Magento/extensions/Cz_Cmsplus/tags/1.3.0/cmsPlus/app/code/local/Cz/Cmsplus/Block/Adminhtml/Addblock/Form.php $
 * $LastChangedBy: rhempel $
 * $LastChangedRevision: 266 $
 * $LastChangedDate: 2011-11-21 15:14:51 +0100 (Mon, 21 Nov 2011) $
 */
?>
<?php

class Cz_Cmsplus_Block_Adminhtml_Addblock_Form extends Mage_Adminhtml_Block_Widget_Form
{

    protected $_additionalButtons = array();

    public function __construct()
    {
        parent::__construct();
        $this->setTemplate('cz/cmsplus/addmenuform.phtml');
    }

    protected function _prepareLayout()
    {

        $this->setChild('tabs',
            $this->getLayout()->createBlock('cmsplus/adminhtml_addblock_tabs', 'tabs')
        );

			$params = $this->getRequest()->getParams();
			
			$_params = array();
			$_params['_current'] = false;
			
			$_params['store'] = Mage::helper('cmsplus')->getStoreId();
		
			if (isset($params) && isset($params['menuid'])) {
				$_params['menuid'] = $params['menuid'];
			}
			
			if (isset($params) && isset($params['pageid'])) {
				$_params['pageid'] = $params['pageid'];
			}
			
			$this->setChild('back_button',
                $this->getLayout()->createBlock('adminhtml/widget_button')
                    ->setData(array(
                        'label'     => Mage::helper('cmsplus')->__('Back'),
                        'onclick'   => "cmsplus.doAction('".$this->getUrl('*/*/addPage',$_params)."',true)",
                        'class' => 'back'
                    ))
            );
			if (Mage::helper('cmsplus')->isEditor()) {
				return parent::_prepareLayout();
			}
			
			$this->setChild('save_button',
                $this->getLayout()->createBlock('adminhtml/widget_button')
                    ->setData(array(
                        'label'     => Mage::helper('cmsplus')->__('Block Save'),
                        'onclick'   => "cmsplus.doSave('".$this->getUrl('*/*/saveBlock',$_params)."',true)",
                        'class' => 'save'
                    ))
            );
  
       
            $this->setChild('delete_button',
                $this->getLayout()->createBlock('adminhtml/widget_button')
                    ->setData(array(
                        'label'     => Mage::helper('cmsplus')->__('Block Delete'),
                        'onclick'   => "cmsplus.doAction('".$this->getUrl('*/*/deleteBlock', $_params)."',true)",
                        'class' => 'delete'
                    ))
            );
            
		if (isset($_params['pageid'])) {
			$page = Mage::getModel('cmsplus/cz_cms_content_page')->load($_params['pageid']);
			$hash = sha1(microtime().$page->getId().$page->getName());
			$page->setPreviewkey($hash);
			$page->save();
			$_params['preview'] = 1;
			$storeId = Mage::helper('cmsplus')->getStoreId();
			$_storeCode = '';
			if ($storeId != 0) {
				$store=Mage::getModel('core/store')->load($storeId);
				$_storeCode = '&___store='.$store->getCode();
			}
			$this->setChild('preview_button',
				$this->getLayout()->createBlock('adminhtml/widget_button')
					->setData(array(
			    		'label'     => Mage::helper('cmsplus')->__('Page Preview'),
						'onclick'   => "cmsplus.doPopup('".$page->getUrl()."?preview=1&hash=".$hash.$_storeCode."',true)",
						'class' => 'preview'
			        ))
			);
		}

        
            
       
        return parent::_prepareLayout();
    }

	public function getBackButtonHtml() {
		return $this->getChildHtml('back_button');
	}
    public function getDeleteButtonHtml()
    {
        return $this->getChildHtml('delete_button');
    }

    public function getSaveButtonHtml()
    {

          return $this->getChildHtml('save_button');
    }
	public function getPreviewButtonHtml() {
		return $this->getChildHtml('preview_button');
	}


    public function getTabsHtml()
    {
        return $this->getChildHtml('tabs');
    }

    public function getHeader()
    {
    	if (Mage::helper('cmsplus')->getAction() == 'blockedit') {
    		$id = Mage::helper('cmsplus')->getLastid();
			$block = Mage::getModel('cmsplus/cz_cms_content_block')->load($id);
    		return Mage::helper('cmsplus')->__('Edit Block ' .$block->getName().'(ID:'.Mage::helper('cmsplus')->getLastid().')');
		} else {
        	return Mage::helper('cmsplus')->__('Add Block');
		}
    }

    public function getDeleteUrl(array $args = array())
    {
        $params = array('_current'=>true);
        $params = array_merge($params, $args);
        return $this->getUrl('*/*/delete', $params);
    }

}
