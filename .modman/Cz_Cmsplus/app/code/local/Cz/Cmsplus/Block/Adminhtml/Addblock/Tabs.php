<?php
/**
 * clever + zöger gmbh
 * http://www.clever-zoeger.de
 * @author Ronny Hempel <rhempel@clever-zoeger.de>
 * @version $Id: Tabs.php 447 2012-07-11 11:23:46Z rhempel $
 * $HeadURL: https://svn.clever-zoeger.de/svn/Magento/extensions/Cz_Cmsplus/tags/1.3.0/cmsPlus/app/code/local/Cz/Cmsplus/Block/Adminhtml/Addblock/Tabs.php $
 * $LastChangedBy: rhempel $
 * $LastChangedRevision: 447 $
 * $LastChangedDate: 2012-07-11 13:23:46 +0200 (Wed, 11 Jul 2012) $
 */
?>
<?php

class Cz_Cmsplus_Block_Adminhtml_Addblock_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{

    /**
     * Initialize Tabs
     *
     */
    public function __construct()
    {
        parent::__construct();
        //$this->setId('cmsplus_info_tabs');
		$this->setId('category_info_tabs');
        $this->setDestElementId('cmsplus_tab_content');
        $this->setTitle(Mage::helper('cmsplus')->__('Block Data'));
        $this->setTemplate('cz/cmsplus/tabshoriz.phtml');
    }

    /**
     * Prepare Layout Content
     *
     * @return Mage_Adminhtml_Block_Catalog_Category_Tabs
     */
    protected function _prepareLayout()
    {
		$active = 1;
		if (Mage::helper('cmsplus')->getAction() == 'blockedit') {
			$block = $this->getLayout()->createBlock('cmsplus/adminhtml_addblock_tabs_view')->toHtml();
	        $this->addTab('view', array(
	            'label'     => Mage::helper('cmsplus')->__('View'),
	            'content'   => $block,
	            'active'    => $active
	        ));
		}

		if (Mage::helper('cmsplus')->isEditor()) {
			return parent::_prepareLayout();
		}
		$block = $this->getLayout()->createBlock('cmsplus/adminhtml_addblock_tabs_settings')->toHtml();

		$this->addTab('config', array(
            'label'     => Mage::helper('cmsplus')->__('Display Settings'),
            'content'   => $block,
            'active'    => $active
        ));
		
		if (Mage::helper('cmsplus')->getAction() == 'blockedit') {
			$this->addTab('access', array(
				'label'     => Mage::helper('cmsplus')->__('Access'),
				'content'   => $this->getLayout()->createBlock('cmsplus/adminhtml_addblock_tabs_access')->toHtml(),
				'active'	=>	false,
			));
		}

        return parent::_prepareLayout();
    }
}
