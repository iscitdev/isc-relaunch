<?php
/**
 * clever + zöger gmbh
 * http://www.clever-zoeger.de
 * @author Ronny Hempel <rhempel@clever-zoeger.de>
 * @version $Id: Access.php 268 2011-11-21 14:24:24Z rhempel $
 * $HeadURL: https://svn.clever-zoeger.de/Magento/extensions/Cz_Cmsplus/trunk/cmsPlus/app/code/local/Cz/Cmsplus/Block/Adminhtml/Addpage/Tabs/Access.php $
 * $LastChangedBy: rhempel $
 * $LastChangedRevision: 268 $
 * $LastChangedDate: 2011-11-21 15:24:24 +0100 (Mon, 21 Nov 2011) $
 */
?>
<?php
class Cz_Cmsplus_Block_Adminhtml_Addblock_Tabs_Access extends Cz_Cmsplus_Block_Adminhtml_Formdefault
{

	
  protected function _prepareForm() {
	$form = new Varien_Data_Form();
	$this->setForm($form);
	$fieldset = $form->addFieldset('main_form', array('legend'=>Mage::helper('cmsplus')->__('Access')));
	
	$this->_setFieldset(array(), $fieldset);
	
	$params = $this->getRequest()->getParams();
	if (!isset($params['menuid'])) {
		$params['menuid'] = 0;
	}
	if (!isset($params['pageid'])) {
		$params['pageid'] = 0;
	}
	if (!isset($params['subpageid'])) {
		$params['subpageid'] = 0;
	}
	$_hidden = '';
	$block = false;
	if (Mage::helper('cmsplus')->getAction() == 'blockedit') {
		$_id = Mage::helper('cmsplus')->getLastid();
		$block = Mage::getModel('cmsplus/cz_cms_content_block')->load($_id);
		
		$_data = $block->getData();

		$access = false;
		if ( $access = unserialize($_data['access'])) {
			$_data['accessview'] = $access['view'];
			$_data['accessedit'] = $access['viewedit'];
		} else {
			//old
			$_data['accessview'] = explode(',',$_data['access']);
			$_data['accessedit'] = explode(',',$_data['access']);
		}

		$fieldset->addField('accessview', 'multiselect', array(
			'label'     => Mage::helper('cmsplus')->__('Access View'),
			'class'     => '',
			'required'  => false,
			'name'      => 'settings[access][view]',
			'values'	=>	Mage::getModel('cmsplus/system_config_source_adminrole')->toOptionArray(true),
		));
		$fieldset->addField('accessedit', 'multiselect', array(
			'label'     => Mage::helper('cmsplus')->__('Access View/Edit'),
			'class'     => '',
			'required'  => false,
			'name'      => 'settings[access][viewedit]',
			'values'	=>	Mage::getModel('cmsplus/system_config_source_adminrole')->toOptionArray(true),
		));
	}
	
	
	if ($block) {
		$form->setValues($_data);
	}
      return parent::_prepareForm();
  }


}