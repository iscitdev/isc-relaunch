<?php
/**
 * clever + zöger gmbh
 * http://www.clever-zoeger.de
 * @author Ronny Hempel <rhempel@clever-zoeger.de>
 * @version $Id: View.php 447 2012-07-11 11:23:46Z rhempel $
 * $HeadURL: https://svn.clever-zoeger.de/svn/Magento/extensions/Cz_Cmsplus/tags/1.3.0/cmsPlus/app/code/local/Cz/Cmsplus/Block/Adminhtml/Addblock/Tabs/View.php $
 * $LastChangedBy: rhempel $
 * $LastChangedRevision: 447 $
 * $LastChangedDate: 2012-07-11 13:23:46 +0200 (Wed, 11 Jul 2012) $
 */
?>
<?php
class Cz_Cmsplus_Block_Adminhtml_Addblock_Tabs_View extends Cz_Cmsplus_Block_Adminhtml_Formdefault
{
  protected function _prepareForm()
  {
	$form = new Varien_Data_Form();
	$this->setForm($form);
	
	$params = $this->getRequest()->getParams();
	
	$params['blockid'] = Mage::helper('cmsplus')->getLastid();
	$block = Mage::getModel('cmsplus/cz_cms_content_block')->load($params['blockid']);
	//check Access
	if (Mage::helper('cmsplus')->hasAccessBlock($block)) {
		$fieldset = $form->addFieldset('main_form', array(
			'legend'=>Mage::helper('cmsplus')->__('View'),
			'class'	=>	'blocktype'.$block->getData('cz_cms_type_idcz_cms_content_block_type'),
		));
		$this->_setFieldset(array(), $fieldset);
		
		
		
	
		
		$columns = $block->getData('cz_cms_type_idcz_cms_content_block_type');
		
		$elements = Mage::getSingleton('cmsplus/cz_cms_content_element')->getCollection()->loadByBlockId($block->getId());
	
		foreach ($elements AS $element) {
			$fieldset->addField('addblock_'.$element->getId(), 'elementcmsplus', array(
				'class'     => '',
				'required'  => false,
				'element'		=>	$element,
				'block'		=>	$block,
			));
		}
		if (Mage::helper('cmsplus')->isEditor()) {
			return parent::_prepareForm();
		}
		$fieldset->addField('changeblocktype', 'buttoncmsplus', array(
			/*'label'     => Mage::helper('cmsplus')->__('Add Block'),*/
			'class'     => 'change btn-change',
			'required'  => false,
			'name'      => 'changeblocktype',
			'onclick'	=>	'jQuery(\'#cmsplus_info_tabs_config\').click(); return false;',
			'value'		=>	Mage::helper('cmsplus')->__('change Block Type'),
		));
	}
	


      return parent::_prepareForm();
  }
}