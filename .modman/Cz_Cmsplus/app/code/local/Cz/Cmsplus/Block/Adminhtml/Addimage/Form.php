<?php
/**
 * clever + zöger gmbh
 * http://www.clever-zoeger.de
 * @author Ronny Hempel <rhempel@clever-zoeger.de>
 * @version $Id: Form.php 181 2011-09-12 14:37:18Z rhempel $
 * $HeadURL: https://svn.clever-zoeger.de/svn/Magento/extensions/Cz_Cmsplus/tags/1.3.0/cmsPlus/app/code/local/Cz/Cmsplus/Block/Adminhtml/Addimage/Form.php $
 * $LastChangedBy: rhempel $
 * $LastChangedRevision: 181 $
 * $LastChangedDate: 2011-09-12 16:37:18 +0200 (Mon, 12 Sep 2011) $
 */
?>
<?php

class Cz_Cmsplus_Block_Adminhtml_Addimage_Form extends Mage_Adminhtml_Block_Widget_Form
{

    protected $_additionalButtons = array();

    public function __construct()
    {
        parent::__construct();
        $this->setTemplate('cz/cmsplus/addmenuform.phtml');
    }

    protected function _prepareLayout()
    {

        $this->setChild('tabs',
            $this->getLayout()->createBlock('cmsplus/adminhtml_addimage_tabs', 'tabs')
        );

			$params = $this->getRequest()->getParams();
			
			$_params = array();
			$_params['_current'] = false;
			
			$_params['store'] = Mage::helper('cmsplus')->getStoreId();
		
			if (isset($params) && isset($params['menuid'])) {
				$_params['menuid'] = $params['menuid'];
			}
			
			if (isset($params) && isset($params['pageid'])) {
				$_params['pageid'] = $params['pageid'];
			}

			$this->setChild('save_button',
                $this->getLayout()->createBlock('adminhtml/widget_button')
                    ->setData(array(
                        'label'     => Mage::helper('cmsplus')->__('Image Save'),
                        'onclick'   => "cmsplus.doSave('".$this->getUrl('*/*/saveImage',$_params)."',true)",
                        'class' => 'save'
                    ))
            );
            
			$this->setChild('back_button',
                $this->getLayout()->createBlock('adminhtml/widget_button')
                    ->setData(array(
                        'label'     => Mage::helper('cmsplus')->__('Back'),
                        'onclick'   => "cmsplus.doAction('".$this->getUrl('*/*/addPage',$_params)."',true)",
                        'class' => 'back'
                    ))
            );
  
       
            $this->setChild('delete_button',
                $this->getLayout()->createBlock('adminhtml/widget_button')
                    ->setData(array(
                        'label'     => Mage::helper('cmsplus')->__('Image Delete'),
                        'onclick'   => "cmsplus.doAction('".$this->getUrl('*/*/deleteImage', $_params)."',true)",
                        'class' => 'delete'
                    ))
            );

        
            
       
        return parent::_prepareLayout();
    }

	public function getBackButtonHtml() {
		return $this->getChildHtml('back_button');
	}
    public function getDeleteButtonHtml()
    {
        return $this->getChildHtml('delete_button');
    }

    public function getSaveButtonHtml()
    {

          return $this->getChildHtml('save_button');
    }


    public function getTabsHtml()
    {
        return $this->getChildHtml('tabs');
    }

    public function getHeader()
    {
    	if (Mage::helper('cmsplus')->getAction() == 'imageedit') {
    		return Mage::helper('cmsplus')->__('Edit Image (ID:'.Mage::helper('cmsplus')->getLastid().')');
		} else {
        	return Mage::helper('cmsplus')->__('Add Image');
		}
    }

    public function getDeleteUrl(array $args = array())
    {
        $params = array('_current'=>true);
        $params = array_merge($params, $args);
        return $this->getUrl('*/*/delete', $params);
    }

}
