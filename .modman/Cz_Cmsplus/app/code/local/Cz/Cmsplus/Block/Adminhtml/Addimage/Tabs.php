<?php
/**
 * clever + zöger gmbh
 * http://www.clever-zoeger.de
 * @author Ronny Hempel <rhempel@clever-zoeger.de>
 * @version $Id: Tabs.php 181 2011-09-12 14:37:18Z rhempel $
 * $HeadURL: https://svn.clever-zoeger.de/svn/Magento/extensions/Cz_Cmsplus/tags/1.3.0/cmsPlus/app/code/local/Cz/Cmsplus/Block/Adminhtml/Addimage/Tabs.php $
 * $LastChangedBy: rhempel $
 * $LastChangedRevision: 181 $
 * $LastChangedDate: 2011-09-12 16:37:18 +0200 (Mon, 12 Sep 2011) $
 */
?>
<?php

class Cz_Cmsplus_Block_Adminhtml_Addimage_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{

    /**
     * Initialize Tabs
     *
     */
    public function __construct()
    {
        parent::__construct();
        //$this->setId('cmsplus_info_tabs');
		$this->setId('category_info_tabs');
        $this->setDestElementId('cmsplus_tab_content');
        $this->setTitle(Mage::helper('cmsplus')->__('Image Data'));
        $this->setTemplate('cz/cmsplus/tabshoriz.phtml');
    }

    /**
     * Prepare Layout Content
     *
     * @return Mage_Adminhtml_Block_Catalog_Category_Tabs
     */
    protected function _prepareLayout()
    {
		
		$block = $this->getLayout()->createBlock('cmsplus/adminhtml_addimage_tabs_settings')->toHtml();
		$active = 1;
		$this->addTab('config', array(
            'label'     => Mage::helper('cmsplus')->__('Display Settings'),
            'content'   => $block,
            'active'    => $active
        ));

        return parent::_prepareLayout();
    }
}
