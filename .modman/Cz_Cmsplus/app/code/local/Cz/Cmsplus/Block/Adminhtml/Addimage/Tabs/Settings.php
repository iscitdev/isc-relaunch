<?php
/**
 * clever + zöger gmbh
 * http://www.clever-zoeger.de
 * @author Ronny Hempel <rhempel@clever-zoeger.de>
 * @version $Id: Settings.php 181 2011-09-12 14:37:18Z rhempel $
 * $HeadURL: https://svn.clever-zoeger.de/svn/Magento/extensions/Cz_Cmsplus/tags/1.3.0/cmsPlus/app/code/local/Cz/Cmsplus/Block/Adminhtml/Addimage/Tabs/Settings.php $
 * $LastChangedBy: rhempel $
 * $LastChangedRevision: 181 $
 * $LastChangedDate: 2011-09-12 16:37:18 +0200 (Mon, 12 Sep 2011) $
 */
?>
<?php
class Cz_Cmsplus_Block_Adminhtml_Addimage_Tabs_Settings extends Cz_Cmsplus_Block_Adminhtml_Formdefault
{
  protected function _prepareForm() {
	$form = new Varien_Data_Form();
	$this->setForm($form);
	$fieldset = $form->addFieldset('main_form', array('legend'=>Mage::helper('cmsplus')->__('Settings')));
	
	$this->_setFieldset(array(), $fieldset);
	
	$params = $this->getRequest()->getParams();
	if (!isset($params['menuid'])) {
		$params['menuid'] = 0;
	}
	if (!isset($params['pageid'])) {
		$params['pageid'] = 0;
	}
	if (!isset($params['subpageid'])) {
		$params['subpageid'] = 0;
	}
	$_hidden = '';
	$image = false;
	if (Mage::helper('cmsplus')->getAction() == 'imageedit') {

		$_id = Mage::helper('cmsplus')->getLastid();
		$image = Mage::getModel('cmsplus/cz_cms_content_page_image')->load($_id);
		$_data = $image->getData();

		if ($image->getHidden() == 1) {
			$_hidden = 'checked';
		}
		
		$_data['form_key'] = Mage::getSingleton('core/session')->getFormKey();
		$_data['isnew'] = 0;
		$_data['hidden'] = 1;
		
		$_data['imageid'] = $params['imageid'];
		
		$_data['image'] = $_data['file'];

		
		$fieldset->addField('imageid', 'hidden', array(
			'label'     => '',
			'class'     => '',
			'required'  => false,
			'name'      => 'imageid',
			'value'		=>	$params['imageid'],
		));

	}
	
	if (Mage::helper('cmsplus')->getAction() == 'imageedit') {
		$fieldset->addField('isnew', 'hidden', array(
			'label'     => '',
			'class'     => '',
			'required'  => false,
			'name'      => 'isnew',
			'value'		=>	0,
		));
	} else {
		$fieldset->addField('isnew', 'hidden', array(
			'label'     => '',
			'class'     => '',
			'required'  => false,
			'name'      => 'isnew',
			'value'		=>	1,
		));
	}

	$fieldset->addField('cz_cms_content_page_idcz_cms_content_page', 'hidden', array(
		'label'     => '',
		'class'     => '',
		'required'  => false,
		'name'      => 'settings[cz_cms_content_page_idcz_cms_content_page]',
		'value'		=>	$params['pageid'],
	));
	$fieldset->addField('cz_cms_type_idcz_cms_content_block_type', 'hidden', array(
		'label'     => '',
		'class'     => '',
		'required'  => false,
		'name'      => 'settings[cz_cms_type_idcz_cms_content_block_type]',
		'value'		=>	$params['typeid'],
	));

	$fieldset->addField('image', 'image', array(
		'label'     => Mage::helper('cmsplus')->__('Image'),
		'class'     => 'required-entry',
		'required'  => true,
		'name'      => 'image',
	));
	
	$fieldset->addField('name', 'text', array(
		'label'     => Mage::helper('cmsplus')->__('Name'),
		'class'     => 'required-entry',
		'required'  => true,
		'name'      => 'settings[name]',
	));
	
	$fieldset->addField('title_text', 'text', array(
		'label'     => Mage::helper('cmsplus')->__('title text'),
		'class'     => '',
		'required'  => false,
		'name'      => 'settings[title_text]',
	));
	
	$fieldset->addField('alt_text', 'text', array(
		'label'     => Mage::helper('cmsplus')->__('alt text'),
		'class'     => '',
		'required'  => false,
		'name'      => 'settings[alt_text]',
	));

	
	$fieldset->addField('image_css_class', 'text', array(
		'label'     => Mage::helper('cmsplus')->__('css class'),
		'class'     => '',
		'required'  => false,
		'name'      => 'settings[image_css_class]',
	));
	
	$fieldset->addField('image_css_id', 'text', array(
		'label'     => Mage::helper('cmsplus')->__('css id'),
		'class'     => '',
		'required'  => false,
		'name'      => 'settings[image_css_id]',
	));
	$options = array(
		''	=>	'',
		'1'	=>	Mage::helper('cmsplus')->__('external Link'),
		'2'	=>	Mage::helper('cmsplus')->__('category Link'),
		'3'	=>	Mage::helper('cmsplus')->__('product Link'),
		'4'	=>	Mage::helper('cmsplus')->__('cmsPlus Page Link')
	);
	$fieldset->addField('linktype', 'select', array(
		'label'     => Mage::helper('cmsplus')->__('Linktype'),
		'class'     => '',
		'required'  => false,
		'name'      => 'settings[linktype]',
		'options'	=>	$options,
	));
	
	$fieldset->addField('extlink', 'text', array(
		'label'     => Mage::helper('cmsplus')->__('External Link Url'),
		'class'     => '',
		'required'  => false,
		'name'      => 'settings[extlink]',
	));

	$options = Mage::getSingleton('cmsplus/category')->getOptionArray();
	
	$fieldset->addField('catlink', 'select', array(
		'label'     => Mage::helper('cmsplus')->__('Category'),
		'class'     => '',
		'required'  => false,
		'name'      => 'settings[catlink]',
		'options'	=>	$options,
	));
	
	$options = Mage::getSingleton('cmsplus/cz_cms_content_page')->getOptionArray();
	
	$fieldset->addField('cmslink', 'select', array(
		'label'     => Mage::helper('cmsplus')->__('cmsPlus Page'),
		'class'     => '',
		'required'  => false,
		'name'      => 'settings[cmslink]',
		'options'	=>	$options,
	));
	
	$fieldset->addField('prodlink', 'text', array(
		'label'     => Mage::helper('cmsplus')->__('Product ID'),
		'class'     => '',
		'required'  => false,
		'name'      => 'settings[prodlink]',
	));



	$fieldset->addField('hidden', 'checkbox', array(
		'label'     => Mage::helper('cmsplus')->__('Hidden'),
		'class'     => '',
		'required'  => false,
		'name'      => 'settings[hidden]',
		'value'		=>	1,
		'checked'	=>	$_hidden
	));
	
	
	if ($image) {
		$form->setValues($_data);
	}
      return parent::_prepareForm();
  }
}