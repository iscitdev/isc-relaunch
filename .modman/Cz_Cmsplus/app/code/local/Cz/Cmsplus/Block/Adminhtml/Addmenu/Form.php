<?php
/**
 * clever + zöger gmbh
 * http://www.clever-zoeger.de
 * @author Ronny Hempel <rhempel@clever-zoeger.de>
 * @version $Id: Form.php 266 2011-11-21 14:14:51Z rhempel $
 * $HeadURL: https://svn.clever-zoeger.de/svn/Magento/extensions/Cz_Cmsplus/tags/1.3.0/cmsPlus/app/code/local/Cz/Cmsplus/Block/Adminhtml/Addmenu/Form.php $
 * $LastChangedBy: rhempel $
 * $LastChangedRevision: 266 $
 * $LastChangedDate: 2011-11-21 15:14:51 +0100 (Mon, 21 Nov 2011) $
 */
?>
<?php

class Cz_Cmsplus_Block_Adminhtml_Addmenu_Form extends Mage_Adminhtml_Block_Widget_Form
{

    protected $_additionalButtons = array();

    public function __construct()
    {
        parent::__construct();
        $this->setTemplate('cz/cmsplus/addmenuform.phtml');
    }

    protected function _prepareLayout()
    {
    	
		if (Mage::helper('cmsplus')->isEditor()) {
			return parent::_prepareLayout();
		}
		
        $this->setChild('tabs',
            $this->getLayout()->createBlock('cmsplus/adminhtml_addmenu_tabs', 'tabs')
        );
		$_params = array();
		$_params['_current'] = false;
		$_params['store'] = Mage::helper('cmsplus')->getStoreId();

		$this->setChild('save_button',
            $this->getLayout()->createBlock('adminhtml/widget_button')
                ->setData(array(
                    'label'     => Mage::helper('cmsplus')->__('Menu Save'),
                    'onclick'   => "cmsplus.doSave('".$this->getUrl('*/*/saveMenu', $_params)."',true)",
                    'class' => 'save'
                ))
        );
		
		if (Mage::helper('cmsplus')->getAction() == 'menuedit') {
			$_id = Mage::helper('cmsplus')->getLastid();
			$menu = Mage::getModel('cmsplus/cz_cms_menu')->load($_id);
			
			$_params['menuid'] = $menu->getId();
            $this->setChild('delete_button',
                $this->getLayout()->createBlock('adminhtml/widget_button')
                    ->setData(array(
                    	'before_html'	=>	'<script>var deleteMsg="'.Mage::helper('cmsplus')->__('Are you sure you want to delete it?').'";</script>',
                        'label'     => Mage::helper('cmsplus')->__('Menu Delete'),
                        'onclick'   => "cmsplus.doDelete('".$this->getUrl('*/*/deleteMenu',$_params)."',true)",
                        'class' => 'delete'
                    ))
            );
		}

        
            
       
        return parent::_prepareLayout();
    }


    public function getDeleteButtonHtml()
    {
        return $this->getChildHtml('delete_button');
    }

    public function getSaveButtonHtml()
    {

          return $this->getChildHtml('save_button');
    }


    public function getTabsHtml()
    {
        return $this->getChildHtml('tabs');
    }

    public function getHeader()
    {
    	if (Mage::helper('cmsplus')->getAction() == 'menuedit') {
    		$id = Mage::helper('cmsplus')->getLastid();
			$menu = Mage::getModel('cmsplus/cz_cms_menu')->load($id);
    		return Mage::helper('cmsplus')->__('Edit Menu '.$menu->getName(). ' ' .$menu->getData(). '(ID:'.Mage::helper('cmsplus')->getLastid().')');
		} else {
        	return Mage::helper('cmsplus')->__('Add Menu');
		}
    }

    public function getDeleteUrl(array $args = array())
    {
        $params = array('_current'=>true);
		$params['store'] = Mage::helper('cmsplus')->getStoreId();
        $params = array_merge($params, $args);
        return $this->getUrl('*/*/delete', $params);
    }

}
