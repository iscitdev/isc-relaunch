<?php
/**
 * clever + zöger gmbh
 * http://www.clever-zoeger.de
 * @author Ronny Hempel <rhempel@clever-zoeger.de>
 * @version $Id: Tabs.php 266 2011-11-21 14:14:51Z rhempel $
 * $HeadURL: https://svn.clever-zoeger.de/svn/Magento/extensions/Cz_Cmsplus/tags/1.3.0/cmsPlus/app/code/local/Cz/Cmsplus/Block/Adminhtml/Addmenu/Tabs.php $
 * $LastChangedBy: rhempel $
 * $LastChangedRevision: 266 $
 * $LastChangedDate: 2011-11-21 15:14:51 +0100 (Mon, 21 Nov 2011) $
 */
?>
<?php

class Cz_Cmsplus_Block_Adminhtml_Addmenu_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{

    /**
     * Initialize Tabs
     *
     */
    public function __construct()
    {
        parent::__construct();
        //$this->setId('cmsplus_info_tabs');
		$this->setId('category_info_tabs');
        $this->setDestElementId('cmsplus_tab_content');
        $this->setTitle(Mage::helper('cmsplus')->__('Menu Data'));
        $this->setTemplate('cz/cmsplus/tabshoriz.phtml');
    }

    /**
     * Prepare Layout Content
     *
     * @return Mage_Adminhtml_Block_Catalog_Category_Tabs
     */
    protected function _prepareLayout()
    {
        $active = 1;
		//$block = $this->getLayout()->createBlock('cmsplus/adminhtml_cmsplus_tabs_view')->toHtml();
		
		//$block = $this->getLayout()->createBlock('cmsplus/cmsplus')
        //        ->setTemplate('cz/cmsplus/tabs/view.phtml')->toHtml();
        /*
		if (Mage::helper('cmsplus')->getAction() == 'menuedit') {
			$block = $this->getLayout()->createBlock('cmsplus/adminhtml_addmenu_tabs_view')->toHtml();
	        $this->addTab('view', array(
	            'label'     => Mage::helper('cmsplus')->__('View'),
	            'content'   => $block,
	            'active'    => $active
	        ));
		}
		 * 
		 */
		
		$block = $this->getLayout()->createBlock('cmsplus/adminhtml_addmenu_tabs_settings')->toHtml();

		
		$this->addTab('config', array(
            'label'     => Mage::helper('cmsplus')->__('Display Settings'),
            'content'   => $block,
            'active'    => $active
        ));

        return parent::_prepareLayout();
    }
}
