<?php
/**
 * clever + zöger gmbh
 * http://www.clever-zoeger.de
 * @author Ronny Hempel <rhempel@clever-zoeger.de>
 * @version $Id: Settings.php 447 2012-07-11 11:23:46Z rhempel $
 * $HeadURL: https://svn.clever-zoeger.de/svn/Magento/extensions/Cz_Cmsplus/tags/1.3.0/cmsPlus/app/code/local/Cz/Cmsplus/Block/Adminhtml/Addmenu/Tabs/Settings.php $
 * $LastChangedBy: rhempel $
 * $LastChangedRevision: 447 $
 * $LastChangedDate: 2012-07-11 13:23:46 +0200 (Wed, 11 Jul 2012) $
 */
?>
<?php
class Cz_Cmsplus_Block_Adminhtml_Addmenu_Tabs_Settings extends Mage_Adminhtml_Block_Widget_Form
{
  protected function _prepareForm() {
	$form = new Varien_Data_Form();
	$this->setForm($form);
	$fieldset = $form->addFieldset('main_form', array('legend'=>Mage::helper('cmsplus')->__('Settings')));
	
	$_hidden = '';
	$menu = false;
	$_data = array();
	if (Mage::helper('cmsplus')->getAction() == 'menuedit') {
		$_id = Mage::helper('cmsplus')->getLastid();
		$menu = Mage::getModel('cmsplus/cz_cms_menu')->load($_id);
		$_data = $menu->getData();
		
		if ($menu->getHidden() == 1) {
			$_hidden = 'checked';
		}
		
		$_data['form_key'] = Mage::getSingleton('core/session')->getFormKey();
		$_data['isnew'] = 0;
		$_data['menuid'] = $menu->getId();
		$_data['hidden'] = 1;
	}
	
	if (Mage::helper('cmsplus')->getAction() == 'menuedit') {
		$fieldset->addField('isnew', 'hidden', array(
			'label'     => '',
			'class'     => '',
			'required'  => false,
			'name'      => 'isnew',
			'value'		=>	0,
		));
		$fieldset->addField('menuid', 'hidden', array(
			'label'     => '',
			'class'     => '',
			'required'  => false,
			'name'      => 'menuid',
			'value'		=>	0,
		));
	} else {
		$fieldset->addField('isnew', 'hidden', array(
			'label'     => '',
			'class'     => '',
			'required'  => false,
			'name'      => 'isnew',
			'value'		=>	1,
		));
		$_data['isnew'] = 1;
	}
	
	
	$fieldset->addField('name', 'text', array(
		'label'     => Mage::helper('cmsplus')->__('Name'),
		'class'     => 'required-entry',
		'required'  => true,
		'name'      => 'settings[name]',
	));
	$fieldset->addField('menu_css_class', 'text', array(
		'label'     => Mage::helper('cmsplus')->__('Menu CSS Class'),
		'class'     => '',
		'required'  => false,
		'name'      => 'settings[menu_css_class]',
	));
	$fieldset->addField('menu_css_id', 'text', array(
		'label'     => Mage::helper('cmsplus')->__('Menu CSS ID'),
		'class'     => '',
		'required'  => false,
		'name'      => 'settings[menu_css_id]',
	));
	
	if (Mage::helper('cmsplus')->getStoreId() != 0) {
		
		$menuObj = Mage::getModel('cmsplus/cz_cms_menu')->getCollection()->loadByStore(0);
		$menu = array(
			''	=>	Mage::helper('cmsplus')->__('Please Select'),
		);
		foreach ($menuObj AS $_menu) {
			$menu[$_menu->getId()] = $_menu->getName();
		}
		
		
		$fieldset->addField('default_menuid', 'select', array(
			'label'     => Mage::helper('cmsplus')->__('Default Menu'),
			'class'     => 'required-entry',
			'required'  => true,
			'name'      => 'settings[default_menuid]',
			'options'	=>	$menu,
		));
	}
	
	
	$fieldset->addField('hidden', 'checkbox', array(
		'label'     => Mage::helper('cmsplus')->__('Hidden'),
		'class'     => '',
		'required'  => false,
		'name'      => 'settings[hidden]',
		'value'		=>	1,
		'checked'	=>	$_hidden
	));
	
	
	if ($menu) {
		$form->setValues($_data);
	}
      return parent::_prepareForm();
  }
}