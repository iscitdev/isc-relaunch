<?php
/**
 * clever + zöger gmbh
 * http://www.clever-zoeger.de
 * @author Ronny Hempel <rhempel@clever-zoeger.de>
 * @version $Id: Addpage.php 181 2011-09-12 14:37:18Z rhempel $
 * $HeadURL: https://svn.clever-zoeger.de/svn/Magento/extensions/Cz_Cmsplus/tags/1.3.0/cmsPlus/app/code/local/Cz/Cmsplus/Block/Adminhtml/Addpage.php $
 * $LastChangedBy: rhempel $
 * $LastChangedRevision: 181 $
 * $LastChangedDate: 2011-09-12 16:37:18 +0200 (Mon, 12 Sep 2011) $
 */
?>
<?php

class Cz_Cmsplus_Block_Adminhtml_Addpage extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct() {
        $this->_objectId    = 'entity_id';
		
		$this->_blockGroup = 'cmsplus';
        $this->_controller  = 'adminhtml';
       	$this->_mode        = 'addpage';

        parent::__construct();
    }

    protected function _prepareLayout() {
    	Mage::helper('cmsplus')->setMenu(0);
		if (Mage::getSingleton('cms/wysiwyg_config')->isEnabled() && ($block = $this->getLayout()->getBlock('head'))) {                                                                               
			$block->setCanLoadTinyMce(true);                                                                                                                                            
		}
		
        return parent::_prepareLayout();
    }
}
