<?php
/**
 * clever + zöger gmbh
 * http://www.clever-zoeger.de
 * @author Ronny Hempel <rhempel@clever-zoeger.de>
 * @version $Id: Form.php 447 2012-07-11 11:23:46Z rhempel $
 * $HeadURL: https://svn.clever-zoeger.de/svn/Magento/extensions/Cz_Cmsplus/tags/1.3.0/cmsPlus/app/code/local/Cz/Cmsplus/Block/Adminhtml/Addpage/Form.php $
 * $LastChangedBy: rhempel $
 * $LastChangedRevision: 447 $
 * $LastChangedDate: 2012-07-11 13:23:46 +0200 (Wed, 11 Jul 2012) $
 */
?>
<?php

class Cz_Cmsplus_Block_Adminhtml_Addpage_Form extends Mage_Adminhtml_Block_Widget_Form
{

    protected $_additionalButtons = array();

    public function __construct()
    {
        parent::__construct();
        $this->setTemplate('cz/cmsplus/addmenuform.phtml');
    }

    protected function _prepareLayout()
    {

        $this->setChild('tabs',
            $this->getLayout()->createBlock('cmsplus/adminhtml_addpage_tabs', 'tabs')
        );

			$params = $this->getRequest()->getParams();
			
			$_params = array();
			$_params['_current'] = false;
			$_params['store'] = Mage::app()->getRequest()->getParam('store');
		
			if (isset($params) && isset($params['menuid'])) {
				$_params['menuid'] = $params['menuid'];
			}
			
			
			
			$this->setChild('back_button',
                $this->getLayout()->createBlock('adminhtml/widget_button')
                    ->setData(array(
                        'label'     => Mage::helper('cmsplus')->__('Back'),
                        'onclick'   => "cmsplus.doAction('".$this->getUrl('*/*/addMenu',$_params)."',true)",
                        'class' => 'back'
                    ))
            );
			
			if (Mage::helper('cmsplus')->isEditor()) {
				return parent::_prepareLayout();
			}
			
			if (isset($params) && isset($params['pageid'])) {
				$_params['pageid'] = $params['pageid'];
			}

			$this->setChild('save_button',
                $this->getLayout()->createBlock('adminhtml/widget_button')
                    ->setData(array(
                        'label'     => Mage::helper('cmsplus')->__('Page Save'),
                        'onclick'   => "cmsplus.doSave('".$this->getUrl('*/*/savePage',$_params)."',true)",
                        'class' => 'save'
                    ))
            );
  
       
            $this->setChild('delete_button',
                $this->getLayout()->createBlock('adminhtml/widget_button')
                    ->setData(array(
                    	'before_html'	=>	'<script>var deleteMsg="'.Mage::helper('cmsplus')->__('Are you sure you want to delete it?').'";</script>',
                        'label'     => Mage::helper('cmsplus')->__('Page Delete'),
                        'onclick'   => "cmsplus.doDelete('".$this->getUrl('*/*/deletePage', $_params)."',true)",
                        'class' => 'delete'
                    ))
            );
		
		if (isset($_params['pageid'])) {
			$page = Mage::getModel('cmsplus/cz_cms_content_page')->load($_params['pageid']);
			$hash = sha1(microtime().$page->getId().$page->getName());
			$page->setPreviewkey($hash);
			$page->save();
			$_params['preview'] = 1;

			$storeId = Mage::helper('cmsplus')->getStoreId();
			$_storeCode = '';
			if ($storeId != 0) {
				$store=Mage::getModel('core/store')->load($storeId);
				$_storeCode = '&___store='.$store->getCode();
			}

			$this->setChild('preview_button',
				$this->getLayout()->createBlock('adminhtml/widget_button')
					->setData(array(
			    		'label'     => Mage::helper('cmsplus')->__('Page Preview'),
						'onclick'   => "cmsplus.doPopup('".$page->getUrl()."?preview=1&hash=".$hash.$_storeCode."',true)",
						'class' => 'preview'
			        ))
			);
		}
            
       
        return parent::_prepareLayout();
    }


    public function getDeleteButtonHtml()
    {
        return $this->getChildHtml('delete_button');
    }

    public function getSaveButtonHtml()
    {

          return $this->getChildHtml('save_button');
    }
	public function getBackButtonHtml() {
		return $this->getChildHtml('back_button');
	}
	public function getPreviewButtonHtml() {
		return $this->getChildHtml('preview_button');
	}
	
    
    public function getAdditionalcontentHtml()
    {
    	if (Mage::helper('cmsplus')->getTab()) {
    	$addcontent = '<script>
				var defaulttab = "cmsplus_info_tabs_'.Mage::helper('cmsplus')->getTab().'";
			</script>';
		} else {
			$addcontent = '<script>
				var defaulttab = "";
			</script>';
		}
		return $addcontent;
    }
	


    public function getTabsHtml()
    {
        return $this->getChildHtml('tabs');
    }

    public function getHeader()
    {
    	if (Mage::helper('cmsplus')->getAction() == 'pageedit') {
    		$id = Mage::helper('cmsplus')->getLastid();
			$page = Mage::getModel('cmsplus/cz_cms_content_page')->load($id);
    		return Mage::helper('cmsplus')->__('Edit Page '.$page->getName().'(ID:'.Mage::helper('cmsplus')->getLastid().')');
		} else {
        	return Mage::helper('cmsplus')->__('Add Page');
		}
    }

    public function getDeleteUrl(array $args = array())
    {
        $params = array('_current'=>true);
		$params['store'] = Mage::helper('cmsplus')->getStoreId();
        $params = array_merge($params, $args);
        return $this->getUrl('*/*/delete', $params);
    }
	
	public function getProductsJson() {
		if (Mage::helper('cmsplus')->getAction() == 'pageedit') {
			$_id = Mage::helper('cmsplus')->getLastid();
			$page = Mage::getModel('cmsplus/cz_cms_content_page')->load($_id);
			$_catIds = $page->getCategories();
			if (is_array($_catIds)) {
				$_out = array();
				foreach ($_catIds AS $_catId) {
					$_out[$_catId] = $_catId;
				}
				return Mage::helper('core')->jsonEncode($_out);
			}
		}
		return '{}';
	}
}
