<?php
/**
 * clever + zöger gmbh
 * http://www.clever-zoeger.de
 * @author Ronny Hempel <rhempel@clever-zoeger.de>
 * @version $Id: Tabs.php 266 2011-11-21 14:14:51Z rhempel $
 * $HeadURL: https://svn.clever-zoeger.de/svn/Magento/extensions/Cz_Cmsplus/tags/1.3.0/cmsPlus/app/code/local/Cz/Cmsplus/Block/Adminhtml/Addpage/Tabs.php $
 * $LastChangedBy: rhempel $
 * $LastChangedRevision: 266 $
 * $LastChangedDate: 2011-11-21 15:14:51 +0100 (Mon, 21 Nov 2011) $
 */
?>
<?php

class Cz_Cmsplus_Block_Adminhtml_Addpage_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{

    /**
     * Initialize Tabs
     *
     */
    public function __construct()
    {
        parent::__construct();
        //$this->setId('cmsplus_info_tabs');
        $this->setId('category_info_tabs');
        $this->setDestElementId('cmsplus_tab_content');
        $this->setTitle(Mage::helper('cmsplus')->__('Page Data'));
        $this->setTemplate('cz/cmsplus/tabshoriz.phtml');
    }

    /**
     * Prepare Layout Content
     *
     * @return Mage_Adminhtml_Block_Catalog_Category_Tabs
     */
    protected function _prepareLayout()
    {

        $active = true;

        if(Mage::helper('cmsplus')->getAction() == 'pageedit') {


            if(!Mage::helper('cmsplus')->hasAccess()) {
                $block = Mage::helper('cmsplus')->__('Access denied');
                $this->addTab('view', array(
                    'label' => Mage::helper('cmsplus')->__('View'),
                    'content' => $block,
                    'active' => true
                ));
                return parent::_prepareLayout();
            }

            $block = $this->getLayout()->createBlock('cmsplus/adminhtml_addpage_tabs_view')->toHtml();
            $this->addTab('view', array(
                'label' => Mage::helper('cmsplus')->__('View'),
                'content' => $block,
                'active' => true
            ));


        }
        if(Mage::helper('cmsplus')->isEditor()) {
            return parent::_prepareLayout();
        }

        $block = $this->getLayout()->createBlock('cmsplus/adminhtml_addpage_tabs_settings')->toHtml();
        $this->addTab('config', array(
            'label' => Mage::helper('cmsplus')->__('Display Settings'),
            'content' => $block,
            'active' => false
        ));


        if(Mage::helper('cmsplus')->getAction() == 'pageedit') {
            $block = $this->getLayout()->createBlock('cmsplus/adminhtml_addpage_tabs_images')->toHtml();
            $this->addTab('images', array(
                'label' => Mage::helper('cmsplus')->__('Images'),
                'content' => $block,
                'active' => false
            ));
        }

        if(Mage::helper('cmsplus')->getAction() == 'pageedit') {
            $this->addTab('mapping', array(
                'label' => Mage::helper('cmsplus')->__('Category Mapping'),
                'content' => $this->getLayout()->createBlock('cmsplus/adminhtml_addpage_tabs_mapping')->setNameInLayout('category.product.grid')->toHtml(),
                'active' => false,
            ));
        }

        if(Mage::helper('cmsplus')->getAction() == 'pageedit') {
            $this->addTab('language', array(
                'label' => Mage::helper('cmsplus')->__('Language Mapping'),
                'content' => $this->getLayout()->createBlock('cmsplus/adminhtml_addpage_tabs_language')->toHtml(),
                'active' => false,
            ));
        }


        if(Mage::helper('cmsplus')->getAction() == 'pageedit') {
            $this->addTab('access', array(
                'label' => Mage::helper('cmsplus')->__('Access'),
                'content' => $this->getLayout()->createBlock('cmsplus/adminhtml_addpage_tabs_access')->toHtml(),
                'active' => false,
            ));
        }

        return parent::_prepareLayout();
    }
}
