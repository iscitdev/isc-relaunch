<?php
/**
 * clever + zöger gmbh
 * http://www.clever-zoeger.de
 * @author Ronny Hempel <rhempel@clever-zoeger.de>
 * @version $Id: Access.php 447 2012-07-11 11:23:46Z rhempel $
 * $HeadURL: https://svn.clever-zoeger.de/svn/Magento/extensions/Cz_Cmsplus/tags/1.3.0/cmsPlus/app/code/local/Cz/Cmsplus/Block/Adminhtml/Addpage/Tabs/Access.php $
 * $LastChangedBy: rhempel $
 * $LastChangedRevision: 447 $
 * $LastChangedDate: 2012-07-11 13:23:46 +0200 (Wed, 11 Jul 2012) $
 */
?>
<?php
class Cz_Cmsplus_Block_Adminhtml_Addpage_Tabs_Access extends Cz_Cmsplus_Block_Adminhtml_Formdefault
{

	
  protected function _prepareForm() {
	$form = new Varien_Data_Form();
	$this->setForm($form);
	$fieldset = $form->addFieldset('main_form', array('legend'=>Mage::helper('cmsplus')->__('Access')));
	
	$this->_setFieldset(array(), $fieldset);
	
	
	$params = $this->getRequest()->getParams();
	if (!isset($params['menuid'])) {
		$params['menuid'] = 0;
	}
	if (!isset($params['pageid'])) {
		$params['pageid'] = 0;
	}
	if (!isset($params['subpageid'])) {
		$params['subpageid'] = 0;
	}
	$_hidden = '';
	$menu = false;

	$_id = Mage::helper('cmsplus')->getLastid();
	$page = Mage::getModel('cmsplus/cz_cms_content_page')->load($_id);
	$_data = $page->getData();
	
	$access = false;
	if ( $access = unserialize($_data['access'])) {
		$_data['accessview'] = $access['view'];
		$_data['accessedit'] = $access['viewedit'];
	} else {
		//old
		$_data['accessview'] = explode(',',$_data['access']);
		$_data['accessedit'] = explode(',',$_data['access']);
	}
	
	$fieldset->addField('accessview', 'multiselect', array(
		'label'     => Mage::helper('cmsplus')->__('Access View'),
		'class'     => '',
		'required'  => false,
		'name'      => 'settings[access][view]',
		'values'	=>	Mage::getModel('cmsplus/system_config_source_adminrole')->toOptionArray(true),
	));
	$fieldset->addField('accessedit', 'multiselect', array(
		'label'     => Mage::helper('cmsplus')->__('Access View/Edit'),
		'class'     => '',
		'required'  => false,
		'name'      => 'settings[access][viewedit]',
		'values'	=>	Mage::getModel('cmsplus/system_config_source_adminrole')->toOptionArray(true),
	));

	
	
	if ($page) {
		$form->setValues($_data);
	}
      return parent::_prepareForm();
  }


}