<?php
/**
 * clever + zöger gmbh
 * http://www.clever-zoeger.de
 * @author Ronny Hempel <rhempel@clever-zoeger.de>
 * @version $Id: Images.php 181 2011-09-12 14:37:18Z rhempel $
 * $HeadURL: https://svn.clever-zoeger.de/svn/Magento/extensions/Cz_Cmsplus/tags/1.3.0/cmsPlus/app/code/local/Cz/Cmsplus/Block/Adminhtml/Addpage/Tabs/Images.php $
 * $LastChangedBy: rhempel $
 * $LastChangedRevision: 181 $
 * $LastChangedDate: 2011-09-12 16:37:18 +0200 (Mon, 12 Sep 2011) $
 */
?>
<?php
class Cz_Cmsplus_Block_Adminhtml_Addpage_Tabs_Images extends Cz_Cmsplus_Block_Adminhtml_Formdefault
{
  protected function _prepareForm()
  {
	$form = new Varien_Data_Form();
	$this->setForm($form);
	
	
	//$attributes = $this->getAttributes();
	//$this->_setFieldset($attributes, $fieldset);
	
	$params = $this->getRequest()->getParams();


	$_params = array();
	$_params['_current'] = false;
	if (isset($params) && isset($params['menuid'])) {
		$_params['menuid'] = $params['menuid'];
	}
	if (isset($params) && isset($params['pageid'])) {
		$_params['pageid'] = $params['pageid'];
	}

	$types = Mage::getModel('cmsplus/cz_cms_type')->getCollection()->loadByTable('image');
	
	$fieldset = array();
	foreach ($types AS $type) {
		$fieldset[$type->getId()] = $form->addFieldset('imageblock_'.$type->getId(), array('legend'=>Mage::helper('cmsplus')->__($type->getName())));
		$this->_setFieldset(array(), $fieldset[$type->getId()]);
	
		$images = Mage::getModel('cmsplus/cz_cms_content_page_image')->getCollection()->setHidden(0)->loadByPageIdAndTypeId($params['pageid'],$type->getId());
		foreach ($images AS $image) {
			$fieldset[$type->getId()]->addField('image_'.$image->getId(), 'imagecmsplus', array(
				'class'     => '',
				'required'  => false,
				'image'		=>	$image,
			));
		}
		$_params['typeid'] = $type->getId();
		$fieldset[$type->getId()]->addField('addimage'.$type->getId(), 'buttoncmsplus', array(
			/*'label'     => Mage::helper('cmsplus')->__('Add Block'),*/
			'class'     => 'add btn-add',
			'required'  => false,
			'name'      => 'addimage',
			'onclick'	=>	'cmsplus.doAction(\''.$this->getUrl('*/*/addImage', $_params).'\'); return false;',
			'value'		=>	Mage::helper('cmsplus')->__('Add Image'),
		));
		
	}
	

      return parent::_prepareForm();
  }
}