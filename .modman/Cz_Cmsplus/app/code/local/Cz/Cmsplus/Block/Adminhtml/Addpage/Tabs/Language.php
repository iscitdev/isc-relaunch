<?php

/**
 * Created by PhpStorm.
 * User: rhutterer
 * Date: 21.07.14
 * Time: 14:56
 */
class Cz_Cmsplus_Block_Adminhtml_Addpage_Tabs_Language extends Cz_Cmsplus_Block_Adminhtml_Formdefault
{

    public function _prepareForm()
    {

        $form = new Varien_Data_Form();
        $this->setForm($form);
        $fieldset = $form->addFieldset('main_form', array('legend' => Mage::helper('cmsplus')->__('Language Mapping')));

        $this->_setFieldset(array(), $fieldset);
        $params = $this->getRequest()->getParams();
        $selects = array();
        $collection = Mage::getModel('cmsplus/cz_cms_content_page')->getCollection();
        foreach($collection as $model) {
            $storeCode = Mage::getModel('core/store')->load($model->getData('storeview_code'))->getCode();
            $selects[] = array(
                'label' => $storeCode . ' / ' . $model->getName(),
                'value' => $model->getId()
            );
        }

        $fieldset->addField('mapped_page_id', 'select', array(
            'label' => Mage::helper('cmsplus')->__('Mapped Page'),
            'class' => '',
            'required' => false,
            'name' => 'settings[mapped_page_id]',
            'values' => $selects,
        ));
        if(Mage::helper('cmsplus')->getAction() == 'pageedit') {
            $_id = Mage::helper('cmsplus')->getLastid();
            $menu = Mage::getModel('cmsplus/cz_cms_content_page')->load($_id);
            $_data = $menu->getData();
            $form->setValues($_data);
        }


        return parent::_prepareForm();
    }
}