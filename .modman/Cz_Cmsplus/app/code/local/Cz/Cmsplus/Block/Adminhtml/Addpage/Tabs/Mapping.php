<?php
/**
 * clever + zöger gmbh
 * http://www.clever-zoeger.de
 * @author Ronny Hempel <rhempel@clever-zoeger.de>
 * @version $Id: Mapping.php 189 2011-09-15 13:50:40Z rhempel $
 * $HeadURL: https://svn.clever-zoeger.de/svn/Magento/extensions/Cz_Cmsplus/tags/1.3.0/cmsPlus/app/code/local/Cz/Cmsplus/Block/Adminhtml/Addpage/Tabs/Mapping.php $
 * $LastChangedBy: rhempel $
 * $LastChangedRevision: 189 $
 * $LastChangedDate: 2011-09-15 15:50:40 +0200 (Thu, 15 Sep 2011) $
 */
?>
<?php

class Cz_Cmsplus_Block_Adminhtml_Addpage_Tabs_Mapping extends Mage_Adminhtml_Block_Widget_Grid {

    public function __construct()
    {
        parent::__construct();
        $this->setId('catalog_category_products');
        $this->setDefaultSort('entity_id');
        $this->setUseAjax(true);
		
		
		//$this->setCheckboxCheckCallback('registerCategoryProduct');
    }

    public function getCategory()
    {
        return Mage::registry('category');
    }

    protected function _addColumnFilterToCollection($column)
    {
        // Set custom filter for in category flag
        if ($column->getId() == 'in_category') {
            $categorieIds = $this->_getSelectedProducts();
            if (empty($categorieIds)) {
                $categorieIds = array();
            }
            if ($column->getFilter()->getValue()) {
                $this->getCollection()->addFieldToFilter('entity_id', array('in'=>$categorieIds));
            }
            elseif(!empty($categorieIds)) {
                $this->getCollection()->addFieldToFilter('entity_id', array('nin'=>$categorieIds));
            }
        }
        else {
            parent::_addColumnFilterToCollection($column);
        }
        return $this;
    }

    protected function _prepareCollection() {

        $collection = Mage::getModel('catalog/category')->getCollection()
            ->addAttributeToSelect('name');

        $this->setCollection($collection);
/*
        if ($this->getCategory()->getProductsReadonly()) {
            $productIds = $this->_getSelectedProducts();
            if (empty($productIds)) {
                $productIds = 0;
            }
            $this->getCollection()->addFieldToFilter('entity_id', array('in'=>$productIds));
        }
*/
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('in_category', array(
            'header_css_class' => 'a-center',
            'type'      => 'checkbox',
            'name'      => 'in_category',
            'values'    => $this->_getSelectedProducts(),
            'align'     => 'center',
            'index'     => 'entity_id'
        ));
        
        $this->addColumn('entity_id', array(
            'header'    => Mage::helper('catalog')->__('ID'),
            'sortable'  => true,
            'width'     => '60',
            'index'     => 'entity_id'
        ));
        $this->addColumn('name', array(
            'header'    => Mage::helper('catalog')->__('Name'),
            'index'     => 'name'
        ));
        return parent::_prepareColumns();
    }

    public function getGridUrl()
    {
    	$_id = Mage::helper('cmsplus')->getLastid();
		$_params = array();
		$_params['_current'] = true;
		$_params['pageid']	=	$_id;

        return $this->getUrl('*/*/grid', array($_params)).'pageid/'.$_id.'/';
    }

    protected function _getSelectedProducts()
    {
    	if (Mage::helper('cmsplus')->getAction() == 'pageedit') {
    		
			$_id = Mage::helper('cmsplus')->getLastid();
			
			$page = Mage::getModel('cmsplus/cz_cms_content_page')->load($_id);
			$_categories = $page->getCategories();

			if (is_array($_categories)) {
				return array_values($_categories);
			}
		} elseif ($_products = $this->getRequest()->getPost('selected_products')) {
			return array_values($_products);
		} elseif ($_id = $this->getRequest()->getParam('pageid')) {
			$page = Mage::getModel('cmsplus/cz_cms_content_page')->load($_id);
			$_categories = $page->getCategories();

			if (is_array($_categories)) {
				return array_values($_categories);
			}
		}
    	return array();
    	/*
        $products = $this->getRequest()->getPost('selected_products');
        if (is_null($products)) {
            $products = $this->getCategory()->getProductsPosition();
            return array_keys($products);
        }
        return $products;
		 */
    }

    /**
     * Get children of specified item
     *
     * @param Varien_Object $item
     * @return array
     */
    public function getMultipleRows($item)
    {
        if ($item instanceof Mage_Catalog_Model_Category) {
            return false;
        }
        return parent::getMultipleRows($item);
    }

}

