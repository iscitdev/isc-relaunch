<?php
/**
 * clever + zöger gmbh
 * http://www.clever-zoeger.de
 * @author Ronny Hempel <rhempel@clever-zoeger.de>
 * @version $Id: Settings.php 181 2011-09-12 14:37:18Z rhempel $
 * $HeadURL: https://svn.clever-zoeger.de/svn/Magento/extensions/Cz_Cmsplus/tags/1.3.0/cmsPlus/app/code/local/Cz/Cmsplus/Block/Adminhtml/Addpage/Tabs/Settings.php $
 * $LastChangedBy: rhempel $
 * $LastChangedRevision: 181 $
 * $LastChangedDate: 2011-09-12 16:37:18 +0200 (Mon, 12 Sep 2011) $
 */
?>
<?php

class Cz_Cmsplus_Block_Adminhtml_Addpage_Tabs_Settings extends Cz_Cmsplus_Block_Adminhtml_Formdefault
{


    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $this->setForm($form);
        $fieldset = $form->addFieldset('main_form', array('legend' => Mage::helper('cmsplus')->__('Settings')));

        $this->_setFieldset(array(), $fieldset);


        $params = $this->getRequest()->getParams();
        if(!isset($params['menuid'])) {
            $params['menuid'] = 0;
        }
        if(!isset($params['pageid'])) {
            $params['pageid'] = 0;
        }
        if(!isset($params['subpageid'])) {
            $params['subpageid'] = 0;
        }
        $_hidden = '';
        $menu = false;
        if(Mage::helper('cmsplus')->getAction() == 'pageedit') {

            $_id = Mage::helper('cmsplus')->getLastid();
            $menu = Mage::getModel('cmsplus/cz_cms_content_page')->load($_id);
            $_data = $menu->getData();

            if($menu->getHidden() == 1) {
                $_hidden = 'checked';
            }

            $_data['form_key'] = Mage::getSingleton('core/session')->getFormKey();
            $_data['isnew'] = 0;
            $_data['menuid'] = $params['menuid'];
            $_data['pageid'] = $menu->getId();
            $_data['hidden'] = 1;

            $fieldset->addField('pageid', 'hidden', array(
                'label' => '',
                'class' => '',
                'required' => false,
                'name' => 'pageid',
                'value' => $params['pageid'],
            ));
        }

        if(Mage::helper('cmsplus')->getAction() == 'pageedit') {
            $fieldset->addField('isnew', 'hidden', array(
                'label' => '',
                'class' => '',
                'required' => false,
                'name' => 'isnew',
                'value' => 0,
            ));
        } else {
            $fieldset->addField('isnew', 'hidden', array(
                'label' => '',
                'class' => '',
                'required' => false,
                'name' => 'isnew',
                'value' => 1,
            ));
        }
        $fieldset->addField('menuid', 'hidden', array(
            'label' => '',
            'class' => '',
            'required' => false,
            'name' => 'menuid',
            'value' => $params['menuid'],
        ));

        $fieldset->addField('idcz_cms_content_page_pid', 'hidden', array(
            'label' => '',
            'class' => '',
            'required' => false,
            'name' => 'settings[idcz_cms_content_page_pid]',
            'value' => $params['subpageid'],
        ));


        $fieldset->addField('name', 'text', array(
            'label' => Mage::helper('cmsplus')->__('Name'),
            'class' => 'required-entry',
            'required' => true,
            'name' => 'settings[name]',
        ));
        $fieldset->addField('name_alternative', 'text', array(
            'label' => Mage::helper('cmsplus')->__('Name alternative'),
            'class' => '',
            'required' => false,
            'name' => 'settings[name_alternative]',
        ));
        $fieldset->addField('page_title', 'text', array(
            'label' => Mage::helper('cmsplus')->__('Page Title'),
            'class' => 'required-entry',
            'required' => true,
            'name' => 'settings[page_title]',
        ));

        $fieldset->addField('urlkey', 'text', array(
            'label' => Mage::helper('cmsplus')->__('Urlkey'),
            'class' => '',
            'required' => false,
            'name' => 'settings[urlkey]',
        ));

        $fieldset->addField('headline', 'text', array(
            'label' => Mage::helper('cmsplus')->__('Headline'),
            'class' => '',
            'required' => false,
            'name' => 'settings[headline]',
        ));

        $fieldset->addField('subheadline', 'text', array(
            'label' => Mage::helper('cmsplus')->__('Subheadline'),
            'class' => '',
            'required' => false,
            'name' => 'settings[subheadline]',
        ));


        $fieldset->addField('page_css_class', 'text', array(
            'label' => Mage::helper('cmsplus')->__('Page CSS Class'),
            'class' => '',
            'required' => false,
            'name' => 'settings[page_css_class]',
        ));
        $fieldset->addField('page_css_id', 'text', array(
            'label' => Mage::helper('cmsplus')->__('Page CSS ID'),
            'class' => '',
            'required' => false,
            'name' => 'settings[page_css_id]',
        ));

        $widgetFilters = array();
        $wysiwygConfig = Mage::getSingleton('cms/wysiwyg_config')->getConfig(array('widget_filters' => $widgetFilters));
        $fieldset->addField('description', 'textarea', array(
            'label' => Mage::helper('cmsplus')->__('Description'),
            'class' => '',
            'required' => false,
            'style' => 'height: 300px;',
            'name' => 'settings[description]',
            'config' => $wysiwygConfig,
        ));


        $fieldset->addField('meta_description', 'text', array(
            'label' => Mage::helper('cmsplus')->__('Meta Description'),
            'class' => '',
            'required' => false,
            'name' => 'settings[meta_description]',
        ));

        $fieldset->addField('meta_keywords', 'text', array(
            'label' => Mage::helper('cmsplus')->__('Meta Keywords'),
            'class' => '',
            'required' => false,
            'name' => 'settings[meta_keywords]',
        ));


        $fieldset->addField('hidden', 'checkbox', array(
            'label' => Mage::helper('cmsplus')->__('Hidden'),
            'class' => '',
            'required' => false,
            'name' => 'settings[hidden]',
            'value' => 1,
            'checked' => $_hidden
        ));


        if($menu) {
            $form->setValues($_data);
        }
        return parent::_prepareForm();
    }


}