<?php
/**
 * clever + zöger gmbh
 * http://www.clever-zoeger.de
 * @author Ronny Hempel <rhempel@clever-zoeger.de>
 * @version $Id: View.php 447 2012-07-11 11:23:46Z rhempel $
 * $HeadURL: https://svn.clever-zoeger.de/svn/Magento/extensions/Cz_Cmsplus/tags/1.3.0/cmsPlus/app/code/local/Cz/Cmsplus/Block/Adminhtml/Addpage/Tabs/View.php $
 * $LastChangedBy: rhempel $
 * $LastChangedRevision: 447 $
 * $LastChangedDate: 2012-07-11 13:23:46 +0200 (Wed, 11 Jul 2012) $
 */
?>
<?php
class Cz_Cmsplus_Block_Adminhtml_Addpage_Tabs_View extends Cz_Cmsplus_Block_Adminhtml_Formdefault
{
  protected function _prepareForm()
  {
	$form = new Varien_Data_Form();
	$this->setForm($form);
	$fieldset = $form->addFieldset('main_form', array('legend'=>Mage::helper('cmsplus')->__('View')));
	
	$this->_setFieldset(array(), $fieldset);
	
	$params = $this->getRequest()->getParams();


	$_params = array();
	$_params['_current'] = false;
	$_params['store'] = Mage::helper('cmsplus')->getStoreId();
		
	if (isset($params) && isset($params['menuid'])) {
		$_params['menuid'] = $params['menuid'];
	}
	if (isset($params) && isset($params['pageid'])) {
		$_params['pageid'] = $params['pageid'];
	}
	
	
	
	$blocks = Mage::getModel('cmsplus/cz_cms_content_block')->getCollection()->setHidden(0)->loadByPageId($_params['pageid']);
	
	foreach ($blocks AS $block) {
		if (Mage::helper('cmsplus')->hasAccessBlock($block)) {
			$fieldset->addField('addblock_'.$block->getId(), 'blockcmsplus', array(
				'class'     => '',
				'required'  => false,
				'block'		=>	$block,
			));
		}
		
		
	}
	if (Mage::helper('cmsplus')->isEditor()) {
			return parent::_prepareForm();
		}
	$fieldset->addField('addblock', 'buttoncmsplus', array(
		/*'label'     => Mage::helper('cmsplus')->__('Add Block'),*/
		'class'     => 'add btn-add',
		'required'  => false,
		'name'      => 'addblock',
		'onclick'	=>	'cmsplus.doAction(\''.$this->getUrl('*/*/addBlock', $_params).'\'); return false;',
		'value'		=>	Mage::helper('cmsplus')->__('Add Block'),
	));

      return parent::_prepareForm();
  }
}