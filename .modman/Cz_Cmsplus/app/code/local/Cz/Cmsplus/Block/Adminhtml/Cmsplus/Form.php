<?php
/**
 * clever + zöger gmbh
 * http://www.clever-zoeger.de
 * @author Ronny Hempel <rhempel@clever-zoeger.de>
 * @version $Id: Form.php 181 2011-09-12 14:37:18Z rhempel $
 * $HeadURL: https://svn.clever-zoeger.de/svn/Magento/extensions/Cz_Cmsplus/tags/1.3.0/cmsPlus/app/code/local/Cz/Cmsplus/Block/Adminhtml/Cmsplus/Form.php $
 * $LastChangedBy: rhempel $
 * $LastChangedRevision: 181 $
 * $LastChangedDate: 2011-09-12 16:37:18 +0200 (Mon, 12 Sep 2011) $
 */
?>
<?php

class Cz_Cmsplus_Block_Adminhtml_Cmsplus_Form extends Mage_Adminhtml_Block_Widget_Form
{

    protected $_additionalButtons = array();

    public function __construct()
    {
        parent::__construct();
        $this->setTemplate('cz/cmsplus/form.phtml');
    }

    protected function _prepareLayout()
    {
        $this->setChild('tabs',
            $this->getLayout()->createBlock('cmsplus/adminhtml_cmsplus_tabs', 'tabs')
        );
		
		$params = $this->getRequest()->getParams();
		
		if (isset($params['menuid']) || isset($params['pageid']) || isset($params['subpageid'])) {
			
			$_params = array();
			$_params['_current'] = false;
			$_params['store'] = Mage::helper('cmsplus')->getStoreId();
	
			if (Mage::helper('cmsplus')->getMenu()) {
				$this->setChild('save_button',
	                $this->getLayout()->createBlock('adminhtml/widget_button')
	                    ->setData(array(
	                        'label'     => Mage::helper('cmsplus')->__('Menu Save'),
	                        'onclick'   => "cmsplus.doSave('".$this->getUrl('*/*/saveMenu', $_params)."',true)",
	                        'class' => 'save'
	                    ))
	            );
	  
	       
	            $this->setChild('delete_button',
	                $this->getLayout()->createBlock('adminhtml/widget_button')
	                    ->setData(array(
	                        'label'     => Mage::helper('cmsplus')->__('Menu Delete'),
	                        'onclick'   => "cmsplus.doAction('".$this->getUrl('*/*/deleteMenu', $_params)."',true)",
	                        'class' => 'delete'
	                    ))
	            );
			} else {
				$this->setChild('save_button',
	                $this->getLayout()->createBlock('adminhtml/widget_button')
	                    ->setData(array(
	                        'label'     => Mage::helper('cmsplus')->__('Page Save'),
	                        'onclick'   => "cmsplus.doSave('".$this->getUrl('*/*/savePage', $_params)."',true)",
	                        'class' => 'save'
	                    ))
	            );
	  
	       
	            $this->setChild('delete_button',
	                $this->getLayout()->createBlock('adminhtml/widget_button')
	                    ->setData(array(
	                        'label'     => Mage::helper('cmsplus')->__('Page Delete'),
	                        'onclick'   => "cmsplus.doAction('".$this->getUrl('*/*/deletePage', $_params)."',true)",
	                        'class' => 'delete'
	                    ))
	            );
			}

        }
            
       
        return parent::_prepareLayout();
    }


    public function getDeleteButtonHtml()
    {
        return $this->getChildHtml('delete_button');
    }

    public function getSaveButtonHtml()
    {

          return $this->getChildHtml('save_button');
    }


    public function getTabsHtml()
    {
        return $this->getChildHtml('tabs');
    }

    public function getHeader()
    {
        return Mage::helper('cmsplus')->__('Please Select Menu or Page');
    }

    public function getDeleteUrl(array $args = array())
    {
        $params = array('_current'=>true);
		$params['store'] = Mage::helper('cmsplus')->getStoreId();
        $params = array_merge($params, $args);
        return $this->getUrl('*/*/delete', $params);
    }

}
