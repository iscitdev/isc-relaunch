<?php
/**
 * clever + zöger gmbh
 * http://www.clever-zoeger.de
 * @author Ronny Hempel <rhempel@clever-zoeger.de>
 * @version $Id: Menutree.php 266 2011-11-21 14:14:51Z rhempel $
 * $HeadURL: https://svn.clever-zoeger.de/svn/Magento/extensions/Cz_Cmsplus/tags/1.3.0/cmsPlus/app/code/local/Cz/Cmsplus/Block/Adminhtml/Cmsplus/Menutree.php $
 * $LastChangedBy: rhempel $
 * $LastChangedRevision: 266 $
 * $LastChangedDate: 2011-11-21 15:14:51 +0100 (Mon, 21 Nov 2011) $
 */
?>
<?php

class Cz_Cmsplus_Block_Adminhtml_Cmsplus_Menutree extends Mage_Adminhtml_Block_Catalog_Category_Abstract
{

    protected $_withProductCount;



    protected function _prepareLayout()
    {
    	
		$this->setChild('store_switcher',
            $this->getLayout()->createBlock('adminhtml/store_switcher')
                ->setSwitchUrl($this->getUrl('*/*/*', array('_current'=>false, '_query'=>false, 'store'=>null)))
                ->setTemplate('store/switcher/enhanced.phtml')
        );
		
    	if (Mage::helper('cmsplus')->isEditor()) {
			return parent::_prepareLayout();
		}

		$params = $this->getRequest()->getParams();

		$_params = array();
		$_params['_current'] = false;
		$_params['store'] = Mage::helper('cmsplus')->getStoreId();

    	$this->setChild('add_menu_button',
            $this->getLayout()->createBlock('adminhtml/widget_button')
                ->setData(array(
                    'label'     => Mage::helper('cmsplus')->__('Add Menu'),
                    'onclick'   => "cmsplus.addMenu('".$this->getUrl('*/*/addMenu', $_params)."',true)",
                    'class'     => 'add',
                    'id'        => 'add_root_category_button'
                ))
        );
		
		if (isset($params) && isset($params['menuid'])) {
			$_params['menuid'] = $params['menuid'];
		}
		
		if (isset($params) && isset($params['pageid'])) {
			$_params['subpageid'] = $params['pageid'];
		}
		
		if (isset($params) && isset($params['subpageid'])) {
			$_params['subpageid'] = $params['subpageid'];
		}
		
		if (isset($_params['menuid']) || isset($_params['subpageid'])) {
			$this->setChild('add_page_button',
	            $this->getLayout()->createBlock('adminhtml/widget_button')
	                ->setData(array(
	                    'label'     => Mage::helper('cmsplus')->__('Add Page'),
	                    'onclick'   => "cmsplus.addPage('".$this->getUrl('*/*/addPage',$_params)."',true)",
	                    'class'     => 'add',
	                    'id'        => 'add_root_category_button'
	                ))
	        );
		}			
        
		
		
		

        return parent::_prepareLayout();
    }
	
	public function getAddMenuButtonHtml()
    {
        return $this->getChildHtml('add_menu_button');
    }
	
	public function getAddPageButtonHtml()
    {
        return $this->getChildHtml('add_page_button');
    }

    protected function _getDefaultStoreId()
    {
        return Mage_Catalog_Model_Abstract::DEFAULT_STORE_ID;
    }

    public function getStoreSwitcherHtml()
    {
        return $this->getChildHtml('store_switcher');
    }

}
