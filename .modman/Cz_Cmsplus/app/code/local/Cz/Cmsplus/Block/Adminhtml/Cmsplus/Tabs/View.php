<?php
/**
 * clever + zöger gmbh
 * http://www.clever-zoeger.de
 * @author Ronny Hempel <rhempel@clever-zoeger.de>
 * @version $Id: View.php 181 2011-09-12 14:37:18Z rhempel $
 * $HeadURL: https://svn.clever-zoeger.de/svn/Magento/extensions/Cz_Cmsplus/tags/1.3.0/cmsPlus/app/code/local/Cz/Cmsplus/Block/Adminhtml/Cmsplus/Tabs/View.php $
 * $LastChangedBy: rhempel $
 * $LastChangedRevision: 181 $
 * $LastChangedDate: 2011-09-12 16:37:18 +0200 (Mon, 12 Sep 2011) $
 */
?>
<?php
class Cz_Cmsplus_Block_Adminhtml_Cmsplus_Tabs_View extends Mage_Adminhtml_Block_Widget_Form
{
  protected function _prepareForm()
  {
	$form = new Varien_Data_Form();
	$this->setForm($form);
	$fieldset = $form->addFieldset('Boxitem_main_form', array('legend'=>Mage::helper('cmsplus')->__('View')));

	$fieldset->addField('title', 'text', array(
		'label'     => Mage::helper('cmsplus')->__('View'),
		'class'     => 'required-entry',
		'required'  => true,
		'name'      => 'title',
	));

/*
	if ( Mage::getSingleton('adminhtml/session')->getBoxitemData() )
	{
		$form->setValues(Mage::getSingleton('adminhtml/session')->getBoxitemData());
		Mage::getSingleton('adminhtml/session')->setBoxitemData(null);
	} elseif ( Mage::registry('boxitem_data') ) {
		$form->setValues(Mage::registry('boxitem_data')->getData());
	}
*/
      return parent::_prepareForm();
  }
}