<?php
/**
 * clever + zöger gmbh
 * http://www.clever-zoeger.de
 * @author Ronny Hempel <rhempel@clever-zoeger.de>
 * @version $Id: Settings.php 181 2011-09-12 14:37:18Z rhempel $
 * $HeadURL: https://svn.clever-zoeger.de/svn/Magento/extensions/Cz_Cmsplus/tags/1.3.0/cmsPlus/app/code/local/Cz/Cmsplus/Block/Adminhtml/Editelement/Tabs/Settings.php $
 * $LastChangedBy: rhempel $
 * $LastChangedRevision: 181 $
 * $LastChangedDate: 2011-09-12 16:37:18 +0200 (Mon, 12 Sep 2011) $
 */
?>
<?php
class Cz_Cmsplus_Block_Adminhtml_Editelement_Tabs_Settings extends Cz_Cmsplus_Block_Adminhtml_Formdefault
{
	

	
	
  protected function _prepareForm() {
	$form = new Varien_Data_Form();
	$this->setForm($form);
	$fieldset = $form->addFieldset('main_form', array('legend'=>Mage::helper('cmsplus')->__('Settings')));
	$this->_setFieldset(array(), $fieldset);
	
	$params = $this->getRequest()->getParams();

	$_hidden = '';
	
	
	$_id = Mage::helper('cmsplus')->getLastid();
	$element = Mage::getModel('cmsplus/cz_cms_content_element')->load($_id);
	$_data = $element->getData();
	
	if ($element->getHidden() == 1) {
		$_hidden = 'checked';
	}
		
	$_data['form_key'] = Mage::getSingleton('core/session')->getFormKey();
	$_data['isnew'] = 0;
	$_data['hidden'] = 1;
	
	$_data['elementid'] = $params['elementid'];
	$_data['elementcontent'] = $_data['content'];
	
	$_data['pageid'] = $params['pageid'];	
	$fieldset->addField('pageid', 'hidden', array(
		'label'     => '',
		'class'     => '',
		'required'  => false,
		'name'      => 'pageid',
		'value'		=>	$params['pageid'],
	));
	
	$_data['menuid'] = $params['menuid'];	
	$fieldset->addField('menuid', 'hidden', array(
		'label'     => '',
		'class'     => '',
		'required'  => false,
		'name'      => 'menuid',
		'value'		=>	$params['menuid'],
	));
	
	$fieldset->addField('elementid', 'hidden', array(
		'label'     => '',
		'class'     => '',
		'required'  => false,
		'name'      => 'elementid',
		'value'		=>	$params['elementid'],
	));
	
	
	$fieldset->addField('isnew', 'hidden', array(
		'label'     => '',
		'class'     => '',
		'required'  => false,
		'name'      => 'isnew',
		'value'		=>	0,
	));
	

	$fieldset->addField('name', 'text', array(
		'label'     => Mage::helper('cmsplus')->__('Name'),
		'class'     => 'required-entry',
		'required'  => true,
		'name'      => 'settings[name]',
	));
	
	$types = Mage::getModel('cmsplus/cz_cms_type')->getCollection()->loadByTable('element');
	$options = array();
	$options[''] = Mage::helper('cmsplus')->__('please choose');
	foreach ($types AS $type) {
		$options[$type->getId()] = $type->getName();
	}

	$fieldset->addField('cz_cms_type_idcz_cms_content_block_type', 'select', array(
		'label'     => Mage::helper('cmsplus')->__('Type'),
		'class'     => 'required-entry',
		'required'  => true,
		'name'      => 'settings[cz_cms_type_idcz_cms_content_block_type]',
		'options'	=>	$options,
	));
	
	
	
	$fieldset->addField('space_before', 'text', array(
		'label'     => Mage::helper('cmsplus')->__('space before (px)'),
		'class'     => '',
		'required'  => false,
		'name'      => 'settings[space_before]',
	));
	
	$fieldset->addField('space_after', 'text', array(
		'label'     => Mage::helper('cmsplus')->__('space after (px)'),
		'class'     => '',
		'required'  => false,
		'name'      => 'settings[space_after]',
	));
	
	$fieldset->addField('border', 'text', array(
		'label'     => Mage::helper('cmsplus')->__('border (px)'),
		'class'     => '',
		'required'  => false,
		'name'      => 'settings[border]',
	));
	
	$fieldset->addField('border_color', 'text', array(
		'label'     => Mage::helper('cmsplus')->__('border color (ffffff)'),
		'class'     => '',
		'required'  => false,
		'name'      => 'settings[border_color]',
	));
	
	$fieldset->addField('width', 'text', array(
		'label'     => Mage::helper('cmsplus')->__('width (% or px)'),
		'class'     => '',
		'required'  => false,
		'name'      => 'settings[width]',
	));
	
	$fieldset->addField('element_css_class', 'text', array(
		'label'     => Mage::helper('cmsplus')->__('css class'),
		'class'     => '',
		'required'  => false,
		'name'      => 'settings[element_css_class]',
	));
	
	$fieldset->addField('element_css_id', 'text', array(
		'label'     => Mage::helper('cmsplus')->__('css id'),
		'class'     => '',
		'required'  => false,
		'name'      => 'settings[element_css_id]',
	));
	
	$fieldset->addField('hidden', 'checkbox', array(
		'label'     => Mage::helper('cmsplus')->__('Hidden'),
		'class'     => '',
		'required'  => false,
		'name'      => 'settings[hidden]',
		'value'		=>	1,
		'checked'	=>	$_hidden
	));
	
		$colorpicker = "
			jQuery('#border_color').ColorPicker({
			onSubmit: function(hsb, hex, rgb, el) {
				jQuery(el).val(hex);
				jQuery(el).ColorPickerHide();
			},
			onBeforeShow: function () {
				jQuery(this).ColorPickerSetColor(this.value);
			}
		})
		.bind('keyup', function(){
			jQuery(this).ColorPickerSetColor(this.value);
		});
	
	";
	$fieldset->addField('script','script',array(
		'script'	=>	$colorpicker,
	)); 
	
	if ($element) {
		$form->setValues($_data);
	}
      return parent::_prepareForm();
  }


}