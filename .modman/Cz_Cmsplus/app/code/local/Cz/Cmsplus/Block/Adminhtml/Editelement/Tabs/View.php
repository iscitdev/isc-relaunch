<?php
/**
 * clever + zöger gmbh
 * http://www.clever-zoeger.de
 * @author Ronny Hempel <rhempel@clever-zoeger.de>
 * @version $Id: View.php 266 2011-11-21 14:14:51Z rhempel $
 * $HeadURL: https://svn.clever-zoeger.de/svn/Magento/extensions/Cz_Cmsplus/tags/1.3.0/cmsPlus/app/code/local/Cz/Cmsplus/Block/Adminhtml/Editelement/Tabs/View.php $
 * $LastChangedBy: rhempel $
 * $LastChangedRevision: 266 $
 * $LastChangedDate: 2011-11-21 15:14:51 +0100 (Mon, 21 Nov 2011) $
 */
?>
<?php
class Cz_Cmsplus_Block_Adminhtml_Editelement_Tabs_View extends Cz_Cmsplus_Block_Adminhtml_Formdefault
{

  protected function _prepareForm()
  {
	$form = new Varien_Data_Form();
	$this->setForm($form);
	$fieldset = $form->addFieldset('main_form', array('legend'=>Mage::helper('cmsplus')->__('View')));
	$this->_setFieldset(array(), $fieldset);
	
	$params = $this->getRequest()->getParams();

	$_id = Mage::helper('cmsplus')->getLastid();
	$element = Mage::getModel('cmsplus/cz_cms_content_element')->load($_id);
	$_data = $element->getData();
	$_data['elementcontent'] = $_data['content'];
	
	$_data['difftest'] = Mage::helper('cmsplus')->htmldiff($_data['content'],$_data['editor_content']);
	
	
	if (!Mage::helper('cmsplus')->isEditor() && !is_null($_data['editor_content'])){
		$fieldset->addField('infotext','note',array(
			'text'	=>	'<span style="color:red;">'.Mage::helper('cmsplus')->__('An editor has changed the contents').'</span>',
		));
		
		$fieldset->addField('editor_content', 'textarea', array(
			'label'     => Mage::helper('cmsplus')->__('editor Content'),
			'class'     => '',
			'required'  => false,
			'readonly'	=>	true,
			'style'		=>	'height: 300px;',
			'name'      => 'settings[editor_content]',
		));
		$_data['difftest'] = '<div style="height: 300px; overflow:scroll;"><style>
			del {color: red;}
			ins {color: green;}
		</style>'.$_data['difftest'].'</div>';
		$fieldset->addField('test', 'note', array(
			'label'	=>	Mage::helper('cmsplus')->__('diff content'),
			'text'	=>	$_data['difftest'],
		));
		
		
		$params = $this->getRequest()->getParams();
			
		$_params = array();
		$_params['_current'] = false;
		
		$_params['store'] = Mage::helper('cmsplus')->getStoreId();
	
		if (isset($params) && isset($params['elementid'])) {
			$_params['elementid'] = $params['elementid'];
		}
		if (isset($params) && isset($params['menuid'])) {
			$_params['menuid'] = $params['menuid'];
		}
		
		if (isset($params) && isset($params['blockid'])) {
			$_params['blockid'] = $params['blockid'];
		}
		
		if (isset($params) && isset($params['pageid'])) {
			$_params['pageid'] = $params['pageid'];
		}
		$_params['acceptchange'] = 1;
		$_params['declinechange'] = 0;
		$link = Mage::helper('adminhtml')->getUrl('*/*/saveElement',$_params);
				
		
		
		$_data['acceptbtn'] = Mage::helper('cmsplus')->__('Accept editor changes');
		$fieldset->addField('acceptbtn','buttoncmsplus',array(
			'class'		=>	'add btn-add',
			'name'		=>	'acceptbtn',
			'required'	=>	false,
			'onclick'	=>	'cmsplus.doAction(\''.$link.'\'); return false;',
			'value'		=>	Mage::helper('cmsplus')->__('Accept editor changes'),
			
		));
		$_params['acceptchange'] = 0;
		$_params['declinechange'] = 1;
		$link = Mage::helper('adminhtml')->getUrl('*/*/saveElement',$_params);
		
		$_data['declinebtn'] = Mage::helper('cmsplus')->__('Decline editor changes');
		$fieldset->addField('declinebtn','buttoncmsplus',array(
			'class'		=>	'delete btn-delete',
			'name'		=>	'declinebtn',
			'required'	=>	false,
			'onclick'	=>	'cmsplus.doAction(\''.$link.'\'); return false;',
			'value'		=>	Mage::helper('cmsplus')->__('Decline editor changes'),
			
		));
		
		
		
		
	} elseif (!is_null($_data['editor_content'])) {
		$_data['content'] = $_data['editor_content'];
		$_data['elementcontent'] = $_data['editor_content'];
	}

	
	$widgetFilters = array();
	$wysiwygConfig = Mage::getSingleton('cms/wysiwyg_config')->getConfig(array('widget_filters' => $widgetFilters));
	$fieldset->addField('elementcontent', 'textarea', array(
		'label'     => Mage::helper('cmsplus')->__('Content'),
		'class'     => '',
		'required'  => false,
		'style'		=>	'height: 300px;',
		'name'      => 'settings[content]',
		'config'	=>	$wysiwygConfig,
	));

	

	if ($element) {
		$form->setValues($_data);
	}

      return parent::_prepareForm();
  }
  
}