<?php
/**
 * clever + zöger gmbh
 * http://www.clever-zoeger.de
 * @author Ronny Hempel <rhempel@clever-zoeger.de>
 * @version $Id: Formdefault.php 181 2011-09-12 14:37:18Z rhempel $
 * $HeadURL: https://svn.clever-zoeger.de/svn/Magento/extensions/Cz_Cmsplus/tags/1.3.0/cmsPlus/app/code/local/Cz/Cmsplus/Block/Adminhtml/Formdefault.php $
 * $LastChangedBy: rhempel $
 * $LastChangedRevision: 181 $
 * $LastChangedDate: 2011-09-12 16:37:18 +0200 (Mon, 12 Sep 2011) $
 */
?>
<?php
class Cz_Cmsplus_Block_Adminhtml_Formdefault extends Mage_Adminhtml_Block_Widget_Form {
	
	protected function _prepareLayout() {
		parent::_prepareLayout();
		if (Mage::helper('catalog')->isModuleEnabled('Mage_Cms')) {
			if (Mage::getSingleton('cms/wysiwyg_config')->isEnabled()) {
				$this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
			}
		}
	}
	
	protected function _getAdditionalElementTypes() {
		$result = array(
			'textarea'	=>	Mage::getConfig()->getHelperClassName('cmsplus/form_wysiwyg'),
			'blockcmsplus'	=>	Mage::getConfig()->getHelperClassName('cmsplus/form_blockcmsplus'),
			'buttoncmsplus'	=>	Mage::getConfig()->getHelperClassName('cmsplus/form_buttoncmsplus'),
			'elementcmsplus'	=>	Mage::getConfig()->getHelperClassName('cmsplus/form_elementcmsplus'),
			'imagecmsplus'	=>	Mage::getConfig()->getHelperClassName('cmsplus/form_imagecmsplus'),
			'script'	=>	Mage::getConfig()->getHelperClassName('cmsplus/form_script'),
		);
		return $result;
	}
}