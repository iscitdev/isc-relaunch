<?php
/**
 * clever + zöger gmbh
 * http://www.clever-zoeger.de
 * @author Ronny Hempel <rhempel@clever-zoeger.de>
 * @version $Id: Cmsplus.php 181 2011-09-12 14:37:18Z rhempel $
 * $HeadURL: https://svn.clever-zoeger.de/svn/Magento/extensions/Cz_Cmsplus/tags/1.3.0/cmsPlus/app/code/local/Cz/Cmsplus/Block/Cmsplus.php $
 * $LastChangedBy: rhempel $
 * $LastChangedRevision: 181 $
 * $LastChangedDate: 2011-09-12 16:37:18 +0200 (Mon, 12 Sep 2011) $
 */
?>
<?php
if (file_exists(dirname(__FILE__).'/Cmsplus_enc.php')) {
	require_once('Cmsplus_enc.php');
} else {
	require_once('Cz_Cmsplus_Block_Cmsplus_enc.php');
}
class Cz_Cmsplus_Block_Cmsplus extends Cz_Cmsplus_Block_Cmsplus_enc {
    protected function _prepareLayout() {
        return parent::_prepareLayout();
    }
	
	private function loadMenu() {
		return parent::loadMenu();
	}
	
	public function setMenuid($id) {
		return parent::setMenuid($id);
	}
	
	private function getError($action) {
		return parent::getError($action);
	}
	public function getMenu() {
		return parent::getMenu();
	}
	
	public function getPage() {
		return parent::getPage();
	}

	public function setPage($page) {
		return parent::setPage($page);
	}

}
