<?php
/**
 * clever + zöger gmbh
 * http://www.clever-zoeger.de
 * @author Ronny Hempel <rhempel@clever-zoeger.de>
 * @version $Id: Cmsplus_enc.php 447 2012-07-11 11:23:46Z rhempel $
 * $HeadURL: https://svn.clever-zoeger.de/svn/Magento/extensions/Cz_Cmsplus/tags/1.3.0/cmsPlus/app/code/local/Cz/Cmsplus/Block/Cmsplus_enc.php $
 * $LastChangedBy: rhempel $
 * $LastChangedRevision: 447 $
 * $LastChangedDate: 2012-07-11 13:23:46 +0200 (Wed, 11 Jul 2012) $
 */
?>
<?php

class Cz_Cmsplus_Block_Cmsplus_enc extends Mage_Core_Block_Template
{
    var $_menuId = null;
    var $_menu = null;
    private $_page = null;


    protected function _prepareLayout()
    {
        if (Mage::helper('cmsplus')->getStoreId() == '') {
            $storeId = Mage::app()->getStore()->getId();
            Mage::helper('cmsplus')->setStoreId($storeId);
        }
        if (!Mage::app()->getStore()->isAdmin()) {
            if ($this->getPage()) {
                $this->getLayout()->getBlock('head')->setTitle($this->getPage()->getPageTitle());

                if ($this->getPage()->getMetaDescription()) {
                    $this->getLayout()->getBlock('head')->setDescription($this->getPage()->getMetaDescription());
                }
                if ($this->getPage()->getMetaKeywords()) {
                    $this->getLayout()->getBlock('head')->setKeywords($this->getPage()->getMetaKeywords());
                }
            }
        }

        return parent::_prepareLayout();
    }

    private function loadMenu()
    {
        if (is_null($this->_menu)) {
            if (Mage::helper('cmsplus')->getStoreId() == 0) {
                Mage::helper('cmsplus')->setStoreId(Mage::app()->getStore()->getId());
            }
            $this->_menu = Mage::getModel('cmsplus/cz_cms_menu')->getCollection()->loadByMenuIdAndStore($this->_menuId);
        }
        return $this->_menu;
    }

    public function setMenuid($id)
    {
        $this->_menuId = $id;
    }

    private function getError($action)
    {
        switch ($action) {
            case 'menuid':
                throw new Exception(Mage::helper('cmsplus')->__('no menuid given'));
                break;

            default:
                throw new Exception(Mage::helper('cmsplus')->__('Error'));
                break;
        }
        die();
    }

    public function getMenu()
    {
        if (!$this->_menuId) return false;
        return $this->loadMenu();
    }

    public function getPage()
    {
        if (is_null($this->_page)) {
            $request = $this->getRequest();
            $path = trim($request->getPathInfo(), '/');
            $path = urldecode($path);
            $p = explode('/', $path);
            if (!empty($p[0])) {
                unset($p[0]);

                $params = $request->getParams();
                if (isset($params['preview']) && isset($params['hash'])) {
                    $page = Mage::getModel('cmsplus/cz_cms_content_page')->getCollection()->setHidden(0)->searchPage($p, 0);
                    if (!$page) return false;
                    if ($page->getPreviewkey() == $params['hash']) {
                        Mage::helper('cmsplus')->setPreview(1);
                        $this->setPage($page);
                    }
                } else {
                    $page = Mage::getModel('cmsplus/cz_cms_content_page')->getCollection()->searchPage($p, 1);
                    $this->setPage($page);
                }

                if (!$page) {
                    //forward 404
                    Mage::helper('cmsplus')->setError(1);
                }

            }
        }
        return $this->_page;
    }

    public function setPage($page)
    {
        if ($page) {
            Mage::helper('cmsplus')->setPageId($page->getId());
            $breadcrumbs = $this->getLayout()->getBlock('breadcrumbs');

            $infos = array('label' => Mage::helper('cms')->__('Home'), 'title' => Mage::helper('cms')->__('Go to Home Page'), 'link' => Mage::getBaseUrl());
            $breadcrumbs->addCrumb('Home', $infos);

            $pageBreadcrumb = $page->getBreadcrumb(array());
            $_count = 0;
            foreach ($pageBreadcrumb AS $_bread) {
                $_count++;
                if ($_count == count($pageBreadcrumb)) {
                    unset($_bread['link']);
                }
                if (isset($_bread['label'])) {
                    $breadcrumbs->addCrumb($_bread['label'], $_bread);
                }
            }
        }
        return $this->_page = $page;
    }

}
