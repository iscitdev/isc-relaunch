<?php
/**
 * clever + zöger gmbh
 * http://www.clever-zoeger.de
 * @author Ronny Hempel <rhempel@clever-zoeger.de>
 * @version $Id: Pager.php 204 2011-09-20 08:35:20Z rhempel $
 * $HeadURL: https://svn.clever-zoeger.de/svn/Magento/extensions/Cz_Cmsplus/tags/1.3.0/cmsPlus/app/code/local/Cz/Cmsplus/Block/Page/Html/Pager.php $
 * $LastChangedBy: rhempel $
 * $LastChangedRevision: 204 $
 * $LastChangedDate: 2011-09-20 10:35:20 +0200 (Tue, 20 Sep 2011) $
 */
if (file_exists(dirname(__FILE__).'/Pager_enc.php')) {
	require_once('Pager_enc.php');
} else {
	require_once('Cz_Cmsplus_Block_Page_Html_Pager_enc.php');
}
class Cz_Cmsplus_Block_Page_Html_Pager extends Cz_Cmsplus_Block_Page_Html_Pager_enc {

    public function getPagerUrl($params=array())
    {
		return parent::getPagerUrl($params);
    }

}
