<?php
/**
 * clever + zöger gmbh
 * http://www.clever-zoeger.de
 * @author Ronny Hempel <rhempel@clever-zoeger.de>
 * @version $Id: Pager_enc.php 204 2011-09-20 08:35:20Z rhempel $
 * $HeadURL: https://svn.clever-zoeger.de/svn/Magento/extensions/Cz_Cmsplus/tags/1.3.0/cmsPlus/app/code/local/Cz/Cmsplus/Block/Page/Html/Pager_enc.php $
 * $LastChangedBy: rhempel $
 * $LastChangedRevision: 204 $
 * $LastChangedDate: 2011-09-20 10:35:20 +0200 (Tue, 20 Sep 2011) $
 */
class Cz_Cmsplus_Block_Page_Html_Pager_enc extends Mage_Page_Block_Html_Pager {

    public function getPagerUrl($params=array())
    {
		$curUrl = explode('?', $this->helper('core/url')->getCurrentUrl());
		$curUrl = trim($curUrl[0],'/');
		$count = 0;
		foreach ($params AS $key => $param) {
			$count++;
			if ($count == 1) {
				$curUrl .= '?'.$key.'='.$param;
			} else {
				$curUrl .= '&'.$key.'='.$param;
			}
			
		}
		return $curUrl;
    }

}
