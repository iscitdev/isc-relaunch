<?php
/**
 * clever + zöger gmbh
 * http://www.clever-zoeger.de
 * @author Ronny Hempel <rhempel@clever-zoeger.de>
 * @version $Id: Catalog.php 220 2011-09-28 13:40:44Z rhempel $
 * $HeadURL: https://svn.clever-zoeger.de/svn/Magento/extensions/Cz_Cmsplus/tags/1.3.0/cmsPlus/app/code/local/Cz/Cmsplus/Helper/Catalog.php $
 * $LastChangedBy: rhempel $
 * $LastChangedRevision: 220 $
 * $LastChangedDate: 2011-09-28 15:40:44 +0200 (Wed, 28 Sep 2011) $
 */
if (file_exists(dirname(__FILE__).'/Catalog_enc.php')) {
	require_once('Catalog_enc.php');
} else {
	require_once('Cz_Cmsplus_Helper_Catalog_enc.php');
}
class Cz_Cmsplus_Helper_Catalog extends Cz_Cmsplus_Helper_Catalog_enc {

    /**
     * Return current category path or get it from current category
     * and creating array of categories|product paths for breadcrumbs
     *
     * @return string
     */
    public function getBreadcrumbPath() {
    	return parent::getBreadcrumbPath();
    }
}
