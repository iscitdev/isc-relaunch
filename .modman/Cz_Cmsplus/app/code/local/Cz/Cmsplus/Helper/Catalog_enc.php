<?php
/**
 * clever + zöger gmbh
 * http://www.clever-zoeger.de
 * @author Ronny Hempel <rhempel@clever-zoeger.de>
 * @version $Id: Catalog_enc.php 251 2011-10-26 14:59:38Z rhempel $
 * $HeadURL: https://svn.clever-zoeger.de/svn/Magento/extensions/Cz_Cmsplus/tags/1.3.0/cmsPlus/app/code/local/Cz/Cmsplus/Helper/Catalog_enc.php $
 * $LastChangedBy: rhempel $
 * $LastChangedRevision: 251 $
 * $LastChangedDate: 2011-10-26 16:59:38 +0200 (Wed, 26 Oct 2011) $
 */
class Cz_Cmsplus_Helper_Catalog_enc extends Mage_Catalog_Helper_Data {

    /**
     * Return current category path or get it from current category
     * and creating array of categories|product paths for breadcrumbs
     *
     * @return string
     */
    public function getBreadcrumbPath() {
        if (!$this->_categoryPath) {
            $path = array();
            $pagehasmenu = false;
            $block = Mage::getBlockSingleton("cmsplus/cmsplus");
            if(!$block || !$block->getPage()) return parent::getBreadcrumbPath();
            $page = $block->getPage();
			$path = $page->getBreadcrumb(array());
            if ($this->getProduct()) {
                $path['product'] = array('label'=>$this->getProduct()->getName());
            }
            $this->_categoryPath = $path;
        }
        return $this->_categoryPath;
    }

}
