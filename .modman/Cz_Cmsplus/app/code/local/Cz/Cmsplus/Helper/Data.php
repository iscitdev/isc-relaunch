<?php
/**
 * clever + zöger gmbh
 * http://www.clever-zoeger.de
 * @author Ronny Hempel <rhempel@clever-zoeger.de>
 * @version $Id: Data.php 202 2011-09-16 12:08:54Z rhempel $
 * $HeadURL: https://svn.clever-zoeger.de/svn/Magento/extensions/Cz_Cmsplus/tags/1.3.0/cmsPlus/app/code/local/Cz/Cmsplus/Helper/Data.php $
 * $LastChangedBy: rhempel $
 * $LastChangedRevision: 202 $
 * $LastChangedDate: 2011-09-16 14:08:54 +0200 (Fri, 16 Sep 2011) $
 */
?>
<?php
if (file_exists(dirname(__FILE__) . '/Data_enc.php')) {
    require_once('Data_enc.php');
} else {
    require_once('Cz_Cmsplus_Helper_Data_enc.php');
}

class Cz_Cmsplus_Helper_Data extends Cz_Cmsplus_Helper_Data_enc
{

    public function getUrl($url)
    {
        return parent::getUrl($url);
    }

    public function buildViewTab()
    {
        return parent::buildViewTab();
    }

    public function makeUrlKey($urlKey)
    {
        return parent::makeUrlKey($urlKey);
    }

    private function getPagesForMenu($menu, $parentId = 0, $level = 0)
    {
        return parent::getPagesForMenu($menu, $parentId, $level);
    }

    public function getMenutree($hidden = 1)
    {
        return parent::getMenutree($hidden);
    }

    public function getCurrentMenuId()
    {
        $request = Mage::app()->getRequest();
        $path = trim($request->getPathInfo(), '/');
        $path = urldecode($path);
        $p = explode('/', $path);

        if (!empty($p[0])) {
            unset($p[0]);
            $params = $request->getParams();
            $page = Mage::getModel('cmsplus/cz_cms_content_page')->getCollection()->searchPage($p, 1);

            if($page){
                $menu = Mage::getModel('cmsplus/cz_cms_menu')->getCollection()->loadByPageId($page->getId());
                $menuId = $menu->getData('cz_cms_menu_idcz_cms_menu');
                return $menuId;
            }
        }
        return null;
    }

    public function initData()
    {
        parent::initData();
    }

    public function __call($method, $args)
    {
        return parent::__call($method, $args);
    }

    public function saveData()
    {
        return parent::saveData();
    }
}

?>