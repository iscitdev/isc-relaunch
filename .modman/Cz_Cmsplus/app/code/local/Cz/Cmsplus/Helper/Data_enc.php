<?php
/**
 * clever + zöger gmbh
 * http://www.clever-zoeger.de
 * @author Ronny Hempel <rhempel@clever-zoeger.de>
 * @version $Id: Data_enc.php 659 2013-11-28 09:14:12Z tomasz $
 * $HeadURL: https://svn.clever-zoeger.de/svn/Magento/extensions/Cz_Cmsplus/tags/1.3.0/cmsPlus/app/code/local/Cz/Cmsplus/Helper/Data_enc.php $
 * $LastChangedBy: tomasz $
 * $LastChangedRevision: 659 $
 * $LastChangedDate: 2013-11-28 10:14:12 +0100 (Thu, 28 Nov 2013) $
 */
?>
<?php

class Cz_Cmsplus_Helper_Data_enc extends Mage_Core_Helper_Abstract {
	var $isMenu = 0;
	
	var $data = array();
	
	var $hidden = 1;
	
	var $countinlevel = array();
	
	var $countActInLevel = array();
	
	/*
Paul's Simple Diff Algorithm v 0.1
(C) Paul Butler 2007 <http://www.paulbutler.org/>
May be used and distributed under the zlib/libpng license.
This code is intended for learning purposes; it was written with short
code taking priority over performance. It could be used in a practical
application, but there are a few ways it could be optimized.
Given two arrays, the function diff will return an array of the changes.
I won't describe the format of the array, but it will be obvious
if you use print_r() on the result of a diff on some test data.
htmlDiff is a wrapper for the diff command, it takes two strings and
returns the differences in HTML. The tags used are <ins> and <del>,
which can easily be styled with CSS.
*/

function diff($old, $new){
    $matrix = array();
    $maxlen = 0;
	foreach($old as $oindex => $ovalue){
		$nkeys = array_keys($new, $ovalue);
		foreach($nkeys as $nindex){
			$matrix[$oindex][$nindex] = isset($matrix[$oindex - 1][$nindex - 1]) ?
				$matrix[$oindex - 1][$nindex - 1] + 1 : 1;
			if($matrix[$oindex][$nindex] > $maxlen){
				$maxlen = $matrix[$oindex][$nindex];
				$omax = $oindex + 1 - $maxlen;
				$nmax = $nindex + 1 - $maxlen;
			}
		}
	}
	if($maxlen == 0) return array(array('d'=>$old, 'i'=>$new));

	return array_merge(
		$this->diff(array_slice($old, 0, $omax), array_slice($new, 0, $nmax)),
		array_slice($new, $nmax, $maxlen),
		$this->diff(array_slice($old, $omax + $maxlen), array_slice($new, $nmax + $maxlen)));
}

function htmlDiff($old, $new){
	$diff = $this->diff(explode(' ', $old), explode(' ', $new));
    $ret = "";
	foreach($diff as $k){
		if(is_array($k))
		$ret .= (!empty($k['d'])?"<del>".implode(' ',$k['d'])."</del> ":'').
		(!empty($k['i'])?"<ins>".implode(' ',$k['i'])."</ins> ":'');
		else $ret .= $k . ' ';
	}
	return $ret;
}
	
	public function hasAccess($_id = false) {
		if ($this->isEditor()) {
			if ($_id === false) {
				$_id = Mage::helper('cmsplus')->getLastid();
			}
			if ($_id) {
				$page = Mage::getModel('cmsplus/cz_cms_content_page')->load($_id);
				if ($page) {
					$_data = $page->getData();
					$_data['access'] = unserialize($_data['access']);
					$roleId = Mage::getSingleton('admin/session')->getUser()->getRole()->getRoleId();
					if (in_array($roleId, $_data['access']['view']) || in_array($roleId, $_data['access']['viewedit'])) {
						return true;
					}
				}
			}
			return false;
		}
		return true;
	}
	
	public function hasAccessBlock(&$block) {
		if ($this->isEditor()) {
			if ($block) {
				if ($block) {
					$_data = $block->getData();
					$_data['access'] = unserialize($_data['access']);
					$roleId = Mage::getSingleton('admin/session')->getUser()->getRole()->getRoleId();
					if (in_array($roleId, $_data['access']['view']) || in_array($roleId, $_data['access']['viewedit'])) {
						return true;
					}
				}
			}
			return false;
		}
		return true;
	}
	public function isEditor() {
		$editors = explode(',',Mage::getStoreConfig('cz/cmsplus/editorgroup'));
		$roleId = Mage::getSingleton('admin/session')->getUser()->getRole()->getRoleId();
		if (in_array($roleId, $editors)) {
			return true;
		}
		return false;
	}

	public function error($text) {
		$this->log($text,3);
	}
	public function log($text,$level=6) {
		// 0: Emerg
		// 1: Alert
		// 2: Crit
		// 3: Err
		// 4: Warn
		// 5: Notice
		// 6: Info
		// 7: Debug
		Mage::log($text,$level,'cmsplus.log');
	}
	
	public function getUrl($url) {
		if(strpos($url,'http://')>0) return substr(strstr($url,'http://'),0,-1);
		elseif(strpos($url,':')>0) $url = str_replace('/','',str_replace(':','',strstr($url,':')));
		return Mage::getBaseUrl().$url; 
	}
	public function buildViewTab() {
		$content = '
			
		
		';
		return $content;
	}
	public function makeUrlKey($urlKey) {
		$replace = array(
			'ä'	=>	'ae',
			'ö'	=>	'oe',
			'ü'	=>	'ue',
			'\''	=>	'_',
			'"'	=>	'_',
			'&'	=>	'_',
			'ß'	=>	'ss',
			'à'	=>	'a',
			'ç'	=>	'c',
			'è'	=>	'e',
			'é'	=>	'e',
			'ê'	=>	'e',
		);
		$urlKey = str_replace(array_keys($replace),array_values($replace),$urlKey);
		return $urlKey;
	}
	private function getPagesForMenu($menu,$parentId=0,$level=0) {
		$content = '';
		$params = Mage::app()->getFrontController()->getRequest()->getParams();
		if (Mage::helper('cmsplus')->getAdditionalParams() && is_array(Mage::helper('cmsplus')->getAdditionalParams())) {
			$params = array_merge($params,Mage::helper('cmsplus')->getAdditionalParams());
		}
		$pages = Mage::getSingleton('cmsplus/cz_cms_content_page')->getCollection()->setHidden($this->hidden)->loadByMenuId($menu->getId(),$parentId);
		$level++;
		$this->countinlevel[$level] = count($pages);
		$_count = 0;

		foreach ($pages AS $_page) {
			$_count++;
			$this->countActInLevel[$level] = $_count;
			$_selected = '';
			if (isset($params) && isset($params['pageid']) && $_page->getId() == $params['pageid']) {
				$_selected = ' x-tree-selected';
			}
			
			if (isset($params) && isset($params['subpageid']) && $_page->getId() == $params['subpageid']) {
				$_selected = ' x-tree-selected';
			}
			$subpages = $this->getPagesForMenu($menu,$_page->getId(),$level);
			
			$active = 'active-category';
			if ($_page->getHidden() == 1 || !$this->hasAccess($_page->getId())) {
				$active = 'no-active-category';
			}
			$nolink = 0;
			if (!$this->hasAccess($_page->getId())) {
				$active = 'no-active-category';
				$nolink = 1;
			}
			$content .= '<li class="x-tree-node" level="'.$level.'" pageid="'.$_page->getId().'">
				<div class="x-tree-node-el folder '.$active.' x-tree-node-expanded'.$_selected.'">
					<span class="x-tree-node-indent">';
					for ($i=0;$i<=$level;$i++) {
						if ($_count >= $this->countinlevel[$i] && $i == $level && $subpages == '') {
							$content .= '<img class="x-tree-ec-icon x-tree-elbow-end" src="'.Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB).'/js/spacer.gif">';
						} elseif ($_count >= $this->countinlevel[$i] && $i == $level) {
							$content .= '<img class="x-tree-elbow-end" src="'.Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB).'/js/spacer.gif">';
						} elseif ($this->countActInLevel[$i] >= $this->countinlevel[$i] ) {
							$content .= '<img class="x-tree-ec-icon" src="'.Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB).'/js/spacer.gif">';
						} elseif ($level != $i) {
							$content .= '<img class="x-tree-elbow-line" src="'.Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB).'/js/spacer.gif">';
						} else {
							$content .= '<img class="x-tree-elbow" src="'.Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB).'/js/spacer.gif">';
						}
					}
			if ($nolink == 1) {
				$content .='
					</span>

					<img unselectable="on" class="x-tree-node-icon" src="'.Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB).'/js/spacer.gif">
						<a href="#">
						<span>'.$_page->getName().'</span>
						</a>
				</div>
				<ul style="" class="x-tree-node-ct connectedSortable">'.$subpages.'</ul>
				</li>';
			} else {
			$content .='
					</span>

					<img unselectable="on" class="x-tree-node-icon" src="'.Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB).'/js/spacer.gif">
					<a tabindex="1" href="'.Mage::helper('adminhtml')->getUrl('*/*/addpage',array('_current'=>false,'store' => Mage::helper('cmsplus')->getStoreId(),'menuid' => $_page->getData('cz_cms_menu_idcz_cms_menu'),'pageid' => $_page->getId())).'" hidefocus="on">
						<span>'.$_page->getName().'</span>
					</a>
				</div>
				<ul style="" class="x-tree-node-ct connectedSortable">'.$subpages.'</ul>
				</li>';
			}
		}
		$level--;
		return $content;
	}
	public function getMenutree($hidden=1) {
		$this->hidden = $hidden;
		$content = '<ul class="x-tree-root-ct x-tree-lines connectedSortable cmsplusmenu">
        		<div class="x-tree-root-node">
        			';
		
		$params = Mage::app()->getFrontController()->getRequest()->getParams();
		if (Mage::helper('cmsplus')->getAdditionalParams() && is_array(Mage::helper('cmsplus')->getAdditionalParams())) {
			$params = array_merge($params,Mage::helper('cmsplus')->getAdditionalParams());
		}
		if ($params && isset($params['store'])) {
			Mage::helper('cmsplus')->setStoreId($params['store']);
		}
		$menucollection = Mage::getSingleton('cmsplus/cz_cms_menu')->getCollection()->setHidden($this->hidden)->prepareSummary();
		if ($menucollection) {
			$maxcount = count($menucollection);
			$this->countinlevel[0] = $maxcount;
			$_count = 0;
			foreach ($menucollection AS $_menu) {
				$_count++;
				$this->countActInLevel[0] = $_count;
				$_selected = '';
				
				if (isset($params) && isset($params['menuid']) && !isset($params['pageid']) && !isset($params['subpageid']) && $_menu->getId() == $params['menuid']) {
					$_selected = ' x-tree-selected';
				}
				$content .= '
				<li class="x-tree-node notsortable">
					<div class="x-tree-node-el folder active-category x-tree-node-expanded'.$_selected.'">
        					<span class="x-tree-node-indent"></span>
        					<img class="x-tree-ec-icon x-tree-elbow';
        		if ($_count >= $maxcount) {
        			$content .= '-end';
				}
        		
        		$content .= '" src="'.Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB).'/js/spacer.gif">
        					<img unselectable="on" class="x-tree-node-icon" src="'.Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB).'/js/spacer.gif">
        					<a tabindex="1" href="'.Mage::helper("adminhtml")->getUrl('*/*/addmenu',array('_current'=>false,'store' => Mage::helper('cmsplus')->getStoreId(),'menuid' => $_menu->getId())).'" hidefocus="on">
        						<span unselectable="on" id="extdd-2">'.$_menu->getName().'</span>
        					</a>
        				</div>
        				<ul style="" class="x-tree-node-ct connectedSortable issortable">'.$this->getPagesForMenu($_menu,0,0).'</ul>
        		</li>
        		';
			}
			
		}
		
		$content .= '</div></ul>';
		
		$content .= '<script>
			var sorturl = "'.Mage::helper('adminhtml')->getUrl('*/*/setOrder',array('_current'=>false,'type' => 'page')).'";
		
			</script>';
		
		return $content;
	}
	
	public function initData() {
		if (Mage::registry('cmsplus_data')) {
			$this->data = unserialize(Mage::registry('cmsplus_data'));
		} else {
			$this->data = array();
		}
	}
	
	public function __call($method,$args) {
		if (substr($method,0,3) == 'set') {
			$method = str_replace('set','',$method);
			$this->initData();
			$this->data[$method] = $args[0];
			$this->saveData();
			return $this->data[$method];
		} elseif (substr($method,0,3) == 'get') {
			$this->initData();
			$method = str_replace('get','',$method);
			if (isset($this->data[$method])) {
				return $this->data[$method];
			} else {
				$this->data[$method] = '';
				return $this->data[$method];
			}
		}
		return false;
	}
	
	public function saveData() {
		if (Mage::registry('cmsplus_data')) {
			Mage::unregister('cmsplus_data');
		}
		Mage::register('cmsplus_data', serialize($this->data));
	}
}
?>
