<?php
/**
 * clever + zöger gmbh
 * http://www.clever-zoeger.de
 * @author Ronny Hempel <rhempel@clever-zoeger.de>
 * @version $Id: Blockcmsplus_enc.php 266 2011-11-21 14:14:51Z rhempel $
 * $HeadURL: https://svn.clever-zoeger.de/svn/Magento/extensions/Cz_Cmsplus/tags/1.3.0/cmsPlus/app/code/local/Cz/Cmsplus/Helper/Form/Blockcmsplus_enc.php $
 * $LastChangedBy: rhempel $
 * $LastChangedRevision: 266 $
 * $LastChangedDate: 2011-11-21 15:14:51 +0100 (Mon, 21 Nov 2011) $
 */
?>
<?php

class Cz_Cmsplus_Helper_Form_Blockcmsplus_enc extends Varien_Data_Form_Element_Abstract
{
    public function __construct($attributes=array())
    {
        parent::__construct($attributes);
        $this->setType('note');
        //$this->setExtType('textfield');
    }
	public function getHtml() {
		$content = '';
		if ($block = $this->getBlock()) {
			$columns = $block->getData('cz_cms_type_idcz_cms_content_block_type');
			$content .= '<div class="blockcmsplus';
			if ($block->getHidden()) {
				$content .= ' ishidden';
			}
			
			$content .= ' columns'.$block->getData('cz_cms_type_idcz_cms_content_block_type').'">';
			$content .= '<div class="title">'.$block->getName().'</div>';
			for ($i=1;$i<=$columns;$i++) {
				$elements = Mage::getModel('cmsplus/cz_cms_content_element')->getCollection()->setHidden(0)->loadByBlockIdAndSortOrder($block->getId(),$i);
				$content .= '<div class="innerblock innerblock-'.$i.'">';
				if ($elements) {
					foreach ($elements AS $element) {
						$content .= '<div class="title">'.$element->getName().'</div>';
					}
				}
				$content .= '</div>';
			}
			$content .= '<div class="buttons">';
			
			$_params = array();
			$_params['_current'] 	=	false;
			$_params['type'] 		= 	'block';
			$params = Mage::app()->getFrontController()->getRequest()->getParams();
			if (isset($params) && isset($params['menuid'])) {
				$_params['menuid'] = $params['menuid'];
			}
			
			if (isset($params) && isset($params['pageid'])) {
				$_params['pageid'] = $params['pageid'];
			}
			$_params['pageid']		=	$block->getData('cz_cms_content_page_idcz_cms_content_page');
			$_params['blockid']		=	$block->getId();
			$_params['store'] = Mage::helper('cmsplus')->getStoreId();
			$_params['direction']	=	'up';

			if (!Mage::helper('cmsplus')->isEditor()) {
				$link = Mage::helper('adminhtml')->getUrl('*/*/setOrder',$_params);
				$content .= '<button class="btn-up" onclick="cmsplus.doAction(\''.$link.'\'); return false;"><span>'.Mage::helper('cmsplus')->__('up').'</span></button>';
				
				$_params['direction']	=	'down';
				$link = Mage::helper('adminhtml')->getUrl('*/*/setOrder',$_params);
				$content .= '<button class="btn-down" onclick="cmsplus.doAction(\''.$link.'\'); return false;"><span>'.Mage::helper('cmsplus')->__('down').'</span></button>';
			}
			$_params = array();
			$_params['_current'] 	=	false;
			$_params['blockid']		=	$block->getId();
			$_params['store'] = Mage::helper('cmsplus')->getStoreId();
			
			$params = Mage::app()->getFrontController()->getRequest()->getParams();
			if (isset($params) && isset($params['menuid'])) {
				$_params['menuid'] = $params['menuid'];
			}
			
			if (isset($params) && isset($params['pageid'])) {
				$_params['pageid'] = $params['pageid'];
			}

			$link = Mage::helper('adminhtml')->getUrl('*/*/addBlock',$_params);
			$content .= '<button class="edit btn-edit" onclick="cmsplus.doAction(\''.$link.'\'); return false;"><span>'.Mage::helper('cmsplus')->__('edit').'</span></button>';
			if (!Mage::helper('cmsplus')->isEditor()) {
				$_params = array();
				$_params['_current'] 	=	false;
				$_params['blockid']		=	$block->getId();
				$_params['store'] = Mage::helper('cmsplus')->getStoreId();
				$params = Mage::app()->getFrontController()->getRequest()->getParams();
				if (isset($params) && isset($params['menuid'])) {
					$_params['menuid'] = $params['menuid'];
				}
				
				if (isset($params) && isset($params['pageid'])) {
					$_params['pageid'] = $params['pageid'];
				}
				$_params['pageid']		=	$block->getData('cz_cms_content_page_idcz_cms_content_page');
				$link = Mage::helper('adminhtml')->getUrl('*/*/deleteBlock',$_params);
				$content .= '<script>var deleteMsg="'.Mage::helper('cmsplus')->__('Are you sure you want to delete it?').'";</script>';
				$content .= '<button class="delete btn-delete" onclick="cmsplus.doDelete(\''.$link.'\'); return false;"><span>'.Mage::helper('cmsplus')->__('delete').'</span></button>';
			}			
			
			
			$content .= '</div>';
			$content .= '</div>';
		}
		
		return $content;
	}

}