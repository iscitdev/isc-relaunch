<?php
/**
 * clever + zöger gmbh
 * http://www.clever-zoeger.de
 * @author Ronny Hempel <rhempel@clever-zoeger.de>
 * @version $Id: Buttoncmsplus.php 181 2011-09-12 14:37:18Z rhempel $
 * $HeadURL: https://svn.clever-zoeger.de/svn/Magento/extensions/Cz_Cmsplus/tags/1.3.0/cmsPlus/app/code/local/Cz/Cmsplus/Helper/Form/Buttoncmsplus.php $
 * $LastChangedBy: rhempel $
 * $LastChangedRevision: 181 $
 * $LastChangedDate: 2011-09-12 16:37:18 +0200 (Mon, 12 Sep 2011) $
 */
?>
<?php

class Cz_Cmsplus_Helper_Form_Buttoncmsplus extends Varien_Data_Form_Element_Abstract
{
    public function __construct($attributes=array())
    {
        parent::__construct($attributes);
        $this->setType('button');
        //$this->setExtType('textfield');
    }

    public function getElementHtml()
    {
        $html = '<button onclick="'.$this->getOnclick().'" class="'.$this->getClass().'" id="' . $this->getHtmlId() . '"><span>' . $this->getValue() . '</span></button>';
        $html.= $this->getAfterElementHtml();
        return $html;
    }
}