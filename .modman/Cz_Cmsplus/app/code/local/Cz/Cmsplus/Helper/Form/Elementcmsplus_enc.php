<?php
/**
 * clever + zöger gmbh
 * http://www.clever-zoeger.de
 * @author Ronny Hempel <rhempel@clever-zoeger.de>
 * @version $Id: Elementcmsplus_enc.php 266 2011-11-21 14:14:51Z rhempel $
 * $HeadURL: https://svn.clever-zoeger.de/svn/Magento/extensions/Cz_Cmsplus/tags/1.3.0/cmsPlus/app/code/local/Cz/Cmsplus/Helper/Form/Elementcmsplus_enc.php $
 * $LastChangedBy: rhempel $
 * $LastChangedRevision: 266 $
 * $LastChangedDate: 2011-11-21 15:14:51 +0100 (Mon, 21 Nov 2011) $
 */
?>
<?php

class Cz_Cmsplus_Helper_Form_Elementcmsplus_enc extends Varien_Data_Form_Element_Abstract
{
    public function __construct($attributes=array())
    {
        parent::__construct($attributes);
        $this->setType('note');
        //$this->setExtType('textfield');
    }
	public function getHtml() {
		$content = '';
		$block = $this->getBlock();
		if ($element = $this->getElement()) {
			$content .= '<div class="elementcmsplus column'.$element->getSortOrder().'">';
			
			
			$content .= '<div class="innerelement">'.$element->getName().'</div>';
			
			$content .= '<div class="buttons">';
			
			$_params = array();
			$_params['_current'] 	=	false;
			$_params['type'] 		= 	'element';
			$_params['elementid']	=	$element->getId();
			$_params['store'] = Mage::helper('cmsplus')->getStoreId();
			$params = Mage::app()->getFrontController()->getRequest()->getParams();
			if (isset($params) && isset($params['menuid'])) {
				$_params['menuid'] = $params['menuid'];
			}
			
			if (isset($params) && isset($params['pageid'])) {
				$_params['pageid'] = $params['pageid'];
			}
			$_params['blockid']		=	$element->getData('cz_cms_content_block_idcz_cms_content_block');
			$_params['direction']	=	'up';
			if (!Mage::helper('cmsplus')->isEditor()) {
				if ($element->getSortOrder() > 1) {
					$link = Mage::helper('adminhtml')->getUrl('*/*/setOrder',$_params);
					$content .= '<button class="btn-up" onclick="cmsplus.doAction(\''.$link.'\'); return false;"><span>'.Mage::helper('cmsplus')->__('left').'</span></button>';
				}
				
				if ($element->getSortOrder() < $block->getData('cz_cms_type_idcz_cms_content_block_type')) {
				
					$_params['direction']	=	'down';
					$link = Mage::helper('adminhtml')->getUrl('*/*/setOrder',$_params);
					$content .= '<button class="btn-down" onclick="cmsplus.doAction(\''.$link.'\'); return false;"><span>'.Mage::helper('cmsplus')->__('right').'</span></button>';
				}
			}		
			$_params = array();
			$_params['_current'] 	=	false;
			$_params['store'] = Mage::helper('cmsplus')->getStoreId();
			
			$_params['elementid']	=	$element->getId();
			$params = Mage::app()->getFrontController()->getRequest()->getParams();
			if (isset($params) && isset($params['menuid'])) {
				$_params['menuid'] = $params['menuid'];
			}
			
			if (isset($params) && isset($params['pageid'])) {
				$_params['pageid'] = $params['pageid'];
			}
			$_params['blockid']		=	$element->getData('cz_cms_content_block_idcz_cms_content_block');
			$link = Mage::helper('adminhtml')->getUrl('*/*/editElement',$_params);
			$content .= '<button class="edit btn-edit" onclick="cmsplus.doAction(\''.$link.'\'); return false;"><span>'.Mage::helper('cmsplus')->__('edit').'</span></button>';
			if (!Mage::helper('cmsplus')->isEditor()) {
				$_params = array();
				$_params['_current'] 	=	false;
				$_params['store'] = Mage::helper('cmsplus')->getStoreId();
				$_params['elementid']	=	$element->getId();
				$params = Mage::app()->getFrontController()->getRequest()->getParams();
				if (isset($params) && isset($params['menuid'])) {
					$_params['menuid'] = $params['menuid'];
				}
				
				if (isset($params) && isset($params['pageid'])) {
					$_params['pageid'] = $params['pageid'];
				}
				$_params['blockid']		=	$element->getData('cz_cms_content_block_idcz_cms_content_block');
				$link = Mage::helper('adminhtml')->getUrl('*/*/emptyElement',$_params);
				$content .= '<button class="delete btn-delete" onclick="cmsplus.doAction(\''.$link.'\'); return false;"><span>'.Mage::helper('cmsplus')->__('clear').'</span></button>';
			}
			
			
			$content .= '</div>';
			$content .= '</div>';
		}
		
		return $content;
	}

}