<?php
/**
 * clever + zöger gmbh
 * http://www.clever-zoeger.de
 * @author Ronny Hempel <rhempel@clever-zoeger.de>
 * @version $Id: Imagecmsplus.php 181 2011-09-12 14:37:18Z rhempel $
 * $HeadURL: https://svn.clever-zoeger.de/svn/Magento/extensions/Cz_Cmsplus/tags/1.3.0/cmsPlus/app/code/local/Cz/Cmsplus/Helper/Form/Imagecmsplus.php $
 * $LastChangedBy: rhempel $
 * $LastChangedRevision: 181 $
 * $LastChangedDate: 2011-09-12 16:37:18 +0200 (Mon, 12 Sep 2011) $
 */
?>
<?php

if (file_exists(dirname(__FILE__).'/Imagecmsplus_enc.php')) {
	require_once('Imagecmsplus_enc.php');
} else {
	require_once('Cz_Cmsplus_Helper_Form_Imagecmsplus_enc.php');
}
class Cz_Cmsplus_Helper_Form_Imagecmsplus extends Cz_Cmsplus_Helper_Form_Imagecmsplus_enc
{
    public function __construct($attributes=array())
    {
        parent::__construct($attributes);
    }
	public function getHtml() {
		return parent::getHtml();
	}

}