<?php
/**
 * clever + zöger gmbh
 * http://www.clever-zoeger.de
 * @author Ronny Hempel <rhempel@clever-zoeger.de>
 * @version $Id: Imagecmsplus_enc.php 189 2011-09-15 13:50:40Z rhempel $
 * $HeadURL: https://svn.clever-zoeger.de/svn/Magento/extensions/Cz_Cmsplus/tags/1.3.0/cmsPlus/app/code/local/Cz/Cmsplus/Helper/Form/Imagecmsplus_enc.php $
 * $LastChangedBy: rhempel $
 * $LastChangedRevision: 189 $
 * $LastChangedDate: 2011-09-15 15:50:40 +0200 (Thu, 15 Sep 2011) $
 */
?>
<?php
class Cz_Cmsplus_Helper_Form_Imagecmsplus_enc extends Varien_Data_Form_Element_Abstract
{
    public function __construct($attributes=array())
    {
        parent::__construct($attributes);
        $this->setType('note');
        //$this->setExtType('textfield');
    }
	public function getHtml() {
		$content = '';
		if ($image = $this->getImage()) {
			$content .= '<div class="imagecmsplus column'.$image->getSortOrder().'">';
			
			
			$content .= '<div class="innerelement';
			
			if ($image->getHidden()) {
				$content .= ' ishidden';
			}
			$content .= '">';
			
			$content .= '<div class="title">'.$image->getName().'</div>';
			$content .= '<div class="image">';
			
			//<img src="'.Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA).'/'.$image->getFile().'">
			$content .= $image->resize(null,100);
			$content .= '</div>';
			
			
			$content .= '<div class="buttons">';
			
			$_params = array();
			$_params['_current'] 	=	false;
			$_params['type'] 		= 	'image';
			$_params['pageid']		=	$image->getData('cz_cms_content_page_idcz_cms_content_page');
			$_params['imageid']		=	$image->getId();
			$_params['typeid']		=	$image->getData('cz_cms_type_idcz_cms_content_block_type');
			$_params['direction']	=	'up';

			$link = Mage::helper('adminhtml')->getUrl('*/*/setOrder',$_params);
			$content .= '<button class="btn-up" onclick="cmsplus.doAction(\''.$link.'\'); return false;"><span>'.Mage::helper('cmsplus')->__('up').'</span></button>';
			
			$_params['direction']	=	'down';
			$link = Mage::helper('adminhtml')->getUrl('*/*/setOrder',$_params);
			$content .= '<button class="btn-down" onclick="cmsplus.doAction(\''.$link.'\'); return false;"><span>'.Mage::helper('cmsplus')->__('down').'</span></button>';
			
			$_params = array();
			$_params['_current'] 	=	false;
			
			$_params['pageid']	=	$image->getData('cz_cms_content_page_idcz_cms_content_page');
			$_params['imageid']		=	$image->getId();
			$_params['typeid']		=	$image->getData('cz_cms_type_idcz_cms_content_block_type');
			
			$link = Mage::helper('adminhtml')->getUrl('*/*/addImage',$_params);
			$content .= '<button class="edit btn-edit" onclick="cmsplus.doAction(\''.$link.'\'); return false;"><span>'.Mage::helper('cmsplus')->__('edit').'</span></button>';
			
			$_params = array();
			$_params['_current'] 	=	false;
			$_params['pageid']	=	$image->getData('cz_cms_content_page_idcz_cms_content_page');
			$_params['imageid']		=	$image->getId();
			$_params['typeid']		=	$image->getData('cz_cms_type_idcz_cms_content_block_type');
			
			$link = Mage::helper('adminhtml')->getUrl('*/*/deleteImage',$_params);
			$content .= '<script>var deleteMsg="'.Mage::helper('cmsplus')->__('Are you sure you want to delete it?').'";</script>';
			$content .= '<button class="delete btn-delete" onclick="cmsplus.doDelete(\''.$link.'\'); return false;"><span>'.Mage::helper('cmsplus')->__('delete').'</span></button>';
			$content .= '</div>';
			
			
			$content .= '</div>';
			$content .= '</div>';
		}
		
		return $content;
	}

}