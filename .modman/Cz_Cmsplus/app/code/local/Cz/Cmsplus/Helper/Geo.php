<?php

/**
 * Created by PhpStorm.
 * User: rhutterer
 * Date: 22.07.14
 * Time: 07:58
 */
class Cz_Cmsplus_Helper_Geo extends Cyberhouse_Geo_Helper_Data
{

    /**
     * checks if the current page is a cms page and return the configured url to given $store
     * @param null $store
     * @return string
     */
    public function getStoreCurrentUrl($store = null)
    {
        $currentStore = Mage::app()->getStore();
        $currentStoreCode = $currentStore->getCode();

        if($currentStore->getId() != $store->getId() && Mage::app()->getRequest()->getModuleName() == 'cmsplus') {
            $request = Mage::app()->getRequest();
            $path = urldecode(trim($request->getPathInfo(), '/'));
            $p = explode('/', $path);
            if(!empty($p[0])) {
                unset($p[0]);
            }
            $page = Mage::getModel('cmsplus/cz_cms_content_page')->getCollection()->searchPage($p, 1);
            if($page) {

                //get configured english page
                $mappedPageId = $page->getData('mapped_page_id');
                if($mappedPageId != null) {
                    $mappedPage = Mage::getModel('cmsplus/cz_cms_content_page')->load($mappedPageId);
                    $mappedStoreViewId = $mappedPage->getStoreviewCode();
                    if($mappedStoreViewId == 0) {
                        $storeCodes = Mage::app()->getWebsite()->getStoreCodes();
                        foreach($storeCodes as $storeCode) {
                            if($storeCode != $currentStoreCode) {
                                $otherStoreViewCode = $storeCode;
                            }
                        }
                    } else {
                        $otherStoreViewCode = Mage::app()->getStore($mappedStoreViewId)->getCode();
                    }
                    //replace current storeviewCode in url with the one from loaded
                    $url = str_replace($currentStoreCode, $otherStoreViewCode, $mappedPage->getUrl());
                    return $url;
                }
            }
        }
        return parent::getStoreCurrentUrl($store);
    }
}