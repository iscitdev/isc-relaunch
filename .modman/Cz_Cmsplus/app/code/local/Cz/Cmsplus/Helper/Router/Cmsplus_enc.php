<?php
/**
 * clever + zöger gmbh
 * http://www.clever-zoeger.de
 * @author Ronny Hempel <rhempel@clever-zoeger.de>
 * @version $Id: Cmsplus_enc.php 181 2011-09-12 14:37:18Z rhempel $
 * $HeadURL: https://svn.clever-zoeger.de/svn/Magento/extensions/Cz_Cmsplus/tags/1.3.0/cmsPlus/app/code/local/Cz/Cmsplus/Helper/Router/Cmsplus_enc.php $
 * $LastChangedBy: rhempel $
 * $LastChangedRevision: 181 $
 * $LastChangedDate: 2011-09-12 16:37:18 +0200 (Mon, 12 Sep 2011) $
 */
?>
<?php

class Cz_Cmsplus_Helper_Router_Cmsplus_enc extends Mage_Core_Controller_Varien_Router_Abstract {

    public function match(Zend_Controller_Request_Http $request) {
        $moduleName     = 'cmsplus';
        $controllerName = 'index';
        $actionName     = 'index';

		$path = trim($request->getPathInfo(), '/');
		
		
		if ($path) {
			$p = explode('/', $path);
		} else {
			return false;
		}

		if (!empty($p[0])) {
			$module = $p[0];
			$storeId = Mage::app()->getStore()->getId();
			$urlKey = trim(Mage::getStoreConfig('cz/cmsplus/urlkey',$storeId));
			if ($module != $urlKey) {
				return false;
			}
		} else {
			return false;
		}

        $request->setModuleName($moduleName)
            ->setControllerName($controllerName)
            ->setActionName($actionName);

        return true;
    }
}
