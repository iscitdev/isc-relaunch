<?php
/**
 * clever + zöger gmbh
 * http://www.clever-zoeger.de
 * @author Ronny Hempel <rhempel@clever-zoeger.de>
 * @version $Id: Category.php 181 2011-09-12 14:37:18Z rhempel $
 * $HeadURL: https://svn.clever-zoeger.de/svn/Magento/extensions/Cz_Cmsplus/tags/1.3.0/cmsPlus/app/code/local/Cz/Cmsplus/Model/Category.php $
 * $LastChangedBy: rhempel $
 * $LastChangedRevision: 181 $
 * $LastChangedDate: 2011-09-12 16:37:18 +0200 (Mon, 12 Sep 2011) $
 */
?>
<?php

class Cz_Cmsplus_Model_Category extends Varien_Object {
	static $_values = array();
	static $_leer = '----------------------------------------------------------------';
	
	public function __construct() {
		$storeId = Mage::app()->getStore()->getId();
		$rootId = Mage::app()->getWebsite(true)->getDefaultStore()->getRootCategoryId();
		$category     = Mage::getModel('catalog/category');
		$category->setStoreId($storeId);
		$category->load($rootId);
		$catData = array();
		$catData[] = array(
			'label'	=>	'',
			'value'	=>	'',
		);
		$this->buildCategories($category->getChildrenCategories(),$catData,0);
		self::$_values = $catData;
	}
	
	private function buildCategories($in_Categories,&$catData,$level) {
		$leer = $level*3;
		$leer = substr(self::$_leer,0,$leer).' ';
		foreach ($in_Categories AS $cat) {
			$catData[] = array(
				'label'	=>	$leer." ".$cat->getName(),
				'value'	=>	$cat->getId(),
			);
			if (count($cat->getChildrenCategories()) > 0) {
				$this->buildCategories($cat->getChildrenCategories(),$catData,++$level);
				$level--;
			}
		}
	}
	
	static public function getOptionArray() {
		$_out = array();
		foreach (self::$_values AS $val) {
			$_out[$val['value']] = $val['label'];
		}
		return $_out;
	}
	
	static public function getValueArray() {
		return self::$_values;
	}
}

?>