<?php
/**
 * clever + zöger gmbh
 * http://www.clever-zoeger.de
 * @author Ronny Hempel <rhempel@clever-zoeger.de>
 * @version $Id: Block.php 251 2011-10-26 14:59:38Z rhempel $
 * $HeadURL: https://svn.clever-zoeger.de/svn/Magento/extensions/Cz_Cmsplus/tags/1.3.0/cmsPlus/app/code/local/Cz/Cmsplus/Model/Cz/Cms/Content/Block.php $
 * $LastChangedBy: rhempel $
 * $LastChangedRevision: 251 $
 * $LastChangedDate: 2011-10-26 16:59:38 +0200 (Wed, 26 Oct 2011) $
 */
?>
<?php
class Cz_Cmsplus_Model_Cz_Cms_Content_Block extends Mage_Core_Model_Abstract {
	private $data = array();
	var $storeId = null;
	
    public function _construct()
    {
    	parent::_construct();
        $this->_init('cmsplus/cz_cms_content_block');
    }	
	
	public function deleteRecursive() {
		$this->delete();
	}
	
	public function delete() {
		$elements = Mage::getSingleton('cmsplus/cz_cms_content_element')->getCollection()->loadByBlockId($this->getId());
		if ($elements) {
			foreach ($elements AS $_element) {
				$_element->delete();
			}
		}
		
		parent::delete();
	}
	
	public function getLeft() {
		if (!isset($this->data[1])) {
			$this->data[1] = Mage::getModel('cmsplus/cz_cms_content_element')->getCollection()->getOneByBlockAndSortOrder($this,1);
		}
		if (isset($this->data[1]) && is_object($this->data[1])) {	
			return $this->renderContent($this->data[1]);//return Mage::helper('cms')->getBlockTemplateProcessor()->filter($this->data[1]->getContent());
		}
		return '';
	}
	public function getMiddle() {
		if (!isset($this->data[2])) {
			$this->data[2] = Mage::getModel('cmsplus/cz_cms_content_element')->getCollection()->getOneByBlockAndSortOrder($this,2);
		}
		if (isset($this->data[2]) && is_object($this->data[2])) {
			return $this->renderContent($this->data[2]);//return Mage::helper('cms')->getBlockTemplateProcessor()->filter($this->data[2]->getContent());
		}
		return '';
	}
	public function getRight() {
		if (!isset($this->data[3])) {
			$this->data[3] = Mage::getModel('cmsplus/cz_cms_content_element')->getCollection()->getOneByBlockAndSortOrder($this,3);
		}
		if (isset($this->data[3]) && is_object($this->data[3])) {
			return $this->renderContent($this->data[3]);//Mage::helper('cms')->getBlockTemplateProcessor()->filter($this->data[3]->getContent());
		}
		return '';
	}
	
	public function renderContent($element) {
		$elementtemplate = 'element.phtml';
		$element->storeId = Mage::helper('cmsplus')->getStoreId();
		$block = Mage::app()->getLayout()->createBlock(
			'cmsplus/cmsplus',
			'cmspluscontent',
			array(
				'template' => 'cz/cmsplus/'.$elementtemplate,
			)
		)->setData('element',$element)->setOrder($element->getSortOrder());
		return $block->toHtml();
	}
	
	public function render() {
		switch ($this->getData('cz_cms_type_idcz_cms_content_block_type')) {
			case '3':
				$blocktemplate = 'block3.phtml';
			break;
			case '2':
				$blocktemplate = 'block2.phtml';
			break;
			case '1':
				$blocktemplate = 'block1.phtml';
			break;
			default:
				$blocktemplate = 'block'.$this->getData('cz_cms_type_idcz_cms_content_block_type').'.phtml';
			break;
		}

		$this->storeId = Mage::helper('cmsplus')->getStoreId();
		$block = Mage::app()->getLayout()->createBlock(
			'cmsplus/cmsplus',
			'cmsplusblock',
			array(
				'template' => 'cz/cmsplus/'.$blocktemplate,
			)
		)->setData('block',$this)->setOrder($this->getSortOrder());
		return $block->toHtml();
	}
}