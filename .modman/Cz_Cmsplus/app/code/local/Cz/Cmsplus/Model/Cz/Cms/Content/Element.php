<?php
/**
 * clever + zöger gmbh
 * http://www.clever-zoeger.de
 * @author Ronny Hempel <rhempel@clever-zoeger.de>
 * @version $Id: Element.php 266 2011-11-21 14:14:51Z rhempel $
 * $HeadURL: https://svn.clever-zoeger.de/svn/Magento/extensions/Cz_Cmsplus/tags/1.3.0/cmsPlus/app/code/local/Cz/Cmsplus/Model/Cz/Cms/Content/Element.php $
 * $LastChangedBy: rhempel $
 * $LastChangedRevision: 266 $
 * $LastChangedDate: 2011-11-21 15:14:51 +0100 (Mon, 21 Nov 2011) $
 */
?>
<?php
class Cz_Cmsplus_Model_Cz_Cms_Content_Element extends Mage_Core_Model_Abstract {
	var $storeId = null;
	
    public function _construct()
    {
    	parent::_construct();
        $this->_init('cmsplus/cz_cms_content_element');
    }	
	
	public function getContent() {
		
		if ($this->getData('cz_cms_type_idcz_cms_content_block_type') == 6) {
			if (Mage::helper('cmsplus')->getPreview() == 1 && !is_null($this->getEditorContent())) {
				$content = $this->getEditorContent();
			} else {
				$content = parent::getContent();
			}
			if (trim($content) == '') {
				$content = '&nbsp;';
			}
			return $content;
		} elseif ($this->getData('cz_cms_type_idcz_cms_content_block_type') == 7) {
			$_el = Mage::getModel('cmsplus/cz_cms_content_element')->load($this->getId());
			$_catIds = $_el->getCategories();
			//$_catIds = implode(',', $_catIds);
			$content = '';
			$block = Mage::app()->getLayout()->createBlock(
					'catalog/product_list',
					'catprodlist',
					array(
						'template'		=>	'catalog/product/list.phtml',
						'category_id'	=>	$_catId,
						'toolbar_block_name'	=>	'product_list_toolbar',
					)
				);
			foreach ($_catIds AS $_catId) {
				$block = Mage::app()->getLayout()->createBlock(
					'catalog/product_list',
					'catprodlist',
					array(
						'template'		=>	'catalog/product/list.phtml',
						'category_id'	=>	$_catId,
						'toolbar_block_name'	=>	'product_list_toolbar',
					)
				);
				
				//$toolbar = $block->getToolbarBlock();
				//$collection = $block->getLoadedProductCollection();
				//$toolbar->setCollection($collection);
				
				$pager = Mage::app()->getLayout()->createBlock(
					/*'page/html_pager',*/
					'cmsplus/page_html_pager',
					'pager',
					array(
					)
				);

				$toolbar = Mage::app()->getLayout()->createBlock(
					'catalog/product_list_toolbar',
					'product_list_toolbar',
					array(
						'template'		=>	'catalog/product/list/toolbar.phtml',
					)
				);
				$toolbar->setChild('product_list_toolbar_pager', $pager);
				$block->setChild('product_list_toolbar', $toolbar);
				$block->setChild('toolbar', $toolbar);
				
				
				//$toolbar->setChild('product_list_toolbar_pager',$pager);
				
				//var_dump($toolbar->getChild('product_list_toolbar_pager'));
				
				//$block->setChild('toolbar', $toolbar);
				//var_dump($block->getChildHtml('toolbar'));
				//die();
				//$content .= $block->toHtml();
				/*
				$content .= '
				{{block type="catalog/product_list" category_id="'.$_catId.'" column_count="3" count="6" limit="4" mode="grid" toolbar_block_name="product_list_toolbar" template="catalog/product/list.phtml"/}}
					
				';
				 */
				$content .= $block->toHtml();
			}

			return $content;
		} elseif ($this->getData('cz_cms_type_idcz_cms_content_block_type') == 8) {
			
			$menu = Mage::getModel('cmsplus/cz_cms_menu')->getCollection()->loadByStore(Mage::helper('cmsplus')->getStoreId());
			
			$block = Mage::app()->getLayout()->createBlock(
				'cmsplus/cmsplus',
				'cmspluscontent',
				array(
					'template' => 'cz/cmsplus/sitemap.phtml',
				)
			)->setData('menues',$menu);
			return $block->toHtml();
			
			
			return $content;
		} else {
			return '';
		}
	}
	
	public function delete() {
		$_categories = Mage::getModel('cmsplus/cz_cms_content_element_has_catalog_category_entity')->getCollection()->loadByElementId($this->getId());
		if ($_categories && $_categories->count() > 0) {
			foreach ($_categories AS $_cat) {
				$_cat->delete();
			}
		}
		return parent::delete();
	}


}