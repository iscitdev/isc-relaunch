<?php
/**
 * clever + zöger gmbh
 * http://www.clever-zoeger.de
 * @author Ronny Hempel <rhempel@clever-zoeger.de>
 * @version $Id: Page.php 381 2012-04-24 09:04:02Z rhempel $
 * $HeadURL: https://svn.clever-zoeger.de/svn/Magento/extensions/Cz_Cmsplus/tags/1.3.0/cmsPlus/app/code/local/Cz/Cmsplus/Model/Cz/Cms/Content/Page.php $
 * $LastChangedBy: rhempel $
 * $LastChangedRevision: 381 $
 * $LastChangedDate: 2012-04-24 11:04:02 +0200 (Tue, 24 Apr 2012) $
 */
?>
<?php

class Cz_Cmsplus_Model_Cz_Cms_Content_Page extends Mage_Core_Model_Abstract
{
    var $_children = false;
    var $_images = array();

    static $_values = array();
    static $_leer = '----------------------------------------------------------------';

    public function _construct()
    {
        parent::_construct();
        $this->_init('cmsplus/cz_cms_content_page');
    }

    public function deleteRecursive()
    {
        $this->delete();
    }

    public function delete()
    {
        $_categories = Mage::getModel('cmsplus/cz_cms_content_page_has_catalog_category_entity')->getCollection()->loadByPageId($this->getId());
        if($_categories && $_categories->count() > 0) {
            foreach($_categories AS $_cat) {
                $_cat->delete();
            }
        }

        $pages = Mage::getSingleton('cmsplus/cz_cms_content_page')->getCollection()->loadChilds($this->getId());
        if($pages) {
            foreach($pages AS $_page) {
                $_page->delete();
            }
        }

        $images = Mage::getSingleton('cmsplus/cz_cms_content_page_image')->getCollection()->loadByPageId($this->getId());
        if($images) {
            foreach($images AS $_image) {
                $_image->delete();
            }
        }

        $blocks = Mage::getSingleton('cmsplus/cz_cms_content_block')->getCollection()->loadByPageId($this->getId());
        if($blocks) {
            foreach($blocks AS $_block) {
                $_block->delete();
            }
        }

        $pagemenu = Mage::getModel('cmsplus/cz_cms_content_page_has_cz_cms_menu')->load($this->getId());
        if($pagemenu) {
            $pagemenu->delete();
        }
        parent::delete();
    }

    public function hasChildren()
    {
        if(!$this->_children) {
            $this->_children = Mage::getModel('cmsplus/cz_cms_content_page')->getCollection()->loadByParentPage($this);
        }
        if($this->_children->count() > 0) return true;
        return false;
    }

    public function getChildren()
    {
        if(!$this->_children) {
            $this->_children = Mage::getModel('cmsplus/cz_cms_content_page')->getCollection()->loadByParentPage($this);
        }
        return $this->_children;
    }

    public function getBreadcrumb($_breadcrumb)
    {
        $hash = sha1($this->getId());
        $cacheKey = 'CZ_CMSPLUS_PAGE_GETBREADCRUMB_' . $hash . '_' . Mage::app()->getStore()->getCode();
        if(Mage::app()->useCache('cmsplus') && $cache = Mage::app()->loadCache($cacheKey)) {
            $_breadcrumb = unserialize($cache);
        } else {
            if($this->getData('idcz_cms_content_page_pid') != 0) {
                $pageParent = Mage::getModel('cmsplus/cz_cms_content_page')->load($this->getData('idcz_cms_content_page_pid'));
                $_breadcrumb = $pageParent->getBreadcrumb($_breadcrumb);
            }

            //add menu item to breadcrumbs
            $pagemenu = Mage::getModel('cmsplus/cz_cms_content_page_has_cz_cms_menu')->load($this->getId());
            $menuId = $pagemenu->getData('cz_cms_menu_idcz_cms_menu');
            if($menuId) {
                $menuItem = Mage::getModel('cmsplus/cz_cms_menu')->load($menuId);
                $_breadcrumb[] = array(
                    'label' => Mage::helper('cmsplus')->__($menuItem->getName()),
                    'title' => Mage::helper('cmsplus')->__($menuItem->getName()),
                );
            }


            $infos = array(
                'label' => Mage::helper('cmsplus')->__($this->getName()),
                'title' => Mage::helper('cmsplus')->__($this->getName()),
                'link' => $this->getUrl(),
            );
            $_breadcrumb[] = $infos;


            if(Mage::app()->useCache('cmsplus')) {
                Mage::app()->saveCache(serialize($_breadcrumb), $cacheKey, array('cmsplus'), 999999999);
            }
        }
        return $_breadcrumb;
    }

    public function buildUrl($actUrl)
    {
        if($this->getData('idcz_cms_content_page_pid') != 0) {
            $pageParent = Mage::getModel('cmsplus/cz_cms_content_page')->load($this->getData('idcz_cms_content_page_pid'));
            $actUrl .= $pageParent->buildUrl($actUrl);
        }
        if(!is_null($this->getUrlkey())) {
            $actUrl .= $this->getUrlkey();
        } else {
            $actUrl .= Mage::helper('cmsplus')->makeUrlKey($this->getName());
        }
        return $actUrl . '/';
    }

    private function getPageIds($_ids)
    {
        $hash = sha1($this->getId());
        $cacheKey = 'CZ_CMSPLUS_PAGE_GETPAGEIDS_' . $hash . '_' . Mage::app()->getStore()->getCode();
        if(Mage::app()->useCache('cmsplus') && $cache = Mage::app()->loadCache($cacheKey)) {
            $_ids = unserialize($cache);
        } else {
            if($this->getData('idcz_cms_content_page_pid') != 0) {
                $pageParent = Mage::getModel('cmsplus/cz_cms_content_page')->load($this->getData('idcz_cms_content_page_pid'));
                $_ids .= $pageParent->getPageIds($_ids);
            }
            $_ids .= $this->getId();
            if(Mage::app()->useCache('cmsplus')) {
                Mage::app()->saveCache(serialize($_ids), $cacheKey, array('cmsplus'), 999999999);
            }
        }
        return $_ids . '/';
    }

    public function getUrl($store = null)
    {
        $hash = sha1($this->getId() . $this->getUrlkey());
        $cacheKey = 'CZ_CMSPLUS_PAGE_GETURL_' . $hash . '_' . Mage::app()->getStore($store)->getCode();
        if(Mage::app()->useCache('cmsplus') && $cache = Mage::app()->loadCache($cacheKey)) {
            $actUrl = unserialize($cache);
        } else {
            $storeId = Mage::app()->getStore($store)->getId();
            $urlKey = trim(Mage::getStoreConfig('cz/cmsplus/urlkey', $storeId));

            $actUrl = '';
            $actUrl = $this->buildUrl($actUrl);
            $actUrl = Mage::helper('cmsplus')->getUrl($urlKey . '/' . $actUrl);
            if(Mage::app()->useCache('cmsplus')) {
                Mage::app()->saveCache(serialize($actUrl), $cacheKey, array('cmsplus'), 999999999);
            }
        }

        return $actUrl;
    }

    public function getBlocks()
    {
        if(!$this->_blocks) {
            $this->_blocks = Mage::getModel('cmsplus/cz_cms_content_block')->getCollection()->loadByPage($this);
        }
        return $this->_blocks;
    }

    public function getImages($type)
    {

        switch($type) {
            case 'image':
                $type = 5;
                break;
            case 'small':
                $type = 4;
                break;
        }
        if(!isset($this->_images[$type])) {
            $this->_images[$type] = Mage::getModel('cmsplus/cz_cms_content_page_image')->getCollection()->loadByPageIdAndTypeId($this->getId(), $type);
        }

        return $this->_images[$type];
    }

    public function isActive()
    {
        $_pageId = Mage::helper('cmsplus')->getPageId();
        if(intval($_pageId) != 0) {
            $_page = Mage::getModel('cmsplus/cz_cms_content_page')->load($_pageId);
            $_pIds = '';
            $_pIds = $_page->getPageIds($_pIds);
            $_pIds = explode('/', $_pIds);
            if(in_array($this->getId(), $_pIds)) {
                return true;
            }
        } else {
            //andere Page
            $_category = Mage::registry('current_category');
            if(!is_null($_category)) {
                $_catId = $_category->getId();
                $_pages = Mage::getModel('cmsplus/cz_cms_content_page')->getCollection()->getPageByCategoryId($_catId);
                if($_pages) {
                    foreach($_pages AS $_page) {
                        $_pIds = '';
                        $_pIds = $_page->getPageIds($_pIds);
                        $_pIds = explode('/', $_pIds);
                        if(in_array($this->getId(), $_pIds)) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    private function buildPages($in_Pages, &$pageData, $level)
    {
        $leer = $level * 3;
        $leer = substr(self::$_leer, 0, $leer) . ' ';
        if(is_null($in_Pages)) {
            //load all root Pages
            $in_Pages = Mage::getModel('cmsplus/cz_cms_content_page')->getCollection()->getPageRootIds();
        }
        foreach($in_Pages AS $page) {
            $pageData[] = array(
                'label' => $leer . " " . $page->getName(),
                'value' => $page->getId(),
            );
            if($page->hasChildren()) {
                $this->buildPages($page->getChildren(), $pageData, ++$level);
                $level--;
            }
        }
    }

    public function getOptionArray()
    {
        $catData = array();
        $catData[] = array(
            'label' => '',
            'value' => '',
        );
        $this->buildPages(null, $catData, 0);
        self::$_values = $catData;

        $_out = array();
        foreach(self::$_values AS $val) {
            $_out[$val['value']] = $val['label'];
        }
        return $_out;
    }
}