<?php
/**
 * clever + zöger gmbh
 * http://www.clever-zoeger.de
 * @author Ronny Hempel <rhempel@clever-zoeger.de>
 * @version $Id: Image.php 203 2011-09-16 12:16:50Z tzoeger $
 * $HeadURL: https://svn.clever-zoeger.de/svn/Magento/extensions/Cz_Cmsplus/tags/1.3.0/cmsPlus/app/code/local/Cz/Cmsplus/Model/Cz/Cms/Content/Page/Image.php $
 * $LastChangedBy: tzoeger $
 * $LastChangedRevision: 203 $
 * $LastChangedDate: 2011-09-16 14:16:50 +0200 (Fri, 16 Sep 2011) $
 */
?>
<?php
class Cz_Cmsplus_Model_Cz_Cms_Content_Page_Image extends Mage_Core_Model_Abstract {
	var $resized = null;
	var $height = null;
	var $width = null;
	
    public function _construct()
    {
    	parent::_construct();
        $this->_init('cmsplus/cz_cms_content_page_image');
    }
	
	public function hasLink() {
		if (!is_null($this->getLinktype()) && intval($this->getLinktype()) != 0) {
			return true;
		}
		return false;
	}
	
	public function getLink() {
		if ($this->hasLink()) {
			switch (intval($this->getLinktype())) {
				case '1':
					return $this->getExtlink();
				break;
				case '2':
					$_id = $this->getCatlink();
					$_obj = Mage::getModel('catalog/category')->load($_id);
					if ($_obj) {
						return $_obj->getUrl();
					}
				break;
				case '3':
					$_id = $this->getProdlink();
					$_obj = Mage::getModel('catalog/product')->load($_id);
					if ($_obj) {
						return $_obj->getProductUrl();
					}
				break;
				case '4':
					$_id = $this->getCmslink();
					$_obj = Mage::getModel('cmsplus/cz_cms_content_page')->load($_id);
					if ($_obj) {
						return $_obj->getUrl();
					}
				break;
			}
		}
		return '';
	}
	
	public function resize($sizeW=null,$sizeH=null) {
		$file = Mage::getBaseDir('media').'/'.$this->getFile();
		if (is_null($this->getFile()) || !file_exists($file)) {
			//image error
			Mage::helper('cmsplus')->error('Image Error. ImageID: '.$this->getId());
			return $this;
		}

		$img = new Imagick($file);
		$d = $img->getImageGeometry();
		if (is_null($sizeH) && is_null($sizeW)) {
			return $this;
		} elseif (is_null($sizeH)) {
			$sizeH = $sizeW * $d['height'] / $d['width'];
		} else {
			$sizeW = $d['width']* $sizeH / $d['height'];
		}
		$path = Mage::getBaseDir('media').'/';
		if (!is_Dir($path.'cache')) {
			mkdir($path.'cache');
		}
		if (!is_Dir($path.'cache/cmsplus')) {
			mkdir($path.'cache/cmsplus');
		}
		$filenew = 'cache/'.$this->getFile().'_'.$sizeW.'_'.$sizeH.'.png';
		$this->height = $sizeH;
		$this->width = $sizeW;
		
		if (!file_exists($path.$filenew)) {
			$img->resizeImage($sizeW,$sizeH,0,1);
			$img->writeImage($path.$filenew);
			
		}
		$img->clear();
		$img->destroy();
		
		$this->resized = $filenew;
		
		return $this;
	}
	
	public function getFile() {
		if ($this->resized) {
			return $this->resized;
		}
		return parent::getFile();
	}
	public function __toString() {
		$file = Mage::getBaseDir('media').'/'.$this->getFile();
		if (is_null($this->getFile()) || !file_exists($file)) {
			//image error
			Mage::helper('cmsplus')->error('Image Error. ImageID: '.$this->getId());
			return '';
		}
		$id = '';
		if ($this->getImageCssId()) {
			$id = 'id="'.$this->getImageCssId().'" ';
		}
		if (is_null($this->height) || is_null($this->width)) {
			$img = new Imagick(Mage::getBaseDir('media').'/'.$this->getFile());
			$d = $img->getImageGeometry();
			$this->height = $d['height'];
			$this->width = $d['width'];
		}
		
		return '<img width="'.$this->width.'" height="'.$this->height.'" '.$id.'class="'.$this->getImageCssClass().'" title="'.$this->getTitleText().'" alt="'.$this->getAltText().'" src="'.Mage::getBaseUrl('media').$this->getFile().'" />';
	}

}