<?php
/**
 * clever + zöger gmbh
 * http://www.clever-zoeger.de
 * @author Ronny Hempel <rhempel@clever-zoeger.de>
 * @version $Id: Menu.php 265 2011-11-21 09:19:45Z tzoeger $
 * $HeadURL: https://svn.clever-zoeger.de/svn/Magento/extensions/Cz_Cmsplus/tags/1.3.0/cmsPlus/app/code/local/Cz/Cmsplus/Model/Cz/Cms/Menu.php $
 * $LastChangedBy: tzoeger $
 * $LastChangedRevision: 265 $
 * $LastChangedDate: 2011-11-21 10:19:45 +0100 (Mon, 21 Nov 2011) $
 */
?>
<?php
class Cz_Cmsplus_Model_Cz_Cms_Menu extends Mage_Core_Model_Abstract
{
    public function _construct()
    {
    	parent::_construct();
        $this->_init('cmsplus/cz_cms_menu');
    }

	
	public function deleteRecursive() {
		$this->delete();
	}
	
	public function delete() {
		$pages = Mage::getSingleton('cmsplus/cz_cms_content_page')->getCollection()->loadByMenuId($this->getId());
		foreach ($pages AS $_page) {
			$_page->delete();	
		}
		parent::delete();
	}
	
	public function getPages() {
	    $_menu = $this->getCollection()->loadByMenuIdAndStore($this->getId());
	    return Mage::getModel('cmsplus/cz_cms_content_page')->getCollection()->loadByMenu($_menu);
	}
}