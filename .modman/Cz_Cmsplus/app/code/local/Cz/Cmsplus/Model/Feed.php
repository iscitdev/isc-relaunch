<?php
/**
 * clever + zöger gmbh
 * http://www.clever-zoeger.de
 * @author Ronny Hempel <rhempel@clever-zoeger.de>
 * @version $Id: Feed.php 193 2011-09-15 14:42:02Z rhempel $
 * $HeadURL: https://svn.clever-zoeger.de/svn/Magento/extensions/Cz_Cmsplus/tags/1.3.0/cmsPlus/app/code/local/Cz/Cmsplus/Model/Feed.php $
 * $LastChangedBy: rhempel $
 * $LastChangedRevision: 193 $
 * $LastChangedDate: 2011-09-15 16:42:02 +0200 (Thu, 15 Sep 2011) $
 */

 
	class Cz_Cmsplus_Model_Feed extends Mage_AdminNotification_Model_Feed {
		const XML_FEED_URL_PATH_CZ = 'system/cz_cmsplus/feed_url_cz';
		protected $_feedUrlCz;
		
		public function doUpdate() {
			if (($this->getFrequency() + $this->getLastUpdate()) > time()) {
				return $this;
			}
			$feedData = array();
			$feedXml = $this->getFeedData();
			if ($feedXml && $feedXml->channel && $feedXml->channel->item) {
				foreach ($feedXml->channel->item as $item) {
					$feedData[] = array(
						'severity'		=>	(int)$item->severity,
						'date_added'	=>	$this->getDate((string)$item->pubDate),
						'title'			=>	(string)$item->title,
						'description'	=>	(string)$item->description,
						'url'			=>	(string)$item->link,
					);
				}
				if ($feedData) {
					Mage::getModel('adminnotification/inbox')->parse(array_reverse($feedData));
				}
			}
			$this->setLastUpdate();
		}
		
		public function getLastUpdate() {
			return Mage::app()->loadCache('cz_notifications_lastcheck');
		}
		
		public function setLastUpdate() {
			Mage::app()->saveCache(time(), 'cz_notifications_lastcheck');
			return $this;
		}
		
		public function getFeedData() {
			$curl = new Varien_Http_Adapter_Curl();
			$curl->setConfig(array(
				'timeout'   => 3
			));
			$curl->write(Zend_Http_Client::GET, $this->getFeedUrl(), '1.0');
			$data = $curl->read();
			if ($data === false) {
				return false;
			}
			$data = preg_split('/^\r?$/m', $data, 2);
			$data = trim($data[1]);
			$curl->close();
			try {
				$xml  = @new SimpleXMLElement($data);
			} catch (Exception $e) {
				return false;	
			}
			
			return $xml;
		}
		
		public function getFeedUrl() {
			if (is_null($this->_feedUrlCz)) {
				$this->_feedUrlCz = (Mage::getStoreConfigFlag(self::XML_USE_HTTPS_PATH) ? 'https://' : 'http://')
					.Mage::getStoreConfig(self::XML_FEED_URL_PATH_CZ);
			}
			return $this->_feedUrlCz;	
		}
	}
?>