<?php
/**
 * clever + zöger gmbh
 * http://www.clever-zoeger.de
 * @author Ronny Hempel <rhempel@clever-zoeger.de>
 * @version $Id: Collection.php 181 2011-09-12 14:37:18Z rhempel $
 * $HeadURL: https://svn.clever-zoeger.de/svn/Magento/extensions/Cz_Cmsplus/tags/1.3.0/cmsPlus/app/code/local/Cz/Cmsplus/Model/Mysql4/Cz/Cms/Content/Block/Collection.php $
 * $LastChangedBy: rhempel $
 * $LastChangedRevision: 181 $
 * $LastChangedDate: 2011-09-12 16:37:18 +0200 (Mon, 12 Sep 2011) $
 */
?>
<?php

class Cz_Cmsplus_Model_Mysql4_Cz_Cms_Content_Block_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
	var $hidden	=	array(0);
	
	public function setHidden($hidden) {
		if ($hidden == 1) {
			$this->hidden = array(0);
		} else {
			$this->hidden = array(0,1);
		}
		
		return $this;
	}
	
    public function _construct()
    {
        parent::_construct();
        $this->_init('cmsplus/cz_cms_content_block');
    }

	public function prepareSummary()
	{
			$this->setConnection($this->getResource()->getReadConnection());
			$this->getSelect()
				->from(array('main_table'=>$this->getTable('cz_cms_content_block')),'*')
				->where('hidden IN (?)', $this->hidden)
				->where('storeview_code = ?', Mage::helper('cmsplus')->getStoreId());
				//->where('hidden = ?', 0)
				//->order('modified_date','asc');
			return $this;
	}
	
	public function loadByPage(&$page) {
		return $this->loadByPageId($page->getId());
	}
	public function loadByPageId($pageid) {
		$this->setConnection($this->getResource()->getReadConnection());
		$this->getSelect()
			->from(array('main_table'=>$this->getTable('cz_cms_content_block')),'*')
			->where('cz_cms_content_page_idcz_cms_content_page = ?', $pageid)
			->where('storeview_code = ?', Mage::helper('cmsplus')->getStoreId())
			->where('hidden IN (?)', $this->hidden)
			->order('sort_order ASC');
		if ($this->count() == 0 && Mage::helper('cmsplus')->getStoreId() != 0) {
			Mage::helper('cmsplus')->setStoreId(0);
			return Mage::getModel('cmsplus/cz_cms_content_block')->getCollection()->loadByPageId($pageid);
		}
		return $this;
	}
	
	public function getLastSortOrder($pageid) {
		$this->setConnection($this->getResource()->getReadConnection());
		$this->getSelect()
			->from(array('main_table'=>$this->getTable('cz_cms_content_block')),'*')
			->where('cz_cms_content_page_idcz_cms_content_page = ?', $pageid)
			->where('storeview_code = ?', Mage::helper('cmsplus')->getStoreId())
			->where('hidden IN (?)', $this->hidden)
			->limit(1)
			->order('sort_order DESC');
		foreach ($this AS $block) {
			return $block->getSortOrder();
		}
		return 0;
	}
	
	public function loadByPageIdAndOrder($pageid,$sortOrder) {
		if ($sortOrder == 'up') {
			$sortOrder = 'DESC';
		} else {
			$sortOrder = 'ASC';
		}

		$this->setConnection($this->getResource()->getReadConnection());
		$this->getSelect()
			->from(array('main_table'=>$this->getTable('cz_cms_content_block')),'*')
			->where('`cz_cms_content_page_idcz_cms_content_page` = ?', $pageid)
			->where('storeview_code = ?', Mage::helper('cmsplus')->getStoreId())
			->where('hidden IN (?)', $this->hidden)
			->order('sort_order '.$sortOrder);
		//var_dump($this->getSelect()->__toString());
		return $this;

	}

}