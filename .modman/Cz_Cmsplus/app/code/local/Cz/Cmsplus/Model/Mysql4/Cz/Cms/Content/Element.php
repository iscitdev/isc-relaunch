<?php
/**
 * clever + zöger gmbh
 * http://www.clever-zoeger.de
 * @author Ronny Hempel <rhempel@clever-zoeger.de>
 * @version $Id: Element.php 181 2011-09-12 14:37:18Z rhempel $
 * $HeadURL: https://svn.clever-zoeger.de/svn/Magento/extensions/Cz_Cmsplus/tags/1.3.0/cmsPlus/app/code/local/Cz/Cmsplus/Model/Mysql4/Cz/Cms/Content/Element.php $
 * $LastChangedBy: rhempel $
 * $LastChangedRevision: 181 $
 * $LastChangedDate: 2011-09-12 16:37:18 +0200 (Mon, 12 Sep 2011) $
 */
?>
<?php
class Cz_Cmsplus_Model_Mysql4_Cz_Cms_Content_Element extends Mage_Core_Model_Mysql4_Abstract
{
    public function _construct()
    {
        $this->_init('cmsplus/cz_cms_content_element', 'idcz_cms_content_element');
    }
	
	protected function _afterSave(Mage_Core_Model_Abstract $object) {
		$_categories = Mage::getModel('cmsplus/cz_cms_content_element_has_catalog_category_entity')->getCollection()->loadByElementId($object->getId());
		if ($_categories && $_categories->count() > 0) {
			foreach ($_categories AS $_cat) {
				$_cat->delete();
			}
		}

		if ($catIds = $object->getCategories()) {
			foreach ($catIds AS $_key => $catId) {
				if (is_null($catId) && $_key == '' || $catId == 0) continue;

				$_czpageCat = Mage::getModel('cmsplus/cz_cms_content_element_has_catalog_category_entity');
				$_czpageCat->setData('cz_cms_content_element_idcz_cms_content_element',$object->getId());
				$_czpageCat->setData('catalog_category_entity_entity_id',$catId);

				$_czpageCat->save();
			}
		}
		
		
		return parent::_afterSave($object);
	}

	protected function _afterLoad(Mage_Core_Model_Abstract $object) {
		$_categories = Mage::getModel('cmsplus/cz_cms_content_element_has_catalog_category_entity')->getCollection()->loadByElementId($object->getId());
		$_catIds = array();
		if ($_categories && $_categories->count() > 0) {
			foreach ($_categories AS $_cat) {
				$_catIds[] = $_cat->getData('catalog_category_entity_entity_id');
			}
		}

		$object->setCategories($_catIds);
		return parent::_afterLoad($object);
	}

}