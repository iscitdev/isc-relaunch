<?php
/**
 * clever + zöger gmbh
 * http://www.clever-zoeger.de
 * @author Ronny Hempel <rhempel@clever-zoeger.de>
 * @version $Id: Page.php 181 2011-09-12 14:37:18Z rhempel $
 * $HeadURL: https://svn.clever-zoeger.de/svn/Magento/extensions/Cz_Cmsplus/tags/1.3.0/cmsPlus/app/code/local/Cz/Cmsplus/Model/Mysql4/Cz/Cms/Content/Page.php $
 * $LastChangedBy: rhempel $
 * $LastChangedRevision: 181 $
 * $LastChangedDate: 2011-09-12 16:37:18 +0200 (Mon, 12 Sep 2011) $
 */
?>
<?php
class Cz_Cmsplus_Model_Mysql4_Cz_Cms_Content_Page extends Mage_Core_Model_Mysql4_Abstract
{
    public function _construct()
    {
        $this->_init('cmsplus/cz_cms_content_page', 'idcz_cms_content_page');
    }
	
	protected function _afterSave(Mage_Core_Model_Abstract $object) {
		$menuid = $object->getMenu();
		$params = Mage::app()->getFrontController()->getRequest()->getParams();
		//var_dump($object->getData('categories'));die();
		if ($menuid && $menuid > 0) {
			// gibt kein edit derzeit
			//die('edit');
		} else {
			$pagemenu = Mage::getModel('cmsplus/cz_cms_content_page_has_cz_cms_menu');
			$storeArray = array(
				'cz_cms_content_page_idcz_cms_content_page'	=>	intval($object->getId()),
				'cz_cms_menu_idcz_cms_menu'	=>	intval($params['menuid']),
			);
			$this->_getWriteAdapter()->insert($this->getTable('cz_cms_content_page_has_cz_cms_menu'), $storeArray);
		}
		
		$_categories = Mage::getModel('cmsplus/cz_cms_content_page_has_catalog_category_entity')->getCollection()->loadByPageId($object->getId());
		if ($_categories && $_categories->count() > 0) {
			foreach ($_categories AS $_cat) {
				$_cat->delete();
			}
		}

		if ($catIds = $object->getCategories()) {
			
			foreach ($catIds AS $_key => $catId) {
				if (is_null($catId) && $_key == '' || $catId == 0) continue;
				$storeArray = array(
					'cz_cms_content_page_idcz_cms_content_page'	=>	intval($object->getId()),
					'catalog_category_entity_entity_id'	=>	intval($catId),
				);
				$_czpageCat = Mage::getModel('cmsplus/cz_cms_content_page_has_catalog_category_entity');
				$_czpageCat->setData('cz_cms_content_page_idcz_cms_content_page',$object->getId());
				$_czpageCat->setData('catalog_category_entity_entity_id',$catId);
				$_czpageCat->save();
				//$this->_getWriteAdapter()->insert($this->getTable('cz_cms_content_page_has_catalog_category_entity'), $storeArray);
				//var_dump($storeArray);
				///die('B');
			}
		}

		return parent::_afterSave($object);
	}
	
	protected function _afterLoad(Mage_Core_Model_Abstract $object) {
		$pagemenu = Mage::getModel('cmsplus/cz_cms_content_page_has_cz_cms_menu')->load($object->getId());
		$object->setMenu($pagemenu->getData('cz_cms_menu_idcz_cms_menu'));
		
		$_categories = Mage::getModel('cmsplus/cz_cms_content_page_has_catalog_category_entity')->getCollection()->loadByPageId($object->getId());
		$_catIds = array();
		if ($_categories && $_categories->count() > 0) {
			foreach ($_categories AS $_cat) {
				$_catIds[] = $_cat->getData('catalog_category_entity_entity_id');
			}
		}

		$object->setCategories($_catIds);
		return parent::_afterLoad($object);
	}
	

}