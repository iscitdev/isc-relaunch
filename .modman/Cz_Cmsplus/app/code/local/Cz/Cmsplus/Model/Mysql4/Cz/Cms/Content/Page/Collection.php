<?php
/**
 * clever + zöger gmbh
 * http://www.clever-zoeger.de
 * @author Ronny Hempel <rhempel@clever-zoeger.de>
 * @version $Id: Collection.php 265 2011-11-21 09:19:45Z tzoeger $
 * $HeadURL: https://svn.clever-zoeger.de/svn/Magento/extensions/Cz_Cmsplus/tags/1.3.0/cmsPlus/app/code/local/Cz/Cmsplus/Model/Mysql4/Cz/Cms/Content/Page/Collection.php $
 * $LastChangedBy: tzoeger $
 * $LastChangedRevision: 265 $
 * $LastChangedDate: 2011-11-21 10:19:45 +0100 (Mon, 21 Nov 2011) $
 */
?>
<?php

class Cz_Cmsplus_Model_Mysql4_Cz_Cms_Content_Page_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
	var $hidden	=	array(0);

	public function setHidden($hidden) {
		if ($hidden == 1) {
			$this->hidden = array(0);
		} else {
			$this->hidden = array(0,1);
		}

		return $this;
	}

    public function _construct()
    {
        parent::_construct();
        $this->_init('cmsplus/cz_cms_content_page');
    }

	public function prepareSummary()
	{
			$this->setConnection($this->getResource()->getReadConnection());
			$this->getSelect()
				->from(array('main_table'=>$this->getTable('cz_cms_content_page')),'*')
				->where('hidden IN (?)', $this->hidden)
				->where('storeview_code = ?', Mage::helper('cmsplus')->getStoreId());
				//->where('hidden = ?', 0)
				//->order('modified_date','asc');
			return $this;
	}

	public function loadByParentPage(&$page) {
		return $this->loadChilds($page->getId());
	}

	public function loadByMenu(&$menu) {
		return $this->loadByMenuId($menu->getId(),0);
	}

	public function loadByMenuId($menuid,$parentId = 0) {
		$this->setConnection($this->getResource()->getReadConnection());
			$this->getSelect()
				->from(array('main_table'=>$this->getTable('cz_cms_content_page')),'*')
				->join(array('pagemenu' => $this->getTable('cz_cms_content_page_has_cz_cms_menu')),'main_table.idcz_cms_content_page = pagemenu.cz_cms_content_page_idcz_cms_content_page','*')
				->where('pagemenu.cz_cms_menu_idcz_cms_menu  = ?', $menuid)
				->where('main_table.idcz_cms_content_page_pid  = ?', $parentId)
				->where('storeview_code = ?', Mage::helper('cmsplus')->getStoreId())
				->where('hidden IN (?)', $this->hidden)
				->order('main_table.sort_order asc');

			return $this;
	}

	public function loadByMenuIdAndSort($menuid,$parentId = 0,$sortOrder) {
		if ($sortOrder == 'next') {
			$sortOrder = 'DESC';
		} else {
			$sortOrder = 'ASC';
		}
		$this->setConnection($this->getResource()->getReadConnection());
			$this->getSelect()
				->from(array('main_table'=>$this->getTable('cz_cms_content_page')),'*')
				->join(array('pagemenu' => $this->getTable('cz_cms_content_page_has_cz_cms_menu')),'main_table.idcz_cms_content_page = pagemenu.cz_cms_content_page_idcz_cms_content_page','*')
				->where('pagemenu.cz_cms_menu_idcz_cms_menu  = ?', $menuid)
				->where('main_table.idcz_cms_content_page_pid  = ?', $parentId)
				->where('storeview_code = ?', Mage::helper('cmsplus')->getStoreId())
				->where('hidden IN (?)', $this->hidden)
				->order('main_table.sort_order '.$sortOrder);

			return $this;
	}

	public function getPageRootIds() {
		$this->setConnection($this->getResource()->getReadConnection());
			$this->getSelect()
				->from(array('main_table'=>$this->getTable('cz_cms_content_page')),'*')
				->where('main_table.idcz_cms_content_page_pid  = ?', 0)
				->where('storeview_code = ?', Mage::helper('cmsplus')->getStoreId())
				->where('hidden IN (?)', $this->hidden)
				->order('main_table.sort_order asc');
		$_ids = array();
		foreach ($this AS $_page) {
			$_ids[] = $_page;
		}
		return $_ids;
	}

	public function getPageByCategoryId($catId) {
		$this->setConnection($this->getResource()->getReadConnection());
			$this->getSelect()
				->from(array('main_table'=>$this->getTable('cz_cms_content_page')),'*')
				->join(array('pagecat' => $this->getTable('cz_cms_content_page_has_catalog_category_entity')),'main_table.idcz_cms_content_page = pagecat.cz_cms_content_page_idcz_cms_content_page','*')
				->where('pagecat.catalog_category_entity_entity_id  = ?', $catId)
				->where('hidden IN (?)', $this->hidden)
				->order('main_table.sort_order asc');

			return $this;
	}

	public function loadByUrlkey($urlkey,$parentId = 0) {
		$this->setConnection($this->getResource()->getReadConnection());
		$this->resetData();
		$this->_setIsLoaded(false);
		$this->getSelect()
			->from(array('main_table'=>$this->getTable('cz_cms_content_page')),'*')
			->where('main_table.urlkey  = ?', $urlkey)
			->where('main_table.idcz_cms_content_page_pid  = ?', $parentId)
			->where('storeview_code = ?', Mage::helper('cmsplus')->getStoreId())
			->where('hidden IN (?)', $this->hidden)
			->order('main_table.sort_order asc');
		return $this->fetchItem();
	}

	public function loadChilds($parentId) {
		$this->setConnection($this->getResource()->getReadConnection());
		$this->getSelect()
			->from(array('main_table'=>$this->getTable('cz_cms_content_page')),'*')
			->where('storeview_code = ?', Mage::helper('cmsplus')->getStoreId())
			->where('hidden IN (?)', $this->hidden)
			->where('main_table.idcz_cms_content_page_pid  = ?', $parentId)
			->order('main_table.sort_order asc')
			;
		return $this;
	}

	public function loadChildsAndOrder($parentId,$sortOrder) {
		if ($sortOrder == 'next') {
			$sortOrder = 'DESC';
		} else {
			$sortOrder = 'ASC';
		}
		$this->setConnection($this->getResource()->getReadConnection());
		$this->getSelect()
			->from(array('main_table'=>$this->getTable('cz_cms_content_page')),'*')
			->where('storeview_code = ?', Mage::helper('cmsplus')->getStoreId())
			->where('hidden IN (?)', $this->hidden)
			->where('main_table.idcz_cms_content_page_pid  = ?', $parentId)
			->order('main_table.sort_order '.$sortOrder)
			;
		return $this;
	}

	public function searchPage($pageTree,$noHidden=0) {
		$page = false;
		$hash = sha1(serialize($pageTree));
		$cacheKey = 'CZ_CMSPLUS_PAGE_COLLECTION_SEARCHPAGE_'.$hash.'_'.Mage::app()->getStore()->getCode();
		if (Mage::app()->useCache('cmsplus') && $cache = Mage::app()->loadCache($cacheKey)) {
			$pageID = unserialize($cache);
			$page = Mage::getModel('cmsplus/cz_cms_content_page')->load($pageID);
		} else {
			$parentId = 0;
			//Mage::helper('cmsplus')->getStoreId();
			foreach ($pageTree AS $_pTree) {
				$page = Mage::getModel('cmsplus/cz_cms_content_page')->getCollection()->setHidden($noHidden)->loadByUrlkey($_pTree,$parentId);
				if (!$page && Mage::helper('cmsplus')->getStoreId() != 0) {
					Mage::helper('cmsplus')->setStoreId(0);
					$page = Mage::getModel('cmsplus/cz_cms_content_page')->getCollection()->setHidden($noHidden)->loadByUrlkey($_pTree,$parentId);
				}
				if ($page) {
					$parentId = $page->getId();
				} else {
					return false;
				}
			}
			if (!$page) return false;
			if (Mage::app()->useCache('cmsplus')) {
				Mage::app()->saveCache(serialize($page->getId()), $cacheKey, array('cmsplus'),999999999);
			}
		}
		if ($noHidden && $page) {
			if ($page->getHidden() == 1) {
				return false;
			}
		}
		return $page;
	}
}