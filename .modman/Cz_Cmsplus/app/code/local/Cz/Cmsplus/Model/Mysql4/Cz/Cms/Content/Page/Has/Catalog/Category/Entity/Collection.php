<?php
/**
 * clever + zöger gmbh
 * http://www.clever-zoeger.de
 * @author Ronny Hempel <rhempel@clever-zoeger.de>
 * @version $Id: Collection.php 535 2013-02-11 14:03:18Z rhempel $
 * $HeadURL: https://svn.clever-zoeger.de/svn/Magento/extensions/Cz_Cmsplus/tags/1.3.0/cmsPlus/app/code/local/Cz/Cmsplus/Model/Mysql4/Cz/Cms/Content/Page/Has/Catalog/Category/Entity/Collection.php $
 * $LastChangedBy: rhempel $
 * $LastChangedRevision: 535 $
 * $LastChangedDate: 2013-02-11 15:03:18 +0100 (Mon, 11 Feb 2013) $
 */
?>
<?php

class Cz_Cmsplus_Model_Mysql4_Cz_Cms_Content_Page_Has_Catalog_Category_Entity_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract {

    public function _construct()
    {
        parent::_construct();
        $this->_init('cmsplus/cz_cms_content_page_has_catalog_category_entity');
    }

	public function prepareSummary()
	{
			$this->setConnection($this->getResource()->getReadConnection());
			$this->getSelect()
				->from(array('main_table'=>$this->getTable('cz_cms_content_page_has_catalog_category_entity')),'*');
			return $this;
	}
	
	
	
	public function loadByPageId($pageId) {
		$this->setConnection($this->getResource()->getReadConnection());
			$this->getSelect()
				->from(array('main_table'=>$this->getTable('cz_cms_content_page_has_catalog_category_entity')),'*')
				->where('cz_cms_content_page_idcz_cms_content_page = ?', $pageId);
			return $this;
	}
	
	public function loadByCategoryId($categoryId) {
		$this->setConnection($this->getResource()->getReadConnection());
			$this->getSelect()
				->from(array('main_table'=>$this->getTable('cz_cms_content_page_has_catalog_category_entity')),'*')
				->join(array('page_table'=>$this->getTable('cz_cms_content_page')),'main_table.cz_cms_content_page_idcz_cms_content_page = page_table.idcz_cms_content_page')
				->where('catalog_category_entity_entity_id = ?', $categoryId)
				->where('page_table.storeview_code IN (?)', array(0,Mage::app()->getStore()->getStoreId()))
				;
			return $this->fetchItem();
	}
	
}