<?php
/**
 * clever + zöger gmbh
 * http://www.clever-zoeger.de
 * @author Ronny Hempel <rhempel@clever-zoeger.de>
 * @version $Id: Collection.php 181 2011-09-12 14:37:18Z rhempel $
 * $HeadURL: https://svn.clever-zoeger.de/svn/Magento/extensions/Cz_Cmsplus/tags/1.3.0/cmsPlus/app/code/local/Cz/Cmsplus/Model/Mysql4/Cz/Cms/Menu/Collection.php $
 * $LastChangedBy: rhempel $
 * $LastChangedRevision: 181 $
 * $LastChangedDate: 2011-09-12 16:37:18 +0200 (Mon, 12 Sep 2011) $
 */
?>
<?php

class Cz_Cmsplus_Model_Mysql4_Cz_Cms_Menu_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    var $hidden = array(0);

    public function setHidden($hidden)
    {
        if ($hidden == 1) {
            $this->hidden = array(0);
        } else {
            $this->hidden = array(0, 1);
        }

        return $this;
    }

    public function _construct()
    {
        parent::_construct();
        $this->_init('cmsplus/cz_cms_menu');
    }

    public function prepareSummary()
    {
        $this->setConnection($this->getResource()->getReadConnection());
        $this->getSelect()
            ->from(array('main_table' => $this->getTable('cz_cms_menu')), '*')
            ->where('hidden IN (?)', $this->hidden)
            ->where('storeview_code = ?', Mage::helper('cmsplus')->getStoreId());
        //->order('modified_date','asc');

        return $this;
    }

    public function loadByStore($storeId)
    {
        $this->setConnection($this->getResource()->getReadConnection());
        $this->getSelect()
            ->from(array('main_table' => $this->getTable('cz_cms_menu')), '*')
            ->where('hidden IN (?)', $this->hidden)
            ->where('storeview_code = ?', $storeId);
        return $this;
    }

    public function loadByPageId($pageId)
    {
        $this->setConnection($this->getResource()->getReadConnection());
        $this->getSelect('*')
            ->from($this->getTable('cz_cms_content_page_has_cz_cms_menu'))
            ->where('cz_cms_content_page_idcz_cms_content_page = ?', $pageId);
        return $this->load()->getFirstItem();

    }

    public function loadByMenuIdAndStore($menuId)
    {

        $this->setConnection($this->getResource()->getReadConnection());
        $this->getSelect()
            ->from(array('main_table' => $this->getTable('cz_cms_menu')), '*')
            ->where('idcz_cms_menu = ?', $menuId)
            ->where('hidden IN (?)', $this->hidden)
            ->where('storeview_code = ?', Mage::helper('cmsplus')->getStoreId());

        foreach ($this AS $menu) {
            return $menu;
        }

        $this->_setIsLoaded(false);
        $this->setConnection($this->getResource()->getReadConnection());
        $this->resetData();
        $this->getSelect()
            ->from(array('main_table' => $this->getTable('cz_cms_menu')), '*')
            ->where('default_menuid = ?', $menuId)
            ->where('hidden IN (?)', $this->hidden)
            ->where('storeview_code = ?', Mage::helper('cmsplus')->getStoreId());

        foreach ($this AS $menu) {
            return $menu;
        }

        $this->_setIsLoaded(false);
        $this->setConnection($this->getResource()->getReadConnection());
        $this->resetData();
        $this->getSelect()
            ->from(array('main_table' => $this->getTable('cz_cms_menu')), '*')
            ->where('idcz_cms_menu = ?', $menuId)
            ->where('hidden IN (?)', $this->hidden)
            ->where('storeview_code = ?', 0);

        foreach ($this AS $menu) {
            Mage::helper('cmsplus')->setStoreId(0);
            return $menu;
        }
        return false;
    }
}