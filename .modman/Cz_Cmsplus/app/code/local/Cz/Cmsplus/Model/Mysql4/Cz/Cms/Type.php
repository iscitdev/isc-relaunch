<?php
/**
 * clever + zöger gmbh
 * http://www.clever-zoeger.de
 * @author Ronny Hempel <rhempel@clever-zoeger.de>
 * @version $Id: Type.php 181 2011-09-12 14:37:18Z rhempel $
 * $HeadURL: https://svn.clever-zoeger.de/svn/Magento/extensions/Cz_Cmsplus/tags/1.3.0/cmsPlus/app/code/local/Cz/Cmsplus/Model/Mysql4/Cz/Cms/Type.php $
 * $LastChangedBy: rhempel $
 * $LastChangedRevision: 181 $
 * $LastChangedDate: 2011-09-12 16:37:18 +0200 (Mon, 12 Sep 2011) $
 */
?>
<?php
class Cz_Cmsplus_Model_Mysql4_Cz_Cms_Type extends Mage_Core_Model_Mysql4_Abstract
{
    public function _construct()
    {
        $this->_init('cmsplus/cz_cms_type', 'idcz_cms_content_block_type');
    }

/*
    protected function _beforeSave(Mage_Core_Model_Abstract $object) {
		$data = $object->getData();
		$object->setData($data);
    	return parent::_beforeSave($object);
    }
*/

    protected function _afterSave(Mage_Core_Model_Abstract $object) {
        return parent::_afterSave($object);
    }

    protected function _afterLoad(Mage_Core_Model_Abstract $object) {
        return parent::_afterLoad($object);
    }



}