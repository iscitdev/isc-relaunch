<?php
/**
 * clever + zöger gmbh
 * http://www.clever-zoeger.de
 * @author Ronny Hempel <rhempel@clever-zoeger.de>
 * @version $Id: Collection.php 181 2011-09-12 14:37:18Z rhempel $
 * $HeadURL: https://svn.clever-zoeger.de/svn/Magento/extensions/Cz_Cmsplus/tags/1.3.0/cmsPlus/app/code/local/Cz/Cmsplus/Model/Mysql4/Cz/Cms/Type/Collection.php $
 * $LastChangedBy: rhempel $
 * $LastChangedRevision: 181 $
 * $LastChangedDate: 2011-09-12 16:37:18 +0200 (Mon, 12 Sep 2011) $
 */
?>
<?php

class Cz_Cmsplus_Model_Mysql4_Cz_Cms_Type_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('cmsplus/cz_cms_type');
    }

	public function prepareSummary()
	{
			$this->setConnection($this->getResource()->getReadConnection());
			$this->getSelect()
				->from(array('main_table'=>$this->getTable('cz_cms_type')),'*');
				//->where('hidden = ?', 0)
				//->order('modified_date','asc');
			return $this;
	}
	
	public function loadByTable($table) {
		$this->setConnection($this->getResource()->getReadConnection());
		$this->getSelect()
			->from(array('main_table'=>$this->getTable('cz_cms_type')),'*')
			->where('`table` LIKE ?', $table)
			->order('idcz_cms_content_block_type ASC');
		return $this;
	}
}