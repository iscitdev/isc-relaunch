<?php
/**
 * clever + zöger gmbh
 * http://www.clever-zoeger.de
 * @author Ronny Hempel <rhempel@clever-zoeger.de>
 * @version $Id: Observer.php 193 2011-09-15 14:42:02Z rhempel $
 * $HeadURL: https://svn.clever-zoeger.de/svn/Magento/extensions/Cz_Cmsplus/tags/1.3.0/cmsPlus/app/code/local/Cz/Cmsplus/Model/Observer.php $
 * $LastChangedBy: rhempel $
 * $LastChangedRevision: 193 $
 * $LastChangedDate: 2011-09-15 16:42:02 +0200 (Thu, 15 Sep 2011) $
 */

 
	class Cz_Cmsplus_Model_Observer {
		public function checkFeed(Varien_Event_Observer $observer) {
			if (Mage::getSingleton('admin/session')->isLoggedIn()) {
				$_feed = Mage::getModel('cmsplus/feed');
				$_feed->doUpdate();
			}
		}
	}
?>