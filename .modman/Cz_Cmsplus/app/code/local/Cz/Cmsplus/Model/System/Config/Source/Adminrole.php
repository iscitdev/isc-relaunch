<?php

class Cz_Cmsplus_Model_System_Config_Source_Adminrole {
	protected $_options;
	
	public function toOptionArray($isMultiselect=false) {
		if (!$this->_options) {
			$this->_options = Mage::getModel('admin/roles')->getCollection()->toOptionArray(false);
		}
		$options = $this->_options;
		if(!$isMultiselect){
			array_unshift($options, array('value'=>'', 'label'=> Mage::helper('adminhtml')->__('--Please Select--')));
		}
		return $options;
	}
}


?>