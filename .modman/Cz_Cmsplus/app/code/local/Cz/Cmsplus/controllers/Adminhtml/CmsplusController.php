<?php
/**
 * clever + zöger gmbh
 * http://www.clever-zoeger.de
 * @author Ronny Hempel <rhempel@clever-zoeger.de>
 * @version $Id: CmsplusController.php 181 2011-09-12 14:37:18Z rhempel $
 * $HeadURL: https://svn.clever-zoeger.de/svn/Magento/extensions/Cz_Cmsplus/tags/1.3.0/cmsPlus/app/code/local/Cz/Cmsplus/controllers/Adminhtml/CmsplusController.php $
 * $LastChangedBy: rhempel $
 * $LastChangedRevision: 181 $
 * $LastChangedDate: 2011-09-12 16:37:18 +0200 (Mon, 12 Sep 2011) $
 */
?>
<?php
if (file_exists(dirname(__FILE__).'/CmsplusController_enc.php')) {
	require_once('CmsplusController_enc.php');
} else {
	require_once('Cz_Cmsplus_Block_Cmsplus_enc.php');
}
class Cz_Cmsplus_Adminhtml_CmsplusController extends Cz_Cmsplus_Adminhtml_CmsplusController_enc
{
	
	protected function _getDefaultStoreId() {
		return parent::_getDefaultStoreId();
	}
	
	public function preDispatch() {
		return parent::preDispatch();
	}
	
	protected function _initAction() {
		return parent::_initAction();
	}

	public function indexAction() {
		return parent::indexAction();
	}
	
	public function addmenuAction() {
		return parent::addmenuAction();
	}
	
	public function addPageAction() {
		return parent::addPageAction();		
	}
	public function emptyElementAction() {
		return parent::emptyElementAction();	
	}
	
	public function editElementAction() {
		return parent::editElementAction();
	}

	public function addBlockAction() {
		return parent::addBlockAction();
	}
	
	public function addImageAction() {
		return parent::addImageAction();
	}
	
	public function saveImageAction() {
		return parent::saveImageAction();
	}

	public function saveElementAction() {
		return parent::saveElementAction();
	}
	
	public function saveBlockAction() {
		return parent::saveBlockAction();
	}

	private function clearCache() {
		return parent::clearCache();
	}
	private function clearImageCache() {
		return parent::clearImageCache();
	}

	public function savePageAction() {
		return parent::savePageAction();
	}
	public function saveMenuAction() {
		return parent::saveMenuAction();
	}

	public function deleteMenuAction() {
		return parent::deleteMenuAction();
	}
	
	public function deletePageAction() {
		return parent::deletePageAction();
	}
	
	public function changeStoreAction() {
		return parent::changeStoreAction();
	}
	
	public function deleteImageAction() {
		return parent::deleteImageAction();
	}
	
	public function deleteBlockAction() {
		return parent::deleteBlockAction();
	}
	
	public function setOrderAction() {
		return parent::setOrderAction();
	}

	public function wysiwygAction() {
		return parent::wysiwygAction();
	}
	
	public function gridAction() {
		return parent::gridAction();
	}
	
	public function gridElementAction() {
		return parent::gridElementAction();
	}

}