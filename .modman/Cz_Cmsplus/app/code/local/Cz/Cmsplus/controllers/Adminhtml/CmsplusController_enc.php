<?php
/**
 * clever + zöger gmbh
 * http://www.clever-zoeger.de
 * @author Ronny Hempel <rhempel@clever-zoeger.de>
 * @version $Id: CmsplusController_enc.php 447 2012-07-11 11:23:46Z rhempel $
 * $HeadURL: https://svn.clever-zoeger.de/svn/Magento/extensions/Cz_Cmsplus/tags/1.3.0/cmsPlus/app/code/local/Cz/Cmsplus/controllers/Adminhtml/CmsplusController_enc.php $
 * $LastChangedBy: rhempel $
 * $LastChangedRevision: 447 $
 * $LastChangedDate: 2012-07-11 13:23:46 +0200 (Wed, 11 Jul 2012) $
 */
?>
<?php

class Cz_Cmsplus_Adminhtml_CmsplusController_enc extends Mage_Adminhtml_Controller_Action
{
	
	protected function _getDefaultStoreId() {
		return Mage_Catalog_Model_Abstract::DEFAULT_STORE_ID;
	}
	
	public function preDispatch() {
		$storeId = $this->getRequest()->getParam('store', $this->_getDefaultStoreId());
		Mage::helper('cmsplus')->setStoreId($storeId);
		return parent::preDispatch();
	}
	
	protected function _initAction() {
		$this->loadLayout()
			->_setActiveMenu('cz/cmsplus')
			->_addBreadcrumb(Mage::helper('adminhtml')->__('Cmsplus'), Mage::helper('adminhtml')->__('Cmsplus'));
		return $this;
	}

	public function indexAction() {
		
		$this->_initAction();
		$this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
		$this->renderLayout();
	}
	
	public function addmenuAction() {
		$params = $this->getRequest()->getParams();
		if (isset($params) && isset($params['menuid'])) {
			Mage::helper('cmsplus')->setAction('menuedit');
			Mage::helper('cmsplus')->setLastid($params['menuid']);
		}
		$this->_initAction();
		$this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
		$this->renderLayout();
	}
	
	public function addPageAction() {
		$params = $this->getRequest()->getParams();
		if (isset($params) && isset($params['pageid'])) {
			Mage::helper('cmsplus')->setAction('pageedit');
			Mage::helper('cmsplus')->setLastid($params['pageid']);
		}
		$this->_initAction();
		$this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
		$this->renderLayout();
				
	}
	public function emptyElementAction() {
		$params = $this->getRequest()->getParams();
		if (isset($params) && isset($params['elementid'])) {
			$element = Mage::getModel('cmsplus/cz_cms_content_element')->load($params['elementid']);
			if ($element) {
				$_data = $element->getData();
				$_data['name'] = '';
				$_data['content'] = '';
				$_data['space_before'] = '';
				$_data['space_after'] = '';
				$_data['border'] = '';
				$_data['bordercolor'] = '';
				$_data['width'] = '';
				$_data['element_css_class'] = '';
				$_data['element_css_id'] = '';
				$element->setData($_data);
				$element->save();
			}
		}
		
		Mage::helper('cmsplus')->setAction('blockedit');
		Mage::helper('cmsplus')->setLastid($params['blockid']);
		$this->_forward('addBlock');	
	}
	
	public function editElementAction() {
		$params = $this->getRequest()->getParams();
		if (isset($params) && isset($params['elementid'])) {
			Mage::helper('cmsplus')->setAction('elementedit');
			Mage::helper('cmsplus')->setLastid($params['elementid']);
		}
		$this->_initAction();
		$this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
		$this->renderLayout();
	}

	public function addBlockAction() {
		$params = $this->getRequest()->getParams();
		if (isset($params) && isset($params['blockid'])) {
			Mage::helper('cmsplus')->setAction('blockedit');
			Mage::helper('cmsplus')->setLastid($params['blockid']);
		}
		$this->_initAction();
		$this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
		$this->renderLayout();
	}
	
	public function addImageAction() {
		$params = $this->getRequest()->getParams();
		if (isset($params) && isset($params['imageid'])) {
			Mage::helper('cmsplus')->setAction('imageedit');
			Mage::helper('cmsplus')->setLastid($params['imageid']);
		}
		$this->_initAction();
		$this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
		$this->renderLayout();
	}
	
	public function saveImageAction() {
		$params = $this->getRequest()->getParams();
		if (isset($params) && isset($params['isnew']) && $params['isnew'] == 1) {
			$image = Mage::getModel('cmsplus/cz_cms_content_page_image');
			$image->setCreatedUid(Mage::getSingleton('admin/session')->getUser()->getId());
			$image->setCreatedDate(date('U'));
			$lastSortOrder = Mage::getModel('cmsplus/cz_cms_content_page_image')->getCollection()->setHidden(0)->getLastSortOrder($params['pageid'],$image->getData('cz_cms_type_idcz_cms_content_block_type'));
			$lastSortOrder++;
			$image->setSortOrder($lastSortOrder);
		} else {

			$image = Mage::getModel('cmsplus/cz_cms_content_page_image')->load($params['imageid']);
			$image->setModifiedUid(Mage::getSingleton('admin/session')->getUser()->getId());
			$image->setModifiedDate(date('U'));
		}
		$image->setData('hidden',0);
		foreach($params['settings'] AS $_key => $_val) {
			$image->setData($_key,$_val);
		}
		if (!is_null($_FILES) && array_key_exists('image',$_FILES)) {
			$imageData = $this->getRequest()->getParam('image');
			if (isset($imageData['delete']) && $imageData['delete'] == 1) { 
				$image->setData('file','');
				try {
					unlink(Mage::getBaseDir('media').'/'.$imageData['value']);
				} catch (Exception $e) {
				}
			}
			if ($_FILES['image']['name'] != '') {
				try {
					$uploader = new Varien_File_Uploader('image');
					$uploader->setAllowedExtensions(array(
						'jpg', 'jpeg', 'gif', 'png'
					));
					$name = $_FILES['image']['name'];
					$result = $uploader->save(Mage::getBaseDir('media').'/cmsplus/');
				} catch(exception $e) {
				}
				$image->setData('file','cmsplus/'.$name);
			}
		}

		$image->save();
		
		$this->clearCache();
		$this->clearImageCache();
		Mage::getSingleton('core/session')->addSuccess(Mage::helper('cmsplus')->__('Image saved'));
		Mage::helper('cmsplus')->setAction('imageedit');
		Mage::helper('cmsplus')->setLastid($image->getId());
		Mage::helper('cmsplus')->setTab('images');
		$this->_forward('addimage');
	}

	private function sendMail($data) {
		$storeId = Mage::app()->getStore()->getId();
		$mailSubject = '';
		
	
		Mage::getModel('core/email_template')
			->setTemplateSubject($mailSubject)
			->sendTransactional(
			Mage::getStoreConfig('cz/cmsplus/email_template'),
			Mage::getStoreConfig('cz/cmsplus/sender_email_identity'),
			Mage::getStoreConfig('cz/cmsplus/recipient_email'),
			null,
			$data,
		$storeId);
	}

	public function saveElementAction() {
		$params = $this->getRequest()->getParams();
		$element = Mage::getModel('cmsplus/cz_cms_content_element')->load($params['elementid']);
		if ($element) {
			if (Mage::helper('cmsplus')->isEditor()) {
				$element->setEditorContent($params['settings']['content']);
				//Send Mail
				
				
				
				
				$_params = array();
				$_params['_current'] 	=	false;
				$_params['store'] = Mage::helper('cmsplus')->getStoreId();
				
				$_params['elementid']	=	$element->getId();
				$params = Mage::app()->getFrontController()->getRequest()->getParams();
				if (isset($params) && isset($params['menuid'])) {
					$_params['menuid'] = $params['menuid'];
				}
				
				if (isset($params) && isset($params['pageid'])) {
					$_params['pageid'] = $params['pageid'];
				}
				$_params['blockid']		=	$element->getData('cz_cms_content_block_idcz_cms_content_block');
				$link = Mage::helper('adminhtml')->getUrl('*/*/editElement',$_params);
			
			
				$user = Mage::getSingleton('admin/session')->getUser();
				$_params['url'] = $link;
				$_params['user'] = $user;
				
				$this->sendMail($_params);
			} elseif (isset($params['acceptchange']) && $params['acceptchange'] == 1) {
				$element->setContent($element->getEditorContent());
				$element->setEditorContent(null);
			} elseif (isset($params['declinechange']) && $params['declinechange'] == 1) {
				$element->setEditorContent(null);
			} else {
				$element->setData('hidden',0);
				foreach($params['settings'] AS $_key => $_val) {
					$element->setData($_key,$_val);
				}
				if (isset($params['category_products'])) {
					$_products = explode('&',$params['category_products']);
					if (isset($_products['on'])) {
						unset($_products['on']);
					}
					$_pIds = array();
					foreach ($_products AS $_prod) {
						$_prod = explode('=',$_prod);
						@$_pIds[$_prod[0]] = $_prod[1];
					}
					$element->setCategories($_pIds);
				} else {
					$element->setCategories(array());
				}
			}
			$element->setModifiedUid(Mage::getSingleton('admin/session')->getUser()->getId());
			$element->setModifiedDate(date('U'));
			$element->save();
			
		}
		$this->clearCache();
		Mage::getSingleton('core/session')->addSuccess(Mage::helper('cmsplus')->__('Element saved'));
		Mage::helper('cmsplus')->setAction('elementedit');
		Mage::helper('cmsplus')->setLastid($element->getId());
		$this->_forward('editElement');
	}
	
	public function saveBlockAction() {
		$params = $this->getRequest()->getParams();

		if (isset($params) && isset($params['isnew']) && $params['isnew'] == 1) {
			$block = Mage::getModel('cmsplus/cz_cms_content_block');
			$access = array(
				'view'		=>	array(),
				'viewedit'	=>	array()
			);
			$block->setData('access',serialize($access));
			foreach($params['settings'] AS $_key => $_val) {
				if (is_array($_val)) {
					$_val = serialize($_val);
				}
				$block->setData($_key,$_val);
			}
			
			
			//get last sort order
			$lastSortOrder = Mage::getModel('cmsplus/cz_cms_content_block')->getCollection()->setHidden(0)->getLastSortOrder($params['pageid']);
			$lastSortOrder++;
			$block->setSortOrder($lastSortOrder);
			$block->setData('cz_cms_content_page_idcz_cms_content_page',$params['pageid']);
			$block->setStoreviewCode(Mage::helper('cmsplus')->getStoreId());
			$block->setCreatedUid(Mage::getSingleton('admin/session')->getUser()->getId());
			$block->setCreatedDate(date('U'));
			$block->save();
			//create elements
			$columns = $block->getData('cz_cms_type_idcz_cms_content_block_type');
			for ($i=1;$i<=$columns;$i++) {
				$element = Mage::getModel('cmsplus/cz_cms_content_element');
				$element->setSortOrder($i);
				$element->setData('cz_cms_content_block_idcz_cms_content_block',$block->getId());
				$element->setStoreviewCode(Mage::helper('cmsplus')->getStoreId());
				$element->setCreatedUid(Mage::getSingleton('admin/session')->getUser()->getId());
				$element->setCreatedDate(date('U'));
				$element->save();
			}
			
		} else {

			$block = Mage::getModel('cmsplus/cz_cms_content_block')->load($params['blockid']);
			if ($block) {
				$block->setData('hidden',0);
				
				//check Type
				$oldtype = $block->getData('cz_cms_type_idcz_cms_content_block_type');
				$newtype = $params['settings']['cz_cms_type_idcz_cms_content_block_type'];
				
				if ($oldtype != $newtype) {
					if ($newtype < $oldtype) {
						//delete elemente
						for ($i=$oldtype;$i>$newtype;$i--) {
							$elements = Mage::getModel('cmsplus/cz_cms_content_element')->getCollection()->setHidden(0)->loadByBlockIdAndSortOrder($block->getId(),$i);
							foreach ($elements AS $element) {
								$element->delete();
							}
						}
					} else {
						//create elements
						for ($i=$oldtype+1;$i<=$newtype;$i++) {
							$element = Mage::getModel('cmsplus/cz_cms_content_element');
							$element->setSortOrder($i);
							$element->setData('cz_cms_content_block_idcz_cms_content_block',$block->getId());
							$element->setStoreviewCode(Mage::helper('cmsplus')->getStoreId());
							$element->setCreatedUid(Mage::getSingleton('admin/session')->getUser()->getId());
							$element->setCreatedDate(date('U'));
							$element->save();
						}
					}
				}
				$access = array(
					'view'		=>	array(),
					'viewedit'	=>	array()
				);
				$block->setData('access',serialize($access));
				foreach($params['settings'] AS $_key => $_val) {
					if (is_array($_val)) {
						$_val = serialize($_val);
					}
					$block->setData($_key,$_val);
				}
				$block->setModifiedUid(Mage::getSingleton('admin/session')->getUser()->getId());
				$block->setModifiedDate(date('U'));
				$block->save();
			}
			
		}
		Mage::getSingleton('core/session')->addSuccess(Mage::helper('cmsplus')->__('Block saved'));
		$this->clearCache();
		Mage::helper('cmsplus')->setAction('blockedit');
		Mage::helper('cmsplus')->setLastid($block->getId());
		$this->_forward('addBlock');
	}

	private function clearCache() {
		Mage::app()->getCache()->clean(Zend_Cache::CLEANING_MODE_MATCHING_ANY_TAG,array('CMSPLUS'));
	}
	private function clearImageCache() {
		$path = Mage::getBaseDir('media').'/cache/cmsplus/';
		if (is_dir($path)) {
			$files = glob($path.'*');
			if ($files && is_array($files)) {
				foreach ($files AS $_file) {
					unlink($_file);
				}
			}
		}
	}

	public function savePageAction() {
		$params = $this->getRequest()->getParams();
		if (isset($params) && isset($params['isnew']) && $params['isnew'] == 1) {
			$page = Mage::getModel('cmsplus/cz_cms_content_page');
			$access = array(
				'view'		=>	array(),
				'viewedit'	=>	array()
			);
			$page->setData('access',serialize($access));
			foreach($params['settings'] AS $_key => $_val) {
				if (is_array($_val)) {
					$_val = serialize($_val);
				}
				$page->setData($_key,$_val);
			}




			if (is_null($page->getUrlkey()) || $page->getUrlkey() == '' || strtolower($page->getUrlkey()) == 'null' ) {
				$page->setUrlkey(Mage::helper('cmsplus')->makeUrlKey($page->getName()));
			} else {
				$page->setUrlkey(Mage::helper('cmsplus')->makeUrlKey($page->getUrlkey()));
			}
			$page->setStoreviewCode(Mage::helper('cmsplus')->getStoreId());
			$page->setCreatedUid(Mage::getSingleton('admin/session')->getUser()->getId());
			$page->setCreatedDate(date('U'));
			$page->save();
		} else {
			$page = Mage::getModel('cmsplus/cz_cms_content_page')->load($params['pageid']);
			if ($page) {
				$page->setData('hidden',0);
				$access = array(
					'view'		=>	array(),
					'viewedit'	=>	array()
				);
				$page->setData('access',serialize($access));
				foreach($params['settings'] AS $_key => $_val) {
					if (is_array($_val)) {
						$_val = serialize($_val);
					}
					$page->setData($_key,$_val);
				}
				if (is_null($page->getUrlkey()) || $page->getUrlkey() == '' || strtolower($page->getUrlkey()) == 'null' ) {
					$page->setUrlkey(Mage::helper('cmsplus')->makeUrlKey($page->getName()));
				} else {
					$page->setUrlkey(Mage::helper('cmsplus')->makeUrlKey($page->getUrlkey()));
				}

				if (isset($params['category_products'])) {
					$_products = explode('&',$params['category_products']);
					if (isset($_products['on'])) {
						unset($_products['on']);
					}
					$_pIds = array();
					foreach ($_products AS $_prod) {
						$_prod = explode('=',$_prod);
						$_pIds[$_prod[0]] = $_prod[1];
					}
					$page->setCategories($_pIds);
				} else {
					$page->setCategories(array());
				}
				//Access
				//$access = implode(',',$params['settings']['access']);
				//$page->setAccess($access);
				
				$page->setModifiedUid(Mage::getSingleton('admin/session')->getUser()->getId());
				$page->setModifiedDate(date('U'));
				$page->save();
				
			}
		}
		$this->clearCache();
		
		Mage::getSingleton('core/session')->addSuccess(Mage::helper('cmsplus')->__('Page saved'));
		Mage::helper('cmsplus')->setAction('pageedit');
		Mage::helper('cmsplus')->setLastid($page->getId());
		$addParams = array(
			'pageid'	=>	$page->getId(),
		);
		Mage::helper('cmsplus')->setAdditionalParams($addParams);
		$this->_forward('addpage',null,null,$addParams);
	}
	public function saveMenuAction() {
		$params = $this->getRequest()->getParams();
		if (isset($params) && isset($params['isnew']) && $params['isnew'] == 1) {
			//new menü
			$menu = Mage::getModel('cmsplus/cz_cms_menu');
			foreach($params['settings'] AS $_key => $_val) {
				$menu->setData($_key,$_val);
			}
			$menu->setStoreviewCode(Mage::helper('cmsplus')->getStoreId());
			$menu->setCreatedUid(Mage::getSingleton('admin/session')->getUser()->getId());
			$menu->setCreatedDate(date('U'));
			$menu->save();
		} else {
			$menu = Mage::getModel('cmsplus/cz_cms_menu')->load($params['menuid']);
			if ($menu) {
				$menu->setData('hidden',0);
				foreach($params['settings'] AS $_key => $_val) {
					$menu->setData($_key,$_val);
				}
				$menu->setStoreviewCode(Mage::helper('cmsplus')->getStoreId());
				$menu->setModifiedUid(Mage::getSingleton('admin/session')->getUser()->getId());
				$menu->setModifiedDate(date('U'));
				$menu->save();
			}

		}
		$this->clearCache();
		Mage::getSingleton('core/session')->addSuccess(Mage::helper('cmsplus')->__('Menu saved'));
		Mage::helper('cmsplus')->setAction('menuedit');
		Mage::helper('cmsplus')->setLastid($menu->getId());
		$this->_forward('addmenu');

	}

	public function deleteMenuAction() {
		$params = $this->getRequest()->getParams();
		if (isset($params) && isset($params['menuid'])) {
			$menu = Mage::getModel('cmsplus/cz_cms_menu')->load($params['menuid']);
			if ($menu) {
				$menu->deleteRecursive();
			}
		}
		$this->clearCache();
		$this->_forward('index');
	}
	
	public function deletePageAction() {
		$params = $this->getRequest()->getParams();
		if (isset($params) && isset($params['pageid'])) {
			$page = Mage::getModel('cmsplus/cz_cms_content_page')->load($params['pageid']);
			if ($page) {
				$page->deleteRecursive();
			}
		}
		$this->clearCache();
		$this->_forward('index');
	}
	
	public function changeStoreAction() {
		$params = $this->getRequest()->getParams();
		$this->_forward('index');
	}
	
	public function deleteImageAction() {
		$params = $this->getRequest()->getParams();
		if (isset($params) && isset($params['imageid'])) {
			$image = Mage::getModel('cmsplus/cz_cms_content_page_image')->load($params['imageid']);
			if ($image) {
				$image->delete();
			}
		}
		$this->clearCache();
		$this->clearImageCache();
		Mage::helper('cmsplus')->setAction('pageedit');
		Mage::helper('cmsplus')->setLastid($params['pageid']);
		Mage::helper('cmsplus')->setTab('images');
		$this->_forward('addPage');
	}
	
	public function deleteBlockAction() {
		$params = $this->getRequest()->getParams();
		if (isset($params) && isset($params['blockid'])) {
			$block = Mage::getModel('cmsplus/cz_cms_content_block')->load($params['blockid']);
			if ($block) {
				$block->deleteRecursive();
			}
		}
		$this->clearCache();
		$this->_forward('addPage');
	}
	
	public function setOrderAction() {
		$this->clearCache();
		$params = $this->getRequest()->getParams();
		switch ($params['type']) {
			case 'image':
				$direction = $params['direction'];
				$images = Mage::getModel('cmsplus/cz_cms_content_page_image')->getCollection()->setHidden(0)->loadByPageIdAndTypeIdAndOrder($params['pageid'],$params['typeid'],$direction);
				$_found = false;
				$_maximgs = count($images);
				foreach ($images AS $image) {
					//var_dump($image->getData());
					//continue;
					if ($_found) {
						$image->setSortOrder($sort);
						$image->save();
						$_found = false;
					} elseif ($image->getId() == $params['imageid']) {
						if ($direction == 'down') {
							$sort = $image->getSortOrder();
							$sortnew = $sort + 1;
							if ($sortnew > $_maximgs) {
								$sortnew = $_maximgs;
							} elseif ($sortnew <1) {
								$sortnew = 1;
							}
							$image->setSortOrder($sortnew);
							$image->save();
							$_found = true;
						} else {
							$sort = $image->getSortOrder();
							$sortnew = $sort - 1;
							if ($sortnew > $_maximgs) {
								$sortnew = $_maximgs;
							} elseif ($sortnew <1) {
								$sortnew = 1;
							}
							$image->setSortOrder($sortnew);
							$image->save();
							$_found = true;
						}
					}
				}

				Mage::helper('cmsplus')->setAction('pageedit');
				Mage::helper('cmsplus')->setLastid($params['pageid']);
				Mage::helper('cmsplus')->setTab('images');
				$this->_forward('addPage');
				return;
			break;
			
			case 'block':
				$direction = $params['direction'];
				$elements = Mage::getModel('cmsplus/cz_cms_content_block')->getCollection()->setHidden(0)->loadByPageIdAndOrder($params['pageid'],$direction);
				$_found = false;
				foreach ($elements AS $element) {
					//var_dump($element->getData());
					//continue;
					if ($_found) {
						$element->setSortOrder($sort);
						$element->save();
						$_found = false;
					} elseif ($element->getId() == $params['blockid']) {
						if ($direction == 'down') {
							$sort = $element->getSortOrder();
							$sortnew = $sort + 1;

							if ($sortnew <1) {
								$sortnew = 1;
							}
							$element->setSortOrder($sortnew);
							$element->save();
							$_found = true;
						} else {
							$sort = $element->getSortOrder();
							$sortnew = $sort - 1;

							if ($sortnew <1) {
								$sortnew = 1;
							}
							$element->setSortOrder($sortnew);
							$element->save();
							$_found = true;
						}
					}
					
				}

				Mage::helper('cmsplus')->setAction('pageedit');
				Mage::helper('cmsplus')->setLastid($params['pageid']);
				$this->_forward('addPage');
				return;
			break;
			
			case 'element':
				$direction = $params['direction'];
				$elements = Mage::getModel('cmsplus/cz_cms_content_element')->getCollection()->setHidden(0)->loadByBlockIdAndOrder($params['blockid'],$direction);
				$_found = false;
				foreach ($elements AS $element) {
					if ($_found) {
						$element->setSortOrder($sort);
						$element->save();
						$_found = false;
					} elseif ($element->getId() == $params['elementid']) {
						if ($direction == 'down') {
							$sort = $element->getSortOrder();
							$sortnew = $sort + 1;
							if ($sortnew > 3) {
								$sortnew = 3;
							} elseif ($sortnew <1) {
								$sortnew = 1;
							}
							$element->setSortOrder($sortnew);
							$element->save();
							$_found = true;
						} else {
							$sort = $element->getSortOrder();
							$sortnew = $sort - 1;
							if ($sortnew > 3) {
								$sortnew = 3;
							} elseif ($sortnew <1) {
								$sortnew = 1;
							}
							$element->setSortOrder($sortnew);
							$element->save();
							$_found = true;
						}
					}
					
				}
				Mage::helper('cmsplus')->setAction('blockedit');
				Mage::helper('cmsplus')->setLastid($params['blockid']);
				$this->_forward('addBlock');
				return;
			break;
			case 'page':
				$otherPageId = 0;
				if (isset($params['nextid'])) {
					$direction = 'next';
					$otherPageId = $params['nextid'];
				} elseif(isset($params['previd'])) {
					$direction = 'prev';
					$otherPageId = $params['previd'];
				}
				$sortPage = Mage::getModel('cmsplus/cz_cms_content_page')->load($params['pageid']);
				$oldPage = Mage::getModel('cmsplus/cz_cms_content_page')->load($otherPageId);
				$parentId = $oldPage->getData('idcz_cms_content_page_pid');
				if ($parentId == 0) {
					$menuId = $oldPage->getMenu();
					$pages = Mage::getModel('cmsplus/cz_cms_content_page')->getCollection()->setHidden(0)->loadByMenuIdAndSort($menuId,0,$direction);
				} else {
					$pages = Mage::getModel('cmsplus/cz_cms_content_page')->getCollection()->setHidden(0)->loadChildsAndOrder($parentId,$direction);
				}
				$count = $pages->count();
				if ($sortPage->getData('idcz_cms_content_page_pid') != $parentId) {
					$count++;
				}

				if ($direction == 'next') {
					$_actcount = $count;
				} else {
					$_actcount = 1;
				}
				foreach ($pages AS $page) {
					$page = Mage::getModel('cmsplus/cz_cms_content_page')->load($page->getId());
					$page->setSortOrder($_actcount);
					$page->save();
					if ($page->getId() == $otherPageId) {
						if ($direction == 'next') {
							$_actcount--;
						} else {
							$_actcount++;
						}
						$sortPage->setSortOrder($_actcount);
						$sortPage->setData('idcz_cms_content_page_pid',$parentId);
						$sortPage->save();
					}
					
					if ($direction == 'next') {
						$_actcount--;
					} else {
						$_actcount++;
					}
				}
				$this->_forward('index');
			break;	
		}
	}

	
	public function wysiwygAction() {
		$elementId = $this->getRequest()->getParam('element_id', md5(microtime()));
		$storeId = Mage::helper('cmsplus')->getStoreId();
		$storeMediaUrl = Mage::app()->getStore($storeId)->getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA);
		$content = $this->getLayout()->createBlock('cmsplus/adminhtml_wysiwyg_content', '', array(
			'editor_element_id' => $elementId,
			'store_id'          => $storeId,
			'store_media_url'   => $storeMediaUrl,
		));
		$this->getResponse()->setBody($content->toHtml());
	}
	
	public function gridAction() {
		$this->getResponse()->setBody(
		$this->getLayout()->createBlock('cmsplus/adminhtml_addpage_tabs_mapping', 'category.product.grid')->toHtml());
	}
	
	public function gridElementAction() {
		$this->getResponse()->setBody(
		$this->getLayout()->createBlock('cmsplus/adminhtml_editelement_tabs_mapping', 'category.product.grid')->toHtml());
	}

}