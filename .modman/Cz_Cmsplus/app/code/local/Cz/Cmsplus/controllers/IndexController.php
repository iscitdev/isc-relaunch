<?php
/**
 * clever + zöger gmbh
 * http://www.clever-zoeger.de
 * @author Ronny Hempel <rhempel@clever-zoeger.de>
 * @version $Id: IndexController.php 381 2012-04-24 09:04:02Z rhempel $
 * $HeadURL: https://svn.clever-zoeger.de/svn/Magento/extensions/Cz_Cmsplus/tags/1.3.0/cmsPlus/app/code/local/Cz/Cmsplus/controllers/IndexController.php $
 * $LastChangedBy: rhempel $
 * $LastChangedRevision: 381 $
 * $LastChangedDate: 2012-04-24 11:04:02 +0200 (Tue, 24 Apr 2012) $
 */
?>
<?php

class Cz_Cmsplus_IndexController extends Mage_Core_Controller_Front_Action {

	public function indexAction() {
		$this->loadLayout();
		$block = $this->getLayout()->createBlock(
			'cmsplus/cmsplus',
			'cmspluspage',
			array('template' => 'cz/cmsplus/page.phtml')
		);
		$this->getLayout()->getBlock('content')->append($block);
		
		if (Mage::helper('cmsplus')->getError() == 1) {
			$this->_forward('noRoute');
		}
		$this->_initLayoutMessages('core/session');
		$this->_initLayoutMessages('checkout/session');
		$this->renderLayout();
	}
	
	public function noRouteAction($coreRoute = null) {
		$this->getResponse()->setHeader('HTTP/1.1','404 Not Found');
		$this->getResponse()->setHeader('Status','404 File not found');

		$pageId = Mage::getStoreConfig(Mage_Cms_Helper_Page::XML_PATH_NO_ROUTE_PAGE);
		//$path = Mage::getUrl().'/'.$pageId;
		$this->_redirect($pageId);
	}

}