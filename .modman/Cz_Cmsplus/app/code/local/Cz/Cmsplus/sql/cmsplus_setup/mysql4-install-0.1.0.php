<?php
/**
 * clever + zöger gmbh
 * http://www.clever-zoeger.de
 * @author Ronny Hempel <rhempel@clever-zoeger.de>
 * @version $Id: mysql4-install-0.1.0.php 181 2011-09-12 14:37:18Z rhempel $
 * $HeadURL: https://svn.clever-zoeger.de/svn/Magento/extensions/Cz_Cmsplus/tags/1.3.0/cmsPlus/app/code/local/Cz/Cmsplus/sql/cmsplus_setup/mysql4-install-0.1.0.php $
 * $LastChangedBy: rhempel $
 * $LastChangedRevision: 181 $
 * $LastChangedDate: 2011-09-12 16:37:18 +0200 (Mon, 12 Sep 2011) $
 */
?>
<?php

$installer = $this;

$installer->startSetup();

$installer->run("

CREATE  TABLE IF NOT EXISTS `{$this->getTable('cz_cms_content_page')}` (
  `idcz_cms_content_page` INT(11) NOT NULL AUTO_INCREMENT,
  `idcz_cms_content_page_pid` INT(11) NULL DEFAULT NULL ,
  `name` VARCHAR(100) NULL DEFAULT NULL ,
  `name_alternative` VARCHAR(150) NULL DEFAULT NULL ,
  `page_title` VARCHAR(100) NULL DEFAULT NULL ,
  `urlkey` VARCHAR(45) NULL DEFAULT NULL ,
  `headline` VARCHAR(250) NULL DEFAULT NULL ,
  `subheadline` VARCHAR(250) NULL DEFAULT NULL ,
  `page_css_class` VARCHAR(50) NULL DEFAULT NULL ,
  `page_css_id` VARCHAR(50) NULL DEFAULT NULL ,
  `description` TEXT NULL DEFAULT NULL ,
  `meta_description` VARCHAR(250) NULL DEFAULT NULL ,
  `meta_keywords` VARCHAR(100) NULL DEFAULT NULL ,
  `storeview_code` INT(11) NULL DEFAULT NULL ,
  `sort_order` INT(11) NULL DEFAULT NULL ,
  `hidden` TINYINT(4) NOT NULL DEFAULT 0 ,
  `created_uid` INT(11) NULL DEFAULT NULL ,
  `created_date` INT(11) NULL DEFAULT NULL ,
  `modified_uid` INT(11) NULL DEFAULT NULL ,
  `modified_date` INT(11) NULL DEFAULT NULL ,
  `cacheable` TINYINT(4) NULL DEFAULT NULL ,
  PRIMARY KEY (`idcz_cms_content_page`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;


CREATE  TABLE IF NOT EXISTS `{$this->getTable('cz_cms_content_page_image')}` (
  `idcz_cms_content_page_image` INT(11) NOT NULL AUTO_INCREMENT,
  `file` VARCHAR(255) NULL DEFAULT NULL ,
  `name` VARCHAR(100) NULL DEFAULT NULL ,
  `title_text` VARCHAR(100) NULL DEFAULT NULL ,
  `alt_text` VARCHAR(100) NULL DEFAULT NULL ,
  `image_css_class` VARCHAR(50) NULL DEFAULT NULL ,
  `image_css_id` VARCHAR(50) NULL DEFAULT NULL ,
  `sort_order` INT(11) NULL DEFAULT NULL ,
  `hidden` TINYINT(4) NOT NULL DEFAULT 0 ,
  `created_uid` INT(11) NULL DEFAULT NULL ,
  `created_date` INT(11) NULL DEFAULT NULL ,
  `modified_uid` INT(11) NULL DEFAULT NULL ,
  `modified_date` INT(11) NULL DEFAULT NULL ,
  `cacheable` TINYINT(4) NULL DEFAULT NULL ,
  `cz_cms_type_idcz_cms_content_block_type` INT(11) NOT NULL ,
  PRIMARY KEY (`idcz_cms_content_page_image`) ,
  INDEX `fk_cz_cms_content_page_image_cz_cms_type1` (`cz_cms_type_idcz_cms_content_block_type` ASC) ,
  CONSTRAINT `fk_cz_cms_content_page_image_cz_cms_type1`
    FOREIGN KEY (`cz_cms_type_idcz_cms_content_block_type` )
    REFERENCES `{$this->getTable('cz_cms_type')}` (`idcz_cms_content_block_type` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;


CREATE  TABLE IF NOT EXISTS `{$this->getTable('cz_cms_content_page_access')}` (
  `idcz_cms_menu_access` INT(11) NOT NULL AUTO_INCREMENT,
  `user` INT(11) NULL DEFAULT NULL ,
  `group` INT(11) NULL DEFAULT NULL ,
  `right` VARCHAR(45) NULL DEFAULT NULL ,
  `cz_cms_menu_idcz_cms_menu1` INT(11) NOT NULL ,
  PRIMARY KEY (`idcz_cms_menu_access`) ,
  INDEX `fk_cz_cms_menu_access_cz_cms_menu2` (`cz_cms_menu_idcz_cms_menu1` ASC) ,
  CONSTRAINT `fk_cz_cms_menu_access_cz_cms_menu2`
    FOREIGN KEY (`cz_cms_menu_idcz_cms_menu1` )
    REFERENCES `{$this->getTable('cz_cms_content_page')}` (`idcz_cms_content_page` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;

CREATE  TABLE IF NOT EXISTS `{$this->getTable('cz_cms_content_block')}` (
  `idcz_cms_content_block` INT(11) NOT NULL AUTO_INCREMENT,
  `cz_cms_content_page_idcz_cms_content_page` INT(11) NOT NULL ,
  `name` VARCHAR(100) NULL DEFAULT NULL ,
  `space_before` SMALLINT(5) NULL DEFAULT NULL ,
  `space_after` SMALLINT(5) NULL DEFAULT NULL ,
  `border` TINYINT(4) NULL DEFAULT NULL ,
  `border_color` VARCHAR(6) NULL DEFAULT NULL ,
  `width` VARCHAR(10) NULL DEFAULT NULL ,
  `block_css_class` VARCHAR(50) NULL DEFAULT NULL ,
  `block_css_id` VARCHAR(50) NULL DEFAULT NULL ,
  `headline` VARCHAR(150) NULL DEFAULT NULL ,
  `description` TEXT NULL DEFAULT NULL ,
  `storeview_code` TINYINT(4) NULL DEFAULT NULL ,
  `sort_order` INT(11) NULL DEFAULT NULL ,
  `hidden` TINYINT(4) NOT NULL DEFAULT 0 ,
  `created_uid` INT(11) NULL DEFAULT NULL ,
  `created_date` INT(11) NULL DEFAULT NULL ,
  `modified_uid` INT(11) NULL DEFAULT NULL ,
  `modified_date` INT(11) NULL DEFAULT NULL ,
  `cacheable` TINYINT(4) NULL DEFAULT NULL ,
  `cz_cms_type_idcz_cms_content_block_type` INT(11) NOT NULL ,
  PRIMARY KEY (`idcz_cms_content_block`) ,
  INDEX `fk_cz_cms_block_cz_cms_content_page1` (`cz_cms_content_page_idcz_cms_content_page` ASC) ,
  INDEX `fk_cz_cms_content_block_cz_cms_type1` (`cz_cms_type_idcz_cms_content_block_type` ASC) ,
  CONSTRAINT `fk_cz_cms_block_cz_cms_content_page1`
    FOREIGN KEY (`cz_cms_content_page_idcz_cms_content_page` )
    REFERENCES `{$this->getTable('cz_cms_content_page')}` (`idcz_cms_content_page` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cz_cms_content_block_cz_cms_type1`
    FOREIGN KEY (`cz_cms_type_idcz_cms_content_block_type` )
	REFERENCES `{$this->getTable('cz_cms_type')}` (`idcz_cms_content_block_type` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;

CREATE  TABLE IF NOT EXISTS `{$this->getTable('cz_cms_type')}` (
  `idcz_cms_content_block_type` INT(11) NOT NULL AUTO_INCREMENT,
  `table` VARCHAR(45) NOT NULL ,
  `name` VARCHAR(45) NULL DEFAULT NULL ,
  PRIMARY KEY (`idcz_cms_content_block_type`) ,
  UNIQUE INDEX `table_name` (`table` ASC, `name` ASC) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;

CREATE  TABLE IF NOT EXISTS `{$this->getTable('cz_cms_content_element')}` (
  `idcz_cms_content_element` INT(11) NOT NULL AUTO_INCREMENT,
  `cz_cms_content_block_idcz_cms_content_block` INT(11) NOT NULL ,
  `name` VARCHAR(100) NULL DEFAULT NULL ,
  `content` LONGTEXT NULL DEFAULT NULL ,
  `space_before` SMALLINT(5) NULL DEFAULT NULL ,
  `space_after` SMALLINT(5) NULL DEFAULT NULL ,
  `border` TINYINT(4) NULL DEFAULT NULL ,
  `bordercolor` VARCHAR(6) NULL DEFAULT NULL ,
  `width` VARCHAR(10) NULL DEFAULT NULL ,
  `element_css_class` VARCHAR(50) NULL DEFAULT NULL ,
  `element_css_id` VARCHAR(50) NULL DEFAULT NULL ,
  `storeview_code` INT(11) NULL DEFAULT NULL ,
  `sort_order` INT(11) NULL DEFAULT NULL ,
  `hidden` TINYINT(4) NOT NULL DEFAULT 0 ,
  `created_uid` INT(11) NULL DEFAULT NULL ,
  `created_date` INT(11) NULL DEFAULT NULL ,
  `modified_uid` INT(11) NULL DEFAULT NULL ,
  `modified_date` INT(11) NULL DEFAULT NULL ,
  `cacheable` TINYINT(4) NULL DEFAULT NULL ,
  PRIMARY KEY (`idcz_cms_content_element`) ,
  INDEX `fk_cz_cms_content_cz_cms_block1` (`cz_cms_content_block_idcz_cms_content_block` ASC) ,
  CONSTRAINT `fk_cz_cms_content_cz_cms_block1`
    FOREIGN KEY (`cz_cms_content_block_idcz_cms_content_block` )
    REFERENCES `{$this->getTable('cz_cms_content_block')}` (`idcz_cms_content_block` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;

CREATE  TABLE IF NOT EXISTS `{$this->getTable('cz_cms_archive')}` (
  `idcz_cms_archive` INT(11) NOT NULL AUTO_INCREMENT,
  `date` VARCHAR(45) NULL DEFAULT NULL ,
  `table_name` VARCHAR(45) NULL DEFAULT NULL ,
  `table_entry_id` VARCHAR(45) NULL DEFAULT NULL ,
  `created_uid` INT(11) NULL DEFAULT NULL ,
  `created_date` INT(11) NULL DEFAULT NULL ,
  `item` BLOB NULL DEFAULT NULL ,
  PRIMARY KEY (`idcz_cms_archive`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;

CREATE  TABLE IF NOT EXISTS `{$this->getTable('cz_cms_menu')}` (
  `idcz_cms_menu` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(100) NULL DEFAULT NULL ,
  `menu_css_class` VARCHAR(50) NULL DEFAULT NULL ,
  `menu_css_id` VARCHAR(50) NULL DEFAULT NULL ,
  `storeview_code` INT(11) NULL DEFAULT NULL ,
  `hidden` TINYINT(4) NOT NULL ,
  `created_uid` INT(11) NULL DEFAULT NULL ,
  `created_date` INT(11) NULL DEFAULT NULL ,
  `modified_uid` INT(11) NULL DEFAULT NULL ,
  `modified_date` INT(11) NULL DEFAULT NULL ,
  PRIMARY KEY (`idcz_cms_menu`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;

CREATE  TABLE IF NOT EXISTS `{$this->getTable('cz_cms_content_page_has_cz_cms_menu')}` (
  `cz_cms_content_page_idcz_cms_content_page` INT(11) NOT NULL ,
  `cz_cms_menu_idcz_cms_menu` INT(11) NOT NULL ,
  PRIMARY KEY (`cz_cms_content_page_idcz_cms_content_page`, `cz_cms_menu_idcz_cms_menu`) ,
  INDEX `fk_cz_cms_content_page_has_cz_cms_menu_cz_cms_menu1` (`cz_cms_menu_idcz_cms_menu` ASC) ,
  INDEX `fk_cz_cms_content_page_has_cz_cms_menu_cz_cms_content_page1` (`cz_cms_content_page_idcz_cms_content_page` ASC) ,
  CONSTRAINT `fk_cz_cms_content_page_has_cz_cms_menu_cz_cms_content_page1`
    FOREIGN KEY (`cz_cms_content_page_idcz_cms_content_page` )
    REFERENCES `{$this->getTable('cz_cms_content_page')}` (`idcz_cms_content_page` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cz_cms_content_page_has_cz_cms_menu_cz_cms_menu1`
    FOREIGN KEY (`cz_cms_menu_idcz_cms_menu` )
    REFERENCES `{$this->getTable('cz_cms_menu')}` (`idcz_cms_menu` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;


");





$installer->endSetup();
