<?php
/**
 * clever + zöger gmbh
 * http://www.clever-zoeger.de
 * @author Ronny Hempel <rhempel@clever-zoeger.de>
 * @version $Id: mysql4-upgrade-0.1.0-0.1.1.php 181 2011-09-12 14:37:18Z rhempel $
 * $HeadURL: https://svn.clever-zoeger.de/svn/Magento/extensions/Cz_Cmsplus/tags/1.3.0/cmsPlus/app/code/local/Cz/Cmsplus/sql/cmsplus_setup/mysql4-upgrade-0.1.0-0.1.1.php $
 * $LastChangedBy: rhempel $
 * $LastChangedRevision: 181 $
 * $LastChangedDate: 2011-09-12 16:37:18 +0200 (Mon, 12 Sep 2011) $
 */
?>
<?php

$installer = $this;

$installer->startSetup();

$installer->run("



INSERT INTO `{$this->getTable('cz_cms_type')}` (`idcz_cms_content_block_type`,`table`,`name`) VALUES (1,'block','1 Column');
INSERT INTO `{$this->getTable('cz_cms_type')}` (`idcz_cms_content_block_type`,`table`,`name`) VALUES (2,'block','2 Column');
INSERT INTO `{$this->getTable('cz_cms_type')}` (`idcz_cms_content_block_type`,`table`,`name`) VALUES (3,'block','3 Column');

");





$installer->endSetup();
