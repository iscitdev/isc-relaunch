<?php
/**
 * clever + zöger gmbh
 * http://www.clever-zoeger.de
 * @author Ronny Hempel <rhempel@clever-zoeger.de>
 * @version $Id: mysql4-upgrade-0.1.2-0.1.3.php 181 2011-09-12 14:37:18Z rhempel $
 * $HeadURL: https://svn.clever-zoeger.de/svn/Magento/extensions/Cz_Cmsplus/tags/1.3.0/cmsPlus/app/code/local/Cz/Cmsplus/sql/cmsplus_setup/mysql4-upgrade-0.1.2-0.1.3.php $
 * $LastChangedBy: rhempel $
 * $LastChangedRevision: 181 $
 * $LastChangedDate: 2011-09-12 16:37:18 +0200 (Mon, 12 Sep 2011) $
 */
?>
<?php

$installer = $this;

$installer->startSetup();

$installer->run("



ALTER TABLE `{$this->getTable('cz_cms_content_page_image')}` ADD COLUMN `cz_cms_content_page_idcz_cms_content_page` INT(11) NOT NULL  AFTER `cz_cms_type_idcz_cms_content_block_type` , 
  ADD CONSTRAINT `fk_cz_cms_content_page_image_cz_cms_content_page1`
  FOREIGN KEY (`cz_cms_content_page_idcz_cms_content_page` )
  REFERENCES `{$this->getTable('cz_cms_content_page')}` (`idcz_cms_content_page` )
  ON DELETE NO ACTION
  ON UPDATE NO ACTION
, ADD INDEX `fk_cz_cms_content_page_image_cz_cms_content_page1` (`cz_cms_content_page_idcz_cms_content_page` ASC) ;



");





$installer->endSetup();
