<?php
/**
 * clever + zöger gmbh
 * http://www.clever-zoeger.de
 * @author Ronny Hempel <rhempel@clever-zoeger.de>
 * @version $Id: mysql4-upgrade-0.1.6-0.1.7.php 181 2011-09-12 14:37:18Z rhempel $
 * $HeadURL: https://svn.clever-zoeger.de/svn/Magento/extensions/Cz_Cmsplus/tags/1.3.0/cmsPlus/app/code/local/Cz/Cmsplus/sql/cmsplus_setup/mysql4-upgrade-0.1.6-0.1.7.php $
 * $LastChangedBy: rhempel $
 * $LastChangedRevision: 181 $
 * $LastChangedDate: 2011-09-12 16:37:18 +0200 (Mon, 12 Sep 2011) $
 */
?>
<?php

$installer = $this;

$installer->startSetup();

$installer->run("

CREATE TABLE IF NOT EXISTS `{$this->getTable('cz_cms_content_page_has_catalog_category_entity')}` (
  `cz_cms_content_page_idcz_cms_content_page` int(11) NOT NULL,
  `catalog_category_entity_entity_id` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`cz_cms_content_page_idcz_cms_content_page`,`catalog_category_entity_entity_id`),
  KEY `fk_cz_cms_content_page_has_catalog_category_entity_catalog_ca1` (`catalog_category_entity_entity_id`),
  KEY `fk_cz_cms_content_page_has_catalog_category_entity_cz_cms_con1` (`cz_cms_content_page_idcz_cms_content_page`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `{$this->getTable('cz_cms_content_page_has_catalog_category_entity')}`
  ADD CONSTRAINT `fk_cz_cms_content_page_has_catalog_category_entity_cz_cms_con1` FOREIGN KEY (`cz_cms_content_page_idcz_cms_content_page`) REFERENCES `{$this->getTable('cz_cms_content_page')}` (`idcz_cms_content_page`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_cz_cms_content_page_has_catalog_category_entity_catalog_ca1` FOREIGN KEY (`catalog_category_entity_entity_id`) REFERENCES `catalog_category_entity` (`entity_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

");


$installer->endSetup();
