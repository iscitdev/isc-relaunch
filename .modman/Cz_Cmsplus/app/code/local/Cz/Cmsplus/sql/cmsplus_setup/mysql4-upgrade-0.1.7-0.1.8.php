<?php
/**
 * clever + zöger gmbh
 * http://www.clever-zoeger.de
 * @author Ronny Hempel <rhempel@clever-zoeger.de>
 * @version $Id: mysql4-upgrade-0.1.7-0.1.8.php 181 2011-09-12 14:37:18Z rhempel $
 * $HeadURL: https://svn.clever-zoeger.de/svn/Magento/extensions/Cz_Cmsplus/tags/1.3.0/cmsPlus/app/code/local/Cz/Cmsplus/sql/cmsplus_setup/mysql4-upgrade-0.1.7-0.1.8.php $
 * $LastChangedBy: rhempel $
 * $LastChangedRevision: 181 $
 * $LastChangedDate: 2011-09-12 16:37:18 +0200 (Mon, 12 Sep 2011) $
 */
?>
<?php

$installer = $this;

$installer->startSetup();

$installer->run("
ALTER TABLE `{$this->getTable('cz_cms_content_page_has_catalog_category_entity')}` DROP PRIMARY KEY;
ALTER TABLE `{$this->getTable('cz_cms_content_page_has_catalog_category_entity')}`  ADD `idczcmscontentpagehascatalogcategoryentity` INT NOT NULL AUTO_INCREMENT PRIMARY KEY FIRST
");

$installer->endSetup();
