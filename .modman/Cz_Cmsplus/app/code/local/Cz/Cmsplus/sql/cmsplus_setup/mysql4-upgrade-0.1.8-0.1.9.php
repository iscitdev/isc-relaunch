<?php
/**
 * clever + zöger gmbh
 * http://www.clever-zoeger.de
 * @author Ronny Hempel <rhempel@clever-zoeger.de>
 * @version $Id: mysql4-upgrade-0.1.8-0.1.9.php 181 2011-09-12 14:37:18Z rhempel $
 * $HeadURL: https://svn.clever-zoeger.de/svn/Magento/extensions/Cz_Cmsplus/tags/1.3.0/cmsPlus/app/code/local/Cz/Cmsplus/sql/cmsplus_setup/mysql4-upgrade-0.1.8-0.1.9.php $
 * $LastChangedBy: rhempel $
 * $LastChangedRevision: 181 $
 * $LastChangedDate: 2011-09-12 16:37:18 +0200 (Mon, 12 Sep 2011) $
 */
?>
<?php

$installer = $this;

$installer->startSetup();

$installer->run("

ALTER TABLE `{$this->getTable('cz_cms_content_element')}` ADD COLUMN `cz_cms_type_idcz_cms_content_block_type` INT(11) NULL  AFTER `cz_cms_content_block_idcz_cms_content_block` , 
  ADD CONSTRAINT `fk_cz_cms_content_element_cz_cms_type1`
  FOREIGN KEY (`cz_cms_type_idcz_cms_content_block_type` )
  REFERENCES `{$this->getTable('cz_cms_type')}` (`idcz_cms_content_block_type` )
  ON DELETE NO ACTION
  ON UPDATE NO ACTION
, ADD INDEX `fk_cz_cms_content_element_cz_cms_type1` (`cz_cms_type_idcz_cms_content_block_type` ASC) ;

CREATE  TABLE IF NOT EXISTS `{$this->getTable('cz_cms_content_element_has_catalog_category_entity')}` (
  `idczcmscontentelementhascatalogcategoryentity` INT(11) NOT NULL AUTO_INCREMENT ,
  `cz_cms_content_element_idcz_cms_content_element` INT(11) NOT NULL ,
  `catalog_category_entity_entity_id` INT(10) UNSIGNED NOT NULL ,
  PRIMARY KEY (`idczcmscontentelementhascatalogcategoryentity`) ,
  INDEX `fk_cz_cms_content_element_has_catalog_category_entity_catalog1` (`catalog_category_entity_entity_id` ASC) ,
  INDEX `fk_cz_cms_content_element_has_catalog_category_entity_cz_cms_1` (`cz_cms_content_element_idcz_cms_content_element` ASC) ,
  CONSTRAINT `fk_cz_cms_content_element_has_catalog_category_entity_cz_cms_1`
    FOREIGN KEY (`cz_cms_content_element_idcz_cms_content_element` )
    REFERENCES `{$this->getTable('cz_cms_content_element')}` (`idcz_cms_content_element` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cz_cms_content_element_has_catalog_category_entity_catalog1`
    FOREIGN KEY (`catalog_category_entity_entity_id` )
    REFERENCES `catalog_category_entity` (`entity_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;

INSERT INTO `{$this->getTable('cz_cms_type')}` (`idcz_cms_content_block_type`,`table`,`name`) VALUES (6,'element','Content Element');
INSERT INTO `{$this->getTable('cz_cms_type')}` (`idcz_cms_content_block_type`,`table`,`name`) VALUES (7,'element','Category List');

UPDATE `{$this->getTable('cz_cms_content_element')}` SET `cz_cms_type_idcz_cms_content_block_type` = 6;
");

$installer->endSetup();
