<?php
/**
 * clever + zöger gmbh
 * http://www.clever-zoeger.de
 * @author Ronny Hempel <rhempel@clever-zoeger.de>
 * @version $Id: mysql4-upgrade-0.1.9-0.1.10.php 181 2011-09-12 14:37:18Z rhempel $
 * $HeadURL: https://svn.clever-zoeger.de/svn/Magento/extensions/Cz_Cmsplus/tags/1.3.0/cmsPlus/app/code/local/Cz/Cmsplus/sql/cmsplus_setup/mysql4-upgrade-0.1.9-0.1.10.php $
 * $LastChangedBy: rhempel $
 * $LastChangedRevision: 181 $
 * $LastChangedDate: 2011-09-12 16:37:18 +0200 (Mon, 12 Sep 2011) $
 */
?>
<?php

$installer = $this;

$installer->startSetup();

$installer->run("

ALTER TABLE `{$this->getTable('cz_cms_content_page_image')}` 
ADD COLUMN `linktype` INT(11) NULL DEFAULT NULL  AFTER `cacheable`,
ADD COLUMN `extlink` VARCHAR(255) NULL DEFAULT NULL  AFTER `linktype`,
ADD COLUMN `catlink` INT(11) NULL DEFAULT NULL  AFTER `extlink`,
ADD COLUMN `prodlink` INT(11) NULL DEFAULT NULL  AFTER `catlink` ;

");

$installer->endSetup();
