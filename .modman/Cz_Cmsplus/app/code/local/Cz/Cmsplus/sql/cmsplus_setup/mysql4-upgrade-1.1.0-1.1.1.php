<?php
/**
 * clever + zöger gmbh
 * http://www.clever-zoeger.de
 * @author Ronny Hempel <rhempel@clever-zoeger.de>
 * @version $Id: mysql4-upgrade-1.1.0-1.1.1.php 197 2011-09-16 07:47:49Z rhempel $
 * $HeadURL: https://svn.clever-zoeger.de/svn/Magento/extensions/Cz_Cmsplus/tags/1.3.0/cmsPlus/app/code/local/Cz/Cmsplus/sql/cmsplus_setup/mysql4-upgrade-1.1.0-1.1.1.php $
 * $LastChangedBy: rhempel $
 * $LastChangedRevision: 197 $
 * $LastChangedDate: 2011-09-16 09:47:49 +0200 (Fri, 16 Sep 2011) $
 */
?>
<?php

$installer = $this;

$installer->startSetup();

$installer->run("

INSERT INTO `{$this->getTable('cz_cms_type')}` (`idcz_cms_content_block_type`,`table`,`name`) VALUES (8,'element','Sitemap');

");

$installer->endSetup();
