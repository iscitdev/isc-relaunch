<?php
/**
 * clever + zöger gmbh
 * http://www.clever-zoeger.de
 * @author Ronny Hempel <rhempel@clever-zoeger.de>
 * @version $Id: mysql4-upgrade-1.1.3-1.2.0.php 266 2011-11-21 14:14:51Z rhempel $
 * $HeadURL: https://svn.clever-zoeger.de/svn/Magento/extensions/Cz_Cmsplus/tags/1.3.0/cmsPlus/app/code/local/Cz/Cmsplus/sql/cmsplus_setup/mysql4-upgrade-1.1.3-1.2.0.php $
 * $LastChangedBy: rhempel $
 * $LastChangedRevision: 266 $
 * $LastChangedDate: 2011-11-21 15:14:51 +0100 (Mon, 21 Nov 2011) $
 */
?>
<?php

$installer = $this;

$installer->startSetup();



$role = Mage::getModel('admin/role');
$role->setParentId(0);
$role->setTreeLevel(1);
$role->setRoleType('G');
$role->setUserId(0);
$role->setRoleName('cmsPlus Editor');
$role->save();

$rule = Mage::getModel('admin/rules');
$rule->setRoleId($role->getId());
$rights = array('admin/cz','admin/cz/cmsplus');
$rule->setResources($rights);
$rule->saveRel();


$installer->run("                                                                                                                                                                                    

INSERT INTO {$this->getTable('core_email_template')} (`template_code`, `template_text`, `template_type`, `template_subject`, `template_sender_name`, `template_sender_email`, `added_at`, `modified_at`) VALUES
('cmsPlus Form (Plain)', 'MenuId: {{var menuid}}\r\nPageId: {{var pageid}}\r\nElementid: {{var elementid}}\r\nUser: {{var user.username}}\r\nBackendurl: {{var url}}', 1, 'cmsPlus Editor', NULL, NULL, NOW(), NOW());


ALTER TABLE `{$this->getTable('cz_cms_content_page')}` ADD `access` VARCHAR( 255 ) NULL;

ALTER TABLE `{$this->getTable('cz_cms_content_element')}` ADD `editor_content` LONGTEXT NULL AFTER `content`;

");



$installer->endSetup();
