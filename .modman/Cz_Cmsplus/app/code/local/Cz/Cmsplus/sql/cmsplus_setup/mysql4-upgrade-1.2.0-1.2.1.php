<?php
/**
 * clever + zöger gmbh
 * http://www.clever-zoeger.de
 * @author Ronny Hempel <rhempel@clever-zoeger.de>
 * @version $Id: mysql4-upgrade-1.1.3-1.2.0.php 266 2011-11-21 14:14:51Z rhempel $
 * $HeadURL: https://svn.clever-zoeger.de/Magento/extensions/Cz_Cmsplus/trunk/cmsPlus/app/code/local/Cz/Cmsplus/sql/cmsplus_setup/mysql4-upgrade-1.1.3-1.2.0.php $
 * $LastChangedBy: rhempel $
 * $LastChangedRevision: 266 $
 * $LastChangedDate: 2011-11-21 15:14:51 +0100 (Mon, 21 Nov 2011) $
 */
?>
<?php

$installer = $this;

$installer->startSetup();





$installer->run("                                                                                                                                                                                    

ALTER TABLE `{$this->getTable('cz_cms_content_page')}` CHANGE `access` `access` TEXT NULL DEFAULT NULL;

ALTER TABLE `{$this->getTable('cz_cms_content_block')}` ADD `access` TEXT NULL;
");



$installer->endSetup();
