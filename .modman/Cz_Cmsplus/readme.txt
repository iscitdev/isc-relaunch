** cmsPlus Magento Module Readme **

0. Requirements:
	*	Magento Version 1.4.x, 1.5.x, 1.6.x, 1.7.x
	*	php extension imagick

1. Installation:
	*	copy all Files to Magento directory

2. Configuration:
	*	Log into Magento Backend
	*	goto System -> Configuration -> General -> Clever+Zöger ->cmsPlus
	*	please configure urlkey, this is required to run
	*	now you can create menu and pages at the top menu clever+zöger -> cmsPlus

3. Upgrade to cmsPlus 1.3.0
	*	please reconfigure Access Rights