/**
 * clever + zöger gmbh
 * http://www.clever-zoeger.de
 * @author Ronny Hempel <rhempel@clever-zoeger.de>
 * @version $Id: czcmsplus.js 381 2012-04-24 09:04:02Z rhempel $
 * $HeadURL: https://svn.clever-zoeger.de/svn/Magento/extensions/Cz_Cmsplus/tags/1.3.0/cmsPlus/skin/adminhtml/default/default/js/czcmsplus.js $
 * $LastChangedBy: rhempel $
 * $LastChangedRevision: 381 $
 * $LastChangedDate: 2012-04-24 11:04:02 +0200 (Tue, 24 Apr 2012) $
 */

jQuery.noConflict();

var cmsPlusClass = function() {
	this.doSave = function(params) {
		jQuery('#cmsplus_info_tabs_config').click();
		cmsplusForm.submit(params);
		return false;
	}
	this.doDelete = function(params) {
		if (!confirm(deleteMsg)) {
			return false;
		}
		window.location.href = params;
		return false;
	}
	this.doPopup = function(params) {
		fenster = window.open(params,'Popup','resizeable=yes,scrollbars=yes');
		fenster.focus();
		return false;
	}
	this.doAction = function(params) {
		window.location.href = params;
		return false;
	}
	this.addMenu = function(params) {
		window.location.href = params;
		return false;
	}
	this.addPage = function(params) {
		window.location.href = params;
		return false;
	}
};

function switchStore(event, switcher) {
	if (event) {
		var obj = event.target;
		var newStoreId = obj.value * 1;
		var storeParam = newStoreId ? 'store/'+newStoreId + '/' : '';
		if (obj.switchParams) {
			storeParam += obj.switchParams;
		}
		if (switcher.useConfirm) {
			if (!confirm(storechangeconfirmtext)){
				obj.value = lastStoreId;
				return false;
			}
		}
		var url = storechangeurl+storeParam;
		cmsplus.doAction(url);
	}
}
function switchLinkType() {
	jQuery('#category_info_tabs_config_content #extlink').removeClass('required-entry').parent().parent().addClass('hidden').find('span.required').remove();
	jQuery('#category_info_tabs_config_content #catlink').removeClass('required-entry').parent().parent().addClass('hidden').find('span.required').remove();
	jQuery('#category_info_tabs_config_content #prodlink').removeClass('required-entry').parent().parent().addClass('hidden').find('span.required').remove();
	jQuery('#category_info_tabs_config_content #cmslink').removeClass('required-entry').parent().parent().addClass('hidden').find('span.required').remove();

	if (jQuery('#category_info_tabs_config_content #linktype').val() == 1) {
		jQuery('#category_info_tabs_config_content #extlink').addClass('required-entry').parent().parent().removeClass('hidden').find('td.label label').append('<span class="required">&nbsp;*</span>');
	} else if (jQuery('#category_info_tabs_config_content #linktype').val() == 2) {
		jQuery('#category_info_tabs_config_content #catlink').addClass('required-entry').parent().parent().removeClass('hidden').find('td.label label').append('<span class="required">&nbsp;*</span>');
	} else if (jQuery('#category_info_tabs_config_content #linktype').val() == 3) {
		jQuery('#category_info_tabs_config_content #prodlink').addClass('required-entry').parent().parent().removeClass('hidden').find('td.label label').append('<span class="required">&nbsp;*</span>');
	} else if (jQuery('#category_info_tabs_config_content #linktype').val() == 4) {
		jQuery('#category_info_tabs_config_content #cmslink').addClass('required-entry').parent().parent().removeClass('hidden').find('td.label label').append('<span class="required">&nbsp;*</span>');
	}
}
function sortingStop(event,ui) {
	var obj = jQuery(ui.item);
	var pageId = obj.attr('pageid');
	var prevObj = obj.prev();
	var prevpageId = prevObj.attr('pageid');
	if (typeof prevpageId == "undefined") {
		var nextObj = obj.next();
		var nextpageId = nextObj.attr('pageid');
		var params = 'pageid/'+pageId+'/nextid/'+nextpageId;
		//after
	} else {
		//before
		var params = 'pageid/'+pageId+'/previd/'+prevpageId;
	}
	jQuery('#loading-mask').show();
	cmsplus.doAction(sorturl+params);
}

function removePlaceholder(event,ui) {
	var objImg = jQuery(ui.item).find('.x-tree-node-indent').first();
	objImg.find('img').remove();
}

function setPlaceholder(event,ui) {
	var obj = jQuery(ui.item);
	var objImg = obj.find('.x-tree-node-indent').first();
	
	removePlaceholder(event,ui);
	
	var obj = jQuery('.placeholder');
	var pageId = obj.attr('pageid');
	var prevObj = obj.prev();
	var prevpageId = prevObj.attr('pageid');
	if (typeof prevpageId == "undefined") {
		var nextObj = obj.next();
		var level = nextObj.attr('level');
	} else {
		var level = prevObj.attr('level');
	}
	
	for (var i=0;i<=level;i++) {
		objImg.append('<img class="x-tree-ec-icon" src="/js/spacer.gif">');
	}
}

function initBackend() {
	if (jQuery('#cmsplus_tab_content').length) {
		if ((typeof varienStoreSwitcher)!='undefined') {
			varienStoreSwitcher.storeSelectorClickCallback = switchStore;
		}
		if (jQuery('#category_info_tabs_config_content #linktype').length) {
			switchLinkType();
			jQuery('#category_info_tabs_config_content #linktype').change(function() {
				switchLinkType();
			});
		}
	}
	//connectWith: ".connectedSortable",
	//tolerance:	"pointer",
	/*distance: 0,*/
	/*handle:	"div",*/
	//helper: "clone",
	jQuery('.cmsplusmenu li ul.issortable').sortable({
			forcePlaceholderSize: true,
			placeholder: "placeholder",
			forceHelperSize: true,
			
			items:	"li",
			axis:	"y",
			sort:	function(event,ui) {setPlaceholder(event,ui);},
			stop:	function(event,ui) {sortingStop(event,ui);},
			start:	function(event,ui) {removePlaceholder(event,ui);}
			
		}).disableSelection();
}


var cmsplus = new cmsPlusClass();

jQuery(document).ready(function(){
	
	initBackend();
});