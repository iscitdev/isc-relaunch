<?php

class Emzee_Customer_Block_Account_Navigation extends Mage_Customer_Block_Account_Navigation
{

    /**
     * Removes the link from the account navigation.
     *
     * @param string $name Name provided in the layout XML file
     * @return Mage_Customer_Block_Account_Navigation
     */
    public function removeLinkByName ( $name )
    {
        if (isset( $this->_links[$name] )) {
            unset( $this->_links[$name] );
        } else {
            Mage::log( "Customer account navigation link '{$name}' does not exist.", Zend_Log::NOTICE );
        }
        return $this;
    }

    public function addLink ( $name, $path, $label, $urlParams = array(), $after = null )
    {
        $link = new Varien_Object( array(
            'name' => $name,
            'path' => $path,
            'label' => $label,
            'url' => $this->getUrl( $path, $urlParams ),
        ) );
        if ($after && ($key = array_search( $after, array_keys( $this->_links )) ) !== false ) {
            array_splice( $this->_links, $key + 1, 0, array( $link ) );
        } else {
            $this->_links[$name] = $link;
        }
        return $this;
    }
}