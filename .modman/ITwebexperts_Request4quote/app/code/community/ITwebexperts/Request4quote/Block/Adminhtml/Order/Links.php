<?php

class ITwebexperts_Request4quote_Block_Adminhtml_Order_Links extends Mage_Core_Block_Template {

    public function getOrder()
    {
        return Mage::registry('current_order');
    }

    public function getQuote(){
        return Mage::getSingleton('request4quote/adminhtml_session_quote')->getQuote();
    }

    public function getOrderCollection(){
        $quote = $this->getQuote();
        if ($quote && $quote->getId()) {
            $collection = Mage::getResourceModel("sales/order_collection");
            $collection->addFieldToFilter('quote_request_id', array('eq' => $quote->getId()));
            return $collection;
        }
        return null;
    }
}