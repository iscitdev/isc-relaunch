<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Adminhtml
 * @copyright   Copyright (c) 2012 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Adminhtml sales order create items grid block
 *
 * @category   Mage
 * @package    Mage_Adminhtml
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class ITwebexperts_Request4quote_Block_Adminhtml_Quote_Create_Items_Grid extends ITwebexperts_Request4quote_Block_Adminhtml_Quote_Create_Abstract
{

    /**
     * Flag to check can items be move to customer storage
     *
     * @var bool
     */
    protected $_moveToCustomerStorage = true;
    protected $_exportTypes;

    public function __construct ()
    {
        parent::__construct();
        $this->setId( 'sales_order_create_search_grid' );
        $this->addExportType( '*/*/exportCsv', Mage::helper( 'sales' )->__( 'CSV' ) );
        $this->addExportType( '*/*/exportExcel', Mage::helper( 'sales' )->__( 'Excel XML' ) );
    }

    /**
     * Add new export type to grid
     *
     * @param   string $url
     * @param   string $label
     * @return  Mage_Adminhtml_Block_Widget_Grid
     */
    public function addExportType ( $url, $label )
    {
        $this->_exportTypes[] = new Varien_Object(
            array(
                'url' => $this->getUrl( $url, array( '_current' => true ) ),
                'label' => $label
            )
        );
        return $this;
    }

    /**
     * Retrieve grid export types
     *
     * @return array|false
     */
    public function getExportTypes ()
    {
        return empty( $this->_exportTypes ) ? false : $this->_exportTypes;
    }

    public function getExportButtonHtml ()
    {
        return Mage::app()->getLayout()->createBlock( 'adminhtml/widget_button' )
            ->setData( array(
                'label' => Mage::helper( 'adminhtml' )->__( 'Export' ),
                'onclick' => $this->getJsObjectName() . '.doExport()',
                'class' => 'task'
            ) )->toHtml();
    }

    public function getJsObjectName ()
    {
        return $this->getId() . 'JsObject';
    }

    public function getItems ()
    {
        $items = $this->getParentBlock()->getItems();
        $oldSuperMode = $this->getQuote()->getIsSuperMode();
        $this->getQuote()->setIsSuperMode( false );
        foreach ($items as $item) {
            // To dispatch inventory event sales_quote_item_qty_set_after, set item qty
            $item->setQty( $item->getQty() );
            $stockItem = $item->getProduct()->getStockItem();
            if ($stockItem instanceof Mage_CatalogInventory_Model_Stock_Item) {
                if ($item->getProduct()->getStatus() == Mage_Catalog_Model_Product_Status::STATUS_DISABLED) {
                    $item->setMessage( Mage::helper( 'adminhtml' )->__( 'This product is currently disabled.' ) );
                    $item->setHasError( true );
                }
            }
        }
        $this->getQuote()->setIsSuperMode( $oldSuperMode );
        return $items;
    }

    public function getSession ()
    {
        return $this->getParentBlock()->getSession();
    }

    public function getItemEditablePrice ( $item )
    {
        return $item->getCalculationPrice() * 1;
    }

    public function getOriginalEditablePrice ( $item )
    {
        if ($item->hasOriginalCustomPrice()) {
            $result = $item->getOriginalCustomPrice() * 1;
        } elseif ($item->hasCustomPrice()) {
            $result = $item->getCustomPrice() * 1;
        } else {
            if (Mage::helper( 'tax' )->priceIncludesTax( $this->getStore() )) {
                $result = $item->getPriceInclTax() * 1;
            } else {
                $result = $item->getOriginalPrice() * 1;
            }
        }
        return $result;
    }

    public function getItemOrigPrice ( $item )
    {
        //        return $this->convertPrice($item->getProduct()->getPrice());
        return $this->convertPrice( $item->getPrice() );
    }

    public function isGiftMessagesAvailable ( $item = null )
    {
        if (is_null( $item )) {
            return $this->helper( 'giftmessage/message' )->getIsMessagesAvailable(
                'items', $this->getQuote(), $this->getStore()
            );
        }

        return $this->helper( 'giftmessage/message' )->getIsMessagesAvailable(
            'item', $item, $this->getStore()
        );
    }

    public function isAllowedForGiftMessage ( $item )
    {
        return Mage::getSingleton( 'adminhtml/giftmessage_save' )->getIsAllowedQuoteItem( $item );
    }

    /**
     * Check if we need display grid totals include tax
     *
     * @return bool
     */
    public function displayTotalsIncludeTax ()
    {
        $res = Mage::getSingleton( 'tax/config' )->displayCartSubtotalInclTax( $this->getStore() )
            || Mage::getSingleton( 'tax/config' )->displayCartSubtotalBoth( $this->getStore() );
        return $res;
    }

    public function getSubtotal ()
    {
        $address = $this->getQuoteAddress();
        if ($this->displayTotalsIncludeTax()) {
            if ($address->getSubtotalInclTax()) {
                return $address->getSubtotalInclTax();
            }
            return $address->getSubtotal() + $address->getTaxAmount();
        } else {
            return $address->getSubtotal();
        }
        return false;
    }

    public function getSubtotalWithDiscount ()
    {
        $address = $this->getQuoteAddress();
        if ($this->displayTotalsIncludeTax()) {
            return $address->getSubtotal() + $address->getTaxAmount() + $this->getDiscountAmount();
        } else {
            return $address->getSubtotal() + $this->getDiscountAmount();
        }
    }

    public function getDiscountAmount ()
    {
        return $this->getQuote()->getShippingAddress()->getDiscountAmount();
    }

    /**
     * Retrive quote address
     *
     * @return Mage_Sales_Model_Quote_Address
     */
    public function getQuoteAddress ()
    {
        if ($this->getQuote()->isVirtual()) {
            return $this->getQuote()->getBillingAddress();
        } else {
            return $this->getQuote()->getShippingAddress();
        }
    }

    /**
     * Define if specified item has already applied custom price
     *
     * @param Mage_Sales_Model_Quote_Item $item
     * @return bool
     */
    public function usedCustomPriceForItem ( $item )
    {
        return $item->hasCustomPrice();
    }

    /**
     * Define if custom price can be applied for specified item
     *
     * @param Mage_Sales_Model_Quote_Item $item
     * @return bool
     */
    public function canApplyCustomPrice ( $item )
    {
        return !$item->isChildrenCalculated();
    }

    public function getQtyTitle ( $item )
    {
        $prices = $item->getProduct()->getTierPrice();
        if ($prices) {
            $info = array();
            foreach ($prices as $data) {
                $qty = $data['price_qty'] * 1;
                $price = $this->convertPrice( $data['price'] );
                $info[] = $this->helper( 'sales' )->__( 'Buy %s for price %s', $qty, $price );
            }
            return implode( ', ', $info );
        } else {
            return $this->helper( 'sales' )->__( 'Item ordered qty' );
        }
    }

    public function getTierHtml ( $item )
    {
        $html = '';
        $prices = $item->getProduct()->getTierPrice();
        if ($prices) {
            foreach ($prices as $data) {
                $qty = $data['price_qty'] * 1;
                $price = $this->convertPrice( $data['price'] );
                $info[] = $this->helper( 'sales' )->__( '%s for %s', $qty, $price );
            }
            $html = implode( '<br/>', $info );
        }
        return $html;
    }

    /**
     * Get Custom Options of item
     *
     * @param Mage_Sales_Model_Quote_Item $item
     * @return array
     */
    public function getCustomOptions ( Mage_Sales_Model_Quote_Item $item )
    {
        $optionStr = '';
        $this->_moveToCustomerStorage = true;
        if ($optionIds = $item->getOptionByCode( 'option_ids' )) {
            foreach (explode( ',', $optionIds->getValue() ) as $optionId) {
                if ($option = $item->getProduct()->getOptionById( $optionId )) {
                    $optionValue = $item->getOptionByCode( 'option_' . $option->getId() )->getValue();

                    $optionStr .= $option->getTitle() . ':';

                    $quoteItemOption = $item->getOptionByCode( 'option_' . $option->getId() );
                    $group = $option->groupFactory( $option->getType() )
                        ->setOption( $option )
                        ->setQuoteItemOption( $quoteItemOption );

                    $optionStr .= $group->getEditableOptionValue( $quoteItemOption->getValue() );
                    $optionStr .= "\n";
                }
            }
        }
        return $optionStr;
    }

    /**
     * Get flag for rights to move items to customer storage
     *
     * @return bool
     */
    public function getMoveToCustomerStorage ()
    {
        return $this->_moveToCustomerStorage;
    }

    public function displaySubtotalInclTax ( $item )
    {
        if ($item->getTaxBeforeDiscount()) {
            $tax = $item->getTaxBeforeDiscount();
        } else {
            $tax = $item->getTaxAmount() ? $item->getTaxAmount() : 0;
        }
        return $this->formatPrice( $item->getRowTotal() + $tax );
    }

    public function displayOriginalPriceInclTax ( $item )
    {
        $tax = 0;
        if ($item->getTaxPercent()) {
            $tax = $item->getPrice() * ( $item->getTaxPercent() / 100 );
        }
        return $this->convertPrice( $item->getPrice() + ( $tax / $item->getQty() ) );
    }

    public function displayRowTotalWithDiscountInclTax ( $item )
    {
        $tax = ( $item->getTaxAmount() ? $item->getTaxAmount() : 0 );
        return $this->formatPrice( $item->getRowTotal() - $item->getDiscountAmount() + $tax );
    }

    public function getInclExclTaxMessage ()
    {
        if (Mage::helper( 'tax' )->priceIncludesTax( $this->getStore() )) {
            return Mage::helper( 'request4quote' )->__( '* - Enter custom price including tax' );
        } else {
            return Mage::helper( 'request4quote' )->__( '* - Enter custom price excluding tax' );
        }
    }

    public function getStore ()
    {
        return $this->getQuote()->getStore();
    }

    /**
     * Return html button which calls configure window
     *
     * @param  $item
     * @return string
     */
    public function getConfigureButtonHtml ( $item )
    {
        $product = $item->getProduct();

        $options = array( 'label' => Mage::helper( 'request4quote' )->__( 'Configure' ) );
        if ($product->canConfigure()) {
            $options['onclick'] = sprintf( 'order.showQuoteItemConfiguration(%s)', $item->getId() );
        } else {
            $options['class'] = ' disabled';
            $options['title'] = Mage::helper( 'request4quote' )->__( 'This product does not have any configurable options' );
        }

        return $this->getLayout()->createBlock( 'adminhtml/widget_button' )
            ->setData( $options )
            ->toHtml();
    }

    /**
     * Get order item extra info block
     *
     * @param Mage_Sales_Model_Quote_Item $item
     * @return Mage_Core_Block_Abstract
     */
    public function getItemExtraInfo ( $item )
    {
        return $this->getLayout()
            ->getBlock( 'order_item_extra_info' )
            ->setItem( $item );
    }

    /**
     * Returns whether moving to wishlist is allowed for this item
     *
     * @param Mage_Sales_Model_Quote_Item $item
     * @return bool
     */
    public function isMoveToWishlistAllowed ( $item )
    {
        return $item->getProduct()->isVisibleInSiteVisibility();
    }


    /**
     * Retrieve collection of customer wishlists
     *
     * @return Mage_Wishlist_Model_Resource_Wishlist_Collection
     */
    public function getCustomerWishlists ()
    {
        return Mage::getModel( "wishlist/wishlist" )->getCollection()
            ->filterByCustomerId( $this->getCustomerId() );
    }

    /**
     * Retrieve a file container array by grid data as CSV
     *
     * Return array with keys type and value
     *
     * @return array
     */
    public function getCsvFile ()
    {
        //$this->_isExport = true;
        $this->_prepareGrid();

        $io = new Varien_Io_File();

        $path = Mage::getBaseDir( 'var' ) . DS . 'export' . DS;
        $name = md5( microtime() );
        $file = $path . DS . $name . '.csv';

        $io->setAllowCreateFolders( true );
        $io->open( array( 'path' => $path ) );
        $io->streamOpen( $file, 'w+' );
        $io->streamLock( true );
        $io->streamWriteCsv( $this->_getExportHeaders() );

        $this->_exportIterateCollection( '_exportCsvItem', array( $io ) );

        if ($this->getCountTotals()) {
            $io->streamWriteCsv( $this->_getExportTotals() );
        }

        $io->streamUnlock();
        $io->streamClose();

        return array(
            'type' => 'filename',
            'value' => $file,
            'rm' => true // can delete file after use
        );
    }

    protected function _prepareGrid ()
    {
        $this->_prepareColumns();
        $this->_prepareCollection();
        return $this;
    }

    protected $columns;
    protected function _prepareColumns(){
        $salesHelper = Mage::helper( "sales" );

        $this->columns = array(
            "sku" => $salesHelper->__('SKU'),
            "name" => $salesHelper->__( 'Product' ),
            "price_incl_tax" => $salesHelper->__( 'Price' ),
            'custom_price' => $salesHelper->__('Custom Price'),
            "qty" => $salesHelper->__( 'Qty' ),
            "row_total_excl_tax" => $salesHelper->__( 'Subtotal' ),
            "discount_amount" => $salesHelper->__( 'Discount' ),
            "row_total_incl_tax" => $salesHelper->__( 'Row Subtotal' ),
            "ext_position_remark" => $this->helper('request4quote')->__('Note'),
            "r4q_status" => $this->helper('request4quote')->__('Status')
        );
    }

    protected $_collection;
    protected function _prepareCollection(){
        $this->_collection = $this->getItems();
    }

    public function getCollection(){
        return $this->_collection;
    }

    /**
     * Retrieve Headers row array for Export
     *
     * @return array
     */
    protected function _getExportHeaders ()
    {
        return $this->columns;
    }

    protected function _getExportTotals(){
        $totals = array( Mage::helper("sales")->__('Subtotal'), null, null, null );
        $totals [] = $this->getSubtotal();
        $totals [] = $this->getDiscountAmount();
        $totals [] = $this->getSubtotal() + $this->getDiscountAmount();
        return $totals;
    }

    public function getCountTotals ()
    {
        return (bool)$this->getSubtotal();
    }

    /**
     * Iterate collection and call callback method per item
     * For callback method first argument always is item object
     *
     * @param string $callback
     * @param array $args additional arguments for callback method
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    public function _exportIterateCollection($callback, array $args)
    {
        $originalCollection = $this->getCollection();
        foreach ($originalCollection as $item) {
            call_user_func_array(array($this, $callback), array_merge(array($item), $args));
        }
    }

    /**
     * Write item data to csv export file
     *
     * @param Varien_Object $item
     * @param Varien_Io_File $adapter
     */
    protected function _exportCsvItem(Varien_Object $item, Varien_Io_File $adapter)
    {
        $row = array();
        foreach (array_keys($this->columns) as $column) {
            $row[] = $this->getValue($item ,$column);
        }
        $adapter->streamWriteCsv($row);
    }

    /**
     * Write item data to Excel 2003 XML export file
     *
     * @param Varien_Object $item
     * @param Varien_Io_File $adapter
     * @param Varien_Convert_Parser_Xml_Excel $parser
     */
    protected function _exportExcelItem(Varien_Object $item, Varien_Io_File $adapter, $parser = null)
    {
        if (is_null($parser)) {
            $parser = new Varien_Convert_Parser_Xml_Excel();
        }

        $row = array();
        foreach (array_keys($this->columns) as $column) {
            $row[] = $this->getValue($item ,$column);
        }
        $data = $parser->getRowXml($row);
        $adapter->streamWrite($data);
    }

    protected function getValue ( $item, $column )
    {
        if($column == "row_total_excl_tax"){
            return $this->helper('checkout')->getSubtotalInclTax($item) - $item->getDiscountAmount();
        }
        if ($column == "r4q_status") {
            return $this->getStatusLabel($item->getDataUsingMethod($column));
        }
        return $item->getDataUsingMethod($column);
    }

    protected function getStatusLabel($value){
        foreach ($this->getItemStatusCollection() as $itemStatus) {
            if ($itemStatus->getStatus() == $value) {
                return $itemStatus->getLabel();
            }
        }
        return $value;
    }

    protected function getItemStatusCollection(){
        return Mage::helper("request4quote")->getItemStatusCollection();
    }

    /**
     * Retrieve a file container array by grid data as MS Excel 2003 XML Document
     *
     * Return array with keys type and value
     *
     * @return string
     */
    public function getExcelFile($sheetName = '')
    {
        $this->_prepareGrid();

        $parser = new Varien_Convert_Parser_Xml_Excel();
        $io     = new Varien_Io_File();

        $path = Mage::getBaseDir('var') . DS . 'export' . DS;
        $name = md5(microtime());
        $file = $path . DS . $name . '.xml';

        $io->setAllowCreateFolders(true);
        $io->open(array('path' => $path));
        $io->streamOpen($file, 'w+');
        $io->streamLock(true);
        $io->streamWrite($parser->getHeaderXml($sheetName));
        $io->streamWrite($parser->getRowXml($this->_getExportHeaders()));

        $this->_exportIterateCollection('_exportExcelItem', array($io, $parser));

        if ($this->getCountTotals()) {
            $io->streamWrite($parser->getRowXml($this->_getExportTotals()));
        }

        $io->streamWrite($parser->getFooterXml());
        $io->streamUnlock();
        $io->streamClose();

        return array(
            'type'  => 'filename',
            'value' => $file,
            'rm'    => true // can delete file after use
        );
    }
}
