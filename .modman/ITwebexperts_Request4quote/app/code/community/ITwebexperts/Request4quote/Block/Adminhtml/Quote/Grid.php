<?php
class ITwebexperts_Request4quote_Block_Adminhtml_Quote_Grid extends Mage_Adminhtml_Block_Widget_Grid {
	
	public function __construct()
    {
        parent::__construct();
		$this->setId('request4quote_quote_grid');
			$this->setUseAjax(false);
			$this->setDefaultSort('created_at');
			$this->setDefaultDir('DESC');
			$this->setSaveParametersInSession(true);
	}
	
	
	
	protected function _getCollectionClass()
    {
        return 'request4quote/quote_collection';
    }
	
	
	protected function _prepareCollection()
    {
        $collection = Mage::getResourceModel($this->_getCollectionClass());
	    $collection->addFieldToFilter('r4q_status', array('neq' => ITwebexperts_Request4quote_Model_Quote::STATUS_NEW));
		$collection->addShippingAddress();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }
	
	
	
	protected function _prepareColumns()
    {

        $this->addColumn('entity_id', array(
            'header'=> Mage::helper('request4quote')->__('Quote #'),
            'width' => '80px',
            'type'  => 'text',
            'index' => 'entity_id',
        ));
		
		
		$this->addColumn('created_at', array(
            'header' => Mage::helper('request4quote')->__('Created On'),
            'index' => 'created_at',
            'type' => 'datetime',
            'width' => '100px',
        ));


        $this->addColumn('customer_firstname', array(
            'header' => Mage::helper('request4quote')->__('Customer Firstname'),
            'index' => 'customer_firstname',
        ));

        $this->addColumn('customer_lastname', array(
            'header' => Mage::helper('request4quote')->__('Customer Lastname'),
            'index' => 'customer_lastname',
        ));
		
		$this->addColumn('customer_email', array(
            'header' => Mage::helper('request4quote')->__('Customer Email'),
            'index' => 'customer_email',
        ));
		
		$this->addColumn('r4q_phone', array(
            'header' => Mage::helper('request4quote')->__('Customer Phone Number'),
            'index' => 'r4q_phone',
        ));

		$this->addColumn('shipping_country_id', array(
            'header' => Mage::helper('request4quote')->__('Shipping Country'),
            'index' => 'shipping_country_id',
        ));
		
		$this->addColumn('shipping_region', array(
            'header' => Mage::helper('request4quote')->__('State/Province'),
            'index' => 'shipping_region',
        ));
		
		$this->addColumn('shipping_city', array(
            'header' => Mage::helper('request4quote')->__('City'),
            'index' => 'shipping_city',
        ));
		
		$this->addColumn('shipping_postcode', array(
            'header' => Mage::helper('request4quote')->__('Zip'),
            'index' => 'shipping_postcode',
        ));
		
		$this->addColumn('shipping_street', array(
            'header' => Mage::helper('request4quote')->__('Street'),
            'index' => 'shipping_street',
        ));

        $this->addColumn('r4q_status', array(
            'header' => Mage::helper('request4quote')->__('Status'),
            'index' => 'r4q_status',
            'width' => '70px',
        ));

        

        return parent::_prepareColumns();
    }
	
	
	public function getRowUrl($row)
    {
        return $this->getUrl('*/adminhtml_quote_edit/start', array('quote_id' => $row->getId()));
    }
	
	
}