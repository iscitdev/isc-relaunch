<?php
class ITwebexperts_Request4quote_Block_Adminhtml_Quote_ItemStatus extends Mage_Adminhtml_Block_Widget_Grid_Container {
	
	public function __construct()
    {
		$this->_blockGroup = 'request4quote';
        $this->_controller = 'adminhtml_quote_itemStatus';
        $this->_headerText = Mage::helper('request4quote')->__('Quote Item Statuses');
        $this->_addButtonLabel = Mage::helper('sales')->__('Create New Status');
        parent::__construct();
    }

    public function getCreateUrl()
    {
        return $this->getUrl('*/adminhtml_quote_item_status/new');
    }
	
}