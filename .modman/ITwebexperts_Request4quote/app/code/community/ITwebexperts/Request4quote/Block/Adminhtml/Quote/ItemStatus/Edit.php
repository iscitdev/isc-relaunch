<?php
class ITwebexperts_Request4quote_Block_Adminhtml_Quote_ItemStatus_Edit extends ITwebexperts_Request4quote_Block_Adminhtml_Quote_ItemStatus_New {
	
	public function __construct()
    {
        parent::__construct();
        $this->_mode = 'edit';
    }
	
	public function getHeaderText()
    {
        return Mage::helper('sales')->__('Edit Order Item Status');
    }
}