<?php
class ITwebexperts_Request4quote_Block_Links extends Mage_Core_Block_Template
{

    public function getOrder()
    {
        return $this->getParentBlock()->getOrder();
    }

    public function getQuoteRequest(){
        return Mage::getModel("request4quote/quote")->load($this->getOrder()->getQuoteRequestId());
    }

    public function getQuote(){
        return $this->getParentBlock()->getQuote();
    }

    public function getOrderCollection(){
        $quote = $this->getQuote();
        if ($quote && $quote->getId()) {
            $collection = Mage::getResourceModel("sales/order_collection");
            $collection->addFieldToFilter('quote_request_id', array('eq' => $quote->getId()));
            return $collection;
        }
        return null;
    }
	
	public function addQuoteLink()
    {
        $parentBlock = $this->getParentBlock();
        if ($parentBlock && Mage::helper('core')->isModuleOutputEnabled('ITwebexperts_Request4quote')) {
            $count = $this->getSummaryQty() ? $this->getSummaryQty()
                : $this->helper('request4quote/cart')->getSummaryCount();
            if ($count == 1) {
                $text = $this->__('My Quote (%s item)', $count);
            } elseif ($count > 0) {
                $text = $this->__('My Quote (%s items)', $count);
            } else {
                $text = $this->__('My Quote');
            }

            $parentBlock->removeLinkByUrl($this->getUrl('requestquote/quote'));
            $parentBlock->addLink($text, 'requestquote/quote', $text, true, array(), 50, null, 'class="top-link-quote"');
        }
        return $this;
    }
	
}