<?php
class ITwebexperts_Request4quote_Helper_Data extends Mage_Core_Helper_Abstract
{

    const PATH_ENABLE_SHIPPING_QUOTES = 'request4quote/general/enable_shipping_quotes';
    const PATH_ENABLE_TAX_ESTIMATES = 'request4quote/general/enable_tax_estimates';

    const PATH_SHOW_BILLING_ADDRESS_FOR_QUOTE_CUSTOMER = 'request4quote/billing_address/show_billing_address_for_quote_customer';
    const PATH_REQUIRE_BILLING_ADDRESS_FOR_QUOTE_CUSTOMER = 'request4quote/billing_address/require_billing_address_for_quote_customer';
    const PATH_SHOW_BILLING_ADDRESS_FOR_QUOTE_ADMIN = 'request4quote/billing_address/show_billing_address_for_quote_admin';
    const PATH_REQUIRE_BILLING_ADDRESS_FOR_QUOTE_ADMIN = 'request4quote/billing_address/require_billing_address_for_quote_admin';

    const PATH_SHOW_SHIPPING_ADDRESS_FOR_QUOTE_CUSTOMER = 'request4quote/shipping_address/show_shipping_address_for_quote_customer';
    const PATH_REQUIRE_SHIPPING_ADDRESS_FOR_QUOTE_CUSTOMER = 'request4quote/shipping_address/require_shipping_address_for_quote_customer';
    const PATH_SHOW_SHIPPING_ADDRESS_FOR_QUOTE_ADMIN = 'request4quote/shipping_address/show_shipping_address_for_quote_admin';
    const PATH_REQUIRE_SHIPPING_ADDRESS_FOR_QUOTE_ADMIN = 'request4quote/shipping_address/require_shipping_address_for_quote_admin';
	
	protected $_isRentalInstalled;
    protected $_isOpenendedInstalled;
    protected $_isPPRWarehouseInstalled;

    protected $_itemStatusCollection;
	
    public function isRentalInstalled()
	{
        if (is_null($this->_isRentalInstalled)) {
			$this->_isRentalInstalled = Mage::helper('core')->isModuleEnabled('ITwebexperts_Payperrentals');
		}
		return $this->_isRentalInstalled;
	}
    public function isOpenendedInstalled()
    {
        if (is_null($this->_isOpenendedInstalled)) {
            $modules = (array)Mage::getConfig()->getNode('modules')->children();
            if (isset($modules['ITwebexperts_Openendedinvoices'])) {
                $this->_isOpenendedInstalled = true;
            } else {
                $this->_isOpenendedInstalled = false;
            }
        }
        return $this->_isOpenendedInstalled;
    }
    public function isPPRWarehouseInstalled()
    {
        return $this->hasWarehouse();
    }
	
	public function getQuoteCode($quote) {
		return substr(sha1($quote->getId()), 0, 6);
	}
    public function getDropdownQuotes(){
        $quoteCollection = Mage::getModel('request4quote/quote')->getCollection()
        ->addFieldToFilter('customer_email', Mage::getSingleton('customer/session')->getCustomer()->getEmail())
        ->setOrder('created_at');
        $dd = '<select name="r4quote" style="float:right;margin-left:3px;"><option value="new">--Current Quote--</option>';
        foreach($quoteCollection as $iquote){
            $quoteCode = substr(sha1($iquote->getId()), 0, 6);
            $quoteDate = $iquote->getCreatedAt();
            $dd .= '<option value="'.$iquote->getId().'">'.$quoteCode.' - '.$quoteDate.'</option>';
        }
        $dd .= '</select>';
        return $dd;
    }

    /**
     * Returns true if shipping quotes are enabled.
     * @return bool
     */
    public function isShippingQuotesEnabled()
    {
        return (bool)Mage::getStoreConfig(self::PATH_ENABLE_SHIPPING_QUOTES);
    }

    /**
     * Returns true if tax estimates enabled.
     * @return bool
     */
    public function isTaxEstimatesEnabled()
    {
        return (bool)Mage::getStoreConfig(self::PATH_ENABLE_TAX_ESTIMATES);
    }

    /**
     * Returns true if billing address can be show to the customer.
     * @return bool
     */
    public function canShowBillingAddressCustomer()
    {
        return (bool)Mage::getStoreConfig(self::PATH_SHOW_BILLING_ADDRESS_FOR_QUOTE_CUSTOMER);
    }

    /**
     * Returns true if billing address enabled for quote (customer side).
     * @return bool
     */
    public function isBillingAddressRequiredCustomer()
    {
        return (bool)Mage::getStoreConfig(self::PATH_REQUIRE_BILLING_ADDRESS_FOR_QUOTE_CUSTOMER);
    }

    /**
     * Returns true if billing address can be show to the admin.
     * @return bool
     */
    public function canShowBillingAddressAdmin()
    {
        return (bool)Mage::getStoreConfig(self::PATH_SHOW_BILLING_ADDRESS_FOR_QUOTE_ADMIN);
    }

    /**
     * Returns true if billing address enabled for quote (admin side).
     * @return bool
     */
    public function isBillingAddressRequiredAdmin()
    {
        return (bool)Mage::getStoreConfig(self::PATH_REQUIRE_BILLING_ADDRESS_FOR_QUOTE_ADMIN);
    }

    /**
     * Returns true if shipping address can be show to the customer.
     * @return bool
     */
    public function canShowShippingAddressCustomer()
    {
        return (bool)Mage::getStoreConfig(self::PATH_SHOW_SHIPPING_ADDRESS_FOR_QUOTE_CUSTOMER);
    }

    /**
     * Returns true if shipping address enabled for quote (customer side).
     * @return bool
     */
    public function isShippingAddressRequiredCustomer()
    {
        return (bool)Mage::getStoreConfig(self::PATH_REQUIRE_SHIPPING_ADDRESS_FOR_QUOTE_CUSTOMER);
    }

    /**
     * Returns true if shipping address can be show to the admin.
     * @return bool
     */
    public function canShowShippingAddressAdmin()
    {
        return (bool)Mage::getStoreConfig(self::PATH_SHOW_SHIPPING_ADDRESS_FOR_QUOTE_ADMIN);
    }

    /**
     * Returns true if shipping address enabled for quote (admin side).
     * @return bool
     */
    public function isShippingAddressRequiredAdmin()
    {
        return (bool)Mage::getStoreConfig(self::PATH_REQUIRE_SHIPPING_ADDRESS_FOR_QUOTE_ADMIN);
    }

    /**
     * Checks if 'ITwebexperts_PPRWarehouse' module exist and enabled in global config.
     *
     * @return boolean
     */
    public function hasWarehouse()
    {
        return Mage::helper('core')->isModuleEnabled('ITwebexperts_PPRWarehouse');
    }

    /**
     * Removes js validation
     *
     * @param string $html
     * @return string
     */
    public function removeRequired($html)
    {
        //$html = str_replace('<span class="required">*</span>', '', $html);
        //$html = str_replace('required-entry', '', $html);
        return $html;//str_replace('required', '', $html);
    }

//    New

    public function getRequest4quoteUrl($product, $additional = array())
    {
        return Mage::helper('request4quote/cart')->getAddUrl($product, $additional);
    }


    public function getItemStatusCollection()
    {
        if (empty($this->_itemStatusCollection)) {
            $this->_itemStatusCollection = Mage::getModel('request4quote/quote_item_status')->getCollection();
        }
        return $this->_itemStatusCollection;
    }

//    End

}

