<?php
class ITwebexperts_Request4quote_Helper_Email extends Mage_Checkout_Helper_Cart {
	
	public function getItemsHandler()
	{
		return Mage::helper('request4quote')->isRentalInstalled() ? 'requestquote_email_items_rental' : 'requestquote_email_items';
	}

    protected function getEmailRecepients ($quote)
    {
        $recepients = array();
        if (Mage::getStoreConfig( "request4quote/quote_settings/send_email_to_customer" )) {
            $recepients [] = $quote->getCustomerEmail();
        }
        if (Mage::getStoreConfig( "request4quote/quote_settings/send_email_to" )) {
            $recepients = array_merge($recepients,explode(",",str_replace(" ","",Mage::getStoreConfig( "request4quote/quote_settings/send_email_to" ))));
        }

        if (Mage::getStoreConfig( "request4quote/quote_settings/send_email_to_cc" )) {
            $recepients = array_merge($recepients,explode(",",str_replace(" ","",Mage::getStoreConfig( "request4quote/quote_settings/send_email_to_cc" ))));
        }

        return $recepients;
    }

    public function sendRequestNotification($quoteId)
    {
        if (!$quoteId) {
            return;
        }
		$quote = Mage::getModel('request4quote/quote')->loadByIdWithoutStore($quoteId);
		$shippingAddress = $quote->getShippingAddress();
		
		$className = Mage::getConfig()->getBlockClassName('request4quote/success');
		$block = new $className();
	
		$emailTemplate  = Mage::getModel('core/email_template')
					->loadDefault('quote_request_email_template');
		//Create an array of variables to assign to template
		$emailTemplateVariables = array();
		$emailTemplateVariables['customer_name'] = $quote->getCustomerFirstname(). " ".$quote->getCustomerLastname();
		$street = $shippingAddress->getStreet();
		$emailTemplateVariables['customer_street'] = reset($street);
		$emailTemplateVariables['request_number'] = $quote->getId();
		$emailTemplateVariables['view_link'] = Mage::getUrl('requestquote/quote/view', array('token' => $quote->getR4qToken()));
		$emailTemplateVariables['time'] = $quote->getCreatedAt();
        $emailTemplateVariables['billing'] = $quote->getBillingAddress();
		$emailTemplateVariables['address'] = ($shippingAddress && !$shippingAddress->getSameAsBilling())?$shippingAddress:$emailTemplateVariables['billing'];
		$emailTemplateVariables['store'] = Mage::app()->getStore();
        $emailTemplateVariables['customer'] = Mage::getModel("customer/customer")->load($quote->getCustomerId());
		$emailTemplateVariables['quote'] = $quote;
		$emailTemplateVariables['handler'] = self::getItemsHandler();
		
	
		$emailTemplate->setSenderName(Mage::getStoreConfig('trans_email/ident_sales/name'));
		$emailTemplate->setSenderEmail(Mage::getStoreConfig('trans_email/ident_sales/email'));
		$emailTemplate->setTemplateSubject(Mage::helper('request4quote')->__('New Quote Request'));
		$emailTemplate->send($this->getEmailRecepients($quote), $emailTemplateVariables['customer_name'], $emailTemplateVariables);
    }    
    
    public function sendRequestProposalNotification($quote)
    {
		$quote = Mage::getModel('request4quote/quote')->loadByIdWithoutStore($quote->getId());
		
		$className = Mage::getConfig()->getBlockClassName('request4quote/success');
		$block = new $className();
	
		$emailTemplate  = Mage::getModel('core/email_template')
					->loadDefault('quote_request_processed_email_template');
		//Create an array of variables to assign to template
		$emailTemplateVariables = array();
		$emailTemplateVariables['customer_name'] = $quote->getCustomerFirstname() . ' ' . $quote->getCustomerLastname();
		$emailTemplateVariables['request_number'] = $quote->getId();
		$emailTemplateVariables['view_link'] = Mage::getUrl('requestquote/quote/view', array('token' => $quote->getR4qToken()));
		$emailTemplateVariables['store'] = Mage::app()->getStore();
		$emailTemplateVariables['quote'] = $quote;
		$emailTemplateVariables['handler'] = self::getItemsHandler();
		$emailTemplateVariables['comment'] = '';
		if ($quote->getR4qUpdateComment()) {
			$emailTemplateVariables['comment'] = '<br />'.Mage::helper('request4quote')->__('Comment: ').$quote->getR4qUpdateComment().'<br />';
		}
		
	
		$emailTemplate->setSenderName(Mage::getStoreConfig('trans_email/ident_sales/name'));
		$emailTemplate->setSenderEmail(Mage::getStoreConfig('trans_email/ident_sales/email'));
		$emailTemplate->setTemplateSubject(Mage::helper('request4quote')->__('Quote request processed email'));
		$emailTemplate->send(array($quote->getCustomerEmail(), Mage::getStoreConfig('trans_email/ident_sales/email')), $quote->getCustomerFirstname() . ' ' . $quote->getCustomerLastname(), $emailTemplateVariables);
		//$processedTemplate = $emailTemplate->getProcessedTemplate($emailTemplateVariables);
		//echo $processedTemplate;
    }    
    
    public function sendRequestDeclineNotification($quote)
    {
		$quote = Mage::getModel('request4quote/quote')->loadByIdWithoutStore($quote->getId());
	
		$className = Mage::getConfig()->getBlockClassName('request4quote/success');
		$block = new $className();
	
		$emailTemplate  = Mage::getModel('core/email_template')
					->loadDefault('quote_request_declined_email_template');
		//Create an array of variables to assign to template
		$emailTemplateVariables = array();
		$emailTemplateVariables['customer_name'] = $quote->getCustomerFirstname() . ' ' . $quote->getCustomerLastname();
		$emailTemplateVariables['request_number'] = Mage::helper('request4quote')->getQuoteCode($quote);
		$emailTemplateVariables['view_link'] = Mage::getUrl('requestquote/quote/view', array('token' => $quote->getR4qToken()));
		$emailTemplateVariables['store'] = Mage::app()->getStore();
		$emailTemplateVariables['quote'] = $quote;
		$emailTemplateVariables['reason'] = $quote->getR4qDeclineReason();
		$emailTemplateVariables['handler'] = self::getItemsHandler();
	
		
	
		$emailTemplate->setSenderName(Mage::getStoreConfig('trans_email/ident_sales/name'));
		$emailTemplate->setSenderEmail(Mage::getStoreConfig('trans_email/ident_sales/email'));
		$emailTemplate->setTemplateSubject(Mage::helper('request4quote')->__('Quote request declined email'));
		$emailTemplate->send(array($quote->getCustomerEmail(), Mage::getStoreConfig('trans_email/ident_sales/email')), $quote->getCustomerFirstname() . ' ' . $quote->getCustomerLastname(), $emailTemplateVariables);
		//$processedTemplate = $emailTemplate->getProcessedTemplate($emailTemplateVariables);
		//echo $processedTemplate;
    }
	
	public function sendAcceptNotification($quote)
	{
		$emailTemplate  = Mage::getModel('core/email_template')
					->loadDefault('quote_request_accept_template');
		//Create an array of variables to assign to template
		$emailTemplateVariables = array();
		$emailTemplateVariables['customer_name'] = $quote->getCustomerFirstname() . ' ' . $quote->getCustomerLastname();
		$emailTemplateVariables['request_number'] = Mage::helper('request4quote')->getQuoteCode($quote);
		$emailTemplateVariables['view_link'] = Mage::getUrl('requestquote/quote/view', array('token' => $quote->getR4qToken()));
		$emailTemplateVariables['store'] = Mage::app()->getStore();
		$emailTemplateVariables['quote'] = $quote;
		$emailTemplateVariables['handler'] = self::getItemsHandler();
		
	
		$emailTemplate->setSenderName(Mage::getStoreConfig('trans_email/ident_sales/name'));
		$emailTemplate->setSenderEmail(Mage::getStoreConfig('trans_email/ident_sales/email'));
		$emailTemplate->setTemplateSubject(Mage::helper('request4quote')->__('Quote request accepted email'));
		// send to customer
		$emailTemplate->send($quote->getCustomerEmail(), $quote->getCustomerFirstname() . ' ' . $quote->getCustomerLastname(), $emailTemplateVariables);
		//$processedTemplate = $emailTemplate->getProcessedTemplate($emailTemplateVariables);
		//echo $processedTemplate;
		//exit();
		// send to admin
		$urlModel = Mage::getModel('adminhtml/url');
		$emailTemplateVariables['view_link'] = Mage::getUrl('request4quote/adminhtml_quote/edit', array('quote_id' => $quote->getId(), Mage_Adminhtml_Model_Url::SECRET_KEY_PARAM_NAME => $urlModel->getSecretKey('adminhtml_quote', 'edit')));
		$emailTemplate->send(Mage::getStoreConfig('trans_email/ident_sales/email'), $quote->getCustomerFirstname() . ' ' . $quote->getCustomerLastname(), $emailTemplateVariables);
	}
	
	public function sendRejectNotification($quote)
	{
		$emailTemplate  = Mage::getModel('core/email_template')
					->loadDefault('quote_request_reject_template');
		//Create an array of variables to assign to template
		$emailTemplateVariables = array();
		$emailTemplateVariables['customer_name'] = $quote->getCustomerFirstname() . ' ' . $quote->getCustomerLastname();
		$emailTemplateVariables['request_number'] = Mage::helper('request4quote')->getQuoteCode($quote);
		$emailTemplateVariables['view_link'] = Mage::getUrl('requestquote/quote/view', array('token' => $quote->getR4qToken()));
		$emailTemplateVariables['store'] = Mage::app()->getStore();
		$emailTemplateVariables['quote'] = $quote;
		$emailTemplateVariables['reason'] = $quote->getR4qRejectReason();
		$emailTemplateVariables['handler'] = self::getItemsHandler();
	
	
		$emailTemplate->setSenderName(Mage::getStoreConfig('trans_email/ident_sales/name'));
		$emailTemplate->setSenderEmail(Mage::getStoreConfig('trans_email/ident_sales/email'));
		$emailTemplate->setTemplateSubject(Mage::helper('request4quote')->__('Quote request rejected email'));
		$emailTemplate->send($quote->getCustomerEmail(), $quote->getCustomerFirstname() . ' ' . $quote->getCustomerLastname(), $emailTemplateVariables);
		// send to admin
		$urlModel = Mage::getModel('adminhtml/url');
		$emailTemplateVariables['view_link'] = Mage::getUrl('request4quote/adminhtml_quote/edit', array('quote_id' => $quote->getId(), Mage_Adminhtml_Model_Url::SECRET_KEY_PARAM_NAME => $urlModel->getSecretKey('adminhtml_quote', 'edit')));
		$emailTemplate->send(Mage::getStoreConfig('trans_email/ident_sales/email'), $quote->getCustomerFirstname() . ' ' . $quote->getCustomerLastname(), $emailTemplateVariables);
	}
}
