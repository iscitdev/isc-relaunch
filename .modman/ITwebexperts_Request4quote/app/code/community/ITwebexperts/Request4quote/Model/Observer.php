<?php
class ITwebexperts_Request4quote_Model_Observer {
	
	public function productLoadAfter($observer)
	{
		$product = $observer->getProduct();
		if ($product->getR4qHidePrice()) {
			$product->setCanShowPrice(false);
			if ($product->getTypeId() == Mage_Catalog_Model_Product_Type::TYPE_BUNDLE) {
				$product->setR4qBundle(true);
			}
		}
	}
	
	public function productCollectionLoadAfter($observer)
	{
		$collection = $observer->getCollection();
		foreach ($collection AS $product) {
			if ($product->getR4qHidePrice()) {
				$product->setCanShowPrice(false);
			}
		}
	}
	
	public function priceBlockAfter($observer)
	{
		$block = $observer->getBlock();
		if ($block instanceof Mage_Catalog_Block_Product_Price || $block instanceof ITwebexperts_Payperrentals_Block_Catalog_Product_Viewprice) {
			$originalHTML = $observer->getTransport()->getHtml();
			$additionalHTML = '<input type="hidden" name="r4q_hidecart" value="'.(int)$block->getProduct()->getR4qOrderDisabled().'" />';
			if ($block->getProduct()->getR4qEnabled() && !Mage::registry('current_product')) {
				$additionalHTML .= '<input type="hidden" name="r4q_quote_enabled" value="'.(int)$block->getProduct()->getId().'" />';
			}
			$observer->getTransport()->setHtml( $originalHTML.$additionalHTML );
		}
	}
	
	public function orderPlaceAfter($observer)
	{
		$orders = $observer->getEvent()->getOrders();
        if (!$orders) {
            $orders = array($observer->getEvent()->getOrder());
        }

        if (!$orders) {
            return $this;
        }
		
		$quoteIds = array();
		foreach ($orders AS $order) {
			foreach ($order->getAllItems() AS $item) {
				$infoByRequest = $item->getBuyRequest();
				if ($infoByRequest->getData(ITwebexperts_Request4quote_Model_Quote::QUOTE_ID_OPTION)) {
					$quoteId = (int)$infoByRequest->getData(ITwebexperts_Request4quote_Model_Quote::QUOTE_ID_OPTION);
					$quoteIds[] = $quoteId;
				}
			}
		}
		if ($quoteIds) {
			$quoteIds = array_unique($quoteIds);
			foreach ($quoteIds AS $quoteId) {
				$quote = Mage::getModel('request4quote/quote')->loadByIdWithoutStore($quoteId);
				if ($quote->getId()) {
					$quote->setR4qStatus(ITwebexperts_Request4quote_Model_Quote::STATUS_ORDERED);
					$quote->save();
				}
			}
		}
		
	}
	
}