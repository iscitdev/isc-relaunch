<?php
if (Mage::helper('itwebcommon')->hasWarehouse()){

    class ITwebexperts_Request4quote_Model_Quote_Item_Component extends Innoexts_Warehouse_Model_Sales_Quote_Item{
		 
		 /**
     * @param null $stockItem
     * @return Varien_Object
    */
    //problem when adding simple rfq
    public function checkQty($stockItem = null)
    {
        $result = new Varien_Object();
        $result->setHasError(false);
        if (!$this->getProductId() || !$this->getQuote()) {
            $result->setHasError(true);
            return $result;
        }
        if ($this->getQuote()->getIsSuperMode()) {
            $result->setHasError(false);
            return $result;
        }
        $product = $this->getProduct();
        if (!$stockItem) {
            $stockItem = $this->getStockItem();
        } else if (!$stockItem->getProduct()) {
            $stockItem->setProduct($product);
        }
        $qty = $this->getQty();
        if (($options = $this->getQtyOptions()) && $qty > 0) {
            $qty = $product->getTypeInstance(true)->prepareQuoteItemQty($qty, $product);
            if ($stockItem) {
                $result = $stockItem->checkQtyIncrements($qty);
                if ($result->getHasError()) {
                    return $result;
                }
            }
            foreach ($options as $option) {
                if ($stockItem) {
                    $option->setStockId($stockItem->getStockId());
                }
                $optionQty = $qty * $option->getValue();
                $increaseOptionQty = ($this->getQtyToAdd() ? $this->getQtyToAdd() : $qty) * $option->getValue();
                $option->unsetStockItem();
                $stockItem = $option->getStockItem();
                if (!$stockItem instanceof Mage_CatalogInventory_Model_Stock_Item || !$stockItem instanceof ITwebexperts_PPRWarehouse_Model_CatalogInventory_Stock_Item)
                    return false;
                $stockItem->setOrderedItems(0);
                $stockItem->setIsChildItem(true);
                $stockItem->setSuppressCheckQtyIncrements(true);
                $qtyForCheck = $increaseOptionQty;
                /** @var ITwebexperts_PPRWarehouse_Model_CatalogInventory_Stock_Item $stockItem */
                $optionResult = $stockItem->checkQuoteItemQty($optionQty, $qtyForCheck, $option->getValue(), $this);
                $stockItem->unsIsChildItem();
                if (!$optionResult->getHasError()) {
                    if ($optionResult->getHasQtyOptionUpdate()) {
                        $result->setHasQtyOptionUpdate(true);
                    }
                    if ($optionResult->getItemIsQtyDecimal()) {
                        $result->setItemIsQtyDecimal(true);
                    }
                    if ($optionResult->getItemQty()) {
                        $result->setItemQty(floatval($result->getItemQty()) + $optionResult->getItemQty());
                    }
                    if ($optionResult->getOrigQty()) {
                        $result->setOrigQty(floatval($result->getOrigQty()) + $optionResult->getOrigQty());
                    }
                    if ($optionResult->getItemUseOldQty()) {
                        $result->setItemUseOldQty(true);
                    }
                    if ($optionResult->getItemBackorders()) {
                        $result->setItemBackorders(floatval($result->getItemBackorders()) + $optionResult->getItemBackorders());
                    }
                } else {
                    return $optionResult;
                }
            }
        } else {
            if (!$stockItem instanceof Mage_CatalogInventory_Model_Stock_Item || !$stockItem instanceof ITwebexperts_PPRWarehouse_Model_CatalogInventory_Stock_Item) {
                $result->setHasError(true);
                return $result;
            }
            $rowQty = $increaseQty = 0;
            if (!$this->getParentItem()) {
                $increaseQty = $this->getQtyToAdd() ? $this->getQtyToAdd() : $qty;
                $rowQty = $qty;
            } else {
                $rowQty = $this->getParentItem()->getQty() * $qty;
            }
            $qtyForCheck = $increaseQty;
            $productTypeCustomOption = $product->getCustomOption('product_type');
            if (!is_null($productTypeCustomOption)) {
                if ($productTypeCustomOption->getValue() == Mage_Catalog_Model_Product_Type_Grouped::TYPE_CODE) {
                    $stockItem->setIsChildItem(true);
                }
            }
            /** @var ITwebexperts_PPRWarehouse_Model_CatalogInventory_Stock_Item $stockItem */
            $result = $stockItem->checkQuoteItemQty($rowQty, $qtyForCheck, $qty, $this);
            if ($stockItem->hasIsChildItem()) {
                $stockItem->unsIsChildItem();
            }
        }
        return $result;
    }
    }

}else{
    class ITwebexperts_Request4quote_Model_Quote_Item_Component extends Mage_Sales_Model_Quote_Item
    {

    }
}

class ITwebexperts_Request4quote_Model_Quote_Item extends ITwebexperts_Request4quote_Model_Quote_Item_Component {
    
    protected $_eventPrefix = 'request4quote_quote_item';
    protected $_eventObject = 'r4q_item';
    protected $statusLabel;
    
    protected function _construct()
    {
        $this->_init('request4quote/quote_item');
        $this->_errorInfos = Mage::getModel('sales/status_list');
    }

    public function addOption($option)
    {
        if (is_array($option)) {
            $option = Mage::getModel('request4quote/quote_item_option')->setData($option)
                ->setItem($this);
        }
        elseif (($option instanceof Varien_Object) && !($option instanceof Mage_Sales_Model_Quote_Item_Option)) {
            $option = Mage::getModel('request4quote/quote_item_option')->setData($option->getData())
               ->setProduct($option->getProduct())
               ->setItem($this);
        }
        elseif($option instanceof Mage_Sales_Model_Quote_Item_Option) {
            $option->setItem($this);
        }
        else {
            Mage::throwException(Mage::helper('sales')->__('Invalid item option format.'));
        }

        if ($exOption = $this->getOptionByCode($option->getCode())) {
            $exOption->addData($option->getData());
        }
        else {
            $this->_addOptionCode($option);
            $this->_options[] = $option;
        }
        return $this;
    }

    public function getStatus(){
        return $this->getR4qStatus();
    }

    public function getStatusLabel(){
        if (!$this->statusLabel) {
            $this->statusLabel = Mage::getModel("request4quote/quote_item_status")->load($this->getStatus())->getStoreLabel();
        }
        return $this->statusLabel;
    }
}
