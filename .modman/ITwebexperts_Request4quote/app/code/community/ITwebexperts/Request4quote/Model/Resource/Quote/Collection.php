<?php
class ITwebexperts_Request4quote_Model_Resource_Quote_Collection extends Mage_Sales_Model_Resource_Quote_Collection {
	
	protected function _construct()
    {
        $this->_init('request4quote/quote');
    }
	
	public function addShippingAddress()
	{
        $_shippingAliasName = 'rfqa';
        $_joinTable = $this->getTable('request4quote/quote_address');
		
			
        $this->getSelect()->joinLeft(
            array($_shippingAliasName => $_joinTable),
            "(main_table.entity_id = {$_shippingAliasName}.quote_id AND IF((SELECT COUNT(*) FROM {$_joinTable} as {$_shippingAliasName} WHERE main_table.entity_id = {$_shippingAliasName}.quote_id AND {$_shippingAliasName}.address_type = 'shipping' GROUP BY {$_shippingAliasName}.quote_id),
                {$_shippingAliasName}.address_type='shipping',
                {$_shippingAliasName}.address_type='billing'))",
                array(
                'shipping_firstname' => $_shippingAliasName . '.firstname',
                'shipping_lastname' => $_shippingAliasName . '.lastname',
                'shipping_telephone' => $_shippingAliasName . '.telephone',
                'shipping_postcode' => $_shippingAliasName . '.postcode',
                'shipping_country_id' => $_shippingAliasName . '.country_id',
                'shipping_region' => $_shippingAliasName . '.region',
                'shipping_city' => $_shippingAliasName . '.city',
                'shipping_street' => $_shippingAliasName . '.street'
                )
            );
		Mage::getResourceHelper('core')->prepareColumnsList($this->getSelect());
		return $this;
	}
	
}