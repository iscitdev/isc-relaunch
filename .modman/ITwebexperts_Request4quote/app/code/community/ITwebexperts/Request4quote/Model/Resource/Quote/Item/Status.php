<?php
class ITwebexperts_Request4quote_Model_Resource_Quote_Item_Status extends ITwebexperts_Request4quote_Model_Resource_Quote_Status
{
	protected function _construct()
    {
        $this->_init('request4quote/quote_item_status', 'status');
		$this->_isPkAutoIncrement = false;
        $this->_labelsTable = $this->getTable('request4quote/quote_item_status_label');
    }
}