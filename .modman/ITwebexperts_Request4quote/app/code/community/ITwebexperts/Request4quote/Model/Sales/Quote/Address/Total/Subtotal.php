<?php

class ITwebexperts_Request4quote_Model_Sales_Quote_Address_Total_Subtotal extends Mage_Sales_Model_Quote_Address_Total_Subtotal
{

    protected function _initItem ( $address, $item )
    {
        parent::_initItem( $address, $item );
        if ($item instanceof Mage_Sales_Model_Quote_Address_Item) {
            $quoteItem = $item->getAddress()->getQuote()->getItemById( $item->getQuoteItemId() );
        } else {
            $quoteItem = $item;
        }
        if (!$quoteItem->getParentItem()) {
            $source = unserialize( $quoteItem->getProduct()->getCustomOption( 'info_buyRequest' )->getValue() );
            if (isset( $source[ITwebexperts_Request4quote_Model_Quote::PRICE_PROPOSAL_OPTION] )) {
                $finalPrice = floatval( $source[ITwebexperts_Request4quote_Model_Quote::PRICE_PROPOSAL_OPTION] );
                $item->setPrice( $finalPrice )
                    ->setBaseOriginalPrice( $finalPrice );
                $item->calcRowTotal();
            }
        }
        return true;
    }

}

