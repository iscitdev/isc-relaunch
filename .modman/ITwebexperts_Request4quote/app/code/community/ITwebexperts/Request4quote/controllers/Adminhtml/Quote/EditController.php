<?php
require_once('CreateController.php');
class ITwebexperts_Request4quote_Adminhtml_Quote_EditController extends ITwebexperts_Request4quote_Adminhtml_Quote_CreateController {


    public function startAction()
    {
        $this->_getSession()->clear();
        $quoteId = $this->getRequest()->getParam('quote_id');

        $quote = Mage::getModel('request4quote/quote')->load($quoteId);
        if ($quoteId) {
            $quote->loadByIdWithoutStore($quoteId);

        }
        $this->_getOrderCreateModel()->initFromQuote($quote);
        $this->_redirect('*/adminhtml_quote_edit/index');
    }
	
	public function indexAction()
	{
		$this->_title($this->__('Request4Quote'))->_title($this->__('Quotes'))->_title($this->__('Edit Quote'));
        $this->_initSession();
        $this->loadLayout();

        $this->_setActiveMenu('request4quote/quote')
            ->renderLayout();
	}

    public function cancelAction()
    {
        $this->_getSession()->clear();
        $this->_redirect('*/adminhtml_quote/index');
    }

    /**
     * Export order grid to CSV format
     */
    public function exportCsvAction()
    {
        $this->_initSession();
        $fileName   = 'quote_request_items.csv';
        $items      = $this->getLayout()->createBlock('request4quote/adminhtml_quote_create_items');
        $grid       = $this->getLayout()->createBlock('request4quote/adminhtml_quote_create_items_grid');
        $items->setChild("grid", $grid );
        $this->_prepareDownloadResponse($fileName, $grid->getCsvFile());
    }

    /**
     *  Export order grid to Excel XML format
     */
    public function exportExcelAction()
    {
        $fileName   = 'quote_request_items.xls';
        $items      = $this->getLayout()->createBlock('request4quote/adminhtml_quote_create_items');
        $grid       = $this->getLayout()->createBlock('request4quote/adminhtml_quote_create_items_grid');
        $items->setChild("grid", $grid );
        $this->_prepareDownloadResponse($fileName, $grid->getExcelFile($fileName));
    }
}