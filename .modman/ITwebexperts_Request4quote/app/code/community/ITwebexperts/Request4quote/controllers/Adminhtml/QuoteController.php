<?php

class ITwebexperts_Request4quote_Adminhtml_QuoteController extends Mage_Adminhtml_Controller_Action
{
	
	protected function _initQuote($idFieldName = 'quote_id')
    {
        $this->_title($this->__('Request4Quote'))->_title($this->__('Manage Quote Requests'));

        $quoteId = (int) $this->getRequest()->getParam($idFieldName);
        $quote = Mage::getModel('request4quote/quote');
        if ($quoteId) {
            $quote->loadByIdWithoutStore($quoteId);
			
        }
        Mage::register('current_quote', $quote);
        return $this;
    }
	
	public function indexAction()
	{
		$this->_title($this->__('Request4Quote'))->_title($this->__('Manage Quote Requests'));
		$this->loadLayout();
        $this->renderLayout();
	}
	
	public function editAction()
	{
        $this->_initQuote('quote_id');
		$this->loadLayout();
        $this->renderLayout();
	}
	
	public function saveAction()
	{
		try {
			$quoteId = $this->getRequest()->getPost('r4qQuoteId');
			$this->_initQuote('r4qQuoteId');
			$r4qQuote = Mage::registry('current_quote');
			if (!$r4qQuote->getId()) {
				throw new Exception($this->__('Wrong Quote ID'));
			}
			$status = $this->getRequest()->getPost('r4qStatus');
			$statusUpdated = false;
			
			if ($declineReason = $this->getRequest()->getPost('r4q_decline_reason')) {
				$r4qQuote->setR4qDeclineReason($declineReason);
			}
			
			if ($updateComment = $this->getRequest()->getPost('r4q_update_comment')) {
				$r4qQuote->setR4qUpdateComment($updateComment);
			}
			
			// is decline
			if ($status == ITwebexperts_Request4quote_Model_Quote::STATUS_DECLINED && $r4qQuote->getR4qStatus() != ITwebexperts_Request4quote_Model_Quote::STATUS_DECLINED) {
				$statusUpdated = true;
				$r4qQuote->setR4qStatus(ITwebexperts_Request4quote_Model_Quote::STATUS_DECLINED);
				$r4qQuote->save();
				$r4qQuote->sendDeclineEmail();
				$this->_redirect('*/*/');
				$this->_getSession()->addSuccess($this->__('Quote request has been declined succesfully.'));
				return;
			}
			

			$r4qData = $this->getRequest()->getPost('r4q');
			if (is_array($r4qData) && isset($r4qData['item'])) {
				foreach ($r4qData['item'] AS $itemId => $itemData) {
					$r4qItem = Mage::getModel('request4quote/quote_item')->load($itemId);
					if ($r4qItem->getId()) {
						$r4qItem->setQuote($r4qQuote);
						if (isset($itemData['remark'])) {
							$r4qItem->setR4qNote($itemData['remark']);
						}
						if (isset($itemData['price_proposal'])) {
							if (is_numeric($itemData['price_proposal'])) {
								$priceProposal = (float)$itemData['price_proposal'];
								$r4qItem->setR4qPriceProposal($priceProposal);
							}
						}
						$r4qItem->save();
					} else {
						throw new Exception($this->__('Wrong Quote Item ID'));
					}
				}
				
				if ($status == ITwebexperts_Request4quote_Model_Quote::STATUS_PROCESSED && $r4qQuote->getR4qStatus() != ITwebexperts_Request4quote_Model_Quote::STATUS_PROCESSED) {
					$statusUpdated = true;
					$r4qQuote->setR4qStatus(ITwebexperts_Request4quote_Model_Quote::STATUS_PROCESSED);
					$r4qQuote->save();
					//$r4qQuote->sendProposalEmail();
					$this->_getSession()->addSuccess($this->__('Quote proposal has been saved succesfully.'));
				}
				
			}
			$newStatus = $this->getRequest()->getPost('r4q_status');
			if (!$statusUpdated && $newStatus != $r4qQuote->getR4qStatus() && $newStatus) {
				$r4qQuote->setR4qStatus($newStatus);
			}
			$r4qQuote->save();
			$this->_getSession()->addSuccess($this->__('Quote request has been saved succesfully.'));
			$this->getResponse()->setRedirect($this->getUrl('*/adminhtml_quote/edit', array(
				'quote_id' => $quoteId)
			));
			return;
        } catch (Mage_Core_Exception $e) {
            $this->_getSession()->addError($e->getMessage());
        } catch (Exception $e) {
            $this->_getSession()->addError($e->getMessage());
            Mage::logException($e);
        }
		$this->_redirect('*/*/');
	}
	
	public function orderAction()
	{
		$quoteId = $this->getRequest()->getParam('quote_id');
		try {
			$this->_initQuote('quote_id');
			$r4qQuote = Mage::registry('current_quote');
			if (!$r4qQuote->getId()) {
				throw new Exception($this->__('Wrong Quote ID'));
			}
			if ($r4qQuote->getR4qStatus() != ITwebexperts_Request4quote_Model_Quote::STATUS_FINISHED && $r4qQuote->getR4qStatus() != ITwebexperts_Request4quote_Model_Quote::STATUS_FINISHED) {
				throw new Exception($this->__('Quote request is not processed'));
			}
			$orderSession = Mage::getSingleton('adminhtml/session_quote');
			$orderSession->clear();
			$orderCreateModel = Mage::getSingleton('adminhtml/sales_order_create');
			
			$importData = array();
			
			if ($r4qQuote->getCustomerId()) {
				$orderSession->setCustomerId($r4qQuote->getCustomerId());
			} else {
				$orderSession->setCustomerId(0);
				if ($r4qQuote->getCustomerEmail()) {
					$importData['order'] = array(
						'account' => array(
							'email' => $r4qQuote->getCustomerEmail()
						)
					);
				}
			}
			if ($r4qQuote->getQuoteCurrencyCode()) {
				$orderSession->setCurrencyId($r4qQuote->getQuoteCurrencyCode());
			}
			$orderSession->setStoreId($r4qQuote->getStoreId());
			
			// import order data
			$orderCreateModel->importPostData($importData);
            if ($r4qQuote->getBillingAddress()) {
                $orderCreateModel->setBillingAddress($r4qQuote->getBillingAddress()->unsAddressId()->getData());
            }
			
            if($r4qQuote->getShippingAddress()){
                $orderCreateModel->setShippingAddress($r4qQuote->getShippingAddress()->unsAddressId()->getData());
            }
            if (!$r4qQuote->getR4qShippingAsBilling() ||$r4qQuote->getR4qShippingAsBilling() == '0') {
                   //$orderCreateModel->setShippingAddress($r4qQuote->getBillingAddress());
                   $orderCreateModel->setShippingAsBilling(0);
            }
			// add items
			
			foreach ($r4qQuote->getAllItems() AS $item) {
				if ($item->getParentItem()) continue;
				$product = $item->getProduct();
				if ($product) {
					
					$product
						->setStoreId($r4qQuote->getStoreId())
						->load($product->getId());
					$info = $item->getOptionByCode('info_buyRequest');
					if ($info) {
						$infoBuyReqest = new Varien_Object(unserialize($info->getValue()));
					} else {
						$infoBuyReqest = new Varien_Object(array('qty' => 1));
					}
                    if (Mage::helper('request4quote')->isOpenendedInstalled()) {
                        $infoBuyReqest[ITwebexperts_Openendedinvoices_Helper_Data::BILLING_PERIOD_CUSTOM_PRICE] = $item->getR4qPriceProposal();
                    }
                    //$infoBuyRequest['r4q_price_proposal'] = $upPrice;
					$infoBuyReqest->setQty($item->getQty());
                    //check if is recurring then set the price for billing period
                    //should be added to open ended
                    $infoBuyReqest->setStockId($item->getStockId());
                    if($infoBuyReqest->getSelPay() && $infoBuyReqest->getSelPay() == 'recurring'){
					$infoBuyReqest->setData(
						ITwebexperts_Request4quote_Model_Quote::PRICE_PROPOSAL_OPTION,
                            0
                        );
                        //if($infoBuyReqest->getBillingPeriodCustomPrice()){
                        //}
                    }
                    else{
                        $infoBuyReqest->setData(
                            ITwebexperts_Request4quote_Model_Quote::PRICE_PROPOSAL_OPTION,
						$item->getR4qPriceProposal()
					);
                    }
					$infoBuyReqest->setData(
						ITwebexperts_Request4quote_Model_Quote::QUOTE_ID_OPTION,
						$r4qQuote->getId()
					);
//					$orderCreateModel->addProduct($product, $infoBuyReqest);
                    $orderCreateModel->addProduct($product,$infoBuyReqest->getData());
				}
			}

            $orderCreateModel->getQuote()->setData("quote_request_id",$r4qQuote->getId());
            $orderCreateModel->getQuote()->setData("admin_user_id",Mage::getSingleton('admin/session')->getUser()->getUserId());

            try {
                $orderCreateModel->saveQuote();
            } catch (Exception $e){
                $orderCreateModel->resetShippingMethod();
                $orderCreateModel->saveQuote();
            }

			$this->_redirect('adminhtml/sales_order_create/index');
			return $this;
		} catch (Mage_Core_Exception $e) {
            $this->_getSession()->addError($e->getMessage());
        } catch (Exception $e) {
            $this->_getSession()->addError($e->getMessage());
            Mage::logException($e);
        }
		$this->_redirect('*/*/');
		
		
	}
	
	public function removeAction()
	{
		try {
			$this->_initQuote('quote_id');
			$r4qQuote = Mage::registry('current_quote');
			if (!$r4qQuote->getId()) {
				throw new Exception($this->__('Wrong Quote ID'));
			}
			// item id
			$itemId = (int)$this->getRequest()->getParam('item_id');
			// remove
			$orderCreateModel = Mage::getSingleton('request4quote/adminhtml_quote_create');
			$orderCreateModel->setQuote($r4qQuote);
			$orderCreateModel->removeItem($itemId, 'quote');
			$orderCreateModel->saveQuote();
			// success message
			$this->_getSession()->addSuccess($this->__('Item has been removed succesfully.'));
			// redirect
			$this->_redirect('*/*/edit', array('quote_id' => $r4qQuote->getId()));
			return;
		} catch (Mage_Core_Exception $e) {
            $this->_getSession()->addError($e->getMessage());
        } catch (Exception $e) {
            $this->_getSession()->addError($e->getMessage());
            Mage::logException($e);
        }
		$this->_redirect('*/*/index');
	}
	
	public function loadBlockAction()
	{
		$error = false;
		$request = $this->getRequest();
		try {
			$this->_initQuote('quote_id');
			$r4qQuote = Mage::registry('current_quote');
			if (!$r4qQuote->getId()) {
				throw new Exception($this->__('Wrong Quote ID'));
			}
			if ($this->getRequest()->has('item')) {
				$items = $this->getRequest()->getPost('item');
				$items = $this->_processFiles($items);
				$orderCreateModel = Mage::getSingleton('request4quote/adminhtml_quote_create');
				$orderCreateModel->setQuote($r4qQuote);
				$originalStoreId = $orderCreateModel->getSession()->getStoreId();
				$originalStore = $orderCreateModel->getSession()->getStore();
				$orderCreateModel->getSession()->setStoreId($r4qQuote->getStoreId());
				$orderCreateModel->getSession()->setStore($r4qQuote->getStore());
				// add products
				$orderCreateModel->addProducts($items);
				// back session to original
				$orderCreateModel->getSession()->setStoreId($originalStoreId);
				$orderCreateModel->getSession()->setStore($originalStore);
				$orderCreateModel->saveQuote();
				
			}
		} catch (Mage_Core_Exception $e) {
            $this->_getSession()->addError($e->getMessage());
        } catch (Exception $e) {
            $this->_getSession()->addError($e->getMessage());
            Mage::logException($e);
        }
        $asJson= $request->getParam('json');
        $block = $request->getParam('block');
		$update = $this->getLayout()->getUpdate();
        if ($asJson) {
            $update->addHandle('request4quote_adminhtml_quote_create_load_block_json');
        } else {
            $update->addHandle('request4quote_adminhtml_quote_create_load_block_plain');
        }
        if ($block) {
            $blocks = explode(',', $block);
            if ($asJson && !in_array('message', $blocks)) {
                $blocks[] = 'message';
            }
            foreach ($blocks as $block) {
                $update->addHandle('request4quote_adminhtml_quote_create_load_block_' . $block);
            }
        }
		$this->loadLayoutUpdates()->generateLayoutXml()->generateLayoutBlocks();
		$result = $this->getLayout()->getBlock('content')->toHtml();
        if ($request->getParam('as_js_varname')) {
            Mage::getSingleton('adminhtml/session')->setUpdateResult($result);
            $this->_redirect('*/*/showUpdateResult');
        } else {
            $this->getResponse()->setBody($result);
        }
	}
	
	public function showUpdateResultAction()
    {
        $session = Mage::getSingleton('adminhtml/session');
        if ($session->hasUpdateResult() && is_scalar($session->getUpdateResult())){
            $this->getResponse()->setBody($session->getUpdateResult());
            $session->unsUpdateResult();
        } else {
            $session->unsUpdateResult();
            return false;
        }
    }
	
	protected function _processFiles($items)
    {
        /* @var $productHelper Mage_Catalog_Helper_Product */
        $productHelper = Mage::helper('catalog/product');
        foreach ($items as $id => $item) {
            $buyRequest = new Varien_Object($item);
            $params = array('files_prefix' => 'item_' . $id . '_');
            $buyRequest = $productHelper->addParamsToBuyRequest($buyRequest, $params);
            if ($buyRequest->hasData()) {
                $items[$id] = $buyRequest->toArray();
            }
        }
        return $items;
    }
}