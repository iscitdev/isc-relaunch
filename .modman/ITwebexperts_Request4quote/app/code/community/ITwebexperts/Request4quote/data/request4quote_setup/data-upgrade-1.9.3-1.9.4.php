<?php

$finishedStatus = array(
    'status' => 'finished',
    'label' => 'Finished',
    'is_system' => 1,
    'is_visible' => 1
);

$status = Mage::getModel('request4quote/quote_status')->setData($finishedStatus)->save();

Mage::getModel('request4quote/quote_status')->getCollection()->addFieldToFilter("status",
    array("in" =>
        array( 'accepted', 'declined', 'ordered', 'processed', 'rejected' ))
)->setDataToAll("is_visible",0)->save();