<?php

$installer = $this;
/* @var $installer Mage_Catalog_Model_Resource_Eav_Mysql4_Setup */

$installer->startSetup();

if( !$installer->getConnection()->tableColumnExists($this->getTable('request4quote/quote_item'), "r4q_status") ){
    $installer->run("
    ALTER TABLE {$this->getTable('request4quote/quote_item')} ADD `r4q_status` varchar(255) NOT NULL DEFAULT '10';
    ");
}

$installer->run("
DROP TABLE IF EXISTS {$this->getTable('request4quote/quote_item_status')};
CREATE TABLE {$this->getTable('request4quote/quote_item_status')} LIKE {$this->getTable('request4quote/quote_status')};
DROP TABLE IF EXISTS {$this->getTable('request4quote/quote_item_status_label')};
CREATE TABLE {$this->getTable('request4quote/quote_item_status_label')} LIKE {$this->getTable('request4quote/quote_status_label')};
");

$installer->run("
INSERT INTO `{$this->getTable('request4quote/quote_item_status')}` (`status`, `label`, `is_system`) VALUES
('10', 'offen', 1),
('20', 'in Beschaffung', 1),
('30', 'in Aufbereitung', 1),
('40', 'in Vormerkung', 1),
('50', 'abgeschlossen', 1),
('60', 'keine Lieferung', 1);
");
$installer->endSetup();
