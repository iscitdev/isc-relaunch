<?php


/* @var $installer Mage_Sales_Model_Resource_Setup */
$installer = $this;
$installer->startSetup();

$attributeCode = "quote_request_id";


$salesInstaller = new Mage_Sales_Model_Resource_Setup('core_setup');
$options = array('type' => "int");
$salesInstaller->addAttribute("quote", $attributeCode, $options);
$salesInstaller->addAttribute("order", $attributeCode, $options);

$installer->endSetup();
