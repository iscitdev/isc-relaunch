<?php


/* @var $installer Mage_Catalog_Model_Resource_Eav_Mysql4_Setup */
$installer = $this;
$installer->startSetup();
if(!$installer->getConnection()->tableColumnExists($installer->getTable("request4quote/quote_status"),"is_visible")) {
    $installer->getConnection()->addColumn($installer->getTable("request4quote/quote_status"),"is_visible",array (
        'type'      => Varien_Db_Ddl_Table::TYPE_SMALLINT,
        'length'    => 1,
        'comment'   => 'Visible save status',
        'unsigned' => true,
        'nullable' => false,
        'default' => 1
    ));
}

$attributeCode = "admin_user_id";

$salesInstaller = new Mage_Sales_Model_Resource_Setup('core_setup');
$options = array('type' => "int", "comment" => "Admin user who created the order");
$salesInstaller->addAttribute("quote", $attributeCode, $options);
$salesInstaller->addAttribute("order", $attributeCode, $options);

$installer->endSetup();