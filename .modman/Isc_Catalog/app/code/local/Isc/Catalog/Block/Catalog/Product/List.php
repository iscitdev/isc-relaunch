<?php
/**
 * <?xml version="1.0" encoding="UTF-8" standalone="yes"?>
 * <media>
 *      <images>
 *          <image sorting="1" ref="900_306083">001 - Produktbild</image>
 *          <image sorting="1" ref="900_307224">007 - VKA</image>
 *          <image sorting="2" ref="900_307223">007 - VKA</image>
 *          <image sorting="3" ref="900_307221">007 - VKA</image>
 *          <image sorting="1" ref="900_350308">010 - Logo / Button</image>
 *          <image sorting="2" ref="900_311771">010 - Logo / Button</image>
 *          <image sorting="1" ref="900_311769">011 - Detailbild ohne Untertitel</image>
 *          <image sorting="2" ref="900_311780">011 - Detailbild ohne Untertitel</image>
 *          <image sorting="3" ref="900_311779">011 - Detailbild ohne Untertitel</image>
 *          <image sorting="1" ref="900_313203">021 - Einsatzbild</image><image sorting="1" ref="900_347067">Produktbild</image>
 *      </images>
 * </media>
 */
class Isc_Catalog_Block_Catalog_Product_List extends Mage_Catalog_Block_Product_Abstract
{

    /**
     *
     * ISC Methods
     *
     */

    /**
     * Method to get the xml of a special attribute
     *
     * @param $attributeName
     * @return mixed
     */
    public function getXml($product, $attributeName)
    {
        return $this->helper('isc_parser/abstract')->parse($product->getData($attributeName));
    }

    /**
     * @param $xml
     * @param $size
     * @return mixed
     */
    public function getProductImageUrl($xml, $size)
    {
        $imageUrl = $this->helper('isc_parser/media')->generateProductImageUrl($xml->images->image['0'], $size);
        return $imageUrl;

    }


    /**
     * @param $image
     * @param $size
     * @return mixed
     */
    public function getImageUrl($image, $size)
    {
        $imageUrl = $this->helper('isc_parser/media')->generateProductImageUrl($image, $size);
        return $imageUrl;

    }


    /**
     * Method to get the VersionCollection for a product
     */
    public function getVersionCollectionByProduct($product){

        if($product){
            return $this->getVersionCollectionByProductNumber($product->getItemNo());
        }
        return false;

    }

    /**
     * Method to get the VersionCollection By productNo
     */
    protected function getVersionCollectionByProductNumber($number){

        if($number){

            $collection = Mage::getModel( 'catalog/product' )->getCollection()
                ->addAttributeToSelect( '*' )
                ->addFieldToFilter( 'sku', array( 'like' => $number . '\_0%' ) );

            if ($collection->count() > 1) {
                return $collection;
            }
        }
        return false;
    }


    /**
     *
     *
     * Magento Default List
     *
     *
     *
     */

    /**
     * Default toolbar block name
     *
     * @var string
     */
    protected $_defaultToolbarBlock = 'catalog/product_list_toolbar';

    /**
     * Product Collection
     *
     * @var Mage_Eav_Model_Entity_Collection_Abstract
     */
    protected $_productCollection;

    /**
     * Retrieve loaded category collection
     *
     * @return Mage_Eav_Model_Entity_Collection_Abstract
     */
    protected function _getProductCollection()
    {
        if (is_null($this->_productCollection)) {
            $layer = $this->getLayer();
            /* @var $layer Mage_Catalog_Model_Layer */
            if ($this->getShowRootCategory()) {
                $this->setCategoryId(Mage::app()->getStore()->getRootCategoryId());
            }

            // if this is a product view page
            if (Mage::registry('product')) {
                // get collection of categories this product is associated with
                $categories = Mage::registry('product')->getCategoryCollection()
                    ->setPage(1, 1)
                    ->load();
                // if the product is associated with any category
                if ($categories->count()) {
                    // show products from this category
                    $this->setCategoryId(current($categories->getIterator()));
                }
            }

            $origCategory = null;
            if ($this->getCategoryId()) {
                $category = Mage::getModel('catalog/category')->load($this->getCategoryId());
                if ($category->getId()) {
                    $origCategory = $layer->getCurrentCategory();
                    $layer->setCurrentCategory($category);
                    $this->addModelTags($category);
                }
            }
            $this->_productCollection = $layer->getProductCollection();

            $this->prepareSortableFieldsByCategory($layer->getCurrentCategory());

            if ($origCategory) {
                $layer->setCurrentCategory($origCategory);
            }
        }

        return $this->_productCollection;
    }

    /**
     * Get catalog layer model
     *
     * @return Mage_Catalog_Model_Layer
     */
    public function getLayer()
    {
        $layer = Mage::registry('current_layer');
        if ($layer) {
            return $layer;
        }
        return Mage::getSingleton('catalog/layer');
    }

    /**
     * Retrieve loaded category collection
     *
     * @return Mage_Eav_Model_Entity_Collection_Abstract
     */
    public function getLoadedProductCollection()
    {
        return $this->_getProductCollection();
    }

    /**
     * Retrieve current view mode
     *
     * @return string
     */
    public function getMode()
    {
        return $this->getChild('toolbar')->getCurrentMode();
    }

    /**
     * Need use as _prepareLayout - but problem in declaring collection from
     * another block (was problem with search result)
     */
    protected function _beforeToHtml()
    {
        $toolbar = $this->getToolbarBlock();

        // called prepare sortable parameters
        $collection = $this->_getProductCollection();

        // use sortable parameters
        if ($orders = $this->getAvailableOrders()) {
            $toolbar->setAvailableOrders($orders);
        }
        if ($sort = $this->getSortBy()) {
            $toolbar->setDefaultOrder($sort);
        }
        if ($dir = $this->getDefaultDirection()) {
            $toolbar->setDefaultDirection($dir);
        }
        if ($modes = $this->getModes()) {
            $toolbar->setModes($modes);
        }

        // set collection to toolbar and apply sort
        $toolbar->setCollection($collection);

        $this->setChild('toolbar', $toolbar);
        Mage::dispatchEvent('catalog_block_product_list_collection', array(
            'collection' => $this->_getProductCollection()
        ));

        $this->_getProductCollection()->load();

        return parent::_beforeToHtml();
    }

    /**
     * Retrieve Toolbar block
     *
     * @return Mage_Catalog_Block_Product_List_Toolbar
     */
    public function getToolbarBlock()
    {
        if ($blockName = $this->getToolbarBlockName()) {
            if ($block = $this->getLayout()->getBlock($blockName)) {
                return $block;
            }
        }
        $block = $this->getLayout()->createBlock($this->_defaultToolbarBlock, microtime());
        return $block;
    }

    /**
     * Retrieve additional blocks html
     *
     * @return string
     */
    public function getAdditionalHtml()
    {
        return $this->getChildHtml('additional');
    }

    /**
     * Retrieve list toolbar HTML
     *
     * @return string
     */
    public function getToolbarHtml()
    {
        return $this->getChildHtml('toolbar');
    }

    public function setCollection($collection)
    {
        $this->_productCollection = $collection;
        return $this;
    }

    public function addAttribute($code)
    {
        $this->_getProductCollection()->addAttributeToSelect($code);
        return $this;
    }

    public function getPriceBlockTemplate()
    {
        return $this->_getData('price_block_template');
    }

    /**
     * Retrieve Catalog Config object
     *
     * @return Mage_Catalog_Model_Config
     */
    protected function _getConfig()
    {
        return Mage::getSingleton('catalog/config');
    }

    /**
     * Prepare Sort By fields from Category Data
     *
     * @param Mage_Catalog_Model_Category $category
     * @return Mage_Catalog_Block_Product_List
     */
    public function prepareSortableFieldsByCategory($category) {
        if (!$this->getAvailableOrders()) {
            $this->setAvailableOrders($category->getAvailableSortByOptions());
        }
        $availableOrders = $this->getAvailableOrders();
        if (!$this->getSortBy()) {
            if ($categorySortBy = $category->getDefaultSortBy()) {
                if (!$availableOrders) {
                    $availableOrders = $this->_getConfig()->getAttributeUsedForSortByArray();
                }
                if (isset($availableOrders[$categorySortBy])) {
                    $this->setSortBy($categorySortBy);
                }
            }
        }

        return $this;
    }

    /**
     * Retrieve block cache tags based on product collection
     *
     * @return array
     */
    public function getCacheTags()
    {
        return array_merge(
            parent::getCacheTags(),
            $this->getItemsTags($this->_getProductCollection())
        );
    }
}

?>