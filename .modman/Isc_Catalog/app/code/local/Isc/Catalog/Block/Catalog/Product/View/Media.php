<?php
/**
 * <?xml version="1.0" encoding="UTF-8" standalone="yes"?>
 * <media>
 *      <images>
 *          <image sorting="1" ref="900_306083">001 - Produktbild</image>
 *          <image sorting="1" ref="900_307224">007 - VKA</image>
 *          <image sorting="2" ref="900_307223">007 - VKA</image>
 *          <image sorting="3" ref="900_307221">007 - VKA</image>
 *          <image sorting="1" ref="900_350308">010 - Logo / Button</image>
 *          <image sorting="2" ref="900_311771">010 - Logo / Button</image>
 *          <image sorting="1" ref="900_311769">011 - Detailbild ohne Untertitel</image>
 *          <image sorting="2" ref="900_311780">011 - Detailbild ohne Untertitel</image>
 *          <image sorting="3" ref="900_311779">011 - Detailbild ohne Untertitel</image>
 *          <image sorting="1" ref="900_313203">021 - Einsatzbild</image><image sorting="1" ref="900_347067">Produktbild</image>
 *      </images>
 * </media>
 */
class Isc_Catalog_Block_Catalog_Product_View_Media extends Mage_Catalog_Block_Product_View_Abstract
{

    /**
     * Method to get the xml of a special attribute
     *
     * @param $attributeName
     * @return mixed
     */
    public function getXml($attributeName)
    {
        $product = $this->getProduct();
        return $this->helper('isc_parser/abstract')->parse($product->getData($attributeName));
    }

    /**
     * Method to get the main Product Image Url
     * @param $xml
     * @param $size
     * @return mixed
     */
    public function getProductImageUrl($xml, $size)
    {
        $imageUrl = $this->helper('isc_parser/media')->generateProductImageUrl($xml->images->image['0'], $size);
        return $imageUrl;

    }

    /**
     * Method to get the Image Url of a Image-XML Element
     * @param $image
     * @param $size
     * @return mixed
     */
    public function getImageUrl($image, $size)
    {
        $imageUrl = $this->helper('isc_parser/media')->generateProductImageUrl($image, $size);
        return $imageUrl;

    }

}
