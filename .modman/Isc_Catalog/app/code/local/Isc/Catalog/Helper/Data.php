<?php

class Isc_Catalog_Helper_Data extends Mage_Core_Block_Template{

    /**
     * Method to check the availibility of a product
     * @param $product
     * @return bool
     */
    public function isProductAvailable($product){
        $stock = Mage::getModel('cataloginventory/stock_item')->loadByProduct($product);
        return $product && $stock->getQty() !== null && $stock->getQty() > 0;
    }


}

?>