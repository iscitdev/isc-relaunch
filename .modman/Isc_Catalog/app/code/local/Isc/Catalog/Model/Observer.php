<?php

class Isc_Catalog_Model_Observer
{


    protected $iscCatalogHelper;
    protected $customerGroupsHelper;

    public function __construct()
    {
        $this->iscCatalogHelper = Mage::helper('isc_catalog');
        $this->customerGroupsHelper = Mage::helper('cyberhouse_customergroups');
    }

    /**
     * Method to Filter the Customer Group before loading the ProductCollection
     * @param $observer
     * @return $this
     */
    public function catalogProductCollectionLoadBefore($observer)
    {

        //main collection variable
        $collection = $observer->getEvent()->getCollection();

        //The Customer Group Filter only on catagory page
        $product = Mage::registry('current_product');
        $category = Mage::registry('current_category');

        if ($product && $product->getId()) {
            // The current page is a product page.
            // Be aware that a current_product and a current_category can be set at the same time.
            // In that case the visitor is viewing a product in a category.
        } elseif ($category && $category->getId()) {
            // The current page is a category page
            //visibility on customer_groups
            $visibilityLevel = $this->customerGroupsHelper->getCurrentCustomerVisibilityLevel();

            // collect attribute value id for each visibility level
            //default is array[0] = 10
            if (count($visibilityLevel) == 1) {
                $collection->addAttributeToFilter('sp_lvl', array('eq' => $visibilityLevel[0]));
            } else {
                $attributeValues = array();
                foreach ($visibilityLevel as $label) {
                    $attributeValues[] = $label;
                }
                $collection->addAttributeToFilter('sp_lvl', array('in' => array($attributeValues)));
            }

        }

        /*        // Check if the current page is a CMS page
                if (Mage::getSingleton('cms/page')->getId()) {
                    // Do nothing


                    if (Mage::getSingleton('cms/page')->getIdentifier() == Mage::getStoreConfig('web/default/cms_home_page')) {
                        // The current page is the CMS home page
                        //do Nothing
                    }
                }

                // Check for cart page
                if (Mage::app()->getFrontController()->getAction()->getFullActionName() == 'checkout_cart_index') {
                    // The current page is the cart
                }*/

        return $this;
    }


}

?>