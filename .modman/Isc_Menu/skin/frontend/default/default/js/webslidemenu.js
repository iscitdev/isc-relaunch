jQuery(function() {
	jQuery('#wsnavtoggle').click(function () {
		jQuery('.wsmenucontainer').toggleClass('wsoffcanvasopener');
	});

	jQuery('.overlapblackbg').click(function () {
		jQuery('.wsmenucontainer').removeClass('wsoffcanvasopener');
	});


	//MAIN Menu UL SHOW/HIDE JS
	jQuery('.wsmenu-list> li').has('.wsmenu-submenu').prepend('<span class="wsmenu-click"><i class="wsmenu-arrow fa fa-angle-down"></i></span>');
	jQuery('.wsmenu-list > li').has('.megamenu').prepend('<span class="wsmenu-click"><i class="wsmenu-arrow fa fa-angle-down"></i></span>');

	jQuery('.wsmenu-click').click(function(){
		jQuery(this).toggleClass('ws-activearrow')
			.parent().siblings().children().removeClass('ws-activearrow');

		jQuery(".wsmenu-submenu, .megamenu").not(jQuery(this).siblings('.wsmenu-submenu, .megamenu')).slideUp('slow');
		jQuery(this).siblings('.wsmenu-submenu').slideToggle('slow');
		jQuery(this).siblings('.megamenu').slideToggle('slow');
	});

	//MAIN Menu UL SHOW/HIDE JS
	//SUB Menu UL SHOW JS
	jQuery('.wsmenu-list > li > ul > li').has('.wsmenu-submenu-sub').prepend('<span class="wsmenu-click02"><i class="wsmenu-arrow fa fa-angle-down"></i></span>');
	jQuery('.wsmenu-list > li > ul > li > ul > li').has('.wsmenu-submenu-sub-sub').prepend('<span class="wsmenu-click02"><i class="wsmenu-arrow fa fa-angle-down"></i></span>');

	jQuery('.wsmenu-click02').click(function(){
		jQuery(this).children('.wsmenu-arrow').toggleClass('wsmenu-rotate');
		jQuery(this).siblings('.wsmenu-submenu-sub').slideToggle('slow');
		jQuery(this).siblings('.wsmenu-submenu-sub-sub').slideToggle('slow');

	});
	//SUB Menu UL SHOW JS



});