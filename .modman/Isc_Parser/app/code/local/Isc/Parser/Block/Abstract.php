<?php

class Isc_Parser_Block_Abstract extends Mage_Catalog_Block_Product
{

    /**
     * General function to prepare the layout and set the product
     */
    public function _prepareLayout()
    {
        $this->setProduct($this->getHelper('catalog/product_view')->getProduct());
        return parent::_construct();
    }

    /**
     * Method to get the xml of a special attribute
     *
     * @param $attributeName
     * @return mixed
     */
    public function getXml($attributeName, $product = null)
    {
        if ($product == null){
            $product = $this->getProduct();
        }

        return $this->helper('isc_parser/abstract')->parse($product->getData($attributeName));
    }


    /**
     * Method to get the formatted price of an product
     * @param Mage_Catalog_Model_Product $product
     * @return string
     */
    public function getFormattedPrice(Mage_Catalog_Model_Product $product){
        $price = $product->getPrice();
        $formattedPrice = ( ( $price > 0 ) ? Mage::helper( 'core' )->currency( $price, true, false ) . '*' : '----' );
        return $formattedPrice;
    }
}