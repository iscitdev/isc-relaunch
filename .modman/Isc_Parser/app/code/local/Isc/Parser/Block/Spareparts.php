<?php

/**
 * <?xml version="1.0" encoding="UTF-8" standalone="yes"?>
 * <sparepartmap>
 *      <explosionDrawing title="201 - Explosionszeichnung">902_152717</explosionDrawing>
 *      <sparepart productId="425980001001_00" positionNo="001" order="1" quantity="1" />
 *      <sparepart productId="425980001002_00" positionNo="002" order="2" quantity="1" />
 *      <sparepart productId="425980001004_00" positionNo="004" order="4" quantity="1" />
 *      <sparepart productId="425980001006_00" positionNo="006" order="6" quantity="1" />
 *      <sparepart productId="425980001007_00" positionNo="007" order="7" quantity="1" />
 *      <sparepart productId="425980001008_00" positionNo="008" order="8" quantity="1" />
 *      <sparepart productId="425980001009_00" positionNo="009" order="9" quantity="1" />
 *      <sparepart productId="425980001040_00" positionNo="O.P." quantity="1" />
 * </sparepartmap>
 */
class Isc_Parser_Block_Spareparts extends Isc_Parser_Block_Abstract
{
    const EXPLOSION_DRAWING_DEFAULT_WIDTH = 800;

    private $cartItems;
    private $productCache = array();

    /**
     * @var Isc_Parser_Helper_Data
     */
    private $dataHelper;

    /**
     * @var Isc_Parser_Helper_Media
     */
    private $mediaHelper;

    public function __construct(array $args)
    {
        parent::__construct($args);

        $this->dataHelper = Mage::helper('isc_parser');
    }

    public function getExplosionDrawing($size = 300) {
        if(!$this->cachedProductExists()) {
            $this->initialize();
        }
        $assetId = $this->productCache[$this->getProduct()->getSku()]["explosiondrawing"];
        return $this->dataHelper->getAssetUrl($assetId, $size);
    }

    public function getSpareparts() {
        if(!$this->cachedProductExists()) {
            $this->initialize();
        }

        return $this->productCache[$this->getProduct()->getSku()]["spareparts"];
    }

    public function getSparepartJavascriptList() {
        if(!$this->cachedProductExists()) {
            $this->initialize();
        }

        $spareparts = $this->productCache[$this->getProduct()->getSku()]["spareparts"];
        $jsList = array();

        foreach($spareparts as $sparepart) {
            $jsList[$sparepart["pos"]] = array(
                'objectType' => 'MageExplosionDrawingPart',
                'tlX' => $sparepart["marker"]["tlX"],
                'tlY' => $sparepart["marker"]["tlY"],
                'brX' => $sparepart["marker"]["brX"],
                'brY' => $sparepart["marker"]["brY"],
                'sku' => $sparepart["sku"],
                'pos' => $sparepart["pos"]
            );
        }

        return $jsList;
    }

    public function getPriceInfoHtml() {
        $price_block = $this->getLayout()->createBlock("magesetup/catalog_product_price");
        if($price_block) {
            $htmlTemplate = $this->getLayout()->createBlock('core/template')
                ->setTemplate('magesetup/price_info.phtml')
                ->setFormattedTaxRate($price_block->getFormattedTaxRate())
                ->setIsIncludingTax($price_block->isIncludingTax())
                ->setIsIncludingShippingCosts($price_block->isIncludingShippingCosts())
                ->setIsShowShippingLink($price_block->isShowShippingLink())
                ->setIsShowWeightInfo($price_block->getIsShowWeightInfo())
                ->setFormattedWeight($price_block->getFormattedWeight())
                ->toHtml();
            return $htmlTemplate;
        }
    }

    protected function initialize() {
        $spareparts = array();
        $explosionDrawing = "";

        $xml = $this->getXml('sparepartmap', null);
        if(!empty($xml)) {
            $cartItems = $this->getCartItems();

            //the explosion drrawing of an sparepart could be null
            if($xml["drawing"]== null) {
                $explosionDrawing = null;
            }
            else{
                $explosionDrawing = $xml["drawing"]->__toString();
            }

            foreach ($xml->sparepart as $element) {
                $sku = $element["productId"]->__toString();
                $product = Mage::getModel('catalog/product')->loadByAttribute("sku", $sku);

                $imageXml = $this->getXml('media', $product);
                $assetId = $imageXml->images->image[0]["ref"]->__toString();

                //the order of an sparepart could be null
                if ($element["order"] ==null ){
                    $order = null;
                }
                else{
                    $order = $element["order"]->__toString();
                }

                $sparepart = array(
                    'sku' => $sku,
                    'pos' => $element["positionNo"]->__toString(),
                    'order' => $order,
                    'quantity' => $element["quantity"]->__toString(),
                    'in_stock' => $this->isInStock($product),
                    'in_cart' => isset($cartItems[$sku]) ? 1 : 0,
                    'salable' => $product->getData('is_salable'),
                    'name' => $product->getName(),
                    'product_id' => $product->getId(),
                    'price' => $product->getPrice(),
                    'formatted_price' => $this->getFormattedPrice($product),
                    'img' => $this->dataHelper->getAssetUrl($assetId),
                    'asset_id' => $assetId,
                    'add_url' => Mage::helper('checkout/cart')->getAddUrl($product),
                    'ajax_url' => Mage::getUrl('checkout/cart/ajaxadd'),
                    'product_url' => $product->getProductUrl(),
                    'product' => $product
                );

                if ($element->marker) {
                    $sparepart['marker'] = array(
                        'tlX' => $element->marker->topLeftCoordinate['x']->__toString(),
                        'tlY' => $element->marker->topLeftCoordinate['y']->__toString(),
                        'brX' => $element->marker->bottomRightCoordinate['x']->__toString(),
                        'brY' => $element->marker->bottomRightCoordinate['y']->__toString()
                    );
                }


                $spareparts[] = $sparepart;
            }
        }
        $this->productCache[$this->getProduct()->getSku()] = array(
            "spareparts" => $spareparts,
            "explosiondrawing" => $explosionDrawing
        );
    }

    protected function isInStock($product){
        $stock = Mage::getModel('cataloginventory/stock_item')->loadByProduct($product);
        return $product && $product->getStockItem() && $stock->getQty() !== null && $stock->getQty() > 0;
    }

    protected function getCartItems()
    {
        if(!$this->cartItems) {
            $this->cartItems = array();
            $cartItemsCollection = Mage::getSingleton('checkout/session')->getQuote()->getItemsCollection();
            foreach($cartItemsCollection as $cartItem) {
                $this->cartItems[$cartItem->getSku()] = true;
            }
        }
        return $this->cartItems;
    }

    protected function cachedProductExists() {
        return array_key_exists($this->getProduct()->getSku(), $this->productCache);
    }

    public function initPriceAlertBlock($product){
        $block = $this->getPriceAlertBlock()->setProduct($product);
        $block->preparePriceAlertData();
        return $block;
    }

    public function initStockAlertBlock($product){
        $block = $this->getStockAlertBlock()->setProduct($product);
        $block->prepareStockAlertData();
        return $block;
    }

    /**
     * @return Cyberhouse_ProductAlert_Block_Product_View
     */
    public function getPriceAlertBlock(){
        return $this->getAlertUrlsBlock()->getChild('productalert_price');
    }

    /**
     * @return Cyberhouse_ProductAlert_Block_Product_View
     */
    public function getStockAlertBlock(){
        return $this->getAlertUrlsBlock()->getChild('productalert_stock');
    }

    public function getExtraAlert ( $product )
    {
        $block = $this->getAlertUrlsBlock()->getChild('backtostock_notice');
        if($block){
            return $block->setProduct($product);
        }
        return $this->getLayout()->createBlock("core/text_list");
    }

    /** @return Mage_Core_Block_Text_List */
    protected function getAlertUrlsBlock ()
    {
        return $this->getParentBlock()->getChild('alert_urls');
    }



}