<?php

class Isc_Parser_Helper_Abstract extends Mage_Core_Helper_Abstract
{


    /**
     * Method to get the xml of a special attribute and product
     *
     * @param $attributeName
     * @return mixed
     */
    public function getXml($attributeName)
    {
        return $this->parse($attributeName);
    }


    /**
     * Method to parse a xml
     * @param $xml
     * @return SimpleXMLElement|void
     */
    public function parse($xml)
    {
        if ($this->_isValidXML($xml)) {
            $xml = new SimpleXMLElement($xml);
            if (!$xml->count()) {
                return;
            }
            return $xml;
        }
    }

    /**Method to check if the xml is a valid document
     * @param $xml
     * @return bool
     */
    protected function _isValidXML($xml)
    {
        $doc = @simplexml_load_string($xml);
        if ($doc) {
            return true; //this is valid
        } else {
            return false; //this is not valid
        }
    }


}
	 