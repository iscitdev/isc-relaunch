<?php
class Isc_Parser_Helper_Data extends Mage_Core_Helper_Abstract
{
    const XML_PATH_ASSET_BASE_URL = 'iscconfig_options/asset_settings/asset_base_url';
    const XML_PATH_IMAGE_FACTORY_PATH = 'iscconfig_options/asset_settings/asset_if_path';

    // example structure: https://assets.einhell.com/im/imf/300/282_887/
    protected $assetUrlBase;
    protected $assetUrlImageFactoryPath;
    protected $fileExtension = '.jpg';

    /**
     * Generate complete asset url from asset id
     * @param string $assetId
     * @param int $size
     * @param bool $square
     * @return string
     */
    public function getAssetUrl($assetId, $size = 300, $square = false) {
        if (!$assetId) {
            return "";
        }

        $url = $this->getImageFactoryAbsolutePath();

        if ($square) {
            $size = 'x' . $size . ',y' .$size;
        }

        $url .= $size.'/'.$assetId.'/';
        return $url;
    }

    public function getDownloadAssetUrl($assetId) {
        return $this->getAssetUrl($assetId, 'none');
    }

    protected function getImageFactoryAbsolutePath(){
        return $this->getAssetBaseUrl().$this->getImageFactoryPath();
    }

    protected function getAssetBaseUrl() {
        if (!$this->assetUrlBase) {
            $this->assetUrlBase =  Mage::getStoreConfig(self::XML_PATH_ASSET_BASE_URL);
        }
        return $this->assetUrlBase;
    }

    protected function getImageFactoryPath(){
        if (!$this->assetUrlImageFactoryPath) {
            $this->assetUrlImageFactoryPath = Mage::getStoreConfig(self::XML_PATH_IMAGE_FACTORY_PATH);
        }
        return $this->assetUrlImageFactoryPath;
    }

    /**
     * Method to format the url and generate a clean url
     * @param $str
     * @param array $replace
     * @param string $delimiter
     * @return mixed|string
     */
    public function encodeUrl($str, $replace = array(), $delimiter = '-')
    {

        $clean = $str;
        $clean = strtolower(trim($clean, '-'));
        $clean = preg_replace("/[\/_|+. -]+/", $delimiter, $clean);

        return rawurlencode($clean);
    }


}
	 