<?php


class Isc_Parser_Helper_Media extends Isc_Parser_Helper_Abstract
{


    /**
     * Method to generate a ProductImageUrl
     * @param $image
     * @param $size
     * @return string
     */
    public function generateProductImageUrl($image, $size)
    {
        $imageUrl = Mage::helper('isc_parser/data')->getAssetUrl($image["ref"], $size);
        return $imageUrl;
    }


    /**
     * @param $xml
     * @return mixed
     */
    public function generateGalleryImageList($xml)
    {

       $galleryImageList =  $xml->images;
        return $galleryImageList;
    }

    /**
     * Method to generate the alt text of an image
     * @param $product
     * @param $image
     * @return string
     */
    public function getImageAltText($product, $image)
    {
        $altText = $product->getAttributeText('tpg') . " " . $product->getName() . " " . $image . " " . $image["sorting"];
        return $altText;
    }

    /**
     * Method to generate the alt text of an product image
     * @param $product
     * @param $image
     * @return string
     */
    public function getProductImageAltText($product)
    {
        $altText = $this->__("Productimage") . " " . $product->getAttributeText('tpg') . " " . $product->getName();
        return $altText;
    }

    /**
     * Method to generate the Image title (https://assets.einhell.com/im/imf/{size}/{asset_id}/IMAGETITLE)
     * @param $product
     * @param $image
     * @return string
     */
    public function getImageTitle($product, $image)
    {
        $srcText = $product->getAttributeText('tpg') . " " . $product->getName() . " " . $image . " " . $image["sorting"];
        $imageTitleText = Mage::helper('isc_parser/data')->encodeUrl($srcText) . '.jpg';
        return $imageTitleText;
    }


}
	 