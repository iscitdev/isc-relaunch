<?php

class Isc_Slider_Block_Slider extends Mage_Catalog_Block_Product_Abstract {

    /**
     *
     */
    public function getSliderProducts() {
        return Mage::getModel('isc_slider/products')->getAllSliderProducts();
    }


}