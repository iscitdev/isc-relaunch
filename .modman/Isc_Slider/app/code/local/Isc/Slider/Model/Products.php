<?php
class Isc_Slider_Model_Products extends Mage_Core_Model_Abstract {

    public function getAllSliderProducts() {
        $collection = Mage::getModel('catalog/product')->getCollection();

        $entity = 'catalog_product';
        $code = 'show_in_slider';
        $attr = Mage::getResourceModel('catalog/eav_attribute')->loadByCode($entity,$code);

        if ($attr->getId()) {
            $collection->addAttributeToSelect('show_in_slider');
            $collection->addFieldToFilter(array(
                array('attribute'=>'show_in_slider','eq'=>'1'),
            ));
        }

        return $collection;

    }
}