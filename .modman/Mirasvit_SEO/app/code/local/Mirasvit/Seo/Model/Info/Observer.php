<?php
/**
 * Mirasvit
 *
 * This source file is subject to the Mirasvit Software License, which is available at http://mirasvit.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Mirasvit
 * @package   Advanced SEO Suite
 * @version   1.3.0
 * @build     997
 * @copyright Copyright (C) 2015 Mirasvit (http://mirasvit.com/)
 */


class Mirasvit_Seo_Model_Info_Observer extends Varien_Object
{
    public function getConfig()
    {
        return Mage::getSingleton('seo/config');
    }

    public function appendInfo($observer) { //it will not work if the FPC or Varnish enabled
        if (!$this->getConfig()->isInfoEnabled(Mage::app()->getStore()->getStoreId())) {
            return;
        }

        if (strpos(Mage::helper('core/url')->getCurrentUrl(), 'checkout')
            || strpos(Mage::helper('core/url')->getCurrentUrl(), 'customer/account')) {
                return;
        }

        $canonical  = '';
        $actionName = Mage::helper('seo')->getFullActionCode();

        $metaTitle             = Mage::app()->getLayout()->getBlock('head')->getTitle();
        $metaKeywords          = Mage::app()->getLayout()->getBlock('head')->getKeywords();
        $metaDescription       = Mage::app()->getLayout()->getBlock('head')->getDescription();
        $robots                = Mage::app()->getLayout()->getBlock('head')->getRobots();
        $metaTitleLength       = Mage::helper('core/string')->strlen($metaTitle);
        $metaDescriptionLength = Mage::helper('core/string')->strlen($metaDescription);

        if (is_object($observer) && is_object($observer->getFront())) {
            $body = $observer->getFront()->getResponse()->getBody();
            preg_match('/rel="canonical"\s*href="(.*?)"\s*\/>/', $body, $canonicalArray);
            if (isset($canonicalArray[1])) {
                $canonical = $canonicalArray[1];
            }
        }

        $info = '
        <div class="seo-info">
            <div style="width: 370px;min-height: 115px;position: fixed;bottom: 10px;right: 10px;background: #47bbb3;color: #fff;z-index: 100000;font-family:Arial;">
                <h1 style="font-family:Arial;background: rgba(255, 255, 255, 0.1);color: #fff;padding: 3px 5px;font-size: 14px;font-weight: bold;text-align:center;">Advanced SEO Suite</h1>
                <div style="padding: 2px 5px 10px 5px;">
                    <h2 style="font-family:Arial;padding:0px;margin: 5px 0px 0px 0px;text-align: left;padding-left: 10px;font-size: 18px;font-weight: 400;color: rgba(255, 255, 255, 0.95);text-transform:none;"><b style="float:left;">Full Action Name:&nbsp;</b>
                        <div style="font-family:Arial;color: rgba(255, 255, 255, 0.7);font-size: 18px;text-align: left;">' . $actionName . '</div>
                    </h2>
                    <hr style="margin: 5px;border: 0;height: 0;border-top: 1px solid rgba(0, 0, 0, 0.1);border-bottom: 1px solid rgba(255, 255, 255, 0.3);"/>
                    <h2 style="font-family:Arial;padding:0px;margin: 5px 0px 0px 0px;text-align: left;padding-left: 10px;font-size: 18px;font-weight: 400;color: rgba(255, 255, 255, 0.95);text-transform:none;"><b style="float:left;">Robots Meta Header:&nbsp;</b>
                        <div style="font-family:Arial;color: rgba(255, 255, 255, 0.7);font-size: 18px;text-align: left;">' . $robots . '</div>
                    </h2>
                    <hr style="margin: 5px;border: 0;height: 0;border-top: 1px solid rgba(0, 0, 0, 0.1);border-bottom: 1px solid rgba(255, 255, 255, 0.3);"/>
                    <h2 style="font-family:Arial;padding:0px;margin: 5px 0px 0px 0px;text-align: left;padding-left: 10px;font-size: 18px;font-weight: 400;color: rgba(255, 255, 255, 0.95);text-transform:none;"><b style="float:left;">Canonical URL:&nbsp;</b>
                        <div style="font-family:Arial;color: rgba(255, 255, 255, 0.7);font-size: 18px;text-align: left;">' . $canonical . '</div>
                        <div style="font-family:Arial;color: #F0E68C;font-size: 18px;text-align: left;">' . $this->_getCanonicalInfoText($canonical) . '</div>
                    </h2>
                    <hr style="margin: 5px;border: 0;height: 0;border-top: 1px solid rgba(0, 0, 0, 0.1);border-bottom: 1px solid rgba(255, 255, 255, 0.3);"/>
                    <h2 style="font-family:Arial;padding:0px;margin: 5px 0px 0px 0px;text-align: left;padding-left: 10px;font-size: 18px;font-weight: 400;color: rgba(255, 255, 255, 0.95);text-transform:none;"><b style="float:left;">Meta Title:&nbsp;</b>
                        <div style="font-family:Arial;color: rgba(255, 255, 255, 0.7);font-size: 18px;text-align: left;">' . $metaTitle . '</div>
                        <div style="font-family:Arial;color: #F0E68C;font-size: 18px;text-align: left;">Length = ' . $metaTitleLength . $this->_getInfoText($metaTitleLength, false, false) . '</div>
                    </h2>
                    <hr style="margin: 5px;border: 0;height: 0;border-top: 1px solid rgba(0, 0, 0, 0.1);border-bottom: 1px solid rgba(255, 255, 255, 0.3);"/>
                    <h2 style="font-family:Arial;padding:0px;margin: 5px 0px 0px 0px;text-align: left;padding-left: 10px;font-size: 18px;font-weight: 400;color: rgba(255, 255, 255, 0.95);text-transform:none;"><b style="float:left;">Meta Keywords:&nbsp;</b>
                        <div style="font-family:Arial;color: rgba(255, 255, 255, 0.7);font-size: 18px;text-align: left;">' . $this->_getInfoText(false, $metaKeywords, false) . '</div>
                    </h2>
                    <hr style="margin: 5px;border: 0;height: 0;border-top: 1px solid rgba(0, 0, 0, 0.1);border-bottom: 1px solid rgba(255, 255, 255, 0.3);"/>
                    <h2 style="font-family:Arial;padding:0px;margin: 5px 0px 0px 0px;text-align: left;padding-left: 10px;font-size: 18px;font-weight: 400;color: rgba(255, 255, 255, 0.95);text-transform:none;"><b style="float:left;">Meta Description:&nbsp;</b>
                        <div style="font-family:Arial;color: rgba(255, 255, 255, 0.7);font-size: 18px;text-align: left;">' . $metaDescription . '</div>
                        <div style="font-family:Arial;color: #F0E68C;font-size: 18px;text-align: left;">Length = ' . $metaDescriptionLength . $this->_getInfoText(false, false, $metaDescriptionLength);'</div>
                    </h2>
                </div>
            </div>
        </div>';

        echo $info;
    }

    protected function _getInfoText($metaTitleLength, $metaKeywords, $metaDescriptionLength) {
        $length = false;

        if ($metaKeywords !== false && !$metaKeywords) {
            return ' <div style="font-family:Arial;color: #F0E68C;font-size: 18px;text-align: left;">(incorrect) Meta Keywords have to be added.</div>';
        } elseif($metaKeywords !== false) {
            return $metaKeywords;
        }

        if ($metaTitleLength && $metaTitleLength > Mirasvit_Seo_Model_Config::META_TITLE_MAX_LENGTH) {
            $length = Mirasvit_Seo_Model_Config::META_TITLE_MAX_LENGTH;
        }

        if ($metaDescriptionLength && $metaDescriptionLength > Mirasvit_Seo_Model_Config::META_DESCRIPTION_MAX_LENGTH) {
            $length = Mirasvit_Seo_Model_Config::META_DESCRIPTION_MAX_LENGTH;
        }

        if ($length) {
            return ' Recommended length up to ' . $length . ' characters. Can be configured in SEO Extended Settings.';
        }

        if ($metaTitleLength === 0) {
            return ' (incorrect) Meta Title have to be added.';
        }

        if ($metaDescriptionLength === 0) {
            return ' (incorrect) Meta Description have to be added.';
        }

        return ' (correct)';
    }

    protected function _getCanonicalInfoText($canonical) {
        if (($canonicalUrl = Mage::helper('seo')->getCanonicalUrl())
            && $canonicalUrl != $canonical) {
                return ' Canonical created not using Mirasvit SEO extension.';
        }
        if ($canonical) {
            return ' (correct)';
        }

        return ' Missing canonical URL.';
    }

}