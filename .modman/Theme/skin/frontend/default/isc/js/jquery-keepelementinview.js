$.fn.keepElementInView = function()
{
    var elemPosTop = this.position().top;
    $(window).scroll(function()
    {
        var wintop = $(window).scrollTop();
        if (wintop > elemPosTop)
        {
            this.css({ "position":"fixed", "top":"10px" });
        }
        else
        {
            this.css({ "position":"inherit" });
        }
    });
    return this;
};