$.noConflict();

var parent_product_sku_selector = '#parent_product_sku';

// custom jquery selector: case insensitive contains
jQuery.expr[":"].containsIgnoreCase = jQuery.expr.createPseudo(function (arg) {
    return function (elem) {
        return jQuery(elem).text().toUpperCase().indexOf(arg.toUpperCase()) >= 0;
    };
});

function _initServiceline() {
    var el = jQuery('#serviceline');
    var content = el.find('.serviceline-inner').hide(); //.hide();
    el.find('#serviceline-handle').click(function () {
        if (jQuery(this).hasClass('open')) {
            jQuery(this).removeClass('open');
            content.slideUp();
        } else {
            jQuery(this).addClass('open');
            content.slideDown();
        }
    });
}

function _initProductSlider() {
    var activeClass = 'open';
    var contentElement = jQuery('#product-slider-content');
    jQuery('#product-slider-handle').on('click', function () {
        if (contentElement.hasClass(activeClass)) {
            contentElement.removeClass(activeClass);
        } else {
            contentElement.addClass(activeClass);
        }
    });
}

function addSliderPopover(slider, el, val) {
    slider.append('<div class="slider-popover" style="' + el.attr('style') + '">' +
    '<div class="inner">' + val + '</div><div class="arrow">&nbsp;</div></div>');
}

function updateSliderPopover(slider) {
    slider.children('.slider-popover').remove();
    addSliderPopover(slider, slider.children('.ui-slider-handle').first(), jQuery("#year-from").val());
    addSliderPopover(slider, slider.children('.ui-slider-handle').last(), jQuery("#year-to").val());
}

function showHideGripEmpty(show) {
    var $el = jQuery('#grid-empty');
    (show) ? $el.show() : $el.hide();
}

/**
 * Method to show or hide the toolsbars, if no product for the input exists
 * @param show
 */
function showToolbarGridEmpty(show){
    var $el = jQuery('.toolbar');
    if (show){
        $el.addClass("hidden")
    }
    else{
        $el.removeClass("hidden")
    }

}

function filterProductGrid($container, filter) {
    $container.isotope({
        filter: filter
    }, function ($items) {
        showHideGripEmpty($items.length < 1);
        showToolbarGridEmpty($items.length < 1)
    })
}

function _initProductGridIsotope() {
    var $productsContainer = jQuery('.category-products');
    $productsContainer.isotope({
        itemSelector: '.span6',
        layoutMode: 'fitRows'
    });

    // enable grid filter
    var $filterInput = jQuery('#grid-filter');
    var $resetButton = $filterInput.siblings('.reset-button');
    $filterInput.keyup(function () {
        var filterVal = jQuery(this).val();
        if (filterVal == '') {
            var filter = '*';
            showHideGripEmpty(false); // hide empty notice immediately
            $resetButton.addClass('hidden');
        } else {
            $resetButton.removeClass('hidden');
            var filter = ":containsIgnoreCase('" + filterVal + "')";
        }
        filterProductGrid($productsContainer, filter);
    });
    // bind reset button
    $filterInput.siblings('.reset-button').on('click', function () {
        $filterInput.val('').trigger('keyup').focus();
    });
    // hide reset button on init
    $resetButton.addClass('hidden');

    // filter on page load
    if ($filterInput.val() != '') {
        filterProductGrid($productsContainer, ":containsIgnoreCase('" + $filterInput.val() + "')");
    }
}

function _initProductionYearSlider() {
    // init required slider values
    var $productionYearSlider = jQuery("#year-range");
    var sliderFrom = jQuery("#year-from");
    var sliderTo = jQuery("#year-to");
    var sliderFromVal = parseInt(sliderFrom.val());
    var sliderToVal = parseInt(sliderTo.val());

    if ((sliderFromVal == 3000 || sliderToVal == 0) || sliderFromVal == sliderToVal) {
        jQuery('#year-range-wrap').remove();
        return;
    }

    // keep backup of init values to check for reset
    var yearFromInit = sliderFromVal;
    var yearToInit = sliderToVal;

    // init slider
    $productionYearSlider.slider({
        range: true,
        min: sliderFromVal,
        max: sliderToVal,
        values: [sliderFromVal, sliderToVal],
        slide: function (event, ui) {
            sliderFrom.val(ui.values[0]);
            sliderTo.val(ui.values[1]);
        }
    });

    // draw popovers
    updateSliderPopover($productionYearSlider);

    // handle popover redraw on attribute change (using 3rd-party extension)
    // (events of slider are fired before complete ui is updated
    // therefore unusable to redraw popovers)
    $productionYearSlider.children('.ui-slider-handle').attrchange({
        callback: function (event) {
            if (event.attributeName == 'style') {
                updateSliderPopover($productionYearSlider);
            }
        }
    });

    // bind slider to isotope filtering
    $productionYearSlider.on('slidechange', function () {
        // update isotope product grid
        var selectorPrefix = 'year-';
        var yearFrom = jQuery('#year-from').val();
        var yearTo = jQuery('#year-to').val();
        var selector = '';
        if (yearFrom == yearFromInit && yearTo == yearToInit) {
            selector = '*';
        } else {
            for (var year = yearFrom; year <= yearTo; year++) {
                if (selector != '') {
                    selector += ',';
                } // dividier between multiple values
                selector += '.' + selectorPrefix + year;
            }
        }
        // bind filter to products grid
        jQuery('#category-products').isotope({filter: selector});
    });
}

function _initDropdowns() {
    jQuery('.dropdown-toggle').dropdown();
}

function _initManufacturerSelection() {

    var $container = jQuery('#manufacturer-list, #manufacturer-grid');

    $container.isotope({
        // options
        itemSelector: '.item',
        layoutMode: 'fitRows'
    });

    jQuery('#manufacturer-filter').find('ul a').on('click', function () {
        jQuery(this).parent().addClass('active').siblings('.active').removeClass('active');
        var selector = jQuery(this).data('filter').toLowerCase();
        $container.isotope({filter: selector});
        return false;
    });
    jQuery('#manufacturer-all').on('click', function () {
        $container.isotope({filter: '*'});
        return false;
    });

    jQuery('#manufacturer-filter').find('ul a').each(function () {
        var filterClass = jQuery(this).data('filter');
        if (jQuery('#manufacturer-grid, #manufacturer-list').children(filterClass.toLowerCase()).length < 1) {
            jQuery(this).css('opacity', '0.2');
        }
    });
}

function Popover() {
    this.offset = {top: 20, left: 50};
    this.template = jQuery('#product-popover-template');
    this.item_id = '#product-popover';
    this.wrap = jQuery('#spareparts-exploded');
    this.init = function (event, element) {
        event.stopPropagation();
        // remove existing popovers
        this.template.addClass("hidden");
        this.create(element);
        this.position(event.pageX, event.pageY);

        this.bindRemover();
    },
        this.remove = function () {
            jQuery(this.item_id).remove();
        },
        this.create = function (el) {

            var $listEl = jQuery('#item-' + el.data('sku'));
            this.template.find('.sp-table .sp-row-alt').addClass("hidden");
            if ($listEl.data('component_url')) {
                this.template.find('.header').html(decodeURIComponent('<a class="component" href="' + $listEl.data('component_url') + '">' + $listEl.data('name') + '<i class="icon-share"></i></a>'));
            }
            else {
                this.template.find('.header').html(decodeURIComponent($listEl.data('name')));
            }
            this.template.find('.position-val').html(el.data('position'));
            this.template.find('.sp-row .sku').html($listEl.data('sku'));
            this.template.find('.sp-row .stock').html($listEl.find('.stock').html());
            this.template.find('.sp-row .stock .out-of-stock-link').on('click',function(){
                 jQuery.fancybox.open(jQuery(jQuery(this).attr('href')));
            });
            this.template.find('.sp-row .price').html($listEl.data('formatted_price'));
            this.template.find('input[name="product"]').val($listEl.data('productId'));
            this.template.find('input[name="qty"]').val(1);
            this.template.find('.sp-table .qty').removeClass("hidden");
            this.template.find('.sp-table .cart').removeClass("hidden");
            this.template.find('.sp-table .cart-submit').removeClass("icon-list-alt");
            if ($listEl.data('customer-group') > 10 || $listEl.data('in-stock') == 0) {
                this.template.find('.sp-table .cart-submit').addClass("icon-list-alt");
            }
            if ($listEl.data('img') != "") {
                this.template.find('.popover-image').addClass("span3").html('<img src="' + $listEl.data('img') + '" alt="Image ' + decodeURIComponent($listEl.data('name')) + '" />');
                this.template.css("min-width", "850px");
            }
            else {
                this.template.find('.popover-image').removeClass("span3").html("");
                this.template.css("min-width", "");
            }
            if ($listEl.data('salable') == false) {
                this.template.find('.sp-table .qty').addClass("hidden");
                this.template.find('.sp-table .cart').addClass("hidden");
            }
            if ($listEl.data('has_alternative')) {
                var $altEl = jQuery('#item-' + $listEl.data('has_alternative'));
                this.template.find('.sp-table .sp-row-alt').removeClass("hidden");
                this.template.find('.sp-row-alt .sku').html($altEl.data('sku'));
                this.template.find('.sp-row-alt  .stock').html($altEl.data('stockLabel'));
                this.template.find('.sp-row-alt  .stock').removeClass("out-of-stock");
                this.template.find('.sp-row-alt  .stock').removeClass("in-stock");
                this.template.find('.sp-row-alt  .stock').addClass($altEl.data('stockClass'));
                this.template.find('.sp-row-alt  .price').html($altEl.data('formatted_price'));
                this.template.find('input[name="product"]').val($altEl.data('productId'));
                this.template.find('input[name="qty"]').val(1);
                this.template.find('.sp-table .sp-row-alt .qty').removeClass("hidden");
                this.template.find('.sp-table .sp-row-alt .cart').removeClass("hidden");
                this.template.find('.sp-table .qty-label').removeClass("hidden");
                this.template.find('.sp-table .cart-label').removeClass("hidden");
                if ($altEl.data('stockClass') == 'out-of-stock') {
                    this.template.find('.sp-table .sp-row-alt .qty').addClass("hidden");
                    this.template.find('.sp-table .sp-row-alt .cart').addClass("hidden");
                }
                this.template.find('.sp-table .sp-row-alt .cart-submit').removeClass("icon-list-alt");
                if ($altEl.data('customer-group') > 10) {
                    this.template.find('.sp-table .sp-row-alt .cart-submit').addClass("icon-list-alt");
                }
            }
            this.template.removeClass("hidden");

            bindPopoverAjaxAddToCart(this.item_id, this);
        },
        this.position = function (x, y) {
            this.template.find('.product-popover').removeClass('shift shift330 shift430');
            this.template.css('top', y + this.offset.top)
                .css('left', x - this.offset.left);
            var rightEnd = x - this.offset.left + this.template.width();
            if (rightEnd > window.innerWidth) {
                var classShift = rightEnd - window.innerWidth;
                console.log(classShift);
                var shift_class = "shift"
                var shift_by = 180;
                if (classShift > 160) {
                    shift_class = "shift330";
                    shift_by = 280;
                }
                if (classShift > 260) {
                    shift_class = "shift430";
                    shift_by = 380;
                }
                this.template.css('left', x - this.offset.left - shift_by);
                this.template.find('.product-popover').addClass(shift_class);
            }
        },
        this.bindRemover = function () {
            var that = this;
            jQuery(this.item_id).on('click', function (e) {
                e.stopPropagation();
            })
            jQuery(this.item_id).find('.popover-close').on('click', function () {
                that.template.addClass("hidden");
                jQuery('body').unbind("click");
            });
            jQuery('body').on('click', function () {
                that.template.addClass("hidden");
                jQuery('body').unbind("click");
            })
        },
        this.remove = function () {
            this.template.addClass("hidden");
            this.template.find('.sp-table .sp-row-alt').addClass("hidden");
        }
}

function Zoomer() {
    this.handle,
        this.preview,
        this.view,
        this.zoomLevels = [100, 130, 150, 200, 300],
        this.currentZoomLevel = 0,
        this.init = function () {
            // set init values
            this.handle = jQuery("#ed-handle");
            this.preview = jQuery('#ed-preview');
            this.previewWidth = this.preview.width();
            this.previewHeight = this.preview.height();
            this.view = jQuery('#ed-view');
            this.img = jQuery('#ed-img');
            this.imgSrc = this.img.attr('src');
            this.imgSrcHighres = this.img.data('highres');
            this.imgWidth = this.img.width();
            this.imgHeight = this.img.height();
            this.zoomInfo = jQuery('#ed-zoom-info');

            this.view.height(this.img.height());

            console.log('init zoomer');

            // init controls
            this.initControls();

            // draw parts
            this.updateParts();

            this.bindParts();
        }
    this.initControls = function () {
        this.initHandle();
        this.initZoomHandles();
    },
        this.initHandle = function () {
            var that = this;
            this.handle.draggable({
                containment: "parent",
                drag: function (event, ui) {
                    that.positionImage(ui.position);
                }
            });
            this.updateHandle(this.preview.width(), this.preview.height());
        }
    this.updateHandle = function (width, height) {
        this.handle.width(width);
        this.handle.height(height);
    }
    this.initZoomHandles = function () {
        var self = this;
        jQuery('#ed-zoom-plus').on('click', function () {
            self.increaseZoom()
        });
        jQuery('#ed-zoom-minus').on('click', function () {
            self.decreaseZoom()
        });
    }
    this.checkOverlap = function () {
        // check if handle "moves out" of preview

        var updateImagePosition = false;

        var left = parseInt(this.handle.css('left'));
        var top = parseInt(this.handle.css('top'));

        if (this.handle.width() + left > this.preview.width()) {
            var offsetLeft = this.preview.width() - this.handle.width();
            this.handle.css('left', offsetLeft);
            updateImagePosition = true;
        }
        if (this.handle.height() + top > this.preview.height()) {
            var offsetTop = this.preview.height() - this.handle.height();
            this.handle.css('top', offsetTop);
            updateImagePosition = true;
        }

        if (updateImagePosition) {
            this.positionImage({top: offsetTop, left: offsetLeft});
        }
    }
    this.increaseZoom = function () {
        if (this.zoomLevels[this.currentZoomLevel + 1] !== undefined) {
            this.currentZoomLevel++;
            this.updateView();
        } else {
            console.log('max zoom level reached, disable control');
        }
    }
    this.decreaseZoom = function () {
        if (this.zoomLevels[this.currentZoomLevel - 1] !== undefined) {
            this.currentZoomLevel--;
            this.updateView();

            this.checkOverlap();
        } else {
            console.log('min zoom level reached, disable control');
        }
    }
    this.updateView = function () {
        // calculate preview size from initial values
        console.log('update view');
        console.log('current zoom level: ' + this.currentZoomLevel + " [" + this.zoomLevels[this.currentZoomLevel] + "]");

        var zoomFactor = this.zoomLevels[this.currentZoomLevel];
        // update zoom info
        this.zoomInfo.html(zoomFactor + "%");

        if (this.currentZoomLevel > 0) {
            // use highres version of zoomed
            this.img.attr('src', this.imgSrcHighres);
        } else {
            this.img.attr('src', this.imgSrc);
        }

        var handleWidth = Math.round(this.previewWidth / (zoomFactor / 100));
        var handleHeight = Math.round(this.previewHeight / (zoomFactor / 100));
        this.updateHandle(handleWidth, handleHeight);

        var imgWidth = Math.round((this.imgWidth / 100) * zoomFactor);
        var imgHeight = Math.round((this.imgHeight / 100) * zoomFactor);
        this.updateImage(imgWidth, imgHeight);

        this.updateParts();

        this.positionImage();
    }
    this.updateImage = function (width, height) {
        this.img.width(width);
        this.img.height(height);
    }
    this.positionImage = function (position) {
        console.log("position big image");

        if (typeof position == "undefined") {
            position = {
                top: parseInt(this.handle.css('top'), 10),
                left: parseInt(this.handle.css('left'), 10)
            };
        }

        // get percentual offset
        var offsetTop = (position.top / this.previewHeight) * 100;
        var offsetLeft = (position.left / this.previewWidth) * 100;

        var marginTop = ((this.img.height() / 100) * offsetTop) * -1;
        var marginLeft = ((this.img.width() / 100) * offsetLeft) * -1;

        this.img.css('margin-top', marginTop).css('margin-left', marginLeft);

        this.positionParts(marginLeft, marginTop);
    }
    this.positionParts = function (marginLeft, marginTop) {
        jQuery('#ed-parts').css('margin-top', marginTop).css('margin-left', marginLeft);
    }
    this.setParts = function (parts) {
        // add parts to explosion drawing
        this.parts = jQuery.parseJSON(parts);

        var that = this;
        jQuery.each(this.parts, function (index, value) {
            if (jQuery('#item-' + value.sku).length < 1) {
                delete that.parts[index];
                console.log(value.sku + " not found in list. removed from explosion drawing!");
            }
        });
    }
    this.updateParts = function () {
        // draw parts on explosion drawing

        var zoomFactor = this.zoomLevels[this.currentZoomLevel];

        // reset parts
        this.view.find('.inner').html('');

        for (var key in this.parts) {
            var p = this.parts[key];

            var top = p.tlY;
            var left = p.tlX;

            var width = p.brX - p.tlX;
            var height = p.brY - p.tlY;

            top = Math.round((top / 100) * zoomFactor);
            left = Math.round((left / 100) * zoomFactor);

            width = Math.round((width / 100) * zoomFactor);
            height = Math.round((height / 100) * zoomFactor);

            var current_item = jQuery('#item-' + p.sku);
            var in_cart = current_item.data('in-cart') == 1 ? 'in-cart' : '';
            var customer_group = current_item.data("customer-group");

            this.view.find('.inner').append(
                '<div class="p ' + in_cart + '" data-sku="' + p.sku + '"' + ' data-position="' + p.pos + '" ' +
                'style="top: ' + top + 'px; left: ' + left + 'px; width: ' + width + 'px; height: ' + height + 'px">&nbsp;</div>'
            )
        }
    }
    this.bindParts = function () {
        this.view.on('click', 'div.p', function (e) {
            var popover = new Popover();
            popover.init(e, jQuery(this));
        })
    }
}


function _initExplosionZoomer() {
    // remove loading class from ed-view
    jQuery('#ed-view').removeClass('loading');

    var zoomer = new Zoomer();

    zoomer.setParts(partsList);
    zoomer.init();
    initHighlight();
}

function initHighlight() {
    var highlight = getParameterByName('highlight');
    if (highlight) {
        var elem = jQuery('#ed-parts').find('*[data-sku="' + highlight + '"]');
        if (elem && elem.length !== 0) {
            var event = new jQuery.Event("click");
            event.pageX = elem.offset().left + 20;
            event.pageY = elem.offset().top + 20;
            elem.trigger(event);
        }
    }
}

function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

function _initIscProductSlider(){
    bindAjaxAddToCart(jQuery('#productslider').find('input.cart-submit'));

}


function _initProductDetailPage() {
    // activate first tab (spare parts)
    jQuery('#product-tabs').find('li:first-child a').tab('show');

    // handle spare parts tabs (exploded drawing + list view)
    jQuery('#spartparts-view-select').children('li').on('click', function () {
        jQuery(this).addClass('active').siblings().removeClass('active');
        jQuery('#' + jQuery(this).data('show')).addClass('active').siblings().removeClass('active');
    });

    jQuery('#spartparts-view-select').children(':visible:first').trigger('click');

    bindAjaxAddToCart(jQuery('#spareparts-list').find('input.cart-submit'));
    bindAjaxAddToCart(jQuery('#accessories').find('input.cart-submit'));
    bindAjaxAddToCart(jQuery('#spareparts_view').find('input.cart-submit'));
    bindAjaxAddToCart(jQuery('#accessories_tabbed').find('input.cart-submit'));
    bindAjaxAddToCart(jQuery('#accessory_view').find('input.cart-submit'));
    bindAjaxAddToCart(jQuery('#main-product').find('input.cart-submit'));
    jQuery('.out-of-stock-link').fancybox({
        'centerOnScroll': true,
        helpers: {
            title: null
        }
    });
}

var cartOverlayId = 'cart-overlay';
function initCartOverlay() {
    jQuery('#' + cartOverlayId).remove();
    jQuery('body').append('<div id="' + cartOverlayId + '"><div class="loading">&nbsp;</div></div>')
    var $overlay = jQuery("#" + cartOverlayId);
    $overlay.on("click", ".btn-close", function () {
        $overlay.remove();
    });
}

function updateCartOverlay(data) {
    var $overlay = jQuery('#' + cartOverlayId);
    $overlay.html(''); // reset
    $overlay.append('<div class="header">' + data.header + '</div>');
    $overlay.append('<div class="message">' + data.message + '</div>');
    if (data.item_message) {
        $overlay.append('<div class="item-message">' + data.item_message + '</div>');
    }

    var buttons = '<div class="btn-wrap">';
    if (data.success) { // only show cart button if success
        buttons += '<div class="btn btn-small"><a href="' + data.cart_url + '">' + data.cart_label + '</a></div>';
    }

    buttons += '<div class="btn btn-small btn-close">' + data.close_label + '</div>';

    $overlay.append(buttons + '</div>');

    // update cart count
    if (data.cart_count > 0) {
        jQuery('#cart-count').html('(' + data.cart_count + ')');
    }

    // add in-cart class to explosion drawing item and list item
    if (data.success && data.sku) {
        jQuery('#ed-parts').find("[data-sku='" + data.sku + "']").addClass('in-cart');
        jQuery('#spareparts-list').find("#item-" + data.sku + "").addClass('in-cart');
    }
}

function bindPopoverAjaxAddToCart(id, popover) {
    jQuery(id).find('input.cart-submit').unbind('click').on('click', function (e) {
        popover.remove();
        initCartOverlay();
        e.preventDefault();
        var $form = jQuery(this).parents('form');
        var postUrl = $form.data('url');
        var parent_product_sku = jQuery(parent_product_sku_selector).data('sku');
        jQuery.ajax({
            type: 'POST',
            url: postUrl,
            dataType: 'json',
            data: $form.serialize() + '&parent_product_sku=' + parent_product_sku,
            success: function (data) {
                updateCartOverlay(data);
            }
        });
    });
}

function bindAjaxAddToCart(elements) {
    jQuery(elements).on('click', function (e) {
        initCartOverlay();
        e.preventDefault();
        var $form = jQuery(this).parents('form');
        var postUrl = $form.data('url');
        var parent_product_sku = jQuery(parent_product_sku_selector).data('sku');
        jQuery.ajax({
            type: 'POST',
            url: postUrl,
            dataType: 'json',
            data: $form.serialize() + '&parent_product_sku=' + parent_product_sku,
            success: function (data) {
                updateCartOverlay(data);
            }
        });
    });
}

function getMaxHeight($elements) {
    return Math.max.apply(null, $elements.map(function () {
        return jQuery(this).height();
    }).get());
}

function _initProductList() {
    jQuery('#category-products').find('.row').each(function () {
        var maxHeight = getMaxHeight(jQuery(this).find('.span3 .item .name'));
        jQuery(this).find('.span3 .item .name').css('minHeight', maxHeight);
    });
}

function _initStyle() {
    // style default submit buttons
    jQuery('button').each(function () {
        if (jQuery(this).attr('type') == 'submit' && !jQuery(this).hasClass('search-button')) {
            jQuery(this).addClass('btn btn-submit');
        }
        if (jQuery(this).hasClass('button')) {
            jQuery(this).addClass('btn');
        }
    })
    jQuery('#review-buttons-container').addClass('clearfix');
}

////////////////////////////////////
// SERVICE CENTER JS
////////////////////////////////////

var TYPE_ERROR_MESSAGE = 0;
var TYPE_SUCCESS_MESSAGE = 1;

/**
 * Inits the js functionality for service center
 * Hides the not active steps
 * Initializes the click listener
 * Init js for product identification
 * @private
 */
function _initServicecenter() {
    //Accordion
    //Initialize accordion and hide the not active containers
    //jQuery(".step").not(".step-active").hide();

    jQuery('input[name="retour-method"]').each(function() {
       if(jQuery(this).is(':checked')) {
          jQuery(this).parent().addClass('active');
       }
    });

    //Click listener for buttons which goes to next step
    jQuery(document).on("click", ".btn-identity-submit", function (e) {
        e.preventDefault();
        validateProductIdentity(this);
    });

    //Show info container
    jQuery(document).on('click', '#choose-methods .info-icon', function () {
        var name = jQuery('div.' + jQuery(this).attr('class').split(" ")[0]);
        if (name.siblings(':visible').length > 0) {
            name.slideToggle();
        }
    });

    //Show retour method info container
    jQuery(document).on("click", 'input[name="retour-method"]', function () {
        if (jQuery(this).val() == 1) {
            jQuery('#serial-number').parent().addClass('active');
            jQuery('#article-identity-number').parent().removeClass('active');
            jQuery('#serial-number-container').slideDown("fast");
            jQuery('#article-number-container').slideUp("fast");
            jQuery('div.info-article-number').slideUp("fast");
            jQuery('div.info-serial-number').slideDown("fast");
        } else {
            jQuery('#serial-number').parent().removeClass('active');
            jQuery('#article-identity-number').parent().addClass('active');
            jQuery('#serial-number-container').slideUp("fast");
            jQuery('#article-number-container').slideDown("fast");
            jQuery('div.info-serial-number').slideUp("fast");
            jQuery('div.info-article-number').slideDown("fast");
        }
    });
}

function validateProductIdentity(element) {
    var validator;
    jQuery('#article-number').val(function (index, value) {
        return value.replace(/\./g, '');
    });
    if (!validator) {
        validator = new Validation($('servicecenter-validation'));
    }
    if (validator && validator.validate()) {
        jQuery('.step-active img').show();
        removeMessages();
        checkProductIdentity(element);
    }
}

function saveFailureDescription(element) {
    var $form = jQuery("#form_failuredescription");
    /*jQuery('.step-active img').show();
    jQuery(element).prop("disabled", true);*/
    jQuery('body').append('<div id="loadingOverlay"><div class="loading">&nbsp;</div></div>');
    jQuery.ajax({
        type: 'POST',
        url: $form.attr("action"),
        data: $form.serialize(),
        success: function (data) {
            var response = JSON.parse(data);
            activateStep(response.content);
            jQuery('#loadingOverlay').remove();
        }
    });
}

function checkProductIdentity(element) {
    //Get selected retour method to identify the product
    var retourMethod = jQuery('input[name="retour-method"]:checked').val();
    var data = (retourMethod == 1) ? {serial: jQuery('#serial-number-input').val()} : {
        article: jQuery('#article-number').val(),
        identify: jQuery('#identify-number').val()
    };
    jQuery(element).prop("disabled", true);
    jQuery('body').append('<div id="loadingOverlay"><div class="loading">&nbsp;</div></div>');
    jQuery.ajax({
        type: "POST",
        url: urlCheckIdentity,
        data: data
    })
        .done(function (msg) {
            var response = JSON.parse(msg);
            if (response.status == 1) {
                activateStep(response.content);
            } else {
                jQuery('.step-active img').hide();
                window.location = response.url;
            }
            jQuery(element).prop("disabled", false);
            jQuery('#loadingOverlay').remove()
        });
}


function activateStep(content) {
    jQuery("#servicecenter.servicecenter-page").replaceWith(content);
    _initStyle();
}

function showMessage(messages, type) {
    var messageContainer = jQuery(' ul.messages');
    messageContainer.append(addMessage(messages, type));
    if (type == TYPE_SUCCESS_MESSAGE) {
        messageContainer.parent().show();
        //.delay(2000).slideUp(300, function () {
        //    messageContainer.children(":first").remove();
        //});

    } else {
        messageContainer.parent().slideDown(200);
    }

}

function removeMessages() {
    var messageContainer = jQuery(' ul.messages li').remove();
}

/**
 * Adds a message to the step
 * @param messages json array with messages
 * @param type the type of the message (TYPE_SUCCESS_MESSAGE, TYPE_ERROR_MESSAGE)
 */
function addMessage(messages, type) {

    var message = ''
    if (type == TYPE_SUCCESS_MESSAGE) {
        message = '<li class="success-msg">';
    } else if (type == TYPE_ERROR_MESSAGE) {
        message = '<li class="error-msg">';
    }
    message += '<ul>';
    jQuery.each(messages, function (i, item) {
        message += '<li><span>' + Translator.translate(item) + '</span></li>';
    });

    message += '</ul></li>';

    return message;
}

////////////////////////////////////
// END SERVICE CENTER JS
////////////////////////////////////

function Search() {
    this.inputField,
        this.results,
        this.dropdown,
        this.resetButton,
        this.inputTimeout = {}, // timeout object for input delay
        this.inputDelay = 300, // input delay before request to server
        this.string = {},
        this.resultRows = 6,
        this.lastSearchVal = '',
        this.init = function () {
            this.inputField = jQuery('#search');
            this.results = jQuery('#search-results');
            this.dropdown = jQuery('#search-autocomplete');
            this.resetButton = jQuery('#search_mini_form .reset-button');

            this.search_ajax_url = search_ajax_url; // set in footer phtml
            this.search_page_url = search_page_url; // footer phtml
            this.strings = search_strings; // footer phtml

            this.dropdown.hide();
            this.resetButton.hide();
            this.bindInputField();
            this.bindResetButton();
        },
        this.bindResetButton = function () {
            var that = this;
            this.resetButton.on('click', function (e) {
                e.preventDefault(); // prevent submit
                that.inputField.val('').trigger('keyup');
                that.removeSearchLayer();
            })
        },
        this.removeSearchLayer = function () {
            this.dropdown.remove();
            this.resetButton.remove();
        },
        this.bindInputField = function () {
            var that = this;
            this.inputField.on('keyup', function () {
                if (typeof that.inputTimeout !== 'undefined' && that.inputTimeout.timeout) {
                    clearTimeout(that.inputTimeout.timeout);
                }
                that.inputTimeout.timeout = setTimeout(function () {
                    if (that.inputField.val() != that.lastSearchVal) {
                        that.lastSearchVal = that.inputField.val();
                        if (that.lastSearchVal.length >= searchInputMinCharCount) {
                            if (that.lastSearchVal.length >= searchInputMinCharCount) {
                                that.setResultsLoading();
                                that.doRequest();
                            }
                        }
                    }
                }, that.inputDelay);
            });
        },
        this.setResultsLoading = function () {
            this.results.html('');
            this.results.addClass('loading');
        },
        this.doRequest = function () {
            var $queryVal = this.inputField.val();

            if ($queryVal == '') {
                this.resetButton.hide();
                this.dropdown.hide();
                return;
            } else {
                this.resetButton.show();
                this.dropdown.show();
            }

            var that = this;
            jQuery.ajax({
                url: that.search_ajax_url,
                data: {q: $queryVal, rows: that.resultRows},
                dataType: 'json'
            }).success(function (data) {
                that.updateResults(data);
            });
        },
        this.renderResults = function (data) {
            var output = '';

            // header
            output += '<div class="top clearfix">';
            output += '<span class="search-for">' + this.strings.search_for + " '" + this.inputField.val() + '\'</span>';
            output += '<span class="result-count">' + ( data.meta.numFound ? data.meta.numFound : 0 ) + " " + this.strings.results + '</span>';
            output += '</div>';

            if (data.meta.numFound == 0) { // no results
                return output + '<div class="no-results">' + this.strings.no_results + '</div>';
            }

            output += "<ul>";
            jQuery(data.items).each(function (index, item) {
                output += '<li class="clearfix">';
                output += '<a href="' + item.url + ( item.sparepart ? ("?highlight=" + item.sparepart) : "" ) + '">';
                if (item.tiny_image_url) {
                    output += '<img src="' + item.tiny_image_url + '" />';
                }
                output += '<span class="name">' + item.name + '</span>';
                output += '<span class="sku">' + item.sku + '</span>';
                output += '</a>';
                output += '</li>';
            });
            output += "</ul>";

            // footer
            output += '<div class="bottom">';
            output += '<div class="footer"><a href="' + this.getSearchPageUrl() + '">' + this.strings.show_more_results + '</a></div>';
            output += '</div>';

            return output;
        },
        this.getSearchPageUrl = function () {
            return this.search_page_url + '?q=' + this.inputField.val();
        },
        this.updateResults = function (data) {
            var output = this.renderResults(data);

            this.results.removeClass('loading');
            this.results.html(output);
        }
}

function SearchPage() {
    this.solrsearch = jQuery('#solrsearch'),
        this.inputField = jQuery('#search-page-input'),
        this.results = jQuery('#search-page-results'),
        this.pageTitle = jQuery('#search-page-title'),
        this.toolbarContainer = jQuery('#solrsearch .search-toolbar'),
        this.resultsContainer = jQuery('.search-page-results-container'),

        this.resetButton,
        this.inputTimeout = {}, // timeout object for input delay
        this.inputDelay = 200, // input delay before request to server
        this.string = {},
        this.lastSearchVal = '',
        this.currentPage = 1,
        this.resultRows = 30,
        this.init = function () {
            this.search_ajax_url = search_ajax_url; // set in footer phtml
            this.strings = search_strings; // footer phtml

            this.bindInputField();
            this.checkValue();
            this.bindToolbar();
        },
        this.checkValue = function () {
            if (this.inputField.val() != '') {
                this.inputField.trigger('keyup');
            } else {
                this.resetSearch();
            }
        },
        this.bindResetButton = function () {
            var that = this;
            this.resetButton.on('click', function (e) {
                e.preventDefault(); // prevent submit
                that.inputField.val('').trigger('keyup');
            })
        },
        this.bindInputField = function () {
            var that = this;
            this.inputField.on('keyup', function () {
                if (typeof that.inputTimeout !== 'undefined' && that.inputTimeout.timeout) {
                    clearTimeout(that.inputTimeout.timeout);
                }
                that.inputTimeout.timeout = setTimeout(function () {
                    if (that.inputField.val() != that.lastSearchVal) {
                        that.lastSearchVal = that.inputField.val();

                        that.doRequest();
                    }
                }, that.inputDelay);
            });
        },
        this.bindToolbar = function () {
            var that = this;
            this.solrsearch.on('click', 'a.next-page', function (e) {
                e.preventDefault();
                that.currentPage++;
                that.doRequest();
            })
            this.solrsearch.on('click', 'a.prev-page', function (e) {
                e.preventDefault();
                if (that.currentPage > 1) {
                    that.currentPage--;
                    that.doRequest();
                }
            })
        },
        this.setResultsLoading = function () {
            this.resultsContainer.html('');
            this.resultsContainer.addClass('loading');
        },
        this.resetSearch = function () {
            this.toolbarContainer.addClass('hidden');
            this.pageTitle.addClass('hidden'); // hide page title
            this.resultsContainer.removeClass('loading');
            this.resultsContainer.html('<div class="span12 search-empty">' + this.strings.search_init + '</div>');
        },
        this.doRequest = function () {
            this.setResultsLoading();

            var $queryVal = this.inputField.val();

            if ($queryVal == '' || $queryVal.length < searchInputMinCharCount) {
                this.resetSearch();
                return false;
            }

            var that = this;
            jQuery.ajax({
                url: that.search_ajax_url,
                data: {
                    q: $queryVal,
                    page: that.currentPage,
                    rows: that.resultRows
                },
                dataType: 'json'
            }).success(function (data) {
                that.updateResults(data);
            });
        },
        this.renderResults = function (data) {
            var output = '';
            jQuery(data.items).each(function (index, item) {
                output += '<div class="span6">';
                output += '<div class="search-page-item clearfix">';
                output += '<a href="' + item.url + ( item.sparepart ? ("?highlight=" + item.sparepart) : "" ) + '">';
                output += '<div class="img-wrap">';
                if (item.image_url) {
                    output += '<img src="' + item.image_url + '" />';
                }
                output += '</div>';
                output += '<div class="item-content">';
                output += '<span class="item_type">' + (item.item_type ? item.item_type : '&nbsp;' ) + '</span>';
                output += '<span class="name">' + (item.item_name ? item.item_name : item.name) + '</span>';
                output += '<span class="product_no">' + product_no_label + ": " + item.product_no + '</span>';
                output += '<span class="product_ident">' + product_ident_label + ": " + item.product_ident + '</span>';
                output += '</div>';
                output += '</a>';
                output += '</div>';
                output += '</div>';
            });
            return output;
        },
        this.renderToolbar = function (data) {
            return this.renderSearchStats(data) + this.renderPagination(data);
        },
        this.renderPagination = function (data) {
            var output = '<ul class="clearfix pagination">';
            if (this.currentPage > 1) {
                output += '<li><a class="prev-page" href="#">' + previous_page_label + '</a></li>';
            }
            output += '<li>' + page_label + ': ' + this.currentPage + '</a></li>';
            if ((this.currentPage * data.meta.docsCount) < data.meta.solrFound) {
                output += '<li><a class="next-page" href="#">' + next_page_label + '</a></li>';
            }
            return output + '</ul>';
        },
        this.renderSearchStats = function (data) {
            var resultTo = (this.currentPage * this.resultRows > data.meta.numFound) ? data.meta.numFound : this.currentPage * this.resultRows;
            var page = (((this.currentPage - 1) * this.resultRows) + 1);

            var output = '<div class="search-stats">';
            output += result_label + ' ' + data.meta.numFound;
            return output + '</div>';
        },
        this.getSearchPageUrl = function () {
            return this.search_page_url + '?q=' + this.inputField.val();
        },
        this.updateResults = function (data) {
            // show title and toolbars
            this.pageTitle.removeClass('hidden');
            this.toolbarContainer.removeClass('hidden');

            // remove loading indicator
            this.resultsContainer.removeClass('loading');

            // render parts and update dom
            this.resultsContainer.html(this.renderResults(data));
            this.toolbarContainer.html(this.renderToolbar(data));

            console.log(data.meta.request_url);
        }
}

function _initSearchPage() {
    var searchPage = new SearchPage();
    searchPage.init();
    if (searchPage.inputField.val() != '') {
        searchPage.doRequest();
    }
}

function _initGlobalSearch() {
    var search = new Search();
    search.init();
}

function _initFancyBox() {

    jQuery('a.fancy').fancybox({
        type: 'image'
    });

    jQuery('a.fancy').on('click', function (e) {
        e.preventDefault();
    });
}

var is_product_view,
    is_landing_page,
    is_category_view,
    is_servicecenter,
    is_search_page;

jQuery(document).ready(function () {
    // init helper
    is_product_view = jQuery('body').hasClass('catalog-product-view');
    is_landing_page = jQuery('body').hasClass('cms-index-index');
    is_category_view = jQuery('body').hasClass('catalog-category-view');
    is_servicecenter = jQuery('body').hasClass('servicecenter-index-index');
    is_search_page = jQuery('body').hasClass('solrsearch-index-index');


    // check for page and init required stuff
    // global js
    _initServiceline();
    _initDropdowns();
    _initStyle();

    // landing page only js
    if (is_landing_page) {
        _initProductSlider();
    }

    if (is_category_view) {
        _initProductGridIsotope();
        _initProductionYearSlider();
        _initManufacturerSelection();
    }

    if (is_product_view) {
        _initProductDetailPage();
        _initFancyBox();
    }

    if (is_servicecenter) {
        _initServicecenter();
    }

    if (is_search_page) {
        _initSearchPage();

    }
    _initIscProductSlider();
    _initGlobalSearch();
    _initProductList();

    jQuery.fn.collapse.Constructor.prototype.transition = function (method, startEvent, completeEvent) {
        var that = this
            , complete = function () {
                if (startEvent.type == 'show') that.reset()
                that.transitioning = 0
                that.$element.trigger(completeEvent)
            }

        this.$element.triggerHandler(startEvent)

        if (startEvent.isDefaultPrevented()) return

        this.transitioning = 1

        this.$element[method]('in')

        jQuery.support.transition && this.$element.hasClass('collapse') ?
            this.$element.one(jQuery.support.transition.end, complete) :
            complete()
    };


    jQuery.fn.collapse.Constructor.prototype.transition = function (method, startEvent, completeEvent) {
        var that = this
            , complete = function () {
                if (startEvent.type == 'show') that.reset()
                that.transitioning = 0
                that.$element.trigger(completeEvent)
            }

        this.$element.triggerHandler(startEvent)

        if (startEvent.isDefaultPrevented()) return

        this.transitioning = 1

        this.$element[method]('in')

        jQuery.support.transition && this.$element.hasClass('collapse') ?
            this.$element.one(jQuery.support.transition.end, complete) :
            complete()
    };

});

jQuery(window).smartresize(function () {
    if (is_product_view) {
        jQuery('#product-popover').remove(); // remove all product popovers on resize
    }

});

jQuery(window).load(function () {
    if (is_product_view) {
        _initExplosionZoomer();
    }
});

jQuery(document).on('click', '.servicecenter-submit', function(event) {
    addressForm = new VarienForm('service-center-address-form');
    if(addressForm.validator.validate()) {
        jQuery('body').append('<div id="loadingOverlay"><div class="loading">&nbsp;</div></div>');
        jQuery.ajax({
            type: "POST",
            url: submitUrl,
            data: jQuery("#service-center-address-form").serialize()
        })
            .done(function (msg) {
                if (!msg){
                    window.location.reload();
                }
                else {
                    var response = JSON.parse(msg);
                    jQuery("#servicecenter.servicecenter-page").replaceWith(response.content);
                }
                jQuery('#loadingOverlay').remove();
            });
    }
    event.preventDefault();
});

jQuery(document).on("click", '.failure-type-submit', function (e) {
    e.preventDefault();
    saveFailureDescription(jQuery('.failure-type-submit'), "#servicecenter-step-address");
});

jQuery(document).on("click", "#servicecenter-step-content .address_add_button", function() {
    jQuery('#service-center-new-address-form, #service-center-new-address-form-right').show();
    jQuery('#address-address-select').val("");
})

jQuery(document).on("change", '#servicecenter-step-content #address-address-select', function() {
    if(jQuery('#address-address-select').val() != "") {
        jQuery('#service-center-new-address-form, #service-center-new-address-form-right').hide();
    } else {
        jQuery('#service-center-new-address-form, #service-center-new-address-form-right').show();
    }
})