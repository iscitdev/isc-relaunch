/**
 * Created with JetBrains PhpStorm.
 * User: avolk
 * Date: 30.10.13
 * Time: 12:40
 * To change this template use File | Settings | File Templates.
 */
var ServiceCenterAddress = Class.create();
ServiceCenterAddress.prototype = {
    /**
     * Initializes the values given in template address.phtml
     *
     * @param form the form for adding an address
     * @param saveUrl the url of save action of the AddressController
     */
    initialize: function(form, addressUrl, saveUrl){
        this.form = form;
        if ($(this.form)) {
            $(this.form).observe('submit', function(event){this.save();Event.stop(event);}.bind(this));
        }
        this.saveUrl = saveUrl;
        this.addressUrl = addressUrl;
        this.onSave = this.nextStep.bindAsEventListener(this);
    },

    /**
     * Shows the new address form if checkbox is activated
     *
     * @param isNew true if the checkbox is activated
     */
    newAddress: function(isNew){
        if (isNew) {
            this.resetSelectedAddress();
            Element.show('service-center-new-address-form');
            Element.show('service-center-new-address-form-right');
        } else {
            Element.hide('service-center-new-address-form');
            Element.hide('service-center-new-address-form-right');
        }
    },

    resetSelectedAddress: function(){
        var selectElement = $('address-address-select')
        if (selectElement) {
            selectElement.value='';
        }
    },

    /**
     * Calls the save action of the AddressController adds the address data to the request
     */
    save: function(){
        var validator = new Validation(this.form);
        if (validator.validate()) {
            jQuery("button[type=submit]").prop("disabled", true);
            var request = new Ajax.Request(
                this.saveUrl,
                {
                    method: 'post',
                    onComplete: this.onComplete,
                    onSuccess: this.onSave,
                    onFailure: "",
                    parameters: Form.serialize(this.form)
                }
            );
        }
    },

    /**
     This method recieves the AJAX response on success.

     */
    nextStep: function(transport){

        var response = JSON.parse(transport.responseText);

        //TODO Replace hard coded status
        switch(response.status) {
            case 1:
                //showNextStep(response.nextStepId);
                //showMessage(["Address successfully added."], TYPE_SUCCESS_MESSAGE, '#servicecenter-step-reason');
                jQuery("#service-center-address-form").submit();
                break;
            case 2:
                //showNextStep(response.nextStepId);
                jQuery("#service-center-address-form").submit();
                break;
            case 3:
                jQuery('.step-active img').hide();
                showMessage(response.errormessage, TYPE_ERROR_MESSAGE);
                break;
        }

        return false;

    }
}
