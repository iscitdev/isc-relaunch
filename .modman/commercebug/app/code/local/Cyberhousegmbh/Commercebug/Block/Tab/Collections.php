<?php
/**
* Copyright © Pulsestorm LLC: All rights reserved
*/

class Cyberhousegmbh_Commercebug_Block_Tab_Collections extends Cyberhousegmbh_Commercebug_Block_Html
{
    public function __construct()
    {						
        $this->setTemplate('tabs/ascommercebug_collections.phtml');
    }		
}