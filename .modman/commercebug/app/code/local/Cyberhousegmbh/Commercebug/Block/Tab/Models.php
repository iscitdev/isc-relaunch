<?php
/**
* Copyright © Pulsestorm LLC: All rights reserved
*/

class Cyberhousegmbh_Commercebug_Block_Tab_Models extends Cyberhousegmbh_Commercebug_Block_Html
{
    public function __construct()
    {						
        $this->setTemplate('tabs/ascommercebug_models.phtml');
    }		
}