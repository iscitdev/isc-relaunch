<?php
class Cyberhousegmbh_Commercebug_Model_Crossareaajax_Clearcache extends Cyberhousegmbh_Commercebug_Model_Crossareaajax
{
    public function handleRequest()
    {
        $shim = $this->getShim();
        $shim->helper('commercebug/cacheclearer')->clearCache();
        $this->endWithHtml('Cache Cleared');    
    }
}