<?php
class Cyberhousegmbh_Commercebug_Model_Crossareaajax_Toggletranslation extends Cyberhousegmbh_Commercebug_Model_Crossareaajax
{
    public function handleRequest()
    {
        $session = $this->_getSessionObject();        
        $c = $session->getData(Cyberhousegmbh_Commercebug_Model_Observer::INLINE_TRANSLATION_ON);
        $c = $c == 'on' ? 'off' : 'on';        
        $session->setData(Cyberhousegmbh_Commercebug_Model_Observer::INLINE_TRANSLATION_ON, $c);        
        $this->endWithHtml('Inline Translation is ' . ucwords($c) .' -- Refresh Page to Access.');        
    }
}