<?php
/**
* Copyright © Pulsestorm LLC: All rights reserved
*/

class Cyberhousegmbh_Commercebug_Model_Layout_Update extends Fooman_SpeedsterAdvanced_Model_Core_Layout_Update {
    //we'll display this request's package layout
    
    //we'll also display this request's "reduced" layout, etc. etc
    

    public function getPackageLayout() {
        $this->fetchFileLayoutUpdates();
        return $this->_packageLayout;
    }		
}