<?php
/**
* Copyright © Pulsestorm LLC: All rights reserved
*/
class Cyberhousegmbh_Commercebug_Model_Shim
{
    static protected $_instance;
    
    public function createBlock($type, $name='', array $attributes = array())
    {
        $shim = self::getInstance();
        $layout = $shim->getSingleton('core/layout');

        $is_mage_2 = $this->_isMage2();
        if($is_mage_2)
        {            
            $type = $this->_aliasToDefaultClass($type, 'Block');
            return $layout->createBlock($type, $name, $attributes);
        }
        else
        {
            return $layout->createBlock($type, $name, $attributes);
        }        
    }
    
    public function helper($class)
    {
        // var_dump($class);
        $is_mage_2 = $this->_isMage2();
        if($is_mage_2)
        {        
            $class = $this->_aliasToDefaultClass($class, 'Helper');            
            return Mage::helper($class);
        }
        else
        {
            #var_dump($class);
            return Mage::helper($class);
        }
    
    }
    
    public function getModel($modelClass = '', $arguments = array())
    {
        $is_mage_2 = $this->_isMage2();
        if($is_mage_2)
        {
            $class = $this->_aliasToDefaultClass($modelClass);
            return Mage::getModel($class, $arguments);
        }
        else
        {
            return Mage::getModel($modelClass, $arguments);
        }
    }
    
    public function getSingleton($modelClass = '', $arguments = array())
    {
        $is_mage_2 = $this->_isMage2();
        if($is_mage_2)
        {
            $class = $this->_aliasToDefaultClass($modelClass);
            return Mage::getSingleton($class, $arguments);
        }
        else
        {
            return Mage::getSingleton($modelClass, $arguments);
        }
    }
    
    protected function _getNamespaceForGroup($group)
    {
        switch($group)
        {
            case 'commercebug':
                return 'Alanstormdotcom';
            default:
                return 'Mage';
        }
        
    }
    
    protected function _aliasToDefaultClass($string,$context='Model')
    {
        if($context == 'Helper' && strpos($string, '/') === false)
        {
            $string .= '/data';
        }    
                
        list($group, $class) = explode('/',$string);    
        
        $class = str_replace(' ','_',ucwords(str_replace('_',' ',$class)));
        $namespace = $this->_getNamespaceForGroup($group);
        $class = $namespace . '_' . ucwords($group) . '_' . $context . '_' . $class;
        // var_dump($class);
        return $class;
    }
    
    protected function _isMage2()
    {
        $is_mage_1 = version_compare('1.99',Mage::getVersion(),'>');      
        return !$is_mage_1;
    }    

    public function isMage2()
    {
        return $this->_isMage2();
    }
    
    static public function getInstance()
    {
        if(!self::$_instance)
        {
            self::$_instance = new Cyberhousegmbh_Commercebug_Model_Shim;
        }
        
        return self::$_instance;
    }
    
    protected function _getProtectedPropertyFromObject($property, $object)
    {
        $r = new ReflectionClass($object);
        $p = $r->getProperty($property);
        $p->setAccessible(true);            
        return $p->getValue($object);    
    }
    
    public function getGroupedClassName($groupType, $classId, $groupRootNode=null)
    {
        if($this->isMage2())
        {
            
//             $o = Mage::getObjectManager();
//             $di = $this->_getProtectedPropertyFromObject('_di', $o);            
//             $class = $di->instanceManager()->getClassFromAlias('Mage_Core_Block_Template');
//             var_dump($class);
//             exit;
//             $definitions = $this->_getProtectedPropertyFromObject('definitions', $di);
//             foreach($definitions as $definition)
//             {
//                 var_dump($definition);                
//             }
            //var_dump($p->getValue($o));
            
//             exit;
//             $class = Mage::getObjectManager()->getClassFromAlias('Mage_Core_Model_Template');
            var_dump(__METHOD__);
//             var_dump($class);
            exit;
        }
        else
        {
            return Mage::getConfig()->getGroupedClassName($groupType, $classId, $groupRootNode);
        }
    }
}