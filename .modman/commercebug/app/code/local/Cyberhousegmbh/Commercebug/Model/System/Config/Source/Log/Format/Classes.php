<?php
/**
* Copyright © Pulsestorm LLC: All rights reserved
*/
class Cyberhousegmbh_Commercebug_Model_System_Config_Source_Log_Format_Classes
{
    public function toOptionArray()
    {
        return array(
        'Cyberhousegmbh_Commercebug_Helper_Formatlog_Raw'=>'Raw JSON Notation',
        'Cyberhousegmbh_Commercebug_Helper_Formatlog_Raw'=>'All Information, Simple Text',
        'custom'=>'Custom Class'
        );
    }
}