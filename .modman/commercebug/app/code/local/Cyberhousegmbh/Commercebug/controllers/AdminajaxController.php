<?php
/**
* Copyright © Pulsestorm LLC: All rights reserved
*/

class Cyberhousegmbh_Commercebug_AdminajaxController extends Mage_Adminhtml_Controller_Action
{
    //the key prevents us from actually doing this
    //"http://magento1point7pointzeropoint1.dev/index.php/commercebugadmin/adminajax/key/1665cf4c988786113cdc4691390b3e32"
    //can't have a base ajax URL
    // protected $_publicActions = array('togglehints');
    
    public function lookupClassAction()
    {
        Mage::getModel('commercebug/crossareaajax_lookupclass')
        ->handleRequest();        
    }
    
    public function togglehintsAction()
    {
        Mage::getModel('commercebug/crossareaajax_togglehints')
        ->handleRequest();
    }

    public function clearcacheAction()
    {
        Mage::getModel('commercebug/crossareaajax_clearcache')
        ->handleRequest();
    }
    
    function lookupUriAction()
    {
        Mage::getModel('commercebug/crossareaajax_lookupuri')
        ->handleRequest();
    }
    
    function toggleblockhintsAction()
    {
        Mage::getModel('commercebug/crossareaajax_toggleblockhints')
        ->handleRequest();
    }
    
    function togglemageloggingAction()
    {
        Mage::getModel('commercebug/crossareaajax_togglemagelogging')
        ->handleRequest();
    }
    
    public function toggletranslationAction()
    {
        Mage::getModel('commercebug/crossareaajax_toggletranslation')
        ->handleRequest();        
    }
    
    function togglecbloggingAction()
    {
        Mage::getModel('commercebug/crossareaajax_togglecblogging')
        ->handleRequest();
    }
}
