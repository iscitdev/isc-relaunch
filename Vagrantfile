PROJECT_NAME = "isc"
IP = "10.10.10.11"
NFS_EXPORT = true

Vagrant.configure("2") do |config|
  config.vm.box = PROJECT_NAME + "/magento"
  config.vm.box_server_url = "http://images.intra.cyberhouse.at:8001"

  config.vm.network :private_network, ip: IP
  config.ssh.forward_agent = true

  config.nfs.map_uid = Process.uid
  config.nfs.map_gid = Process.gid

  config.vm.synced_folder ".", "/vagrant", disabled: true
  config.vm.synced_folder "htdocs", "/vagrant/htdocs", id: "html-root", type: "rsync", rsync__exclude: [".git/","htdocs/var/"], rsync__args: ["--verbose","--archive","-z"]
  if RUBY_PLATFORM =~ /linux/
        config.vm.synced_folder "vendor", "/vagrant/vendor", id:"vendor-root", :nfs => true, nfs_udp: true, nfs_version: 4, nfs_export: NFS_EXPORT
        config.vm.synced_folder ".modman", "/vagrant/.modman", id:"modman-root", :nfs => true, nfs_udp: true, nfs_version: 4, nfs_export: NFS_EXPORT
        config.vm.synced_folder "config", "/vagrant/config", id:"config", :nfs => true, nfs_udp: true, nfs_version: 4, nfs_export: NFS_EXPORT
        config.vm.synced_folder "db", "/vagrant/db", id:"db", :nfs => true, nfs_udp: true, nfs_version: 4, nfs_export: NFS_EXPORT
        config.vm.synced_folder "bin", "/vagrant/bin", id:"bin", :nfs => true, nfs_udp: true, nfs_version: 4, nfs_export: NFS_EXPORT
  elsif RUBY_PLATFORM =~ /darwin/
		config.vm.synced_folder "vendor", "/vagrant/vendor", id:"vendor-root", :nfs => true, nfs_udp: true, nfs_version: 3, nfs_export: NFS_EXPORT
		config.vm.synced_folder ".modman", "/vagrant/.modman", id:"modman-root", :nfs => true, nfs_udp: true, nfs_version: 3, nfs_export: NFS_EXPORT
		config.vm.synced_folder "config", "/vagrant/config", id:"config", :nfs => true, nfs_udp: true, nfs_version: 3, nfs_export: NFS_EXPORT
		config.vm.synced_folder "db", "/vagrant/db", id:"db", :nfs => true, nfs_udp: true, nfs_version: 3, nfs_export: NFS_EXPORT
		config.vm.synced_folder "bin", "/vagrant/bin", id:"bin", :nfs => true, nfs_udp: true, nfs_version: 3, nfs_export: NFS_EXPORT
   else
        config.vm.synced_folder ".","/vagrant", id:"vagrant-root"
   end

  # Set the virtual machine host name
  config.vm.hostname = PROJECT_NAME + ".local"


  config.vm.provision :shell, :inline => "echo \"CET\" |
    sudo tee /etc/timezone && dpkg-reconfigure --frontend noninteractive tzdata"

  config.vm.provider :virtualbox do |v|
    v.customize ["modifyvm", :id, "--name", PROJECT_NAME]
    v.customize ["modifyvm", :id, "--natdnshostresolver1", "on"]
    v.customize ["modifyvm", :id, "--memory", 2048]
    v.customize ["modifyvm", :id, "--vram", 64]
    v.customize ["modifyvm", :id, "--cpus", "1"]
    v.customize ["modifyvm", :id, "--cpuexecutioncap", "100"]
  end

  config.vm.provision "shell", inline: "chmod +x /vagrant/bin/*.sh"
  config.vm.provision "shell", inline: "/vagrant/bin/vagrant_up.sh"
  #, run: "always"
  config.vm.provision "shell", inline: "service nginx restart", run: "always"
  config.vm.provision "shell", inline: "/vagrant/bin/mailcatcher.sh", run: "always"

end