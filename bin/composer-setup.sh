#!/bin/bash

# check for local.xml
if [ ! -h htdocs/app/etc/local.xml ]; then
    ln -s config/local.xml htdocs/app/etc/local.xml
fi

# deploy with modman
cd htdocs && ../bin/modman deploy-all --force