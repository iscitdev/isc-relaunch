#!/usr/bin/env bash
export DEBIAN_FRONTEND=noninteractive

# /tmp has to be world-writable, but sometimes isn't by default.
chmod 0777 /tmp

# copy ssh key
cp -r /vagrant/.ssh/* /home/vagrant/.ssh/
chmod 0600 /home/vagrant/.ssh/*
chown vagrant:vagrant /home/vagrant/.ssh/*



# PHP 5.5 from PPA
apt-get update
apt-get install -y python-software-properties
# Add Mysql 5.6
add-apt-repository -y ppa:ondrej/mysql-5.6
add-apt-repository -y ppa:ondrej/php5
apt-get update

# Required packages
apt-get -q -y install mysql-server
apt-get install -y libnss-mdns curl git libssl0.9.8 sendmail language-pack-de-base vim
apt-get install -y php5-dev php5-cli php5-fpm php5-curl php5-mcrypt php5-gd php5-mysql php-pear php5-tidy nginx

# xDebug
apt-get install php5-xdebug
ln -s /vagrant/config/settings/xdebug.ini /etc/php5/fpm/conf.d/21-xdebug-extra.ini

# Install Ruby 2.1
apt-get install -y build-essential software-properties-common ruby1.9.3 libsqlite3-dev

# Mailcatcher to test emails (needs latest Ruby)
gem install -y mailcatcher
cp /vagrant/config/settings/mailcatcher.ini /etc/php5/fpm/conf.d/22-mailcatcher.ini

#Set up Git interface: use colors, add "git tree" command
git config --global color.ui true
git config --global alias.tree "log --oneline --decorate --all --graph"

# Composer
#curl -sS https://getcomposer.org/installer | php
#mv composer.phar /usr/local/bin/composer

# Modman
#curl -s -o /usr/local/bin/modman https://raw.githubusercontent.com/colinmollenhour/modman/master/modman
#chmod +x /usr/local/bin/modman

# n98-magerun
#curl -s -o /usr/local/bin/n98-magerun https://raw.githubusercontent.com/netz98/n98-magerun/master/n98-magerun.phar
#chmod +x /usr/local/bin/n98-magerun

# make Magento directories writable as needed and add www-data user to vagrant group
chmod -R 0777 /vagrant/htdocs/var /vagrant/htdocs/app/etc /vagrant/htdocs/media
usermod -a -G vagrant www-data
usermod -a -G www-data vagrant


# MySQL configuration, cannot be linked because MySQL refuses to load world-writable configuration
cp -f /vagrant/config/settings/my.cnf /etc/mysql/my.cnf
service mysql restart
# Allow access from host
mysql -uroot -e "GRANT ALL PRIVILEGES ON *.* TO 'root'@'%'; FLUSH PRIVILEGES;"

# Set locale
ln -fs /vagrant/config/settings/locale /etc/default/locale


ln -fs /vagrant/config/settings/nginx/default /etc/nginx/sites-available/default
service nginx reload